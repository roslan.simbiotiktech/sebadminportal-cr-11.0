package com.seb.admin.portal.spg.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.constant.SPGConstant;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SPGAPIGeneral {

	@JsonProperty("uid")
	private String uid = SPGConstant.apiUID;
	
	@JsonProperty("pw")
	private String pw = SPGConstant.apiPassword;
	
	@JsonProperty("admin_id")
	private String admin_id;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public String getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}

	@Override
	public String toString() {
		return "uid:[" + uid + "], admin_id:[" + admin_id + "]";
	}
	
	
	
}
