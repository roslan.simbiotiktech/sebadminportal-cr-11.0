package com.seb.admin.portal.spg.entity;

public class UpdateFPXBank {
	
	private int bank_id;
	private String logo;
	private String bank_name;
	private boolean enabled;
	private String bank_type;
	private String hasPic;
	public int getBank_id() {
		return bank_id;
	}
	public void setBank_id(int bank_id) {
		this.bank_id = bank_id;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getBank_type() {
		return bank_type;
	}
	public void setBank_type(String bank_type) {
		this.bank_type = bank_type;
	}
	public String getHasPic() {
		return hasPic;
	}
	public void setHasPic(String hasPic) {
		this.hasPic = hasPic;
	}
	@Override
	public String toString() {
		return "bank_id:[" + bank_id + "], logo:[" + logo + "], bank_name:[" + bank_name + "], hasPic:[" + hasPic + "]";
	}
	
	
}
