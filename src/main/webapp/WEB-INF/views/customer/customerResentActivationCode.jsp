<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="" name="description">
<meta content="" name="author">
<%@ include file="../menuTitle.jsp"%>
<%@ include file="../global.jsp"%>
<title></title>
<style type="text/css">
.test[style] {
	padding-right: 0 !important;
}

.test.modal-open {
	overflow: auto;
}
</style>
</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->

		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="font-weight: bold">Customer&gt; Resent Activation Code</div>
						<div class="panel-body">
							<form id="searchForm" name="searchForm" method="post" action="${pageContext.request.contextPath}/admin/resendActivationCode.do" class="form-horizontal">
								${successMsg}${errorMsg}
								<div id="successMsg"></div>
								<div id="errorMsg"></div>
								<div class="form-group" id="msisdnDiv">
									<p class="col-lg-5 control-label">Mobile Number :</p>
									<div class="col-lg-3">
										<input class="form-control" id="msisdn" maxlength="20"
											name="msisdn" placeholder="Mobile Number" type="text">
									</div>
								</div>

								<div class="form-group" id="nameDiv">
									<p class="col-lg-5 control-label">Name :</p>
									<div class="col-lg-3">
										<input class="form-control" id="name" maxlength="160"
											name="name" placeholder="Name" type="text">
									</div>
								</div>

								<div class="form-group" id="loginIDDiv">
									<p class="col-lg-5 control-label">Login ID :</p>
									<div class="col-lg-3">
										<input class="form-control" id="loginID" maxlength="50"
											name="loginID" placeholder="Login ID" type="text">
									</div>
								</div>

								<div class="form-group" id="emailDiv">
									<p class="col-lg-5 control-label">Email :</p>
									<div class="col-lg-3">
										<input class="form-control" id="email" maxlength="50"
											name="email" placeholder="Email" type="text">
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden"
											value="${_csrf.token}">
										<button class="btn btn-primary" id="submitBtn" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1"
											onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
							</form>

							<div id="resultDiv" style="display: none; margin-top: 10%">
								<form class="form-horizontal" id="resendForm" method="post" name="resendForm">
									<h4>
										Search Results : Mobile Number :<b>${msisdn}</b>,Name :<b>${name}</b>, <br>
										Login Id :<b>${loginId}</b>,Email :<b>${email}</b>
									</h4>
									<div class="form-group">
										<div class="alert alert-danger" id="messages" role="alert" style="display: none;"></div>
									</div>

									<table cellspacing="0" class="table table-bordered"
										id="tblSearch">
										<thead>
											<tr>
												<th>No.</th>
												<th>Login ID</th>
												<th>Mobile Number</th>
												<th>Last Login Date</th>
												<th>Contract Account Number</th>
												<th>Contract Account Name</th>
												<th>Account Status</th>

											</tr>
											<!-- #viewATForm -->
											<!-- #resultDiv -->
										</thead>
										<tbody>
										</tbody>
									</table>
									<input type="hidden" name="loginId" id="loginId" value="" />
									<input type="hidden" name="txtRemark" id="txtRemark" value="" />
									<input type="hidden" name="txtPCM" id="txtPCM" value="" />
									<input type="hidden" name="txtMsisdn" id="txtMsisdn" value="" />
									<!-- Open Edit Modal -->
									<div class="modal" id="resendActivationCodeModal" tabindex="-1"
										role="dialog" aria-labelledby="resendActivationCodeModalLabel"
										aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button class="close" data-dismiss="modal" type="button">
														<span>&times;</span><span class="sr-only">Close</span>
													</button>
													<h2 class="modal-title">Resend Activation Code</h2>
												</div>
												<div class="modal-body" id="resendContent">
													<div class="form-group">
														<p class="col-lg-6 control-label">Login ID :</p>
														<div class="col-lg-4 control-label"
															style="text-align: left;">
															<label class="lblNoBoldCls" id="mLoginId"
																name="mLoginId" />
														</div>
													</div>
													<div class="form-group">
														<p class="col-lg-6 control-label">Email :</p>
														<p class="col-lg-4 control-label"
															style="text-align: left;">
															<label class="lblNoBoldCls" id="mEmail"
																name="mEmail" />
														</p>
													</div>
													<div class="form-group">
														<p class="col-lg-6 control-label">Mobile Number :</p>
														<p class="col-lg-4 control-label"
															style="text-align: left;">
															<label class="lblNoBoldCls" id="mMsisdn"
																name="mMsisdn" />
														</p>
													</div>
													<div class="form-group">
														<p class="col-lg-6 control-label">Activation Code Type <span style="color: red;">*</span>: </p>
														<div class="col-lg-4 control-label" style="text-align: left;">
															<div class="radio">
																<label><input type="radio" name="activateOpt" value="PCM1">Forgot Password</label>
															</div>
															<div class="radio">
															  	<label><input type="radio" name="activateOpt" value="PCM2">Change Mobile Number</label>
															</div>
															<div class="radio">
															  <label><input type="radio" name="activateOpt" value="PCM3">Change Password</label>
															</div>														
														</div>
													</div>
													<div id="newMsisdnDiv" style="display:none" class="form-group" >
														<p class="col-lg-6 control-label">New Mobile Number <span style="color: red;">*</span> :</p>
														<p class="col-lg-4 control-label" style="text-align: left;">
															<input type="text" class="form-control"id="mNewMsisdn"
																name="mNewMsisdn" maxlength="20"/>
														</p>
													</div>
													<div class="form-group">
														<p class="col-lg-6 control-label">Remark <span style="color: red;">*</span> :</p>
														<p class="col-lg-4 control-label"
															style="text-align: left;">
															<textarea class="form-control" cols="23" id="mRemark"
																name="mRemark" maxlength="100" rows="5"></textarea>
														</p>
													</div>
													<div class="form-group">
														<p class="col-lg-6 control-label">Last Login Date :</p>
														<p class="col-lg-4 control-label"
															style="text-align: left;">
															<label class="lblNoBoldCls" id="mLastLoginAt"
																name="mLastLoginAt" />
														</p>
													</div>
												</div>
												<div class="modal-footer">
													<button class="btn btn-primary"  type="submit">Resend Activation Code</button>
													<button class="btn btn-warning" data-dismiss="modal"
														type="button">Cancel</button>
												</div>
											</div>
										</div>
									</div>
									<!-- #Open Edit Modal -->
									<!-- Confirmation YES/NO -->
								<div class="modal" id="confirmModal" tabindex="-1"
									role="dialog" aria-labelledby="myModalLabel"
									aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-hidden="true">&times;</button>
												<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
											</div>
											<div id="confirmContent" class="modal-body"></div>
											<div class="modal-footer">
												<button class="btn btn-primary" id="confirmResendBtn"
													type="button">Update</button>
												<button type="button" class="btn btn-primary"
													data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- #Confirmation YES/NO -->
								<!-- Status Modal -->
								<div class="modal" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title" id="myModalLabel">Resend Activation Code</h4>
											</div>
											<div id=statusContent class="modal-body"></div>
											<div class="modal-footer">
												<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- #Status Modal -->
								</form>
								
							</div>
							<!-- #resultDiv -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel panel-default -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
		</div>
	</div>
	<script type="text/javascript">
	function resetForm() {
	    $('#').find('.has-error').removeClass("has-error");
	    $('#searchForm').find('.has-success').removeClass("has-success");
	    $('#searchForm').find('.form-control-feedback').removeClass('glyphicon-remove');
	    $('#searchForm').find('.form-control-feedback').removeClass('glyphicon-ok');
	    $('#searchForm').formValidation('resetForm', true);
	    $('.form-group').find('small.help-block').hide();
	    $('.form-group').removeClass('has-error has-feedback');
	    $('.form-group').find('i.form-control-feedback').hide();
	    return false;
	}

	function onClickReset(loginId, email,mobileNumber,lastLoginAt) {
	    $('#loginId').val(loginId);
	    $("#mLoginId").html(loginId);
    	$("#mEmail").html(email);
    	$("#mMsisdn").html(mobileNumber);
    	$("#mLastLoginAt").html(lastLoginAt);
	}
	$(document).ready(function() {
	    if (${userList != null && userList.size() >= 0}) {
	        $('#resultDiv').show();
	        var columns = [{
	            "mDataProp": "loginId"
	        }, {
	            "mDataProp": "loginId",
	            "mRender": function(data, type, row) {
	                var html = '';
	                if (data != null) {
	                    html = "<p><a data-toggle=\"modal\" data-target=\"#resendActivationCodeModal\" onclick=\"onClickReset('" + row.loginId + "','" + row.email + "','" + row.mobileNumber + "','" + row.lastLoginAt + "')\">" + data + "</a></p>";
	                }
	                return html;
	            }
	        }, {
	            "mDataProp": "mobileNumber"
	        }, {
	            "mDataProp": "lastLoginAt"
	        }, {
	            "mDataProp": "contractSubscribed",
	            "mRender": function(data, type, row) {
	                var html = '';
	                if (data != null) {
	                    $.each(data, function(index, currEmp) {
	                        html += "<p>" + currEmp.accountNumber + "</p>";
	                    });
	                    html += "";
	                }
	                return html;
	            }
	        }, {
	            "mDataProp": "contractSubscribed",
	            "mRender": function(data) {
	                var html = '<div>';
	                if (data != null) {
	                    $.each(data, function(index, currEmp) {
	                        html += "<p>" + currEmp.accountName + "</p>";
	                    });
	                    html += "</div>";
	                }
	                return html;
	            }
	        }, {
	            "mDataProp": "contractSubscribed",
	            "mRender": function(data) {
	                var html = '<div>';
	                if (data != null) {
	                    $.each(data, function(index, currEmp) {
	                        html += "<p>" + currEmp.subscriptionStatus + "</p>";
	                    });
	                    html += "</div>";
	                }
	                return html;
	            }
	        }];
	        var table = $('#tblSearch').DataTable({
	            bFilter: false,
	            bSort: false,
	            lengthMenu: [
	                [50],
	                [50]
	            ],
	            "bProcesing": true,
	            "bServerSide": true,
	            "bLengthChange": false,
	            "iDisplayStart": 0,
	            "sEmptyTable": "No data available in table",
	            "sAjaxSource": "${pageContext.request.contextPath}/admin/searchProfileTable.do?${_csrf.parameterName}=${_csrf.token}&name=${name}&email=${email}&msisdn=${msisdn}&contractAccNo=${contractAccNo}&&loginId=${loginId}",
	            "aoColumns": columns,
	            "fnServerData": function(sSource, aoData, fnCallback) {
	                $.ajax({
	                    beforeSend: function(xhr) {
	                        sebApp.showIndicator();
	                    },
	                    dataType: 'json',
	                    type: "POST",
	                    url: sSource,
	                    data: aoData,
	                    success: function(data, textStatus, jqXHR) {
	                        fnCallback(data, textStatus, jqXHR);
	                    },
	                    failure: function(data) {},
	                    complete: function(e) {
	                        sebApp.hideIndicator();
	                    }
	                });
	            },
	            "rowCallback": function(row, data, iDisplayIndex) {
	                var info = table.page.info();
	                var page = info.page;
	                var length = info.length;
	                var index = (page * length + (iDisplayIndex + 1));
	                $('td:eq(0)', row).html(index);
	            },
	        });
	    }
	    $('input[type=radio][name=activateOpt]').change(function() {
	        if (this.value == 'PCM2') {
	          $('#newMsisdnDiv').show();
	          $('#resendForm').formValidation('revalidateField', 'mNewMsisdn');
	        }
	        else {
	        	$('#newMsisdnDiv').hide();
	        	 $('#resendForm').formValidation('revalidateField', 'mNewMsisdn');
	        }
	    });
	    
	    $("#mNewMsisdn").keypress(function(e) {
	        //if the letter is not digit then display error and don't type anything
	        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	            return false;
	        }
	    });
	    
	    $('#resendActivationCodeModal').on('show.bs.modal', function (e) {
	    	$('body').addClass('test');
		})
		$('#statusModal').on('show.bs.modal', function (e) {
	    	$('body').addClass('test');
		})
		$('#resendActivationCodeModal').on('hidden.bs.modal', function() {
			$('#resendForm').formValidation('resetForm', true);
		});
	    
	    $("button#confirmResendBtn").click(function() {
	    	console.log("confirmResendBtn clicked");
	    	
	        $('#resendActivationCodeModal').modal('hide');
	        console.log('radio:'+ $("input[type='radio'][name='activateOpt']:checked").val());
	        console.log('remark:'+ $('#txtRemark').val());
	        console.log('msisdn:'+  $('#txtMsisdn').val());
	        var token = $("meta[name='_csrf']").attr("content");
	        var header = $("meta[name='_csrf_header']").attr("content");
	        $.ajax({
	            url: "${pageContext.request.contextPath}/admin/updateActivationCode.do",
	            type: "POST",
	            dataType: "json",
	            data: JSON.stringify({
	                loginId: $('#loginId').val(),
	                type: $("#txtPCM").val(),
	                remark : $('#txtRemark').val(),
	                mobileNumber :  $('#txtMsisdn').val()
	                
	            }),
	            beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
	                xhr.setRequestHeader("Accept", "application/json");
	                xhr.setRequestHeader("Content-Type", "application/json");
	                sebApp.showIndicator();
	                
	            },
	            success: function(data) {
	                if (data.respStat != null && data.respStat == 0) {
	                    $('#statusContent').html('['+data.typeDesc+'] activation code [<b>' + data.otp + '</b>] had been sent to user[' + data.loginId + '].');

	                } else if (data.respStat != null && data.respStat != 0) {
	                    $('#statusContent').html('<span style="color:red;">['+data.typeDesc+'] activation code FAILED to resend.</span> ');
	                }
	            },
	            failure: function(data) {
                    $('#statusContent').html('<span style="color:red;">Activation code FAILED to resend.</span>');
	            },
	            complete: function() {
	                sebApp.hideIndicator();
	                $('#statusModal').modal('show');
	                $('#confirmModal').modal('hide');
	                
	            }
	        });
	    })
	    $('#searchForm').formValidation({
	        framework: 'bootstrap',
	        icon: {
	            invalid: 'glyphicon glyphicon-remove',
	        },
	        fields: {
	            msisdn: {
	                validators: {
	                    stringLength: {
	                        min: 10,
	                        max: 20,
	                        message: 'Mobile Number must at least 10 digits'
	                    }
	                }
	            },
	            email: {
	                validators: {
	                    emailAddress: {
	                        message: 'The value is not a valid email address'
	                    }
	                }
	            }
	        }
	    });
	    $('#resendForm').formValidation({
	       	framework: 'bootstrap',
	     	excluded: ':disabled',
	     	 icon: {
       	      invalid: 'glyphicon glyphicon-remove',
       	    },
	        fields: {
	        	activateOpt: {
                    validators: {
                        notEmpty: {
                            message: 'Activation Code Type is required.'
                        }
                    }
                },
	        	mRemark: {
	                validators: {
	                    notEmpty: {
	                        message: 'Remark is required'
	                    },
	                    stringLength : {
							min : 1,
							max : 100,
							message : 'Remark must less than 100 characters'
						}
	                } 
	            },
	            mNewMsisdn : {
					validators : {
						callback : {
							callback : function(value, validator, $field) {
							   var radio = $('#resendForm').find('[name="activateOpt"]:checked').val();
							   var reg = /^\d+$/;
							   console.log(reg.test(value));
								if (radio == 'PCM2' && $.trim(value) == '') {
									return {
										valid : false,
										message : 'New Mobile Number is required'
									}
								}
								else if (radio == 'PCM2' && !reg.test(value)) {
									return {
										valid : false,
										message : 'New Mobile Number is only digits allowed.'
									}
								}else {
									return true;
								}
							}
						}
						 
					}
				}
	        }
	    }).on('success.form.fv', function(e) {
	        // Prevent form submission
			e.preventDefault();
			$('#txtRemark').val($('#mRemark').val());
			$('#txtPCM').val($("input[type='radio'][name='activateOpt']:checked").val());
			$('#txtMsisdn').val($('#mNewMsisdn').val());
	    	$('#confirmContent').html("Are you sure you want to resend activation code to user [<b>"+$('#loginId').val()+"</b>]?");
	      	
	      	var isValidForm = $('#resendForm').data('formValidation').isValid();
	      	if(isValidForm){
	      		$('#resendActivationCodeModal').modal('hide'); 
				$('#confirmModal').modal('show'); 
	      	}
	  });
	});
</script>
</body>
</html>