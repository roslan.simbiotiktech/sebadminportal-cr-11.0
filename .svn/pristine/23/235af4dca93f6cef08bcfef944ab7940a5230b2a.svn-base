package com.seb.admin.portal.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.seb.admin.portal.adapter.ResendOTPRequest;
import com.seb.admin.portal.adapter.ResendOTPResponse;
import com.seb.admin.portal.adapter.ViewProfileResponse;
import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.constant.MakeAReportStatus;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.model.AuditTrailType;
import com.seb.admin.portal.model.BankResponse;
import com.seb.admin.portal.model.CustomerAuditTrail;
import com.seb.admin.portal.model.MyBillPayReportReq;
import com.seb.admin.portal.model.MyBillPayReportResp;
import com.seb.admin.portal.model.MyReportRequest;
import com.seb.admin.portal.model.MyUserStatistic;
import com.seb.admin.portal.model.PaymentGateway;
import com.seb.admin.portal.model.Report;
import com.seb.admin.portal.service.LoginService;
import com.seb.admin.portal.service.ReportService;
import com.seb.admin.portal.util.DateUtil;
import com.seb.admin.portal.util.StringUtil;

@Controller
public class ReportController {

	private static final Logger log = Logger.getLogger(ReportController.class);

	@Autowired
	private LoginService loginService;
	@Autowired
	private ReportService reportService;


	/**============================================
	 * Make A Report
	 *============================================*/
	@RequestMapping(value = "/admin/makeAReport",  method = RequestMethod.GET)
	public ModelAndView doMakeAReport()	{
		ModelAndView model = new ModelAndView();
		model.setViewName("/report/makeAReport");
		return model;
	}

	@RequestMapping(value = "/admin/makeAReport",  method = RequestMethod.POST)
	public ModelAndView searchMakeAReport(
			@RequestParam(value="indateFrom", defaultValue = "") String dateFrom,
			@RequestParam(value="indateTo", defaultValue = "") String dateTo,
			@RequestParam(value="selectSource", defaultValue = "") String source,
			@RequestParam(value="selectType", defaultValue = "") String type,
			@RequestParam(value="selectStatus", defaultValue = "") String status,
			@RequestParam(value="displaySource", defaultValue = "") String displaySource,
			@RequestParam(value="displayType", defaultValue = "") String displayType,
			@RequestParam(value="displayStatus", defaultValue = "") String displayStatus)	{

		System.out.println("dateFrom:"+dateFrom);
		System.out.println("dateTo:"+dateTo);
		System.out.println("source:"+source);
		System.out.println("type:"+type);
		System.out.println("status:"+status);

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();
		DateUtil dateUtil = new DateUtil();
		List<Report> reportList = new ArrayList<Report>();
		model.addObject("makeAReportStatus",MakeAReportStatus.values());


		try{
			reportService.auditLog("Make A Report", new AuditTrail(user.getId(),0,"","Search Make A Report: "+user.getLoginId(),"","dateFrom:["+dateFrom+"], dateTo:["+dateTo+"], selectSource:["+displaySource+"], selectType:["+displayType+"], selectStatus:["+displayStatus+"]", ""));		

			if(!(dateFrom.isEmpty() && dateTo.isEmpty() && type.isEmpty() && status.isEmpty())){

				Date convertDateFrom = new Date();
				Date convertDateTo = new Date();

				convertDateFrom  = (StringUtil.trimToEmpty(dateFrom).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateFrom+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");						
				convertDateTo  = (StringUtil.trimToEmpty(dateTo).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateTo+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						

				reportList = reportService.findAllMakeReportByCondition(convertDateFrom, convertDateTo, source, type, status);					


			}else{
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Mandatory inputs are required."));
			}

		}catch(Exception ex){
			reportService.auditLog("Make A Report", new AuditTrail(user.getId(),0,"","Search Make A Report "+user.getLoginId(),"","Fail to process", "dateFrom:["+dateFrom+"], dateTo:["+dateTo+"], selectType:["+displayType+"], selectStatus:["+displayStatus+"]\nFail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search report."));
			ex.printStackTrace();
		}

		model.addObject("reportList", reportList);
		model.addObject("dateFrom", dateFrom);
		model.addObject("dateTo", dateTo);
		model.addObject("status", displayStatus);
		model.addObject("type", displayType);
		model.setViewName("/report/makeAReport");
		return model;
	}

	@RequestMapping(value = "/admin/getMyReportDetails",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody Report getProfileDetails(@RequestBody Report req)
			throws IOException {
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		Report report = new Report();
		try {
			report = reportService.findMakeAReportById(req.getTransId());
		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return report;
	}

	@RequestMapping(value = "/admin/updateMakeAReport",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody Map<String,String> updateMakeAReport(@RequestBody MyReportRequest re)
			throws IOException {
		System.out.println("[updateMakeAReport]");
		System.out.println(re.toString());

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		Map<String,String> map = new HashMap<String,String>();


		try {
			Report newReport = new Report();

			String patternString = "[0-9]+";
			Pattern pattern = Pattern.compile(patternString);


			if(!StringUtil.trimToEmpty(re.getPreCaseNumber()).equalsIgnoreCase(re.getCaseNumber())){
				Matcher matcher = pattern.matcher(re.getCaseNumber());
				boolean matches = matcher.matches();
				if(!matches){
					map.put("error","Please insert only valid case Id.");
					return map;
				}else{
					//set new case id
					System.out.println("valid case number ::"+ Integer.parseInt(re.getCaseNumber().trim()));
					newReport.setCaseNumber(Integer.parseInt(re.getCaseNumber().trim()));
				}					
			}
			if(re.getStatus().equalsIgnoreCase(MakeAReportStatus.CLOSED.getLabel()) || re.getStatus().equalsIgnoreCase(MakeAReportStatus.RESOLVED.getLabel()) && !re.getRemark().equalsIgnoreCase("")){
				newReport.setRemark(re.getRemark());
			}

			newReport.setStatus(re.getStatus());
			newReport.setTransId(re.getTransId());
			System.out.println("audit log");
			map.put("success","success");
			map.put("id",String.valueOf(re.getTransId()));
			map.put("status",re.getStatus());
			map.put("remark",re.getRemark());
			map.put("caseNumber",re.getCaseNumber());


			reportService.updateMakeAReport(newReport);

			reportService.auditLog("Make A Report", new AuditTrail(user.getId(),0,"","Update Make A Report: "+user.getLoginId()+", Trans ID: "+re.getTransId(),"Case ID:["+re.getPreCaseNumber()+"], Status:["+re.getPreStatus()+"], Remark:["+re.getPreRemark()+"]","Case ID:["+re.getCaseNumber()+"], Status:["+re.getStatus()+"], Remark:["+re.getRemark()+"]", ""));		


		}  catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex);
			reportService.auditLog("Make A Report", new AuditTrail(user.getId(),0,"","Update Make A Report: "+user.getLoginId()+", Trans ID: "+re.getTransId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
		}
		return map;
	}


	@RequestMapping(value="/admin/getImages/{type}/{id}")
	public void getUserImage(HttpServletResponse response ,  @PathVariable("type") String type, @PathVariable("id") int id) throws IOException{

		try {
			Report myReport = reportService.findMakeAReportById(id);		

			if(myReport!=null && type.equalsIgnoreCase("1") && !StringUtil.trimToEmpty(myReport.getPhoto1Type()).isEmpty()){
				response.setContentType(myReport.getPhoto1Type());
				byte[] buffer = myReport.getPhoto1();
				InputStream in1 = new ByteArrayInputStream(buffer);
				IOUtils.copy(in1, response.getOutputStream());
				response.getOutputStream().close();

			}else if(myReport!=null && type.equalsIgnoreCase("2") && !StringUtil.trimToEmpty(myReport.getPhoto2Type()).isEmpty()){
				response.setContentType(myReport.getPhoto2Type());
				byte[] buffer = myReport.getPhoto2();
				InputStream in1 = new ByteArrayInputStream(buffer);
				IOUtils.copy(in1, response.getOutputStream());
				response.getOutputStream().close();
			}else if(myReport!=null && type.equalsIgnoreCase("3") && !StringUtil.trimToEmpty(myReport.getPhoto3Type()).isEmpty()){
				response.setContentType(myReport.getPhoto3Type());
				byte[] buffer = myReport.getPhoto3();
				InputStream in1 = new ByteArrayInputStream(buffer);
				IOUtils.copy(in1, response.getOutputStream());
				response.getOutputStream().close();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	/**============================================
	 * Customer Activities Report
	 *============================================*/
	@RequestMapping(value = "/admin/customerActivityReport",  method = RequestMethod.GET)
	public ModelAndView doCustomerActivityReport()	{
		ModelAndView model = new ModelAndView();
		model.setViewName("/report/customerActivityReport");
		return model;
	}

	@RequestMapping(value = "/admin/customerActivityReport",  method = RequestMethod.POST)
	public ModelAndView searchCustomerActivityReport(@RequestParam(value="indateFrom", defaultValue = "") String dateFrom,
			@RequestParam(value="indateTo", defaultValue = "") String dateTo,
			@RequestParam(value="email", defaultValue = "") String email)	{

		log.info("dateFrom:"+dateFrom);
		log.info("dateTo:"+dateTo);
		log.info("email:"+email);

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		DateUtil dateUtil = new DateUtil();

		List<CustomerAuditTrail> reportList = new ArrayList<CustomerAuditTrail>();

		try{
			reportService.auditLog("Customer Activities Report", new AuditTrail(user.getId(),0,"","Search Customer Activities Report: "+user.getLoginId(),"","dateFrom:["+dateFrom+"], dateTo:["+dateTo+"], email:["+email+"]", ""));		

			if(!(dateFrom.isEmpty() && dateTo.isEmpty())){

				Date convertDateFrom = new Date();
				Date convertDateTo = new Date();

				convertDateFrom  = (StringUtil.trimToEmpty(dateFrom).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateFrom+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");						
				convertDateTo  = (StringUtil.trimToEmpty(dateTo).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateTo+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						

				email = StringUtil.trimToEmpty(email);
				reportList = reportService.findAllCustAuditReport(convertDateFrom, convertDateTo, email);					

			}else{
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Mandatory inputs are required."));
			}
		}catch(Exception ex){
			reportService.auditLog("Customer Activities Report", new AuditTrail(user.getId(),0,"","Search Customer Activities Report: "+user.getLoginId(),"","Fail to process", "dateFrom:["+dateFrom+"], dateTo:["+dateTo+"]\nFail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search report."));
			ex.printStackTrace();
		}

		model.addObject("reportList", reportList);
		model.addObject("dateFrom", dateFrom);
		model.addObject("dateTo", dateTo);
		model.addObject("email", email);
		model.setViewName("/report/customerActivityReport");
		return model;
	}







	/**============================================
	 * Audit Trails Report
	 *============================================*/
	@RequestMapping(value = "/admin/auditTrails",  method = RequestMethod.GET)
	public ModelAndView doAuditTrails()	{

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();		
		List <AdminUser> adminList = new ArrayList<AdminUser>();
		List <AuditTrailType> typeList = new ArrayList<AuditTrailType>();

		try{
			adminList = loginService.findAllUser();
			typeList = reportService.findAllAuditType();
		}catch (Exception ex) {
		}	

		model.addObject("adminList", adminList);
		model.addObject("typeList", typeList);
		model.setViewName("/report/auditTrails");
		return model;
	}

	@RequestMapping(value = "/admin/auditTrails",  method = RequestMethod.POST)
	public ModelAndView searchAuditTrails(
			@RequestParam(value="indateFrom", defaultValue = "") String dateFrom,
			@RequestParam(value="indateTo", defaultValue = "") String dateTo,
			@RequestParam(value="selectType", defaultValue = "") String selectType,
			@RequestParam(value="selectLoginId", defaultValue = "") String selectLoginId,
			@RequestParam(value="displayName", defaultValue = "") String displayName,
			@RequestParam(value="displayType", defaultValue = "") String displayType){

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();	

		List <AdminUser> adminList = new ArrayList<AdminUser>();
		List <AuditTrail> auditList = new ArrayList<AuditTrail>();
		List <AuditTrailType> typeList = new ArrayList<AuditTrailType>();

		try{
			DateUtil dateUtil = new DateUtil();
			adminList = loginService.findAllUser();
			typeList = reportService.findAllAuditType();

			if(!(StringUtil.trimToEmpty(dateFrom).isEmpty() && !StringUtil.trimToEmpty(dateTo).isEmpty() && StringUtil.trimToEmpty(selectType).isEmpty() && StringUtil.trimToEmpty(selectLoginId).isEmpty()))
			{
				reportService.auditLog("Audit Trail", new AuditTrail(user.getId(),0,"","Search Audit Trails: "+user.getLoginId(),"","dateFrom:["+dateFrom+"], dateTo:["+dateTo+"], selectType:["+displayType+"], selectLoginId:["+displayName+"]", ""));		

				Date myStartDate = new Date();
				Date myEndDate = new Date();

				myStartDate  = (StringUtil.trimToEmpty(dateFrom).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateFrom+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");						
				myEndDate  = (StringUtil.trimToEmpty(dateTo).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateTo+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						


				if(selectLoginId.equalsIgnoreCase("all") && !selectType.equalsIgnoreCase("all")){
					System.out.println("(type only)search audit trails for loginID ["+selectLoginId+"] and  type ["+selectType+"]");
					auditList = reportService.findAllByType(myStartDate, myEndDate,  Integer.parseInt(selectType));	

				}else if(!selectLoginId.equalsIgnoreCase("all") && selectType.equalsIgnoreCase("all")){
					System.out.println("(login only)search audit trails for loginID ["+selectLoginId+"] and  type ["+selectType+"]");
					auditList = reportService.findAllByAdmin(myStartDate, myEndDate,  Integer.parseInt(selectLoginId));	

				}else if(!(selectLoginId.equalsIgnoreCase("all") && selectType.equalsIgnoreCase("all"))){
					System.out.println("(login & type)search audit trails for loginID ["+selectLoginId+"] and  type ["+selectType+"]");
					auditList = reportService.findAllByTypeAdmin(myStartDate, myEndDate,  Integer.parseInt(selectLoginId), Integer.parseInt(selectType));	
				}
				else{
					System.out.println("search audit trails for loginID ["+selectLoginId+"] and  type ["+selectType+"]");
					auditList = reportService.findAllByDate(myStartDate, myEndDate);	

				}
			}else{
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Mandatory inputs are required."));
			}



		}catch (Exception ex) {
			reportService.auditLog("Audit Trail", new AuditTrail(user.getId(),0,"","Search Audit Trails: "+user.getLoginId(),"","Fail to process", "dateFrom:["+dateFrom+"], dateTo:["+dateTo+"], selectType:["+displayType+"], selectLoginId:["+displayName+"]\nFail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search audit trails."));
			ex.printStackTrace();

		}	

		model.addObject("dateFrom", dateFrom);
		model.addObject("dateTo", dateTo);
		model.addObject("selectType", selectType);
		model.addObject("displayType", displayType);
		model.addObject("displayName", displayName);
		model.addObject("adminList", adminList);
		model.addObject("auditList", auditList);
		model.addObject("typeList", typeList);
		model.setViewName("/report/auditTrails");
		return model;
	}

	/*User Statistic Report*/
	@RequestMapping(value = "/admin/userStatisticReport",  method = RequestMethod.GET)
	public ModelAndView doUserStatisticReport()	{
		ModelAndView model = new ModelAndView();
		model.setViewName("/report/userStatisticReport");
		return model;
	}

	@RequestMapping(value = "/admin/userStatisticReport",  method = RequestMethod.POST)
	public ModelAndView searchUserStatisticReport(@RequestParam(value="indateFrom", defaultValue = "") String dateFrom,
			@RequestParam(value="indateTo", defaultValue = "") String dateTo)	{

		System.out.println("dateFrom:"+dateFrom);
		System.out.println("dateTo:"+dateTo);

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		DateUtil dateUtil = new DateUtil();

		MyUserStatistic userStat = new MyUserStatistic();
		try{

			if(!(dateFrom.isEmpty() && dateTo.isEmpty())){

				Date convertDateFrom = new Date();
				Date convertDateTo = new Date();

				convertDateFrom  = (StringUtil.trimToEmpty(dateFrom).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateFrom+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");						
				convertDateTo  = (StringUtil.trimToEmpty(dateTo).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateTo+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						

				int totalActive = (int) reportService.findTotalByStatus("ACTIVE", convertDateFrom,convertDateTo);
				int totalPending = (int) reportService.findTotalByStatus("PENDING", convertDateFrom,convertDateTo);
				int totalSuspended = (int) reportService.findTotalByStatus("SUSPENDED", convertDateFrom,convertDateTo);

				System.out.println("totalActive>"+totalActive);
				System.out.println("totalPending>"+totalPending);
				System.out.println("totalPending>"+totalPending);

				userStat.setDateFrom(convertDateFrom);
				userStat.setDateTo(convertDateTo);
				userStat.setTotalActive(totalActive);
				userStat.setTotalPending(totalPending);
				userStat.setTotalSuspended(totalSuspended);

				reportService.auditLog("User Statistic Report", new AuditTrail(user.getId(),0,"","Search User Statistic Report: "+user.getLoginId(),"","dateFrom:["+dateFrom+"], dateTo:["+dateTo+"]", ""));		

			}else{
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Mandatory inputs are required."));
			}


		}catch(Exception ex){
			reportService.auditLog("User Statistic Report", new AuditTrail(user.getId(),0,"","Search Customer Activities Report: "+user.getLoginId(),"","Fail to process", "dateFrom:["+dateFrom+"], dateTo:["+dateTo+"]\nFail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search report."));
			ex.printStackTrace();
		}

		model.addObject("userStat", userStat);
		model.addObject("dateFrom", dateFrom);
		model.addObject("dateTo", dateTo);

		model.setViewName("/report/userStatisticReport");
		return model;
	}

	/**============================================
	 * Bill Payment Report
	 *============================================*/
	@RequestMapping(value = "/admin/billPaymentReport",  method = RequestMethod.GET)
	public ModelAndView doBillPaymentReport()	{
		ModelAndView model = new ModelAndView();
		model.setViewName("/report/billPaymentReport");
		return model;
	}

	@RequestMapping(value = "/admin/billPaymentReport",  method = RequestMethod.POST)
	public ModelAndView searchBillPaymentReport(HttpServletRequest req, HttpServletResponse resp, 
			@RequestParam(value="indateFrom", defaultValue = "") String dateFrom,
			@RequestParam(value="indateTo", defaultValue = "") String dateTo,
			@RequestParam(value="selectPymtMethod", defaultValue = "") String selectPymtMethod,
			@RequestParam(value="displayPymtMethod", defaultValue = "") String displayPymtMethod,
			@RequestParam(value="selectSebRefStatus", defaultValue = "") String selectSebRefStatus,
			@RequestParam(value="displaySebRefStatus", defaultValue = "") String displaySebRefStatus,
			@RequestParam(value="contractAccNo", defaultValue = "") String contractAccNo,
			@RequestParam(value="email", defaultValue = "") String email,
			@RequestParam(value="msisdn", defaultValue = "") String msisdn,
			@RequestParam(value="pymtRef", defaultValue = "") String pymtRef,
			@RequestParam(value="fpxTxnId", defaultValue = "") String fpxTxnId,
			@RequestParam(value="mbbRefNo", defaultValue = "") String mbbRefNo
			)	{

		Enumeration params = req.getParameterNames(); 
		while(params.hasMoreElements()){
			String paramName = (String)params.nextElement();
			System.out.println(paramName+" : "+req.getParameter(paramName));
		}

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		DateUtil dateUtil = new DateUtil();
		MyBillPayReportResp r0 = new MyBillPayReportResp();
		MyBillPayReportReq myReq = new MyBillPayReportReq();
		try{

			if(!(dateFrom.isEmpty() && dateTo.isEmpty())){

				Date convertDateFrom = new Date();
				Date convertDateTo = new Date();

				convertDateFrom  = (StringUtil.trimToEmpty(dateFrom).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateFrom+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");						
				convertDateTo  = (StringUtil.trimToEmpty(dateTo).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateTo+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						

				List<PaymentGateway> pgList = reportService.findAllPaymentGateway();

				String searchStr = "";
				boolean isPass = false;
				Map listMap = new HashMap();
				for(PaymentGateway pg : pgList){					
					listMap.put(pg.getGatewayId(),pg.getReferencePrefix());
				}
				if(selectPymtMethod.equalsIgnoreCase("ALL")){
					if(pymtRef.isEmpty()){
						System.out.println("ALL && empty");
						isPass = true;
					}else{
						Iterator iterator = listMap.entrySet().iterator();
						while (iterator.hasNext()) {
							System.out.println("[iterator]");
							Map.Entry mapEntry = (Map.Entry) iterator.next();
							String gatewayId =  (String) mapEntry.getKey();
							String prefix = (String) mapEntry.getValue();
							if(pymtRef.trim().toUpperCase().startsWith(prefix.toUpperCase())){
								System.out.println("not empty && match prefix");
								isPass = true;
								searchStr = pymtRef.toUpperCase().replace(prefix.toUpperCase(), "");
							}
						}
					}
				}else{
					if(pymtRef.isEmpty()){
						System.out.println("NOT ALL && is empty");
						isPass = true;
					}else{
						System.out.println("NOT ALL && NOT empty");
						Iterator iterator = listMap.entrySet().iterator();
						while (iterator.hasNext()) {
							System.out.println("[else:iterator]");
							Map.Entry mapEntry = (Map.Entry) iterator.next();
							String gatewayId =  (String) mapEntry.getKey();
							String prefix = (String) mapEntry.getValue();
							if(selectPymtMethod.equalsIgnoreCase(gatewayId) && pymtRef.trim().toUpperCase().startsWith(prefix.toUpperCase())){
								System.out.println("method && prefix is match");
								isPass = true;
								searchStr = pymtRef.toUpperCase().replace(prefix.toUpperCase(), "");
							}
						}
					}

				}

				System.out.println("---------------------------------------------------------------------");
				System.out.println("isPass ::"+isPass);
				System.out.println("pymtRef ::"+pymtRef);
				if(!isPass){
					searchStr = "0"	;
				}
				System.out.println("searchStr ::"+searchStr);

				myReq = new MyBillPayReportReq( convertDateFrom, convertDateTo, selectPymtMethod, selectSebRefStatus, contractAccNo.trim(), email.trim(), searchStr.trim(), fpxTxnId, mbbRefNo,  1) ;
				r0 = reportService.findAllBillPayReport(myReq);

				reportService.auditLog("Bill Payment Report", new AuditTrail(user.getId(),0,"","Search Bill Payment Report: "+user.getLoginId(),"","dateFrom:["+dateFrom+"], dateTo:["+dateTo+"], paymentMethod:["+selectPymtMethod+"], contractAccNo:["+contractAccNo+"], email:["+email+"], msisdn:["+msisdn+"], paymentReference:["+pymtRef+"], fpxTransactionId:["+fpxTxnId+"], maybankReferenceNo:["+mbbRefNo+"]", ""));		

			}else{
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Mandatory inputs are required."));
			}


		}catch(Exception ex){
			reportService.auditLog("Bill Payment Report", new AuditTrail(user.getId(),0,"","Search Customer Activities Report: "+user.getLoginId(),"","Fail to process", "dateFrom:["+dateFrom+"], dateTo:["+dateTo+"], paymentMethod:["+selectPymtMethod+"], contractAccNo:["+contractAccNo+"], email:["+email+"], msisdn:["+msisdn+"], paymentReference:["+pymtRef+"], fpxTransactionId:["+fpxTxnId+"], maybankReferenceNo:["+mbbRefNo+"]\nFail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search report."));
			ex.printStackTrace();
		}

		model.addObject("reportList", r0.getPaymentList());
		model.addObject("dateFrom", dateFrom);
		model.addObject("dateTo", dateTo);
		model.addObject("displayPymtMethod", displayPymtMethod);
		model.addObject("paymentMethod", selectPymtMethod);
		model.addObject("contractAccNo", contractAccNo);
		model.addObject("email", email);
		model.addObject("pymtRef", pymtRef);
		model.addObject("fpxTxnId", fpxTxnId);
		model.addObject("mbbRefNo", mbbRefNo);
		model.addObject("totalRecords", r0.getTotalRecords());

		System.out.println("bill payment report size>>"+r0.getTotalRecords());

		model.setViewName("/report/billPaymentReport");
		return model;
	}

	@RequestMapping(value = "/report/getBankResponse",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody List<BankResponse> getBankResponse(@RequestBody BankResponse bankResp)
			throws IOException {
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		System.out.println("[getBankResponse]");
		List<BankResponse> resp = new ArrayList<BankResponse>() ;	
		try {
			System.out.println("request::"+bankResp.toString());
			resp = reportService.findByMerchantidAndResponsecode(bankResp.getMerchantId(), bankResp.getResponseCode());
			for (BankResponse b : resp){
				//System.out.println("merchant id :: "+b.getMerchantId()+", status code ::"+b.getResponseCode()+", response desc :: "+b.getResponseDescription());		
			}

		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return resp;
	}

	/*@RequestMapping(value = "/admin/billPaymentReport",  method = RequestMethod.POST)
	public ModelAndView searchBillPaymentReport(HttpServletRequest req, HttpServletResponse resp, 
			@RequestParam(value="indateFrom", defaultValue = "") String dateFrom,
			@RequestParam(value="indateTo", defaultValue = "") String dateTo,
			@RequestParam(value="selectPymtMethod", defaultValue = "") String selectPymtMethod,
			@RequestParam(value="displayPymtMethod", defaultValue = "") String displayPymtMethod,
			@RequestParam(value="contractAccNo", defaultValue = "") String contractAccNo,
			@RequestParam(value="email", defaultValue = "") String email,
			@RequestParam(value="msisdn", defaultValue = "") String msisdn,
			@RequestParam(value="pymtRef", defaultValue = "") String pymtRef,
			@RequestParam(value="fpxTxnId", defaultValue = "") String fpxTxnId,
			@RequestParam(value="mbbRefNo", defaultValue = "") String mbbRefNo
			)	{

		Enumeration params = req.getParameterNames(); 
		while(params.hasMoreElements()){
			String paramName = (String)params.nextElement();
			//System.out.println(paramName+" : "+req.getParameter(paramName));
		}

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		DateUtil dateUtil = new DateUtil();
		MyBillPayReportResp r0 = new MyBillPayReportResp();
		MyBillPayReportReq myReq = new MyBillPayReportReq();
		try{

			if(!(dateFrom.isEmpty() && dateTo.isEmpty())){

				Date convertDateFrom = new Date();
				Date convertDateTo = new Date();

				convertDateFrom  = (StringUtil.trimToEmpty(dateFrom).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateFrom+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");						
				convertDateTo  = (StringUtil.trimToEmpty(dateTo).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateTo+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						

				List<PaymentGateway> pgList = reportService.findAllPaymentGateway();

				String searchStr = "";
				boolean isPass = false;
				Map listMap = new HashMap();
				for(PaymentGateway pg : pgList){					
					listMap.put(pg.getGatewayId(),pg.getReferencePrefix());
				}
				if(selectPymtMethod.equalsIgnoreCase("ALL")){
					if(pymtRef.isEmpty()){
						System.out.println("ALL && empty");
						isPass = true;
					}else{
						Iterator iterator = listMap.entrySet().iterator();
						while (iterator.hasNext()) {
							System.out.println("[iterator]");
							Map.Entry mapEntry = (Map.Entry) iterator.next();
							String gatewayId =  (String) mapEntry.getKey();
							String prefix = (String) mapEntry.getValue();
							if(pymtRef.trim().toUpperCase().startsWith(prefix.toUpperCase())){
								System.out.println("not empty && match prefix");
								isPass = true;
								searchStr = pymtRef.toUpperCase().replace(prefix.toUpperCase(), "");
							}
						}
					}
				}else{
					if(pymtRef.isEmpty()){
						System.out.println("NOT ALL && is empty");
						isPass = true;
					}else{
						System.out.println("NOT ALL && NOT empty");
						Iterator iterator = listMap.entrySet().iterator();
						while (iterator.hasNext()) {
							System.out.println("[else:iterator]");
							Map.Entry mapEntry = (Map.Entry) iterator.next();
							String gatewayId =  (String) mapEntry.getKey();
							String prefix = (String) mapEntry.getValue();
							if(selectPymtMethod.equalsIgnoreCase(gatewayId) && pymtRef.trim().toUpperCase().startsWith(prefix.toUpperCase())){
								System.out.println("method && prefix is match");
								isPass = true;
								searchStr = pymtRef.toUpperCase().replace(prefix.toUpperCase(), "");
							}
						}
					}

				}

				System.out.println("---------------------------------------------------------------------");
				System.out.println("isPass ::"+isPass);
				System.out.println("pymtRef ::"+pymtRef);
				if(!isPass){
					searchStr = "0"	;
				}
				System.out.println("searchStr ::"+searchStr);

				myReq = new MyBillPayReportReq( convertDateFrom, convertDateTo, selectPymtMethod, contractAccNo.trim(), email.trim(), searchStr.trim(), fpxTxnId, mbbRefNo,  1) ;
				r0 = reportService.findAllBillPayReport(myReq);

				reportService.auditLog("Bill Payment Report", new AuditTrail(user.getId(),0,"","Search Bill Payment Report: "+user.getLoginId(),"","dateFrom:["+dateFrom+"], dateTo:["+dateTo+"], paymentMethod:["+selectPymtMethod+"], contractAccNo:["+contractAccNo+"], email:["+email+"], msisdn:["+msisdn+"], paymentReference:["+pymtRef+"], fpxTransactionId:["+fpxTxnId+"], maybankReferenceNo:["+mbbRefNo+"]", ""));		

			}else{
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Mandatory inputs are required."));
			}


		}catch(Exception ex){
			reportService.auditLog("Bill Payment Report", new AuditTrail(user.getId(),0,"","Search Customer Activities Report: "+user.getLoginId(),"","Fail to process", "dateFrom:["+dateFrom+"], dateTo:["+dateTo+"], paymentMethod:["+selectPymtMethod+"], contractAccNo:["+contractAccNo+"], email:["+email+"], msisdn:["+msisdn+"], paymentReference:["+pymtRef+"], fpxTransactionId:["+fpxTxnId+"], maybankReferenceNo:["+mbbRefNo+"]\nFail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search report."));
			ex.printStackTrace();
		}

		model.addObject("reportList", r0.getPaymentList());
		model.addObject("dateFrom", dateFrom);
		model.addObject("dateTo", dateTo);
		model.addObject("displayPymtMethod", displayPymtMethod);
		model.addObject("paymentMethod", selectPymtMethod);
		model.addObject("contractAccNo", contractAccNo);
		model.addObject("email", email);
		model.addObject("pymtRef", pymtRef);
		model.addObject("fpxTxnId", fpxTxnId);
		model.addObject("mbbRefNo", mbbRefNo);
		model.addObject("totalRecords", r0.getTotalRecords());

		System.out.println("bill payment report size>>"+r0.getTotalRecords());

		model.setViewName("/report/billPaymentReport");
		return model;
	}


	@RequestMapping(value = "/admin/searchBillPaymentTable", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody
	String searchCustHistoryTable(HttpServletRequest req,
			@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength,
			@RequestParam String sEcho,
			@RequestParam String dateFrom,
			@RequestParam String dateTo,
			@RequestParam(value="paymentMethod", defaultValue = "") String paymentMethod,
			@RequestParam(value="contractAccNo", defaultValue = "") String contractAccNo,
			@RequestParam(value="email", defaultValue = "") String email,
			@RequestParam(value="pymtRef", defaultValue = "") String pymtRef,
			@RequestParam(value="fpxTxnId", defaultValue = "") String fpxTxnId,
			@RequestParam(value="mbbRefNo", defaultValue = "") String mbbRefNo,
			@RequestParam(value="totalRecords", defaultValue = "0") String totalRecords

			) {
		System.out.println("[searchBillPaymentTable]");
		Enumeration params = req.getParameterNames(); 
		while(params.hasMoreElements()){
			String paramName = (String)params.nextElement();
			//System.out.println(paramName+" : "+req.getParameter(paramName));
		}

		DataTablesBillPayment<MyPayment> dt = new DataTablesBillPayment<MyPayment>();
		MyBillPayReportResp r0 = new MyBillPayReportResp();
		MyBillPayReportReq myReq = new MyBillPayReportReq();
		try {
			int page = (iDisplayStart + iDisplayLength)/iDisplayLength;
			System.out.println("page >>"+page);


			DateUtil dateUtil = new DateUtil();			
			Date convertDateFrom = new Date();
			Date convertDateTo = new Date();

			convertDateFrom  = (StringUtil.trimToEmpty(dateFrom).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateFrom+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");						
			convertDateTo  = (StringUtil.trimToEmpty(dateTo).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateTo+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						

			List<PaymentGateway> pgList = reportService.findAllPaymentGateway();
			String searchStr = "";
			boolean isPass = false;
			Map listMap = new HashMap();
			for(PaymentGateway pg : pgList){					
				listMap.put(pg.getGatewayId(),pg.getReferencePrefix());
			}
			if(paymentMethod.equalsIgnoreCase("ALL")){
				if(pymtRef.isEmpty()){
					System.out.println("ALL && empty");
					isPass = true;
				}else{
					Iterator iterator = listMap.entrySet().iterator();
					while (iterator.hasNext()) {
						System.out.println("[iterator]");
						Map.Entry mapEntry = (Map.Entry) iterator.next();
						String gatewayId =  (String) mapEntry.getKey();
						String prefix = (String) mapEntry.getValue();
						if(pymtRef.trim().toUpperCase().startsWith(prefix.toUpperCase())){
							System.out.println("not empty && match prefix");
							isPass = true;
							searchStr = pymtRef.toUpperCase().replace(prefix.toUpperCase(), "");
						}
					}
				}
			}else{
				if(pymtRef.isEmpty()){
					System.out.println("NOT ALL && is empty");
					isPass = true;
				}else{
					System.out.println("NOT ALL && NOT empty");
					Iterator iterator = listMap.entrySet().iterator();
					while (iterator.hasNext()) {
						System.out.println("[else:iterator]");
						Map.Entry mapEntry = (Map.Entry) iterator.next();
						String gatewayId =  (String) mapEntry.getKey();
						String prefix = (String) mapEntry.getValue();
						if(paymentMethod.equalsIgnoreCase(gatewayId) && pymtRef.trim().toUpperCase().startsWith(prefix.toUpperCase())){
							System.out.println("method && prefix is match");
							isPass = true;
							searchStr = pymtRef.toUpperCase().replace(prefix.toUpperCase(), "");
						}
					}
				}

			}

			System.out.println("---------------------------------------------------------------------");
			System.out.println("isPass ::"+isPass);
			System.out.println("pymtRef ::"+pymtRef);
			if(!isPass){
				searchStr = "0"	;
			}
			System.out.println("searchStr ::"+searchStr);

			myReq = new MyBillPayReportReq( convertDateFrom, convertDateTo, paymentMethod, contractAccNo.trim(), email.trim(), searchStr.trim(), fpxTxnId, mbbRefNo,  page) ;
			r0 = reportService.findAllBillPayReport(myReq);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dt.setAaData(r0.getPaymentList());  // this is the dataset reponse to client
		dt.setiTotalDisplayRecords(Integer.parseInt(totalRecords));  // // the total data in db for datatables to calculate page no. and position

		dt.setiTotalRecords(Integer.parseInt(totalRecords));   // the total data in db for datatables to calculate page no.
		dt.setsEcho(sEcho);
		dt.setiDisplayLength(1);
		//System.out.println("total records>"+totalRecords);
		//System.out.println("display records>"+dt.getiTotalDisplayRecords());
		return toJson(dt);
	}


	private String toJson(DataTablesBillPayment dt){
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(dt);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}*/


}