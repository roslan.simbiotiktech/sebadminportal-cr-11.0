package com.seb.admin.portal.spg.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OMbb {
	@JsonProperty("id")
	private String gatewayId;
	
	@JsonProperty("merchant_id")
	private String merchant_id;
	
	@JsonProperty("submit_url")
	private String submit_url;
	
	@JsonProperty("query_url")
	private String query_url;
	
	@JsonProperty("hash_key")
	private String hash_key;
	
	@JsonProperty("amex_merchant_id")
	private String amex_merchant_id;
	
	@JsonProperty("amex_hash_key")
	private String amex_hash_key;
	
	@JsonProperty("minimun_payment_amount")
	private Double minimun_payment_amount;
	
	@JsonProperty("maximum_payment_amount")
	private Double maximum_payment_amount;
	
	@JsonProperty("enabled")
	private boolean enabled;

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getSubmit_url() {
		return submit_url;
	}

	public void setSubmit_url(String submit_url) {
		this.submit_url = submit_url;
	}

	public String getQuery_url() {
		return query_url;
	}

	public void setQuery_url(String query_url) {
		this.query_url = query_url;
	}

	public String getHash_key() {
		return hash_key;
	}

	public void setHash_key(String hash_key) {
		this.hash_key = hash_key;
	}

	public String getAmex_merchant_id() {
		return amex_merchant_id;
	}

	public void setAmex_merchant_id(String amex_merchant_id) {
		this.amex_merchant_id = amex_merchant_id;
	}

	public String getAmex_hash_key() {
		return amex_hash_key;
	}

	public void setAmex_hash_key(String amex_hash_key) {
		this.amex_hash_key = amex_hash_key;
	}

	public Double getMinimun_payment_amount() {
		return minimun_payment_amount;
	}

	public void setMinimun_payment_amount(Double minimun_payment_amount) {
		this.minimun_payment_amount = minimun_payment_amount;
	}

	public Double getMaximum_payment_amount() {
		return maximum_payment_amount;
	}

	public void setMaximum_payment_amount(Double maximum_payment_amount) {
		this.maximum_payment_amount = maximum_payment_amount;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "gatewayId:[" + gatewayId + "], merchant_id:[" + merchant_id + "], submit_url:[" + submit_url
				+ "], query_url:[" + query_url + "], hash_key:[" + hash_key + "], amex_merchant_id:[" + amex_merchant_id
				+ "], amex_hash_key:[" + amex_hash_key + "], minimun_payment_amount:[" + minimun_payment_amount
				+ "], maximum_payment_amount:[" + maximum_payment_amount + "], enabled:[" + enabled + "]";
	}
	

	
}
