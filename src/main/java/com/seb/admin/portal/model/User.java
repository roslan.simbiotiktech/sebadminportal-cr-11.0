package com.seb.admin.portal.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.util.StringUtil;


/**
 * The persistent class for the REPORT database table.
 * 
 */
@Entity
@Table(name="USER", schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="User.findAll", query="SELECT u FROM User u"),
	@NamedQuery(name="User.findTotalByStatus", query="SELECT  count(u.userId) as total FROM User u WHERE UPPER(u.status) =:status AND u.createdDatetime>=:dateFrom AND u.createdDatetime<=:dateTo"),
	//@NamedQuery(name="User.findTotalBffyStatus", query="SELECT SUM(CASE WHEN u.status =:status THEN 1 ELSE 0 END) AS count1, SUM(CASE WHEN u.status =:status THEN 1 ELSE 0 END) AS count2, SUM(CASE WHEN u.status =:status THEN 1 ELSE 0 END) AS count3 FROM User u")
})
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="USER_ID", unique=true, nullable=false, length=50)
	private String userId;

	@Column(name="CREATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="CURRENT_LOGIN_AT")
	private Timestamp currentLoginAt;

	@Column(name="EXISTING_EMAIL", length=50)
	private String existingEmail;

	@Column(name="FLAG_PASSWORD_EXPIRED", nullable=false)
	private int flagPasswordExpired;

	@Column(name="LAST_LOGIN_AT")
	private Timestamp lastLoginAt;

	@Column(name="PREFERRED_COMMUNICATION_METHOD", length=10)
	private String preferredCommunicationMethod;
	
	//@Column(length=500)
	//private String remark;

	@Lob
	@Column(name="PROFILE_PHOTO")
	private byte[] profilePhoto;

	@Column(name="PROFILE_PHOTO_TYPE", length=20)
	private String profilePhotoType;

	@Column(name="STATUS", nullable=false, length=20)
	private String status;

	public User() {
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public Timestamp getCurrentLoginAt() {
		return this.currentLoginAt;
	}

	public void setCurrentLoginAt(Timestamp currentLoginAt) {
		this.currentLoginAt = currentLoginAt;
	}

	public String getExistingEmail() {
		return this.existingEmail;
	}

	public void setExistingEmail(String existingEmail) {
		this.existingEmail = existingEmail;
	}

	public int getFlagPasswordExpired() {
		return this.flagPasswordExpired;
	}

	public void setFlagPasswordExpired(int flagPasswordExpired) {
		this.flagPasswordExpired = flagPasswordExpired;
	}

	public Timestamp getLastLoginAt() {
		return this.lastLoginAt;
	}

	public void setLastLoginAt(Timestamp lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}

	public String getPreferredCommunicationMethod() {
		return this.preferredCommunicationMethod;
	}

	public void setPreferredCommunicationMethod(String preferredCommunicationMethod) {
		this.preferredCommunicationMethod = preferredCommunicationMethod;
	}

	/*public String getRemark() {
		return StringUtil.trimToEmpty(remark);
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}*/

	public byte[] getProfilePhoto() {
		return this.profilePhoto;
	}

	public void setProfilePhoto(byte[] profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	public String getProfilePhotoType() {
		return this.profilePhotoType;
	}

	public void setProfilePhotoType(String profilePhotoType) {
		this.profilePhotoType = profilePhotoType;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}