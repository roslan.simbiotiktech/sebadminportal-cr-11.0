package com.seb.admin.portal.spg.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OMerchantBase;
import com.seb.admin.portal.spg.entity.ResponseStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MerchantResponse 
{
	@JsonProperty("status")
	private ResponseStatus responseStatus;
	@JsonProperty("merchant")
	private OMerchantBase merchant;


	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}
	public OMerchantBase getMerchant() {
		return merchant;
	}
	public void setMerchant(OMerchantBase merchant) {
		this.merchant = merchant;
	}
	

}