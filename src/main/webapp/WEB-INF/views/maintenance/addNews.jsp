<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>

<style type="text/css">
.spreview {
	display: none;
	max-width: 140px;
	max-height: 107px;
	border: 1px solid #d5d5d5;
	margin-top: 10px;
}

.spreview img {
	max-width: 100%;
	height: auto;
}

.lpreview {
	display: none;
	max-width: 130px;
	max-height: 195px;
	border: 1px solid #d5d5d5;
	margin-top: 10px;
}

.lpreview img {
	max-width: 100%;
	height: auto;
}
</style>
</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->

		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Maintenance &gt; News &gt; Add</div>
						<div class="panel-body">
							<form action="${pageContext.request.contextPath}/admin/addNews.do?${_csrf.parameterName}=${_csrf.token}" class="form-horizontal"
								id="addNewsForm" method="POST" name="addNewsForm" enctype="multipart/form-data">
								<div id="errorMsg" style="display: none;"></div>${successMsg}${errorMsg}
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Title <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="title" maxlength="100" name="title" placeholder="Title" type="text">
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Image (small) :
									</p>

									<div class="col-lg-3">
										<input class="file" id="simg" name="simg" type="file">
										<p class="smallnotes">
											<small><em>Format:640px X 490px(JPEG/JPG/PNG), Max 300KB</em></small>
										</p>
										<!-- <div id="errorMsg1"></div> -->
										<div id="spreview" class="spreview"></div>
										<!-- <img id="spreview" src="" width="140" height="107" style="display: none;"/> -->

									</div>

								</div>

								<div class="form-group" style="display: none;">
									<p class="col-lg-5 control-label">
										Image (large) <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<input class="file" id="limg" name="limg" type="file">
										<p class="smallnotes">
											<small><em>Format:640px X 960px(JPEG/JPG/PNG), Max 300KB</em></small>
										</p>
										<div id="lpreview" class="lpreview"></div>
										<!-- <div id="errorMsg2"></div> -->
										<!--  <img id="lpreview" src="" width="130" height="195" style="display: none;"/> -->
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Description <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-7" style="float: right;">
										<textarea class="form-control" cols="23" id="desc" maxlength="2000" name="desc" rows="10"
											onkeydown="javascript:return myCustomOnChangeHandler();">
</textarea>

									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Start Date <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<div class='input-group date' id='startDate'>
											<input id="instartDate" class="form-control datepicker" type='text' name="instartDate" onkeydown="return false;"> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										End Date <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<div class='input-group date' id='endDate'>
											<input class="form-control datepicker" type='text' id="inendDate" name="inendDate" onkeydown="return false;"> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Publish Date <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<div class='input-group date' id='publishDate'>
											<input class="form-control datepicker" type='text' id="inpublishDate" name="inpublishDate" onkeydown="return false;"> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Push Notification <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<label class="radio-inline"><input id="pushNotif1" name="pushNotif" type="radio" value="1">On</label> <label class="radio-inline"><input
											id="pushNotif2" name="pushNotif" type="radio" value="0" checked>Off</label>
									</div>
								</div>

								<div id="pushDateTimeDiv" class="form-group" style="display: none;">
									<p class="col-lg-5 control-label">
										Push DateTime <span style="color: red;">*</span> :
									</p>
									<div class="form-group">
										<div class="col-lg-3">
											<div class='input-group date' id='pushDateTime'>
												<input id="inpushDateTime" name="inpushDateTime" class="form-control datepicker" type='text' readonly="readonly"> <span
													class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
											</div>
										</div>
									</div>

									<div class="form-group">
										<p class="col-lg-5 control-label">
											Message <span style="color: red;">*</span> :
										</p>
										<div class="col-lg-3">

											<textarea class="form-control" cols="23" id="text" maxlength="120" name="text" rows="10"></textarea>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" type="submit" data-toggle="modal" data-target="#modal" id="submitBtn">Add</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>

								<div class="modal fade" id="modal" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button class="close" data-dismiss="modal" type="button">
													<span>&times;</span><span class="sr-only">Close</span>
												</button>
												<h2 class="modal-title">Confirmation</h2>
											</div>
											<div class="modal-body" id="add">Are you sure you want to add news?</div>
											<div class="modal-footer">
												<button class="btn btn-primary" id="add" type="submit">Add</button>
												<button class="btn btn-warning" data-dismiss="modal" type="button">Close</button>
											</div>
										</div>
									</div>
								</div>
							</form>



						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
		function resetForm() {
			$('#').find('.has-error').removeClass("has-error");
			$('#addNewsForm').find('.has-success').removeClass("has-success");
			$('#addNewsForm').find('.form-control-feedback').removeClass('glyphicon-remove');
			$('#addNewsForm').find('.form-control-feedback').removeClass('glyphicon-ok');
			$('.form-group').find('small.help-block').hide();
			//Removing the error elements from the from-group
			$('.form-group').removeClass('has-error has-feedback');
			$('.form-group').find('i.form-control-feedback').hide();

			$('#addNewsForm').formValidation('resetForm', true);
			setTimeout(function() {
				$('#instartDate').val(moment(new Date()).format('DD/MM/YYYY'));
				$('#inendDate').val(moment(moment().add(1, 'd').toDate()).format('DD/MM/YYYY'));
				$('#inpublishDate').val(moment(new Date()).format('DD/MM/YYYY'));
				$('#inpushDateTime').val(moment(new Date()).format('DD/MM/YYYY hh:mm A'));
			}, 50);

			$('#spreview').html('').hide();
			$('#lpreview').html('').hide();

			return false;
		}

		function sreadFile(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function(e) {
					$('#spreview').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
				document.getElementById("spreview").style.display = "block";
			}
		}

		function lreadFile(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function(e) {
					$('#lpreview').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
				document.getElementById("spreview").style.display = "block";
			}
		}

		$(document).ready(function() {

			tinymce.init({
				selector : "#desc",				
				theme : "modern",
				mode : "textareas",
				menubar : false,
				statusbar : true,
				name:"desc",
				plugins : "charactercount",				
				setup : function(editor) {
					var maxlength = parseInt($('#' + (editor.id)).attr("maxlength"));
				
					editor.on('keyup', function(e) {
						// Revalidate the hobbies field
						$('#addNewsForm').formValidation('revalidateField', 'desc');
					});
					 editor.on('keydown', function(e) {
					    var charcnt = tinyMCE.editors[this.getParam('name')].plugins["charactercount"].getCount() >= maxlength;
					    if (charcnt && e.keyCode != 8 && e.keyCode != 46) {
					        return tinymce.dom.Event.cancel(e);
					    }
					});
					editor.on('change', function(e) {
					    var charcnt = tinyMCE.editors[this.getParam('name')].plugins["charactercount"].getCount() >= maxlength;
					   
					    if (charcnt) {
					        return tinymce.dom.Event.cancel(e);
					    }	            
					 });
				}, //setup
				init_instance_callback : function(editor) {
					$('.mce-tinymce').show('fast');
					$(editor.getContainer()).find(".mce-path").css("display", "none");
				}
			});

			$(function() {
				var today = new Date();
				var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
				$('#startDate').datetimepicker({
					format : 'DD/MM/YYYY',
					defaultDate : new Date(),
					ignoreReadonly : true
				}).on("dp.change", function(e) {
					$('#addNewsForm').formValidation('revalidateField', 'instartDate');
					$('#addNewsForm').formValidation('revalidateField', 'inendDate');
				});
				$('#endDate').datetimepicker({
					format : 'DD/MM/YYYY',
					//minDate: moment().add(1, 'd').toDate()
					defaultDate : tomorrow,
					ignoreReadonly : true

				}).on("dp.change", function(e) {
					$('#addNewsForm').formValidation('revalidateField', 'instartDate');
					$('#addNewsForm').formValidation('revalidateField', 'inendDate');
				});
				$('#publishDate').datetimepicker({
					format : 'DD/MM/YYYY',
					defaultDate : new Date(),
					ignoreReadonly : true
				}).on("dp.change", function(e) {
					$('#addNewsForm').formValidation('revalidateField', 'inpublishDate');
				});
				$('#pushDateTime').datetimepicker({
					format : 'DD/MM/YYYY hh:mm A',
					defaultDate : new Date(),
					ignoreReadonly : true
				}).on("dp.change", function(e) {
					$('#addNewsForm').formValidation('revalidateField', 'inpushDateTime');
				});
			});
			$('#pushNotif1').on("click", function() {
				$('#pushDateTimeDiv').show();
			});
			$('#pushNotif2').on("click", function() {
				$('#pushDateTimeDiv').hide();
			});
			$('#pushNotif2').on("change", function() {
				$('#pushDateTimeDiv').hide();
			});
			/*  var _URL = window.URL || window.webkitURL;
			 $("#simg").change(function(e) {
			     var image, file;
			     $('#errorMsg1').html('');
			     if ((file = this.files[0])) {
			         image = new Image();
			         image.onload = function() {
			             if (this.width != 640 || this.height != 490) {
			                 $('#errorMsg1').html('<span style="font-size: small; color: #B64243">Image size must be 640px X 490px</span>').show();
			             }
			         };
			         image.src = _URL.createObjectURL(file);
			     }
			 });
			 $("#limg").change(function(e) {
			     $('#errorMsg2').html('');
			     var image, file;
			     if ((file = this.files[0])) {
			         image = new Image();
			         image.onload = function() {
			             if (this.width != 640 || this.height != 960) {
			                 $('#errorMsg2').html('<span style="font-size: small; color: #B64243">Image size must be 640px X 960px</span>');
			             }
			         };
			         image.src = _URL.createObjectURL(file);
			     }
			 });
			 */
			

			$("#submitBtn").on('click', function(e) {
				var text = tinyMCE.activeEditor.getContent({
				});
				/* var text = tinyMCE.activeEditor.getContent({
					format : 'text'
				}); */
				console.log(text);
				e.preventDefault();
			});

			$('#addNewsForm').formValidation({
				framework : 'bootstrap',
				excluded : [ ':disabled' ],
				icon : {
					invalid : 'glyphicon glyphicon-remove',
				},
				fields : {
					title : {
						validators : {
							notEmpty : {
								message : 'Title is required'
							},
							stringLength : {
								min : 1,
								max : 100,
								message : 'Title not more than 100 characters'
							}
						}
					},
					text : {
						validators : {
							callback : {
								callback : function(value, validator, $field) {
									//var radio = $("input[type='radio'][name='pushNotif']:checked").val();
								   var radio = $('#addNewsForm').find('[name="pushNotif"]:checked').val();
									//console.log("text:radio>"+radio);
									//console.log("text:value>"+$.trim(value));
									if (radio == 1 && $.trim(value) == '') {
										return {
											valid : false,
											message : 'Message is required'
										}
									} else {
										//console.log("else");
										return true;
									}
								}
							}
						}
					},
					simg : {
						validators : {
							/* notEmpty : {
								message : 'Small image is required'
							}, */
							file : {
								extension : 'jpg,jpeg,png',
								type : 'image/jpg,image/jpeg,image/png',
								maxSize : 307200, // 2048 * 1024
								message : "Image must less than 300KB"
							},
							promise : {
								promise : function(value, validator, $field) {
									var dfd = new $.Deferred(), files = $field.get(0).files;

									if (!files.length || typeof FileReader === 'undefined') {
										dfd.resolve({
											valid : true
										});
										return dfd.promise();
									}

									var img = new Image();
									img.onload = function() {
										var w = this.width, h = this.height;
										dfd.resolve({
											valid : (w <= 640 && h <= 490),
											message : 'Image size must not more than 640px X 490px',
											source : img.src,
											width : w,
											height : h
										});
									};
									img.onerror = function() {
										dfd.reject({
											message : 'Please select an image'
										});
									};

									var reader = new FileReader();
									reader.readAsDataURL(files[0]);
									reader.onloadend = function(e) {
										img.src = e.target.result;
									};

									return dfd.promise();
								}
							}
						}
					},
					/* limg: {
					    validators: {
					        notEmpty: {
					            message: 'Large image is required'
					        },
					        file: {
					          extension: 'jpg,jpeg,png',
					          type: 'image/jpg,image/jpeg,image/png',
					          maxSize: 307200, // 2048 * 1024
					          message: "Image must less than 300KB"
					      	},
					        promise: {
					          promise: function(value, validator, $field) {
					          	console.log("fgfg");
					              var dfd   = new $.Deferred(),
					                  files = $field.get(0).files;

					              if (!files.length || typeof FileReader === 'undefined') {
					                  dfd.resolve({ valid: true });
					                  return dfd.promise();
					              }

					              var img = new Image();
					              img.onload = function() {
					                  var w = this.width,
					                      h = this.height;
						console.log(w);
						console.log(h);
					                  dfd.resolve({
					                      valid: (w <= 640 && h <= 960),
					                      message: 'Image size must not more than 640px X 960px',
					                      source: img.src,  
					                      width: w,
					                      height: h
					                  });
					              };
					              img.onerror = function() {
					                  dfd.reject({
					                      message: 'Please select an image'
					                  });
					              };

					              var reader = new FileReader();
					              reader.readAsDataURL(files[0]);
					              reader.onloadend = function(e) {
					                  img.src = e.target.result;
					              };

					              return dfd.promise();
					          }
					        }
					    }
					}, */
					desc : {
						validators : {
							callback : {
								callback : function(value, validator, $field) {
									var text = tinyMCE.activeEditor.getContent({
										format : 'text'
									});
									
									if ($.trim(text).length ==0) {
										return {
											valid : false,
											message : 'Description is required.'
										}
									}									
									else if (text.length > 2000) {
										return {
											valid : false,
											message : 'Description must be less than 2000 characters.'
										}
									} else {
										 return ($.trim(text).length>0 && text.length <= 2000);
									}
								}
							}							
						}
					},
					instartDate : {
						validators : {
							notEmpty : {
								message : 'Start Date is required'
							},
							date : {
								format : 'DD/MM/YYYY',
								max : 'inendDate',
								message : 'The start date is not a valid'
							}

						}
					},
					inendDate : {
						validators : {
							notEmpty : {
								message : 'End Date is required'
							},
							date : {
								format : 'DD/MM/YYYY',
								min : 'instartDate',
								message : 'The end date is not a valid'
							}

						}
					},
					inpublishDate : {
						validators : {
							notEmpty : {
								message : 'Publish Date is required'
							},
							date : {
								format : 'DD/MM/YYYY',
								min : 'instartDate',
								max : 'inendDate',
								message : 'The publish date is not a valid'
							}

						}
					},
					inpushDateTime : {
						validators : {
							callback : {
								callback : function(value, validator, $field) {
									var radio = $("input[type='radio'][name='pushNotif']:checked").val();
									if (value == '') {
										$('#inpushDateTime').val(moment(new Date()).format('DD/MM/YYYY hh:mm A'));
									}
									var mypush = $('#inpushDateTime').val().substring(0, 10);
									if (radio == '1' && (moment($('#inendDate').val(), "DD/MM/YYYY").isBefore(moment(mypush, "DD/MM/YYYY")))) {
										return {
											valid : false,
											message : 'Push notification date time must  ealier than end date'
										}
									} else {
										return true;
									}
								}
							},
							format : 'DD/MM/YYYY hh:mm A'
						}
					}
				}
			}).on('change', '[name="pushNotif"]', function(e) {
        $('#addNewsForm').formValidation('revalidateField', 'text');
      }).on('err.validator.fv', function(e, data) {
				if (data.field === 'simg' && data.validator === 'promise') {
					$('#spreview').html('').hide();
				}
				if (data.field === 'limg' && data.validator === 'promise') {
					$('#lpreview').html('').hide();
				}
			}).on('success.validator.fv', function(e, data) {
				data.fv.disableSubmitButtons(false);
				if (data.field === 'simg' && data.validator === 'promise' && data.result.source) {
					$('#spreview').html('').append($('<img/>').attr('src', data.result.source)).show();
				}
				if (data.field === 'limg' && data.validator === 'promise' && data.result.source) {
					$('#lpreview').html('').append($('<img/>').attr('src', data.result.source)).show();
				}
			});
		});
	</script>
</body>
</html>