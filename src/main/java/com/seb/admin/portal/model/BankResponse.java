package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;


/**
 * The persistent class for the BANK_RESPONSE database table.
 * 
 */
@Entity
@Table(name="BANK_RESPONSE" , schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="BankResponse.findAll", query="SELECT b FROM BankResponse b"),
	@NamedQuery(name="BankResponse.findByMerchantidAndResponsecode", query="SELECT b FROM BankResponse b WHERE b.merchantId = :merchantId AND b.responseCode = :responseCode")
	})

public class BankResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID")
	private int id;

	@Column(name="CREATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="DELETED_FLAG")
	private int deletedFlag;

	@Column(name="MERCHANT_ID")
	private String merchantId;

	@Column(name="RESPONSE_CODE")
	private String responseCode;

	@Column(name="RESPONSE_DESCRIPTION")
	private String responseDescription;

	@Column(name="UPDATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp updatedDatetime;

	public BankResponse() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getMerchantId() {
		return this.merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescription() {
		return this.responseDescription;
	}

	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	@Override
	public String toString() {
		return "BankResponse [id=" + id + ", createdDatetime=" + createdDatetime + ", deletedFlag=" + deletedFlag
				+ ", merchantId=" + merchantId + ", responseCode=" + responseCode + ", responseDescription="
				+ responseDescription + ", updatedDatetime=" + updatedDatetime + "]";
	}
	
	

}