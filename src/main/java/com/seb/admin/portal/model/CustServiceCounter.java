package com.seb.admin.portal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.seb.admin.portal.constant.ApplicationConstant;


/**
 * The persistent class for the CUST_SERVICE_COUNTER database table.
 * 
 */
@Entity
@Table(name="CUST_SERVICE_COUNTER", schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="CustServiceCounter.findAll", query="SELECT c FROM CustServiceCounter c where c.deletedFlag=0"),
	//@NamedQuery(name="CustServiceCounter.findCounterByStatus", query ="SELECT c FROM CustServiceCounter c LEFT JOIN c.custServiceCounterEditeds e WHERE e.counterId IS NULL AND UPPER(c.status) =:status AND c.deletedFlag=0" ),	
	@NamedQuery(name="CustServiceCounter.findCounterByStatus", query ="SELECT c FROM CustServiceCounter c WHERE UPPER(c.status) =:status AND UPPER(c.category) =:category AND c.deletedFlag=0" ),	
	@NamedQuery(name="CustServiceCounter.findMainLocatorByStatus", query ="SELECT c FROM CustServiceCounter c WHERE (UPPER(c.status) =:status1 OR UPPER(c.status) =:status2) AND c.deletedFlag=0" ),	
	@NamedQuery(name="CustServiceCounter.findAllCounterByLocId", query ="SELECT c FROM CustServiceCounter c WHERE UPPER(c.status) = 'A' AND c.custServiceLocationId =:locId AND c.deletedFlag=0" )

})
public class CustServiceCounter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(name="ADDRESS_LINE1", length=255)
	private String addressLine1;

	@Column(name="ADDRESS_LINE2", length=255)
	private String addressLine2;

	@Column(name="ADDRESS_LINE3", length=255)
	private String addressLine3;

	@Column(name="ADMIN_USER_ID")
	private int adminUserId;

	@Column(length=25)
	private String category;

	@Column(name="APPROVER_ID")
	private int approverId;

	@Column(length=255)
	private String city;

	@Column(name="CREATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="CUST_SERVICE_LOCATION_ID")
	private int custServiceLocationId;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(nullable=false)
	private double latitude;

	@Column(nullable=false)
	private double longitude;

	@Column(name="OPENING_HOUR", length=255)
	private String openingHour;

	@Column(length=255)
	private String postcode;

	@Column(name="REJECT_REASON", length=600)
	private String rejectReason;

	@Column(length=600)
	private String remark;

	@Column(name="STATUS", nullable=false, length=25)
	private String status ="P";

	@Column(name="UPDATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp updatedDatetime;

	@JsonIgnore
	@JoinColumn(name = "CUST_SERVICE_LOCATION_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private CustServiceLocation custServLoc;

	//bi-directional many-to-one association to CustServiceCounterEdited
	@JsonIgnore
	@OneToMany(mappedBy="custServiceCounter")
	private List<CustServiceCounterEdited> custServiceCounterEditeds;


	public CustServiceCounter() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddressLine1() {
		return this.addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return this.addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return this.addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public int getAdminUserId() {
		return this.adminUserId;
	}

	public void setAdminUserId(int adminUserId) {
		this.adminUserId = adminUserId;
	}	

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getApproverId() {
		return this.approverId;
	}

	public void setApproverId(int approverId) {
		this.approverId = approverId;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getCustServiceLocationId() {
		return this.custServiceLocationId;
	}

	public void setCustServiceLocationId(int custServiceLocationId) {
		this.custServiceLocationId = custServiceLocationId;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getOpeningHour() {
		return this.openingHour;
	}

	public void setOpeningHour(String openingHour) {
		this.openingHour = openingHour;
	}

	public String getPostcode() {
		return this.postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getRejectReason() {
		return this.rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}


	public CustServiceLocation getCustServLoc() {
		return custServLoc;
	}

	public void setCustServLoc(CustServiceLocation custServLoc) {
		this.custServLoc = custServLoc;
	}


	public List<CustServiceCounterEdited> getCustServiceCounterEditeds() {
		return this.custServiceCounterEditeds;
	}

	public void setCustServiceCounterEditeds(List<CustServiceCounterEdited> custServiceCounterEditeds) {
		this.custServiceCounterEditeds = custServiceCounterEditeds;
	}

	public CustServiceCounterEdited addCustServiceCounterEdited(CustServiceCounterEdited custServiceCounterEdited) {
		getCustServiceCounterEditeds().add(custServiceCounterEdited);
		custServiceCounterEdited.setCustServiceCounter(this);

		return custServiceCounterEdited;
	}

	public CustServiceCounterEdited removeCustServiceCounterEdited(CustServiceCounterEdited custServiceCounterEdited) {
		getCustServiceCounterEditeds().remove(custServiceCounterEdited);
		custServiceCounterEdited.setCustServiceCounter(null);

		return custServiceCounterEdited;
	}

}