package com.seb.admin.portal.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.manager.PropertiesManager;
import com.seb.admin.portal.model.AccessRight;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.model.MyAccessRight;
import com.seb.admin.portal.model.MyModule;
import com.seb.admin.portal.model.PortalModule;
import com.seb.admin.portal.service.LoginService;
import com.seb.admin.portal.service.ReportService;
import com.seb.admin.portal.service.UserMgmtService;
import com.seb.admin.portal.util.LDAPUtil;
import com.seb.admin.portal.util.StringUtil;

@Controller
public class UserMgmtController {

	private static final Logger log = Logger.getLogger(UserMgmtController.class);

	@Autowired
	private LoginService loginService;
	@Autowired
	private ReportService reportService;
	@Autowired
	private UserMgmtService userMgmtService;

	PropertiesManager property = new PropertiesManager();

	@RequestMapping(value = "/admin/addUser",  method = RequestMethod.GET)
	public ModelAndView doAddUser()	{
		ModelAndView model = new ModelAndView();

		model.setViewName("/userMgmt/addUser");
		return model;
	}

	@RequestMapping(value = "/admin/addUser",  method = RequestMethod.POST)
	public ModelAndView saveUser(@RequestParam(value="name", defaultValue = "") String name,
			@RequestParam(value="loginId", defaultValue = "") String loginId,
			//@RequestParam(value="department", defaultValue = "") String department,
			@RequestParam(value="selectRole", defaultValue = "") String selectRole,
			@RequestParam(value="roleName", defaultValue = "") String roleName,
			@RequestParam(value="moduleList", defaultValue = "") String moduleList)	{

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		/*System.out.println("loginId :"+loginId);
		//System.out.println("department :"+department);
		System.out.println("selectRole :"+selectRole);
		System.out.println("roleName :"+roleName);
		System.out.println("moduleList :"+moduleList);*/
		System.out.println("ldap type="+property.getProperty(ApplicationConstant.LDAP_TYPE));
		try {
			LDAPUtil ldap = new LDAPUtil();
			List usersList = new ArrayList();
			usersList = property.getProperty(ApplicationConstant.LDAP_TYPE).equalsIgnoreCase("customer") ? ldap.searchByEmail(loginId) : ldap.searchBySamacctName(loginId, "");
			if(usersList!=null && usersList.size()>0){
				AdminUser admin = loginService.checkExistUser(loginId);			
				if(admin == null || admin.getLoginId().isEmpty()){
					List<String> mainMod = new ArrayList<String>();
					String [] arrMod = moduleList.split("\\,");
					System.out.println("arrMod.length>>"+arrMod.length);
					if(!moduleList.equalsIgnoreCase("") && arrMod.length>=1){
						for(int i=0;i<arrMod.length;i++){
							String mod = arrMod[i].toString();
							//System.out.println("mod - "+mod);
							if((mod.trim().split("\\.")).length == 3){//level 3
								mainMod.add( mod.substring(0,2));
								mainMod.add( mod.substring(0,5));
								mainMod.add( mod);
							}else if((mod.trim().split("\\.")).length == 2){//level 2
								mainMod.add( mod.substring(0,2));							
								mainMod.add( mod);
							}else{
								mainMod.add( mod);
							}						
						}
						mainMod = mainMod.stream().distinct().collect(Collectors.toList());
						/*for(String m : mainMod){
							System.out.println("module_codes="+m.toString());
						}*/				
						String department= "";
						String email ="";

						if(property.getProperty(ApplicationConstant.LDAP_TYPE).equalsIgnoreCase("customer")){
							department = "Customer";
							email = loginId;
						}else{
							department = ldap.getUserBasicAttributes("department",loginId, ldap.getLdapContext());
							email = ldap.getUserBasicAttributes("mail",loginId, ldap.getLdapContext());
						}

						AdminUser newAdmin = new AdminUser();
						newAdmin.setLoginId(loginId);
						newAdmin.setPassword("");
						newAdmin.setName(email);						
						newAdmin.setRole(selectRole);
						newAdmin.setDepartment(department);

						if(StringUtil.trimToEmpty(department).isEmpty()){
							reportService.auditLog("User Management", new AuditTrail(user.getId(),0,"","Add New User: "+user.getLoginId(),"","Fail to process", "Fail-Message : cannot connect LDAP"));					
							model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to add new user."));
						}else{
							userMgmtService.saveNewAdminUser(newAdmin, mainMod);
							List<PortalModule> portal = userMgmtService.findSelectedModulesByCode(mainMod);

							StringBuilder sb = new StringBuilder();
							for(PortalModule p : portal){
								sb.append("id:["+p.getId()+"], module:["+p.getDescription()+"]\n");
							}
							model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully added new user."));
							reportService.auditLog("User Management", new AuditTrail(user.getId(),0,"","Add User: "+user.getLoginId(),"",sb.toString(),""));
						}
					}else{
						model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Module is required."));
					}



				}else{
					model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Login ID already exist in database."));

				}
			}else{
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "AD login not exist. Please input a valid AD login."));
			}

		} catch (Exception ex) {			
			reportService.auditLog("User Management", new AuditTrail(user.getId(),0,"","Add New User: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to add new user."));
			ex.printStackTrace();

		}
		model.setViewName("/userMgmt/addUser");
		return model;
	}

	@RequestMapping(value = "/admin/editUser",  method = RequestMethod.GET)
	public ModelAndView doEditUser() throws IOException	{		
		ModelAndView model = new ModelAndView();
		List<AdminUser> loginIdList = new ArrayList<AdminUser> ();

		try {
			loginIdList = loginService.findAllUser();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addObject("loginIdList", loginIdList);
		model.setViewName("/userMgmt/editUser");

		return model;
	}

	@RequestMapping(value = "/admin/editUser",  method = RequestMethod.POST)
	public ModelAndView searchUser(@RequestParam(value="loginId", defaultValue = "") String loginId,
			@RequestParam(value="selectLoginId", defaultValue = "") String selectLoginId) throws IOException{
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();	

		ModelAndView model = new ModelAndView();
		List<AdminUser> loginIdList = new ArrayList<AdminUser> ();
		List<AccessRight> accessList =  new ArrayList<AccessRight>();
		AdminUser selectedUser = new AdminUser();

		try {
			loginIdList = loginService.findAllUser();
			accessList = userMgmtService.findUserManagement(Integer.parseInt(selectLoginId));
			selectedUser = loginService.getSelectedUser(loginId);

			reportService.auditLog("User Management", new AuditTrail(user.getId(),0,"","Search Update User: "+user.getLoginId(),"","loginId:["+loginId+"]",""));

		} catch (Exception ex) {
			log.error(ex.getMessage());
			reportService.auditLog("User Management", new AuditTrail(user.getId(),0,"","Search Update User: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
		}
		
		model.addObject("selectedUser", selectedUser);
		model.addObject("loginIdList", loginIdList);
		model.addObject("accessList", accessList);
		model.addObject("loginId", loginId);
		model.setViewName("/userMgmt/editUser");

		return model;
	}

	@RequestMapping(value = "/admin/updateUser",  method = RequestMethod.POST )
	public @ResponseBody Map<String,String> updateUser(@RequestBody List<Map<String, String>> client)
			throws IOException {

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		String error  = "";
		Map<String,String> map = new HashMap<String,String>();

		try {
			AdminUser admin = new AdminUser();
			for (Map<String, String> formInput : client) {

				if(formInput!=null && !formInput.isEmpty()){
					int rowId		= formInput.get("id") != null ? Integer.parseInt(formInput.get("id")) : 0;
					String role		= formInput.get("role") != null ? formInput.get("role").trim() : "ROLE_N";
					String loginId		= formInput.get("loginId") != null ? formInput.get("loginId").trim() : "";
					String status	= formInput.get("status") != null ? formInput.get("status").trim() : "ACTIVE".trim();

					System.out.println("rowId>>"+rowId);
					System.out.println("loginId>>"+loginId);
					System.out.println("role>>"+role);
					System.out.println("status>>"+status);

					admin.setId(rowId);
					admin.setRole(role);
					admin.setStatus(status);


					AdminUser  oldAdmin= loginService.checkExistUser(loginId);
					String oldRole = oldAdmin.getRole();
					String oldStatus = oldAdmin.getStatus();


					Map paramOldAccess = new HashMap();
					for(AccessRight accObj :oldAdmin.getAccessRights()){
						paramOldAccess.put(accObj.getPortalModule().getDescription(), accObj.getId());					
					}
					//ROLE_M > ROLE_C	
					System.out.println("oldRole>>"+oldRole);
					System.out.println("role>>"+role);
					if(!oldRole.equalsIgnoreCase(role)){
						if(oldRole.equalsIgnoreCase(ApplicationConstant.ROLE_N) || role.equalsIgnoreCase(ApplicationConstant.ROLE_N)){
							List<PortalModule> pMaker = userMgmtService.findSCLModule(ApplicationConstant.ROLE_M);
							for(PortalModule p : pMaker){
								for(AccessRight accObj :oldAdmin.getAccessRights()){
									if(accObj.getPortalModuleId() == p.getId())
									{
										AccessRight n_access = new AccessRight();
										n_access.setId(accObj.getId());
										n_access.setDeletedFlag(1);
										System.out.println("disabled all SCL(maker) adminuser="+rowId+",id="+n_access.getId()+",deleteflag=0");
										userMgmtService.updateAccessRight(n_access);
									}
								}
							}

							List<PortalModule> pChecker = userMgmtService.findSCLModule(ApplicationConstant.ROLE_C);
							for(PortalModule p : pChecker){
								for(AccessRight accObj :oldAdmin.getAccessRights()){
									if(accObj.getPortalModuleId() == p.getId())
									{
										AccessRight n_access = new AccessRight();
										n_access.setId(accObj.getId());
										n_access.setDeletedFlag(1);
										System.out.println("disabled all SCL(checker) adminuser="+rowId+",id="+n_access.getId()+",deleteflag=0");
										userMgmtService.updateAccessRight(n_access);
									}
								}
							}
						}else if(paramOldAccess.containsKey(ApplicationConstant.MODULE_M_SCL) && paramOldAccess.containsKey(ApplicationConstant.MODULE_C_SCL)){
							System.out.println("contain checker scl & maker scl");
							//update new role
							List<PortalModule> pNew = userMgmtService.findSCLModule(role);
							for(PortalModule p : pNew){
								for(AccessRight accObj :oldAdmin.getAccessRights()){
									if(accObj.getPortalModuleId() == p.getId())
									{
										AccessRight n_access = new AccessRight();
										n_access.setId(accObj.getId());
										n_access.setDeletedFlag(0);
										System.out.println("update new role rowid="+rowId+",id="+n_access.getId()+",deleteflag=0");
										userMgmtService.updateAccessRight(n_access);

									}
								}

							}
							//update old role
							List<PortalModule> pO = userMgmtService.findSCLModule(oldRole);
							for(PortalModule p : pO){
								for(AccessRight accObj :oldAdmin.getAccessRights()){
									if(accObj.getPortalModuleId() == p.getId())
									{
										AccessRight o_access = new AccessRight();
										o_access.setId(accObj.getId());
										o_access.setDeletedFlag(1);
										System.out.println("update old rowid="+rowId+",id="+o_access.getId()+",deleteflag=1");
										userMgmtService.updateAccessRight(o_access);

									}
								}
							}
						}else if((paramOldAccess.containsKey(ApplicationConstant.MODULE_M_SCL) && !paramOldAccess.containsKey(ApplicationConstant.MODULE_C_SCL)) ||
								paramOldAccess.containsKey(ApplicationConstant.MODULE_C_SCL) && !paramOldAccess.containsKey(ApplicationConstant.MODULE_M_SCL)){
							System.out.println("no record for new role");
							//update old role
							List<PortalModule> pO = userMgmtService.findSCLModule(oldRole);
							for(PortalModule p : pO){
								for(AccessRight accObj :oldAdmin.getAccessRights()){
									if(accObj.getPortalModuleId() == p.getId())
									{
										AccessRight o_access = new AccessRight();
										o_access.setId(accObj.getId());
										o_access.setDeletedFlag(1);
										System.out.println("update old adminuser="+rowId+",id="+o_access.getId()+",deleteflag=1");
										userMgmtService.updateAccessRight(o_access);
									}
								}
							}
							List<PortalModule> pN = userMgmtService.findSCLModule(role);
							for(PortalModule p : pN){
								AccessRight n_access = new AccessRight();
								n_access.setAdminUserId(rowId);
								n_access.setPortalModuleId(p.getId());
								n_access.setDeletedFlag(0);
								userMgmtService.saveAccessRight(n_access);
								System.out.println("insert new role service counter locator");
							}
						}
					}

					userMgmtService.updateAdminUser(admin);
					reportService.auditLog("User Management", new AuditTrail(user.getId(),0,"","Update User: "+user.getLoginId(),"role:["+oldRole+"], status:["+oldStatus+"]","role:["+role+"], status:["+status+"]",""));
					map.put("success", StringUtil.showCommonMsg("success", "Successfully update user details."));

				}
			}

		} catch (Exception ex) {
			map.put("error", StringUtil.showCommonMsg("error", "Fail to update user"));
			reportService.auditLog("User Management", new AuditTrail(user.getId(),0,"","Update User: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
			log.error(ex.getMessage());
			ex.printStackTrace();		

		}
		return map;
	}	

	@RequestMapping(value = "/admin/moduleAccessMgmt",  method = RequestMethod.GET)
	public ModelAndView doModuleAccessManagemet()	{
		ModelAndView model = new ModelAndView();
		List<AdminUser> loginIdList = new ArrayList<AdminUser> ();
		try {
			loginIdList = loginService.findActiveUser();


		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		model.addObject("loginIdList", loginIdList);
		model.setViewName("/userMgmt/moduleAccessMgmt");
		return model;
	}

	@RequestMapping(value = "/admin/moduleAccessMgmt",  method = RequestMethod.POST)
	public ModelAndView searchModuleAccessManagemet(@RequestParam(value="selectLoginId", defaultValue = "") String selectLoginId,
			@RequestParam(value="displayLoginId", defaultValue = "") String displayLoginId)	{
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		List<AdminUser> loginIdList = new ArrayList<AdminUser> ();
		List<PortalModule> moduleList = new ArrayList<PortalModule>();
		List<AccessRight> accessList =  new ArrayList<AccessRight>();
		List<MyModule> module = new ArrayList<MyModule>();

		try {
			loginIdList = loginService.findActiveUser();	
			AdminUser selectedUser = loginService.getUserDetails(displayLoginId);
			moduleList = userMgmtService.findAllModuleByRole(selectedUser.getRole());
			accessList = userMgmtService.findUserManagement(Integer.parseInt(selectLoginId));


			for(PortalModule p : moduleList){
				MyModule mod = new MyModule();
				mod.setId(p.getId());
				mod.setName(p.getName());
				mod.setDescription(p.getDescription());
				mod.setAccessUrl(p.getAccessUrl());
				mod.setModuleCode(p.getModuleCode());
				mod.setLevel(p.getLevel());
				mod.setRole(p.getRole());
				module.add(mod);
			}

			for(MyModule mod : module){			
				int module_id = mod.getId();
				mod.setAdminId(selectedUser.getId());
				mod.setAdminRole(selectedUser.getRole());
				mod.setAdminLoginId(selectedUser.getLoginId());
				for(AccessRight acc :accessList){
					int user_access = acc.getPortalModuleId();
					if(module_id == user_access){
						if(acc.getDeletedFlag()== 0){
							mod.setAccess("Y");
							break;
						}
					}
				}
			}
			reportService.auditLog("User Management", new AuditTrail(user.getId(),0,"","Module Access Management: "+user.getLoginId(),"","loginId:["+displayLoginId+"]",""));




		} catch (Exception ex) {
			log.error(ex.getMessage());
			reportService.auditLog("User Management", new AuditTrail(user.getId(),0,"","Module Access Management: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
			ex.printStackTrace();
		}
		model.addObject("loginIdList", loginIdList);
		model.addObject("moduleList", moduleList);
		model.addObject("accessList", accessList);
		model.addObject("module", module);
		model.addObject("displayLoginId", displayLoginId);
		model.setViewName("/userMgmt/moduleAccessMgmt");
		return model;
	}


	@RequestMapping(value = "/admin/updateModuleAccessMgmt",  method = RequestMethod.POST )
	public @ResponseBody Map<String,String> updateModuleAccessMgmt(@RequestBody List<Map<String, String>> client)
			throws IOException {

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		Map<String,String> map = new HashMap<String,String>();

		try {
			List<String> mainMod = new ArrayList<String>();

			String adminRole = "";
			int adminId =0;
			String adminLoginId="";
			for (Map<String, String> formInput : client) {

				if(formInput!=null && !formInput.isEmpty()){
					int rowId		= formInput.get("id") != null ? Integer.parseInt(formInput.get("id")) : 0;
					adminId	= formInput.get("adminId") != null ? Integer.parseInt(formInput.get("adminId")) : 0;
					String moduleCode	= formInput.get("moduleCode") != null ? formInput.get("moduleCode") : "".trim();
					adminRole	= formInput.get("adminRole") != null ? formInput.get("adminRole") : "".trim();
					adminLoginId = formInput.get("adminLoginId") != null ? formInput.get("adminLoginId") : "".trim();

					if((moduleCode.trim().split("\\.")).length == 3){//level 3
						mainMod.add( moduleCode.substring(0,2));
						mainMod.add( moduleCode.substring(0,5));
						mainMod.add( moduleCode);
					}else if((moduleCode.trim().split("\\.")).length == 2){//level 2
						mainMod.add( moduleCode.substring(0,2));							
						mainMod.add( moduleCode);
					}else{
						mainMod.add( moduleCode);
					}	
				}
			}

			mainMod = mainMod.stream().distinct().collect(Collectors.toList());

			List<Integer> selectionModIds = userMgmtService.findPortalModuleIdsByCode(mainMod);
			List<AccessRight> access = userMgmtService.findExistingAccessByAdminId(adminId);
			List<PortalModule> allmodule = userMgmtService.findAllModuleByRole2(adminRole);

			//START for audit trail purpose
			List<AccessRight> oldAccess = userMgmtService.findUserManagement(adminId);
			List<String> moduleList = new ArrayList<String>();
			for(AccessRight a : oldAccess){
				moduleList.add(a.getPortalModule().getDescription());
			}
			//End for audit trail purpose

			//retrieve all user module based on ROLE and added into customize modal <MyAccessRight>
			List<MyAccessRight> allMyAccess = new ArrayList<MyAccessRight>();
			for(PortalModule p : allmodule){
				MyAccessRight myacc = new MyAccessRight();				
				myacc.setPortalModuleId(p.getId());
				allMyAccess.add(myacc);
			}

			//if module is checked, status =0, else status =1.
			for(MyAccessRight mod : allMyAccess){			
				int module_id = mod.getPortalModuleId();
				mod.setAdminUserId(adminId);
				for(Integer newacc :selectionModIds){
					int user_access = newacc;
					if(module_id == user_access){						
						mod.setStatus("0");
						break;

					}
				}
			}

			Map param = new HashMap();
			Map param_exist = new HashMap();

			for(MyAccessRight mod : allMyAccess){					
				param.put(mod.getPortalModuleId(),mod.getStatus());
			}

			Iterator iterator = param.entrySet().iterator();
			//update status
			while (iterator.hasNext()) {
				Map.Entry mapEntry = (Map.Entry) iterator.next();
				int module_id =  (int) mapEntry.getKey();
				String stat = (String) mapEntry.getValue();

				int add = 0;

				for(AccessRight acc  :access){
					int user_access = acc.getPortalModuleId();
					//System.out.println("module_id:"+module_id+",user_access:"+user_access+",stat:"+stat+", acc.getDeletedFlag():"+ acc.getDeletedFlag());
					if(module_id == user_access){								
						if(!stat.equalsIgnoreCase(""+acc.getDeletedFlag())){
							//System.out.println("update module :[adminId:"+adminId+",module_id:"+module_id+",deleted_flag="+stat);
							AccessRight newAccess = new AccessRight();
							//System.out.println("access id :"+acc.getId());
							newAccess.setId(acc.getId());
							newAccess.setPortalModuleId(module_id);
							newAccess.setDeletedFlag(Integer.parseInt(stat));
							userMgmtService.updateAccessRight(newAccess);
						}
						add=1;
						break;
					}else{
						add=0;
					}
				}

				if(add==0){		
					AccessRight newAccess = new AccessRight();
					newAccess.setAdminUserId(adminId);
					newAccess.setPortalModuleId(module_id);
					newAccess.setDeletedFlag(Integer.parseInt(stat));
					userMgmtService.saveAccessRight(newAccess);
					//System.out.println("insert module :[adminId:"+adminId+",module_id:"+module_id+"deleted_flag="+stat);

				}
				param_exist.put(module_id, stat);
			}


			for(AccessRight acc  :access){
				int user_access = acc.getPortalModuleId();

				if(param_exist.containsKey(user_access)==false){
					AccessRight newAccess = new AccessRight();
					newAccess.setId(acc.getId());
					newAccess.setPortalModuleId(user_access);
					newAccess.setDeletedFlag(1);
					userMgmtService.updateAccessRight(newAccess);
					//System.out.println("deleted flag set to 1 for module :"+user_access);
				}
			}		

			//START for audit trail purpose
			List<AccessRight> newAccess = userMgmtService.findUserManagement(adminId);
			List<String> newList = new ArrayList<String>();
			for(AccessRight a : newAccess){
				newList.add(a.getPortalModule().getDescription());
			}
			//End for audit trail purpose

			reportService.auditLog("User Management", new AuditTrail(user.getId(),0,"","Module Access Management: "+user.getLoginId(),"for user ["+adminLoginId+"],module:"+moduleList.toString(),"module:"+newList.toString(), ""));			
			map.put("success", StringUtil.showCommonMsg("success", "Successfully update module access management."));

		} catch (Exception ex) {
			reportService.auditLog("User Management", new AuditTrail(user.getId(),0,"","Module Access Management: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));			
			log.error(ex.getMessage());
			ex.printStackTrace();
			map.put("error", StringUtil.showCommonMsg("error", "Fail to update module access management."));


		}


		return map;
	}

	@RequestMapping(value = "/admin/getModulesList",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody List<MyModule> getModulesList(@RequestBody MyModule module)
			throws IOException {
		List<MyModule> moduleList = new ArrayList<MyModule>();

		try {
			//System.out.println("paramRegion>>"+module.getRole());	
			List<PortalModule> portalList = userMgmtService.findAllModuleByRole(module.getRole());
			for(PortalModule p: portalList){
				MyModule my = new MyModule();
				my.setRole(p.getRole());
				my.setModuleCode(p.getModuleCode());
				my.setDescription(p.getDescription());
				moduleList.add(my);
			}		


			//System.out.println(">>"+moduleList.size());			
		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return moduleList;
	}





}
