package com.seb.admin.portal.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.AccessRight;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.PortalModule;

@Repository
public class UserMgmtDAOImpl implements UserMgmtDAO{

	private static final Logger log = Logger.getLogger(UserMgmtDAOImpl.class);

	@PersistenceContext(unitName = ApplicationConstant.UNIT_NAME , type = PersistenceContextType.EXTENDED)
	private EntityManager em;

	public List<AccessRight> findAllAccessById(int adminId) throws SQLException{
		List<AccessRight> userAccessList = new ArrayList<AccessRight>();
		userAccessList = em.createNamedQuery("AccessRight.findAllByAdminId",AccessRight.class).setParameter("adminId", adminId).getResultList();
		return userAccessList;
	}

	public List<AccessRight> findMainAccessById(int adminId, String role) throws SQLException{
		List<AccessRight> userAccessList = new ArrayList<AccessRight>();
		userAccessList = em.createNamedQuery("AccessRight.findMainAccessById",AccessRight.class).setParameter("adminId", adminId).setParameter("role", "%"+role+"%").getResultList();
		return userAccessList;
	}

	public List<PortalModule> findSubModuleByModuleCode(int adminId, String parentCode, int level, String role) throws SQLException{
		List<PortalModule> subModuleList = new ArrayList<PortalModule>();
		subModuleList = em.createNamedQuery("PortalModule.findSubModuleByModuleCode",PortalModule.class).setParameter("adminId", adminId).setParameter("parentCode", parentCode).setParameter("level", level).setParameter("role", "%"+role+"%").getResultList();
		return subModuleList;
	}

	@Override
	public List<PortalModule> findAllModuleByRole(String role) throws SQLException {
		List<PortalModule> modulesList = new ArrayList<PortalModule>();
		try{
			modulesList = em.createNamedQuery("PortalModule.findAllModuleByRole",PortalModule.class).setParameter("role", "%"+role+"%").getResultList();

		} catch(Exception ex){
			ex.printStackTrace();
		}
		return modulesList;
	}
	
	@Override
	public List<PortalModule> findAllModuleByRole2(String role) throws SQLException {
		List<PortalModule> modulesList = new ArrayList<PortalModule>();
		try{
			modulesList = em.createNamedQuery("PortalModule.findAllModuleByRole2",PortalModule.class).setParameter("role", "%"+role+"%").getResultList();

		} catch(Exception ex){
			ex.printStackTrace();
		}
		return modulesList;
	}

	@Override
	public PortalModule findModuleById(int moduleId){		
		PortalModule finder = em.find(PortalModule.class, moduleId);		
		return finder;
	}
	
	@Override
	public List<PortalModule> findSCLModule(String role) throws SQLException {
		List <PortalModule> moduleList = new ArrayList<PortalModule>();
		try{
			moduleList = em.createNamedQuery("PortalModule.findSCLModule",PortalModule.class).setParameter("role", "%"+role+"%").getResultList();

		}catch(Exception ex){ex.printStackTrace();
		}
		return moduleList;
	}


	@Override
	public AdminUser saveNewAdmin(AdminUser newAdmin)  throws SQLException{
		try{		
			em.persist(newAdmin);
			em.refresh(newAdmin);
			return newAdmin;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Integer> findPortalModuleIdsByCode(List<String> moduleCodes) throws SQLException {
		List<Integer> ids = new ArrayList<Integer>();
		try{
			ids = em.createNamedQuery("PortalModule.findPortalModuleIdsByCode",Integer.class).setParameter("moduleCodes", moduleCodes).getResultList();

		}catch(Exception ex){ex.printStackTrace();}
		return ids;
	}
	
	@Override
	public List<Integer> findPortalModuleIdsByCodeNRole(List<String> moduleCodes, String role) throws SQLException {
		List<Integer> ids = new ArrayList<Integer>();
		try{
			ids = em.createNamedQuery("PortalModule.findPortalModuleIdsByCodeNRole",Integer.class).setParameter("moduleCodes", moduleCodes).setParameter("role", "%"+role+"%").getResultList();

		}catch(Exception ex){ex.printStackTrace();}
		return ids;
	}
	
	@Override
	public List<AccessRight> findExistModule(AccessRight a0) throws SQLException {
		List<AccessRight> list = new ArrayList<AccessRight>();
		try{	
			list = em.createNamedQuery("AccessRight.findExistModule",AccessRight.class).setParameter("adminId", a0.getAdminUserId()).setParameter("moduleId", a0.getPortalModuleId()).getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
		}	
		return  list;
	}
	
	@Override
	public AccessRight saveAccessRight(AccessRight acc) throws SQLException {
		try{		
			em.persist(acc);
			em.refresh(acc);
			return acc;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public List<PortalModule> findSelectedModulesByCode(List<String> moduleCodes) throws SQLException {
		List<PortalModule> modules = new ArrayList<PortalModule>();
		try{
			modules = em.createNamedQuery("PortalModule.findSelectedModulesByCode",PortalModule.class).setParameter("moduleCodes", moduleCodes).getResultList();

		}catch(Exception ex){ex.printStackTrace();}
		return modules;
	}

	@Override
	public List<AccessRight> findUserManagement(int loginId) throws SQLException {
		List<AccessRight> accessList = new ArrayList<AccessRight>();
		try{
			accessList = em.createNamedQuery("AccessRight.findUserManagement",AccessRight.class).setParameter("adminId", loginId).getResultList();

		}catch(Exception ex){ex.printStackTrace();}
		return accessList;
	}

	@Override
	public List<AccessRight> findExistingAccessByAdminId(int adminId) throws SQLException {
		List<AccessRight> accessList = new ArrayList<AccessRight>();
		try{
			accessList = em.createNamedQuery("AccessRight.findExistingAccessByAdminId",AccessRight.class).setParameter("adminId", adminId).getResultList();

		}catch(Exception ex){ex.printStackTrace();
		}
		return accessList;
	}

	@Override
	public AdminUser updateAdminUser(AdminUser a0) throws SQLException {

		AdminUser update = em.find(AdminUser.class, a0.getId());
		System.out.println("role>"+a0.getRole());
		update.setRole(a0.getRole());
		update.setStatus(a0.getStatus());
		em.merge(update);

		return a0;
	}
	
	@Override
	public AdminUser updateAdminUserAD(AdminUser a0) throws SQLException {

		AdminUser update = em.find(AdminUser.class, a0.getId());
		System.out.println("role>"+a0.getRole());
		update.setDepartment(a0.getDepartment());
		update.setName(a0.getName());
		em.merge(update);

		return a0;
	}
	
	@Override
	public AccessRight updateAccessRight(AccessRight a0) throws SQLException {
		
		try{
			System.out.println("access right id ="+a0.getId());
			AccessRight update = em.find(AccessRight.class, a0.getId());
			update.setDeletedFlag(a0.getDeletedFlag());
			em.merge(update);
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return a0;
	}
	








}
