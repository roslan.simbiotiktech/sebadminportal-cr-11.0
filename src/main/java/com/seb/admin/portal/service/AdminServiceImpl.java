package com.seb.admin.portal.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.seb.admin.portal.dao.AdminDAO;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminDAO adminDAO;

	@Transactional
	public boolean changePassword(int loginID, String newPassword) throws SQLException {		
		return adminDAO.changePassword(loginID, newPassword);	
	}		
}
