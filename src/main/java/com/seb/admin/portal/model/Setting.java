package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.seb.admin.portal.constant.ApplicationConstant;


/**
 * The persistent class for the "SETTING" database table.
 * 
 */
@Entity
@Table(name="SETTING", schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="Setting.findAll", query="SELECT s FROM Setting s"),
	@NamedQuery(name="Setting.findByKey", query="SELECT s FROM Setting s WHERE s.settingKey =:key")	
})
public class Setting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SETTING_KEY", unique=true, nullable=false, length=100)
	private String settingKey;

	@Column(name="SETTING_VALUE", nullable=false, length=100)
	private String settingValue;

	public Setting() {
	}

	public String getSettingKey() {
		return this.settingKey;
	}

	public void setSettingKey(String settingKey) {
		this.settingKey = settingKey;
	}

	public String getSettingValue() {
		return this.settingValue;
	}

	public void setSettingValue(String settingValue) {
		this.settingValue = settingValue;
	}

}