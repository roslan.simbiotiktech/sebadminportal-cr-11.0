package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;
import java.util.Arrays;


/**
 * The persistent class for the REPORT database table.
 * 
 */
@Entity
@Table(name="SFDC_REPORT", schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="SfdcReport.findAll", query="SELECT r FROM SfdcReport r"),
	@NamedQuery(name="SfdcReport.findAllByCondition", query="SELECT r FROM SfdcReport r WHERE r.createdDatetime >=:dateFrom AND r.createdDatetime <=:dateTo AND UPPER(r.channel) LIKE :channel AND UPPER(r.reportType) LIKE :type AND UPPER(r.sebStatus) LIKE :status"),
	@NamedQuery(name="SfdcReport.findByTransIdAndDate", query="SELECT r FROM SfdcReport r WHERE r.createdDatetime =:createdDatetime AND r.transId=:transId")
})
public class SfdcReport implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TRANS_ID", unique=true, nullable=false)
	private int transId;

	@Column(name="CASE_ID")
	private String caseId;

	@Column(name="CASE_NUMBER")
	private String caseNumber;

	private String category;

	private String channel;

	@Column(name="CLIENT_OS")
	private String clientOs;
 
	@Column(name="CONTRACT_ACCOUNT_NUMBER")
	private String contractAccountNumber;

	@Column(name="CREATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp createdDatetime;

	private String description;

	@Column(name="ISSUE_TYPE")
	private String issueType;

	@Column(name="LOC_ADDRESS")
	private String locAddress;

	@Column(name="LOC_LATITUDE")
	private String locLatitude;

	@Column(name="LOC_LONGITUDE")
	private String locLongitude;

	@Lob
	@Column(name="PHOTO_1")
	private byte[] photo1;

	@Column(name="PHOTO_1_TYPE")
	private String photo1Type;

	@Lob
	@Column(name="PHOTO_2")
	private byte[] photo2;

	@Column(name="PHOTO_2_TYPE")
	private String photo2Type;

	@Lob
	@Column(name="PHOTO_3")
	private byte[] photo3;

	@Column(name="PHOTO_3_TYPE")
	private String photo3Type;

	@Column(name="PREVIOUS_NOTIFIED_STATUS")
	private String previousNotifiedStatus;

	private String remark;

	@Column(name="REPORT_TYPE")
	private String reportType;

	private String station;

	@Column(name="STATUS")
	private String status;

	@Column(name="UPDATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp updatedDatetime;

	@Column(name="USER_EMAIL")
	private String userEmail;

	@Column(name="USER_MOBILE_NUMBER")
	private String userMobileNumber;

	@Column(name="USER_NAME")
	private String userName;

	@Column(name="CLASSIFICATION")
	private String classification;
	
	
	@Column(name="SUB_STATUS")
	private String subStatus;
	
	@Column(name="SEB_STATUS")
	private String sebStatus;
	
	
	public SfdcReport() {
	}

	public int getTransId() {
		return this.transId;
	}

	public void setTransId(int transId) {
		this.transId = transId;
	}

	public String getCaseId() {
		return this.caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getCaseNumber() {
		return this.caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getChannel() {
		return this.channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getClientOs() {
		return this.clientOs;
	}

	public void setClientOs(String clientOs) {
		this.clientOs = clientOs;
	}

	public String getContractAccountNumber() {
		return this.contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}



	public String getIssueType() {
		return this.issueType;
	}

	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}

	public String getLocAddress() {
		return this.locAddress;
	}

	public void setLocAddress(String locAddress) {
		this.locAddress = locAddress;
	}

	public String getLocLatitude() {
		return this.locLatitude;
	}

	public void setLocLatitude(String locLatitude) {
		this.locLatitude = locLatitude;
	}

	public String getLocLongitude() {
		return this.locLongitude;
	}

	public void setLocLongitude(String locLongitude) {
		this.locLongitude = locLongitude;
	}

	public byte[] getPhoto1() {
		return this.photo1;
	}

	public void setPhoto1(byte[] photo1) {
		this.photo1 = photo1;
	}

	public String getPhoto1Type() {
		return this.photo1Type;
	}

	public void setPhoto1Type(String photo1Type) {
		this.photo1Type = photo1Type;
	}

	public byte[] getPhoto2() {
		return this.photo2;
	}

	public void setPhoto2(byte[] photo2) {
		this.photo2 = photo2;
	}

	public String getPhoto2Type() {
		return this.photo2Type;
	}

	public void setPhoto2Type(String photo2Type) {
		this.photo2Type = photo2Type;
	}

	public byte[] getPhoto3() {
		return this.photo3;
	}

	public void setPhoto3(byte[] photo3) {
		this.photo3 = photo3;
	}

	public String getPhoto3Type() {
		return this.photo3Type;
	}

	public void setPhoto3Type(String photo3Type) {
		this.photo3Type = photo3Type;
	}

	public String getPreviousNotifiedStatus() {
		return this.previousNotifiedStatus;
	}

	public void setPreviousNotifiedStatus(String previousNotifiedStatus) {
		this.previousNotifiedStatus = previousNotifiedStatus;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getReportType() {
		return this.reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getStation() {
		return this.station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserMobileNumber() {
		return this.userMobileNumber;
	}

	public void setUserMobileNumber(String userMobileNumber) {
		this.userMobileNumber = userMobileNumber;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getSebStatus() {
		return sebStatus;
	}

	public void setSebStatus(String sebStatus) {
		this.sebStatus = sebStatus;
	}

	
	@Override
	public String toString() {
		return "transId:[" + transId + "], caseId:[" + caseId + "], caseNumber:[" + caseNumber + "], category:["
				+ category + "], channel:[" + channel + "], clientOs:[" + clientOs + "], contractAccountNumber:["
				+ contractAccountNumber + "], createdDatetime:[" + createdDatetime + "], description:[" + description
				+ "], issueType:[" + issueType + "], locAddress:[" + locAddress + "], locLatitude:[" + locLatitude
				+ "], locLongitude:[" + locLongitude + "], photo1:[" + Arrays.toString(photo1) + "], photo1Type:["
				+ photo1Type + "], photo2:[" + Arrays.toString(photo2) + "], photo2Type:[" + photo2Type + "], photo3:["
				+ Arrays.toString(photo3) + "], photo3Type:[" + photo3Type + "], previousNotifiedStatus:["
				+ previousNotifiedStatus + "], remark:[" + remark + "], reportType:[" + reportType + "], station:["
				+ station + "], status:[" + status + "], updatedDatetime:[" + updatedDatetime + "], userEmail:["
				+ userEmail + "], userMobileNumber:[" + userMobileNumber + "], userName:[" + userName
				+ "], classification:[" + classification + "], subStatus:[" + subStatus + "], sebStatus:[" + sebStatus
				+ "]";
	}



	
	

}