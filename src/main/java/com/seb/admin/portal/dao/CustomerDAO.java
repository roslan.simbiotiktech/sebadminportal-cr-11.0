package com.seb.admin.portal.dao;

import java.sql.SQLException;

import com.seb.admin.portal.model.Subscription;

public interface CustomerDAO {
	
	public Subscription findSelectedSubscription(String email, String accountNumber, String status) throws SQLException;

	public boolean updateSubscriptionStatus(int subsId, String subscriptionStatus, String remark) throws SQLException;

	public boolean updateUserProfile(String loginId, String userStatus, String preferredMethod)
			throws SQLException;

	public boolean updateUserSubscription(String email, String oldStatus, String newStatus) throws SQLException;
	

}
