package com.seb.admin.portal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class MyPowerAlert implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	private int id;

	private String area;

	private String causes;

	private Timestamp createdDatetime;

	private int custServiceLocationId;

	private int deletedFlag;

	private Timestamp maintenanceEndDatetime;

	private Timestamp maintenanceStartDatetime;

	private int powerAlertTypeId;
	
	private String powerAlertTypeName;
	
	private String station;

	private Timestamp restorationDatetime;

	private String title;

	private Timestamp updatedDatetime;
	
	private List <MyTagNotification> myTagList;

		
	
	//Tag Notification
	
	private int notificationId;

	private Integer associatedId;

	private String notificationType;

	private Timestamp pushDatetime;
	
	private String status;

	private String tagName;

	private String text;

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCauses() {
		return causes;
	}

	public void setCauses(String causes) {
		this.causes = causes;
	}

	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getCustServiceLocationId() {
		return custServiceLocationId;
	}

	public void setCustServiceLocationId(int custServiceLocationId) {
		this.custServiceLocationId = custServiceLocationId;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public Timestamp getMaintenanceEndDatetime() {
		return maintenanceEndDatetime;
	}

	public void setMaintenanceEndDatetime(Timestamp maintenanceEndDatetime) {
		this.maintenanceEndDatetime = maintenanceEndDatetime;
	}

	public Timestamp getMaintenanceStartDatetime() {
		return maintenanceStartDatetime;
	}

	public void setMaintenanceStartDatetime(Timestamp maintenanceStartDatetime) {
		this.maintenanceStartDatetime = maintenanceStartDatetime;
	}

	public int getPowerAlertTypeId() {
		return powerAlertTypeId;
	}

	public void setPowerAlertTypeId(int powerAlertTypeId) {
		this.powerAlertTypeId = powerAlertTypeId;
	}

	public Timestamp getRestorationDatetime() {
		return restorationDatetime;
	}

	public void setRestorationDatetime(Timestamp restorationDatetime) {
		this.restorationDatetime = restorationDatetime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public int getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}

	public Integer getAssociatedId() {
		return associatedId;
	}

	public void setAssociatedId(Integer associatedId) {
		this.associatedId = associatedId;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public Timestamp getPushDatetime() {
		return pushDatetime;
	}

	public void setPushDatetime(Timestamp pushDatetime) {
		this.pushDatetime = pushDatetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPowerAlertTypeName() {
		return powerAlertTypeName;
	}

	public void setPowerAlertTypeName(String powerAlertTypeName) {
		this.powerAlertTypeName = powerAlertTypeName;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}
	
	public List<MyTagNotification> getMyTagList() {
		return myTagList;
	}

	public void setMyTagList(List<MyTagNotification> myTagList) {
		this.myTagList = myTagList;
	}

	

	
	

}
