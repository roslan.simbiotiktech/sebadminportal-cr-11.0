package com.seb.admin.portal.spg.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OFile;
import com.seb.admin.portal.spg.entity.ResponseStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GatewayFPXGenerateCSRResponse {
	
	@JsonProperty("status")
	private ResponseStatus responseStatus;
	@JsonProperty("file")
	private OFile oFile;
	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}
	public OFile getoFile() {
		return oFile;
	}
	public void setoFile(OFile oFile) {
		this.oFile = oFile;
	}
	
	@Override
	public String toString() {
		return "responseStatus:[" + responseStatus + "], oFile:[" + oFile + "]";
	}
	
	

}
