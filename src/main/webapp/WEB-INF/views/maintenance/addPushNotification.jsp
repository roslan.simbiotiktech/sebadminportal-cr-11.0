<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="_csrf" content="${_csrf.token}" />
	<meta name="_csrf_header" content="${_csrf.headerName}" />
    <meta content="" name="description">
    <meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
        </nav>
        <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp"%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Maintenance &gt; Push Notification &gt; Add
                        </div>
                        <div class="panel-body">
                            <form id="addPNForm" name="addPNForm" action="${pageContext.request.contextPath}/admin/addPushNotification.do" class="form-horizontal" method="post" >
                                ${successMsg}${errorMsg}
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Push DateTime <span style="color: red;">*</span> :</p>

                                    <div class="col-lg-3">
                                        <div class='input-group date' id='pushDateTime'>
                                        	<input id="inpushDateTime" name="inpushDateTime" class="form-control datepicker" type='text' readonly="readonly" > <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    	</div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Type <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <select class="form-control capital" id="selectType"
											name="selectType">
											<option value="">Please Select</option>
											<option value="PLAIN">Free Text</option>
											<option value="PROMO">Promo</option>
											<option value="POWER_ALERT">Outage Alert</option>
										</select>
									</div>
								</div>
                                
                                <div id="contentDiv" class="form-group" style="display: none;">
									<p class="col-lg-5 control-label">
										Title <span style="color: red;">*</span> :
									</p>
									<div id="selectTitleDiv" class="col-lg-3">
										<select class="form-control capital" id="selectTitle"
											name="selectTitle">
											<option value="">Please Select</option>
										</select>
									</div>
								</div>
								
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Message <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <textarea class="form-control" cols="23" id="message" maxlength="120" name="message" rows="10"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-5 col-lg-offset-3 text-right">
                                        <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
                                        <input id="displayType" name="displayType" type="hidden" value=""> 
                                        <input id="displayTitle" name="displayTitle" type="hidden" value=""> 
                                        <button class="btn btn-primary"  id="submitBtn" type="button" data-toggle="modal" data-target="#modal">Add</button> <button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
                                    </div>
                                </div>
                                
                                <!-- Add Confirmation Modal -->
								<div class="modal fade" id="modal" tabindex="-1">
						        <div class="modal-dialog">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button class="close" data-dismiss="modal" type="button"><span>&times;</span><span class="sr-only">Close</span></button>
											<h2 class="modal-title">Confirmation</h2>
						                </div>						
						                <div class="modal-body" id="add">
						                    Are you sure you want to add push notification?
						                </div>						
						                <div class="modal-footer">
						                    <button class="btn btn-primary" id="add" name="add" type="submit">Add</button> <button class="btn btn-warning" data-dismiss="modal" type="button">Close</button> 
						                </div>
						            </div>
						        </div>
   								</div>
   								<!-- End Confirmation Modal -->
                            </form>
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /#page-wrapper -->
    </div>
    <script type="text/javascript">
    function resetSS() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#addPNForm').find('.has-success').removeClass("has-success");
    	$('#addPNForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#addPNForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();    	
    }
        
    
    function resetForm() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#addPNForm').find('.has-success').removeClass("has-success");
    	$('#addPNForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#addPNForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();
    	$('#addPNForm').formValidation('resetForm', true);
    	$('select').val('');
    	setTimeout(function() {
    		/* var ROUNDING = 30 * 60 * 1000; /*ms
    		start = moment();
    		start = moment(Math.round((+start) / ROUNDING) * ROUNDING);
    		start.format("D YYYY, h:mm:ss a");
    		console.log('dddddd>>>>>>>'+start.format("D YYYY, h:mm:ss a")); */
    		//$('#inpushDateTime').val(moment(date_round(new Date(), moment.duration(30, 'minutes'))).format('DD/MM/YYYY hh:mm A'));
    		$('#inpushDateTime').val(moment(new Date()).format('DD/MM/YYYY hh:mm A'));

    	}, 50);
    	return false;
    }
    $(document).ready(function() {    	
    	$('#pushDateTime').datetimepicker({
            format: 'DD/MM/YYYY hh:mm A',
            //defaultDate: moment(date_round(new Date(), moment.duration(30, 'minutes'))),
            //minDate:  moment(date_round(new Date(), moment.duration(30, 'minutes'))),
            defaultDate: new Date(),
            minDate:  new Date(),
            ignoreReadonly: true
        }); 
    	
        $('#selectType').change(function() {
        	 $('#contentDiv').show();
		    var selectedValue = $(this).find("option:selected").val();
		    var selectedText =  $(this).find("option:selected").text();  
		    $('#displayType').val(selectedText);
		    //console.log('fdfd='+selectedText.toLowerCase().indexOf('text'));
		    if (selectedValue != "" && selectedText.toLowerCase().indexOf('text') <=-1) {
		    	$('#contentDiv').show();
		    	document.getElementById('contentDiv').style.display = 'block';
		    	//console.log("selectedText>"+selectedText);		       
		        var token = $("meta[name='_csrf']").attr("content");
		        var header = $("meta[name='_csrf_header']").attr("content");
		        $.ajax({
		            url: "${pageContext.request.contextPath}/admin/getTitle.do?",
		            type: "POST",
		            data: JSON.stringify({ text: selectedText, value: selectedValue }),
		           // cache: false,
		            beforeSend: function(xhr) {
		                xhr.setRequestHeader(header, token);
		                xhr.setRequestHeader("Accept", "application/json");
		                xhr.setRequestHeader("Content-Type", "application/json");
		                sebApp.showIndicator();
		            },
		            success: function(data) {		            	
		            	var html = '<select class="form-control capital" id="selectTitle" name="selectTitle"><option value="">Please Select</option>';
		                  $.each(data, function(index, currEmp) {
		                    html += '<option value="' + currEmp.value + '">' + currEmp.text + '</option>';
		                }); 
		                html += '</select>';
		                $('#selectTitleDiv').html(html).show();
		               // resetSS();
		            },
		            failure: function(data) {},
		            complete: function(){sebApp.hideIndicator();}
		        });
		    }else{
		    	  $('#contentDiv').hide();
		    }
		});
        
        $('#selectTitle').change(function() {
		    var selectedValue = $(this).find("option:selected").val();
		    var selectedText =  $(this).find("option:selected").text();
		    $('#displayTitle').val(selectedText);
        })
        $('#addPNForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
            	inpushDateTime: {
    				validators: {
    					date: {
    						format: 'DD/MM/YYYY hh:mm A',
    					},
    					callback: {
    						callback: function(value, validator, $field) {
    							if (value == '') {
    								$('#inpushDateTime').val(moment(new Date()).format('DD/MM/YYYY hh:mm A'));
    								return true;
    							}
    							return true;
    						}
    					}
    				}
    			},
    			selectType: {
    				validators: {
    					callback: {
    						message: 'Please select Type',
    						callback: function(value, validator, $field) {
    							var options = validator.getFieldElements('selectType').val();
    							return (options != null && options.length > 0);
    						}
    					}
    				}
    			},
    			selectTitle: {
                    validators: {
                        callback: {
                            message: 'Please select Title',
                            callback: function(value, validator, $field) {                            
                                // Get the selected options                           
                                var options = validator.getFieldElements('selectTitle').val();
                            	//console.log('>>>>>>>>>>>'+options);
                                return (options != null && options.length >0);
                            }
                        }
                    }
                },
    			message: {
                    validators: {
                        notEmpty: {
                            message: 'Message is required'
                        },
                        stringLength: {
                            min: 1,
                            max: 120,
                            message: 'Message must less than 120 characters'
                        }
                    }
                }
            }
        });
    });
    </script>
</body>
</html>