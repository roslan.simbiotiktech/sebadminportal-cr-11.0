package com.seb.admin.portal.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.service.LoginService;
import com.seb.admin.portal.service.ReportService;

public class CustomLogoutSuccessHandler extends
SimpleUrlLogoutSuccessHandler implements LogoutSuccessHandler {

	@Autowired
	private LoginService loginService;

	@Autowired
	private ReportService reportService;


	@Override
	public void onLogoutSuccess
	(HttpServletRequest request, HttpServletResponse response, Authentication authentication) 
			throws IOException, ServletException {
		//AdminUser user = (AdminUser) authentication.getPrincipal();


		if (authentication != null && authentication.getDetails() != null) {  
			try {  
				request.getSession().invalidate();  
				AdminUser user = loginService.getUserDetails(authentication.getName());
				System.out.println("onLogoutSuccess>>>>"+user.getLoginId());
				if( user !=null){
					reportService.auditLog("Logout" , new AuditTrail(user.getId(),0,"","Logout: "+user.getLoginId(),"", "", ""));			
				}
				//request.getSession().invalidate();
				new SecurityContextLogoutHandler().logout(request, response, authentication);
				SecurityContextHolder.clearContext();

			} catch (Exception e) {  
				e.printStackTrace();  
				e = null;  
			}  
		} 
		response.setStatus(HttpServletResponse.SC_OK);  
		//redirect to login  
		response.sendRedirect(request.getContextPath() +"/mainLogin.do"); 
	}
}