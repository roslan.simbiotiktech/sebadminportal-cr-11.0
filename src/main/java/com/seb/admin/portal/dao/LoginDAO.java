package com.seb.admin.portal.dao;

import java.sql.SQLException;
import java.util.List;

import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuthenticAdmin;

public interface LoginDAO {
	public List<String> findAllLoginID() throws SQLException;
	public List<AdminUser> findAllUser() throws SQLException;
	public AdminUser getUserDetails(String loginId);
	public AdminUser getSelectedUser(String loginId)throws SQLException;
	public AdminUser findExactAdminUser(int id) throws SQLException;

	public AdminUser checkExistUser(String loginId);
	public List<AdminUser> findActiveUser() throws SQLException;
	
	public boolean updateLastLoggedIn(AdminUser user);
	
	
}
