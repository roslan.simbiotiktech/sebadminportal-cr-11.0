package com.seb.admin.portal.spg.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.constant.PaymentGatewayType;
import com.seb.admin.portal.constant.SpgReferenceStatus;
import com.seb.admin.portal.spg.entity.PAGING;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SPGPaymentReportRequest extends SPGAPIGeneral{
	
	@JsonProperty("fpx_bank_id")
	private String payment_gateway;
	
	@JsonProperty("payment_gateway_reference_id")
	private String payment_gateway_reference_id;
	
	@JsonProperty("spg_reference_status")
	private String spg_reference_status;
	
	@JsonProperty("cutomer_username")
	private String cutomer_username;
	
	@JsonProperty("customer_email")
	private String customer_email;
	
	@JsonProperty("merchant_id")
	private Integer merchant_id;
	
	@JsonProperty("merchant_reference_id")
	private String merchant_reference_id;
	
	@JsonProperty("PAGING")
	private PAGING paging;

	public String getPayment_gateway() {
		return payment_gateway;
	}

	public void setPayment_gateway(String payment_gateway) {
		this.payment_gateway = payment_gateway;
	}

	public String getPayment_gateway_reference_id() {
		return payment_gateway_reference_id;
	}

	public void setPayment_gateway_reference_id(String payment_gateway_reference_id) {
		this.payment_gateway_reference_id = payment_gateway_reference_id;
	}

	public String getSpg_reference_status() {
		return spg_reference_status;
	}

	public void setSpg_reference_status(String spg_reference_status) {
		this.spg_reference_status = spg_reference_status;
	}

	public String getCutomer_username() {
		return cutomer_username;
	}

	public void setCutomer_username(String cutomer_username) {
		this.cutomer_username = cutomer_username;
	}

	public String getCustomer_email() {
		return customer_email;
	}

	public void setCustomer_email(String customer_email) {
		this.customer_email = customer_email;
	}

	public Integer getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(Integer merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getMerchant_reference_id() {
		return merchant_reference_id;
	}

	public void setMerchant_reference_id(String merchant_reference_id) {
		this.merchant_reference_id = merchant_reference_id;
	}

	public PAGING getPaging() {
		return paging;
	}

	public void setPaging(PAGING paging) {
		this.paging = paging;
	}

	@Override
	public String toString() {
		return "payment_gateway:[" + payment_gateway + "], payment_gateway_reference_id:["
				+ payment_gateway_reference_id + "], spg_reference_status:[" + spg_reference_status
				+ "], cutomer_username:[" + cutomer_username + "], customer_email:[" + customer_email
				+ "], merchant_id:[" + merchant_id + "], merchant_reference_id:[" + merchant_reference_id
				+ "], paging:[" + paging + "]";
	}

}
