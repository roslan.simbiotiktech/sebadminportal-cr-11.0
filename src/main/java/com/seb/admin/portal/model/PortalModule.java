package com.seb.admin.portal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.seb.admin.portal.constant.ApplicationConstant;


/**
 * The persistent class for the PORTAL_MODULE database table.
 * 
 */
@Entity
@Table(name="PORTAL_MODULE", schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="PortalModule.findAll", query="SELECT p FROM PortalModule p WHERE p.deletedFlag=0 ORDER BY p.moduleCode asc"),
	@NamedQuery(name="PortalModule.findSubModuleByModuleCode", query="SELECT p FROM PortalModule p JOIN p.accessRights ad WHERE ad.adminUserId =:adminId AND  p.parentModuleCode =:parentCode AND p.level =:level AND p.role LIKE :role AND p.deletedFlag=0 AND ad.deletedFlag=0  ORDER BY p.moduleCode asc" ),
	@NamedQuery(name="PortalModule.findAllModuleByRole", query="SELECT p FROM PortalModule p WHERE  p.accessUrl<>'' AND  p.role LIKE :role AND p.deletedFlag=0 ORDER BY p.moduleCode asc"),
	@NamedQuery(name="PortalModule.findPortalModuleIdsByCode", query="SELECT p.id FROM PortalModule p WHERE p.moduleCode IN (:moduleCodes) AND p.deletedFlag=0"),
	@NamedQuery(name="PortalModule.findPortalModuleIdsByCodeNRole", query="SELECT p.id FROM PortalModule p WHERE p.moduleCode IN (:moduleCodes) AND p.role LIKE :role AND p.deletedFlag=0"),
	@NamedQuery(name="PortalModule.findSelectedModulesByCode", query="SELECT p FROM PortalModule p WHERE p.moduleCode IN (:moduleCodes) AND p.deletedFlag=0"),
	@NamedQuery(name="PortalModule.findAllModuleByRole2", query="SELECT p FROM PortalModule p WHERE  p.role LIKE :role AND p.deletedFlag=0 ORDER BY p.moduleCode asc"),
	@NamedQuery(name="PortalModule.findSCLModule", query="select p from PortalModule p  where p.role like :role and p.moduleCode IN ('02.01','02.01.01','02.01.02')")
 
})
public class PortalModule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(name="ACCESS_URL", length=100)
	private String accessUrl;

	@Column(length=100)
	private String category;

	@Column(name="CREATED_DATETIME" , nullable=false, insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(length=255)
	private String description;

	@Lob
	@Column(name="IMAGE_CONTENT")
	private String imageContent;

	@Column(name="LEVEL")
	private int level;

	@Column(name="MODULE_CODE", length=100)
	private String moduleCode;

	@Column(name="NAME", length=100)
	private String name;

	@Column(name="PARENT_MODULE_CODE", length=100)
	private String parentModuleCode;

	@Column(name="ROLE", length=255)
	private String role;

	@Column(name="SUB_MODULE_NAME", length=255)
	private String subModuleName;

	@Column(name="UPDATED_DATETIME" , nullable=false, insertable = false, updatable = false)
	private Timestamp updatedDatetime;

	//bi-directional many-to-one association to AccessRight
	@OneToMany(mappedBy="portalModule")
	private List<AccessRight> accessRights;

	public PortalModule() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccessUrl() {
		return this.accessUrl;
	}

	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageContent() {
		return this.imageContent;
	}

	public void setImageContent(String imageContent) {
		this.imageContent = imageContent;
	}

	public int getLevel() {
		return this.level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getModuleCode() {
		return this.moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentModuleCode() {
		return this.parentModuleCode;
	}

	public void setParentModuleCode(String parentModuleCode) {
		this.parentModuleCode = parentModuleCode;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getSubModuleName() {
		return this.subModuleName;
	}

	public void setSubModuleName(String subModuleName) {
		this.subModuleName = subModuleName;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	@JsonManagedReference
	public List<AccessRight> getAccessRights() {
		return accessRights;
	}

	public void setAccessRights(List<AccessRight> accessRights) {
		this.accessRights = accessRights;
	}
	
}