package com.seb.admin.portal.util;

import java.security.MessageDigest;

import org.apache.log4j.Logger;

public class Encryptor
{
	private static final Logger log = Logger.getLogger(Encryptor.class);
	
	public Encryptor() 
	{		
	}

	/**
	 * encrypts a string (for password)
	 * 
	 * @param key
	 * @return
	 */
	public static String getMD5(String key) 
	{
		StringBuffer sb = new StringBuffer();
		
		try 
		{
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(key.getBytes());
			
			byte[] toDigest = ((MessageDigest) md.clone()).digest();
			String tmp = null;
			
			for (int i = 0; i < toDigest.length; i++) 
			{
				if (toDigest[i] >= 0) 
				{
					tmp = Integer.toHexString(toDigest[i]);
				}
				else
				{
					if (toDigest[i] < 0) 
					{
						tmp = Integer.toHexString(toDigest[i]).substring(6);
					}
				}
				
				sb.append(tmp.length() == 1 ? "0" + tmp : tmp);
			}
		} 
		catch (Exception ex) 
		{
			log.fatal(ex.getCause(), ex);
		}				
		
		return sb.toString();
	}
	
	
	public static void main(String[] args) {
		System.out.println(Encryptor.getMD5("sebadmin"));
	}

}
