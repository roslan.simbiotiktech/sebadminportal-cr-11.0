package com.seb.admin.portal.service;

import java.util.List;

import com.seb.admin.portal.model.ReportRemarksHistory;

public interface ReportRemarksHistoryService {

	public List<ReportRemarksHistory> getReportRemarksHistory(String transId);
	
}
