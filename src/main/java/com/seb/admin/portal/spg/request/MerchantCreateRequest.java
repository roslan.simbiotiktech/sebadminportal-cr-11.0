package com.seb.admin.portal.spg.request;


import com.fasterxml.jackson.annotation.JsonInclude;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OMerchantBase;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class MerchantCreateRequest extends SPGAPIGeneral {
	
	@JsonProperty("merchant")
	private OMerchantBase merchant;

	public OMerchantBase getMerchant() {
		return merchant;
	}

	public void setMerchant(OMerchantBase merchant) {
		this.merchant = merchant;
	}

	@Override
	public String toString() {
		return  ""+merchant ;
	}
	
	

}
