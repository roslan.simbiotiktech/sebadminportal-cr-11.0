package com.seb.admin.portal.adapter;

import java.util.List;

public class CustomerHistoryResponse {
	private long respStat;
	private String errCode;
	private String errDesc;	
	private String maxPage;
	private String totalRecords;
	private String page;
	
	private List<CustAuditTrail> custATList;

	public long getRespStat() {
		return respStat;
	}

	public void setRespStat(long respStat) {
		this.respStat = respStat;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrDesc() {
		return errDesc;
	}

	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}

	public String getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(String maxPage) {
		this.maxPage = maxPage;
	}

	public String getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public List<CustAuditTrail> getCustATList() {
		return custATList;
	}

	public void setCustATList(List<CustAuditTrail> custATList) {
		this.custATList = custATList;
	}

	@Override
	public String toString() {
		return "CustomerHistoryResponse [respStat=" + respStat + ", errCode=" + errCode + ", errDesc=" + errDesc
				+ ", maxPage=" + maxPage + ", totalRecords=" + totalRecords + ", page=" + page + ", custATList="
				+ custATList + "]";
	}
	
	

}
