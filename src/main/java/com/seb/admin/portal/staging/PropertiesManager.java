package com.seb.admin.portal.staging;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import com.seb.admin.portal.constant.ApplicationConstant;



public class PropertiesManager {
	
	Properties properties = new Properties();
	public PropertiesManager()
	{
		try
		{
			properties.load(new FileInputStream(ApplicationConstant.PROP_PATH));
		}
		catch(IOException ioE)
		{
			ioE.printStackTrace();
		}
	}
	
	public PropertiesManager(String file_path)
	{
		try
		{
			properties.load(new FileInputStream(file_path));

		}
		catch(IOException ioE)
		{
			ioE.printStackTrace();
			System.out.println("** NOT SUPPOSET TO BE IN HERE!***");
		}
	}
	
	
	public String getProperty(String property)
	{
		return properties.getProperty(property);		
	}
	
	public int getIntegerProperty(String property){
		int pro = Integer.valueOf(properties.getProperty(property));
		
		return pro;
	}

}
