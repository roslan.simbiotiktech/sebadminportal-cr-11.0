package com.seb.admin.portal.staging;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;


public class MyUser implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	private Timestamp createdDatetime;

	private int deletedFlag;

	private String department;

	private Date lastLoggedIn;

	private String loginId;

	private String name;

	private String password = "abc123";

	private String role;

	private String status= "ACTIVE";

	private Timestamp updatedDatetime;


	public MyUser() {
	}
	
	public MyUser(Integer id, String loginId, String password, String role, Date lastLoggedIn, int deletedFlag, String status) {
		this.id = id;
		this.loginId = loginId;
		this.password = password;
		this.role = role;
		this.status = status;
		this.lastLoggedIn = lastLoggedIn;
		this.deletedFlag = deletedFlag;

	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Date getLastLoggedIn() {
		return lastLoggedIn;
	}

	public void setLastLoggedIn(Date lastLoggedIn) {
		this.lastLoggedIn = lastLoggedIn;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	

	
	
	

}