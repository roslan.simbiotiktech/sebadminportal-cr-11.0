package com.seb.admin.portal.adapter;

public class ContractDetail {
	private String accountNumber;
	private String accountName;
	private String subscriptionSatus;
	private String accountNick;
	private String subscriptionType;
	private String subscribedAt;
	private String remark;
	
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getSubscriptionSatus() {
		return subscriptionSatus;
	}
	public void setSubscriptionSatus(String subscriptionSatus) {
		this.subscriptionSatus = subscriptionSatus;
	}
	public String getAccountNick() {
		return accountNick;
	}
	public void setAccountNick(String accountNick) {
		this.accountNick = accountNick;
	}
	public String getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	public String getSubscribedAt() {
		return subscribedAt;
	}
	public void setSubscribedAt(String subscribedAt) {
		this.subscribedAt = subscribedAt;
	}	
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "ContractDetail [accountNumber=" + accountNumber + ", accountName=" + accountName
				+ ", subscriptionSatus=" + subscriptionSatus + ", accountNick=" + accountNick + ", subscriptionType="
				+ subscriptionType + ", subscribedAt=" + subscribedAt + ", remark=" + remark +"]";
	}
	
	
	

}