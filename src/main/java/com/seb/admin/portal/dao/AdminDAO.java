package com.seb.admin.portal.dao;

import java.sql.SQLException;

public interface AdminDAO {
	public boolean changePassword(int loginID, String newPassword)  throws SQLException;
}
