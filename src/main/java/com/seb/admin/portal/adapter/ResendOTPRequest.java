package com.seb.admin.portal.adapter;

import com.seb.admin.portal.util.StringUtil;

public class ResendOTPRequest {
	
	private String loginId;
	private String type;
	private String mobileNumber;
	private String remark;
	private String typeDesc;
	
	
	public ResendOTPRequest() {
	}
	
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMobileNumber() {
		return StringUtil.trimToEmpty(mobileNumber);
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTypeDesc() {
		return typeDesc;
	}

	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	
	
}
