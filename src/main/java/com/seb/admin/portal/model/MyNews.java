package com.seb.admin.portal.model;

import java.sql.Timestamp;
import java.util.List;

public class MyNews {
	private int id;

	private Timestamp createdDatetime;

	private int deletedFlag;

	private String description;

	private Timestamp endDatetime;
	
	private byte[] imageL;
	
	private String imageLFileType;
	
	private byte[] imageS;

	private String imageSFileType;

	private Timestamp publishDatetime;

	private Timestamp startDatetime;

	private String title;

	private List <MyTagNotification> myTagList;
		
	//Tag Notification
	
	private int notificationId;

	private Integer associatedId;

	private String notificationType;

	private Timestamp pushDatetime;
	
	private String status;

	private String tagName;

	private String text;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEndDatetime() {
		return endDatetime;
	}

	public void setEndDatetime(Timestamp endDatetime) {
		this.endDatetime = endDatetime;
	}

	public byte[] getImageL() {
		return imageL;
	}

	public void setImageL(byte[] imageL) {
		this.imageL = imageL;
	}

	public String getImageLFileType() {
		return imageLFileType;
	}

	public void setImageLFileType(String imageLFileType) {
		this.imageLFileType = imageLFileType;
	}

	public byte[] getImageS() {
		return imageS;
	}

	public void setImageS(byte[] imageS) {
		this.imageS = imageS;
	}

	public String getImageSFileType() {
		return imageSFileType;
	}

	public void setImageSFileType(String imageSFileType) {
		this.imageSFileType = imageSFileType;
	}

	public Timestamp getPublishDatetime() {
		return publishDatetime;
	}

	public void setPublishDatetime(Timestamp publishDatetime) {
		this.publishDatetime = publishDatetime;
	}

	public Timestamp getStartDatetime() {
		return startDatetime;
	}

	public void setStartDatetime(Timestamp startDatetime) {
		this.startDatetime = startDatetime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}

	public Integer getAssociatedId() {
		return associatedId;
	}

	public void setAssociatedId(Integer associatedId) {
		this.associatedId = associatedId;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public Timestamp getPushDatetime() {
		return pushDatetime;
	}

	public void setPushDatetime(Timestamp pushDatetime) {
		this.pushDatetime = pushDatetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<MyTagNotification> getMyTagList() {
		return myTagList;
	}

	public void setMyTagList(List<MyTagNotification> myTagList) {
		this.myTagList = myTagList;
	}
}
