package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.seb.admin.portal.constant.ApplicationConstant;

import java.util.List;


/**
 * The persistent class for the AUDIT_TRAIL_TYPE database table.
 * 
 */
@Entity
@Table(name="AUDIT_TRAIL_TYPE" , schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="AuditTrailType.findAll", query="SELECT a FROM AuditTrailType a  WHERE a.deletedFlag=0"),
	@NamedQuery(name="AuditTrailType.findAllAuditType", query="SELECT a FROM AuditTrailType a  WHERE a.deletedFlag=0 ORDER BY a.name"),
	@NamedQuery(name="AuditTrailType.findById", query="SELECT a FROM AuditTrailType a WHERE a.name = :name AND a.deletedFlag=0")
})
public class AuditTrailType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(name="NAME", unique=true, nullable=false, length=255)
	private String name;

	//bi-directional many-to-one association to AuditTrail
	@OneToMany(mappedBy="auditTrailType")
	private List<AuditTrail> auditTrails;

	public AuditTrailType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AuditTrail> getAuditTrails() {
		return this.auditTrails;
	}

	public void setAuditTrails(List<AuditTrail> auditTrails) {
		this.auditTrails = auditTrails;
	}

	public AuditTrail addAuditTrail(AuditTrail auditTrail) {
		getAuditTrails().add(auditTrail);
		auditTrail.setAuditTrailType(this);

		return auditTrail;
	}

	public AuditTrail removeAuditTrail(AuditTrail auditTrail) {
		getAuditTrails().remove(auditTrail);
		auditTrail.setAuditTrailType(null);

		return auditTrail;
	}
	

}