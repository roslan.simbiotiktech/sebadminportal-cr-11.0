<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="${_csrf.token}" name="_csrf">
    <meta content="${_csrf.headerName}" name="_csrf_header">
    <meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include  file="../global.jsp"%>
    <title></title>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top">
            <%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
        </nav>
        <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp"%>
            <div class="row">
                <div class="col-lg-12" style="width: inherit;min-width: 100%">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            User Management &gt; Edit User
                        </div>
                        <div class="panel-body">
                            <form id="searchUserForm" name="searchUserForm" action="${pageContext.request.contextPath}/admin/editUser.do" method="post"  class="form-horizontal">
                                ${successMsg}${errorMsg}<div id="successMsg" style="display: none"></div><div id="errorMsg" style="display: none" class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> <span class="sr-only"></span></div>
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Login ID <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <select class="form-control" id="selectLoginId"
											name="selectLoginId">
											<option value="">Please Select</option>
											<c:forEach items="${loginIdList}" var="item">
												<option value="${item.id}">${item.loginId}</option>
											</c:forEach>
										</select>
										<input type="hidden" id="loginId" name="loginId" value=""/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-5 col-lg-offset-3 text-right">
                                        <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> <button class="btn btn-primary" type="submit">Search</button> <button class="btn btn-warning" id="cancelBtn1" type="reset">Cancel</button>
                                    </div>
                                </div>
                            </form>
                            <div id="resultDiv" style="display:none; margin-top: 10%">
                                <form  id="editUserForm" name="editUserForm" action="${pageContext.request.contextPath}/admin/updateUser.do" class="form-horizontal" method="post">
                                    <h4>Search Results : Login ID: <b>${loginId}</b></h4>
                                    <div class="form-group">
                                        <div class="alert alert-danger" id="messages" role="alert" style="display: none;"></div>
                                    </div>                                     
                                    <table cellspacing="0" class="table table-striped table-bordered" id="tblSearch">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Name</th>
                                                <th>Login ID</th>
                                                <th>Department</th>
                                                <th>Role</th>
                                                <th>Module</th>
                                                <th>Status</th>
                                                <th>Last Login Date</th>                                              
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <c:if test="${not empty selectedUser}">
                                    <c:set var="item" scope="session" value="${selectedUser}"/>
                                            <tr>
                                                <td>${status.index}<input type="hidden" id="id" name="id" value="${item.id}"></td>
                                                <td>${item.name}</td>
                                                <td>${item.loginId}<input type="hidden" id="displayLoginId" name="displayLoginId" value="${item.loginId}"></td>
                                                <td>${item.department}</td>
                                                <td>
	                                                <select class="form-control" id="selectRole" name="selectRole"  >
			                                            <option value="">Please Select</option>
			                                            <option value="ROLE_C" ${not empty item.role && item.role == 'ROLE_C' ? 'selected' : ''} >Checker</option>
			                                            <option value="ROLE_M" ${not empty item.role && item.role == 'ROLE_M' ? 'selected' : ''}>Maker</option>
			                                            <option value="ROLE_N" ${not empty item.role && item.role == 'ROLE_N' ? 'selected' : ''}>Normal</option>
	                                        		</select>
												</td>
                                                <td>
                                                <c:forEach items="${accessList}" var="item2" varStatus="loop">
                                                	<p>${item2.portalModule.description}</p>
                                                </c:forEach>

												</td>
                                                <td>
                                                <c:choose>
													<c:when test="${item.status eq 'ACTIVE' || item.status eq 'active'}">														
														<div class="radio">
		  													<label><input  type="radio" name="status" value="ACTIVE" checked="checked"> ACTIVE</label>
														</div><br/>
														<div class="radio">
														  <label><input  type="radio" name="status" value="DISABLED"> DISABLED</label>
														</div>
													</c:when>
													<c:otherwise>
														<div class="radio">
		  													<label><input  type="radio" name="status" value="ACTIVE"> ACTIVE</label>
														</div><br/>
														<div class="radio">
														  <label><input  type="radio" name="status" value="DISABLED" checked="checked"> DISABLED</label>
														</div>
													</c:otherwise>
													</c:choose>	
												</td>
                                                <td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.lastLoggedIn}" pattern="dd/MM/YYYY hh:mm:ss a"/></td>                                              
                                            </tr>
                                           <%--  </c:forEach> --%>
                                            </c:if>
                                        </tbody>
                                    </table><br>
                                    <div class="form-group">
                                        <div class="col-lg-5 col-lg-offset-3 text-right">
                                            <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> <button class="btn btn-primary" id="submitBtn" type="button" data-toggle="modal" data-target="#modal">Update</button> <button class="btn btn-warning" id="cancelBtn2" type="reset">Cancel</button>
                                        </div>
                                    </div>
                                    
                                    <!-- Add Confirmation Modal -->
									<div class="modal" id="modal" tabindex="-1">
							        <div class="modal-dialog">
							            <div class="modal-content">
							                <div class="modal-header">
							                    <button class="close" data-dismiss="modal" type="button"><span>&times;</span><span class="sr-only">Close</span></button>
												<h2 class="modal-title">Confirmation</h2>
							                </div>						
							                <div class="modal-body" id="add">
							                    Are you sure you want to update user?
							                </div>						
							                <div class="modal-footer">
							                    <button class="btn btn-primary" id="add" type="button">Update</button> <button class="btn btn-warning" data-dismiss="modal" type="button">Close</button> 
							                </div>
							            </div>
							        </div>
	   								</div>
   								<!-- End Confirmation Modal -->
                                </form><!-- #form -->
                            </div><!-- /.resultList -->
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /#page-wrapper -->
    </div>
    <script type="text/javascript">   

    var errorNo = "";
    function validateForm() {        
        if ($('#selectRole').val() == '') {
            errorNo += "<p>Error : Role is required</p>";
        }       
    }

    $(document).ready(function() {
    	 $('#selectLoginId').change(function() {        	
         	var loginId = $("#selectLoginId option:selected").text();
         	 $('#loginId').val(loginId);         	 
         });
    	 
        if(${accessList!=null && accessList.size()>=0}){    
            $('#resultDiv').show();          
          
           var table= $('#tblSearch').DataTable({
            //  responsive: true,   
            bFilter: false,
            bSort: false,
            lengthMenu: [[50, 100], [50, 100]]
            });
        }
        
        $("#cancelBtn1").click(function() {
            $('#searchUserForm').find('.has-error').removeClass("has-error");
           $('#searchUserForm').find('.has-success').removeClass("has-success");
           $('#searchUserForm').find('.form-control-feedback').removeClass('glyphicon-remove');
           $('#searchUserForm').find('.form-control-feedback').removeClass('glyphicon-ok');
           $('.form-group').find('small.help-block').hide(); 
           $('#searchUserForm').formValidation('resetForm', true);
        })
           
      
       $("button#add").click(function() {
        	
        	$('#modal').modal('hide');
             var rows_selected = new Array();
            errorNo = "";
            //console.log("submitBtn was clicked");
            var oTable = $('#tblSearch').dataTable();              
           
             $('#errorMsg').hide();
                
                    var item = $('#id').val();  
                    var loginId = $('#displayLoginId').val();  
                    var role =  $('#selectRole').find("option:selected").val();
                    var status = $("input[type='radio'][name='status']:checked").val();
                    //end                    
                    rows_selected.push({
                            id: item,
                            loginId : loginId,
                            role : role,
                            status:status
                   })   
                 validateForm();
                    
                //console.log("errorNo.length="+errorNo.length);
                if(errorNo.length>0){                   
                    $('#messages').html(errorNo).show();
                    window.scrollTo(0,250);
                    return false;
                }
                    
                    //console.log("validate success:");
                     var token = $("meta[name='_csrf']").attr("content");
                     var header = $("meta[name='_csrf_header']").attr("content");
                     $.ajax({
                        url: "${pageContext.request.contextPath}/admin/updateUser.do",
                         type: "POST",
                         dataType: "json",
                         data: JSON.stringify(rows_selected),                   
                         beforeSend: function(xhr) {
                             xhr.setRequestHeader(header, token);
                             xhr.setRequestHeader("Accept", "application/json");
                             xhr.setRequestHeader("Content-Type", "application/json");
                             sebApp.showIndicator();
                         },
                         success: function(data) {                           
                             if(data.success!=null && data.success.length>0){
                                 $('#successMsg').html(data.success).show();
                             }
                             else if(data.error!=null && data.error.length>0){   
                                 $('#errorMsg').html(data.error).show();
                             } 
                             
                             window.scrollTo(0,5);
                             rows_selected = new Array();
                             $('#resultDiv').hide();
                             
                         },                         
                         failure: function(data) {alert("Fail To Connect");},
                         complete: function(){
                             sebApp.hideIndicator();
                         }
                     });
                
                return true;
        });
        
       
     
        $('#searchUserForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
            	selectLoginId: {
                    validators: {
                        callback: {
                            message: 'Please Select Login ID',
                            callback: function(value, validator, $field) {
                                var options = validator.getFieldElements('selectLoginId').val();    
                                return (options != null && options.length >0);
                            }
                        }
                    }
                }
            }
        });
    });
    </script>
</body>
</html>