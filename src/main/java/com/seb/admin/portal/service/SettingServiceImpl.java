package com.seb.admin.portal.service;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.seb.admin.portal.dao.SettingDAO;
import com.seb.admin.portal.model.Setting;

@Service
public class SettingServiceImpl implements SettingService {

	@Autowired
	private SettingDAO settingDAO;

	@Override
	public Setting findByKey(String key)  throws SQLException {
		return settingDAO.findByKey(key) ;
	}

	@Override
	@Transactional
	public boolean updateSetting(Setting s0) throws SQLException {
		return settingDAO.updateSetting(s0);
	}



}
