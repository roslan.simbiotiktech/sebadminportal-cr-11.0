package com.seb.admin.portal.service;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.seb.admin.portal.model.ContactUs;
import com.seb.admin.portal.model.CustServiceCounter;
import com.seb.admin.portal.model.CustServiceCounterEdited;
import com.seb.admin.portal.model.CustServiceLocation;
import com.seb.admin.portal.model.Faq;
import com.seb.admin.portal.model.FaqCategory;
import com.seb.admin.portal.model.News;
import com.seb.admin.portal.model.PowerAlert;
import com.seb.admin.portal.model.PowerAlertType;
import com.seb.admin.portal.model.TagNotification;
import com.seb.admin.portal.model.TariffCategory;
import com.seb.admin.portal.model.TariffSetting;

public interface MaintenanceService {
	public List <String> findAllRegions() throws SQLException;
	public List <CustServiceLocation> findAllStation(String region) throws SQLException;
	public List <CustServiceLocation> findAllLocation() throws SQLException;
	public CustServiceLocation findCustServiceLocationById(int id);
	public List<CustServiceCounter> findCounterByStatus(String status, String category) throws SQLException;
	public List<CustServiceCounter> findMainLocatorByStatus(String status1, String status2) throws SQLException;
	public List<CustServiceCounterEdited> findSubLocatorByStatus (String status,int counterId)throws SQLException;

	public List<CustServiceLocation> findAll()throws SQLException ;
	public List<CustServiceLocation> findAllStation() throws SQLException;
	public List<CustServiceCounter> findAllCounterByLocId(int id) throws SQLException;
	public CustServiceCounter findCustServiceCounterById(int id) throws SQLException;
	public CustServiceCounterEdited findCustServiceCounterEditedById(int id) throws SQLException;
	public boolean updateEditedMakerCSC(CustServiceCounterEdited e0) throws SQLException ; 
	public boolean deleteEditedCSC(int id) throws SQLException;

	public void saveCSC(CustServiceCounter c0)  throws SQLException;
	public void saveEditedCSC(CustServiceCounterEdited c0) throws SQLException;
	public boolean updateCSCByMaker(CustServiceCounter c0);	
	boolean updateCSCCustomByMaker(CustServiceCounter c0) throws SQLException;
	public boolean updateCSCByChecker(CustServiceCounter c0)  throws SQLException;
	public boolean updateAprovedEditedCSCByChecker(CustServiceCounter c0) throws SQLException;

	//news
	public List<News> findNews(Date dateFrom, Date dateTo, String title) throws SQLException;
	public News findNewsById(int id) throws SQLException;
	public List<News> findNewsTitleByEndDate() throws SQLException;
	public List<News> findNewsIds(List<Integer> ids) throws SQLException;

	public News saveNews(News news) throws SQLException;
	public News updateNews(News n0) throws SQLException;

	//Outage Alert
	public List<PowerAlertType> findPowerAlertType() throws SQLException;
	public PowerAlertType findPowerAlertTypeById(int id)  throws SQLException;
	public List<PowerAlert> findAllOutageByTypeId(Date startDate, Date endDate, int powerTypeId) throws SQLException;
	public List<PowerAlert> findAllPreventiveByTypeId(Date startDate, Date endDate, int powerTypeId) throws SQLException;
	//public List<PowerAlert> findAllPowerAlertByCondition(Date startDate, Date endDate, int powerTypeId) throws SQLException;
	public PowerAlert findPowerAlertById(int id) throws SQLException;
	public List<PowerAlert> findAllOutageContent() throws SQLException;
	public List<PowerAlert> findAllPreventiveContent() throws SQLException;
	public List<PowerAlert> findAllOutagePowerAlertByIds(List<Integer> ids) throws SQLException;
	public List<PowerAlert> findAllPreventivePowerAlertByIds(List<Integer> ids) throws SQLException;
	public PowerAlert findAllPowerAlertByCondition2ById(int id)   throws SQLException;

	public PowerAlert savePowerAlert(PowerAlert p0)  throws SQLException;
	public PowerAlert updatePowerAlert(PowerAlert p0) throws SQLException;	

	//push notification
	public List<TagNotification> findAllPNByTitleId (int titleId) throws SQLException;
	public List<Integer> findPNDistinctTitleIdBtwDate(String notifType, String tagName) throws SQLException;
	public List<Integer> findDistinctTitleIdBtwDatePower(String notifType, String tagName, String powerType)
			throws SQLException;
	public List<TagNotification> findAllFreeTextByIds() throws SQLException;
	public List<TagNotification> findAllByCondition1(Date startDate, Date endDate, String notifType, 
			Integer associatedId, String message) throws SQLException;
	public TagNotification findPushNotificationById(int id) throws SQLException;
	public List<TagNotification> findContentByAssIdAndType(Integer associatedId, String notificationType) throws SQLException;
	public List<Integer> findSubTitleIdBtwDate(Date startDate, Date endDate, String notifType, String tagName)
			throws SQLException;
	public List<Integer> findSubTitleIdBtwDatePower(Date startDate, Date endDate, String notifType, String tagName,
			String powerType) throws SQLException;

	public TagNotification updatePushNotification(TagNotification p0);
	public TagNotification savePushNotication(TagNotification p0) throws SQLException;

	//FAQ
	public List<FaqCategory> findAllFaqCategory() throws SQLException;
	public FaqCategory findFaqCategoryById(int id) throws Exception;
	public List<Faq> findWithOrderByQuestEn(int catId, String status) throws SQLException;
	public List<Faq> findAllFaqById(int id) throws SQLException;
	public List<Faq> findActiveQuesEnOrderBySequence(int catId) throws SQLException;
	public Long findNextSeq(int catId) throws SQLException;

	public Faq saveFAQ(Faq f0)  throws SQLException; 
	public boolean updateFAQ(Faq f0, boolean updateSeq) throws SQLException;

	//contact us
	public ContactUs findAllContact() throws SQLException;	
	public boolean updateContact(ContactUs contact) throws SQLException;
	
	//tariff settings
	public TariffSetting saveTariffSettings(TariffSetting ts) throws SQLException;
	public TariffCategory findTariffCategoryById(int id) throws SQLException;
	public List<TariffCategory> findAllTariffCategory() throws SQLException;
	public List<TariffSetting> findAllTariffSettings(int catId) throws SQLException;
	public int deleteTariffSettingByCatId(int catId) throws SQLException;



	
	
	



}
