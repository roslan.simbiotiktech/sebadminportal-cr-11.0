package com.seb.admin.portal.model;

import java.math.BigDecimal;

public class SEBBillPayment {
	
	private int spgPaymentId;
	
	private String spgMerchantTransactionId;
	
	private BigDecimal spgAmount;
	
	private String spgGatewayTransactionId;
		
	private int paymentId;

	private BigDecimal amount;

	private String bankId;

	private String contractAccountNumber;

	private String createdDatetime;

	private String gatewayId;

	private String gatewayRef;

	private String gatewayStatus;

	private int isMobile;

	private String paymentDatetime;

	private String status;

	private String updatedDatetime;

	private String userEmail;
	
	
	private String spgBankID;
	
	private String spgBankName;
	
	private String spgBankType;
	
	private String spgCardType;
	
	private String referencePrefix;
	
	private String notificationEmail;

	public SEBBillPayment(int spgPaymentId, String spgMerchantTransactionId, BigDecimal spgAmount,
			String spgGatewayTransactionId, int paymentId, BigDecimal amount, String bankId,
			String contractAccountNumber, String createdDatetime, String gatewayId, String gatewayRef,
			String gatewayStatus, int isMobile, String paymentDatetime, String status, String updatedDatetime,
			String userEmail, String spgBankID, String spgBankName, String spgBankType,
			String spgCardType, String referencePrefix, String notificationEmail) {
		super();
		this.spgPaymentId = spgPaymentId;
		this.spgMerchantTransactionId = spgMerchantTransactionId;
		this.spgAmount = spgAmount;
		this.spgGatewayTransactionId = spgGatewayTransactionId;
		this.paymentId = paymentId;
		this.amount = amount;
		this.bankId = bankId;
		this.contractAccountNumber = contractAccountNumber;
		this.createdDatetime = createdDatetime;
		this.gatewayId = gatewayId;
		this.gatewayRef = gatewayRef;
		this.gatewayStatus = gatewayStatus;
		this.isMobile = isMobile;
		this.paymentDatetime = paymentDatetime;
		this.status = status;
		this.updatedDatetime = updatedDatetime;
		this.userEmail = userEmail;
		this.spgBankID = spgBankID;
		this.spgBankName = spgBankName;
		this.spgBankType = spgBankType;
		this.spgCardType = spgCardType;
		this.referencePrefix = referencePrefix;
		this.notificationEmail = notificationEmail;
	}
	
	

	public int getSpgPaymentId() {
		return spgPaymentId;
	}

	public void setSpgPaymentId(int spgPaymentId) {
		this.spgPaymentId = spgPaymentId;
	}

	public String getSpgMerchantTransactionId() {
		return spgMerchantTransactionId;
	}

	public void setSpgMerchantTransactionId(String spgMerchantTransactionId) {
		this.spgMerchantTransactionId = spgMerchantTransactionId;
	}

	public BigDecimal getSpgAmount() {
		return spgAmount;
	}

	public void setSpgAmount(BigDecimal spgAmount) {
		this.spgAmount = spgAmount;
	}

	public String getSpgGatewayTransactionId() {
		return spgGatewayTransactionId;
	}

	public void setSpgGatewayTransactionId(String spgGatewayTransactionId) {
		this.spgGatewayTransactionId = spgGatewayTransactionId;
	}

	public int getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getContractAccountNumber() {
		return contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}

	public String getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(String createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getGatewayRef() {
		return gatewayRef;
	}

	public void setGatewayRef(String gatewayRef) {
		this.gatewayRef = gatewayRef;
	}

	public String getGatewayStatus() {
		return gatewayStatus;
	}

	public void setGatewayStatus(String gatewayStatus) {
		this.gatewayStatus = gatewayStatus;
	}

	public int getIsMobile() {
		return isMobile;
	}

	public void setIsMobile(int isMobile) {
		this.isMobile = isMobile;
	}

	public String getPaymentDatetime() {
		return paymentDatetime;
	}

	public void setPaymentDatetime(String paymentDatetime) {
		this.paymentDatetime = paymentDatetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(String updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getSpgBankID() {
		return spgBankID;
	}

	public void setSpgBankID(String spgBankID) {
		this.spgBankID = spgBankID;
	}

	public String getSpgBankName() {
		return spgBankName;
	}

	public void setSpgBankName(String spgBankName) {
		this.spgBankName = spgBankName;
	}

	public String getSpgBankType() {
		return spgBankType;
	}

	public void setSpgBankType(String spgBankType) {
		this.spgBankType = spgBankType;
	}

	public String getSpgCardType() {
		return spgCardType;
	}

	public void setSpgCardType(String spgCardType) {
		this.spgCardType = spgCardType;
	}

	public String getReferencePrefix() {
		return referencePrefix;
	}

	public void setReferencePrefix(String referencePrefix) {
		this.referencePrefix = referencePrefix;
	}

	public String getNotificationEmail() {
		return notificationEmail;
	}

	public void setNotificationEmail(String notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	
}
