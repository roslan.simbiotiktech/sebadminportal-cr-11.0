package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.seb.admin.portal.constant.ApplicationConstant;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the TARIFF_SETTING database table.
 * 
 */
@Entity
@Table(name="TARIFF_SETTING", schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="TariffSetting.findAll", query="SELECT t FROM TariffSetting t"),
	@NamedQuery(name="TariffSetting.findAllByCategoryId", query="SELECT t FROM TariffSetting t WHERE t.tariffCategoryId =:catId AND t.deletedFlag=0"),
	@NamedQuery(name="TariffSetting.deleteTariffSettingByCatId", query="DELETE FROM TariffSetting t WHERE t.tariffCategoryId =:catId AND t.deletedFlag=0")	

})
public class TariffSetting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;

	@Column(name="CREATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="DELETED_FLAG")
	private int deletedFlag;

	@Column(name="EXEMPTED_GST_FIRST_KWH")
	private int exemptedGstFirstKwh;

	@Column(name="KW_MAX_DEMAND_AMOUNT")
	private BigDecimal kwMaxDemandAmount;

	@Column(name="LOW_POWER_FACTOR")
	private int lowPowerFactor;

	@Column(name="MIN_MONTHLY_CHARGE_AMOUNT")
	private BigDecimal minMonthlyChargeAmount;

	@Column(name="NON_PEAK_AMOUNT")
	private BigDecimal nonPeakAmount;

	private int peak;

	@Column(name="PEAK_AMOUNT")
	private BigDecimal peakAmount;

	@Column(name="TARIFF_CATEGORY_ID")
	private int tariffCategoryId;

	@Column(name="TR_AMOUNT_PER_UNIT")
	private BigDecimal trAmountPerUnit;

	@Column(name="TR_FROM_UNIT")
	private int trFromUnit;

	@Column(name="TR_MORE_AMOUNT")
	private BigDecimal trMoreAmount;

	@Column(name="TR_MORE_UNIT")
	private int trMoreUnit;

	@Column(name="TR_TO_UNIT")
	private int trToUnit;

	@Column(name="UPDATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp updatedDatetime;
	
	@JoinColumn(name = "TARIFF_CATEGORY_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private TariffCategory tariffCategory;

	public TariffSetting() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public int getExemptedGstFirstKwh() {
		return this.exemptedGstFirstKwh;
	}

	public void setExemptedGstFirstKwh(int exemptedGstFirstKwh) {
		this.exemptedGstFirstKwh = exemptedGstFirstKwh;
	}

	public BigDecimal getKwMaxDemandAmount() {
		return this.kwMaxDemandAmount;
	}

	public void setKwMaxDemandAmount(BigDecimal kwMaxDemandAmount) {
		this.kwMaxDemandAmount = kwMaxDemandAmount;
	}

	public int getLowPowerFactor() {
		return this.lowPowerFactor;
	}

	public void setLowPowerFactor(int lowPowerFactor) {
		this.lowPowerFactor = lowPowerFactor;
	}

	public BigDecimal getMinMonthlyChargeAmount() {
		return this.minMonthlyChargeAmount;
	}

	public void setMinMonthlyChargeAmount(BigDecimal minMonthlyChargeAmount) {
		this.minMonthlyChargeAmount = minMonthlyChargeAmount;
	}

	public BigDecimal getNonPeakAmount() {
		return this.nonPeakAmount;
	}

	public void setNonPeakAmount(BigDecimal nonPeakAmount) {
		this.nonPeakAmount = nonPeakAmount;
	}

	public int getPeak() {
		return this.peak;
	}

	public void setPeak(int peak) {
		this.peak = peak;
	}

	public BigDecimal getPeakAmount() {
		return this.peakAmount;
	}

	public void setPeakAmount(BigDecimal peakAmount) {
		this.peakAmount = peakAmount;
	}

	public int getTariffCategoryId() {
		return this.tariffCategoryId;
	}

	public void setTariffCategoryId(int tariffCategoryId) {
		this.tariffCategoryId = tariffCategoryId;
	}

	public BigDecimal getTrAmountPerUnit() {
		return this.trAmountPerUnit;
	}

	public void setTrAmountPerUnit(BigDecimal trAmountPerUnit) {
		this.trAmountPerUnit = trAmountPerUnit;
	}

	public int getTrFromUnit() {
		return this.trFromUnit;
	}

	public void setTrFromUnit(int trFromUnit) {
		this.trFromUnit = trFromUnit;
	}

	public BigDecimal getTrMoreAmount() {
		return this.trMoreAmount;
	}

	public void setTrMoreAmount(BigDecimal trMoreAmount) {
		this.trMoreAmount = trMoreAmount;
	}

	public int getTrMoreUnit() {
		return this.trMoreUnit;
	}

	public void setTrMoreUnit(int trMoreUnit) {
		this.trMoreUnit = trMoreUnit;
	}

	public int getTrToUnit() {
		return this.trToUnit;
	}

	public void setTrToUnit(int trToUnit) {
		this.trToUnit = trToUnit;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	@JsonBackReference //for parent table
	public TariffCategory getTariffCategory() {
		return tariffCategory;
	}

	public void setTariffCategory(TariffCategory tariffCategory) {
		this.tariffCategory = tariffCategory;
	}}