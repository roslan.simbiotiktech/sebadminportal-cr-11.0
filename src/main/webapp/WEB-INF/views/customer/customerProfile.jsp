<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="" name="description">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
<style type="text/css">
/* .modal-body {
    max-height: calc(100vh - 212px);
    overflow-y: auto;
} */
/* table tbody tr.even{
    color:#444;
} */
/* table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {
  background-color: #f9f9f9;
} */
.myodd {
	background-color: #F9F9F9 !important;
}

.test[style] {
	padding-right: 0 !important;
}

.test.modal-open {
	overflow: auto;
}
</style>
</head>

<body style="padding-right: 0px !important">
	<div id="wrapper">
		<!-- Navigation -->

		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="font-weight: bold">Customer &gt; Profile</div>

						<div class="panel-body">
							<form id="profileForm" name="profileForm" method="post" action="${pageContext.request.contextPath}/admin/customerProfile.do"
								class="form-horizontal">
								${successMsg}${errorMsg}
								<div class="form-group required">
									<p class="col-lg-5 control-label">
										Search By <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<select class="form-control" id="selectProfile" name="selectProfile">
											<option value="">Please Select</option>
											<option value="0">All</option>
											<option value="contractAccNo">Contract Account Number</option>
											<option value="email">Email</option>
											<option value="loginID">LoginID</option>
											<option value="msisdn">Mobile Number</option>
											<option value="name">Name</option>
										</select>
									</div>
								</div>

								<div class="form-group" id="msisdnDiv" style="display: none;">
									<p class="col-lg-5 control-label">
										Mobile Number <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="msisdn" maxlength="20" name="msisdn" placeholder="msisdn" type="text">
									</div>
								</div>

								<div class="form-group" hidden="" id="nameDiv">
									<p class="col-lg-5 control-label">
										Name <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="name" maxlength="160" name="name" placeholder="Name" type="text">
									</div>
								</div>

								<div class="form-group" hidden="" id="loginIDDiv">
									<p class="col-lg-5 control-label">
										Login ID <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="loginID" maxlength="50" name="loginID" placeholder="Login ID" type="text">
									</div>
								</div>

								<div class="form-group" hidden="" id="contractAccNoDiv">
									<p class="col-lg-5 control-label">
										Contract Account Number <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="contractAccNo" maxlength="12" name="contractAccNo" placeholder="Contract Account Number" type="text">
									</div>
								</div>

								<div class="form-group" hidden="" id="emailDiv">
									<p class="col-lg-5 control-label">
										Email <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="email" maxlength="50" name="email" placeholder="Email" type="text">
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> <input type="hidden" id="displayType" name="displayType"
											value="" />
										<button class="btn btn-primary" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
							</form>
							<div id="resultDiv" style="display: none; margin-top: 10%">
								<form action="${pageContext.request.contextPath}/admin/auditTrails.do?${_csrf.parameterName}=${_csrf.token}" class="form-horizontal"
									id="viewATForm" method="post" name="viewATForm">
									<h4>
										Search Results : ${label} :<b>${myvalue}</b>
									</h4>
									<div class="form-group">
										<div class="alert alert-danger" id="messages" role="alert" style="display: none;"></div>
									</div>

									<table cellspacing="0" class="table table-striped table-bordered" id="tblSearch" style="max-width: 100%;">
										<thead>
											<tr>
												<th>No.</th>
												<th>Login ID</th>
												<th>Mobile Number</th>
												<th>Last Login Date</th>
												<!-- <th>Profile Status</th> -->
												<th>Contract Account Number</th>
												<th>Contract Account Name</th>
												<th>Account Status</th>

											</tr>
											<!-- #viewATForm -->
											<!-- #resultDiv -->
										</thead>
										<tbody>
										</tbody>
									</table>
								</form>
								<form id="deleteModalForm" class="form-horizontal" role="form">
									<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h4 class="modal-title" id="myModalLabel">Subscription Details</h4>
												</div>
												<div id="profileDetails" class="modal-body"></div>
												<div class="modal-footer">
													<div id="deleteDiv" style="display: none;" class="btn-group">
														<button type="button" class="btn btn-danger" data-toggle="modal" href="#stack2">Delete</button>
													</div>
													<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>

												</div>
											</div>
										</div>
									</div>
									<!-- #modal 1 -->
									<div class="modal" id="stack2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button class="close" data-dismiss="modal" type="button">
														<span>&times;</span><span class="sr-only">Close</span>
													</button>
													<h2 class="modal-title">Confirmation</h2>
												</div>
												<div class="modal-body" id="deleteAlert">
													<div class="form-group">
														<div class="col-sm-12">
															<p id="bodyContent">Please enter the reason for the deletion of Contract Account Number [].</p>
														</div>
														<div class="col-sm-12">
															<input type="text" maxlength="400" class="form-control" id="remark" name="remark" autofocus="autofocus" placeholder="Please enter remark for reference" />
														</div>
													</div>
												</div>
												<!-- #deleteAlert -->
												<div id="storeDiv"></div>
												<div class="modal-footer">
													<button class="btn btn-primary" id="deleteBtn" type="submit" >Proceed</button>
													<button class="btn btn-warning" data-dismiss="modal" type="button">Cancel</button>
												</div>
											</div>
										</div>
									</div>
									<!-- #stack2 -->
								</form>
								<!-- modal show when click account -->
								<div class="modal" id="informModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title" id="myModalLabel">Subscription Details</h4>
											</div>
											<div id="informDetails" class="modal-body"></div>
											<div class="modal-footer">
												<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- #modal show when click account -->
								<!-- modal show after success/fail update profile -->
								<div class="modal" id="inform2Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title" id="myModalLabel">Profile Details</h4>
											</div>
											<div id="inform2DetailModal" class="modal-body"></div>
											<div class="modal-footer">
												<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- #modal show after success/fail update profile -->
								<!-- #modal show when click loginid -->
									<form id="viewLoginForm" class="form-horizontal" role="form">
									<div class="modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h4 class="modal-title" id="myModalLabel">Profile Details</h4>
												</div>
												<div id="viewLoginDetails"  class="modal-body"><div class="form-group"><p class="col-lg-6 control-label">Login ID :</p><div class="col-lg-4 control-label" style="text-align: left;"><label class ="lblNoBoldCls" id="mLoginId"  name="mLoginId"/></div></div><div class="form-group"><p class="col-lg-6 control-label">NRIC/Passport :</p><p class="col-lg-4 control-label" style="text-align: left;"><label class ="lblNoBoldCls" id="mIC"  name="mIC"/></p></div><div class="form-group"><p class="col-lg-6 control-label">Email :</p><p class="col-lg-4 control-label" style="text-align: left;"><label class ="lblNoBoldCls" id="mEmail"  name="mEmail"/></p></div><div class="form-group"><p class="col-lg-6 control-label">Mobile Number :</p><p class="col-lg-4 control-label" style="text-align: left;"><label class ="lblNoBoldCls" id="mMsisdn"  name="mMsisdn"/></p></div><div class="form-group"><p class="col-lg-6 control-label">Home Telephone :</p><p class="col-lg-4 control-label" style="text-align: left;"><label class ="lblNoBoldCls" id="mTelNo"  name="mTelNo"/></p></div><div class="form-group hide"><p class="col-lg-6 control-label">Preferred Communication Method :</p><div class="col-lg-4 control-label" style="text-align: left;"><select class="form-control" id="selectPreferredMethod" name="selectPreferredMethod"><c:forEach items="${comMethodList}" var="val"> <option value="${val}">${val}</option> </c:forEach></select></div></div>
												<div style="" class="form-group">
												<p class="col-lg-6 control-label">Profile Status :</p>
												<div class="col-lg-4 control-label" style="text-align: left;"><select class="form-control" id="selectProfileStatus" name="selectProfileStatus"><c:forEach items="${userStatusList}" var="val"> <option value="${val}">${val}</option> </c:forEach></select></div></div><div class="form-group"><p class="col-lg-6 control-label">Remark :</p><p class="col-lg-4 control-label" style="text-align: left;"><textarea class="form-control" cols="23" id="mRemark" name="mRemark" maxlength="100" rows="5" ></textarea></p></div><div class="form-group"><p class="col-lg-6 control-label">Last Login Date :</p><p class="col-lg-4 control-label" style="text-align: left;"><label class ="lblNoBoldCls" id="mLastLoginAt"  name="mLastLoginAt"/></p></div></div>
												<div class="modal-footer">
													<div id="submitDiv" class="btn-group">
														<button id="updateProfileBtn"  type="submit" class="btn btn-primary" data-toggle="modal">Update</button>
													</div>
													<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>

												</div>
											</div>
										</div>
									</div>
									
									<!-- # modal show when click loginid -->
									<!-- Add Update(Preferred Method) Confirmation Modal -->
									<div class="modal" id="stack3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button class="close" data-dismiss="modal" type="button">
														<span>&times;</span><span class="sr-only">Close</span>
													</button>
													<h2 class="modal-title">Confirmation</h2>
												</div>
												<div class="modal-body" id="updateProfileAlert">
												</div>
												<div class="modal-footer">
												<div id="passDataDiv"></div>
													<button class="btn btn-primary" id="submitProfile" type="button">Update</button>
													<button class="btn btn-warning" data-dismiss="modal" type="button">Cancel</button>
												</div>
											</div>
										</div>
									</div>
									<!-- End Add Update(Preferred Method) Confirmation Modal -->
								</form>
							</div>
							<!-- /.resultList -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
	function resetForm() {
    $('#').find('.has-error').removeClass("has-error");
    $('#profileForm').find('.has-success').removeClass("has-success");
    $('#profileForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    $('#profileForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    $('#profileForm').formValidation('resetForm', true);
    
    $('.form-group').find('small.help-block').hide();
    $('.form-group').removeClass('has-error has-feedback');
    $('.form-group').find('i.form-control-feedback').hide();
    return false;
}
var myAcc;

function viewProfile(uni, accNo, loginId) {
    //console.log('accNo=' + accNo);
    //console.log('loginId=' + loginId);
    myAcc = uni;
    console.log("myAcc >>"+myAcc);
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $.ajax({
        url: "${pageContext.request.contextPath}/admin/getProfileDetails.do",
        type: "POST",
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            loginId: loginId,
            contractAccNo: accNo
        }),
        beforeSend: function(xhr) {
            xhr.setRequestHeader(header, token);
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(data) {
            //console.log("data.loginId>>"+data.loginId);
            if(data.contractDetail.subscriptionSatus == "ACTIVE"){
            	$('#deleteDiv').show();
            	$('#bodyContent').html("Please enter the reason for the deletion of Contract Account Number [<b>"+data.contractDetail.accountNumber+"</b>].");
            	$('#storeDiv').html('<input type="hidden" id="txtAccNo" value="'+data.contractDetail.accountNumber+'"/><input type="hidden" id="txtEmail" value="'+data.email+'"/><input type="hidden" id="txtStatus" value="'+data.contractDetail.subscriptionSatus+'"/>');
              	$('#profileDetails').html('<form action="" class="form-horizontal" id="subDetailsForm" name="subDetailsForm"><div class="form-group"><p class="col-lg-6 control-label">Login ID :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.loginId + '</p></div><div class="form-group"><p class="col-lg-6 control-label">NRIC/Passport :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.nricOrPassport + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Email :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.email + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Mobile Number :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.mobileNumber + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Home Telephone :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.homeTel + '</p></div><div class="form-group hide"><p class="col-lg-6 control-label">Preferred Communication Method :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.preferredCommMethod + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Contract Account Number :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.contractDetail.accountNumber + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Contract Account Name :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.contractDetail.accountName + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Contract Account Nickname :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.contractDetail.accountNick + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Subscription Status :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.contractDetail.subscriptionSatus + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Date Subscribed :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.contractDetail.subscribedAt + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Last Login Date :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.lastLoginAt + '</p></div></form>');
            }else{
            	 $('#deleteDiv').hide();
               	$('#profileDetails').html('<form action="" class="form-horizontal" id="subDetailsForm" name="subDetailsForm"><div class="form-group"><p class="col-lg-6 control-label">Login ID :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.loginId + '</p></div><div class="form-group"><p class="col-lg-6 control-label">NRIC/Passport :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.nricOrPassport + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Email :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.email + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Mobile Number :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.mobileNumber + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Home Telephone :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.homeTel + '</p></div><div class="form-group hide"><p class="col-lg-6 control-label">Preferred Communication Method :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.preferredCommMethod + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Contract Account Number :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.contractDetail.accountNumber + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Contract Account Name :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.contractDetail.accountName + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Contract Account Nickname :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.contractDetail.accountNick + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Subscription Status :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.contractDetail.subscriptionSatus + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Remark :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.contractDetail.remark + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Date Subscribed :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.contractDetail.subscribedAt + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Last Login Date :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.lastLoginAt + '</p></div></form>');
            }
        },
        failure: function(data) {
            alert("fail");
        },
        complete: function(e) {
            sebApp.hideIndicator();
        }
    });
}


function viewLogin(uni, loginId) {
	$('#viewLoginForm').formValidation('resetForm', true);
    myAcc = uni;
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    $.ajax({
        url: "${pageContext.request.contextPath}/admin/getProfileDetails.do",
        type: "POST",
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            loginId: loginId,
            contractAccNo: ''
        }),
        beforeSend: function(xhr) {
            xhr.setRequestHeader(header, token);
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(data) {
        	//data.accountStatus = "TERMINATE";
            	//$('#viewLoginDetails').html('<div class="form-group"><p class="col-lg-6 control-label">Login ID :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.loginId + '</p></div><div class="form-group"><p class="col-lg-6 control-label">NRIC/Passport :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.nricOrPassport + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Email :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.email + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Mobile Number :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.mobileNumber + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Home Telephone :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.homeTel + '</p></div><div class="form-group"><p class="col-lg-6 control-label">Preferred Communication Method :</p><div class="col-lg-4 control-label" style="text-align: left;"><select class="form-control" id="selectPreferredMethod" name="selectPreferredMethod"><c:forEach items="${comMethodList}" var="val"> <option value="${val}">${val}</option> </c:forEach></select></div></div><div class="form-group"><p class="col-lg-6 control-label">Profile Status :</p><div class="col-lg-4 control-label" style="text-align: left;"><select class="form-control" id="selectProfileStatus" name="selectProfileStatus"><c:forEach items="${userStatusList}" var="val"> <option value="${val}">${val}</option> </c:forEach></select></div></div><div class="form-group"><p class="col-lg-6 control-label">Remark :</p><p class="col-lg-4 control-label" style="text-align: left;"><textarea class="form-control" cols="23" id="loginRemark" name="loginRemark" maxlength="100" rows="5" >'+data.remark+'</textarea><input type="hidden" id="loginId" value="'+data.loginId+'"/></p></div><div class="form-group"><p class="col-lg-6 control-label">Last Login Date :</p><p class="col-lg-4 control-label" style="text-align: left;">' + data.lastLoginAt + '</p></div>');
            	$("#selectPreferredMethod").val(data.preferredCommMethod).change();
            	$("#selectProfileStatus").val(data.accountStatus).change();
            	//$( "#submitDiv" ).hide();
            	
            	$("#mLoginId").html(data.loginId);
            	$("#mIC").html(data.nricOrPassport);
            	$("#mEmail").html(data.email);
            	$("#mMsisdn").html(data.mobileNumber);
            	$("#mTelNo").html(data.homeTel);
            	$("#mLastLoginAt").html(data.lastLoginAt);
            	$("#mRemark").html(data.remark);
          		$('#updateProfileAlert').html("Are you sure you want to update [<b>"+data.loginId+"</b>] profile?");
          		//data-target=\"#loginModal\" 
          		
          		$('#loginModal').modal('show');
        },
        failure: function(data) {
            alert("fail");
        },
        complete: function(e) {
            sebApp.hideIndicator();
        }
    });
}

$(document).ready(function() {
	var count=0;
	var innerCount=0;
    if (${userList != null && userList.size() >= 0}) {
        $('#resultDiv').show();
        var columns = [{
            "mDataProp": "loginId"
        }, {
            "mDataProp": "loginId",
            "mRender": function(data, type, row) {
                var html = '';
                if (data != null) {
                        html += "<p><a data-toggle=\"modal\" onclick=\"viewLogin('"+(count+=1)+"','" + row.loginId + "')\">" + data + "</a></p>";
                    html += "";
                }
                return html;
            }
        }, {
            "mDataProp": "mobileNumber"
        }, {
            "mDataProp": "lastLoginAt"
        },/* {
            "mDataProp": "status"
        }, */ {
            "mDataProp": "contractSubscribed",
            "mRender": function(data, type, row) {
                var html = '';
                if (data != null) {
                    html += "";  
                    $.each(data, function(index, currEmp) {
                        html += "<p><a data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"viewProfile('"+(count+=1)+"','" + currEmp.accountNumber + "','" + row.loginId + "')\">" + currEmp.accountNumber + "</a></p>";
                    });
                    html += "";
                }
                return html;
            }
        }, {
            "mDataProp": "contractSubscribed",
            "mRender": function(data) {
                var html = '';
                if (data != null) {
                    $.each(data, function(index, currEmp) {
                        html += "<p>" + currEmp.accountName + "</p>";
                    });
                    
                }
                return html;
            }
        }, {
            "mDataProp": "contractSubscribed",
            "mRender": function(data) {
                var html = '';
                if (data != null) {
                    $.each(data, function(index, currEmp) {
                        html += '<p id="xxx'+(innerCount+=1) +'">'+currEmp.subscriptionStatus + '</p>';
                    });
                    html += "";
                }
                return html;
            }
        }];
        var table = $('#tblSearch').DataTable({
            //  responsive: true,   
            bFilter: false,
            bSort: false,
            lengthMenu: [
                [50],
                [50]
            ],
            "bProcesing": true,
            "bServerSide": true,
            "bLengthChange": false,
            "iDisplayStart": 0,
            "sEmptyTable": "No data available in table",
            "sAjaxSource": "${pageContext.request.contextPath}/admin/searchProfileTable.do?${_csrf.parameterName}=${_csrf.token}&name=${name}&email=${email}&msisdn=${msisdn}&contractAccNo=${contractAccNo}&&loginId=${loginId}",
            "aoColumns": columns,
            /* [
             {
                 "mDataProp": "activityAt"
             }, {
                 "mDataProp": "mobileNumber"
             }, {
                 "mDataProp": "mobileNumber"
             }, {
                 "mDataProp": "loginId"
             }, {
                 "mDataProp": "activity"
             },
         ], */
            "fnServerData": function(sSource, aoData, fnCallback) {
                $.ajax({
                    beforeSend: function(xhr) {
                        sebApp.showIndicator();
                    },
                    dataType: 'json',
                    type: "POST",
                    url: sSource,
                    data: aoData,
                    success: function(data, textStatus, jqXHR) {
                        fnCallback(data, textStatus, jqXHR);
                    },
                    failure: function(data) {},
                    complete: function(e) {
                        sebApp.hideIndicator();
                    }
                });
            },
            "rowCallback": function(row, data, iDisplayIndex) {
                var info = table.page.info();
                var page = info.page;
                var length = info.length;
                var index = (page * length + (iDisplayIndex + 1));
                $('td:eq(0)', row).html(index);
               
            },
           
        });
    }
    $('#myModal').on('show.bs.modal', function (e) {
      $('body').addClass('test');
  	})
  	
  	 $('#loginModal').on('show.bs.modal', function (e) {
      $('body').addClass('test');
      $('#viewLoginForm').formValidation('resetForm', true);
      
  	})
  	
  	$('#stack2').on('show.bs.modal', function (e) {
    	$('body').addClass('test');
	})
	$('#informModal').on('show.bs.modal', function (e) {
    	$('body').addClass('test');
	})
	
	$('#inform2Modal').on('show.bs.modal', function (e) {
    	$('body').addClass('test');
	})
	
	$('#stack3').on('show.bs.modal', function (e) {
    	$('body').addClass('test');
	})

	$('#stack2').on('hidden.bs.modal', function() {
	});
    
    $('#loginModal').on('hidden.bs.modal', function() {
    	$('#viewLoginForm').formValidation('resetForm', true);
	});

    
    $('#selectProfile').change(function() {
        var selectedValue = $(this).find("option:selected").val();
        $('#displayType').val($(this).find("option:selected").text());
        if (selectedValue != "") {
            //console.log('fdfdf');
            $('#msisdn').val('');
            $('#name').val('');
            $('#loginID').val('');
            $('#contractAccNo').val('');
            $('#email').val('');
            $('#msisdnDiv').hide();
            $('#nameDiv').hide();
            $('#loginIDDiv').hide();
            $('#contractAccNoDiv').hide();
            $('#emailDiv').hide();
            $("#" + selectedValue + "Div").show();
        } else {
            //console.log('empty');
            $('#msisdnDiv').hide();
            $('#nameDiv').hide();
            $('#loginIDDiv').hide();
            $('#contractAccNoDiv').hide();
            $('#emailDiv').hide();
        }
    });
    $("#msisdn").keypress(function(e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    
     //[start]update profile
    $('#viewLoginForm').formValidation({
       	framework: 'bootstrap',
     	excluded: ':disabled',
     	icon : {
			invalid : 'glyphicon glyphicon-remove',
		},
        fields: {
        	mRemark: {
                validators: {
                    notEmpty: {
                        message: 'Remark is required'
                    },
                    stringLength : {
						min : 1,
						max : 100,
						message : 'Remark must less than 100 characters'
					}
                } 
            }
        }
    }).on('success.form.fv', function(e) {
        // Prevent form submission
		e.preventDefault();
      	//$('#viewLoginForm').formValidation('revalidateField', 'mRemark');
    	$('#passDataDiv').html('<input type="hidden" id="txtLoginId" value="'+$("#mLoginId").text()+'"/><input type="hidden" id="txtPreferredMethod" value="'+$("#selectPreferredMethod").val()+'"/><input type="hidden" id="txtProfileStatus" value="'+$("#selectProfileStatus").val()+'"/><input type="hidden" id="txtRemark" value="'+$("#mRemark").val()+'"/>');
      	
      	
      	var isValidForm = $('#viewLoginForm').data('formValidation').isValid();
      	if(isValidForm){
      		$('#stack3').modal('show'); 
      		
     		 $("button#submitProfile").unbind('click');
			
     		 //ajax submit update profile
            $("button#submitProfile").click(function() {
        			//$('#myModal').modal('hide');   
        			var token = $("meta[name='_csrf']").attr("content");
        			var header = $("meta[name='_csrf_header']").attr("content");
        			$.ajax({
        				url: "${pageContext.request.contextPath}/admin/doEditProfile.do",
        				type: "POST",
        				dataType: "json",
        				data: JSON.stringify({
        					loginId: $('#txtLoginId').val(),
        					preferredCommunicationMethod: $('#txtPreferredMethod').val(),
        					userStatus : $('#selectProfileStatus').val(),
        					remark : $("#txtRemark").val()
        				}),
        				
        				beforeSend: function(xhr) {
        					xhr.setRequestHeader(header, token);
        					xhr.setRequestHeader("Accept", "application/json");
        					xhr.setRequestHeader("Content-Type", "application/json");
        					sebApp.showIndicator();
        				},
        				success: function(data) {
        					console.log("success update.")
        					
        					if(data!=null && data.RESPONSE =="OK"){
        						 console.log("data.RESPONSE ::"+data.RESPONSE);
        						console.log("data.METHOD ::"+data.METHOD);
        						console.log("data.STATUS ::"+data.STATUS);
        						console.log("data.REMARK ::"+data.REMARK);
        						console.log("data.ERROR ::"+data.ERROR); 
        						
        						$('#inform2DetailModal').html("<p>Profile [<b>"+data.LOGIN+"</b>] has been updated.")
        						$('#inform2Modal').modal('show');
        						 $('#tblSearch').DataTable().ajax.reload();
        						
        						
        					}else{
        						console.log("data.RESPONSE ::"+data.RESPONSE);
        						console.log("data.METHOD ::"+data.METHOD);
        						console.log("data.STATUS ::"+data.STATUS);
        						console.log("data.REMARK ::"+data.REMARK);
        						console.log("data.ERROR ::"+data.ERROR);
        						
        						$('#inform2DetailModal').html("<p style='color:red;'>Profile [<b>"+data.LOGIN+"</b>] failed to update.")
        						$('#inform2Modal').modal('show');
        						
        						
        					}
        									
        				},
        				failure: function(data) {
        					alert("Fail To Connect");
        				},
        				complete: function() {
        					sebApp.hideIndicator();
        					$('#loginModal').modal('hide');  
        		  			$('#stack3').modal('hide');
        				}
        			});
        		})
            //end ajax submit update profile
      	}
  });
    //[end] update profile
    
    //[start]deleteModalForm
    $('#deleteModalForm').formValidation({
        framework: 'bootstrap',
       	excluded: ':disabled',
       	icon : {
			invalid : 'glyphicon glyphicon-remove',
		},
        fields: {
            remark: {
                validators: {
                    notEmpty: {
                        message: 'Remark is required'
                    }
                } 
            }
        }
    }).on('success.form.fv', function(e) {
      // Prevent form submission
      e.preventDefault();
      var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");
			$.ajax({
				url: "${pageContext.request.contextPath}/admin/deleteSubscription.do",
				type: "POST",
				dataType: "json",
				data: JSON.stringify({
					email: $('#txtEmail').val(),
					subscriptionStatus: $('#txtStatus').val(),
					accountNumber: $('#txtAccNo').val(),
					remark: $('#remark').val()
					
				}),
				beforeSend: function(xhr) {
					xhr.setRequestHeader(header, token);
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
					sebApp.showIndicator();
				},
				success: function(data) {
				
				if(data!=null && data.success !=""){
					document.getElementById('xxx'+ myAcc).innerHTML = data.success;
					$('#informDetails').html("<p>Contract Account [<b>"+$('#txtAccNo').val()+"</b>] has been deleted.")
					$('#informModal').modal('show');
				}else{
					$('#informDetails').html("<p>Contract Account [<b>"+$('#txtAccNo').val()+"</b>] FAILED to delete.")
					$('#informModal').modal('show');
				}
				
				$('#myModal').modal('hide');  
	  			$('#stack2').modal('hide');
		  		
				},
				failure: function(data) {
					alert("Fail To Connect");
				},
				complete: function() {
					sebApp.hideIndicator();
				}
			}); 
  });
    //[end] deleteModalForm
    
    $('#inform2DetailModal').on('hidden.bs.modal', function() {
    	$('.modal-backdrop').remove();
		});
    
    $('#profileForm').formValidation({
        framework: 'bootstrap',
        icon: {
            invalid: 'glyphicon glyphicon-remove',
        },
        fields: {
            selectProfile: {
                validators: {
                    callback: {
                        message: 'Please Select Search By',
                        callback: function(value, validator, $field) {
                            var options = validator.getFieldElements('selectProfile').val();
                            var text = $("#selectProfile option:selected").text();
                            return (text != null && text != "Please Select");
                        }
                    }
                }
            },
            msisdn: {
                validators: {
                    notEmpty: {
                        message: 'Mobile Number is required'
                    },
                    stringLength: {
                        min: 10,
                        max: 20,
                        message: 'Mobile Number must at least 10 digits'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_]+$/,
                        message: 'The Name can only consist of alphabetical, number and underscore'
                    }
                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: 'Name is required'
                    }
                }
            },
            loginID: {
                validators: {
                    notEmpty: {
                        message: 'LoginID is required'
                    }
                }
            },
            contractAccNo: {
                validators: {
                    notEmpty: {
                        message: 'Contract Account Number is required'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email is required'
                    },
                    emailAddress: {
                        message: 'The value is not a valid email address'
                    }
                }
            }
        }
    });
});
	</script>
</body>
</html>