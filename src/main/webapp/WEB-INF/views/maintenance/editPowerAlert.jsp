<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="${_csrf.token}" name="_csrf">
<meta content="${_csrf.headerName}" name="_csrf_header">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
<title></title>
</head>
<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>
		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>
			<div class="row">
				<div class="col-lg-12" style="width: inherit; min-width: 100%">
					<div class="panel panel-default">
						<div class="panel-heading" style="font-weight: bold">Maintenance &gt; Outage Alert &gt; Edit</div>
						<div class="panel-body">
							<form action="${pageContext.request.contextPath}/admin/editPowerAlert.do" class="form-horizontal" id="searchPAForm" method="post"
								name="searchPAForm">
								${successMsg}${errorMsg}
								<div id="successMsg" style="display: none"></div>
								<div id="errorMsg" style="display: none"></div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Start Date <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='startDate'>
											<input class="form-control datepicker" id="instartDate" name="instartDate" readonly="readonly" type='text'> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										End Date<span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='endDate'>
											<input class="form-control datepicker" id="inendDate" name="inendDate" readonly="readonly" type='text'> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Type <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<select class="form-control capital" id="selectType" name="selectType">
											<option value="">Please Select</option>
											<c:forEach items="${typeList}" var="item">
												<option value="${item.id}">${item.name}</option>
											</c:forEach>
										</select> <input type="hidden" id="powerTypeName" name="powerTypeName" value="" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input id="displayType" name="displayType" type="hidden" value=""> <input id="displayStatus" name="displayStatus" type="hidden"
											value=""> <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" type="reset">Cancel</button>
									</div>
								</div>
							</form>
							<div id="resultDiv" style="display: none; margin-top: 10%">
								<form action="${pageContext.request.contextPath}/admin/editPowerAlert.do" class="form-horizontal" id="updatePowerAlert" method="post"
									name="updatePowerAlert">
									<h4>
										Search Results : Start Date :<b>${startDate}</b>, End Date :<b>${endDate}</b>, Type :<b>${powerTypeName}</b>
									</h4>
									<div class="form-group">
										<div class="alert alert-danger" id="messages" style="display: none;"></div>
									</div>
									<table cellspacing="0" class="table table-striped table-bordered" id="tblSearch">
										<thead>
											<tr>
												<th>No.</th>
												<th>Type</th>
												<th>Station</th>
												<th>Area</th>
												<th>Title</th>
												<th>Causes</th>
												<c:choose>
													<c:when test="${fn:containsIgnoreCase(powerTypeName, 'preventive')}">
														<th>Start Date Time of Maintenance</th>
														<th>End Date Time of Maintenance</th>
													</c:when>
													<c:otherwise>
														<th>Estimated Restoration Time</th>
													</c:otherwise>
												</c:choose>
												<th>Push Notification</th>
												<th>Select</th>
											</tr>
										</thead>
										<tbody>
											<c:if test="${not empty outageList}">
												<c:forEach items="${outageList}" var="item" varStatus="status">
													<fmt:formatDate var="dateNow" type="both" dateStyle="long" value="<%=new java.util.Date()%>" pattern="yyyy-MM-dd HH:mm:ss" />
													<fmt:formatDate var="restoreDate" type="both" dateStyle="long" value="${item.restorationDatetime}" pattern="yyyy-MM-dd HH:mm:ss" />

													<fmt:formatDate var="startDateTime" type="both" dateStyle="long" value="${item.maintenanceStartDatetime}" pattern="yyyy-MM-dd HH:mm:ss" />
													<fmt:formatDate var="endDateTime" type="both" dateStyle="long" value="${item.maintenanceEndDatetime}" pattern="yyyy-MM-dd HH:mm:ss" />
													<input type="text" style="display: none;" id="currentDate${item.id}" name="currentDate${item.id}"
														value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="<%=new java.util.Date()%>" pattern="dd/MM/YYYY"/>" />
													<input type="text" style="display: none;" id="newDate${item.id}" name="newDate${item.id}"
														value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="<%=new java.util.Date()%>" pattern="dd/MM/YYYY hh:mm a"/>" />
													<input type="text" id="powerTypeName${item.id}" name="powerTypeName${item.id}" value="${powerTypeName}" style="display: none;" />
													<c:if test="${fn:containsIgnoreCase(powerTypeName, 'outage')}">
														<c:choose>
															<c:when test="${restoreDate lt dateNow}">
																<tr>
																	<td>${status.index+1}</td>
																	<td>${item.powerAlertTypeName}</td>
																	<td>${item.station}</td>
																	<td>${item.area}</td>
																	<td>${item.title}</td>
																	<td>${item.causes}</td>
																	<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.restorationDatetime}"
																			pattern="dd/MM/YYYY hh:mm a" /></td>
																	<td><c:if test="${not empty item.myTagList}">
																			<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																				<c:if test="${tagStatus.index!=0}">
																					<br />
																					<br />
																					<br />
																					<br />
																				</c:if>

																				<c:choose>
																					<c:when test="${tagItem.status eq 'OFF'}">
																						<p>${tagItem.status}</p>
																					</c:when>
																					<c:otherwise>
																						<p>${tagItem.status}</p>
																						<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a" />
																						<br>
																						<br>
																						<p>Message :</p>
																						<textarea readonly="readonly" class="form-control" cols="23" rows="5">${tagItem.text}</textarea>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																		</c:if></td>
																	<td><c:if test="${not empty item.myTagList}">
																			<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																				<c:if test="${tagStatus.index!=0}">
																					<p>
																						<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																					</p>
																				</c:if>
																				<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																			</c:forEach>
																		</c:if></td>
																</tr>
															</c:when>
															<c:otherwise>
																<tr>
																	<td>${status.index+1}<input class="form-control" id="total${item.id}" name="total${item.id}" type="hidden"
																		value="${status.index+1}"></td>
																	<td>${item.powerAlertTypeName}<input id="type${item.id}" name="type${item.id}" type="hidden" value="${item.powerAlertTypeName}">
																	</td>
																	<td>${item.station}<input id="station${item.id}" name="station${item.id}" type="hidden" value="${item.station}"></td>
																	<td>${item.area}<input id="area${item.id}" name="area${item.id}" type="hidden" value="${item.area}"></td>
																	<td>${item.title}</td>
																	<td><textarea class="form-control" cols="23" id="causes${item.id}" name="causes${item.id}" maxlength="500" rows="10">${item.causes}</textarea></td>
																	<td>
																		<div class='input-group date' id='restoreDate${item.id}'>
																			<input id="inrestoreDate${item.id}" style="width: 150px" class="form-control datepicker" type='text' name="inrestoreDate${item.id}"
																				readonly="readonly"
																				value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.restorationDatetime}" pattern="dd/MM/YYYY hh:mm a"/>">
																			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																		</div>
																	</td>
																	<td><c:if test="${not empty item.myTagList}">
																			<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																				<c:if test="${tagStatus.index!=0}">
																					<br />
																					<br />
																					<br />
																				</c:if>

																				<c:choose>
																					<c:when test="${tagItem.status eq 'SENT' || tagItem.status eq 'SENDING' }">
																						<p>${tagItem.status}</p>
																						<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a" />
																						<br>
																						<br>
																						<p>Message :</p>
																						<textarea readonly="readonly" class="form-control" cols="23" rows="5">${tagItem.text}</textarea>
																					</c:when>
																					<c:when test="${tagItem.status == 'UNSENT'}">
																						<div class="radio">
																							<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="1"
																								onclick="onClickRadio('${item.id}-${tagItem.notificationId}')" checked="checked"> On</label>
																						</div>
																						<br />
																						<div class="radio">
																							<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="0"
																								onclick="onClickRadio('${item.id}-${tagItem.notificationId}')"> Off</label>
																						</div>
																						<div id="pushNotifDiv${item.id}-${tagItem.notificationId}" style="visibility: hidden">
																							<div class='input-group date' id='pushDateTime${item.id}-${tagItem.notificationId}'>
																								<input id="inpushDateTime${item.id}-${tagItem.notificationId}" name="inpushDateTime${item.id}-${tagItem.notificationId}"
																									style="width: 150px" class="form-control datepicker" readonly type='text' onkeydown="return false;"
																									value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a"/>">
																								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																							</div>
																							<br> <br>
																							<p>
																								Message <span style="color: red;">*</span> :
																							</p>
																							<textarea class="form-control" id="text${item.id}-${tagItem.notificationId}" maxlength="120"
																								name="text${item.id}-${tagItem.notificationId}" rows="5">${tagItem.text}</textarea>
																							<input type="hidden" id="notificationId${item.id}-${tagItem.notificationId}"
																								name="notificationId${item.id}-${tagItem.notificationId}" value="${tagItem.notificationId}" />

																						</div>
																					</c:when>
																					<c:otherwise>
																						<div class="radio">
																							<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="1"
																								onclick="onClickRadio('${item.id}-${tagItem.notificationId}')"> On</label>
																						</div>
																						<br />
																						<div class="radio">
																							<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="0"
																								onclick="onClickRadio('${item.id}-${tagItem.notificationId}')" checked="checked"> Off</label>
																						</div>
																						<div id="pushNotifDiv${item.id}-${tagItem.notificationId}" style="visibility: hidden">
																							<div class='input-group date' id='pushDateTime${item.id}-${tagItem.notificationId}'>
																								<input id="inpushDateTime${item.id}-${tagItem.notificationId}" name="inpushDateTime${item.id}-${tagItem.notificationId}"
																									style="width: 150px" class="form-control datepicker" readonly type='text' onkeydown="return false;"
																									value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a"/>">
																								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																							</div>
																							<br> <br>
																							<p>
																								Message <span style="color: red;">*</span> :
																							</p>
																							<textarea class="form-control" id="text${item.id}-${tagItem.notificationId}" maxlength="120"
																								name="text${item.id}-${tagItem.notificationId}" rows="5">${tagItem.text}</textarea>
																							<input type="hidden" id="notificationId${item.id}-${tagItem.notificationId}"
																								name="notificationId${item.id}-${tagItem.notificationId}" value="${tagItem.notificationId}" />
																						</div>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																		</c:if></td>
																	<td><c:if test="${not empty item.myTagList}">
																			<c:set var="lastStatus" scope="session" value="" />
																			<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																				<c:choose>
																					<c:when test="${tagStatus.index==0}">
																						<!-- first row of result -->
																						<c:if test="${tagItem.status == 'SENT'}">
																							<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																						</c:if>
																						<c:if test="${tagItem.status != 'SENT'}">
																							<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																								name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																								onclick="onChangeCheckbox (this,${status.index+1})" />
																						</c:if>
																					</c:when>
																					<c:when test="${tagStatus.index!=0}">
																						<!-- subsequently result -->
																						<c:choose>
																							<c:when test="${lastStatus == 'SENT' && tagItem.status == 'SENT'}">
																								<p>
																									<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																								</p>
																								<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																							</c:when>
																							<c:when test="${lastStatus == 'SENT' && (tagItem.status == 'UNSENT' || tagItem.status == 'OFF')}">
																								<p>&nbsp;</p>
																								<p>
																									<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																								</p>
																								<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																									name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																									onclick="onChangeCheckbox (this,${status.index+1})" />
																							</c:when>
																							<c:when test="${lastStatus != 'SENT' && (tagItem.status == 'UNSENT' || tagItem.status == 'OFF')}">
																								<p>&nbsp;</p>
																								<p>
																									<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																								</p>
																								<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																									name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																									onclick="onChangeCheckbox (this,${status.index+1})" />
																							</c:when>
																							<c:when test="${lastStatus != 'SENT' && (tagItem.status == 'SENT')}">
																								<p>
																									<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																								</p>
																								<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																							</c:when>
																							<c:otherwise>
																								<p>
																									<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																								</p>
																								<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																									name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																									onclick="onChangeCheckbox (this,${status.index+1})" />
																							</c:otherwise>
																						</c:choose>
																					</c:when>
																				</c:choose>
																				<c:set var="lastStatus" scope="session" value="${tagItem.status}" />
																			</c:forEach>
																		</c:if></td>
																</tr>
															</c:otherwise>
														</c:choose>
													</c:if>
													<!-- ## outage -->

													<!-- Preventive -->
													<c:if test="${fn:containsIgnoreCase(powerTypeName, 'preventive')}">
														<c:choose>
															<c:when test="${endDateTime lt dateNow}">
																<tr>
																	<td>${status.index+1}</td>
																	<td>${item.powerAlertTypeName}</td>
																	<td>${item.station}</td>
																	<td>${item.area}</td>
																	<td>${item.title}</td>
																	<td>${item.causes}</td>
																	<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.maintenanceStartDatetime}"
																			pattern="dd/MM/YYYY hh:mm a" /></td>
																	<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.maintenanceEndDatetime}"
																			pattern="dd/MM/YYYY hh:mm a" /></td>
																	<td><c:if test="${not empty item.myTagList}">
																			<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																				<c:if test="${tagStatus.index!=0}">
																					<br />
																					<br />
																					<br />
																					<br />
																				</c:if>

																				<c:choose>
																					<c:when test="${tagItem.status eq 'OFF'}">
																						<p>${tagItem.status}</p>
																					</c:when>
																					<c:otherwise>
																						<p>${tagItem.status}</p>
																						<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a" />
																						<br>
																						<br>
																						<p>Message :</p>
																						<textarea readonly="readonly" class="form-control" cols="23" rows="5">${tagItem.text}</textarea>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																		</c:if></td>
																	<td><c:if test="${not empty item.myTagList}">
																			<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																				<c:if test="${tagStatus.index!=0}">
																					<p>
																						<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																					</p>
																				</c:if>
																				<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																			</c:forEach>
																		</c:if></td>
																</tr>
															</c:when>
															<c:otherwise>
																<tr>
																	<td>${status.index+1}<input class="form-control" id="total${item.id}" name="total${item.id}" type="hidden"
																		value="${status.index+1}"></td>
																	<td>${item.powerAlertTypeName}<input id="type${item.id}" name="type${item.id}" type="hidden" value="${item.powerAlertTypeName}">
																	</td>
																	<td>${item.station}<input id="station${item.id}" name="station${item.id}" type="hidden" value="${item.station}"></td>
																	<td>${item.area}<input id="area${item.id}" name="area${item.id}" type="hidden" value="${item.area}"></td>
																	<td>${item.title}</td>
																	<td><textarea class="form-control" cols="23" id="causes${item.id}" name="causes${item.id}" maxlength="200" rows="10">${item.causes}</textarea></td>
																	<td>
																		<div class='input-group date' id='startDateTime${item.id}'>
																			<input id="instartDateTime${item.id}" name="instartDateTime${item.id}" style="width: 150px" class="form-control datepicker"
																				type='text'
																				value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.maintenanceStartDatetime}" pattern="dd/MM/YYYY hh:mm a"/>">
																			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																		</div>
																	</td>
																	<td>
																		<div class='input-group date' id='endDateTime${item.id}'>
																			<input id="inendDateTime${item.id}" name="inendDateTime${item.id}" style="width: 150px" class="form-control datepicker" type='text'
																				value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.maintenanceEndDatetime}" pattern="dd/MM/YYYY hh:mm a"/>">
																			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																		</div>
																	</td>
																	<td><c:if test="${not empty item.myTagList}">
																			<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																				<c:if test="${tagStatus.index!=0}">
																					<br />
																					<br />
																					<br />
																				</c:if>

																				<c:choose>
																					<c:when test="${tagItem.status eq 'SENT' || tagItem.status eq 'SENDING' }">
																						<p>${tagItem.status}</p>
																						<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a" />
																						<br>
																						<br>
																						<p>Message :</p>
																						<textarea readonly="readonly" class="form-control" cols="23" rows="5">${tagItem.text}</textarea>
																					</c:when>
																					<c:when test="${tagItem.status == 'UNSENT'}">
																						<div class="radio">
																							<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="1"
																								onclick="onClickRadio('${item.id}-${tagItem.notificationId}')" checked="checked"> On</label>
																						</div>
																						<br />
																						<div class="radio">
																							<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="0"
																								onclick="onClickRadio('${item.id}-${tagItem.notificationId}')"> Off</label>
																						</div>
																						<div id="pushNotifDiv${item.id}-${tagItem.notificationId}" style="visibility: hidden">
																							<div class='input-group date' id='pushDateTime${item.id}-${tagItem.notificationId}'>
																								<input id="inpushDateTime${item.id}-${tagItem.notificationId}" name="inpushDateTime${item.id}-${tagItem.notificationId}"
																									style="width: 150px" class="form-control datepicker" readonly type='text' onkeydown="return false;"
																									value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a"/>">
																								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																							</div>
																							<br> <br>
																							<p>
																								Message <span style="color: red;">*</span> :
																							</p>
																							<textarea class="form-control" id="text${item.id}-${tagItem.notificationId}" maxlength="120"
																								name="text${item.id}-${tagItem.notificationId}" rows="5">${tagItem.text}</textarea>
																							<input type="hidden" id="notificationId${item.id}-${tagItem.notificationId}"
																								name="notificationId${item.id}-${tagItem.notificationId}" value="${tagItem.notificationId}" />

																						</div>
																					</c:when>
																					<c:otherwise>
																						<div class="radio">
																							<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="1"
																								onclick="onClickRadio('${item.id}-${tagItem.notificationId}')"> On</label>
																						</div>
																						<br />
																						<div class="radio">
																							<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="0"
																								onclick="onClickRadio('${item.id}-${tagItem.notificationId}')" checked="checked"> Off</label>
																						</div>
																						<div id="pushNotifDiv${item.id}-${tagItem.notificationId}" style="visibility: hidden">
																							<div class='input-group date' id='pushDateTime${item.id}-${tagItem.notificationId}'>
																								<input id="inpushDateTime${item.id}-${tagItem.notificationId}" name="inpushDateTime${item.id}-${tagItem.notificationId}"
																									style="width: 150px" class="form-control datepicker" readonly type='text' onkeydown="return false;"
																									value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a"/>">
																								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																							</div>
																							<br> <br>
																							<p>
																								Message <span style="color: red;">*</span> :
																							</p>
																							<textarea class="form-control" id="text${item.id}-${tagItem.notificationId}" maxlength="120"
																								name="text${item.id}-${tagItem.notificationId}" rows="5">${tagItem.text}</textarea>
																							<input type="hidden" id="notificationId${item.id}-${tagItem.notificationId}"
																								name="notificationId${item.id}-${tagItem.notificationId}" value="${tagItem.notificationId}" />
																						</div>
																					</c:otherwise>
																				</c:choose>
																			</c:forEach>
																		</c:if></td>
																	<td><c:if test="${not empty item.myTagList}">
																			<c:set var="lastStatus" scope="session" value="" />
																			<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																				<c:choose>
																					<c:when test="${tagStatus.index==0}">
																						<!-- first row of result -->
																						<c:if test="${tagItem.status == 'SENT'}">
																							<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																						</c:if>
																						<c:if test="${tagItem.status != 'SENT'}">
																							<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																								name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																								onclick="onChangeCheckbox (this,${status.index+1})" />
																						</c:if>
																					</c:when>
																					<c:when test="${tagStatus.index!=0}">
																						<!-- subsequently result -->
																						<c:choose>
																							<c:when test="${lastStatus == 'SENT' && tagItem.status == 'SENT'}">
																								<p>
																									<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																								</p>
																								<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																							</c:when>
																							<c:when test="${lastStatus == 'SENT' && (tagItem.status == 'UNSENT' || tagItem.status == 'OFF')}">
																								<p>&nbsp;</p>
																								<p>
																									<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																								</p>
																								<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																									name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																									onclick="onChangeCheckbox (this,${status.index+1})" />
																							</c:when>
																							<c:when test="${lastStatus != 'SENT' && (tagItem.status == 'UNSENT' || tagItem.status == 'OFF')}">
																								<p>&nbsp;</p>
																								<p>
																									<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																								</p>
																								<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																									name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																									onclick="onChangeCheckbox (this,${status.index+1})" />
																							</c:when>
																							<c:when test="${lastStatus != 'SENT' && (tagItem.status == 'SENT')}">
																								<p>
																									<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																								</p>
																								<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																							</c:when>
																							<c:otherwise>
																								<p>
																									<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																								</p>
																								<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																									name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																									onclick="onChangeCheckbox (this,${status.index+1})" />
																							</c:otherwise>
																						</c:choose>
																					</c:when>
																				</c:choose>
																				<c:set var="lastStatus" scope="session" value="${tagItem.status}" />
																			</c:forEach>
																		</c:if></td>
																</tr>
															</c:otherwise>
														</c:choose>
													</c:if>
												</c:forEach>
											</c:if>
										</tbody>
									</table>
									<div class="form-group">
										<div class="col-lg-5 col-lg-offset-3 text-right">
											<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
											<button id="submitBtn" class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal">Update</button>
											<button class="btn btn-warning" id="cancelBtn1" type="reset">Cancel</button>
										</div>
									</div>
									<!-- Add Confirmation Modal -->
									<div class="modal" id="modal" tabindex="-1">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button class="close" data-dismiss="modal" type="button">
														<span>&times;</span><span class="sr-only">Close</span>
													</button>
													<h2 class="modal-title">Confirmation</h2>
												</div>
												<div class="modal-body" id="add">Are you sure you want to update outage alert?</div>
												<div class="modal-footer">
													<button class="btn btn-primary" id="add" type="button">Update</button>
													<button class="btn btn-warning" data-dismiss="modal" type="button">Close</button>
												</div>
											</div>
										</div>
									</div>
									<!-- End Confirmation Modal -->


								</form>
								<!-- #viewATForm -->
							</div>
							<!-- #resultDiv -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
	 function onClickRadio(item) {
	 		var radio = $("input[type='radio'][name='pushNotif" + item + "']:checked").val();
	 		if (radio == "1") {
	 		 document.getElementById('pushNotifDiv' + item).style.visibility = "visible";
	 		} else if (radio == "0") {
	 		 document.getElementById('pushNotifDiv' + item).style.visibility = "hidden";
	 		}
	 	}
	 	

	// Handle click on checkbox
	function onChangeCheckbox(checkbox, no) {
		if (checkbox.checked) {
			document.getElementById("submitBtn").disabled = false;
		} else {
			error = "";
			$('#messages').hide();
		}
	}
     
     var errorNo = "";

     function validateForm(num, mixId) {
     	var alls = mixId.split("-");
     	var item = alls[0];
     	
     	//console.log('validateForm');
     	if ($.trim($('#causes' + item).val()) == '') {
     		errorNo += "<p>Error : Item " + num + " : Causes is required</p>";
     	}    
     	if($('#powerTypeName' + item).val().toLowerCase().indexOf('preventive') > -1){
     		//console.log('preventive');
     		//console.log('inendDateTime>'+$('#inendDateTime' + item).val());
     		//console.log('instartDateTime>'+$('#instartDateTime' + item).val());
     		if(moment($('#inendDateTime' + item).val(), "DD/MM/YYYY hh:mm A").isBefore(moment($('#instartDateTime' + item).val(), "DD/MM/YYYY hh:mm A"))){
       			errorNo += "<p>Error : Item " + num + " : start date time of maintenance must ealier than end date time of maintenance.</p>";

     		}
     		else if(moment($('#inendDateTime' + item).val(), "DD/MM/YYYY hh:mm A").isBefore(moment($('#newDate' + item).val(), "DD/MM/YYYY hh:mm A"))){
             		errorNo += "<p>Error : Item " + num + " : End date Time of maintenance must later than current date.</p>";     			
     		}
     		if($("input[type='radio'][name='pushNotif" + mixId + "']:checked").val() ==1 && (moment($('#inendDateTime' + item).val(), "DD/MM/YYYY hh:mm A").isBefore(moment($('#inpushDateTime' + mixId).val(), "DD/MM/YYYY hh:mm A")))){
     			errorNo += "<p>Error : Item " + num + " : Push notification date time must ealier than end date time of maintenance.</p>";
     		}
     		/* if (moment($('#currentDate' + item).val(), "DD/MM/YYYY hh:mm:ss").isAfter(moment($('#inpushDateTime' + mixId).val(), "DD/MM/YYYY hh:mm:ss"))) {
     			errorNo += "<p>Error : Item " + num + " : Push notification date time must ealier than current date.</p>";
    	 	}  */
     		if ($("input[type='radio'][name='pushNotif" + mixId + "']:checked").val() ==1 && moment($('#inpushDateTime' + mixId).val(), "DD/MM/YYYY hh:mm A").isBefore(moment($('#newDate' + item).val(), "DD/MM/YYYY hh:mm A"))) {
     			errorNo += "<p>Error : Item " + num + " : Push notification date time must later than current date.</p>";
    	 	}        		
     		if( $("input[type='radio'][name='pushNotif" + mixId + "']:checked").val()==1 && $.trim($('#text' + mixId).val()).length<=0){
     			errorNo += "<p>Error : Item " + num + " : Push notification message is required.</p>";
     		}
   		
     	}else{
     		//console.log('outage'); 
       		//console.log("newDate>>"+$('#newDate' + item).val());
      		//console.log("inrestoreDate>>"+$('#inrestoreDate' + item).val());
	
     		//console.log('inrestoreDate >'+moment($('#inrestoreDate' + item).val(), "DD/MM/YYYY hh:mm A")); 
     		//console.log('currentDate >'+moment($('#currentDate' + item).val(), "DD/MM/YYYY hh:mm A")); 
     		if (moment($('#inrestoreDate' + item).val(), "DD/MM/YYYY hh:mm A").isBefore(moment($('#newDate' + item).val(), "DD/MM/YYYY hh:mm A"))) {
         		//console.log('restore < today');
         		errorNo += "<p>Error : Item " + num + " : Estimated restoration time must later than current date.</p>";
         	}
     		if( $("input[type='radio'][name='pushNotif" + mixId + "']:checked").val()==1 && (moment($('#inpushDateTime' + mixId).val(), "DD/MM/YYYY hh:mm A").isAfter(moment($('#inrestoreDate' + item).val(), "DD/MM/YYYY hh:mm A")))){
     			errorNo += "<p>Error : Item " + num + " : Push notification date time must  ealier than estimated restoration time.</p>";
     		}
     		if ($("input[type='radio'][name='pushNotif" + mixId + "']:checked").val() ==1 && moment($('#inpushDateTime' + mixId).val(), "DD/MM/YYYY hh:mm A").isBefore(moment($('#newDate' + item).val(), "DD/MM/YYYY hh:mm A"))) {
     			errorNo += "<p>Error : Item " + num + " : Push notification date time must later than current date.</p>";
    	 	}     		
     		if( $("input[type='radio'][name='pushNotif" + mixId + "']:checked").val()==1 && $.trim($('#text' + mixId).val()).length<=0){
     			errorNo += "<p>Error : Item " + num + " : Push notification message is required.</p>";
     		}
     	}
     }
     
    $(document).ready(function() {
    	var today = new Date();
    	var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
    	$('#startDate').datetimepicker({
    		format: 'DD/MM/YYYY',
    		defaultDate: new Date(),
    		ignoreReadonly: true
    	});
    	$('#endDate').datetimepicker({
    		format: 'DD/MM/YYYY',
    		defaultDate: tomorrow,
    		ignoreReadonly: true
    	});
    	
    	$('#selectType').change(function() {        	
         	var powerTypeName = $("#selectType option:selected").text();
         	console.log("powerTypeName>>>"+powerTypeName);
         	 $('#powerTypeName').val(powerTypeName);         	 
         });
           
       <c:if test="${not empty outageList}">
       <c:forEach items="${outageList}" var="item" varStatus="loop">
	           
	           $('#restoreDate' + ${item.id}).datetimepicker({
	        	 	format: 'DD/MM/YYYY hh:mm A',
	        	 	defaultDate: new Date(),
	        	 	ignoreReadonly: true,
	        	 	stepping: 30
	        	});
	           
	           $('#startDateTime' + ${item.id}).datetimepicker({
            	   format: 'DD/MM/YYYY hh:mm A',
                	defaultDate: new Date()
            	}); 
            	$('#endDateTime' + ${item.id}).datetimepicker({
            	   format: 'DD/MM/YYYY hh:mm A',
                	defaultDate: new Date()
            	}); 
            	
            	<c:forEach var="tagItem" items="${item.myTagList}"  varStatus="loop2">
 	           $('#pushDateTime'+${item.id}+'-'+${tagItem.notificationId}).datetimepicker({
 	             format: 'DD/MM/YYYY hh:mm A',
 	             defaultDate: new Date(),
 	             ignoreReadonly: true
 	         	}); 
 	           
 	           <c:if test="${tagItem.status == 'UNSENT'}">
 		        	//$('#pushNotifDiv' + ${item.id}+'-'+${tagItem.notificationId}).show();
 		        	$('#pushNotifDiv' + ${item.id}+'-'+${tagItem.notificationId}).css('visibility', 'visible');
 				</c:if>	
            </c:forEach>
            
            	
	       </c:forEach>
           </c:if>      
           
          	if (${outageList != null && outageList.size() >= 0}) {
        		$('#resultDiv').show();
        		var table = $('#tblSearch').DataTable({
        			//  responsive: true,   
        			bFilter: false,
        			bSort: false,
        			lengthMenu: [[50, 100],[50, 100]]
        		});
        	}

     $("#cancelBtn1").click(function(){
         $('#searchPAForm').find('.has-error').removeClass("has-error");
            $('#searchPAForm').find('.has-success').removeClass("has-success");
            $('#searchPAForm').find('.form-control-feedback').removeClass('glyphicon-remove');
            $('#searchPAForm').find('.form-control-feedback').removeClass('glyphicon-ok');
            $('.form-group').find('small.help-block').hide();   
            $('#searchATForm').formValidation('resetForm', true);
        });
     
     $("button#add").click(function() {
     	$('#modal').modal('hide');
    	 var rows_selected = new Array();
      		errorNo = "";
          //console.log("submitBtn was clicked");
          var oTable = $('#tblSearch').dataTable();
          var rowcollection = oTable.$(".call-checkbox:checked", {
              "page": "all"
          });  
          if (rowcollection.length == 0) {
          	window.scrollTo(0,0);
          	$('#errorMsg').html("<div id=\"errorMsg\" class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> <span class=\"sr-only\"></span> Please select the checkbox to update. </div>").show();
              return false;
          }
          $('#errorMsg').hide();
          rowcollection.each(function(index, elem) {
            var idpush 	= $(elem).val();
            var item 	= idpush.split("-")[0];
            
            console.log("my item = "+item);
            console.log("idpush>>"+idpush);
            var num = $('#total' + item.split("-")[0]).val();
              console.log("no>>" + num);console.log("item>>" + item);
              var powerTypeName = $('#powerTypeName').val();
              var station = $('#station' + item).val();
              var area = $('#area' + item).val();
              var causes = $('#causes' + item).val();
              var inrestoreDate = $('#inrestoreDate' + item).val();
              var instartDateTime = $('#instartDateTime' + item).val();
              var inendDateTime = $('#inendDateTime' + item).val();
              var pushNotif = $("input[type='radio'][name='pushNotif" + idpush + "']:checked").val();
              var inpushDateTime = $('#inpushDateTime' + idpush).val();
              var notificationId = $('#notificationId' + idpush).val();
              var text = $('#text' + idpush).val();
              console.log("inrestoreDate>>"+inrestoreDate);
              console.log("instartDateTime>>"+instartDateTime);
              console.log("inendDateTime>>"+inendDateTime);
              
                     
	         rows_selected.push({
	                 id: item,
	                 powerTypeName : powerTypeName,
	                 station : station,
	                 area : area,
	                 causes : causes,
	                 inrestoreDate: inrestoreDate,
	                 instartDateTime: instartDateTime,
	                 inendDateTime: inendDateTime,
	                 pushNotif: pushNotif,
	                 inpushDateTime: inpushDateTime,
	                 text: text,
	                 notificationId:notificationId
	        		})
	        		console.log("rows_selected>>"+rows_selected);
	        validateForm(num, idpush);

         })
         
         console.log("errorNo.length="+errorNo.length);
          if(errorNo.length>0){                	
               $('#messages').html(errorNo).show();
               window.scrollTo(0,250);
           	return false;
           }
         
          //console.log("validate success:");
          var token = $("meta[name='_csrf']").attr("content");
          var header = $("meta[name='_csrf_header']").attr("content");
          
          $.ajax({
              url: "${pageContext.request.contextPath}/admin/updatePowerAlert.do",
              type: "POST",
              dataType: "json",
              data: JSON.stringify(rows_selected),
              beforeSend: function(xhr) {
                  xhr.setRequestHeader(header, token);
                  xhr.setRequestHeader("Accept", "application/json");
                  xhr.setRequestHeader("Content-Type", "application/json");
                  sebApp.showIndicator();
              },
              success: function(data) {
            	  if(data.success!=null && data.success.length>0){
                      $('#successMsg').html(data.success).show();
                  }
                  else if(data.error!=null && data.error.length>0){   
                      $('#errorMsg').html(data.error).show();
                  } 
                  
                  window.scrollTo(0, 5);
                  rows_selected = new Array();
                  $('#resultDiv').hide();
              },
              failure: function(data) {
                  alert("Fail To Connect");
              },
              complete: function(){sebApp.hideIndicator();}
          });
          return true;
      });

     $('#searchPAForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
                instartDate: {
                    validators: {
                        callback: {
                            callback: function(value, validator, $field) {     
                                // console.log("value1>"+$('#instartDate').val());
                                if (value =='') {
                                    $('#instartDate').val(moment(new Date()).format('DD/MM/YYYY'));
                                    //console.log("value2>"+$('#indateFrom').val());
                                    return true;
                                }
                                return true;
                            }
                        },
                        format: 'DD/MM/YYYY'                        
                    }
                },
                inendDate: {
                    validators: {
                        callback: {
                            callback: function(value, validator, $field) {                                
                                if (value=='') {
                                    $('#inendDate').val(moment(new Date()).format('DD/MM/YYYY'));                                    
                                    return true;
                                }
                                return true;
                            }
                        },
                        format: 'DD/MM/YYYY'                        
                    }
                },
                selectType: {
                    validators: {
                        callback: {
                            message: 'Please Select Type',
                            callback: function(value, validator, $field) {
                                var options = validator.getFieldElements('selectType').val();
                                return (options != null && options.length >0);
                            }
                        }
                    }
                }                
            }
        });
    });
    </script>
</body>
</html>