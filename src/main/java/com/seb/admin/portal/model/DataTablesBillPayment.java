package com.seb.admin.portal.model;

import java.util.List;

public class DataTablesBillPayment<PaymentReference> implements java.io.Serializable{

	private static final long serialVersionUID = -8220588043068200705L;
	private List<PaymentReference> aaData;
	private String  sEcho;
	private Integer iTotalRecords;
	private Integer iTotalDisplayRecords;
	private String sColumns;

	private int iDisplayStart;
	private int iDisplayLength;
	public List<PaymentReference> getAaData() {
		return aaData;
	}
	public void setAaData(List<PaymentReference> aaData) {
		this.aaData = aaData;
	}
	public String getsEcho() {
		return sEcho;
	}
	public void setsEcho(String sEcho) {
		this.sEcho = sEcho;
	}
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}
	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}
	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}
	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}
	public String getsColumns() {
		return sColumns;
	}
	public void setsColumns(String sColumns) {
		this.sColumns = sColumns;
	}
	public int getiDisplayStart() {
		return iDisplayStart;
	}
	public void setiDisplayStart(int iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}
	public int getiDisplayLength() {
		return iDisplayLength;
	}
	public void setiDisplayLength(int iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}


	

}
