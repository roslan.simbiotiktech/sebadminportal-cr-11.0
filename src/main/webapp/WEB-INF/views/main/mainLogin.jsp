<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>

<style type="text/css">
@import url("${pageContext.request.contextPath}/resources/dist/css/mainpage.css");
</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<img style="margin-top: 10%" src="${pageContext.request.contextPath}/resources/images/SEBLogo-White.png" alt="" width="370px" height="146px" />

				<div class="login-panel panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Please Sign In</h3>
					</div>

					<div class="panel-body">

						<form action="<c:url value='/j_spring_security_check' />" class="form-horizontal" id="loginForm" method="post" name="loginForm">
							<fieldset>
								${loginErrorMsg}${error}
								<div class="form-group">
									<p class="col-lg-4 control-label">Login ID :</p>

									<div class="col-lg-7">
										<input autofocus="" class="form-control" name="loginId" id="loginId" placeholder="Login ID" type="text" maxlength="100">
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-4 control-label">Password :</p>

									<div class="col-lg-7">
										<input class="form-control" name="password" id="password" placeholder="Password" type="password" value="" maxlength="100">
									</div>
								</div>

								<!-- <div class="checkbox col-lg-8">
									<label><input name="remember-me" type="checkbox"
										value="Remember Me">Remember Me</label>
								</div> -->

								<div class="form-group">
									<div class="col-lg-8 col-lg-offset-3 text-right">
										<button class="btn btn-primary" type="submit">Sign In</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
							</fieldset>
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function resetForm() {
			$('#').find('.has-error').removeClass("has-error");
			$('#loginForm').find('.has-success').removeClass("has-success");
			$('#loginForm').find('.form-control-feedback').removeClass('glyphicon-remove');
			$('#loginForm').find('.form-control-feedback').removeClass('glyphicon-ok');
			$('#loginForm').formValidation('resetForm', true);
			
			$('.form-group').find('small.help-block').hide();
			$('.form-group').removeClass('has-error has-feedback');
			$('.form-group').find('i.form-control-feedback').hide();
			
			return false;
		}

		$(document).ready(function() {
			$('#loginForm').formValidation({
				framework : 'bootstrap',
				icon : {
					invalid : 'glyphicon glyphicon-remove',
				},
				fields : {
					loginId : {
						validators : {
							notEmpty : {
								message : 'Login ID is required'
							}
						}
					},
					password : {
						validators : {
							notEmpty : {
								message : 'Password is required'
							}
						}
					}
				}
			});
		});
	</script>
</body>
</html>