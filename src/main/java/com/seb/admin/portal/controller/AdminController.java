package com.seb.admin.portal.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.service.AdminService;
import com.seb.admin.portal.service.ReportService;
import com.seb.admin.portal.util.Encryptor;
import com.seb.admin.portal.util.StringUtil;

@Controller
public class AdminController {	

	private static final Logger log = Logger.getLogger(AdminController.class);

	@Autowired
	private ReportService reportService;
	@Autowired
	private AdminService adminService;
	
	/** main page **/
	@RequestMapping(value = "/admin/changePassword", method = RequestMethod.GET)
	public ModelAndView doChangePassword() {
		
		return new ModelAndView("/admin/changePassword");
	}

	@RequestMapping(value = "/admin/changePassword", method = RequestMethod.POST)
	public ModelAndView updateChangePassword(@RequestParam(value="password", required=true) String newPassword){
		
		ModelAndView model = new ModelAndView();
		
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		
		try
		{				
			
			
			boolean status = adminService.changePassword(user.getId(), Encryptor.getMD5(newPassword));
			System.out.println(">status>"+status);
			log.info("change password >"+newPassword);
			reportService.auditLog("Change Password", new AuditTrail(user.getId(),0,"","Change Password: "+user.getLoginId(),"", "", ""));

			if(status){				
				model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully changed password."));

			}else{
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to changed password."));
			}
			
			System.out.println("status >>"+status);

		}catch(Exception ex){
			ex.printStackTrace();
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to changed password."));
			reportService.auditLog("Change Password", new AuditTrail(user.getId(),0,"","Change Password: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		

			log.error(ex.getMessage());
			ex.printStackTrace();
		}		
		model.setViewName("/admin/changePassword");
		return model;


	}

}
