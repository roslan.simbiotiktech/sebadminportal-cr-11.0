package com.seb.admin.portal.dao;

import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.springframework.stereotype.Repository;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.Setting;

@Repository
public class SettingDAOImpl implements SettingDAO {

	@PersistenceContext(unitName = ApplicationConstant.UNIT_NAME, type = PersistenceContextType.EXTENDED)
	private EntityManager em;

	@Override
	public Setting findByKey(String key)  throws SQLException {
		Setting setting = new Setting();
		try{
			setting = (Setting) em.createNamedQuery("Setting.findByKey").setParameter("key", key).getSingleResult();
			System.out.println(">>>>>>>>>"+setting.getSettingKey());
		}catch(Exception ex){
			ex.printStackTrace();
		}		
		return setting;
	}
	
	
	@Override
	public boolean updateSetting(Setting s0) throws SQLException {
		try{	
			Setting update = em.find(Setting.class, s0.getSettingKey());
			update.setSettingValue(s0.getSettingValue());
			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();
			return false;
		}	
		return true;
	}
	

	
}
