<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
        </nav>
        <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp"%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Maintenance &gt; Contact Us
                        </div>
                        <div class="panel-body">
                            <form  id="addContactUsForm"name="addContactUsForm" action="${pageContext.request.contextPath}/admin/contactUs.do" class="form-horizontal"  method="post">
                                ${successMsg}${errorMsg}	
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Telephone Number <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <input class="form-control" id="tel" name="tel" placeholder="Telephone Number" type="text" maxlength="20" value="${contact.telNo }">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Fax <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <input class="form-control" id="fax" name="fax" placeholder="Fax" type="text"  maxlength="20" value="${contact.faxNo }">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Email <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <input class="form-control" id="email" name="email" placeholder="Email" type="text" maxlength="50" value="${contact.email }">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Address <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <textarea class="form-control" cols="23" id="address" name="address" rows="10" maxlength="200">${contact.address }
</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Website URL<span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <input class="form-control" id="url" name="url" placeholder="Website URL" type="text" maxlength="50" value="${contact.webUrl }">
                                    </div>
                                </div>                                                              
                                <div class="form-group">
                                    <div class="col-lg-5 col-lg-offset-3 text-right">
                                        <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> <button class="btn btn-primary" id="submitBtn" type="button" data-toggle="modal" data-target="#modal">Update</button> <button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
                                    </div>
                                </div>
                                
                                <!-- Add Confirmation Modal -->
								<div class="modal fade" id="modal" tabindex="-1">
						        <div class="modal-dialog">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button class="close" data-dismiss="modal" type="button"><span>&times;</span><span class="sr-only">Close</span></button>
											<h2 class="modal-title">Confirmation</h2>
						                </div>						
						                <div class="modal-body" id="add">
						                    Are you sure you want to update Contact Us?
						                </div>						
						                <div class="modal-footer">
						                    <button class="btn btn-primary" id="add" type="submit">Update</button> <button class="btn btn-warning" data-dismiss="modal" type="button">Close</button> 
						                </div>
						            </div>
						        </div>
   								</div>
   								<!-- End Confirmation Modal -->
   								
                            </form>
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /#page-wrapper -->
    </div>
    <script type="text/javascript">
    function resetForm() {
    	document.getElementById("addContactUsForm").reset();
    	$('#').find('.has-error').removeClass("has-error");
    	$('#addContactUsForm').find('.has-success').removeClass("has-success");
    	$('#addContactUsForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#addContactUsForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();
    	    	
    	return false;
    }

    $(document).ready(function() {       
        $('#addContactUsForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
                tel: {
                    validators: {
                        notEmpty: {
                            message: 'Telephone Number is required'
                        },
                        stringLength: {
                            max: 20,
                            message: 'Telephone Number must less than 20 characters'
                        },
                        regexp: {
                            regexp: /^[0-9-+]+$/,
                            message: 'Telephone Number only consist of number and dash'
                        }
                    }
                },
                fax: {
                    validators: {
                    	notEmpty: {
                            message: 'Fax is required'
                        },
                        stringLength: {
                            max: 20,
                            message: 'Fax must less than 20 characters'
                        },
                        regexp: {
                            regexp: /^[0-9-+]+$/,
                            message: 'Fax Number only consist of number and dash'
                        }
                    }
                },
                email: {
                    validators: {
                        emailAddress: {
                            message: 'The value is not a valid email address'
                        }
                    }
                },
                address: {
                    validators: {
                        notEmpty: {
                            message: 'Address is required'
                        },
                        stringLength: {
                            min: 1,
                            max: 200,
                            message: 'Address must less than 200 characters'
                        }
                    }
                },
                url: {
                    validators: {
                    	notEmpty: {
                            message: 'Website URL is required'
                        },
                        stringLength: {
                            min: 1,
                            max: 200,
                            message: 'Website must less than 50 characters'
                        }
                    }
                }
            }
        });
    });
    </script>
</body>
</html>