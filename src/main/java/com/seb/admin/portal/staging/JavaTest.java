package com.seb.admin.portal.staging;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang.StringEscapeUtils;

import com.seb.admin.portal.model.PaymentGateway;

public class JavaTest {

	public static void main(String[] args) throws ParseException { 
		String message = "a";
		
		System.out.println(StringEscapeUtils.unescapeHtml(StringEscapeUtils.unescapeHtml(message)));
		
		
		String paymentMethod = "FPX";
		String pymtRef = "F23";
		String searchStr = "";
		boolean isPass = false;
		Map listMap = new HashMap();
		listMap.put("FPX","F");
		listMap.put("MPG","M");

		
		if(paymentMethod.equalsIgnoreCase("ALL")){
			if(pymtRef.isEmpty()){
				System.out.println("ALL && empty");
				isPass = true;
			}else{
				Iterator iterator = listMap.entrySet().iterator();
				while (iterator.hasNext()) {
					System.out.println("[iterator]");
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					String gatewayId =  (String) mapEntry.getKey();
					String prefix = (String) mapEntry.getValue();
					if(pymtRef.trim().toUpperCase().startsWith(prefix.toUpperCase())){
						System.out.println("not empty && match prefix");
						isPass = true;
						searchStr = pymtRef.toUpperCase().replace(prefix.toUpperCase(), "");
					}
				}
			}
		}else{
			if(pymtRef.isEmpty()){
				System.out.println("NOT ALL && is empty");
				isPass = true;
			}else{
				System.out.println("NOT ALL && NOT empty");
				Iterator iterator = listMap.entrySet().iterator();
				while (iterator.hasNext()) {
					System.out.println("[else:iterator]");
					Map.Entry mapEntry = (Map.Entry) iterator.next();
					String gatewayId =  (String) mapEntry.getKey();
					String prefix = (String) mapEntry.getValue();
					if(paymentMethod.equalsIgnoreCase(gatewayId) && pymtRef.trim().toUpperCase().startsWith(prefix.toUpperCase())){
						System.out.println("method && prefix is match");
						isPass = true;
						searchStr = pymtRef.toUpperCase().replace(prefix.toUpperCase(), "");
					}
				}
			}
			
		}
		
		System.out.println("---------------------------------------------------------------------");
		System.out.println("isPass ::"+isPass);
		System.out.println("pymtRef ::"+pymtRef);
		if(!isPass){
			searchStr = "0"	;
		}
		System.out.println("searchStr ::"+searchStr);
		
		

	}
}
