<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">			
			 <li>
				<a class="sidebarlink" href="${pageContext.request.contextPath}/mainLogin.do"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Home</a>
			</li> 
			<c:set var="menuTitle" scope="session" value=""/>
			
			<c:forEach items="${sessionMenuList}" var="item" varStatus="status">	
				<c:if test="${item.name ne menuTitle}">			
					<c:choose>
						<c:when test="${item.level eq '1' && item.hasChildLevel2 eq 'false'}">
						<li>
							<a class="sidebarlink" href="${pageContext.request.contextPath}${item.accessUrl}"><%-- <img src="${pageContext.request.contextPath}/resources/images/sidebar/home.png" alt="" width="32px"  height="32px" /> --%>${item.imageContent} ${item.name}</a>
						</c:when>
						<c:when test="${item.level eq '1' && item.hasChildLevel2 eq 'true'}">
						<li>
							<a class="sidebarlink" href="${status.index+1 }"><%-- <img src="${pageContext.request.contextPath}/resources/images/sidebar/${item.imageContent}" alt="" width="32px"  height="32px" /> --%>${item.imageContent} ${item.name} <span class="fa arrow"></span></a>
						</c:when>
					</c:choose>	
						
						<c:if  test="${item.hasChildLevel2 eq 'true'}">
							<ul class="nav nav-second-level">
							
									<c:forEach items="${item.level2Menu}" var="item2" varStatus="status">	
										<c:choose>																		
											<c:when test="${item.hasChildLevel3 eq 'false'}">
												<li><a class="sidebarlink"  href="${pageContext.request.contextPath}${item2.accessUrl}">${item2.name}</a></li>
											</c:when>
											<c:when test="${item.hasChildLevel3 eq 'true' && item2.accessUrl ne ''}">
												<li><a class="sidebarlink" href="${pageContext.request.contextPath}${item2.accessUrl}">${item2.name}</a>
											</c:when>											
											<c:when test="${item.hasChildLevel3 eq 'true' && item2.accessUrl eq ''}">
											
											<li><a class="sidebarlink" href="${status.index+1 }">${item2.name}${item2.accessUrl}<span class="fa arrow"></span></a>
																																				
											
												<ul class="nav nav-third-level">
													 <c:forEach items="${item.level3Menu}" var="item3" varStatus="status">	
													 	<c:if  test="${item3.parentModuleCode eq item2.moduleCode}">						                      
				                        					<li><a class="sidebarlink" href="${pageContext.request.contextPath}${item3.accessUrl}">${item3.name}</a></li>
													 	</c:if>
													 </c:forEach> 
												</ul>						
											</li>											
											</c:when>
										</c:choose>	
										
									</c:forEach>
							
							</ul>							
						</c:if>
					
					<c:set var="menuTitle" scope="session" value="${item.name}"/>
				</c:if>
				
				
			
			</c:forEach>
			
		</ul>
	</div>
</div>