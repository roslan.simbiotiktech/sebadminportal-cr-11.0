package com.seb.admin.portal.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.seb.admin.portal.dao.LoginDAO;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.service.Assembler;
import com.seb.admin.portal.service.UserMgmtService;
import com.seb.admin.portal.util.Encryptor;
import com.seb.admin.portal.util.LDAPUtil;
import com.seb.admin.portal.util.StringUtil;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private LoginDAO loginDAO;
	@Autowired
	private Assembler assembler;
	@Autowired
	private UserMgmtService userMgmtService;

	private static final Logger log = Logger.getLogger(CustomAuthenticationProvider.class);
	
	public CustomAuthenticationProvider() {
		super();
	}

	@Override
	public Authentication authenticate(Authentication authentication) 
			throws AuthenticationException {
		String loginId = authentication.getName();
		String password = authentication.getCredentials().toString();
		
		// use the credentials to try to authenticate against the third party system
		if (!(StringUtil.trimToEmpty(loginId).equalsIgnoreCase("") && StringUtil.trimToEmpty(password).equalsIgnoreCase(""))) {
			System.out.println("authenticate start");

			//LDAP START
			LDAPUtil ldap = new LDAPUtil();
			String dn = ldap.getDN(loginId);  //get DN
			log.info("dn>>"+dn);
			if(dn != null)
			{    			
				if(!ldap.checkOU(dn).equalsIgnoreCase("OU not exist"))  //OU exist in ldap
				{
					//check OU exist in tbl_secuser_group
					String secuserGroup = ldap.checkOU(dn); //active group
					//String secuserGroup = sessionUser.loginGroupName(con, "OU=" + ldap.getOU(dn)); //active group
					log.info("secuserGroup = " + secuserGroup);

					if(!secuserGroup.equalsIgnoreCase(""))  //OU exist in tbl_secuser_group and status = active
					{
						boolean result = ldap.validateLogin(dn, password);   //validate samAcount and password from LDAP
						log.info("validateLogin() result = " + result);

						if(result == true)
						{
							//secuserGroup = OU=ServiceAccounts,OU=_HQ
							String [] OU = secuserGroup.split(",");
							log.info("extract OU>"+OU[0].toString());
							//check of Login exist in local db
							AdminUser secuser = loginDAO.getUserDetails (loginId);
							

							//Login exist in local db
							if(secuser != null)
							{								
								System.out.println("department::"+OU[0].toString());
								
								if(secuser.getStatus().equalsIgnoreCase("ACTIVE")){

								//if((OU[0].toString().contains(secuser.getDepartment())) || secuser.getStatus().equalsIgnoreCase("ACTIVE")){

									AdminUser newUser = new AdminUser();
									newUser.setId(secuser.getId());
									newUser.setDepartment(OU[0].toString());
									newUser.setName(ldap.getUserBasicAttributes("mail",loginId, ldap.getLdapContext()));
									try {
										userMgmtService.updateAdminUserAD(newUser);
									} catch (SQLException e) {									
										e.printStackTrace();
									}
									
									log.info("user is ACTIVE;deparment="+secuser.getDepartment()+",email="+secuser.getName());
									
									return new MyAuthenticationToken(loginId, password, assembler.getAuthorities(secuser.getRole()),secuser);
								}
								else
								{
									log.info("user DELETED or NO DEPARTMENT");	
									throw new UsernameNotFoundException("User Record Not Found in Admin Portal.");
								}
							}else{
								System.out.println("user not found");
								throw new UsernameNotFoundException("User Record Not Found in Admin Portal.");
							}
						}

						else
						{
							log.info("Invalid password : " + loginId);
							throw new BadCredentialsException("Invalid Login ID or Password.");

						}
					}
					else   //group_status = deleted or not group not exist 
					{
						log.info("Group does not authorise access.");
						throw new UsernameNotFoundException("User Record Not Found in Admin Portal.");
					}
				}
				else
				{
					log.info("Group not exist. Please input a valid AD login.");
					throw new UsernameNotFoundException("User Record Not Found in Admin Portal.");
				}

			}
			//LDAP END
			else{
				if(loginId.equalsIgnoreCase("sebadmin"))  //sebadmin = administrator, password = sebadmin
				 //sebadmin = administrator, password = sebadmin
				{
					log.info("Administrator login >> " + loginId);

					AdminUser secuser = loginDAO.getUserDetails (loginId);

					String encryptedPassword = Encryptor.getMD5(password);

					if(encryptedPassword.equalsIgnoreCase(secuser.getPassword())){
						log.info("user is ACTIVE");
						return new MyAuthenticationToken(loginId, password, assembler.getAuthorities(secuser.getRole()),secuser);

					}else{
						log.info("Invalid password : " + loginId);
						throw new BadCredentialsException("Invalid Login ID or Password.");

					}
				}else{
					log.info("Login [" + loginId + "] not exist in AD");
					throw new UsernameNotFoundException("User Record Not Found in Admin Portal.");
				}
			}


			/*AdminUser userEntity = loginDAO.getUserDetails (name);

			if (userEntity == null) {
				throw new BadCredentialsException("Username not found.");
			}

			if (!password.equals(userEntity.getPassword())) {
				throw new BadCredentialsException("Wrong password.");
			}


			if (userEntity == null){
				System.out.println("user not found");
				throw new UsernameNotFoundException("user not found");
			}*/

			//UserDetails userDetails = assembler.buildUserFromUserEntity(userEntity);

			//System.out.println(">>>>>>>>>>"+ assembler.getAuthorities(userEntity.getRole()));

		} else {
			log.info("empty username and password");
			throw new UsernameNotFoundException("Login ID or Password is required.");
		}
		//return null;
	}

	/*@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(MyAuthenticationToken.class);
	}*/
	@Override
	public boolean supports(Class<? extends Object> authentication){
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication)
				&& authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
	
	/**
	 * Return the username/login of the user
	 */
	public static String getLoggedUserName() {
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
		
		String userName = null;
		if(authentication.getPrincipal() instanceof org.springframework.security.core.userdetails.User){
			org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
			userName = user.getUsername();
		}else{
			userName = (String) authentication.getPrincipal();
			authentication.getName();
			authentication.getPrincipal();
			authentication.getCredentials();
			authentication.isAuthenticated();
			authentication.getDetails();
		}
		return userName;
	}

	/**
	 * Return the list of user roles
	 */
	public static Collection<String> getLoggedUserRolesNames() {
		SecurityContext context = SecurityContextHolder.getContext();
		//GrantedAuthority[] authorities = context.getAuthentication().getAuthorities();
		Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) context.getAuthentication().getAuthorities();
		Collection<String> roles = new ArrayList<String>();
		if (authorities != null && authorities.size()>0) {
			for (GrantedAuthority authority : authorities) {
				roles.add(authority.getAuthority());
			}
		}
		return roles;
	}

	
	
	/**
	 * Return the informations/details relatives to the authenticated user
	 * @return
	 */
	public static AdminUser getCurrentUser(){
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
		AdminUser user = null;
		if(authentication instanceof MyAuthenticationToken){
			user = ((MyAuthenticationToken)authentication).getMyUser();
		}
		
		return user;
	}
	
	
}