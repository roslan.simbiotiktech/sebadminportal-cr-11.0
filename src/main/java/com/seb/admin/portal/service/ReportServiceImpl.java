package com.seb.admin.portal.service;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.seb.admin.portal.dao.ReportDAO;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.model.AuditTrailType;
import com.seb.admin.portal.model.BankResponse;
import com.seb.admin.portal.model.CustServiceCounterEdited;
import com.seb.admin.portal.model.CustomerAuditTrail;
import com.seb.admin.portal.model.MyBillPayReportReq;
import com.seb.admin.portal.model.MyBillPayReportResp;
import com.seb.admin.portal.model.PaymentGateway;
import com.seb.admin.portal.model.Report;

@Service
public class ReportServiceImpl implements ReportService{

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private ReportDAO reportDAO;


	/**============================================
	 * Audit Trails Report
	 *============================================*/
	public AuditTrailType findAuditByID(String methodName){
		return reportDAO.findAuditByID(methodName);
	}

	@Override
	@Transactional
	public int auditLog(String name , AuditTrail at) {		

		try{
			String ipAddress = request.getHeader("X-FORWARDED-FOR");  
			if (ipAddress == null) {  
				ipAddress = request.getRemoteAddr();  
			}

			AuditTrailType auType = reportDAO.findAuditByID(name);
			at.setAuditTrailTypeId(auType.getId());
			at.setIpAddress(ipAddress);

			return reportDAO.insertAuditTrail(at);

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return 0;

	}

	@Override
	public List<AuditTrail> findAllByAdmin(Date dateFrom, Date dateTo, int adminId) {
		return reportDAO.findAllByAdmin(dateFrom, dateTo, adminId);
	}

	@Override
	public List<AuditTrail> findAllByDate(Date dateFrom, Date dateTo) {
		return reportDAO.findAllByDate(dateFrom, dateTo);
	}

	@Override
	public List<AuditTrailType> findAllAuditType() throws SQLException {		
		return reportDAO.findAllAuditType();
	}

	@Override
	public List<AuditTrail> findAllByType(Date dateFrom, Date dateTo, int typeId) {
		return reportDAO.findAllByType(dateFrom, dateTo, typeId);
	}

	@Override
	public List<AuditTrail> findAllByTypeAdmin(Date dateFrom, Date dateTo, int adminId, int typeId) {
		return reportDAO.findAllByTypeAdmin(dateFrom, dateTo, adminId, typeId);
	}

	/**============================================
	 * Make A Report
	 *============================================*/
	public List<Report> findAllMakeReportByCondition(Date dateFrom, Date dateTo, String source, String type, String status) {		
		source = source.equalsIgnoreCase("ALL") ? "%" : source.toUpperCase();
		type = type.equalsIgnoreCase("ALL") ? "%" : type.toUpperCase();
		status = status.equalsIgnoreCase("ALL") ? "%" : status.toUpperCase();		

		System.out.println("source>"+source);
		System.out.println("type>"+type);
		System.out.println("status>"+status);
		return reportDAO.findAllMakeReportByCondition(dateFrom, dateTo, source, type,status);
	}

	@Override
	public Report findMakeAReportById(int id) throws SQLException {
		return reportDAO.findMakeAReportById(id);
	}
	
	@Override
	@Transactional
	public Report updateMakeAReport(Report report,String loginId,String createdDatetime) throws SQLException{
		return reportDAO.updateMakeAReport(report,loginId,createdDatetime);
	}
	


	/**============================================
	 * Customer Activities Report
	 *============================================*/
	@Override
	public List<CustomerAuditTrail> findAllCustAuditReport(Date dateFrom, Date dateTo, String email) throws Exception{
		return reportDAO.findAllCustAuditReport(dateFrom, dateTo, email);
	}
	
	/**============================================
	 * User Statistic Report
	 *============================================*/
	@Override
	public long findTotalByStatus(String status, Date dateFrom, Date dateTo) throws Exception {
		// TODO Auto-generated method stub
		return reportDAO.findTotalByStatus(status, dateFrom, dateTo);
	}
	
	/**============================================
	 *Bill Payment Report
	 *============================================*/
	@Override
	public MyBillPayReportResp findAllBillPayReport(MyBillPayReportReq billPayReq) throws Exception {
		return reportDAO.findAllBillPayReport(billPayReq);
	}
	
	@Override
	public List<PaymentGateway> findAllPaymentGateway() throws SQLException {
		// TODO Auto-generated method stub
		return reportDAO.findAllPaymentGateway();
	}

	@Override
	public PaymentGateway findPaymentGatewayByID(String id) throws Exception {
		// TODO Auto-generated method stub
		return reportDAO.findPaymentGatewayByID(id);
	}

	@Override
	public List<BankResponse> findByMerchantidAndResponsecode(String merchantId, String responseCode)
			throws SQLException {
		// TODO Auto-generated method stub
		return reportDAO.findByMerchantidAndResponsecode(merchantId, responseCode);
	}

	@Override
	public Report findMakeAReportByIdAndDate(int id, String createdDatetime) throws Exception {
		// TODO Auto-generated method stub
		return reportDAO.findMakeAReportByIdAndDate( id, createdDatetime);
	}

}
