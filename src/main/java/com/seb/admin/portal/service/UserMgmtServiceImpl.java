package com.seb.admin.portal.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.seb.admin.portal.dao.UserMgmtDAO;
import com.seb.admin.portal.model.AccessRight;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.PortalModule;

@Service
public class UserMgmtServiceImpl implements UserMgmtService{

	@Autowired
	private UserMgmtDAO userMgmtDao;


	@Override
	public List<AccessRight>  findAllAccessById(int adminId) throws SQLException {
		return userMgmtDao.findAllAccessById(adminId);
	}

	@Override
	public List<AccessRight> findMainAccessById(int adminId, String role) throws SQLException {
		// TODO Auto-generated method stub
		return userMgmtDao.findMainAccessById(adminId, role);
	}


	@Override
	public List<PortalModule> findSubModuleByModuleCode(int adminId, String parentCode, int level, String role) throws SQLException {
		return userMgmtDao.findSubModuleByModuleCode(adminId, parentCode, level, role);
	}

	@Override
	public List<PortalModule> findAllModuleByRole(String role) throws SQLException {
		return userMgmtDao.findAllModuleByRole(role);
	}

	@Override
	public List<PortalModule> findAllModuleByRole2(String role) throws SQLException{
		return userMgmtDao.findAllModuleByRole2(role);
	}

	@Override
	public PortalModule findModuleById(int moduleId) throws SQLException {
		return userMgmtDao.findModuleById(moduleId);
	}

	@Override
	@Transactional
	public boolean saveNewAdminUser(AdminUser newAdmin, List<String> mainMod) throws SQLException{

		AdminUser myAdmin = userMgmtDao.saveNewAdmin(newAdmin);

		List<Integer> modIds = new ArrayList<Integer>();
		try {
			modIds = userMgmtDao.findPortalModuleIdsByCodeNRole(mainMod,newAdmin.getRole());

			if(newAdmin!=null){
				for(Integer  m : modIds){
					AccessRight acc = new AccessRight();
					acc.setAdminUserId(myAdmin.getId());
					acc.setPortalModuleId(m);
					userMgmtDao.saveAccessRight(acc);
				}	
			}

			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<PortalModule> findSelectedModulesByCode(List<String> moduleCodes) throws SQLException {
		return userMgmtDao.findSelectedModulesByCode(moduleCodes);
	}

	@Override
	public List<AccessRight> findUserManagement(int loginId) throws SQLException {
		return userMgmtDao.findUserManagement(loginId);
	}

	@Override
	public List<AccessRight> findExistingAccessByAdminId(int adminId) throws SQLException {
		return userMgmtDao.findExistingAccessByAdminId(adminId);
	}

	@Override
	@Transactional
	public AdminUser updateAdminUser(AdminUser a0) throws SQLException {
		return userMgmtDao.updateAdminUser(a0);
	}
	
	@Override
	@Transactional
	public AdminUser updateAdminUserAD(AdminUser a0) throws SQLException {
		return userMgmtDao.updateAdminUserAD(a0);
	}

	@Override
	public List<Integer> findPortalModuleIdsByCode(List<String> moduleCodes) throws SQLException {
		return userMgmtDao.findPortalModuleIdsByCode(moduleCodes);
	}
	
	@Override
	public List<PortalModule> findSCLModule(String role) throws SQLException {
		return userMgmtDao.findSCLModule(role);
	}

	@Override
	@Transactional
	public AccessRight updateAccessRight(AccessRight a0) throws SQLException {
		return userMgmtDao.updateAccessRight(a0);
	}
	
	@Override
	public List<AccessRight> findExistModule(AccessRight a0) throws SQLException{
		return userMgmtDao.findExistModule(a0);
	}


	@Override
	@Transactional
	public AccessRight saveAccessRight(AccessRight acc) throws SQLException {
		List <AccessRight> existing = new ArrayList<AccessRight>();
		existing = userMgmtDao.findExistModule(acc);
		if(existing!=null && existing.size()>0){
			return userMgmtDao.updateAccessRight(acc);
		}else{
			return userMgmtDao.saveAccessRight(acc);
		}
		
	}


}
