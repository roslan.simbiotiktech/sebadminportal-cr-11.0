<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>

<style type="text/css">
@import
	url("${pageContext.request.contextPath}/resources/dist/css/dragdrop.css")
	;
</style>

</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->

		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">Maintenance &gt; FAQ &gt; Sequence</div>

						<div class="panel-body">
							<form id="searchFAQSort" name="searchFAQSort" method="post" action="${pageContext.request.contextPath}/admin/editFAQSequence.do"
								class="form-horizontal">
								${successMsg}${errorMsg}
								<div id="successMsg" style="display: none"></div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Type <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<select class="form-control capital" id="selectType" name="selectType">
											<option value="">Please Select</option>
											<c:forEach items="${typeList}" var="item">
												<option value="${item.id}">${item.description}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input type="hidden" id="displayType" name="displayType" value="" /> <input name="${_csrf.parameterName}" type="hidden"
											value="${_csrf.token}">
										<button class="btn btn-primary" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
							</form>

							<div id="resultDiv" style="display: none; margin-top: 10%">
								<form id="updateFAQForm" name="updateFAQForm" action="${pageContext.request.contextPath}/admin/updateFAQSequence" class="form-horizontal"
									method="post">
									<h4>
										Search Results : Category : <b>${displayType}</b>
									</h4>
									<div class="form-group">
										<div class="alert alert-danger" id="messages" style="display: none;"></div>
									</div>
									<br>
									<div class="dhe-example-section" id="ex-1-1">
										<div class="dhe-example-section-content">
											<c:if test="${not empty faqList}">
												<p class="smallnotes">
													<em>Notes: Drag/drop the question to order FAQ sequences.</em>
												</p>
												<div id="example-1-1">
													<ul class="sortable-list">
														<c:forEach items="${faqList}" var="item" varStatus="loop">
															<li id="${item.id}" class="sortable-item"><div class="form-group">
																	<p class="col-md-12 text-left" style="padding-left: 4%">${item.questionEn}</p>
																</div></li>
														</c:forEach>
													</ul>
												</div>
												<!--#example-1-1  -->
											</c:if>
										</div>

										<!-- END: XHTML for example 1.1 -->

									</div>
									<!--#dhe-example-section-content  -->
									<c:if test="${not empty faqList && faqList.size() > 0}">
										<div class="form-group">
											<div class="col-md-5 col-md-offset-3 text-right">
												<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
												<button id="submitBtn" class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal">Update</button>
												<button class="btn btn-warning" id="cancelBtn2" type="reset">Cancel</button>
												<input type="hidden" id="sort" name="sort" value="" /> <input type="hidden" id="displayType" name="displayType" value="${displayType }" />
											</div>
										</div>
									</c:if>
							</div>
							<!--#resultDiv  -->
							<!-- Add Confirmation Modal -->
							<div class="modal" id="modal" tabindex="-1">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button class="close" data-dismiss="modal" type="button">
												<span>&times;</span><span class="sr-only">Close</span>
											</button>
											<h2 class="modal-title">Confirmation</h2>
										</div>
										<div class="modal-body" id="add">Are you sure you want to update FAQ sequence?</div>
										<div class="modal-footer">
											<button class="btn btn-primary" id="add" type="button">Update</button>
											<button class="btn btn-warning" data-dismiss="modal" type="button">Close</button>
										</div>
									</div>
								</div>
							</div>
							<!-- End Confirmation Modal -->
							</form>
						</div>
						<!-- resultDiv -->
					</div>
					<!-- #panel-body -->
				</div>
				<!-- #panel panel-default -->
			</div>
			<!-- /.col-md-12 -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /#page-wrapper -->
	</div>
</body>
<script type="text/javascript">
//Get all items from a container
function getItems(container) {
        var columns = [];
        $(container + ' ul').each(function() {
            columns.push($(this).sortable('toArray').join(','));
        });
        return columns.join('|');
    }
    // itemStr = 'A,B|C,D|E'
$(document).ready(function() {
    $('#selectType').change(function(e) {
        $('#displayType').val($(this).find("option:selected").text());
    });
    if (${faqList != null && faqList.size() >= 0}) {
        $('#resultDiv').show();
        // Example 1.1: A single sortable list
        $('#example-1-1 .sortable-list').sortable({
            placeholder: 'placeholder',
        });
    }
    $("button#add").click(function() {
    	$('#modal').modal('hide');
        var itemStr = getItems('#example-1-1');
        console.log(">>" + itemStr);
        $('#sort').val(itemStr);
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        var rows_selected = new Array();
        rows_selected.push({
            sort: $('#sort').val(),
            displayType: $('#displayType').val()
        })
        $.ajax({
            url: "${pageContext.request.contextPath}/admin/updateFAQSequence.do",
            type: "POST",
            dataType: "json",
            data: JSON.stringify(rows_selected),
            beforeSend: function(xhr) {
                xhr.setRequestHeader(header, token);
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
                sebApp.showIndicator();
            },
            success: function(data) {
                if (data.success != null && data.success.length > 0) {
                    $('#successMsg').html(data.success).show();
                } else if (data.error != null && data.error.length > 0) {
                    $('#errorMsg').html(data.error).show();
                }
                rows_selected = new Array();
                window.scrollTo(0, 5);
                $('#resultDiv').hide();
            },
            failure: function(data) {
                alert("Fail To Connect");
            },
            complete: function(e) {
                sebApp.hideIndicator();
            }
        });
    })
});
</script>
</html>
