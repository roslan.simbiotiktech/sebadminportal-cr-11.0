package com.seb.admin.portal.staging;

public class TestRef {


	public static String getReferenceNo(String prefix, long referenceNo){
		return String.format("%s%08d", prefix, referenceNo);
	}

	
	public static void main(String[] args) {
		System.out.println(getReferenceNo("M", 500000));
	}
}

