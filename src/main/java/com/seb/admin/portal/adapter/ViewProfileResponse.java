package com.seb.admin.portal.adapter;

public class ViewProfileResponse {
	private long respStat;
	private String errCode;
	private String errDesc;	
	
	private String loginId;
	private String name;
	private String nricOrPassport;
	private String email;
	private String mobileNumber;
	private String homeTel;
	private String officeTel;
	private String preferredCommMethod;
	private String accountStatus;
	private String lastLoginAt;
	private String remark;
	
	private ContractDetail  contractDetail;
	
	private String contractAccNo;
	
	public long getRespStat() {
		return respStat;
	}
	public void setRespStat(long respStat) {
		this.respStat = respStat;
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public String getErrDesc() {
		return errDesc;
	}
	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNricOrPassport() {
		return nricOrPassport;
	}
	public void setNricOrPassport(String nricOrPassport) {
		this.nricOrPassport = nricOrPassport;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getHomeTel() {
		return homeTel;
	}
	public void setHomeTel(String homeTel) {
		this.homeTel = homeTel;
	}
	public String getOfficeTel() {
		return officeTel;
	}
	public void setOfficeTel(String officeTel) {
		this.officeTel = officeTel;
	}
	public String getPreferredCommMethod() {
		return preferredCommMethod;
	}
	public void setPreferredCommMethod(String preferredCommMethod) {
		this.preferredCommMethod = preferredCommMethod;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getLastLoginAt() {
		return lastLoginAt;
	}
	public void setLastLoginAt(String lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}
	public ContractDetail getContractDetail() {
		return contractDetail;
	}
	public void setContractDetail(ContractDetail contractDetail) {
		this.contractDetail = contractDetail;
	}
	@Override
	public String toString() {
		return "ViewProfileResponse [respStat=" + respStat + ", errCode=" + errCode + ", errDesc=" + errDesc
				+ ", loginId=" + loginId + ", name=" + name + ", nricOrPassport=" + nricOrPassport + ", email=" + email
				+ ", mobileNumber=" + mobileNumber + ", homeTel=" + homeTel + ", officeTel=" + officeTel
				+ ", preferredCommMethod=" + preferredCommMethod + ", accountStatus=" + accountStatus + ", lastLoginAt="
				+ lastLoginAt + ", contractDetail=" + contractDetail + "]";
	}
	public String getContractAccNo() {
		return contractAccNo;
	}
	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	

	
	

}
