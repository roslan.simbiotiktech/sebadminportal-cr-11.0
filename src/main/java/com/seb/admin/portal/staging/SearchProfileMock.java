package com.seb.admin.portal.staging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.seb.admin.portal.adapter.Contract;
import com.seb.admin.portal.adapter.USER;

public class SearchProfileMock {

	private static List<HttpMessageConverter<?>> getMessageConverters() {
		List<HttpMessageConverter<?>> converters = new ArrayList<HttpMessageConverter<?>>();
		converters.add(new MappingJackson2HttpMessageConverter());
		return converters;
	}   

	public static void main(String[] args) {

		System.out.println("getCms_search_profile");
		String serviceURL = "http://1.32.122.125:8080/SEB-Middleware/api/cms_search_profile";  
		RestTemplate restTemplate = new RestTemplate();  
		restTemplate.getMessageConverters().add( new MappingJackson2HttpMessageConverter() );      


		HttpHeaders headers = new HttpHeaders();  
		headers.setContentType(MediaType.APPLICATION_JSON);  
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));


		try { 
			System.out.println("Calling");



			/*	MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			   map.add("LOGIN_ID", "peirhwa87@gmail.com");
			    map.add("EMAIL", "");
			    map.add("NAME", "");
			    map.add("MOBILE_NUMBER", "");
			    map.add("CONTRACT_ACC_NO", "");
			    map.add("name", "xx");
				map.add("password", "xx");
				String result = restTemplate.postForObject(serviceURL, map, String.class);
				System.out.println(result);*/

			/*Map objMap =new LinkedHashMap();
			objMap.put("LOGIN_ID", "");
			objMap.put("EMAIL", "");
			objMap.put("NAME", "");
			objMap.put("MOBILE_NUMBER", "");
			objMap.put("CONTRACT_ACC_NO", "201066695109");//201066695109
			String jsonText = JSONValue.toJSONString(objMap);*/
			
			Map objMap =new LinkedHashMap();
			Map pagingObj =new LinkedHashMap();

			pagingObj.put("PAGE", 1);
			pagingObj.put("RECORDS_PER_PAGE", 50);
			objMap.put("PAGING", pagingObj);
			objMap.put("LOGIN_ID", "jeffrey.tan@isentric.com");//woon.san.yap@isentric.com
			objMap.put("EMAIL", "");
			objMap.put("NAME", "");
			objMap.put("MOBILE_NUMBER", "");
			objMap.put("CONTRACT_ACC_NO", "");//201057928110
			String jsonText = JSONValue.toJSONString(objMap);
			System.out.println(jsonText);




			HttpEntity<String> entity = new HttpEntity<String>(jsonText,headers);
			ResponseEntity<String> result = restTemplate.exchange(serviceURL, HttpMethod.POST, entity, String.class);
			System.out.println(result.getBody());

			long RESP_STAT = -1;
			String ERR_CODE = "";
			StringBuilder ERR_DESC = new StringBuilder(""); 

			
			List<USER> usersList = new ArrayList<USER>();

			if(result.getStatusCode() == HttpStatus.OK){
				JSONParser parser = new JSONParser();
				try{
					Object obj = parser.parse(result.getBody());
					JSONObject jsonObject = (JSONObject) obj;

					// handle a structure into the json object
					JSONObject subObject = (JSONObject) jsonObject.get("STATUS");
					RESP_STAT = (Long) subObject.get("RESP_STAT");

					if(RESP_STAT!=0){
						JSONObject errorObj = (JSONObject) subObject.get("ERROR");	
						ERR_CODE = (String) errorObj.get("ERR_CODE");

						JSONArray errorList = (JSONArray) errorObj.get("ERR_DESC");
						Iterator<String> iterator = errorList.iterator();
						while (iterator.hasNext()) {
							ERR_DESC.append(iterator.next()+System.getProperty("line.separator"));
						}
					}else{// callback:sucess
						JSONArray userObject = (JSONArray) jsonObject.get("USERS");						
						Iterator i = userObject.iterator();
						
						while (i.hasNext()) {
							USER myUser = new USER();
							JSONObject innerUserObj = (JSONObject) i.next();
							myUser.setStatus((String) innerUserObj.get("STATUS"));
							myUser.setLoginId((String) innerUserObj.get("LOGIN_ID"));
							myUser.setMobileNumber((String) innerUserObj.get("MOBILE_NUMBER"));
							//myUser.setCONTRACT_ACC_NO(cONTRACT_ACC_NO);
							myUser.setEmail((String) innerUserObj.get("EMAIL"));
							myUser.setLastLoginAt((String) innerUserObj.get("LAST_LOGIN_AT"));
							myUser.setName((String) innerUserObj.get("NAME"));
							System.out.println("name>>"+innerUserObj.get("NAME"));
							
							JSONArray contractObject = (JSONArray) innerUserObj.get("CONTRACT_SUBSCRIBED");	
							if(contractObject!=null){
								Iterator c = contractObject.iterator();
								List<Contract> contractList = new ArrayList<Contract>();
								while (c.hasNext()) {
									Contract contract = new Contract();
									JSONObject innerContractObj = (JSONObject) c.next();
									contract.setAccountName((String) innerContractObj.get("ACCOUNT_NAME"));
									contract.setAccountNumber((String) innerContractObj.get("ACCOUNT_NUMBER"));
									contract.setSubscriptionStatus((String) innerContractObj.get("SUBSCRIPTION_STATUS"));
									contractList.add(contract);
								}//end loop contractList
								myUser.setContractSubscribed(contractList);
							
							}
							usersList.add(myUser);

						}//end USERS
					}


					for(USER u : usersList){
						System.out.println(">>>"+u.toString());
					}
				

					//System.out.println("\nCompany List:");
					/*Iterator<String> iterator = companyList.iterator();
					while (iterator.hasNext()) {
						System.out.println(iterator.next());
					}*/
				}catch (Exception e) {
					e.printStackTrace();
				}
			}



			/*System.out.println(result.getBody());  
		System.out.println(result.getStatusCode());*/

		} catch (RestClientException re) {  
			System.out.println("Re");
			re.printStackTrace();
			if (re instanceof HttpClientErrorException) {  
				HttpClientErrorException he = (HttpClientErrorException) re;  
				System.out.println(he.getResponseBodyAsString());  
				System.out.println(he.getStatusCode());  
			} else if (re instanceof HttpServerErrorException) {  
				HttpServerErrorException he = (HttpServerErrorException) re;  
				System.out.println(he.getResponseBodyAsString());  
				System.out.println(he.getStatusCode());  
			}   
		} catch (Exception e) {  
			e.printStackTrace();  
		}  

	}
}
