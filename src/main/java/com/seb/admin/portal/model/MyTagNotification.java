package com.seb.admin.portal.model;

import java.sql.Timestamp;

public class MyTagNotification {
	
	private int notificationId;
	private Integer associatedId;
	private Timestamp createdDatetime;
	private String notificationType;
	private String powerAlertType;
	private Timestamp pushDatetime;
	private int sendCounter;
	private Timestamp sendingDatetime;
	private String status;
	private String tagName;
	private String text;
	private String title;
	
	public int getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}
	public Integer getAssociatedId() {
		return associatedId;
	}
	public void setAssociatedId(Integer associatedId) {
		this.associatedId = associatedId;
	}
	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}
	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public String getPowerAlertType() {
		return powerAlertType;
	}
	public void setPowerAlertType(String powerAlertType) {
		this.powerAlertType = powerAlertType;
	}
	public Timestamp getPushDatetime() {
		return pushDatetime;
	}
	public void setPushDatetime(Timestamp pushDatetime) {
		this.pushDatetime = pushDatetime;
	}
	public int getSendCounter() {
		return sendCounter;
	}
	public void setSendCounter(int sendCounter) {
		this.sendCounter = sendCounter;
	}
	public Timestamp getSendingDatetime() {
		return sendingDatetime;
	}
	public void setSendingDatetime(Timestamp sendingDatetime) {
		this.sendingDatetime = sendingDatetime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getNewsId() {
		return newsId;
	}
	public void setNewsId(int newsId) {
		this.newsId = newsId;
	}
	public String getNewsTitle() {
		return newsTitle;
	}
	public void setNewsTitle(String newsTitle) {
		this.newsTitle = newsTitle;
	}
	public Timestamp getNewsStartDatetime() {
		return newsStartDatetime;
	}
	public void setNewsStartDatetime(Timestamp newsStartDatetime) {
		this.newsStartDatetime = newsStartDatetime;
	}
	public Timestamp getNewsEndDatetime() {
		return newsEndDatetime;
	}
	public void setNewsEndDatetime(Timestamp newsEndDatetime) {
		this.newsEndDatetime = newsEndDatetime;
	}
	public int getPowerAlertId() {
		return powerAlertId;
	}
	public void setPowerAlertId(int powerAlertId) {
		this.powerAlertId = powerAlertId;
	}
	public String getPowerStation() {
		return powerStation;
	}
	public void setPowerStation(String powerStation) {
		this.powerStation = powerStation;
	}
	public String getPowerTitle() {
		return powerTitle;
	}
	public void setPowerTitle(String powerTitle) {
		this.powerTitle = powerTitle;
	}
	

	//news
	private int newsId;
	private String newsTitle;
	private Timestamp newsStartDatetime;
	private Timestamp newsEndDatetime;
	
	//power alert
	private int powerAlertId;
	private String powerStation;
	private String powerTitle;
	
}
