package com.seb.admin.portal.spg.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentDetailReportRequest extends SPGAPIGeneral {
	@JsonProperty("payment_id")
	private long payment_id;

	public long getPayment_id() {
		return payment_id;
	}

	public void setPayment_id(long payment_id) {
		this.payment_id = payment_id;
	}

	@Override
	public String toString() {
		return "payment_id:[" + payment_id + "]";
	}
	
	
	
}
