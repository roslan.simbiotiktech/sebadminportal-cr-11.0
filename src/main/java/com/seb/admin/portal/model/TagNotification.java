package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import org.apache.commons.lang.StringEscapeUtils;

import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;


/**
 * The persistent class for the TAG_NOTIFICATION database table.
 * 
 */
@Entity
@Table(name="TAG_NOTIFICATION" , schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="TagNotification.findAll", query="SELECT t FROM TagNotification t"),
	@NamedQuery(name="TagNotification.findAllByTitleId", query="SELECT t FROM TagNotification t WHERE t.associatedId =:associatedId"), 
	@NamedQuery(name="TagNotification.findContentByAssIdAndType", query="SELECT t FROM TagNotification t WHERE t.associatedId =:associatedId AND t.notificationType =:notificationType ORDER BY t.notificationId asc"),
	@NamedQuery(name="TagNotification.findDistinctTitleIdBtwDate", query="SELECT distinct(t.associatedId) FROM TagNotification t WHERE t.associatedId IS NOT NULL AND UPPER(t.notificationType) = :notifType  AND UPPER(t.tagName) LIKE :tagName"),
	@NamedQuery(name="TagNotification.findDistinctTitleIdBtwDatePower", query="SELECT distinct(t.associatedId) FROM TagNotification t WHERE t.associatedId IS NOT NULL AND UPPER(t.notificationType) = :notifType  AND UPPER(t.tagName) LIKE :tagName AND UPPER(t.powerAlertType) LIKE :powerName"),
	@NamedQuery(name="TagNotification.findAllFreeTextByIds", query="SELECT t FROM TagNotification t WHERE t.associatedId IS NULL AND UPPER(t.tagName) = 'NOTIFICATION_GENERAL'"),
	@NamedQuery(name="TagNotification.findAllByCondition1", query="SELECT t FROM TagNotification t WHERE t.pushDatetime >=:startDate AND t.pushDatetime<=:endDate AND  UPPER(t.notificationType) =:notifType  AND (t.associatedId =:associatedId OR t.associatedId IS NULL) AND UPPER(t.text) LIKE :msg"),
	@NamedQuery(name="TagNotification.findSubTitleIdBtwDate", query="SELECT distinct(t.associatedId) FROM TagNotification t WHERE t.associatedId IS NOT NULL AND UPPER(t.notificationType) = :notifType  AND UPPER(t.tagName) LIKE :tagName AND  t.pushDatetime >= :startDate AND t.pushDatetime<= :endDate"),
	@NamedQuery(name="TagNotification.findSubTitleIdBtwDatePower", query="SELECT distinct(t.associatedId) FROM TagNotification t WHERE t.associatedId IS NOT NULL AND UPPER(t.notificationType) = :notifType  AND UPPER(t.tagName) LIKE :tagName AND UPPER(t.powerAlertType) LIKE :powerName AND  t.pushDatetime >= :startDate AND t.pushDatetime<= :endDate"),


})
public class TagNotification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="NOTIFICATION_ID", unique=true, nullable=false)
	private int notificationId;

	@Column(name="ASSOCIATED_ID" , nullable=true)
	private Integer associatedId;

	@Column(name="CREATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="NOTIFICATION_TYPE", nullable=true , length=20)
	private String notificationType;

	@Column(name="POWER_ALERT_TYPE", length=50)
	private String powerAlertType;

	@Column(name="PUSH_DATETIME", nullable=false)
	private Timestamp pushDatetime;

	@Column(name="SEND_COUNTER", nullable=false)
	private int sendCounter;

	@Column(name="SENDING_DATETIME")
	private Timestamp sendingDatetime;

	@Column(name="STATUS", nullable=false, length=10)
	private String status;

	@Column(name="TAG_NAME", nullable=false, length=50)
	private String tagName;

	@Column(name="TEXT", nullable=false, length=120)
	//@Lob
	private String text;
	
	@JoinColumn(name = "ASSOCIATED_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY )
	private PowerAlert  powerAlert;

	public TagNotification() {
	}

	public int getNotificationId() {
		return this.notificationId;
	}

	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}

	public Integer getAssociatedId() {
		return this.associatedId;
	}

	public void setAssociatedId(Integer associatedId) {
		this.associatedId = associatedId;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getNotificationType() {
		return this.notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getPowerAlertType() {
		return this.powerAlertType;
	}

	public void setPowerAlertType(String powerAlertType) {
		this.powerAlertType = powerAlertType;
	}

	public Timestamp getPushDatetime() {
		return this.pushDatetime;
	}

	public void setPushDatetime(Timestamp pushDatetime) {
		this.pushDatetime = pushDatetime;
	}

	public int getSendCounter() {
		return this.sendCounter;
	}

	public void setSendCounter(int sendCounter) {
		this.sendCounter = sendCounter;
	}

	public Timestamp getSendingDatetime() {
		return this.sendingDatetime;
	}

	public void setSendingDatetime(Timestamp sendingDatetime) {
		this.sendingDatetime = sendingDatetime;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTagName() {
		return this.tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = StringEscapeUtils.unescapeHtml(text);
		//this.text = text;
	}
	
	public PowerAlert getPowerAlert() {
		return powerAlert;
	}

	public void setPowerAlert(PowerAlert powerAlert) {
		this.powerAlert = powerAlert;
	}

	@Override
	public String toString() {
		return "TagNotification [notificationId=" + notificationId + ", associatedId=" + associatedId
				+ ", createdDatetime=" + createdDatetime + ", notificationType=" + notificationType
				+ ", powerAlertType=" + powerAlertType + ", pushDatetime=" + pushDatetime + ", sendCounter="
				+ sendCounter + ", sendingDatetime=" + sendingDatetime + ", status=" + status + ", tagName=" + tagName
				+ ", text=" + text + ", powerAlert=" + powerAlert + "]";
	}

	
	
	

}