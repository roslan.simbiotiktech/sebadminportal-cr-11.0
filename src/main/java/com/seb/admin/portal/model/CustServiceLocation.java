package com.seb.admin.portal.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.seb.admin.portal.constant.ApplicationConstant;


/**
 * The persistent class for the CUST_SERVICE_LOCATION database table.
 * 
 */
@Entity
@Table(name="CUST_SERVICE_LOCATION" , schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="CustServiceLocation.findAll", query="SELECT c FROM CustServiceLocation c WHERE c.deletedFlag=0 ORDER BY c.station"),
	@NamedQuery(name="CustServiceLocation.findRegion", query="SELECT distinct(c.region) FROM CustServiceLocation c WHERE c.deletedFlag=0 ORDER BY c.region"),
	@NamedQuery(name="CustServiceLocation.findStation", query="SELECT c FROM CustServiceLocation c WHERE c.region = :region AND c.deletedFlag=0 ORDER BY c.station"),
	@NamedQuery(name="CustServiceLocation.findAllStation", query="SELECT c FROM CustServiceLocation c WHERE c.deletedFlag=0 ORDER BY c.station")

})
@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
public class CustServiceLocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(length=100)
	private String region;

	@Column(length=100)
	private String station;

	//bi-directional many-to-one association to CustServiceCounter
	@JsonIgnore
	@OneToMany(mappedBy="custServLoc" , fetch = FetchType.LAZY)
	private List<CustServiceCounter> custServCounter;

	@JsonIgnore
	//bi-directional many-to-one association to CustServiceCounter
	@OneToMany(mappedBy="editedCustServiceLocation")
	private List<CustServiceCounterEdited> custServiceCounterEditeds;

	public CustServiceLocation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getRegion() {
		return this.region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStation() {
		return this.station;
	}

	public void setStation(String station) {
		this.station = station;	}

	/*@JsonManagedReference //for children table*/
	public List<CustServiceCounter> getCustServCounter() {
		return this.custServCounter;
	}

	public void setCustServCounter(List<CustServiceCounter> custServCounter) {
		this.custServCounter = custServCounter;
	}

	public CustServiceCounter addCustServCounter(CustServiceCounter custServCounter) {
		getCustServCounter().add(custServCounter);
		custServCounter.setCustServLoc(this);
		return custServCounter;
	}

	public CustServiceCounter removeCustServCounter(CustServiceCounter custServCounter) {
		getCustServCounter().remove(custServCounter);
		custServCounter.setCustServLoc(null);

		return custServCounter;
	}
	
	/*@JsonManagedReference //for children table*/
	public List<CustServiceCounterEdited> getCustServiceCounterEditeds() {
		return this.custServiceCounterEditeds;
	}

	public void setCustServiceCounterEditeds(List<CustServiceCounterEdited> custServiceCounterEditeds) {
		this.custServiceCounterEditeds = custServiceCounterEditeds;
	}

	public CustServiceCounterEdited addCustServiceCounterEdited(CustServiceCounterEdited custServiceCounterEdited) {
		getCustServiceCounterEditeds().add(custServiceCounterEdited);
		custServiceCounterEdited.setEditedCustServiceLocation(this);

		return custServiceCounterEdited;
	}

	public CustServiceCounterEdited removeCustServiceCounterEdited(CustServiceCounterEdited custServiceCounterEdited) {
		getCustServiceCounterEditeds().remove(custServiceCounterEdited);
		custServiceCounterEdited.setEditedCustServiceLocation(null);

		return custServiceCounterEdited;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((custServCounter == null) ? 0 : custServCounter.hashCode());
		result = prime * result + deletedFlag;
		result = prime * result + id;
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		result = prime * result + ((station == null) ? 0 : station.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustServiceLocation other = (CustServiceLocation) obj;
		if (custServCounter == null) {
			if (other.custServCounter != null)
				return false;
		} else if (!custServCounter.equals(other.custServCounter))
			return false;
		if (deletedFlag != other.deletedFlag)
			return false;
		if (id != other.id)
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.equals(other.region))
			return false;
		if (station == null) {
			if (other.station != null)
				return false;
		} else if (!station.equals(other.station))
			return false;
		return true;
	}



}