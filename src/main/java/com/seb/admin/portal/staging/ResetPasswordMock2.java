package com.seb.admin.portal.staging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.seb.admin.portal.adapter.ContractDetail;
import com.seb.admin.portal.adapter.USER;
import com.seb.admin.portal.adapter.ViewProfileResponse;

public class ResetPasswordMock2 {


	public static void main(String[] args) {

		System.out.println("[CMS Reset Customer Password]");
		String serviceURL = "http://219.92.232.213:8080/SEB-Middleware/api/cms_reset_customer_password";  
		RestTemplate restTemplate = new RestTemplate();  
		restTemplate.getMessageConverters().add( new MappingJackson2HttpMessageConverter() );      


		HttpHeaders headers = new HttpHeaders();  
		headers.setContentType(MediaType.APPLICATION_JSON);  
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));


		try { 
			
			System.out.println("Calling");

			//Map objMap =new LinkedHashMap();
			JSONObject objMap = new JSONObject();
			
		//	pagingObj.put("PAGE", 1);
			//pagingObj.put("RECORDS_PER_PAGE", 50);
			
			objMap.put("LOGIN_ID", "woon.san.yap@isentric.com");
			//objMap.put("DATE_TO", "30/11/2015 23:59:59");			
			//objMap.put("PAGING", pagingObj);
			
			//objMap.put("LOGIN_ID", "woon.san.yap@isentric.com");
			//objMap.put("MOBILE_NUMBER", "");
			//objMap.put("CONTRACT_ACC_NO", "");//201057928110
			String jsonText = JSONValue.toJSONString(objMap);



			HttpEntity<String> entity = new HttpEntity<String>(jsonText,headers);
			ResponseEntity<String> result = restTemplate.exchange(serviceURL, HttpMethod.POST, entity, String.class);
			// Get the response as a string
			//MyResponse result = restTemplate.postForObject(serviceURL,  entity, MyResponse.class);
			//System.out.println("Name:"+result.getBody().getSTATUS());
			//System.out.println("Village:"+result.getBody().getUSERS()); 
			//System.out.println("response="+result.getStatusCode());
			////System.out.println("response="+result.getHeaders());
			System.out.println(result.getBody());

			long RESP_STAT = -1;
			String ERR_CODE = "";
			StringBuilder ERR_DESC = new StringBuilder(""); 
			ViewProfileResponse resp = new ViewProfileResponse();
			ContractDetail con = new ContractDetail();
			String PREFERRED_COMM_METHOD = "";
			
			List<USER> usersList = new ArrayList<USER>();

			if(result.getStatusCode() == HttpStatus.OK){
				JSONParser parser = new JSONParser();
				try{
					Object obj = parser.parse(result.getBody());
					JSONObject jsonObject = (JSONObject) obj;

					// handle a structure into the json object
					JSONObject subObject = (JSONObject) jsonObject.get("STATUS");
					RESP_STAT = (Long) subObject.get("RESP_STAT");

					if(RESP_STAT!=0){
						JSONObject errorObj = (JSONObject) subObject.get("ERROR");	
						ERR_CODE = (String) errorObj.get("ERR_CODE");

						JSONArray errorList = (JSONArray) errorObj.get("ERR_DESC");
						Iterator<String> iterator = errorList.iterator();
						while (iterator.hasNext()) {
							ERR_DESC.append(iterator.next()+System.getProperty("line.separator"));
						}
					}else{// callback:sucess
						JSONObject contractDetailObj = (JSONObject) jsonObject.get("CONTRACT_DETAIL");	
						PREFERRED_COMM_METHOD = (String) jsonObject.get("PREFERRED_COMM_METHOD");
					}
				}catch (Exception e) {
					e.printStackTrace();
				}
			}

			System.out.println(resp.toString());


			/*System.out.println(result.getBody());  
		System.out.println(result.getStatusCode());*/

		} catch (RestClientException re) {  
			System.out.println("Re");
			re.printStackTrace();
			if (re instanceof HttpClientErrorException) {  
				HttpClientErrorException he = (HttpClientErrorException) re;  
				System.out.println(he.getResponseBodyAsString());  
				System.out.println(he.getStatusCode());  
			} else if (re instanceof HttpServerErrorException) {  
				HttpServerErrorException he = (HttpServerErrorException) re;  
				System.out.println(he.getResponseBodyAsString());  
				System.out.println(he.getStatusCode());  
			}   
		} catch (Exception e) {  
			e.printStackTrace();  
		}  

	}
}
