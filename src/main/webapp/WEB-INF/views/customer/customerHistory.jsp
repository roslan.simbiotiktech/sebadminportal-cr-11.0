<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="" name="description">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="font-weight: bold">Customer &gt; History</div>

						<div class="panel-body">
							<form action="${pageContext.request.contextPath}/admin/customerHistory.do" class="form-horizontal" id="searchHistoryForm"
								name="searchHistoryForm" method="post" name="searchHistoryForm">
								${successMsg}${errorMsg}
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Date From <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='dateFrom'>
											<input class="form-control datepicker" id="indateFrom" name="indateFrom" readonly type='text'> <span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Date To <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='dateTo'>
											<input class="form-control datepicker" id="indateTo" name="indateTo" readonly type='text'> <span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">Mobile Number :</p>

									<div class="col-lg-3">
										<input class="form-control" id="msisdn" maxlength="20" name="msisdn" placeholder="Mobile Number" type="text">
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">Contract Account Number :</p>

									<div class="col-lg-3">
										<input class="form-control" id="contractAccNo" name="contractAccNo" placeholder="Contract Account Number" type="text" maxlength="12">
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">Login ID :</p>

									<div class="col-lg-3">
										<input class="form-control" id="loginID" name="loginID" placeholder="Login ID" type="text" maxlength="50">
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" id="submitBtn" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
							</form>
							<div id="resultDiv" style="display: none; margin-top: 10%">
								<form action="${pageContext.request.contextPath}/admin/customerHistory.do" class="form-horizontal" id="resultForm" method="post"
									name="resultForm">
									<%-- <input type="text" id="dateFrom" name="dateFrom" value="${dateFrom}"/>
									<input type="text" id="dateTo" name="dateTo" value="${dateTo}"/>
									<input type="text" id="msisdn" name="msisdn" value="${msisdn}"/>
									<input type="text" id="contractAccNo" name="contractAccNo" value="${contractAccNo}"/>
									<input type="text" id="loginId" name="loginId" value="${loginId}"/> --%>

									<h4>
										Search Results : Date From :<b>${dateFrom}</b>, Date To :<b>${dateTo}</b> , Mobile Number :<b>${msisdn}</b><br> Contract Account Number
										:<b>${contractAccNo}</b>, Login Id :<b>${loginId}</b>
									</h4>
									<table cellspacing="0" class="table table-striped table-bordered" id="tblSearch">
										<thead>
											<tr>
												<th>No.</th>
												<th>Date</th>
												<th>Mobile Number</th>
												<th>Contract Account Number</th>
												<th>Login ID</th>
												<th>Activity</th>
											</tr>
										</thead>
										<tbody>
											<c:set var="name" scope="session" value="" />
											<c:if test="${not empty userList}">
												<c:forEach items="${userList}" var="item" varStatus="status">
													<c:choose>
														<c:when test="${status.count % 2 == 0}">
															<c:set var="css" scope="session" value="myeven" />
														</c:when>
														<c:otherwise>
															<c:set var="css" scope="session" value="myodd" />
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:if>
											<%-- 	<c:if test="${not empty custAuditList}">
												<c:forEach items="${custAuditList}" var="item" varStatus="status">
													<tr>
														<td>${status.index+1}</td>
														<td>${item.activityAt}</td>
														<td>${item.mobileNumber}</td>
														<td><c:if test="${not empty item.contractSubscribed}">
																<c:forEach items="${item.contractSubscribed}" var="con" varStatus="loop">
																${con.accountNumber}
																</c:forEach>
															</c:if></td>
														<td>${item.loginId}</td>
														<td>${item.activity}</td>
													</tr>
												</c:forEach>
											</c:if>
 --%>
										</tbody>
									</table>
								</form>
							</div>
							<!-- #resultDiv -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel panel-default -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
		</div>
	</div>
	<script type="text/javascript">
	function resetForm() {
    $('#').find('.has-error').removeClass("has-error");
    $('#searchHistoryForm').find('.has-success').removeClass("has-success");
    $('#searchHistoryForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    $('#searchHistoryForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    $('#searchHistoryForm').formValidation('resetForm', true);
    $('.form-group').find('small.help-block').hide();
    $('.form-group').removeClass('has-error has-feedback');
    $('.form-group').find('i.form-control-feedback').hide();
    setTimeout(function() {
        $('#indateTo').val(moment(new Date()).format('DD/MM/YYYY'));
        $('#indateFrom').val(moment(new Date()).format('DD/MM/YYYY'));
    }, 50);
    return false;
}
	$(document).ready(function() {
    if (${custAuditList != null && custAuditList.size() >= 0}) {
        $('#resultDiv').show();
        var columns = [{
            "mDataProp": "mobileNumber"
        }, {
            "mDataProp": "activityAt"
        }, {
            "mDataProp": "mobileNumber"
        }, {
            "mDataProp": "contractSubscribed",
            "mRender": function(data) {
                var html = '<div>';
                if (data != null) {
                    $.each(data, function(index, currEmp) {
                        html += "<p>" + currEmp.accountNumber + "</p>";
                    });
                    html += "</div>";
                }
                return html;
            }
        }, {
            "mDataProp": "loginId"
        }, {
            "mDataProp": "activity"
        }];
        var table = $('#tblSearch').DataTable({
            //  responsive: true,   
            bFilter: false,
            bSort: false,
            lengthMenu: [
                [50],
                [50]
            ],
            "bProcesing": true,
            "bServerSide": true,
            "bLengthChange": false,
            "iDisplayStart": 0,
            "sEmptyTable": "No data available in table",
            "sAjaxSource": "${pageContext.request.contextPath}/admin/searchCustHistoryTable.do?${_csrf.parameterName}=${_csrf.token}&dateFrom=${dateFrom}&dateTo=${dateTo}&msisdn=${msisdn}&contractAccNo=${contractAccNo}&&loginId=${loginId}",
            "aoColumns": columns,
            /* [
                {
                    "mDataProp": "activityAt"
                }, {
                    "mDataProp": "mobileNumber"
                }, {
                    "mDataProp": "mobileNumber"
                }, {
                    "mDataProp": "loginId"
                }, {
                    "mDataProp": "activity"
                },
            ], */
            "fnServerData": function(sSource, aoData, fnCallback) {
                $.ajax({
                	beforeSend: function(xhr) {		                
		                sebApp.showIndicator();			                
		            },
                    dataType: 'json',
                    type: "POST",
                    url: sSource,
                    data: aoData,
                    success: function(data, textStatus, jqXHR) {
                        fnCallback(data, textStatus, jqXHR);
                    },
                    failure: function(data) {},
    		        complete: function(e){ sebApp.hideIndicator();}
                });
            },
            "rowCallback": function(row, data, iDisplayIndex) {
                var info = table.page.info();
                var page = info.page;
                var length = info.length;
                var index = (page * length + (iDisplayIndex + 1));
                $('td:eq(0)', row).html(index);
            },
        });
    }
    $('#dateFrom').datetimepicker({
        format: 'DD/MM/YYYY',
        defaultDate: new Date(),
        ignoreReadonly: true
    }).on("dp.change", function(e) {
        $('#searchHistoryForm').formValidation('revalidateField', 'indateFrom');
    });
    $('#dateTo').datetimepicker({
        format: 'DD/MM/YYYY',
        defaultDate: new Date(),
        ignoreReadonly: true
    }).on("dp.change", function(e) {
        $('#searchHistoryForm').formValidation('revalidateField', 'indateTo');
    });
    $("#msisdn").keypress(function(e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $('#searchHistoryForm').formValidation({
        framework: 'bootstrap',
        icon: {
            invalid: 'glyphicon glyphicon-remove',
        },
        fields: {
            indateFrom: {
                validators: {
                    callback: {
                        callback: function(value, validator, $field) {
                            if (value == '') {
                                $('#indateFrom').val(moment(new Date()).format('DD/MM/YYYY'));
                                return true;
                            }
                            return true;
                        }
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        max: 'indateTo',
                        message: 'The start date is not a valid'
                    }
                }
            },
            indateTo: {
                validators: {
                    date: {
                        format: 'DD/MM/YYYY',
                        min: 'indateFrom',
                        message: 'The end date is not a valid'
                    },
                    callback: {
                        callback: function(value, validator, $field) {
                            if (value == '') {
                                $('#indateTo').val(moment(new Date()).format('DD/MM/YYYY'));
                                return true;
                            }
                            return true;
                        }
                    },
                    format: 'DD/MM/YYYY'
                }
            },
        }
    }).on('success.field.fv', function(e, data) {
        if (data.field === 'indateFrom' && !data.fv.isValidField('indateTo')) {
            // We need to revalidate the end date
            data.fv.revalidateField('indateTo');
        }
        if (data.field === 'indateTo' && !data.fv.isValidField('indateFrom')) {
            // We need to revalidate the start date
            data.fv.revalidateField('indateFrom');
        }
    });
});
</script>
</body>
</html>