package com.seb.admin.portal.adapter;

public class ResetPasswordResponse {
	private long respStat;
	private String errCode;
	private String errDesc;	
	
	private String loginId;
	
	private String preferredCommMethod;
	private String newPassword;
	
	public long getRespStat() {
		return respStat;
	}
	public void setRespStat(long respStat) {
		this.respStat = respStat;
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public String getErrDesc() {
		return errDesc;
	}
	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}
	public String getPreferredCommMethod() {
		return preferredCommMethod;
	}
	public void setPreferredCommMethod(String preferredCommMethod) {
		this.preferredCommMethod = preferredCommMethod;
	}
	@Override
	public String toString() {
		return "ResetPasswordResponse [respStat=" + respStat + ", errCode=" + errCode + ", errDesc=" + errDesc
				+ ", preferredCommMethod=" + preferredCommMethod + "]";
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	


}
