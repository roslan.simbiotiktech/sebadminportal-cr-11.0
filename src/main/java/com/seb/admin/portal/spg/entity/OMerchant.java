package com.seb.admin.portal.spg.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OMerchant {

	@JsonProperty("name")
	private String name;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("signature_secret")
	private String signature_secret;
	
	@JsonProperty("minimum_payment")
	private double minimum_payment;
	
	@JsonProperty("maximum_payment")
	private double maximum_payment;
	
	@JsonProperty("server_to_server_payment_update_url")
	private String server_to_server_payment_update_url;
	
	@JsonProperty("client_payment_update_url")
	private String client_payment_update_url;
	
	@JsonProperty("enabled")
	private boolean enabled;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSignature_secret() {
		return signature_secret;
	}

	public void setSignature_secret(String signature_secret) {
		this.signature_secret = signature_secret;
	}

	public double getMinimum_payment() {
		return minimum_payment;
	}

	public void setMinimum_payment(double minimum_payment) {
		this.minimum_payment = minimum_payment;
	}

	public double getMaximum_payment() {
		return maximum_payment;
	}

	public void setMaximum_payment(double maximum_payment) {
		this.maximum_payment = maximum_payment;
	}

	public String getServer_to_server_payment_update_url() {
		return server_to_server_payment_update_url;
	}

	public void setServer_to_server_payment_update_url(String server_to_server_payment_update_url) {
		this.server_to_server_payment_update_url = server_to_server_payment_update_url;
	}

	public String getClient_payment_update_url() {
		return client_payment_update_url;
	}

	public void setClient_payment_update_url(String client_payment_update_url) {
		this.client_payment_update_url = client_payment_update_url;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
