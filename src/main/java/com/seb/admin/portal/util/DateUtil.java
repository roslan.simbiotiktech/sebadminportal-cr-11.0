package com.seb.admin.portal.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {


	public String getCurrentDate()
	{
		Date date=new Date();
		DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate=dateFormat.format(date);
		System.out.println(formattedDate); 

		try {
			dateFormat.parse(formattedDate);
			//System.out.println(">>>>>>"+dateFormat.parse(formattedDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return formattedDate;
	}

	public String getTomorrowDate(){
		try {
			Date now = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(now);
			cal.add(Calendar.DAY_OF_YEAR, 1); // <--
			Date tomorrowDate= cal.getTime();

			DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
			String tomorrow=dateFormat.format(tomorrowDate);
			return tomorrow;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public Date convertToDate (String myDate, String srcFormat){
		SimpleDateFormat formatter = new SimpleDateFormat(srcFormat);
		
		try {
			if(myDate!=null && !myDate.equalsIgnoreCase("")) {
				Date date = formatter.parse(myDate);
				return date;
			}
		
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String dateFormat2Format(String myDate, String srcFormat, String destFormat){
		try {
			DateFormat srcDf = new SimpleDateFormat(srcFormat);

			// parse the date string into Date object
			Date date = srcDf.parse(myDate);

			DateFormat destDf = new SimpleDateFormat(destFormat);

			// format the date into another format
			myDate = destDf.format(date);

			//System.out.println("Converted date is : " + myDate);

			return myDate;

		}
		catch (ParseException e) {
			e.printStackTrace();
		}

		return null;

	}
	
	
	

	private static Date createNewDate(int day, int month, int year, int hh, int mm, int ss) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		try {
			return formatter.parse("" + day + "/" + month + "/" + year+" "+hh+":"+mm+":"+ss);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(String oldDate){
		oldDate = oldDate.substring(0,16).concat(":00")+(oldDate.substring(oldDate.length()-3));
		return oldDate;
		
	}
	
	public static Timestamp stringToTimestamp(String dateStr) {
		  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
		    Date parsedDate = new Date();
			try {
				parsedDate = dateFormat.parse(dateStr);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
		    return timestamp;
		    
	}
	
	public static void main(String[] args) {
		  Date date = new Date();
		    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");

		    String strDate = sdf.format(date);
		    System.out.println("formatted date in mm/dd/yy : " + strDate);

		    sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		    strDate = sdf.format(date);
		    System.out.println("formatted date in mm-dd-yyyy hh:mm:ss : " + strDate);
		
		
	}

}
