package com.seb.admin.portal.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.seb.admin.portal.constant.ApplicationConstant;


/**
 * The persistent class for the AUDIT_TRAIL database table.
 * 
 */
@Entity
@Table(name="AUDIT_TRAIL" , schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="AuditTrail.findAll", query="SELECT a FROM AuditTrail a"),
	@NamedQuery(name="AuditTrail.findAllByAdmin", query="SELECT a FROM AuditTrail a LEFT JOIN a.adminUser ad WHERE a.adminUserId =:adminId AND a.auditDatetime >=:dateFrom AND a.auditDatetime <=:dateTo AND a.deletedFlag=0 ORDER BY a.id desc"),
	@NamedQuery(name="AuditTrail.findAllByDate", query="SELECT a FROM AuditTrail a WHERE a.auditDatetime >=:dateFrom AND a.auditDatetime <=:dateTo AND a.deletedFlag=0 ORDER BY a.id desc"),
	@NamedQuery(name="AuditTrail.findAllByType", query="SELECT a FROM AuditTrail a WHERE  a.auditDatetime >=:dateFrom AND a.auditDatetime <=:dateTo AND a.auditTrailTypeId =:typeId  AND a.deletedFlag=0 ORDER BY a.id desc"),
	@NamedQuery(name="AuditTrail.findAllByTypeAdmin", query="SELECT a FROM AuditTrail a WHERE  a.auditDatetime >=:dateFrom AND a.auditDatetime <=:dateTo AND a.auditTrailTypeId =:typeId AND a.adminUserId =:adminId  AND a.deletedFlag=0 ORDER BY a.id desc")	
})
public class AuditTrail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	public AuditTrail(int adminUserId, int auditTrailTypeId, String ipAddress, String activity, 
			String oldValue, String newValue, String remark) {
		super();
		this.activity = activity;
		this.adminUserId = adminUserId;
		this.auditTrailTypeId = auditTrailTypeId;
		this.ipAddress = ipAddress;
		this.newValue = newValue;
		this.oldValue = oldValue;
		this.remark = remark;
	}
	
	@Column(name="ACTIVITY", length=500)
	private String activity;

	@Column(name="ADMIN_USER_ID")
	private int adminUserId;

	@Column(name="AUDIT_DATETIME", nullable=false,  insertable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date auditDatetime;

	@Column(name="AUDIT_TRAIL_TYPE_ID")
	private int auditTrailTypeId;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(name="IP_ADDRESS", length=255)
	private String ipAddress;

	@Lob
	@Column(name="NEW_VALUE")
	private String newValue;

	@Lob
	@Column(name="OLD_VALUE")
	private String oldValue;

	@Lob
	private String remark;
	
	@JoinColumn(name = "AUDIT_TRAIL_TYPE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.EAGER)
	private AuditTrailType auditTrailType;

	@JoinColumn(name = "ADMIN_USER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.EAGER )
	private AdminUser  adminUser;

	public AuditTrail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActivity() {
		return this.activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public int getAdminUserId() {
		return this.adminUserId;
	}

	public void setAdminUserId(int adminUserId) {
		this.adminUserId = adminUserId;
	}

	public Date getAuditDatetime() {
		return this.auditDatetime;
	}

	public void setAuditDatetime(Date auditDatetime) {
		this.auditDatetime = auditDatetime;
	}

	public int getAuditTrailTypeId() {
		return this.auditTrailTypeId;
	}

	public void setAuditTrailTypeId(int auditTrailTypeId) {
		this.auditTrailTypeId = auditTrailTypeId;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getNewValue() {
		return this.newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public String getOldValue() {
		if(this.oldValue !=null && this.oldValue .length()>0){
			oldValue  = oldValue+">>";
		}
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public AuditTrailType getAuditTrailType() {
		return auditTrailType;
	}

	public void setAuditTrailType(AuditTrailType auditTrailType) {
		this.auditTrailType = auditTrailType;
	}

	@JsonBackReference //for parent table
	public AdminUser getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(AdminUser adminUser) {
		this.adminUser = adminUser;
	}


}