<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->

		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="font-weight: bold">Policy</div>

						<div class="panel-body">
							<form id="policyForm" name="policyForm" method="post" action="${pageContext.request.contextPath}/admin/policy.do" class="form-horizontal">
								${successMsg}${errorMsg}
								<div class="form-group">
									<p class="col-lg-5 control-label">Activation Code Expiry (in minutes)</p>

									<div class="col-lg-3">
										<input class="form-control" id="expiry" name="expiry" placeholder="in minutes" type="text" maxlength="2" value="${oldSetting.settingValue }">
									</div>
								</div>

								<!-- <div class="form-group">
                                    <p class="col-lg-5 control-label">Remember Me (in months)</p>

                                    <div class="col-lg-3">
                                        <input class="form-control" id="rmbMe" name="rmbMe" placeholder="3" type="text" value="3" style="width: 30%">
                                    </div>
                                </div> -->

								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal">Update</button>
										<button class="btn btn-warning" id="cancelBtn1" type="reset">Cancel</button>
									</div>
								</div>

								<!-- Add Confirmation Modal -->
								<div class="modal" id="modal" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button class="close" data-dismiss="modal" type="button">
													<span>&times;</span><span class="sr-only">Close</span>
												</button>
												<h2 class="modal-title">Confirmation</h2>
											</div>
											<div class="modal-body" id="add">Are you sure you want to update policy?</div>
											<div class="modal-footer">
												<button class="btn btn-primary" id="add" type="submit">Update</button>
												<button class="btn btn-warning" data-dismiss="modal" type="button">Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- End Confirmation Modal -->

							</form>
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
	function resetForm() {
		$('#').find('.has-error').removeClass("has-error");
		$('#policyForm').find('.has-success').removeClass("has-success");
		$('#policyForm').find('.form-control-feedback').removeClass('glyphicon-remove');
		$('#policyForm').find('.form-control-feedback').removeClass('glyphicon-ok');
		$('#policyForm').formValidation('resetForm', true);
		
		$('.form-group').find('small.help-block').hide();
		$('.form-group').removeClass('has-error has-feedback');
		$('.form-group').find('i.form-control-feedback').hide();
		return false;
	}
	
    $(document).ready(function() {  
    
     $("#expiry").keypress(function (e) {
         //if the letter is not digit then display error and don't type anything
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                   return false;
        }
       });
     
     $("#cancelBtn1").click(function() {
         $('#policyForm').find('.has-error').removeClass("has-error");
         $('#policyForm').find('.has-success').removeClass("has-success");
         $('#policyForm').find('.form-control-feedback').removeClass('glyphicon-remove');
         $('#policyForm').find('.form-control-feedback').removeClass('glyphicon-ok');
         $('.form-group').find('small.help-block').hide();
         $('#policyForm')[0].reset();
     });
     
     $('#policyForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
                expiry: {
                    validators: {
                        notEmpty: {
                            message: 'Activation Code Expiry (in minutes) is required'
                        }	                       
                    }
                }            
            }
        });
    });
    </script>
</body>
</html>