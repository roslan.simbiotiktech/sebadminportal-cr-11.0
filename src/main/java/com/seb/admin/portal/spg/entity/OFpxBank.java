package com.seb.admin.portal.spg.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.constant.FpxBankType;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OFpxBank {
	@JsonProperty("last_updated_by")
	private String last_updated_by;
	
	@JsonProperty("last_updated_datetime")
	private String last_updated_datetime;
	
	@JsonProperty("created_by")
	private String created_by;
	
	@JsonProperty("created_datetime")
	private String created_datetime;
	
	@JsonProperty("bank_fpx_id")
	private String bank_fpx_id;
	
	@JsonProperty("bank_type")
	private FpxBankType bank_type;
	
	@JsonProperty("enabled")
	private boolean enabled;
	
	@JsonProperty("bank_id")
	private int bank_id;
	
	@JsonProperty("bank_name")
	private String bank_name;
	
	@JsonProperty("logo")
	private String logo;

	public String getLast_updated_by() {
		return last_updated_by;
	}

	public void setLast_updated_by(String last_updated_by) {
		this.last_updated_by = last_updated_by;
	}

	public String getLast_updated_datetime() {
		return last_updated_datetime;
	}

	public void setLast_updated_datetime(String last_updated_datetime) {
		this.last_updated_datetime = last_updated_datetime;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getCreated_datetime() {
		return created_datetime;
	}

	public void setCreated_datetime(String created_datetime) {
		this.created_datetime = created_datetime;
	}

	public String getBank_fpx_id() {
		return bank_fpx_id;
	}

	public void setBank_fpx_id(String bank_fpx_id) {
		this.bank_fpx_id = bank_fpx_id;
	}

	public FpxBankType getBank_type() {
		return bank_type;
	}

	public void setBank_type(FpxBankType bank_type) {
		this.bank_type = bank_type;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public int getBank_id() {
		return bank_id;
	}

	public void setBank_id(int bank_id) {
		this.bank_id = bank_id;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bank_fpx_id == null) ? 0 : bank_fpx_id.hashCode());
		result = prime * result + bank_id;
		result = prime * result + ((bank_name == null) ? 0 : bank_name.hashCode());
		result = prime * result + ((bank_type == null) ? 0 : bank_type.hashCode());
		result = prime * result + ((created_by == null) ? 0 : created_by.hashCode());
		result = prime * result + ((created_datetime == null) ? 0 : created_datetime.hashCode());
		result = prime * result + (enabled ? 1231 : 1237);
		result = prime * result + ((last_updated_by == null) ? 0 : last_updated_by.hashCode());
		result = prime * result + ((last_updated_datetime == null) ? 0 : last_updated_datetime.hashCode());
		result = prime * result + ((logo == null) ? 0 : logo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OFpxBank other = (OFpxBank) obj;
		if (bank_fpx_id == null) {
			if (other.bank_fpx_id != null)
				return false;
		} else if (!bank_fpx_id.equals(other.bank_fpx_id))
			return false;
		if (bank_id != other.bank_id)
			return false;
		if (bank_name == null) {
			if (other.bank_name != null)
				return false;
		} else if (!bank_name.equals(other.bank_name))
			return false;
		if (bank_type != other.bank_type)
			return false;
		if (created_by == null) {
			if (other.created_by != null)
				return false;
		} else if (!created_by.equals(other.created_by))
			return false;
		if (created_datetime == null) {
			if (other.created_datetime != null)
				return false;
		} else if (!created_datetime.equals(other.created_datetime))
			return false;
		if (enabled != other.enabled)
			return false;
		if (last_updated_by == null) {
			if (other.last_updated_by != null)
				return false;
		} else if (!last_updated_by.equals(other.last_updated_by))
			return false;
		if (last_updated_datetime == null) {
			if (other.last_updated_datetime != null)
				return false;
		} else if (!last_updated_datetime.equals(other.last_updated_datetime))
			return false;
		if (logo == null) {
			if (other.logo != null)
				return false;
		} else if (!logo.equals(other.logo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "last_updated_by:[" + last_updated_by + "], last_updated_datetime:[" + last_updated_datetime
				+ "], created_by:[" + created_by + "], created_datetime:[" + created_datetime + "], bank_fpx_id:["
				+ bank_fpx_id + "], bank_type:[" + bank_type + "], enabled:[" + enabled + "], bank_id:[" + bank_id
				+ "], bank_name:[" + bank_name + "], logo:[" + logo + "]";
	}

	
}
