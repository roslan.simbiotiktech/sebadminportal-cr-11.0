package com.seb.admin.portal.spg.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OMerchantBase  implements Comparable<OMerchantBase> {

	@JsonProperty("id")
	private int id;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("signature_secret")
	private String signature_secret;
	
	@JsonProperty("minimum_payment")
	private double minimum_payment;
	
	@JsonProperty("maximum_payment")
	private double maximum_payment;
	
	@JsonProperty("server_to_server_payment_update_url")
	private String server_to_server_payment_update_url;
	
	@JsonProperty("client_payment_update_url")
	private String client_payment_update_url;
	
	@JsonProperty("enabled")
	private boolean enabled;
	
	@JsonProperty("last_updated_by")
	private String last_updated_by;
	
	@JsonProperty("last_updated_datetime")
	private String last_updated_datetime;
	
	
	@JsonProperty("created_by")
	private String created_by;
	
	
	@JsonProperty("created_datetime")
	private String created_datetime;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSignature_secret() {
		return signature_secret;
	}

	public void setSignature_secret(String signature_secret) {
		this.signature_secret = signature_secret;
	}

	public double getMinimum_payment() {
		return minimum_payment;
	}

	public void setMinimum_payment(double minimum_payment) {
		this.minimum_payment = minimum_payment;
	}

	public double getMaximum_payment() {
		return maximum_payment;
	}

	public void setMaximum_payment(double maximum_payment) {
		this.maximum_payment = maximum_payment;
	}

	public String getServer_to_server_payment_update_url() {
		return server_to_server_payment_update_url;
	}

	public void setServer_to_server_payment_update_url(String server_to_server_payment_update_url) {
		this.server_to_server_payment_update_url = server_to_server_payment_update_url;
	}

	public String getClient_payment_update_url() {
		return client_payment_update_url;
	}

	public void setClient_payment_update_url(String client_payment_update_url) {
		this.client_payment_update_url = client_payment_update_url;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLast_updated_by() {
		return last_updated_by;
	}

	public void setLast_updated_by(String last_updated_by) {
		this.last_updated_by = last_updated_by;
	}

	public String getLast_updated_datetime() {
		return last_updated_datetime;
	}

	public void setLast_updated_datetime(String last_updated_datetime) {
		this.last_updated_datetime = last_updated_datetime;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getCreated_datetime() {
		return created_datetime;
	}

	public void setCreated_datetime(String created_datetime) {
		this.created_datetime = created_datetime;
	}

	@Override
	public int compareTo(OMerchantBase ob) {
		// TODO Auto-generated method stub
		return name.compareTo(ob.getName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client_payment_update_url == null) ? 0 : client_payment_update_url.hashCode());
		result = prime * result + ((created_by == null) ? 0 : created_by.hashCode());
		result = prime * result + ((created_datetime == null) ? 0 : created_datetime.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (enabled ? 1231 : 1237);
		result = prime * result + id;
		result = prime * result + ((last_updated_by == null) ? 0 : last_updated_by.hashCode());
		result = prime * result + ((last_updated_datetime == null) ? 0 : last_updated_datetime.hashCode());
		long temp;
		temp = Double.doubleToLongBits(maximum_payment);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(minimum_payment);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((server_to_server_payment_update_url == null) ? 0 : server_to_server_payment_update_url.hashCode());
		result = prime * result + ((signature_secret == null) ? 0 : signature_secret.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OMerchantBase other = (OMerchantBase) obj;
		if (client_payment_update_url == null) {
			if (other.client_payment_update_url != null)
				return false;
		} else if (!client_payment_update_url.equals(other.client_payment_update_url))
			return false;
		if (created_by == null) {
			if (other.created_by != null)
				return false;
		} else if (!created_by.equals(other.created_by))
			return false;
		if (created_datetime == null) {
			if (other.created_datetime != null)
				return false;
		} else if (!created_datetime.equals(other.created_datetime))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (enabled != other.enabled)
			return false;
		if (id != other.id)
			return false;
		if (last_updated_by == null) {
			if (other.last_updated_by != null)
				return false;
		} else if (!last_updated_by.equals(other.last_updated_by))
			return false;
		if (last_updated_datetime == null) {
			if (other.last_updated_datetime != null)
				return false;
		} else if (!last_updated_datetime.equals(other.last_updated_datetime))
			return false;
		if (Double.doubleToLongBits(maximum_payment) != Double.doubleToLongBits(other.maximum_payment))
			return false;
		if (Double.doubleToLongBits(minimum_payment) != Double.doubleToLongBits(other.minimum_payment))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (server_to_server_payment_update_url == null) {
			if (other.server_to_server_payment_update_url != null)
				return false;
		} else if (!server_to_server_payment_update_url.equals(other.server_to_server_payment_update_url))
			return false;
		if (signature_secret == null) {
			if (other.signature_secret != null)
				return false;
		} else if (!signature_secret.equals(other.signature_secret))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "id:[" + id + "], name:[" + name + "], description:[" + description + "], signature_secret:["
				+ signature_secret + "], minimum_payment:[" + minimum_payment + "], maximum_payment:[" + maximum_payment
				+ "], server_to_server_payment_update_url:[" + server_to_server_payment_update_url
				+ "], client_payment_update_url:[" + client_payment_update_url + "], enabled:[" + enabled
				+ "], last_updated_by:[" + last_updated_by + "], last_updated_datetime:[" + last_updated_datetime
				+ "], created_by:[" + created_by + "], created_datetime:[" + created_datetime + "]";
	}

	
	



}
