package com.seb.admin.portal.staging;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import com.novell.ldap.LDAPConnection;



public class LDAPUtil 
{
	final String LDAP_HOST = "localhost";
	final int LDAP_PORT = 3890;

	final String ldapAdServer = "ldap://localhost:389";
	final String ldapSearchBase = "DC=sarawakenergy,DC=com,DC=my";

	final String ldapUsername = "cmareader";
	final String ldapPassword = "CM@@2015";

	final String ldapAccountToLookup = "cmareader";


	public static final int LDAP_SEARCH_SCOPE = LDAPConnection.SCOPE_SUB;
	public static final int LDAP_VERSION = LDAPConnection.LDAP_V3;
	public static final String LDAP_DN = "DC=sarawakenergy,DC=com,DC=my";
	public static final String LDAP_PASSWORD = "CM@@2015";

	public static final String LDAP_SEARCH_BASE = "dc=alrajhibank,dc=com,dc=my";
	public static final String LDAP_CONTAINER_NAME = "dc=alrajhibank,dc=com,dc=my";
	public static final String LDAP_SEARCH_CRITERIA_1 = "LDAP.SEARCH.CRITERIA.1";
	public static final String LDAP_SEARCH_CRITERIA_2 = "LDAP.SEARCH.CRITERIA.2";


	public LDAPUtil()
	{

	}

	//searchResults = lc.search( searchBase, searchScope, searchFilter, attributesToReturn, attributesOnlyFlag );

	/**
	 * createConnection = bind to AD
	 * @return
	 * date added: 10th April 2009
	 */
	private LdapContext createConnection()
	{
		LdapContext ctx = null;
		try{
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY,
					"com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.SECURITY_AUTHENTICATION, "Simple");
			env.put(Context.SECURITY_PRINCIPAL, "cmareader");
			env.put(Context.SECURITY_CREDENTIALS, "CM@@2015");
			env.put(Context.PROVIDER_URL, "ldap://localhost:3890");
			ctx = new InitialLdapContext(env, null);
			System.out.println("Connection Successful.");
		}catch(NamingException nex){
			System.out.println("LDAP Connection: FAILED");
			nex.printStackTrace();
		}
		return ctx;

	}

	/**
	 * validateLogin = validate input against AD using AD sAMAccountName & password
	 * @param dn
	 * @param password
	 * @return
	 * date added: 10th April 2009
	 */
	public boolean validateLogin(String dn,String password)
	{	
		LDAPConnection lc = null;
		boolean result = false;
		try
		{
			lc = new LDAPConnection();
			lc.connect(LDAP_HOST, LDAP_PORT);
			lc.bind(LDAP_VERSION, dn, password);

			System.out.println("validateLogin() LDAP Connect = " + lc.isConnected());
			result = lc.isConnected();
		}
		catch(Exception ex)
		{
			System.out.println("validateLogin() [" + dn + "] Invalid Login");
		}
		return result;	
	}

	/**
	 * getDN = retrieve DN of AD login
	 * @param userId
	 * @return
	 * date added: 10th April 2009
	 */

	/**
	 * checkOU = derive OU from DN
	 * @param dn
	 * @return
	 * date added: 10th April 2009
	 */
	public String checkOU(String dn)
	{
		String[] element = dn.split(",");
		String OU = "";

		//added on 25th April 2012
		for (int i = 0; i < element.length; i++)
		{
			if(element[i].startsWith("OU"))
			{
				System.out.println("OU >> " + element[i]);

				OU += i > 1 ? "," : "";
				OU += element[i];
			}
		}

		System.out.println("DN no CN, no DC = " + OU);



		/*   	 	for(int j = 0; j < element.length; j++)
   	 	{
   	 		System.out.println("element " + j + " = " + element[j]);

   	 		if(element[j].split("=")[0].equalsIgnoreCase("OU"))
   	 		{
   	 			OU = element[j].split("=")[1];
   	 			System.out.println("element[j].split[1] = " + OU);

   	 			if(OU.endsWith("Branch"))
   	 			{
   	 				OU = OU + "," + element[j+1];

   	 				System.out.println("OU Branch = " + OU);   //OU=BangsarBranch,OU=Branches

   	 				return OU;
   	 			}
   	 		}
   	 	}*/
		if(OU!=null)
		{	
			return OU;
		}
		else
		{
			return "OU not exist";
		}
	}


	//Search DN = DC=alrajhibank,DC=com,DC=my
	//Filter = (&(objectclass=*)(OU=*))   --> list out all OU in LDAP

	/**
	 * checkOU = derive OU from DN
	 * @param dn
	 * @return
	 * date added: 10th April 2009
	 */

	/**
	 * searchBySamacctName = search by sAMAccountName, cn (name) from AD
	 * @param samaAccount
	 * @param cnName
	 * @return
	 * date added: 20th April 2009
	 */




	public static void main(String[] args) 
	{
		LDAPUtil ldap = new LDAPUtil();

		ldap.createConnection();

		ldap.validateLogin("CN=cmareader,OU=ServiceAccounts,OU=_HQ,DC=sarawakenergy,DC=com,DC=my",LDAP_PASSWORD );


	}	
}
