package com.seb.admin.portal.dao;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.model.AuditTrailType;
import com.seb.admin.portal.model.BankResponse;
import com.seb.admin.portal.model.CustomerAuditTrail;
import com.seb.admin.portal.model.MyBillPayReportReq;
import com.seb.admin.portal.model.MyBillPayReportResp;
import com.seb.admin.portal.model.MyPayment;
import com.seb.admin.portal.model.PaymentGateway;
import com.seb.admin.portal.model.PaymentReference;
import com.seb.admin.portal.model.PaymentReferenceEntry;
import com.seb.admin.portal.model.Report;
import com.seb.admin.portal.model.ReportRemarksHistory;
import com.seb.admin.portal.model.SfdcReport;
import com.seb.admin.portal.util.StringUtil;

@Repository
public class ReportDAOImpl implements ReportDAO{

	private static final Logger log = Logger.getLogger(ReportDAOImpl.class);
	
	SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd", Locale.ENGLISH);

	@PersistenceContext(unitName = ApplicationConstant.UNIT_NAME)
	private EntityManager em;

	/**============================================
	 * Audit Trails Report
	 *============================================*/
	public AuditTrailType findAuditByID(String name) {
		AuditTrailType au = new AuditTrailType();
		try{
			au = (AuditTrailType) em.createNamedQuery("AuditTrailType.findById").setParameter("name", name).getSingleResult();

		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
		}		
		return au;
	}

	public int insertAuditTrail(AuditTrail at) {
		int ret = -3;
		try{
			AuditTrail newAt = new AuditTrail(at.getAdminUserId(), at.getAuditTrailTypeId(), at.getIpAddress(), at.getActivity(), 
					at.getOldValue(), at.getNewValue(),at.getRemark());		
			em.persist(newAt);
			ret = 1;

		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
		}


		return ret;
	}

	@Override
	public List<AuditTrailType> findAllAuditType() throws SQLException {
		List <AuditTrailType> typeList = new ArrayList <AuditTrailType>();
		try{
			typeList = em.createNamedQuery("AuditTrailType.findAllAuditType",AuditTrailType.class).getResultList();

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return typeList;
	}

	@Override
	public List<AuditTrail> findAllByAdmin(Date dateFrom, Date dateTo, int adminId) {
		List <AuditTrail> trailList = new ArrayList <AuditTrail>();
		try{
			trailList = em.createNamedQuery("AuditTrail.findAllByAdmin",AuditTrail.class).setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.setParameter("adminId", adminId).getResultList();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return trailList;


	}

	@Override
	public List<AuditTrail> findAllByDate(Date dateFrom, Date dateTo) {
		List <AuditTrail> trailList = new ArrayList <AuditTrail>();
		try{
			trailList = em.createNamedQuery("AuditTrail.findAllByDate",AuditTrail.class)
					.setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo).getResultList();

		}catch(NoResultException noR){

		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return trailList;
	}



	@Override
	public List<AuditTrail> findAllByType (Date dateFrom, Date dateTo, int typeId){
		List <AuditTrail> trailList = new ArrayList <AuditTrail>();
		try{
			trailList = em.createNamedQuery("AuditTrail.findAllByType",AuditTrail.class).setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.setParameter("typeId", typeId).getResultList();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return trailList;
	}

	@Override
	public List<AuditTrail> findAllByTypeAdmin(Date dateFrom, Date dateTo, int adminId, int typeId) {
		List <AuditTrail> trailList = new ArrayList <AuditTrail>();
		try{
			trailList = em.createNamedQuery("AuditTrail.findAllByTypeAdmin",AuditTrail.class).setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.setParameter("adminId", adminId)
					.setParameter("typeId", typeId).getResultList();
		}catch(NoResultException noR){}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return trailList;
	}

	/**============================================
	 * Make A Report
	 *============================================*/

	@Override
	public List<Report> findAllMakeReportByCondition(Date dateFrom, Date dateTo, String source, String type, String status) {
		List <Report> list = new ArrayList <Report>();
		List <Report> FilnalList = new ArrayList <Report>();
		List <SfdcReport> list2 = new ArrayList <SfdcReport>();
		try{
			list = em.createNamedQuery("Report.findAllByCondition",Report.class).setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.setParameter("channel", source)
					.setParameter("type", type)
					.setParameter("status", status).getResultList();
			
			if(list.size()!=0) {
				System.out.println("First list is not empty "+list.size());
				for (Iterator iterator = list.iterator(); iterator.hasNext();) {
					Report report = (Report) iterator.next();
					if(report.getCaseNumber()!=null) {
						
						report.setCommonCaseNumber(String.valueOf(report.getCaseNumber()));
					}else {
						report.setCommonCaseNumber("--");
					}
					
					// Raj added seb status
					report.setSebStatus(String.valueOf(report.getStatus()));
				
					FilnalList.add(report);
				}
			}
			
			
			
		}catch(NoResultException noR){}
		catch(Exception ex){
			ex.printStackTrace();
		}
		// for second table
			
			list2 = em.createNamedQuery("SfdcReport.findAllByCondition",SfdcReport.class).setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.setParameter("channel", source)
					.setParameter("type", type)
					.setParameter("status", status).getResultList();
			if(list2.size()!=0) {
				System.out.println("Second list is not empty "+list2.size());
				for (Iterator iterator = list2.iterator(); iterator.hasNext();) {
					SfdcReport sfdcReport = (SfdcReport) iterator.next();
					Report r =new Report();
					 r.setCreatedDatetime(sfdcReport.getCreatedDatetime());
					 r.setReportType(sfdcReport.getReportType());
					 r.setIssueType(sfdcReport.getIssueType());
					 r.setTransId(sfdcReport.getTransId());
					 r.setStatus(sfdcReport.getStatus());	
					 r.setSebStatus(sfdcReport.getSebStatus()); // @Raj added seb status
					 String caseNumber=null;
					 if(sfdcReport.getCaseNumber()!=null) {
						 caseNumber=sfdcReport.getCaseNumber();
						 r.setCommonCaseNumber(caseNumber);
					 }else {
						 r.setCaseNumber(00);
					 }					
					 r.setChannel(!sfdcReport.getChannel().isEmpty() ? sfdcReport.getChannel() : "--");
					 r.setUserEmail(sfdcReport.getUserEmail());
					 r.setUserMobileNumber(sfdcReport.getUserMobileNumber());
					 r.setStation(sfdcReport.getStation());
					 r.setLocAddress(sfdcReport.getLocAddress());
					 r.setDescription(sfdcReport.getDescription());
					 r.setRemark(sfdcReport.getRemark()!=null ? sfdcReport.getRemark() : "--");					
					 FilnalList.add(r);
					
				}
		}else {
			System.out.println("Second list is empty");
		}
		
		return FilnalList;
	}	

	@Override
	public Report findMakeAReportById (int id) throws SQLException{
		return em.find(Report.class, id);		
	}
	@SuppressWarnings({ "null", "rawtypes", "unused" })
	@Override
	@Transactional
	public Report findMakeAReportByIdAndDate (int id,String createdDatetime) throws Exception{
		Report r =null;

        Session session = em.unwrap(Session.class);
        SessionFactory sessionFactory = session.getSessionFactory();
        SQLQuery query = session.createSQLQuery("SELECT * FROM SEB.REPORT where TRANS_ID="+id+" AND CREATED_DATETIME LIKE '"+createdDatetime+"%'");
         List<Report> rs=  query.addEntity(Report.class).list();
         System.out.println(rs.size()); 
         if(rs.size()!=0) {
        	 r=rs.get(0); 
        	 r.setCommonCaseNumber(String.valueOf(rs.get(0).getCaseNumber()));
        	// r.setSebStatus("--");// @raj added seb status
        	 r.setSebStatus(String.valueOf(rs.get(0).getStatus()));
         }
		if(r==null) {
	
		        SQLQuery query2 = session.createSQLQuery("SELECT * FROM SEB.SFDC_REPORT where TRANS_ID="+id+" AND CREATED_DATETIME LIKE '"+createdDatetime+"%'");
		         List<SfdcReport> rsfdc=  query2.addEntity(SfdcReport.class).list();
		         System.out.println("SfdcReport list size "+rsfdc.size());
		         if(rsfdc.size()!=0) {
		        	 SfdcReport sr=rsfdc.get(0);
		        	 r=new Report();
		        	// r.setCaseId(!sr.getCaseId().equals(null) ? sr.getCaseId() : "-");
		        	 r.setSebStatus(sr.getSebStatus());// added by Raj
		        	 String caseNumber=null;
					 if(sr.getCaseNumber()!=null) {
						 caseNumber=sr.getCaseNumber();
						 r.setCommonCaseNumber(caseNumber);					
					 }else {
						 r.setCaseNumber(00);
					 }	
		 			System.out.println(sr.getRemark());
		 			System.out.println("Second Table case number"+r.getCommonCaseNumber());
		 			if(sr.getRemark()!=null) {
		 				System.out.println("remarks are not  empty");
		 				r.setRemark(sr.getRemark());
		 			}else {
		 				System.out.println("remarks are  empty");
		 				r.setRemark(sr.getRemark());
		 			}		 				
		 			r.setStatus(!sr.getStatus().equals(null) ? sr.getStatus() : "");
		 			r.setTransId(sr.getTransId());
		 			r.setReportType(!sr.getReportType().equals(null) ? sr.getReportType() : "--");
		 			r.setCreatedDatetime(sr.getCreatedDatetime());
		 			r.setUserEmail(!sr.getUserEmail().equals(null) ? sr.getUserEmail() : "");
		         }
		        	 
			
			
		}
		
		return r;	
	}


	@Override

	public Report updateMakeAReport(Report re,String loginId,String createdDatetime) throws SQLException{
		Report update = new Report();
		SfdcReport updateSfdc = new SfdcReport();
		
		try{	
			int ind=1;
			log.info("dao>> "+re.toString());
			System.out.println("dao>>"+re.toString());
			System.out.println(">>>>>>>>>>>>>>>>>>>>"+re.toString());
			update = em.find(Report.class, re.getTransId());
			
			Session session = em.unwrap(Session.class);
	        SessionFactory sessionFactory = session.getSessionFactory();
	        SQLQuery query = session.createSQLQuery("SELECT * FROM SEB.REPORT where TRANS_ID="+re.getTransId()+" AND CREATED_DATETIME LIKE '"+createdDatetime+"%'");
	         List<Report> rs=  query.addEntity(Report.class).list();
	         System.out.println(rs.size()); 
	         if(rs.size()!=0) {
	        	 update=rs.get(0); 			
			if(re.getCaseNumber()!= null) {
				update.setCaseNumber(re.getCaseNumber());
				ind=0;
			}
				
			if(!StringUtil.trimToEmpty(re.getStatus()).equalsIgnoreCase("")) {
				update.setStatus(re.getStatus());
				ind=0;
			}
				
			if(!StringUtil.trimToEmpty(re.getRemark()).equalsIgnoreCase("")) {
				update.setRemark(re.getRemark());
				ind=0;
			}				
			em.merge(update);					
	         }else {
	        	 //SfdcReport report table update
	        	 
	        	   SQLQuery query2 = session.createSQLQuery("SELECT * FROM SEB.SFDC_REPORT where TRANS_ID="+re.getTransId()+" AND CREATED_DATETIME LIKE '"+createdDatetime+"%'");
			         List<SfdcReport> rsfdc=  query2.addEntity(SfdcReport.class).list();
			         System.out.println("SfdcReport list size "+rsfdc.size());
			         if(rsfdc.size()!=0) {
			        	 updateSfdc=rsfdc.get(0); 	
			 			if(re.getCaseNumber()!= null) {
			 				updateSfdc.setCaseNumber(String.valueOf(re.getCaseNumber()));
							ind=2;
						}
							
					/*	if(!StringUtil.trimToEmpty(re.getStatus()).equalsIgnoreCase("")) {
							updateSfdc.setStatus(re.getStatus());
							ind=2;
						}*/
			 			// @Rajeder added for seb status
			 			
			 			if(!StringUtil.trimToEmpty(re.getStatus()).equalsIgnoreCase("")) {
							updateSfdc.setSebStatus(re.getStatus());
							ind=2;
						}
							
						if(!StringUtil.trimToEmpty(re.getRemark()).equalsIgnoreCase("")) {
							updateSfdc.setRemark(re.getRemark());
							ind=2;
						}				
						em.merge(updateSfdc);
			        	 
			        	 
			        	 
			         }
	         }
			
			log.info("Remarks table inserted>>> "+re.toString());
		// added by pramod 
			
			if(ind==0) {
				ReportRemarksHistory rhh=new ReportRemarksHistory();
				 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				rhh.setIdInfo(update.getTransId());
				rhh.setUpdatedDatetime(timestamp);
				rhh.setCreatedDatetime(timestamp);
				rhh.setRemark(update.getRemark());
				rhh.setReportType(update.getReportType());
				rhh.setStatus(update.getStatus());
				rhh.setAdminEmail(loginId);
				rhh.setCuserEmail(update.getUserEmail());
				
				this.insertRemarksHistory(rhh);
				log.info("ReportRemarksHistory table inserted>>> "+re.toString());
			}else if(ind==2) {
				ReportRemarksHistory rhh=new ReportRemarksHistory();
				 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				rhh.setIdInfo(updateSfdc.getTransId());
				rhh.setUpdatedDatetime(timestamp);
				rhh.setCreatedDatetime(timestamp);
				rhh.setRemark(updateSfdc.getRemark());
				rhh.setReportType(updateSfdc.getReportType());
				rhh.setStatus(updateSfdc.getStatus());
				rhh.setAdminEmail(loginId);
				rhh.setCuserEmail(updateSfdc.getUserEmail());
				
				this.insertRemarksHistory(rhh);
				log.info("ReportRemarksHistory table inserted>>> "+re.toString());
			}
			
		
		}catch(NoResultException e){
			e.printStackTrace();
			log.error(e);
			return new Report();
		}	
		return update;
	}
	
	public void insertRemarksHistory(ReportRemarksHistory rrh) {
		em.persist(rrh);
	}
	

	/**============================================
	 * Customer Activities Report
	 *============================================*/
	@Override
	public List<CustomerAuditTrail> findAllCustAuditReport(Date dateFrom, Date dateTo, String email) throws Exception {
		List <CustomerAuditTrail> list = new ArrayList <CustomerAuditTrail>();
		try{
			list = em.createNamedQuery("CustomerAuditTrail.findAllCustAuditReport",CustomerAuditTrail.class)
					.setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.setParameter("email", "%"+email+"%").getResultList();
		}catch(NoResultException noR){}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return list;
	}	


	/**============================================
	 * User Statistic Report
	 *============================================*/
	@Override
	public long findTotalByStatus(String status, Date dateFrom, Date dateTo) throws Exception {
		long result  = 0;
		try{
			result = em.createNamedQuery("User.findTotalByStatus",Long.class)
					.setParameter("status", status.toUpperCase())
					.setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo).getSingleResult();

		}catch(NoResultException noR){}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	/*@Override
	public MyBillPayReportResp findAllBillPayReport(MyBillPayReportReq b0) throws Exception {
		
		List <SEBBillPayment> sebPaymentList = new ArrayList <SEBBillPayment>();
		List <MyPayment> myPaymentList = new ArrayList <MyPayment>(); 
		MyBillPayReportResp resp = new MyBillPayReportResp();

		String queryBillPaySQL1 = "select 0 AS spgPaymentId, CONCAT(pr.REFERENCE_PREFIX,LPAD(pr.PAYMENT_ID,8,'0' )) AS spgMerchantTransactionId, "
				+ "0 AS spgAmount, pr.GATEWAY_REF AS spgGatewayTransactionId, "
				+ "pr.PAYMENT_ID AS paymentId,pre.AMOUNT as amount, pr.SPG_BANK_ID AS bankId, pre.CONTRACT_ACCOUNT_NUMBER as contractAccountNumber,"
				+ "pr.CREATED_DATETIME AS createdDatetime, pr.GATEWAY_ID AS gatewayId,"
				+ "pr.GATEWAY_REF AS gatewayRef,pr.GATEWAY_STATUS AS gatewayStatus,pr.IS_MOBILE AS isMobile,"
				+ "pr.PAYMENT_DATETIME AS paymentDatetime,pr.STATUS as status, pr.UPDATED_DATETIME AS updatedDatetime,"
				+ "pr.USER_EMAIL AS userEmail,pr.SPG_BANK_ID AS spgBankID,pr.SPG_BANK_NAME AS spgBankName, " 
				+ "pr.SPG_BANK_TYPE AS spgBankType,"
				+ "pr.SPG_CARD_TYPE AS spgCardType,pr.REFERENCE_PREFIX AS referencePrefix," 
				+ "pr.NOTIFICATION_EMAIL AS notificationEmail "
				+ "FROM "+ApplicationConstant.SCHEMA+".PAYMENT_REFERENCE pr "
				+ "LEFT JOIN "+ApplicationConstant.SCHEMA+".PAYMENT_REFERENCE_ENTRY pre ON pre.PAYMENT_ID = pr.PAYMENT_ID WHERE" ;

		if(b0.getDateFrom()!=null && b0.getDateTo()!=null){
			System.out.println("dateFrom>>"+b0.getDateFrom());
			System.out.println("dateTo>>"+b0.getDateTo());			
			queryBillPaySQL1 +=" DATE(pr.CREATED_DATETIME) BETWEEN DATE('"+sdf.format(b0.getDateFrom())+"') AND DATE('"+sdf.format(b0.getDateTo())+"')";

		}

		if(b0.getContractAccNo() !=null && !b0.getContractAccNo().equalsIgnoreCase("")){
			System.out.println("contractAccNo>>"+b0.getContractAccNo());
			queryBillPaySQL1 +=" pr.CONTRACT_ACCOUNT_NUMBER='"+b0.getContractAccNo()+"'";

		}		
		if(b0.getSebRefStatus()!=null && !b0.getSebRefStatus().equalsIgnoreCase("ALL")){
			System.out.println("SebRefStatus>"+b0.getSebRefStatus());
			queryBillPaySQL1 +=" pr.STATUS='"+b0.getSebRefStatus()+"'";
		}
		if(b0.getEmail()!=null && !b0.getEmail().equalsIgnoreCase("")){
			System.out.println("email>>"+b0.getEmail());
			queryBillPaySQL1 +=" pr.USER_EMAIL='"+b0.getEmail()+"'";
		}
		if(b0.getPymtRef() !=null && !b0.getPymtRef().equalsIgnoreCase("")){
			System.out.println("payment reference>>"+b0.getPymtRef());
			queryBillPaySQL1 +=" pr.PAYMENT_ID='"+b0.getPymtRef()+"'";
		}
		if(b0.getPaymentMethod().equalsIgnoreCase("FPX")){//FPX
			if(!b0.getFpxTxnId().equalsIgnoreCase("")){
				System.out.println("fpx transaction id>>"+b0.getFpxTxnId());
				queryBillPaySQL1 +=" pr.GATEWAY_REF='"+b0.getFpxTxnId()+"'";
			}else {
				queryBillPaySQL1 +=" pr.REFERENCE_PREFIX='F'";
			}
		}else if(b0.getPaymentMethod().equalsIgnoreCase("MPG")){//MPG
			if(!b0.getMbbRefNo().equalsIgnoreCase("")){
				System.out.println("maybank reference>>"+b0.getMbbRefNo());
				queryBillPaySQL1 +=" pr.GATEWAY_REF='"+b0.getMbbRefNo()+"'";
			}else {
				System.out.println("GATEWAY_ID>>"+b0.getPaymentMethod());
				queryBillPaySQL1 +=" pr.REFERENCE_PREFIX='M'";
			}
		}

		
		try{
			queryBillPaySQL1 +=" ORDER BY pr.CREATED_DATETIME desc";

			log.info("sebBillPaymentResult ::"+ queryBillPaySQL1);

			Query query = em.createNativeQuery(queryBillPaySQL1, "sebBillPaymentResult");
			sebPaymentList = query.getResultList();
			DateUtil dateUtil = new DateUtil();
			for(SEBBillPayment l : sebPaymentList){
				MyPayment my = new MyPayment();

				my.setPaymentId(l.getPaymentId());

				my.setAmount(l.getAmount());
				my.setContractAccountNumber(l.getContractAccountNumber());
				my.setCreatedDatetime(dateUtil.convertToDate(l.getCreatedDatetime(), "yyyy-MM-dd HH:mm:ss.S"));
				my.setGatewayId(l.getGatewayId());
				my.setGatewayRef(l.getGatewayRef());
				my.setGatewayStatus(l.getGatewayStatus());
				my.setIsMobile(l.getIsMobile());
				my.setPaymentDatetime(dateUtil.convertToDate(l.getPaymentDatetime(), "yyyy-MM-dd HH:mm:ss.S"));
				my.setStatus(l.getStatus());
				my.setUpdatedDatetime(dateUtil.convertToDate(l.getUpdatedDatetime(), "yyyy-MM-dd HH:mm:ss.S"));
				my.setUserEmail(l.getUserEmail());
				my.setNotificationEmail(l.getNotificationEmail());
				my.setSpgBankID(l.getSpgBankID());
				my.setSpgBankName(l.getSpgBankName());
				my.setSpgBankType(l.getSpgBankType());
				my.setSpgCardType(l.getSpgCardType());
				my.setReferencePrefix(l.getReferencePrefix());

				my.setUserPaymentId(getReferenceNo(l.getReferencePrefix(),l.getPaymentId()));
				myPaymentList.add(my);


			}
			resp.setPaymentList(myPaymentList);


		}catch(NoResultException noR){
			log.error(noR.getMessage());
			noR.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return resp;
	}	*/

	@Override
	public MyBillPayReportResp findAllBillPayReport(MyBillPayReportReq b0) throws Exception {
		int pageNumber = b0.getPageNumber();
		int pageSize = b0.getPageSize();
		int totalRecords = 0;

		List <PaymentReferenceEntry> paymentList = new ArrayList <PaymentReferenceEntry>();
		List <MyPayment> myPaymentList = new ArrayList <MyPayment>(); 
		MyBillPayReportResp resp = new MyBillPayReportResp();	

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<PaymentReferenceEntry> cq = cb.createQuery(PaymentReferenceEntry.class);
		Root<PaymentReference> aRoot = cq.from(PaymentReference.class);
		Join<PaymentReferenceEntry, PaymentReferenceEntry> bJoin= aRoot.join("paymentReferenceEntries", JoinType.LEFT);
		List<Predicate> predicates = new ArrayList<Predicate>();
		cq.select(bJoin);
		cq.orderBy(cb.desc(aRoot.get("createdDatetime")));

		if(b0.getDateFrom()!=null && b0.getDateTo()!=null){
			System.out.println("dateFrom>>"+b0.getDateFrom());
			System.out.println("dateTo>>"+b0.getDateTo());
			Path<Date> createdDate = aRoot.get("createdDatetime");
			predicates.add(cb.between(createdDate, b0.getDateFrom(), b0.getDateTo()));

		}
		if(b0.getContractAccNo() !=null && !b0.getContractAccNo().equalsIgnoreCase("")){
			System.out.println("contractAccNo>>"+b0.getContractAccNo());
			Path<String> contractAccountNumber = bJoin.get("contractAccountNumber");
			predicates.add(cb.equal(contractAccountNumber, b0.getContractAccNo()));
		}		
		if(b0.getSebRefStatus()!=null && !b0.getSebRefStatus().equalsIgnoreCase("ALL")){
			System.out.println("SebRefStatus>"+b0.getSebRefStatus());
			Path<String> status = aRoot.get("status");
			predicates.add(cb.equal(status, b0.getSebRefStatus()));
		}
		if(b0.getEmail()!=null && !b0.getEmail().equalsIgnoreCase("")){
			System.out.println("email>>"+b0.getEmail());
			Path<String> email = aRoot.get("userEmail");
			predicates.add(cb.equal(email, b0.getEmail()));
		}
		if(b0.getPymtRef() !=null && !b0.getPymtRef().equalsIgnoreCase("")){
			System.out.println("payment reference>>"+b0.getPymtRef());
			Path<Integer> paymentId = aRoot.get("paymentId");
			predicates.add(cb.equal(paymentId, Integer.parseInt(b0.getPymtRef())));
		}
		if(b0.getPaymentMethod().equalsIgnoreCase("FPX")){//FPX
			if(!b0.getFpxTxnId().equalsIgnoreCase("")){
				System.out.println("fpx transaction id>>"+b0.getFpxTxnId());
				Path<String> fpxRef = aRoot.get("gatewayRef");
				predicates.add(cb.equal(fpxRef, b0.getFpxTxnId()));
			}else {
				Path<String> referencePrefix = aRoot.get("referencePrefix");
				predicates.add(cb.equal(referencePrefix, "F"));
			}
		}else if(b0.getPaymentMethod().equalsIgnoreCase("MPG")){//MPG
			if(!b0.getMbbRefNo().equalsIgnoreCase("")){
				System.out.println("maybank reference>>"+b0.getMbbRefNo());
				Path<String> gatewayRef = aRoot.get("gatewayRef");
				predicates.add(cb.equal(gatewayRef, b0.getMbbRefNo()));
			}else {
				System.out.println("GATEWAY_ID>>"+b0.getPaymentMethod());
				Path<String> referencePrefix = aRoot.get("referencePrefix");
				predicates.add(cb.equal(referencePrefix, "M"));
			}
		}

		try{
			cq.where(predicates.toArray(new Predicate[]{}));

			TypedQuery<PaymentReferenceEntry> query = em.createQuery(cq);
			paymentList = query.getResultList();

			for(PaymentReferenceEntry l : paymentList){
				MyPayment my = new MyPayment();

				my.setPaymentId(l.getPaymentReference().getPaymentId());

				my.setAmount(l.getAmount());
				my.setContractAccountNumber(l.getContractAccountNumber());
				my.setCreatedDatetime(l.getPaymentReference().getCreatedDatetime());
				my.setGatewayId(l.getPaymentReference().getGatewayId());
				my.setGatewayRef(l.getPaymentReference().getGatewayRef());
				my.setGatewayStatus(l.getPaymentReference().getGatewayStatus());
				my.setIsMobile(l.getPaymentReference().getIsMobile());
				my.setPaymentDatetime(l.getPaymentReference().getPaymentDatetime());
				my.setStatus(l.getPaymentReference().getStatus());
				my.setUpdatedDatetime(l.getPaymentReference().getUpdatedDatetime());
				my.setUserEmail(l.getPaymentReference().getUserEmail());
				my.setNotificationEmail(l.getPaymentReference().getNotificationEmail());
				my.setSpgBankID(String.valueOf(l.getPaymentReference().getSpgBankId()));
				my.setSpgBankName(l.getPaymentReference().getSpgBankName());
				my.setSpgBankType(l.getPaymentReference().getSpgBankType());
				my.setSpgCardType(l.getPaymentReference().getSpgCardType());
				my.setReferencePrefix(l.getPaymentReference().getReferencePrefix());
				my.setExternalGatewayStatus(l.getPaymentReference().getExternalGatewayStatus());
				my.setUserPaymentId(getReferenceNo(l.getPaymentReference().getReferencePrefix(),l.getPaymentReference().getPaymentId()));
				myPaymentList.add(my);


			}
			resp.setPaymentList(myPaymentList);


		}catch(NoResultException noR){
			log.error(noR.getMessage());
			noR.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
			log.error(ex.getMessage());
		}


		return resp;
	}	

	public static String getReferenceNo(String prefix, long referenceNo){
		return String.format("%s%08d", prefix, referenceNo);
	}

	@Override
	public PaymentGateway findPaymentGatewayByID(String id){		
		PaymentGateway finder = em.find(PaymentGateway.class, id);		
		return finder;
	}

	@Override
	public List<PaymentGateway> findAllPaymentGateway() throws SQLException {
		List <PaymentGateway> pgList = new ArrayList <PaymentGateway>();
		try{
			pgList = em.createNamedQuery("PaymentGateway.findAll",PaymentGateway.class).getResultList();
			System.out.println("pgList>>>"+pgList.size());
		}catch(NoResultException noR){
			log.error(noR.getMessage());
			noR.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return pgList;
	}


	@Override
	public List<BankResponse> findByMerchantidAndResponsecode(String merchantId, String status) throws SQLException {
		List <BankResponse> bankRespList = new ArrayList <BankResponse>();
		try{
			bankRespList = em.createNamedQuery("BankResponse.findByMerchantidAndResponsecode",BankResponse.class)
					.setParameter("merchantId", merchantId)
					.setParameter("responseCode", status)
					.getResultList();
			System.out.println("bankRespList>>>"+bankRespList.size());
		}catch(NoResultException noR){
			log.error(noR.getMessage());
			noR.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return bankRespList;
	}

}
