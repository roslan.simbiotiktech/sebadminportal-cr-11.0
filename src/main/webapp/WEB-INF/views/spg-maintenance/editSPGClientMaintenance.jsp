<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="width=device-width, initial-scale=1" name="viewport">
      <meta name="_csrf" content="${_csrf.token}" />
	  <meta name="_csrf_header" content="${_csrf.headerName}" />
      <meta content="" name="description">
      <meta content="" name="author">
      <%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
      <title></title>
      <style type="text/css">
	.test[style] {
		padding-right: 0 !important;
	}		
	.test.modal-open {
		overflow: auto!important;
	}
	.modal {
    overflow-y: scroll;
	}
</style>
   </head>
   <body>
      <div id="wrapper">
         <!-- Navigation -->
         <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
         </nav>
         <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp"%>
            <div class="row">
               <div class="col-lg-12" style="width: inherit; min-width: 100%">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Payment Client Maintenance &gt; Edits
                     </div>
                     <div class="panel-body">
                        <form id="profileForm" name="profileForm" method="post" action="${pageContext.request.contextPath}/admin/spg/pymtClientMaintenance/edit.do"
                           class="form-horizontal">
                           ${successMsg}${errorMsg}								
                           <div class="form-group required">
                              <p class="col-lg-5 control-label">
                                 Client Name <span style="color: red;">*</span> :
                              </p>
                              <div class="col-lg-3">
                                 <select class="form-control" id="clientName" name="clientName">
                                    <option value="">Please Select</option>
                                    <c:forEach items="${clientList}" var="item">
                                       <option value="${item.name}">${item.name}</option>
                                    </c:forEach>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-5 col-lg-offset-3 text-right">
                                 <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> <input type="hidden" id="displayType" name="displayType"
                                    value="" />
                                 <button class="btn btn-primary" type="submit">Search</button>
                                 <button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
                              </div>
                           </div>
                        </form>
                        <div id="resultDiv" style="display: none; margin-top: 10%">
                           <form action="${pageContext.request.contextPath}/admin/spg/pymtClientMaintenance/edit.do?${_csrf.parameterName}=${_csrf.token}" class="form-horizontal"
                              id="viewResultForm" method="post" name="viewResultForm">
                              <h4>
                                 Search Results :Client Name :<b>${clientName}</b>
                              </h4>
                              <div class="form-group">
                                 <div class="alert alert-danger" id="messages" role="alert" style="display: none;"></div>
                              </div>
                              <table cellspacing="0" class="table table-striped table-bordered" id="tblSearch" style="max-width: 100%;">
                                 <thead>
                                    <tr>
                                       <th>Client ID</th>
                                       <th>Description</th>
                                       <th>Hash Key</th>
                                       <th>Server to Server Payment Status URL</th>
                                       <th>Client Payment Complete Redirect URL</th>
                                       <th>Client Status</th>
                                    </tr>
                                 <tbody>
                                    <c:if test="${not empty tableMerchantList}">
                                       <c:forEach items="${tableMerchantList}" var="item" varStatus="status">
                                          <tr>
                                             <td><a data-toggle="modal" onclick="merchantQuery(${item.id})">${item.id}</a></td>
                                             <td><p id="ndesc${item.id}">${item.description}</p></td>
                                             <td><p id="nhash${item.id}">${item.signature_secret}</p></td>
                                             <td><p id="nserverUrl${item.id}">${item.server_to_server_payment_update_url}</p></td>
                                             <td><p id="nclientUrl${item.id}">${item.client_payment_update_url}</p></td>
                                             <td>                                            
                                                <c:if test="${item.enabled == false}"><p id="nstatus${item.id}">DISABLED</p></c:if>
                                                <c:if test="${item.enabled == true}"><p>ENABLED</p></c:if>
                                             </td>
                                          </tr>
                                       </c:forEach>
                                    </c:if>
                                 </tbody>
                                 <!-- #viewResultForm -->
                                 <!-- #resultDiv -->
                                 </thead>
                                 <tbody></tbody>
                              </table>
                              <!-- edit modal -->
                              <div class="modal" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                 <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                          <h4 class="modal-title" id="myModalLabel">Payment Client Details</h4>
                                       </div>
                                       <div id="paymentClientDetails" class="modal-body">
                                          <div class="form-group">
                                             <p class="col-lg-6 control-label">Client ID :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                                <label class="lblNoBoldCls" id="eClientId" name="eClientId" />
                                             </p>
                                          </div> 
                                          
                                           <div class="form-group">
                                             <p class="col-lg-6 control-label">Client Name :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                                <label class="lblNoBoldCls" id="eClientName" name="eClientName" />
                                             </p>
                                          </div> 
                                                                                   
                                          <div class="form-group">
                                             <p class="col-lg-6 control-label">Description :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                                <textarea class="form-control" cols="23" id="eDescription" name="eDescription" rows="3" maxlength="500"></textarea>
                                             </p>
                                          </div>
                                          
                                          <div class="form-group">
                                             <p class="col-lg-6 control-label">Hash Key :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                    			<input class="form-control" id="eHashKey" name="eHashKey" placeholder="Hash Key" type="text" maxlength="64">
                                             </p>
                                          </div>
                                          
                                          <div class="form-group">
                                             <p class="col-lg-6 control-label">Server to Server Payment Status URL :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                    			<textarea class="form-control" cols="23" id="eStatusUrl" name="eStatusUrl" rows="3" maxlength="1000"></textarea>
                                             </p>
                                          </div>
                                          
                                          <div class="form-group">
                                             <p class="col-lg-6 control-label">Client Payment Complete Redirect URL :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                        		<textarea class="form-control" cols="23" id="eRedirectUrl" name="eRedirectUrl" rows="3" maxlength="1000"></textarea>
                                             </p>
                                          </div>
                                          
                                           <div class="form-group">
                                             <p class="col-lg-6 control-label">Status :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                        		<select class="form-control capital" id="selectStatus" name="selectStatus">
													<option value="false">DISABLED</option>
													<option value="true">ENABLED</option>
												</select>
                                             </p>
                                          </div>
                                          
                                           <div class="form-group">
                                             <p class="col-lg-6 control-label">Last Updated DateTime :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                                <label class="lblNoBoldCls" id="updateDt" name="updateDt" />
                                             </p>
                                          </div> 
                                          
                                           <div class="form-group">
                                             <p class="col-lg-6 control-label">Updated By :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                                <label class="lblNoBoldCls" id="updateBy" name="updateBy" />
                                             </p>
                                          </div> 
                                          
                                          <div class="modal-footer">
                                             <button class="btn btn-primary" id="updateBtn" type="submit" data-toggle="modal" data-target="#confirmModal1">Update</button>
                                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- #edit modal -->
                              <!-- Add Update Confirmation Modal -->
									<div class="modal" id="confirmModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button class="close" data-dismiss="modal" type="button">
														<span>&times;</span><span class="sr-only">Close</span>
													</button>
													<h2 class="modal-title">Confirmation</h2>
												</div>
												<div class="modal-body">
												Are you sure you want to update client details?
												</div>
												<div class="modal-footer">
													<button class="btn btn-primary" id="submitBtn" type="button">Update</button>
													<button class="btn btn-warning" data-dismiss="modal" type="button">Cancel</button>
												</div>
											</div>
										</div>
									</div>
									<!-- End Add Update(Preferred Method) Confirmation Modal -->
                              <!-- modal show status after success/fail 1  -->
                                <div class="modal" id="statusModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel">Information</h4>
                                            </div>
                                            <div id="statusContent1" class="modal-body"></div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- #modal show status after success/fail 1 -->
                           </form>
                        </div>
                        <!-- /.resultList -->
                        <!-- /.resultList -->
                     </div>
                     <!-- /.panel-body -->
                  </div>
                  <!-- /.panel -->
               </div>
               <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         </div>
         <!-- /#page-wrapper -->
    </div>
    <script type="text/javascript">
    
    function resetForm() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#addForm').find('.has-success').removeClass("has-success");
    	$('#addForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#addForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();
    	$('#addForm').formValidation('resetForm', true);
    	$('select').val('');
    	
    	return false;
    }
    
    
    function merchantQuery(id) { 
    	console.log("id>"+id);
    	
	    var token = $("meta[name='_csrf']").attr("content");
	    var header = $("meta[name='_csrf_header']").attr("content");
	    $.ajax({
	        url: "${pageContext.request.contextPath}/admin/spg/pymtClientMaintenance/merchantQuery.do",
	        type: "POST",
			dataType: "json",
	        data: JSON.stringify({
	        	id: id
	         }),
	         beforeSend: function(xhr) {
                 xhr.setRequestHeader(header, token);
                 xhr.setRequestHeader("Accept", "application/json");
                 xhr.setRequestHeader("Content-Type", "application/json");
                 sebApp.showIndicator();
             },
	        success: function(data) {
	        	console.log(data.success);
              	if (data.success == true) {
              		var html = "";
    	        	$('#detailsModal').modal('show');  
                	$('#eClientId').html(data.objectDetail.id);
                	$('#eClientName').html(data.objectDetail.name);
                	$('#updateDt').html(data.objectDetail.last_updated_datetime);
                	$('#updateBy').html(data.objectDetail.last_updated_by);
                	$('#eDescription').val(data.objectDetail.description);
                	$('#eHashKey').val(data.objectDetail.signature_secret);
                	$('#eStatusUrl').val(data.objectDetail.server_to_server_payment_update_url);
                	$('#eRedirectUrl').val(data.objectDetail.client_payment_update_url);
                	$('#selectStatus').val(''+data.objectDetail.enabled+'').change();
              	}else{
              		$('#statusModal1').modal('show'); 
              		$('#statusContent1').html('<span style="color:red;">'+data.message+'</span> ');
              	}
	        	
	        },
	        failure: function(data) {
	            alert("fail");
	        },
	        complete: function(e) {
	            sebApp.hideIndicator();
	        }
	    });
	}
    
    //[start]update profile
    $('#viewResultForm').formValidation({
       	framework: 'bootstrap',
     	excluded: ':disabled',
     	icon : {
			invalid : 'glyphicon glyphicon-remove',
		},
        fields: {        	
            eDescription: {
                validators: {
                    notEmpty: {
                        message: 'Description is required'
                    } 
                }
            },
            eHashKey: {
                validators: {
                    notEmpty: {
                        message: 'Hash Key is required'
                    } 
                }
            },
            eStatusUrl: {
                validators: {
                    notEmpty: {
                        message: 'Server to Server Payment Status URL is required'
                    } 
                }
            }
        }
    }).on('success.form.fv', function(e) {
        // Prevent form submission
		e.preventDefault();
      	var isValidForm = $('#viewResultForm').data('formValidation').isValid();
      	console.log("isValidForm>"+isValidForm);
      	if(isValidForm){
     		 $("button#submitBtn").unbind('click');			
     		 //ajax submit update
            $("button#submitBtn").click(function() {
            	$('#detailsModal').modal('show');  
           
            	console.log($('#eClientId').html());
            	console.log($('#eClientName').html());
            	console.log($('#eDescription').val());
            	console.log($('#eHashKey').val());
            	console.log($('#eStatusUrl').val());
            	console.log($('#eRedirectUrl').val());
            	console.log($('#selectStatus').val());
            	
        			var token = $("meta[name='_csrf']").attr("content");
        			var header = $("meta[name='_csrf_header']").attr("content");
        			$.ajax({
        				url: "${pageContext.request.contextPath}/admin/spg/pymtClientMaintenance/doUpdatePaymentClient.do",
        				type: "POST",
        				dataType: "json",
        				data: JSON.stringify({
        					id : $('#eClientId').html(),
        					name : $('#eClientName').html(),
        					description: $('#eDescription').val(),
        					signature_secret: $('#eHashKey').val(),
        					server_to_server_payment_update_url : $('#eStatusUrl').val(),
        					client_payment_update_url : $("#eRedirectUrl").val(),
        					enabled: $('#selectStatus').val()
        				}),
        				
        				beforeSend: function(xhr) {
        					xhr.setRequestHeader(header, token);
        					xhr.setRequestHeader("Accept", "application/json");
        					xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
        					sebApp.showIndicator();
        				},
        				success: function(data) {
        					$('#statusModal1').modal('show'); 
        					if (data.success == true) {        					
            					console.log("success update.");
        						$('#statusContent1').html(data.message);
        						console.log('#ndesc'+data.objectDetail.id);
        						$('#ndesc'+data.objectDetail.id).html(data.objectDetail.description);
        						$('#nhash'+data.objectDetail.id).html(data.objectDetail.signature_secret);
        						$('#nserverUrl'+data.objectDetail.id).html(data.objectDetail.server_to_server_payment_update_url);
        						$('#nclientUrl'+data.objectDetail.id).html(data.objectDetail.server_to_server_payment_update_url);
        						if(data.objectDetail.enabled == true){
        							$('#nstatus'+data.objectDetail.id).html('ENABLED');
        						}else
        							$('#nstatus'+data.objectDetail.id).html('DISABLED');
        						
        					}else{
        	              		$('#statusContent1').html('<span style="color:red;">'+data.message+'</span> ');
        					}
        									
        				},
        				failure: function(data) {
        					alert("Fail To Connect");
        				},
        				complete: function() {
        					sebApp.hideIndicator();
        					$('#detailsModal').modal('hide');
        		  			$('#confirmModal1').modal('hide');
        				}
        			});
        		})
            //end ajax submit update profile
      	}
  });
    //[end] update profile
    
    $(document).ready(function() {    	
    	if (${tableMerchantList != null && tableMerchantList.size() >= 0}) {
			$('#resultDiv').show();
			var table = $('#tblSearch').DataTable({
				//  responsive: true,   
				bFilter: false,
				bSort: false,
				"autoWidth": false,
				
				lengthMenu: [
					[50, 100],
					[50, 100]
				]
			});
		}
    	
    	$('#detailsModal').on('show.bs.modal', function (e) {
	    	$('body').addClass('test');
		});	
    	$('#statusModal1').on('show.bs.modal', function (e) {
	    	$('body').addClass('test');
		});
    	
    	$('#detailsModal').on('hidden.bs.modal', function() {
			$('#viewResultForm').formValidation('resetForm', true);
		});
	    
	
    	

    });
    </script>
</body>
</html>