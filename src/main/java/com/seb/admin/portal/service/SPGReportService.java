package com.seb.admin.portal.service;

import java.sql.SQLException;
import java.util.List;

import com.seb.admin.portal.model.BankResponse;

public interface SPGReportService {
	
	public List<BankResponse> findByMerchantidAndResponsecode(String merchantId, String responseCode) throws SQLException;



}
