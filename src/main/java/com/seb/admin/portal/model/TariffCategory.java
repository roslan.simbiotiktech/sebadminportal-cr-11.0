package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.seb.admin.portal.constant.ApplicationConstant;

import java.util.List;


/**
 * The persistent class for the TARIFF_CATEGORY database table.
 * 
 */
@Entity
@Table(name="TARIFF_CATEGORY" , schema = ApplicationConstant.SCHEMA)
@NamedQuery(name="TariffCategory.findAll", query="SELECT t FROM TariffCategory t")
@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
public class TariffCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(name="DELETED_FLAG")
	private int deletedFlag;

	@Column(length=255)
	private String description;

	@Column(name="NAME")
	private String name;

	//bi-directional many-to-one association to TariffSetting
	@OneToMany(mappedBy="tariffCategory")
	private List<TariffSetting> tariffSettings;

	public TariffCategory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@JsonManagedReference //for children table
	public List<TariffSetting> getTariffSettings() {
		return this.tariffSettings;
	}

	public void setTariffSettings(List<TariffSetting> tariffSettings) {
		this.tariffSettings = tariffSettings;
	}

	public TariffSetting addTariffSetting(TariffSetting tariffSetting) {
		getTariffSettings().add(tariffSetting);
		tariffSetting.setTariffCategory(this);

		return tariffSetting;
	}

	public TariffSetting removeTariffSetting(TariffSetting tariffSetting) {
		getTariffSettings().remove(tariffSetting);
		tariffSetting.setTariffCategory(null);

		return tariffSetting;
	}

	@Override
	public String toString() {
		return "TariffCategory [id=" + id + ", deletedFlag=" + deletedFlag + ", description=" + description + ", name="
				+ name + ", tariffSettings=" + tariffSettings + "]";
	}

	
}