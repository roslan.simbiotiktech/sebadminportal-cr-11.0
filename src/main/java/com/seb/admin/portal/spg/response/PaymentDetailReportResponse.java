package com.seb.admin.portal.spg.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OPaymentDetailReport;
import com.seb.admin.portal.spg.entity.ResponseStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentDetailReportResponse {
	@JsonProperty("status")
	private ResponseStatus responseStatus;
	
	@JsonProperty("payment_detail_reports")
	private List<OPaymentDetailReport> paymentDetailReport;

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	public List<OPaymentDetailReport> getPaymentDetailReport() {
		return paymentDetailReport;
	}

	public void setPaymentDetailReport(List<OPaymentDetailReport> paymentDetailReport) {
		this.paymentDetailReport = paymentDetailReport;
	}

	@Override
	public String toString() {
		return "responseStatus:[" + responseStatus + "], paymentDetailReport:[" + paymentDetailReport + "]";
	}

	
}
