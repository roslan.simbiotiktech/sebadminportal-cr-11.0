package com.seb.admin.portal.spg.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OFpxBank;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OFpxBankUpdateRequest extends SPGAPIGeneral {
	
	@JsonProperty("fpx_bank")
	private OFpxBank fpx_bank;

	public OFpxBank getFpx_bank() {
		return fpx_bank;
	}

	public void setFpx_bank(OFpxBank fpx_bank) {
		this.fpx_bank = fpx_bank;
	}

	@Override
	public String toString() {
		return "fpx_bank:[" + fpx_bank + "]";
	}
	
	

}
