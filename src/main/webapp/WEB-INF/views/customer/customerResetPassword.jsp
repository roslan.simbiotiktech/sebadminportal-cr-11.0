<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="" name="description">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>

<title></title>
</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->

		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="font-weight: bold">Customer &gt; Reset Password</div>
						<div class="panel-body">
							<form id="resetPwdForm" name="resetPwdForm" method="post" action="${pageContext.request.contextPath}/admin/customerResetPassword.do"
								class="form-horizontal">
								${successMsg}${errorMsg}
								<div id="successMsg"></div>
								<div id="errorMsg"></div>
								<div class="form-group" id="msisdnDiv">
									<p class="col-lg-5 control-label">
										Mobile Number  :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="msisdn" maxlength="20" name="msisdn" placeholder="Mobile Number" type="text">
									</div>
								</div>

								<div class="form-group" id="nameDiv">
									<p class="col-lg-5 control-label">
										Name  :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="name" maxlength="160" name="name" placeholder="Name" type="text">
									</div>
								</div>


								<div class="form-group" id="loginIDDiv">
									<p class="col-lg-5 control-label">
										Login ID  :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="loginID" maxlength="50" name="loginID" placeholder="Login ID" type="text">
									</div>
								</div>


								<div class="form-group" id="emailDiv">
									<p class="col-lg-5 control-label">
										Email  :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="email" maxlength="50" name="email" placeholder="Email" type="text">
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" id="submitBtn" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
							</form>

							<div id="resultDiv" style="display: none; margin-top: 10%">
								<form action="${pageContext.request.contextPath}/admin/auditTrails.do?${_csrf.parameterName}=${_csrf.token}" class="form-horizontal"
									id="viewForm" method="post" name="viewForm">
									<h4>
										Search Results : Mobile Number :<b>${msisdn}</b>     Name :<b>${name}</b>     <br>
										Login Id :<b>${loginId}</b>     Email :<b>${email}</b>
									</h4>
									<div class="form-group">
										<div class="alert alert-danger" id="messages" role="alert" style="display: none;"></div>
									</div>

									<table cellspacing="0" class="table table-bordered" id="tblSearch">
										<thead>
											<tr>
												<th>No.</th>
												<th>Login ID</th>
												<th>Mobile Number</th>
												<th>Last Login Date</th>
												<th>Contract Account Number</th>
												<th>Contract Account Name</th>
												<th>Account Status</th>

											</tr>
											<!-- #viewATForm -->
											<!-- #resultDiv -->
										</thead>
										<tbody>											
										</tbody>
									</table>
									<input type="hidden" name="loginId" id="loginId" value="" />
									<!-- Add Confirmation Modal -->
									<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button class="close" data-dismiss="modal" type="button">
														<span>&times;</span><span class="sr-only">Close</span>
													</button>
													<h2 class="modal-title">Confirmation</h2>
												</div>
												<div class="modal-body" id="content"></div>
												<div class="modal-footer">
													<button class="btn btn-primary" id="add" type="button">Reset Password</button>
													<button class="btn btn-warning" data-dismiss="modal" type="button">Cancel</button>
												</div>
											</div>
										</div>
									</div>
									<!-- End Confirmation Modal -->
								<!-- modal show after success/fail reset -->
								<div class="modal" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 class="modal-title" id="myModalLabel">Reset Password</h4>
											</div>
											<div id="statusContent" class="modal-body"></div>
											<div class="modal-footer">
												<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- #modal show after success/fail reset -->
								</form>
							</div>
							<!-- #resultDiv -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel panel-default -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
		</div>
	</div>
	<script type="text/javascript">
	function resetForm() {
		$('#').find('.has-error').removeClass("has-error");
		$('#resetPwdForm').find('.has-success').removeClass("has-success");
		$('#resetPwdForm').find('.form-control-feedback').removeClass('glyphicon-remove');
		$('#resetPwdForm').find('.form-control-feedback').removeClass('glyphicon-ok');
		$('#resetPwdForm').formValidation('resetForm', true);
		$('.form-group').find('small.help-block').hide();
		$('.form-group').removeClass('has-error has-feedback');
		$('.form-group').find('i.form-control-feedback').hide();
		return false;
	}

	function onClickReset(loginId) {
		$('#loginId').val(loginId);
		$('#content').html('Are you sure you want to reset password for [<b>' + loginId + '</b>]');
	}
	$(document).ready(function() {
		if (${userList != null && userList.size() >= 0}) {
			$('#resultDiv').show();
			var columns = [{
        "mDataProp": "loginId"
    }, {
        "mDataProp": "loginId",
        "mRender": function(data, type, row) {
          var html = '';
          if (data != null) {
              html = "<p><a data-toggle=\"modal\" data-target=\"#myModal\" onclick=\"onClickReset('" + data + "')\">" + data + "</a></p>";
          }
          return html;
      }
    }, {
        "mDataProp": "mobileNumber"
    }, {
        "mDataProp": "lastLoginAt"
    }, {
        "mDataProp": "contractSubscribed",
        "mRender": function(data, type, row) {
            var html = '';
            if (data != null) {               
                $.each(data, function(index, currEmp) {
                    html += "<p>"+currEmp.accountNumber+"</p>";
                });
                html += "";
            }
            return html;
        }
    }, {
        "mDataProp": "contractSubscribed",
        "mRender": function(data) {
            var html = '<div>';
            if (data != null) {
                $.each(data, function(index, currEmp) {
                    html += "<p>" + currEmp.accountName + "</p>";
                });
                html += "</div>";
            }
            return html;
        }
    }, {
        "mDataProp": "contractSubscribed",
        "mRender": function(data) {
            var html = '<div>';
            if (data != null) {
                $.each(data, function(index, currEmp) {
                    html += "<p>" + currEmp.subscriptionStatus + "</p>";
                });
                html += "</div>";
            }
            return html;
        }
    }];
    var table = $('#tblSearch').DataTable({
        //  responsive: true,   
        bFilter: false,
        bSort: false,
        lengthMenu: [
            [50],
            [50]
        ],
        "bProcesing": true,
        "bServerSide": true,
        "bLengthChange": false,
        "iDisplayStart": 0,
        "sEmptyTable": "No data available in table",
        "sAjaxSource": "${pageContext.request.contextPath}/admin/searchProfileTable.do?${_csrf.parameterName}=${_csrf.token}&name=${name}&email=${email}&msisdn=${msisdn}&contractAccNo=${contractAccNo}&&loginId=${loginId}",
        "aoColumns": columns,
        /* [
         {
             "mDataProp": "activityAt"
         }, {
             "mDataProp": "mobileNumber"
         }, {
             "mDataProp": "mobileNumber"
         }, {
             "mDataProp": "loginId"
         }, {
             "mDataProp": "activity"
         },
     ], */
        "fnServerData": function(sSource, aoData, fnCallback) {
            $.ajax({
                beforeSend: function(xhr) {
                    sebApp.showIndicator();
                },
                dataType: 'json',
                type: "POST",
                url: sSource,
                data: aoData,
                success: function(data, textStatus, jqXHR) {
                    fnCallback(data, textStatus, jqXHR);
                },
                failure: function(data) {},
                complete: function(e) {
                    sebApp.hideIndicator();
                }
            });
        },
        "rowCallback": function(row, data, iDisplayIndex) {
            var info = table.page.info();
            var page = info.page;
            var length = info.length;
            var index = (page * length + (iDisplayIndex + 1));
            $('td:eq(0)', row).html(index);
        },
    });
		}
		$("#msisdn").keypress(function(e) {
			//if the letter is not digit then display error and don't type anything
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
				return false;
			}
		});
		$("button#add").click(function() {
			$('#myModal').modal('hide');    
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");
			$.ajax({
				url: "${pageContext.request.contextPath}/admin/resetCustomerPassword.do",
				type: "POST",
				dataType: "json",
				data: JSON.stringify({
					loginId: $('#loginId').val()
				}),
				beforeSend: function(xhr) {
					xhr.setRequestHeader(header, token);
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
					sebApp.showIndicator();
				},
				success: function(data) {
					if (data.respStat != null && data.respStat == 0) {
						console.log('data.newPassword>'+data.newPassword);
						console.log('data.loginId>'+data.loginId);
						$('#statusContent').html('New Password  <b>['+data.newPassword+']</b> has been sent to ['+data.loginId+'] registered mobile number.');
						$('#statusModal').modal('show');
						//$('#successMsg').html("<div id=\"successMsg\"  class=\"alert alert-success\" role=\"alert\"><span class=\"glyphicon glyphicon glyphicon-ok\" aria-hidden=\"true\"></span> <span class=\"sr-only\"></span>User\'s preferred communication method :"+data.preferredCommMethod+" </div>").show();
						//$('#successMsg').html("<div id=\"successMsg\"  class=\"alert alert-success\" role=\"alert\"><span class=\"glyphicon glyphicon glyphicon-ok\" aria-hidden=\"true\"></span> <span class=\"sr-only\"></span>Success Reset Password.</div>").show();

					} else if (data.respStat != null && data.respStat != 0) {

						$('#errorMsg').html("<div id=\"successMsg\" class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> <span class=\"sr-only\"></span> "+data.errDesc+" </div>").show();
					}
					//window.scrollTo(0, 5);					
				},
				failure: function(data) {
					alert("Fail To Connect");
				},
				complete: function() {
					sebApp.hideIndicator();
				}
			});
		})
		$('#resetPwdForm').formValidation({
			framework: 'bootstrap',
			icon: {
				invalid: 'glyphicon glyphicon-remove',
			},
			fields: {
				msisdn: {
					validators: {
						stringLength: {
							min: 10,
							max: 20,
							message: 'Mobile Number must at least 10 digits'
						}
					}
				},
				/* name: {
	                       validators: {
	                           notEmpty: {
	                               message: 'Name is required'
	                           }                       
	                       }
	                   },
	                   loginID: {
	                       validators: {
	                           notEmpty: {
	                               message: 'LoginID is required'
	                           }                       
	                       }
	                   },  */
				email: {
					validators: {
						emailAddress: {
							message: 'The value is not a valid email address'
						}
					}
				}
			}
		});
	});
</script>
</body>
</html>