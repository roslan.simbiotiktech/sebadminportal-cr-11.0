package com.seb.admin.portal.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;


public class AuthenticationFailureListener implements AuthenticationFailureHandler{

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException ae)
					throws IOException, ServletException {
		System.out.println("[onAuthenticationFailure]:"+ae.getMessage());
		response.sendRedirect("login.do?error=true&param="+ae.getMessage());

	}
}