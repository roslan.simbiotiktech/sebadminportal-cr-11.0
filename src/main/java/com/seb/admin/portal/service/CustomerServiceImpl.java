package com.seb.admin.portal.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.seb.admin.portal.adapter.Contract;
import com.seb.admin.portal.adapter.ContractDetail;
import com.seb.admin.portal.adapter.CustAuditTrail;
import com.seb.admin.portal.adapter.CustomerHistoryResponse;
import com.seb.admin.portal.adapter.ResendOTPRequest;
import com.seb.admin.portal.adapter.ResendOTPResponse;
import com.seb.admin.portal.adapter.ResetPasswordResponse;
import com.seb.admin.portal.adapter.SearchProfileRequest;
import com.seb.admin.portal.adapter.SearchProfileResponse;
import com.seb.admin.portal.adapter.USER;
import com.seb.admin.portal.adapter.ViewProfileResponse;
import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.dao.CustomerDAO;
import com.seb.admin.portal.manager.PropertiesManager;
import com.seb.admin.portal.model.Subscription;
import com.seb.admin.portal.util.StringUtil;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger log = Logger.getLogger(CustomerServiceImpl.class);
	PropertiesManager property = new PropertiesManager();

	@Autowired
	private CustomerDAO customerDAO;


	@Override
	public SearchProfileResponse cms_search_profile(int page, SearchProfileRequest req) throws Exception{		

		SearchProfileResponse myResp = new SearchProfileResponse();

		RestTemplate restTemplate = new RestTemplate();  
		restTemplate.getMessageConverters().add(new FormHttpMessageConverter());  
		HttpHeaders headers = new HttpHeaders();  
		headers.setContentType(MediaType.APPLICATION_JSON);  

		try { 

			Map reqObj = new LinkedHashMap();
			Map pagingObj = new LinkedHashMap();
			pagingObj.put("PAGE", page);
			pagingObj.put("RECORDS_PER_PAGE", 50);

			reqObj.put("PAGING", pagingObj);
			reqObj.put("LOGIN_ID", req.getLoginId());
			reqObj.put("EMAIL", req.getEmail());
			reqObj.put("NAME", req.getName());
			reqObj.put("MOBILE_NUMBER", req.getMobileNumber());
			reqObj.put("CONTRACT_ACC_NO", req.getContractAccNo());

			HttpEntity<String> entity = new HttpEntity<String>(JSONValue.toJSONString(reqObj),headers);
			ResponseEntity<String> result = restTemplate.exchange(property.getProperty(ApplicationConstant.SEARCH_PROFILE_URL), HttpMethod.POST, entity, String.class);

			System.out.println("*************[cms_search_profile]**************"); 
			log.info("HTTP URL :"+property.getProperty(ApplicationConstant.SEARCH_PROFILE_URL));
			log.info("HTTP Req :"+JSONValue.toJSONString(reqObj));
			log.info("HTTP Status :"+result.getStatusCode());
			//System.out.println("HTTP Resp :"+result.getBody());

			long RESP_STAT = -1;
			String ERR_CODE = "";
			StringBuilder ERR_DESC = new StringBuilder(""); 

			List<USER> usersList = new ArrayList<USER>();

			if(result.getStatusCode() == HttpStatus.OK){
				JSONParser parser = new JSONParser();
				try{
					Object obj = parser.parse(result.getBody());
					JSONObject jsonObject = (JSONObject) obj;

					// handle a structure into the json object
					JSONObject subObject = (JSONObject) jsonObject.get("STATUS");
					RESP_STAT = (Long) subObject.get("RESP_STAT");

					if(RESP_STAT!=0){
						JSONObject errorObj = (JSONObject) subObject.get("ERROR");	
						ERR_CODE = (String) errorObj.get("ERR_CODE");

						JSONArray errorList = (JSONArray) errorObj.get("ERR_DESC");
						Iterator<String> iterator = errorList.iterator();
						while (iterator.hasNext()) {
							ERR_DESC.append(iterator.next()+System.getProperty("line.separator"));
						}
					}else{// callback:sucess
						JSONObject paginationObj = (JSONObject) jsonObject.get("PAGINATION");
						myResp.setMaxPage(String.valueOf((Long) paginationObj.get("MAX_PAGE")));
						myResp.setTotalRecords(String.valueOf((Long) paginationObj.get("TOTAL_RECORDS")));

						JSONObject jsonPagingObj = (JSONObject) paginationObj.get("PAGING");	
						myResp.setPage(String.valueOf((Long) paginationObj.get("PAGE")));

						JSONArray userObject = (JSONArray) jsonObject.get("USERS");		
						if(userObject!=null){
							Iterator i = userObject.iterator();
							while (i.hasNext()) {
								USER myUser = new USER();

								JSONObject innerUserObj = (JSONObject) i.next();
								myUser.setStatus((String) innerUserObj.get("STATUS"));
								myUser.setLoginId((String) innerUserObj.get("LOGIN_ID"));
								myUser.setMobileNumber((String) innerUserObj.get("MOBILE_NUMBER"));
								//myUser.setCONTRACT_ACC_NO(cONTRACT_ACC_NO);
								myUser.setEmail((String) innerUserObj.get("EMAIL"));
								myUser.setLastLoginAt((String) innerUserObj.get("LAST_LOGIN_AT"));
								myUser.setName((String) innerUserObj.get("NAME"));

								JSONArray contractObject = (JSONArray) innerUserObj.get("CONTRACT_SUBSCRIBED");	
								if(contractObject!=null){
									Iterator c = contractObject.iterator();
									List<Contract> contractList = new ArrayList<Contract>();
									while (c.hasNext()) {
										Contract contract = new Contract();
										JSONObject innerContractObj = (JSONObject) c.next();
										contract.setAccountName((String) innerContractObj.get("ACCOUNT_NAME"));
										contract.setAccountNumber((String) innerContractObj.get("ACCOUNT_NUMBER"));
										contract.setSubscriptionStatus((String) innerContractObj.get("SUBSCRIPTION_STATUS"));
										contractList.add(contract);
									}//end loop contractList
									myUser.setContractSubscribed(contractList);							
								}							
								usersList.add(myUser);
							}//END users
						}

					}

				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			myResp.setRespStat(RESP_STAT);
			myResp.setErrCode(ERR_CODE);
			myResp.setErrDesc(ERR_DESC.toString());
			myResp.setRespUser(usersList);

		} catch (RestClientException re) {  
			if (re instanceof HttpClientErrorException) {  
				HttpClientErrorException he = (HttpClientErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				log.error(he.getStatusCode());  
			} else if (re instanceof HttpServerErrorException) {  
				HttpServerErrorException he = (HttpServerErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				log.error(he.getStatusCode());  
			}
			else{
				log.error("error :"+re.getMessage());
			}
		} catch (Exception e) {  
			log.error("error :"+e.getMessage());
			e.printStackTrace();  
		}
		return myResp;  
	}

	@Override
	public ViewProfileResponse cms_view_profile_details(ViewProfileResponse req) throws Exception{		
		ViewProfileResponse resp = new ViewProfileResponse();

		RestTemplate restTemplate = new RestTemplate();  
		restTemplate.getMessageConverters().add(new FormHttpMessageConverter());  
		HttpHeaders headers = new HttpHeaders();  
		headers.setContentType(MediaType.APPLICATION_JSON);  

		try { 

			Map reqObj = new LinkedHashMap();
			reqObj.put("LOGIN_ID", req.getLoginId());
			reqObj.put("CONTRACT_ACC_NO", req.getContractAccNo());

			HttpEntity<String> entity = new HttpEntity<String>(JSONValue.toJSONString(reqObj),headers);
			ResponseEntity<String> result = restTemplate.exchange(property.getProperty(ApplicationConstant.VIEW_PROFILE_DETAILS_URL), HttpMethod.POST, entity, String.class);

			System.out.println("*************[cms_search_profile]**************");  
			log.info("HTTP URL :"+property.getProperty(ApplicationConstant.VIEW_PROFILE_DETAILS_URL));
			log.info("HTTP Req :"+JSONValue.toJSONString(reqObj));
			log.info("HTTP Status :"+result.getStatusCode());
			//System.out.println("HTTP Resp :"+result.getBody());

			long RESP_STAT = -1;
			String ERR_CODE = "";
			StringBuilder ERR_DESC = new StringBuilder(""); 

			ContractDetail con = new ContractDetail();

			if(result.getStatusCode() == HttpStatus.OK){
				JSONParser parser = new JSONParser();
				try{
					Object obj = parser.parse(result.getBody());
					JSONObject jsonObject = (JSONObject) obj;

					// handle a structure into the json object
					JSONObject subObject = (JSONObject) jsonObject.get("STATUS");
					RESP_STAT = (Long) subObject.get("RESP_STAT");

					if(RESP_STAT!=0){
						JSONObject errorObj = (JSONObject) subObject.get("ERROR");	
						ERR_CODE = (String) errorObj.get("ERR_CODE");

						JSONArray errorList = (JSONArray) errorObj.get("ERR_DESC");
						Iterator<String> iterator = errorList.iterator();
						while (iterator.hasNext()) {
							ERR_DESC.append(iterator.next()+System.getProperty("line.separator"));
						}
					}else{// callback:sucess
						JSONObject contractDetailObj = (JSONObject) jsonObject.get("CONTRACT_DETAIL");	
						if(contractDetailObj!=null){
							con.setSubscribedAt((String) contractDetailObj.get("SUBSCRIBED_AT"));
							con.setAccountName((String) contractDetailObj.get("ACCOUNT_NAME"));
							con.setSubscriptionType((String) contractDetailObj.get("SUBSCRIPTION_TYPE"));
							con.setSubscriptionSatus((String) contractDetailObj.get("SUBSCRIPTION_STATUS"));
							con.setAccountNick((String) contractDetailObj.get("ACCOUNT_NICK"));
							con.setAccountNumber((String) contractDetailObj.get("ACCOUNT_NUMBER"));
							con.setRemark(StringUtil.trimToEmpty((String) contractDetailObj.get("REMARK")));
						}

						resp.setContractDetail(con);
						resp.setHomeTel((String)(jsonObject.get("HOME_TEL")));
						resp.setLoginId((String)(jsonObject.get("LOGIN_ID")));
						resp.setMobileNumber((String)(jsonObject.get("MOBILE_NUMBER")));
						resp.setNricOrPassport((String)(jsonObject.get("NRIC_OR_PASSPORT")));
						resp.setAccountStatus((String)(jsonObject.get("ACCOUNT_STATUS")));
						resp.setPreferredCommMethod(StringUtil.trimToEmpty((String)(jsonObject.get("PREFERRED_COMM_METHOD"))));
						resp.setEmail((String)(jsonObject.get("EMAIL")));
						resp.setOfficeTel(StringUtil.trimToEmpty((String)(jsonObject.get("OFFICE_TEL"))));
						resp.setLastLoginAt(StringUtil.trimToEmpty((String)(jsonObject.get("LAST_LOGIN_AT"))));
						resp.setName(StringUtil.trimToEmpty((String)(jsonObject.get("NAME"))));
						resp.setRemark(StringUtil.trimToEmpty((String)(jsonObject.get("REMARK"))));
					}

				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			resp.setRespStat(RESP_STAT);
			resp.setErrCode(ERR_CODE);
			resp.setErrDesc(ERR_DESC.toString());

		} catch (RestClientException re) {  
			if (re instanceof HttpClientErrorException) {  
				HttpClientErrorException he = (HttpClientErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				System.out.println(he.getStatusCode());  
			} else if (re instanceof HttpServerErrorException) {  
				HttpServerErrorException he = (HttpServerErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				log.error(he.getStatusCode());  
			}else {
				log.error("error :"+re.getMessage());
			}
		} catch (Exception e) {  
			log.error("error :"+e.getMessage());
			e.printStackTrace();  
		}
		return resp;  


	}

	@Override
	public CustomerHistoryResponse cms_view_customer_history(int page, String dateFrom, String dateTo, String msisdn, String contractAccNo, String loginId ) throws Exception {

		CustomerHistoryResponse resp = new CustomerHistoryResponse();
		List<CustAuditTrail> auditList = new ArrayList<CustAuditTrail>();

		RestTemplate restTemplate = new RestTemplate();  
		restTemplate.getMessageConverters().add(new FormHttpMessageConverter());  
		HttpHeaders headers = new HttpHeaders();  
		headers.setContentType(MediaType.APPLICATION_JSON);  

		try { 

			Map reqObj = new LinkedHashMap();
			Map pagingObj = new LinkedHashMap();
			pagingObj.put("PAGE", page);
			pagingObj.put("RECORDS_PER_PAGE", 50);

			reqObj.put("PAGING", pagingObj);
			reqObj.put("DATE_FROM", dateFrom);
			reqObj.put("DATE_TO", dateTo);
			reqObj.put("LOGIN_ID", loginId);
			reqObj.put("MOBILE_NUMBER", msisdn);
			reqObj.put("CONTRACT_ACC_NO", contractAccNo);


			HttpEntity<String> entity = new HttpEntity<String>(JSONValue.toJSONString(reqObj),headers);
			ResponseEntity<String> result = restTemplate.exchange(property.getProperty(ApplicationConstant.VIEW_CUSTOMER_HISTORY_URL), HttpMethod.POST, entity, String.class);

			System.out.println("*************[cms_view_customer_history]**************");  
			log.info("HTTP URL :"+property.getProperty(ApplicationConstant.VIEW_CUSTOMER_HISTORY_URL));
			log.info("HTTP Req :"+JSONValue.toJSONString(reqObj));
			log.info("HTTP Status :"+result.getStatusCode());
			//System.out.println("HTTP Resp :"+result.getBody());

			long RESP_STAT = -1;
			String ERR_CODE = "";
			StringBuilder ERR_DESC = new StringBuilder(""); 


			if(result.getStatusCode() == HttpStatus.OK){
				JSONParser parser = new JSONParser();
				try{
					Object obj = parser.parse(result.getBody());
					JSONObject jsonObject = (JSONObject) obj;

					// handle a structure into the json object
					JSONObject subObject = (JSONObject) jsonObject.get("STATUS");
					RESP_STAT = (Long) subObject.get("RESP_STAT");

					if(RESP_STAT!=0){
						JSONObject errorObj = (JSONObject) subObject.get("ERROR");	
						ERR_CODE = (String) errorObj.get("ERR_CODE");

						JSONArray errorList = (JSONArray) errorObj.get("ERR_DESC");
						Iterator<String> iterator = errorList.iterator();
						while (iterator.hasNext()) {
							ERR_DESC.append(iterator.next()+System.getProperty("line.separator"));
						}
					}else{// callback:sucess
						JSONObject paginationObj = (JSONObject) jsonObject.get("PAGINATION");
						resp.setMaxPage(String.valueOf((Long) paginationObj.get("MAX_PAGE")));
						resp.setTotalRecords(String.valueOf((Long) paginationObj.get("TOTAL_RECORDS")));

						JSONObject jsonPagingObj = (JSONObject) paginationObj.get("PAGING");	
						resp.setPage(String.valueOf((Long) paginationObj.get("PAGE")));

						JSONArray auditObj = (JSONArray) jsonObject.get("AUDIT_TRAILS");	

						if(auditObj!=null){
							Iterator au = auditObj.iterator();							
							while (au.hasNext()) {
								CustAuditTrail audit = new CustAuditTrail();
								JSONObject innerAuditObj = (JSONObject) au.next();

								audit.setActivityAt((String) innerAuditObj.get("ACITIVTY_AT"));
								audit.setLoginId((String) innerAuditObj.get("LOGIN_ID"));
								audit.setMobileNumber((String) innerAuditObj.get("MOBILE_NUMBER"));			
								audit.setActivity((String) innerAuditObj.get("ACTIVITY"));


								JSONArray contractObject = (JSONArray) innerAuditObj.get("CONTRACT_SUBSCRIBED");	
								if(contractObject!=null){
									Iterator c = contractObject.iterator();
									List<Contract> contractList = new ArrayList<Contract>();
									while (c.hasNext()) {
										Contract contract = new Contract();
										JSONObject innerContractObj = (JSONObject) c.next();
										contract.setAccountName((String) innerContractObj.get("ACCOUNT_NAME"));
										contract.setAccountNumber((String) innerContractObj.get("ACCOUNT_NUMBER"));
										contract.setSubscriptionStatus((String) innerContractObj.get("SUBSCRIPTION_STATUS"));
										contractList.add(contract);
									}//end loop contractList
									audit.setContractSubscribed(contractList);
								}

								auditList.add(audit);
							}//end loop audit trail list						
						}//(auditObj !=null)

					}

				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			resp.setRespStat(RESP_STAT);
			resp.setErrCode(ERR_CODE);
			resp.setErrDesc(ERR_DESC.toString());
			resp.setCustATList(auditList);

		} catch (RestClientException re) {  
			if (re instanceof HttpClientErrorException) {  
				HttpClientErrorException he = (HttpClientErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				log.error(he.getStatusCode());  
			} else if (re instanceof HttpServerErrorException) {  
				HttpServerErrorException he = (HttpServerErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				log.error(he.getStatusCode());  
			}else{
				log.error("error :"+re.getMessage());
			}
		} catch (Exception e) { 
			log.error("error :"+e.getMessage());
			e.printStackTrace();  
		}
		return resp;  



	}

	@Override
	public ResetPasswordResponse cms_reset_customer_password(String loginId) throws Exception {

		ResetPasswordResponse resp = new ResetPasswordResponse();

		RestTemplate restTemplate = new RestTemplate();  
		restTemplate.getMessageConverters().add(new FormHttpMessageConverter());  
		HttpHeaders headers = new HttpHeaders();  
		headers.setContentType(MediaType.APPLICATION_JSON);  

		try { 

			Map reqObj = new LinkedHashMap();
			reqObj.put("LOGIN_ID", loginId);			

			HttpEntity<String> entity = new HttpEntity<String>(JSONValue.toJSONString(reqObj),headers);
			ResponseEntity<String> result = restTemplate.exchange(property.getProperty(ApplicationConstant.RESET_CUSTOMER_PASSWORD_URL), HttpMethod.POST, entity, String.class);

			System.out.println("*************[cms_reset_customer_password]**************");  
			log.info("HTTP Req :"+JSONValue.toJSONString(reqObj));
			log.info("HTTP Status :"+result.getStatusCode());
			//System.out.println("HTTP Resp :"+result.getBody());

			long RESP_STAT = -1;
			String ERR_CODE = "";
			StringBuilder ERR_DESC = new StringBuilder(""); 

			if(result.getStatusCode() == HttpStatus.OK){
				JSONParser parser = new JSONParser();
				try{
					Object obj = parser.parse(result.getBody());
					JSONObject jsonObject = (JSONObject) obj;

					// handle a structure into the json object
					JSONObject subObject = (JSONObject) jsonObject.get("STATUS");

					RESP_STAT = (Long) subObject.get("RESP_STAT");

					if(RESP_STAT!=0){
						JSONObject errorObj = (JSONObject) subObject.get("ERROR");	
						ERR_CODE = (String) errorObj.get("ERR_CODE");

						JSONArray errorList = (JSONArray) errorObj.get("ERR_DESC");
						Iterator<String> iterator = errorList.iterator();
						while (iterator.hasNext()) {
							ERR_DESC.append(iterator.next()+System.getProperty("line.separator"));
						}
					}else{						
						resp.setPreferredCommMethod((String) jsonObject.get("PREFERRED_COMM_METHOD"));
					}

					resp.setRespStat(RESP_STAT);
					resp.setErrCode(ERR_CODE);
					resp.setErrDesc(ERR_DESC.toString());
					resp.setNewPassword((String)(jsonObject.get("NEW_PASSWORD")));
					resp.setLoginId(loginId);

				}catch (Exception e) {
					e.printStackTrace();
				}
			}


		} catch (RestClientException re) {  
			if (re instanceof HttpClientErrorException) {  
				HttpClientErrorException he = (HttpClientErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				log.error(he.getStatusCode());  
			} else if (re instanceof HttpServerErrorException) {  
				HttpServerErrorException he = (HttpServerErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				log.error(he.getStatusCode());  
			}else{
				log.error("error :"+re.getMessage());
			}
		} catch (Exception e) { 
			log.error("error :"+e.getMessage());
			e.printStackTrace();  
		}
		return resp; 
	}

	@Override
	public Subscription findSelectedSubscription(String email, String accountNumber, String status)
			throws SQLException {
		return customerDAO.findSelectedSubscription(email, accountNumber, status);
	}

	@Override
	@Transactional
	public boolean updateSubscriptionStatus(int subsId, String subscriptionStatus, String remark) throws SQLException{
		return customerDAO.updateSubscriptionStatus(subsId, subscriptionStatus, remark);

	}
	
	@Override
	@Transactional
	public boolean updateUserSubscription(String email, String oldStatus, String newStatus) throws SQLException{
		return customerDAO.updateUserSubscription(email, oldStatus, newStatus);
	}


	@Override
	@Transactional
	public boolean updateUserProfile(String loginId, String userStatus, String preferredMethod) throws SQLException{
		return customerDAO.updateUserProfile(loginId, userStatus, preferredMethod);
	}

	@Override
	@Transactional
	public ResendOTPResponse cms_resend_otp(ResendOTPRequest req) throws SQLException {
		ResendOTPResponse resp = new ResendOTPResponse();

		RestTemplate restTemplate = new RestTemplate();  
		restTemplate.getMessageConverters().add(new FormHttpMessageConverter());  
		HttpHeaders headers = new HttpHeaders();  
		headers.setContentType(MediaType.APPLICATION_JSON);  

		try { 

			Map reqObj = new LinkedHashMap();
			reqObj.put("LOGIN_ID", req.getLoginId());
			reqObj.put("TYPE", req.getType());	

			HttpEntity<String> entity = new HttpEntity<String>(JSONValue.toJSONString(reqObj),headers);
			ResponseEntity<String> result = restTemplate.exchange(property.getProperty(ApplicationConstant.RESEND_OTP_URL), HttpMethod.POST, entity, String.class);

			System.out.println("*************[cms_resend_otp]**************");  
			log.info("HTTP Req :"+JSONValue.toJSONString(reqObj));
			log.info("HTTP Status :"+result.getStatusCode());
			System.out.println("HTTP Req :"+JSONValue.toJSONString(reqObj));
			System.out.println("HTTP Status :"+result.getStatusCode());
			
			long RESP_STAT = -1;
			String ERR_CODE = "";
			StringBuilder ERR_DESC = new StringBuilder(""); 

			if(result.getStatusCode() == HttpStatus.OK){
				JSONParser parser = new JSONParser();
				try{
					Object obj = parser.parse(result.getBody());
					JSONObject jsonObject = (JSONObject) obj;

					// handle a structure into the json object
					JSONObject subObject = (JSONObject) jsonObject.get("STATUS");

					RESP_STAT = (Long) subObject.get("RESP_STAT");

					if(RESP_STAT!=0){
						JSONObject errorObj = (JSONObject) subObject.get("ERROR");	
						ERR_CODE = (String) errorObj.get("ERR_CODE");

						JSONArray errorList = (JSONArray) errorObj.get("ERR_DESC");
						Iterator<String> iterator = errorList.iterator();
						while (iterator.hasNext()) {
							ERR_DESC.append(iterator.next()+System.getProperty("line.separator"));
						}
					}else{						
						resp.setPreferredCommMethod((String) jsonObject.get("PREFERRED_COMM_METHOD"));
						resp.setOtp((String) jsonObject.get("OTP"));
					}

					resp.setRespStat(RESP_STAT);
					resp.setErrCode(ERR_CODE);
					resp.setErrDesc(ERR_DESC.toString());
					resp.setLoginId(req.getLoginId());
					resp.setTypeDesc(req.getTypeDesc());
					
				}catch (Exception e) {
					e.printStackTrace();
				}
			}


		} catch (RestClientException re) {  
			if (re instanceof HttpClientErrorException) {  
				HttpClientErrorException he = (HttpClientErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				log.error(he.getStatusCode());  
			} else if (re instanceof HttpServerErrorException) {  
				HttpServerErrorException he = (HttpServerErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				log.error(he.getStatusCode());  
			}else{
				log.error("error :"+re.getMessage());
			}
		} catch (Exception e) { 
			log.error("error :"+e.getMessage());
			e.printStackTrace();  
		}
		return resp; 

	}

	@Override
	public ResendOTPResponse cms_resend_mobile_verification_code(ResendOTPRequest req) throws SQLException {

		ResendOTPResponse resp = new ResendOTPResponse();

		RestTemplate restTemplate = new RestTemplate();  
		restTemplate.getMessageConverters().add(new FormHttpMessageConverter());  
		HttpHeaders headers = new HttpHeaders();  
		headers.setContentType(MediaType.APPLICATION_JSON);  

		try { 

			Map reqObj = new LinkedHashMap();
			reqObj.put("MOBILE_NUMBER", req.getMobileNumber());	

			HttpEntity<String> entity = new HttpEntity<String>(JSONValue.toJSONString(reqObj),headers);
			ResponseEntity<String> result = restTemplate.exchange(property.getProperty(ApplicationConstant.RESEND_MOBILE_VERIFICATION_URL), HttpMethod.POST, entity, String.class);

			System.out.println("*************[cms_resend_mobile_verification_code]**************");  
			log.info("HTTP Req :"+JSONValue.toJSONString(reqObj));
			log.info("HTTP Status :"+result.getStatusCode());
			System.out.println("HTTP Req :"+JSONValue.toJSONString(reqObj));
			System.out.println("HTTP Status :"+result.getStatusCode());
			long RESP_STAT = -1;
			String ERR_CODE = "";
			StringBuilder ERR_DESC = new StringBuilder(""); 

			if(result.getStatusCode() == HttpStatus.OK){
				JSONParser parser = new JSONParser();
				try{
					Object obj = parser.parse(result.getBody());
					JSONObject jsonObject = (JSONObject) obj;

					// handle a structure into the json object
					JSONObject subObject = (JSONObject) jsonObject.get("STATUS");

					RESP_STAT = (Long) subObject.get("RESP_STAT");

					if(RESP_STAT!=0){
						JSONObject errorObj = (JSONObject) subObject.get("ERROR");	
						ERR_CODE = (String) errorObj.get("ERR_CODE");

						JSONArray errorList = (JSONArray) errorObj.get("ERR_DESC");
						Iterator<String> iterator = errorList.iterator();
						while (iterator.hasNext()) {
							ERR_DESC.append(iterator.next()+System.getProperty("line.separator"));
						}
					}else{						
						resp.setPreferredCommMethod("");
						resp.setOtp((String) jsonObject.get("OTP"));
					}

					resp.setRespStat(RESP_STAT);
					resp.setErrCode(ERR_CODE);
					resp.setErrDesc(ERR_DESC.toString());
					resp.setLoginId(req.getLoginId());
					resp.setTypeDesc(req.getTypeDesc());

				}catch (Exception e) {
					e.printStackTrace();
				}
			}


		} catch (RestClientException re) {  
			if (re instanceof HttpClientErrorException) {  
				HttpClientErrorException he = (HttpClientErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				log.error(he.getStatusCode());  
			} else if (re instanceof HttpServerErrorException) {  
				HttpServerErrorException he = (HttpServerErrorException) re;  
				log.error(he.getResponseBodyAsString());  
				log.error(he.getStatusCode());  
			}else{
				log.error("error :"+re.getMessage());
			}
		} catch (Exception e) { 
			log.error("error :"+e.getMessage());
			e.printStackTrace();  
		}
		return resp; 

	
	}
}
