<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<%@ include file="../menuTitle.jsp"%>
<%@ include file="../global.jsp"%>
</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%>
			<%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Change Password</div>
						<div class="panel-body">
							<form id="chgPwdForm" action="${pageContext.request.contextPath}/admin/changePassword.do" method="post" class="form-horizontal" id="submitForm">
								${successMsg}${errorMsg}
								<div class="form-group">
									<p class="col-lg-5 control-label">Login ID :</p>
									<div class="col-lg-3">
										<input id="loginId" name="loginId" type="text" class="form-control" placeholder="Login ID"
											value=" ${userSession.loginId}" readonly="readonly">
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">New Password :</p>
									<div class="col-lg-3">
										<input id="password" name="password" type="password" class="form-control" placeholder="New Password" maxlength="100">
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">Confirm New Password :</p>
									<div class="col-lg-3">
										<input id="confirmPwd" name="confirmPwd" type="password" class="form-control" placeholder="Confirm New Password" maxlength="100">
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> <button class="btn btn-primary"  id="submitBtn"
											type="button" data-toggle="modal" data-target="#modal">Update</button> <button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
								
								 <!-- Add Confirmation Modal -->
								<div class="modal fade" id="modal" tabindex="-1">
						        <div class="modal-dialog">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button class="close" data-dismiss="modal" type="button"><span>&times;</span><span class="sr-only">Close</span></button>
											<h2 class="modal-title">Confirmation</h2>
						                </div>						
						                <div class="modal-body" id="add">
						                    Are you sure you want to change password?
						                </div>						
						                <div class="modal-footer">
						                    <button class="btn btn-primary" id="add" name="add" type="submit">Update</button> <button class="btn btn-warning" data-dismiss="modal" type="button">Close</button> 
						                </div>
						            </div>
						        </div>
   								</div>
   								<!-- End Confirmation Modal -->
							</form>

						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>

	<script type="text/javascript">
	function resetForm() {
		$('#').find('.has-error').removeClass("has-error");
		$('#chgPwdForm').find('.has-success').removeClass("has-success");
		$('#chgPwdForm').find('.form-control-feedback').removeClass('glyphicon-remove');
		$('#chgPwdForm').find('.form-control-feedback').removeClass('glyphicon-ok');
		$('.form-group').find('small.help-block').hide();
		$('.form-group').removeClass('has-error has-feedback');
		$('.form-group').find('i.form-control-feedback').hide();
		$('#chgPwdForm').formValidation('resetForm', true);
	}

    $(document).ready(function() {   
     $('#chgPwdForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
                password: {
                    validators: {
                        notEmpty: {
                            message: 'New Password is required'
                        },                        
                        regexp: {
                            regexp: /^[a-zA-Z0-9_]+$/,
                            message: 'New Password can only consist of alphabetical, number and underscore'
                        }
                    }
                },
                confirmPwd: {
                    validators: {
                        notEmpty: {
                            message: 'Confirm Password is required'
                        },
                        identical: {
                            field: 'password',
                            message: 'The password and its confirm are not the same'
                        }
                    }
                }           
            }
        });
    });
    </script>
</body>

</html>
