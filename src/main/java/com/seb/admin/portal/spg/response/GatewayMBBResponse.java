package com.seb.admin.portal.spg.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OMbb;
import com.seb.admin.portal.spg.entity.ResponseStatus;

public class GatewayMBBResponse {
	@JsonProperty("status")
	private ResponseStatus responseStatus;
	@JsonProperty("mbb")
	private OMbb mbbGateway;
	
	
	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}
	public OMbb getMbbGateway() {
		return mbbGateway;
	}
	public void setMbbGateway(OMbb mbbGateway) {
		this.mbbGateway = mbbGateway;
	}
	@Override
	public String toString() {
		return "responseStatus:[" + responseStatus + "], mbbGateway:[" + mbbGateway + "]";
	}
	
	

}
