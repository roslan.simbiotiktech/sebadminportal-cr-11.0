<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="_csrf" content="${_csrf.token}" />
	<meta name="_csrf_header" content="${_csrf.headerName}" />
    <meta content="" name="description">
    <meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
        </nav>
        <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp"%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Payment Gateway Maintenance &gt; Maybank Gateway
                        </div>
                        <div class="panel-body">
                            <form id="mbbForm" name="mbbForm" action="${pageContext.request.contextPath}/admin/spg/pymtGatewayMaintenance/mpg.do" class="form-horizontal" method="post" >
                                ${successMsg}${errorMsg}
                                <c:if test="${not empty mbb}">
                                    <c:set var="item" scope="session" value="${mbb}"/>
                                    
                               <%--  <div class="form-group">
                                    <p class="col-lg-5 control-label">ID <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                   	<input class="form-control" id="gatewayId" name="gatewayId" type="text" maxlength="20" value="${item.gatewayId}" readonly="readonly">
                                    </div>
                                </div> --%>
                                    
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Hash Key <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="hashKey" name="hashKey" placeholder="Hash Key" type="text" maxlength="20" value="${item.hash_key}" required>
                                    </div>
                                </div>
                                
                               <div class="form-group">
                                    <p class="col-lg-5 control-label">AMEX Hash Key <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="amexHashKey" name="amexHashKey" placeholder="AMEX Hash Key" type="text" maxlength="20" value="${item.amex_hash_key}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Merchant ID <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="merchantId" name="merchantId" placeholder="Merchant ID" type="text" maxlength="20" value="${item.merchant_id}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">AMEX Merchant ID <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="amexMerchantId" name="amexMerchantId" placeholder="AMEX Merchant ID" type="text" maxlength="20" value="${item.amex_merchant_id}" required>
                                    </div>
                                </div>                                
                                
                                 <div class="form-group">
                                    <p class="col-lg-5 control-label">Minimum Payment Amount :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="minPayAmt" name="minPayAmt" placeholder="Minimum Payment Amount" type="text" maxlength="10" value="${item.minimun_payment_amount }">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <p class="col-lg-5 control-label">Maximum Payment Amount :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="maxPayAmt" name="maxPayAmt" placeholder="Maximum Payment Amount" type="text" maxlength="10" value="${item.maximum_payment_amount }">
                                    </div>
                                </div>
                                
                               <div class="form-group">
                                    <p class="col-lg-5 control-label">Transaction Submit URL <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="trxSubmitURL" name="trxSubmitURL" placeholder="Transaction Submit URL" type="text" maxlength="1000" value="${item.submit_url}" required>
                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <p class="col-lg-5 control-label">Transaction Query URL <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="trxQueryURL" name="trxQueryURL" placeholder="Transaction Query URL" type="text" maxlength="1000" value="${item.query_url}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Status <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-6">
                                        <label class="radio-inline">
									      <input type="radio" name="statusOpt" ${not empty item.enabled && item.enabled == true ? 'checked' : ''} value="true">ENABLED
									    </label>
									    <label class="radio-inline">
									      <input type="radio" name="statusOpt"  ${not empty item.enabled && item.enabled == false ? 'checked' : ''} value="DISABLED">DISABLED
									    </label>
									</div>
								</div>
								
                                <div class="form-group">
                                    <div class="col-lg-5 col-lg-offset-3 text-right">
                                        <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
                                        <button class="btn btn-primary"  id="submitBtn" type="button" data-toggle="modal" data-target="#modal">Update</button> <button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
                                    </div>
                                </div>
                                </c:if>
                                <!-- Add Confirmation Modal -->
								<div class="modal fade" id="modal" tabindex="-1">
						        <div class="modal-dialog">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button class="close" data-dismiss="modal" type="button"><span>&times;</span><span class="sr-only">Close</span></button>
											<h2 class="modal-title">Confirmation</h2>
						                </div>						
						                <div class="modal-body" id="add">
						                    Are you sure you want to edit Maybank Gateway?
						                </div>						
						                <div class="modal-footer">
						                    <button class="btn btn-primary" id="add" name="add" type="submit">Update</button> <button class="btn btn-warning" data-dismiss="modal" type="button">Close</button> 
						                </div>
						            </div>
						        </div>
   								</div>
   								<!-- End Confirmation Modal -->
                            </form>
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /#page-wrapper -->
    </div>
    <script type="text/javascript">
    function resetSS() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#mbbForm').find('.has-success').removeClass("has-success");
    	$('#mbbForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#mbbForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();    	
    }
        
    
    function resetForm() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#mbbForm').find('.has-success').removeClass("has-success");
    	$('#mbbForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#mbbForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();
    	$('#mbbForm').formValidation('resetForm', true);
    	$('select').val('');
    	
    	return false;
    }
    $(document).ready(function() {    	
    	
        $('#mbbForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
            	hashKey: {
                    validators: {
                        notEmpty: {
                            message: 'Hash Key is required'
                        } 
                    }
                },
                amexHashKey: {
                    validators: {
                        notEmpty: {
                            message: 'AMEX Hash Key is required'
                        } 
                    }
                },
                merchantId: {
                    validators: {
                        notEmpty: {
                            message: 'Merchant Id is required'
                        } 
                    }
                },
                amexMerchantId: {
                    validators: {
                        notEmpty: {
                            message: 'AMEX Merchant Id is required'
                        } 
                    }
                },
                minPayAmt : {
					validators : {
						callback : {
							callback : function(value, validator, $field) {
							   var reg = /^-?\d+\.?\d*$/;
								if(!reg.test(value)){
									   return {
												valid : false,
												message : 'Please enter valid amount'
											}
								   }  else{
									   return true;									   
								   }   										   
							   
								
							}
						}
						 
					}
				},  
				maxPayAmt : {
					validators : {
						callback : {
							callback : function(value, validator, $field) {
							   var reg = /^-?\d+\.?\d*$/;
								if(!reg.test(value)){
									   return {
												valid : false,
												message : 'Please enter valid amount'
											}
								   }  else{
									   return true;									   
								   }   										   
							   
								
							}
						}
						 
					}
				},  
				trxSubmitURL: {
                    validators: {
                        notEmpty: {
                            message: 'Transaction Submit URL is required'
                        } 
                    }
                },
                trxQueryURL: {
                    validators: {
                        notEmpty: {
                            message: 'Transaction Query URL is required'
                        } 
                    }
                }//end
            }
        });
    });
    </script>
</body>
</html>