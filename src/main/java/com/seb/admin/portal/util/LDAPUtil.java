package com.seb.admin.portal.util;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import org.apache.log4j.Logger;

import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPReferralException;
import com.novell.ldap.LDAPSearchResults;
import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.manager.PropertiesManager;

public class LDAPUtil 
{
	private static final Logger log = Logger.getLogger(LDAPUtil.class);
	
	PropertiesManager property = new PropertiesManager();
	
	public LDAPUtil()
	{
		
	}

	//searchResults = lc.search( searchBase, searchScope, searchFilter, attributesToReturn, attributesOnlyFlag );
	
	/**
	 * createConnection = bind to AD
	 * @return
	 * date added: 10th April 2009
	 */
	private LDAPConnection createConnection()
	{
		LDAPConnection lc = null;
		
		try
		{
			/*System.out.println("LDAP_HOST>"+property.getProperty(ApplicationConstant.LDAP_HOST) );
			System.out.println("LDAP_PORT>"+property.getProperty(ApplicationConstant.LDAP_PORT) );
			System.out.println("LDAP_DN>"+property.getProperty(ApplicationConstant.LDAP_DN) );*/
			lc = new LDAPConnection();
			lc.connect(property.getProperty(ApplicationConstant.LDAP_HOST), Integer.parseInt(property.getProperty(ApplicationConstant.LDAP_PORT)));
			lc.bind(ApplicationConstant.LDAP_VERSION, property.getProperty(ApplicationConstant.LDAP_DN),  property.getProperty(ApplicationConstant.LDAP_PASSWORD).getBytes(ApplicationConstant.ENCODING_UTF8));
			
			log.info("createConnection() LDAP Connect = " + lc.isConnected());
		}
		catch(Exception e)
		{
			log.info( "createConnection() Error: " + e.toString() );
			e.printStackTrace();
		}
		finally
		{
			return lc;
		}
		
	}
	
	/**
	 * validateLogin = validate input against AD using AD sAMAccountName & password
	 * @param dn
	 * @param password
	 * @return
	 * date added: 10th April 2009
	 */
	public boolean validateLogin(String dn,String password)
	{	
		LDAPConnection lc = null;
		boolean result = false;
		try
		{
			lc = new LDAPConnection();
			lc.connect(property.getProperty(ApplicationConstant.LDAP_HOST),  Integer.parseInt(property.getProperty(ApplicationConstant.LDAP_PORT)));
			lc.bind(ApplicationConstant.LDAP_VERSION, dn, password.getBytes(ApplicationConstant.ENCODING_UTF8));
			System.out.println("validateLogin() LDAP Connect = " + lc.isConnected());
			log.info("validateLogin() LDAP Connect = " + lc.isConnected());
			result = lc.isConnected();
		}
		catch(Exception ex)
		{
			log.info("validateLogin() [" + dn + "] Invalid Login");
		}
		return result;	
	}
	
	/**
	 * getDN = retrieve DN of AD login
	 * @param userId
	 * @return
	 * date added: 10th April 2009
	 */
	public String getDN (String userId)
	{
		LDAPSearchResults searchResults = new LDAPSearchResults();
		LDAPConnection lc = null;
		String filterCriteria = null;
		String result = null;
		//String[] attrIDs = {"cn", "sn", "name", "givenName", "mailNickname", "name", "sAMAccountName", "mail", "ipPhone", "distinguishedName", "department"};
		
		try
		{	
		
			filterCriteria = "(" + property.getProperty(ApplicationConstant.LDAP_SEARCH_CRITERIA_1) + "="  + userId + ")";			
			lc = createConnection();		
			log.info("filterCriteria = " + filterCriteria);
			
			searchResults = lc.search( property.getProperty(ApplicationConstant.LDAP_SEARCH_BASE), ApplicationConstant.LDAP_SEARCH_SCOPE, filterCriteria, null, true);			
			if(searchResults.hasMore())
			{				
				try
				{
					LDAPEntry nextEntry = searchResults.next();	 
					
					result = nextEntry.getDN();
					log.info("getDN() result [" + userId + "] >> " + result);  //getDN() result  [wckwah] = CN=William Chan Kai Wah,OU=DataCenter,OU=Chulan Tower,OU=ARB Users,DC=alrajhibank,DC=com,DC=my
				}
				catch (LDAPReferralException ldre)
				{
					log.info("getDN() [" + userId + "] Record not Found");	
				}
			}
		}
		catch( LDAPException e ) 
		{	
			log.error("getDN(a)>LDAPException::"+e.getMessage());
			log.error("getDN(b)>LDAPException::"+e.getLDAPErrorMessage());
	        e.printStackTrace();
	    }
		catch( Exception e ) 
		{	
			log.error("getDN>exception::"+e.getMessage());
	        e.printStackTrace();
	    }   
		finally
		{
			try
			{
				if(lc != null)
				{
					lc.disconnect();
				}
			}
			catch( Exception e )
			{
				log.info( "Error: " + e.toString() );
		        e.printStackTrace();
			}						
		}	        
		return result;
	}
	
	/**
	 * checkOU = derive OU from DN
	 * @param dn
	 * @return
	 * date added: 10th April 2009
	 */
	public String checkOU(String dn)
	{
		String[] element = dn.split(",");
		String OU = "";
		
		//added on 25th April 2012
		for (int i = 0; i < element.length; i++)
		{
			if(element[i].toUpperCase().startsWith("OU"))
			{
				System.out.println("OU >> " + element[i]);
				
				OU += i > 1 ? "," : "";
				OU += element[i];
			}
		}
		
		log.info("DN no CN, no DC = " + OU);

		
			
/*   	 	for(int j = 0; j < element.length; j++)
   	 	{
   	 		log.info("element " + j + " = " + element[j]);
   	 		
   	 		if(element[j].split("=")[0].equalsIgnoreCase("OU"))
   	 		{
   	 			OU = element[j].split("=")[1];
   	 			log.info("element[j].split[1] = " + OU);
   	 			
   	 			if(OU.endsWith("Branch"))
   	 			{
   	 				OU = OU + "," + element[j+1];
   				
   	 				log.info("OU Branch = " + OU);   //OU=BangsarBranch,OU=Branches
   	 			
   	 				return OU;
   	 			}
   	 		}
   	 	}*/
   	 	if(OU!=null)
   	 	{	
   	 		return OU;
   	 	}
   	 	else
   	 	{
   	 		return "OU not exist";
   	 	}
	}
	
	
	//Search DN = DC=alrajhibank,DC=com,DC=my
	//Filter = (&(objectclass=*)(OU=*))   --> list out all OU in LDAP
	
	/**
	 * checkOU = derive OU from DN
	 * @param dn
	 * @return
	 * date added: 10th April 2009
	 */
	public ArrayList getOU(String ou)
	{
		LDAPSearchResults searchResults = new LDAPSearchResults();
		LDAPConnection lc = null;
		String filterCriteria = null;
		ArrayList result = new ArrayList();
		String a = "";
		//String[] attr = {"OU"};
		
		try
		{	
			if(ou.equalsIgnoreCase("all"))
			{
				filterCriteria = "(&(objectclass=*)(OU=*))";
			}
			else
			{
				filterCriteria = "(&(objectclass=*)(OU=" + ou + "))";
			}
			
			lc = createConnection();		
			log.info("filterCriteria OU = " + filterCriteria);
			
			searchResults = lc.search(  property.getProperty(ApplicationConstant.LDAP_SEARCH_BASE), ApplicationConstant.LDAP_SEARCH_SCOPE, filterCriteria, null, true);	
			
			while(searchResults.hasMore())
			{				
				try
				{
					LDAPEntry nextEntry = searchResults.next();	  				
					a = nextEntry.getDN().replace(",DC=sarawakenergy,DC=com,DC=my", "");
					//System.out.println("a = " + a);
					
					result.add(a);
				}
				catch (LDAPReferralException ldre)
				{
					log.info("getOU >> " + ou + " = Record not Found");	
				}
			}
		}
		catch( LDAPException e ) 
		{		
			log.error("getOU(a)>LDAPException::"+e.getMessage());
			log.error("getOU(b)>LDAPException::"+e.getLDAPErrorMessage());
	        e.printStackTrace();
	    }
		catch( Exception e ) 
		{			
			log.error("getOU>Exception::"+e.getMessage());
	        e.printStackTrace();
	    }   
		finally
		{
			try
			{
				if(lc != null)
				{
					lc.disconnect();
				}
			}
			catch( Exception e )
			{
				log.info( "Error: " + e.toString() );
		        e.printStackTrace();
			}						
		}	    
		
		//System.out.println("result = " + result);
		
		
		return result;
	}
	
	/**
	 * searchBySamacctName = search by sAMAccountName, cn (name) from AD
	 * @param samaAccount
	 * @param cnName
	 * @return
	 * date added: 20th April 2009
	 */
	public ArrayList searchBySamacctName(String samaAccount, String cnName)
	{
		ArrayList result = new ArrayList();
		
		LDAPSearchResults searchResults = new LDAPSearchResults();
		LDAPConnection lc = null;
		String filterCriteria = null;
		String a = "";
		
		try
		{	
			if(samaAccount.equalsIgnoreCase("") && !cnName.equalsIgnoreCase(""))
			{
				filterCriteria = "(&(objectclass=*)(cn=*" + cnName + "*))";
			}
			else if(!samaAccount.equalsIgnoreCase("") && cnName.equalsIgnoreCase(""))
			{
				filterCriteria = "(&(objectclass=*)(sAMAccountName=*" + samaAccount + "*))";
			}
			else if(!samaAccount.equalsIgnoreCase("") && !cnName.equalsIgnoreCase(""))
			{
				filterCriteria = "(&(objectclass=*)(|(sAMAccountName=*" + samaAccount + "*)(cn=*" + cnName + "*)))";
			}
			
			lc = createConnection();		
			
			log.info("filterCriteria searchBySamacctName = " + filterCriteria);
			
			searchResults = lc.search(  property.getProperty(ApplicationConstant.LDAP_SEARCH_BASE), ApplicationConstant.LDAP_SEARCH_SCOPE, filterCriteria, null, true);	
			
			while(searchResults.hasMore())
			{				
				try
				{
					LDAPEntry nextEntry = searchResults.next();	  				
					a = nextEntry.getDN();
					//System.out.println("b = " + a);
					
					result.add(a);
				}
				catch (LDAPReferralException ldre)
				{
					log.info("searchBySamacctName >> " + samaAccount + " = Record not Found");	
				}
			}
		}
		catch( LDAPException e ) 
		{			
			log.error("searchBySamacctName(a)>LDAPException::"+e.getMessage());
			log.error("searchBySamacctName(b)>LDAPException::"+e.getLDAPErrorMessage());
	        e.printStackTrace();
	    }
		catch( Exception e ) 
		{	
			log.error("searchBySamacctName>Exception::"+e.getMessage());
	        e.printStackTrace();
	    }   
		finally
		{
			try
			{
				if(lc != null)
				{
					lc.disconnect();
				}
			}
			catch( Exception e )
			{
				log.info( "Error: " + e.toString() );
		        e.printStackTrace();
			}						
		}	
		return result;
	}
	
	
	public ArrayList searchByEmail(String email)
	{
		ArrayList result = new ArrayList();
		
		LDAPSearchResults searchResults = new LDAPSearchResults();
		LDAPConnection lc = null;
		String filterCriteria = null;
		String a = "";
		
		try
		{	
			if(!email.equalsIgnoreCase(""))
			{
				filterCriteria = "(&(objectclass=*)(email=*" + email + "*))";
			}			
			
			lc = createConnection();		
			
			log.info("filterCriteria searchByEmail = " + filterCriteria);
			
			searchResults = lc.search(  property.getProperty(ApplicationConstant.LDAP_SEARCH_BASE), ApplicationConstant.LDAP_SEARCH_SCOPE, filterCriteria, null, true);	
			
			while(searchResults.hasMore())
			{				
				try
				{
					LDAPEntry nextEntry = searchResults.next();	  				
					a = nextEntry.getDN();
					//System.out.println("b = " + a);
					
					result.add(a);
				}
				catch (LDAPReferralException ldre)
				{
					log.info("searchByEmail >> " + email + " = Record not Found");	
				}
			}
		}
		catch( LDAPException e ) 
		{	
			log.error("searchByEmail(a)>LDAPException::"+e.getMessage());
			log.error("searchByEmail(b)>LDAPException::"+e.getLDAPErrorMessage());
	        e.printStackTrace();
	    }
		catch( Exception e ) 
		{		
			log.error("searchByEmail>Exception::"+e.getMessage());
	        e.printStackTrace();
	    }   
		finally
		{
			try
			{
				if(lc != null)
				{
					lc.disconnect();
				}
			}
			catch( Exception e )
			{
				log.info( "Error: " + e.toString() );
		        e.printStackTrace();
			}						
		}	
		return result;
	}
	
	/**
	 * 
	 * @return get LDAP connection using JNDI
	 */
	public LdapContext getLdapContext(){
		
		/*System.out.println("LDAP_HOST>"+property.getProperty(ApplicationConstant.LDAP_HOST) );
		System.out.println("LDAP_PORT>"+property.getProperty(ApplicationConstant.LDAP_PORT) );
		System.out.println("LDAP_DN>"+property.getProperty(ApplicationConstant.LDAP_DN) );*/
		
		LdapContext ctx = null;
		try{
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY,
					"com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.SECURITY_AUTHENTICATION, "Simple");		
			env.put(Context.SECURITY_PRINCIPAL, property.getProperty(ApplicationConstant.LDAP_DN));
			env.put(Context.SECURITY_CREDENTIALS, property.getProperty(ApplicationConstant.LDAP_PASSWORD));
			env.put(Context.PROVIDER_URL, "ldap://"+property.getProperty(ApplicationConstant.LDAP_HOST)+":"+property.getProperty(ApplicationConstant.LDAP_PORT));
			ctx = new InitialLdapContext(env, null);
			log.info("Connection Successful.");
		}catch(NamingException nex){
			log.info("LDAP Connection: FAILED");
			nex.printStackTrace();
		}
		return ctx;
	}

	
	/*public String getUserBasicAttributes(String loginId, LdapContext ctx) {
		String department = "";
		try {

			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String[] attrIDs = { "distinguishedName"};
			constraints.setReturningAttributes(attrIDs);
			//First input parameter is search bas, it can be "CN=Users,DC=YourDomain,DC=com"
			//Second Attribute can be uid=username
			NamingEnumeration answer = ctx.search(property.getProperty(ApplicationConstant.LDAP_SEARCH_BASE), "sAMAccountName="
					+ loginId, constraints);
			if (answer.hasMore()) {
				Attributes attrs = ((SearchResult) answer.next()).getAttributes();				
				if(attrs.get("distinguishedName")!=null){
					String distinguishedName = String.valueOf(attrs.get("distinguishedName"));
					String splitName [] = distinguishedName.split(",");
					department = splitName[1].replaceAll("OU=", "").trim();
				}				
				log.info("department="+department);

			}else{
				throw new Exception("Invalid User");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return department;
	}*/
	
	/**
	 * get all attributes
	 * @param criteria
	 * @param loginId
	 * @param ctx
	 * @return
	 */
	public String getUserBasicAttributes(String criteria, String loginId, LdapContext ctx) {
		String search ="";
		try {			
			if(criteria.equalsIgnoreCase("mail")){
				search = "mail";
			}else{
				search = "distinguishedName";
			}

			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String[] attrIDs = {search,"userPrincipalName"};
			constraints.setReturningAttributes(attrIDs);
			//First input parameter is search bas, it can be "CN=Users,DC=YourDomain,DC=com"
			//Second Attribute can be uid=username
			NamingEnumeration answer = ctx.search(property.getProperty(ApplicationConstant.LDAP_SEARCH_BASE), "sAMAccountName="
					+ loginId, constraints);
			if (answer.hasMore()) {
				System.out.println("criteria>>"+criteria+",search>>"+search);
				Attributes attrs = ((SearchResult) answer.next()).getAttributes();				
				if(!criteria.equalsIgnoreCase("mail")){
					String distinguishedName = attrs.get("distinguishedName").toString();
					System.out.println(">>"+distinguishedName);
					String splitName [] = distinguishedName.split(",");
					search = splitName[1].replaceAll("OU=", "").trim();
					
				} else{
					if(attrs.get("mail")!=null){
						search = attrs.get("mail").toString();
						search = search.replaceAll("mail:", "").trim();
					}else if(attrs.get("userPrincipalName")!=null){
						search = attrs.get("userPrincipalName").toString();	
						search = search.replaceAll("userPrincipalName:", "").trim();
					}else{
						search ="";
					}
				}
			}else{
				throw new Exception("Invalid User");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return search;
	}
	
	public static void main(String[] args) 
	{
		LDAPUtil ldap = new LDAPUtil();
		
		String samaAccount = "vusr02";
		String cnName = "yuenyyh";
		
		//1. GET DN BY sAMAccountName
		String dn = ldap.getDN("wckwah");	
		String ou = "";
		String[] element = dn.split(",");
		
		
		if(dn!=null)
		{
			//2. CHECK OU			
			System.out.println("dn = " + dn); //dn = CN=William Chan Kai Wah,OU=DataCenter,OU=Chulan Tower,OU=ARB Users,DC=alrajhibank,DC=com,DC=my
		
			for(int j = 0; j < element.length; j++)
			{
				System.out.println("element " + j + "=" + element[j]);
			}
			
			
			for (int i = 0; i < element.length; i++)
			{
				if(element[i].startsWith("OU"))
				{
					System.out.println("OU >> " + element[i]);
					
					ou += i > 1 ? "," : "";
					ou += element[i];
				}
			}
			
			System.out.println("new dn no CN, no DC = " + ou);

			
/*			if(!ldap.checkOU(dn).equalsIgnoreCase("OU not exist"))
			{
				System.out.println("Valid OU");
				//3. LOGIN VALIDATION
				System.out.println("Login = " + ldap.validateLogin(dn,"testing"));
			}*/
		}
		else
		{
			System.out.println("Invalid Login Username");
		}
		
		
		//2. get OU list from LDAP
		//ldap.getOU("all");
		
		
		//3. search bu samaccountname & name(cn)
		//ldap.searchBySamacctName(samaAccount, cnName);
		
		
	}	
}
