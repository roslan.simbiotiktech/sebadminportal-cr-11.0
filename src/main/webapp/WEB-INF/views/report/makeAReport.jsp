<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="" name="description">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
<title></title>
<style type="text/css">
.test[style] {
	padding-right: 0 !important;
}

.test.modal-open {
	overflow: auto;
}
</style>
</head>
<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>
		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>
			<div class="row">
				<div class="col-lg-12" style="width: inherit; min-width: 100%">
					<div class="panel panel-default">
						<div class="panel-heading" style="font-weight: bold">Report
							&gt; Make A Report</div>
						<div class="panel-body">
							<form id="searchReport" name="searchReport" action="${pageContext.request.contextPath}/admin/makeAReport.do" class="form-horizontal"
								method="post">
								${successMsg}${errorMsg}
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Date From <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='dateFrom'>
											<input class="form-control datepicker" id="indateFrom"
												name="indateFrom" readonly type='text'> <span
												class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Date To <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='dateTo'>
											<input class="form-control datepicker" id="indateTo"
												name="indateTo" readonly type='text'> <span
												class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Source <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<select class="form-control capital" id="selectSource"
											name="selectSource">
											<option value="">Please Select</option>
											<option value="ALL">All</option>
											<option value="APPS">Apps</option>
											<option value="WEB">Web</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Type <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<select class="form-control capital" id="selectType"
											name="selectType">
											<option value="">Please Select</option>
											<option value="ALL">All</option>
											<option value="BILLING_AND_METER">Billing & Meter</option>
											<option value="FAULTY_STREET_LIGHT">Faulty Street
												Light</option>
											<option value="GENERAL_INQUIRY">General Enquiry</option>
											<option value="OUTAGE">Outage</option>
											<option value="TECHNICAL_ISSUE">Technical Issue</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Status <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<select class="form-control capital" id="selectStatus"
											name="selectStatus">
											<option value="">Please Select</option>
											<option value="ALL">All</option>
											<option value="OPEN">Open</option>
											<option value="ESCALATED">Escalated</option>
											<option value="ASSIGNED">Assigned</option>
											<option value="IN_PROGRESS">In Progress</option>
											<option value="PLEASE_CONTACT_SEB">Please contact SEB</option>
											<option value="RESOLVED">Resolved</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input type="hidden" id="displayType" name="displayType"
											value="" /> <input type="hidden" id="displayStatus"
											name="displayStatus" value="" /> <input type="hidden"
											id="displaySource" name="displaySource" value="" /> <input
											name="${_csrf.parameterName}" type="hidden"
											value="${_csrf.token}">
										<button class="btn btn-primary" id="submitBtn" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1"
											onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
							</form>
							<div id="resultDiv" style="display: none; margin-top: 10%">
								<form id="viewForm" name="viewForm"
									action="${pageContext.request.contextPath}/admin/makeAReport.do"
									class="form-horizontal" method="post">
									<h4>
										Search Results : Date From :<b>${dateFrom}</b>, Date To :<b>${dateTo}</b>,
										Source :<b>${source}</b>,Type :<b>${type}</b>, Status :<b>${status}</b>
									</h4>
									<div class="form-group">
										<div class="alert alert-danger" id="messages" role="alert"
											style="display: none;"></div>
									</div>
									<table cellspacing="0"
										class="table table-striped table-bordered" id="tblSearch">
										<thead>
											<tr>
												<th>No.</th>
												<th>Date</th>
												<th>Report Type</th>
												<th>Issue Type</th>
												<th>Trans ID</th>
												<th>Case ID</th>
												<!-- <th>Doc ID</th> -->
												<th>Source</th>
												<th>Login ID/Email</th>
												<th>Mobile No</th>
												<th>CRM Status</th>
												<th>Status</th>
												<th>Station</th>
												<th>Location</th>
												<th>Description</th>
												<th>Remark</th>
											</tr>
										</thead>
										<tbody>
											<c:if test="${not empty reportList}">
												<c:forEach items="${reportList}" var="item"
													varStatus="status">
													<tr>
														<td>${status.index+1}</td>
														<td><fmt:formatDate type="both" dateStyle="short"
																timeStyle="medium" value="${item.createdDatetime}"
																pattern="dd/MM/yyyy hh:mm:ss a" /></td>
														<td>${item.reportType}</td>
														<td>${item.issueType}</td>
														<td><c:choose>
																<c:when
																	test="${fn:containsIgnoreCase(item.status, 'CLOSED')|| fn:containsIgnoreCase(item.status, 'RESOLVED')}">
														${item.transId}
														</c:when>
																<c:otherwise>
																	<a data-toggle="modal"
																		onclick="viewMyReport('${item.transId }','${item.createdDatetime}')">${item.transId}</a>
																</c:otherwise>
															</c:choose></td>
														<td><p id="xCaseNumberx${item.transId}">${item.commonCaseNumber}</p></td>
														<%-- <td>${item.docId}</td> --%>
														<td>${item.channel}</td>
														<td>${item.userEmail}</td>
														<td>${item.userMobileNumber}</td>
														<td>${item.status}</td>
														<td><p id="xstatusx${item.transId}">${item.sebStatus}</p></td>
														<td>${item.station}</td>
														<td>${item.locAddress}</td>
														<td>
															<%-- <c:if test="${not empty item.photo1Type}">
													<img id="spreview${item.transId}" src="${pageContext.request.contextPath}/admin/getImages/1/<c:out value="${item.transId}"/>.do" width="140" height="107"/>
												</c:if>
												<c:if test="${not empty item.photo2Type}">
													<img id="spreview${item.transId}" src="${pageContext.request.contextPath}/admin/getImages/2/<c:out value="${item.transId}"/>.do" width="140" height="107"/>
												</c:if>
												<c:if test="${not empty item.photo3Type}">
													<img id="spreview${item.transId}" src="${pageContext.request.contextPath}/admin/getImages/3/<c:out value="${item.transId}"/>.do" width="140" height="107"/>
												</c:if>
												<c:if test="${not empty item.photo4Type}">
													<img id="spreview${item.transId}" src="${pageContext.request.contextPath}/admin/getImages/4/<c:out value="${item.transId}"/>.do" width="140" height="107"/>
												</c:if>
												<c:if test="${not empty item.photo5Type}">
													<img id="spreview${item.transId}" src="${pageContext.request.contextPath}/admin/getImages/5/<c:out value="${item.transId}"/>.do" width="140" height="107"/>
												</c:if> --%> ${item.description}
														</td>
														<td><a data-toggle="modal" onclick="viewRemarks('${item.transId}','${item.userEmail}','${item.reportType}')">View All Remarks</a><br> <p>${item.remark}</p>
														</td>
													</tr>
												</c:forEach>
											</c:if>

										</tbody>
									</table>
								</form>
								<!-- #viewATForm -->
								<!-- detailsForm -->
								<form id="detailsForm" name="detailsForm"
									class="form-horizontal" role="form">
									<input type="hidden" name="txtRemark" id="txtRemark" value="" />
									<input type="hidden" name="txtCaseNumber" id="txtCaseNumber"
										value="" /> <input type="hidden" name="txtUserEmail"
										id="txtUserEmail" value="" /> <input type="hidden"
										name="txtTransId" id="txtTransId" value="" />
									<!-- modal show when click to edit the report details -->
									<div class="modal" id="detailsModal" tabindex="-1"
										role="dialog" aria-labelledby="myModalLabel"
										aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"
														aria-hidden="true">&times;</button>
													<h4 class="modal-title" id="myModalLabel">Report
														Details</h4>
												</div>
												<div id="reportDetail" class="modal-body">
													<div class="form-group">
														<p class="col-lg-4 control-label">Trans ID :</p>
														<div class="col-lg-6 control-label"
															style="text-align: left;">
															<label class="lblNoBoldCls" id="mTransID" name="mTransID" />
														</div>
													</div>
													<div class="form-group">
														<p class="col-lg-4 control-label">Type :</p>
														<p class="col-lg-6 control-label"
															style="text-align: left;">
															<label class="lblNoBoldCls" id="mType" name="mType" />
														</p>
													</div>
													<div class="form-group">
														<p class="col-lg-4 control-label">Login ID :</p>
														<p class="col-lg-6 control-label"
															style="text-align: left;">
															<label class="lblNoBoldCls" id="mUserEmail"
																name="mUserEmail" />
														</p>
													</div>
													<div class="form-group">
														<p class="col-lg-4 control-label">Status :</p>
														<div class="col-lg-6 control-label"
															style="text-align: left;">
															<select class="form-control" id="mselectStatus"
																name="mselectStatus"><c:forEach
																	items="${makeAReportStatus}" var="stat">
																	<option value="${stat}"
																		${item.status == stat ? 'selected' : ''}>${stat.label}</option>
																</c:forEach></select>
														</div>
													</div>
													<div class="form-group">
														<p class="col-lg-4 control-label">Case ID :</p>
														<p class="col-lg-6 control-label"
															style="text-align: left;">
															<input type="text" class="form-control" id="mCaseNumber"
																name="commonCaseNumber" />
														</p>
													</div>
													<div class="form-group">
														<p class="col-lg-4 control-label">Remark :</p>
														<div class="col-lg-6 control-label"
															style="text-align: left;">
															<textarea class="form-control" cols="23" id="mRemark"
																name="mRemark" maxlength="50" rows="5"></textarea>															
														</div>														
													</div>
													<div class="form-group">
									<input type="hidden" id="mCreatedDate"  pattern="dd/MM/yyyy hh:mm:ss a"> 				
													</div>
													 
													
												
												</div>
												<div class="modal-footer">
													<div id="submitDiv" class="btn-group">
														<button class="btn btn-primary" type="submit">Update</button>
													</div>
													<button type="button" class="btn btn-warning"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
									<!-- #modal show when click to edit the report details -->
									<!-- Confirmation YES/NO -->
									<div class="modal" id="confirmModal" tabindex="-1"
										role="dialog" aria-labelledby="myModalLabel"
										aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"
														aria-hidden="true">&times;</button>
													<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
												</div>
												<div id="confirmContent" class="modal-body"></div>
												<div class="modal-footer">
													<button class="btn btn-primary" id="confirmUpdateBtn"
														type="button">Update</button>
													<button type="button" class="btn btn-primary"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
									<!-- #Confirmation YES/NO -->
									<!-- Status Modal -->
									<div class="modal" id="statusModal" tabindex="-1" role="dialog"
										aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"
														aria-hidden="true">&times;</button>
													<h4 class="modal-title" id="myModalLabel">Make A
														Report</h4>
												</div>
												<div id=statusContent class="modal-body"></div>
												<div class="modal-footer">
													<button type="button" class="btn btn-primary"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
									<!-- #Status Modal -->
								</form>
								
								
								<!-- added by pramod --> 
								
								
								
								<form id="viewRemarksForm" name="viewRemarksForm"
									class="form-horizontal" role="form">
									 <input type="hidden"
										name="txtTransId" id="txtTransId" value="" />
									<!-- modal show when click to view Remarks -->
									<div class="modal"  id="viewRemarksModal" tabindex="-1"
										role="dialog" aria-labelledby="viewRemarks"
										aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"
														aria-hidden="true">&times;</button>
													<h4 class="modal-title" id="viewRemarks">Report
														Details</h4>
												</div>
												<div id="reportDetail" class="modal-body">
													<div class="form-group">
														<p class="col-lg-4 control-label">Trans ID :</p>
														<div class="col-lg-6 control-label"
															style="text-align: left;">
															<label class="lblNoBoldCls" id="mrTransID" name="mrTransID" />
														</div>
													</div>
													<div class="form-group">
														<p class="col-lg-4 control-label">Type :</p>
														<p class="col-lg-6 control-label"
															style="text-align: left;">
															<label class="lblNoBoldCls" id="mrType" name="mrType" />
															
														</p>
													</div>
													<div class="form-group">
														<p class="col-lg-4 control-label">Login ID :</p>
														<p class="col-lg-6 control-label"
															style="text-align: left;">
															<label class="lblNoBoldCls" id="mrLoginID" name="mrType" />
														</p>
													</div>
											
												<div class="form-group">
												
												
													
									
									<p class="col-lg-12 control-label">
									<table cellspacing="0" class="table table-striped table-bordered" id="tblRemarks"  style="width: 98%;margin-left: 6px;">
										<thead>
											<tr>
												<th>Date Update</th>
												<th>By</th>
												<th>Status</th>
												<th>Remarks</th>
											
											</tr>
										</thead>
										<tbody id="remarksBody">
							
										</tbody>
									</table>
									
									</p>
										
														
														
														
													</div>
											
												</div>
												<div class="modal-footer">
													<div id="exportDiv" class="btn-group">
														<button class="dt-button buttons-csv buttons-html5" type="button"onclick="download()">Export to CSV</button>
													</div>
													<button type="button" class="btn btn-warning"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div>
									<!-- #modal show when click to edit the report details -->
					<!-- 				Confirmation YES/NO
									<div class="modal" id="confirmModal" tabindex="-1"
										role="dialog" aria-labelledby="myModalLabel"
										aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"
														aria-hidden="true">&times;</button>
													<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
												</div>
												<div id="confirmContent" class="modal-body"></div>
												<div class="modal-footer">
													<button class="btn btn-primary" id="confirmUpdateBtn"
														type="button">Update</button>
													<button type="button" class="btn btn-primary"
														data-dismiss="modal">Close</button>
												</div>
											</div>
										</div>
									</div> -->
									
							
								</form>
								
								
								
							</div>
							<!-- #resultDiv -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
	//added by pramod
	var exportCsvData = [];
	var exportTransIdCsvData ="";
	//ended by pramod
	var preCaseNumber = null;
	var preRemark = null;
	var preStatus = null;
	
	function resetForm() {
		$('#').find('.has-error').removeClass("has-error");
		$('#searchReport').find('.has-success').removeClass("has-success");
		$('#searchReport').find('.form-control-feedback').removeClass('glyphicon-remove');
		$('#searchReport').find('.form-control-feedback').removeClass('glyphicon-ok');
		$('#searchReport').formValidation('resetForm', true);
		$('.form-group').find('small.help-block').hide();
		$('.form-group').removeClass('has-error has-feedback');
		$('.form-group').find('i.form-control-feedback').hide();
        
		setTimeout(function() {
			$('#indateTo').val(moment(new Date()).format('DD/MM/YYYY'));
			$('#indateFrom').val(moment(new Date()).format('DD/MM/YYYY'));
		}, 50);
		return false;
	}
	
//added by pramod
	
	function viewRemarks(transId,loginID,reportType){
	console.log(loginID);
	$("#mrTransID").html(transId);
	
	var transID=transId;
	$("#mrLoginID").html(loginID);
	$("#mrType").html(reportType);
    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");
    
	 $.ajax({
	        url: "${pageContext.request.contextPath}/admin/searchReportRemarksHistory.do",
	        type: "POST",
	        dataType: "json",
	        contentType: 'application/json; charset=utf-8',
	        data: JSON.stringify({
	        	id: transID
	        }),
	        beforeSend: function(xhr) {
	        	console.log(transId);
	            xhr.setRequestHeader(header, token);
	            xhr.setRequestHeader("Accept", "application/json");
	            xhr.setRequestHeader("Content-Type", "application/json");
	        },
	        success: function(data) {
	        	
	        	
	        	console.log(data);
	        	if(data.length!=0){
	        		$("#remarksBody").html("");
	        		console.log("data is their");
	        		var x="";
	        		
	        		//for  CSV download
	        			        			        		
	        		exportTransIdCsvData=transID;
	        		console.log("data is their CSV");
	        		$.each(data, function(key, value) {
	        			var remarks="";
	        			var myDate = new Date(value.updatedDatetime);
	        			
	        			value.updatedDatetime=myDate;
		        		var remarks="";
		        		if(value.remark==null){
		        			remarks="--";
		        		}else{
		        			remarks=value.remark;
		        		}
	        			
		        		value.remark=remarks
	        			exportCsvData.push(value);
	        		});
	        		console.log(exportCsvData);
	        		//for  CSV download
	        		
		        	$.each(data, function(key, value) {
		        		console.log(value);
		        		var myDate = new Date(value.updatedDatetime);
		        		var remarks="";
		        		if(value.remark==null){
		        			remarks="--";
		        		}else{
		        			remarks=value.remark;
		        		}
		        		
		        	x+='<tr><td>'+	 myDate.toLocaleString()+'</td>'+'<td>'+value.adminEmail+'</td>'+'<td>'+value.status+'</td>'+'<td>'+remarks+'</td></tr>';
		        	});
		        	 console.log(x);
		        	 $("#remarksBody").append(x);
		        	 $("#exportDiv").show();
	        	}else{
	        		$("#remarksBody").html("");
	        		$("#remarksBody").css({"box-shadow":"0 0 0 1px #e4e4e4","border-radius":"2px"});
	        		$("#remarksBody").append('<p  style="font-weight: bold;text-align: right;">'+"Previous Remarks are not available"+'</p>');
	        		
	        		$("#exportDiv").hide();
	        		console.log("data is not their");	        		
	        	}
	        	
	        	
	        	
	        },
	        failure: function(data) {
	            alert("fail");
	        }
	        });
	
	
	
    $('#viewRemarksModal').modal('show');
	}
	
	
	//export CSV data code started
	
	function convertToCSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','

            line += array[i][index];
        }

        str += line + '\r\n';
    }

    return str;
}
	function exportCSVFile(headers, items, fileTitle) {
	    if (headers) {
	        items.unshift(headers);
	    }

	    // Convert Object to JSON
	    var jsonObject = JSON.stringify(items);

	    var csv = this.convertToCSV(jsonObject);

	    var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

	    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
	    if (navigator.msSaveBlob) { // IE 10+
	        navigator.msSaveBlob(blob, exportedFilenmae);
	    } else {
	        var link = document.createElement("a");
	        if (link.download !== undefined) { // feature detection
	            // Browsers that support HTML5 download attribute
	            var url = URL.createObjectURL(blob);
	            link.setAttribute("href", url);
	            link.setAttribute("download", exportedFilenmae);
	            link.style.visibility = 'hidden';
	            document.body.appendChild(link);
	            link.click();
	            document.body.removeChild(link);
	        }
	    }
	     exportCsvData = [];
		 exportTransIdCsvData ="";
	}
	
	
	function download(){
		
		var headers = {
			      date: 'Date Update', 
			      by: "By",
			      status: "Status",
			      remarks: "Remarks"
			  };

		  var itemsFormatted = [];

		  // format the data
		  exportCsvData.forEach((item) => {
			  var myDate = new Date(item.createdDatetime);
			  var myDateFinal=getTime(myDate);
		      itemsFormatted.push({
		    	  date: myDateFinal, // remove commas to avoid errors,
		    	  by: item.adminEmail,
		    	  status: item.status,
		          remarks: item.remark
		      });
		  });

		  var fileTitle = exportTransIdCsvData; // or 'my-unique-title'
		  console.log("itemsFormatted");
console.log(itemsFormatted);
		  exportCSVFile(headers, itemsFormatted, fileTitle); // call the exportCSVFile() function to process the JSON and trigger the download
	}
	//data formate 
	
	function getTime(date) {
		
		
		var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
        var am_pm = date.getHours() >= 12 ? "PM" : "AM";
        hours = hours < 10 ? "0" + hours : hours;
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        time = hours + ":" + minutes + ":" + seconds + " " + am_pm;
		
        var finalDate = date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+time;
		return finalDate;
	}
	
	//export CSV data code ended
	
	
	function viewMyReport(transId,createdDate) {		
	//	$('#detailsForm').formValidation('resetForm', true);

	    var token = $("meta[name='_csrf']").attr("content");
	    var header = $("meta[name='_csrf_header']").attr("content");
	    $.ajax({
	        url: "${pageContext.request.contextPath}/admin/getMyReportDetails.do",
	        type: "POST",
	        dataType: "json",
	        contentType: 'application/json; charset=utf-8',
	        data: JSON.stringify({
	        	transId: transId,
	        	createdDate : createdDate
	        }),
	        beforeSend: function(xhr) {
	        	console.log(JSON.stringify({
		        	transId: transId,
		        	createdDate : createdDate
		        }));
	            xhr.setRequestHeader(header, token);
	            xhr.setRequestHeader("Accept", "application/json");
	            xhr.setRequestHeader("Content-Type", "application/json");
	        },
	        success: function(data) {
	            	//$("#selectPreferredMethod").val(data.preferredCommMethod).change();
	            	//$("#selectProfileStatus").val(data.accountStatus).change();
	            	//$( "#submitDiv" ).hide();
	            	preCaseNumber = data.commonCaseNumber;
					preRemark = data.remark;
					preStatus = data.status;
					
					$('#txtUserEmail').val(data.userEmail);
					$('#txtTransId').val(data.transId);
					
	            /* 	$("#mselectStatus").val(data.status).change();
	            	if(data.status == 'ESCALATED' || data.status == 'ASSIGNED'){
	            		$("#mselectStatus").children('option[value="NEW"]').attr('disabled', true);
	            	}else{
	            		$("#mselectStatus").children('option[value="NEW"]').attr('disabled', false);
	            	} */	            	
	            	 //-- Raj added
	            	$("#mselectStatus").val(data.sebStatus);
	            	/* if(data.status == 'ESCALATED' || data.status == 'ASSIGNED'){
	            		$("#mselectStatus").children('option[value="NEW"]').attr('disabled', true);
	            	}else{
	            		$("#mselectStatus").children('option[value="NEW"]').attr('disabled', false);
	            	} */
	            	
	            	$("#mTransID").html(data.transId);
	            	$("#mType").html(data.reportType);
	            	$("#mCaseNumber").val(data.commonCaseNumber);
	            	$("#mUserEmail").html(data.userEmail);
	            	$("#mRemark").val(data.remark);
	            	console.log(data);
	            	console.log(data.createdDatetime);	            	
	            	$("#mCreatedDate").val(data.createdDatetime);  		
	          		$('#detailsModal').modal('show');
	        },
	        failure: function(data) {
	            alert("fail");
	        },
	        complete: function(e) {
	            sebApp.hideIndicator();
	        }
	    });
	}
	
	

	
	$(document).ready(function() {
		
		//added by pramod
		var exportCsvData = [];
		var exportTransIdCsvData ="";
		//ended by pramod
		
		$("#mselectStatus").change(function(){ 
			// $('#detailsForm').formValidation('resetForm', true);
		$('#detailsForm').data('formValidation').enableFieldValidators('mRemark', false);
		//$('#detailsForm').formValidation('revalidateField', 'mRemark');
		   var value = $(this).val();
		   document.getElementById("mRemark").disabled = true;
		   $('#mCaseNumber').prop('readonly', false);
		  if (value === 'CLOSED' || value === 'RESOLVED'){
			  document.getElementById("mRemark").disabled = false;
			  $('#detailsForm').data('formValidation').enableFieldValidators('mRemark', true);
				
			  $('#mCaseNumber').prop('readonly', true);
			 // $('#detailsForm').formValidation('revalidateField', 'mRemark');
		  }
		  console.log("onchange select option");
		
		  


		});
		
		$('#selectSource').change(function() {
			var displaySource = $("#selectSource option:selected").text();
			$('#displaySource').val(displaySource);
		});
		$('#selectType').change(function() {
			var displayType = $("#selectType option:selected").text();
			$('#displayType').val(displayType);
		});
		$('#selectStatus').change(function() {
			var displayStatus = $("#selectStatus option:selected").text();
			$('#displayStatus').val(displayStatus);
		});
		if (${reportList != null && reportList.size() >= 0}) {
			$('#resultDiv').show();
			var table = $('#tblSearch').DataTable({
				//  responsive: true,
				bFilter: false,
				bSort: false,
				lengthMenu: [
					[50, 100],
					[50, 100]
				],
				dom: 'Bfrtip',
        		buttons: [
        		     {
        		    	extend: 'csvHtml5',
                   		text: 'Export To CSV',
                   		filename : 'MakeAReport_'+new Date().getTime()
        		     }     
            		
        		]
			});
			
		}
		$('#dateFrom').datetimepicker({
			format : 'DD/MM/YYYY',
			defaultDate : new Date(),
			ignoreReadonly : true
		}).on("dp.change", function(e) {
      		$('#searchReport').formValidation('revalidateField', 'indateFrom');
    	});
		
		$('#dateTo').datetimepicker({
			format : 'DD/MM/YYYY',
			defaultDate : new Date(),
			ignoreReadonly : true
		}).on("dp.change", function(e) {
  			$('#searchReport').formValidation('revalidateField', 'indateTo');
		});
		
		 	$('#detailsModal').on('show.bs.modal', function (e) {
		    	$('body').addClass('test');
			})
			$('#detailsModal').on('hidden.bs.modal', function() {
				//$('#detailsForm').formValidation('resetForm', true);
			});
		
		$('#searchReport').formValidation({
			framework: 'bootstrap',
			icon: {
				invalid: 'glyphicon glyphicon-remove',
			},
			fields: {
				indateFrom : {
					validators : {
						callback : {
							callback : function(value, validator, $field) {
								if (value == '') {
									$('#indateFrom').val(moment(new Date()).format('DD/MM/YYYY'));
									return true;
								}
								return true;
							}
						},
						date: {
           	 				format: 'DD/MM/YYYY',
              			 	max: 'indateTo',
               				message: 'The start date is not a valid'
           				}
					}
				},
				indateTo : {
					validators : {
						date: {
	       	 				format: 'DD/MM/YYYY',
	          			 	min: 'indateFrom',
	           				message: 'The end date is not a valid'
     						},
						callback : {
							callback : function(value, validator, $field) {
								if (value == '') {
									$('#indateTo').val(moment(new Date()).format('DD/MM/YYYY'));
									//console.log("value>" + $('#indateTo').val());

									return true;
								}
								return true;
							}
						},
						format : 'DD/MM/YYYY'
					}
				},
				selectSource: {
					validators: {
						callback: {
							message: 'Please Select Source',
							callback: function(value, validator, $field) {
								var options = validator.getFieldElements('selectSource').val();
								return (options != null && options.length > 0);
							}
						}
					}
				},
				selectType: {
					validators: {
						callback: {
							message: 'Please Select Type',
							callback: function(value, validator, $field) {
								var options = validator.getFieldElements('selectType').val();
								return (options != null && options.length > 0);
							}
						}
					}
				},
				selectStatus: {
					validators: {
						callback: {
							message: 'Please Select Status',
							callback: function(value, validator, $field) {
								var options = validator.getFieldElements('selectStatus').val();
								return (options != null && options.length > 0);
							}
						}
					}
				}
			}
		}).on('success.field.fv', function(e, data) {
		      if (data.field === 'indateFrom' && !data.fv.isValidField('indateTo')) {
		        // We need to revalidate the end date
		        data.fv.revalidateField('indateTo');
		    }
		
		    if (data.field === 'indateTo' && !data.fv.isValidField('indateFrom')) {
		        // We need to revalidate the start date
		        data.fv.revalidateField('indateFrom');
		    }
		});
		
		 	$('#detailsModal').on('show.bs.modal', function (e) {
		    	$('body').addClass('test');
			})
			
			$('#detailsModal').on('hidden.bs.modal', function() {
				//$('#detailsForm').formValidation('resetForm', true);
			});
		
		 	$('#detailsForm').formValidation({
		       	framework: 'bootstrap',
		     	excluded: ':disabled',
		     	 icon: {
	       	      invalid: 'glyphicon glyphicon-remove',
	       	    },
		        fields: {
		        	mRemark: {
	                    validators: {
	                        callback: {
	                            callback: function(value, validator, $field) {
	                            	console.log("validation for remark");
	                            	var options = validator.getFieldElements('mselectStatus').val();
	                            	console.log("value::"+value);
	                            	console.log("1."+(options === 'RESOLVED' || options === 'CLOSED') );
	                            	console.log("2."+(value == null || value.length < 1));
	                            	if((options === 'RESOLVED' || options === 'CLOSED') && (value == null || value.length < 1)){	                               
	                                  return{
	                                        valid: false,
	                                        message: 'Remark is requred'
	                                    }
	                                }else {
	                                	console.log("rerer");
	                                    return true;
	                                }
	                            }
	                        }
	                    }
	                },
	                mCaseNumber: {
	                    validators: {
	                        callback: {
	                            callback: function(value, validator, $field) {
	                            	var options = validator.getFieldElements('mselectStatus').val();
	                            	var text = value!=null && value ==='-' ? "" : value;
	                            	if(options !== 'NEW' && (text == '' || text.length < 1 )){	                               
	                                  return{
	                                        valid: false,
	                                        message: 'Case ID is requred'
	                                    }
	                                }else {
	                                    return true;
	                                }
	                            }
	                        }
	                    }
	                }
		        }
		    }).on('success.form.fv', function(e) {
		    	// Prevent form submission
				e.preventDefault();	
				$('#txtRemark').val($('#mRemark').val());
				$('#txtCaseNumber').val($('#mCaseNumber').val());				
				console.log("case id ::"+$('#mCaseNumber').val());
				console.log("text case id ::"+$('#txtCaseNumber').val());
		    	$('#confirmContent').html("Are you sure you want to update report for user [<b>"+$("#txtUserEmail").val()+"</b>]?");
		      	var isValidForm = $('#detailsForm').data('formValidation').isValid();
		      	if(isValidForm){
					$('#confirmModal').modal('show'); 
		      	}
		  });
		 	
		$("button#confirmUpdateBtn").click(function() {
	    	console.log("confirmUpdateBtn clicked");
	    	
	        console.log('mselectStatus:'+ $("#mselectStatus").val());
	        console.log('remark:'+ $('#txtRemark').val());
	        console.log('mCaseNumber:'+ $('#txtCaseNumber').val());
	        console.log('transId:'+$('#txtTransId').val());
	        console.log('mCreatedDate:'+$("#mCreatedDate").val());
	        var token = $("meta[name='_csrf']").attr("content");
	        var header = $("meta[name='_csrf_header']").attr("content");
	        $.ajax({
	            url: "${pageContext.request.contextPath}/admin/updateMakeAReport.do",
	            type: "POST",
	            dataType: "json",
	            data: JSON.stringify({
	            	transId : $('#txtTransId').val(),
	                remark : $("#txtRemark").val(),
	                caseNumber :  $('#txtCaseNumber').val(),
	                status :  $("#mselectStatus").val(),
	                preCaseNumber : preCaseNumber,
	                preStatus : preStatus,
	                preRemark : preRemark,
	                createdDate:$("#mCreatedDate").val()
	                
	            }),
	            beforeSend: function(xhr) {
	                xhr.setRequestHeader(header, token);
	                xhr.setRequestHeader("Accept", "application/json");
	                xhr.setRequestHeader("Content-Type", "application/json");
	                sebApp.showIndicator();
	                
	            },
	            success: function(data) {
	            	console.log(data.error);
	                if (data.error != null ) {
	                    $('#statusContent').html('<span style="color:red;">'+data.error+'<span style="color:red;">');

	                } else if (data.success != null && data.success ==='success') {
	                	$('#txtRemark').val($('#mRemark').val());
	    				$('#txtCaseNumber').val($('#mCaseNumber').val());	
	    				
	                	$('#detailsModal').modal('hide');
	                    $('#statusContent').html('Success Update Report for user [<b>'+$('#txtUserEmail').val()+'</b>]');
	                    $('#xstatusx'+data.id).html(data.status);
	                    $('#xsebStatusx'+data.id).html(data.sebStatus);
	                    $('#xremarkx'+data.id).html(data.remark);
	                    $('#xCaseNumberx'+data.id).html(data.commonCaseNumber);
	                    $('#tblSearch').DataTable().draw('full-reset');
	                }
	            },
	            failure: function(data) {
                    $('#statusContent').html('<span style="color:red;">Activation code FAILED to resend.</span>');
	            },
	            complete: function() {
	                sebApp.hideIndicator();
	                $('#statusModal').modal('show');
	                $('#confirmModal').modal('hide');
	                
	            }
	        });
	    })//end
	});
    </script>
</body>
</html>