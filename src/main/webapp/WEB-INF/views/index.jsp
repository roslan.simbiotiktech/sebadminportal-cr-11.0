<!DOCTYPE html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta http-equiv="refresh" content="0;url=mainLogin.do">
<title><spring:message code="header.title" /></title>
<script language="javascript">
    window.location.href = "mainLogin.do";
</script>
</head>
<body>
<a href="mainLogin.do"></a>
</body>
</html>
