package com.seb.admin.portal.spg.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class FPXBankListRequest extends SPGAPIGeneral {
	
	@JsonProperty("fpx_bank_id")
	private String fpx_bank_id;
	
	@JsonProperty("bank_name")
	private String bank_name;
	
	@JsonProperty("enabled")
	private Boolean enabled;

	public String getFpx_bank_id() {
		return fpx_bank_id;
	}

	public void setFpx_bank_id(String fpx_bank_id) {
		this.fpx_bank_id = fpx_bank_id;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "fpx_bank_id:[" + fpx_bank_id + "], bank_name:[" + bank_name + "], enabled:[" + enabled + "]";
	}
	
	
	
}
