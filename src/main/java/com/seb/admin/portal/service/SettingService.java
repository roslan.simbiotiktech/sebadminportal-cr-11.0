package com.seb.admin.portal.service;

import java.sql.SQLException;

import com.seb.admin.portal.model.Setting;

public interface SettingService {
	
	public Setting findByKey(String key) throws SQLException;
	public boolean updateSetting(Setting s0) throws SQLException;


}
