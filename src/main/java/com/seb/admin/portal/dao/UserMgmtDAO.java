package com.seb.admin.portal.dao;

import java.sql.SQLException;
import java.util.List;

import com.seb.admin.portal.model.AccessRight;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.PortalModule;

public interface UserMgmtDAO {
	public List<AccessRight>  findAllAccessById(int adminId) throws SQLException;
	public List<AccessRight> findMainAccessById(int adminId, String role) throws SQLException;
	public List<PortalModule> findSubModuleByModuleCode(int adminId, String parentCode, int level, String role) throws SQLException;
	public List<PortalModule> findAllModuleByRole(String role) throws SQLException;
	public PortalModule findModuleById(int moduleId) throws SQLException;
	public AdminUser saveNewAdmin(AdminUser newAdmin) throws SQLException;
	public List<Integer> findPortalModuleIdsByCode(List<String> moduleCodes) throws SQLException;
	public List<Integer> findPortalModuleIdsByCodeNRole(List<String> moduleCodes, String role) throws SQLException;
	public List<PortalModule> findSelectedModulesByCode(List<String> moduleCodes) throws SQLException;
	public List<AccessRight> findUserManagement(int loginId) throws SQLException;
	public List<AccessRight> findExistingAccessByAdminId(int adminId) throws SQLException;
	public List<PortalModule> findAllModuleByRole2(String role) throws SQLException;
	public List<PortalModule> findSCLModule(String role) throws SQLException;

	public List<AccessRight> findExistModule(AccessRight a0) throws SQLException;
	public AccessRight saveAccessRight(AccessRight acc) throws SQLException;
	public AdminUser updateAdminUser (AdminUser admin) throws SQLException;
	public AdminUser updateAdminUserAD (AdminUser admin) throws SQLException;
	public AccessRight updateAccessRight(AccessRight a0) throws SQLException;
	
	
}
