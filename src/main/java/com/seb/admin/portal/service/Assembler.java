package com.seb.admin.portal.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuthenticAdmin;

@Service("assembler")
public class Assembler {

	@Transactional
	public User buildUserFromUserEntity(AdminUser userEntity) {

		String username = userEntity.getLoginId();

		String password = userEntity.getPassword();

		boolean enabled = true;

		boolean accountNonExpired =  true;

		boolean credentialsNonExpired = true;

		boolean accountNonLocked = true;

		//User user = new User(username, password, enabled,accountNonExpired, credentialsNonExpired, accountNonLocked, getAuthorities(userEntity.getRole()));
			
		AuthenticAdmin user = new AuthenticAdmin(username, password, enabled,accountNonExpired, credentialsNonExpired, accountNonLocked, getAuthorities(userEntity.getRole()), userEntity.getId(), userEntity.getPassword(), userEntity.getRole(), userEntity.getStatus(), userEntity.getLastLoggedIn(), userEntity.getDeletedFlag());
		return user;

	}

	public Collection<? extends GrantedAuthority> getAuthorities(String role) {
		List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
		return authList;
	}

	public List<String> getRoles(String role) {
		List<String> roles = new ArrayList<String>();

		roles.add(role);
		//roles.add("ROLE_CHECKER");
		//roles.add("ROLE_MAKER");
		//roles.add("ROLE_NORMAL");

		return roles;
	}

	public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (String role : roles) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}

}