<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta content="IE=edge" http-equiv="X-UA-Compatible">
      <meta content="width=device-width, initial-scale=1" name="viewport">
      <meta name="_csrf" content="${_csrf.token}" />
	  <meta name="_csrf_header" content="${_csrf.headerName}" />
      <meta content="" name="description">
      <meta content="" name="author">
      <%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
      <title></title>
      <style type="text/css">
	.test[style] {
		padding-right: 0 !important;
	}		
	.test.modal-open {
		overflow: auto!important;
	}
	.modal {
    overflow-y: scroll;
	}
	
	.spreview {
           display: none;
           max-width: 120px;
           border: 1px solid #d5d5d5;
           margin-top: 10px;
       }
       
       .spreview img {
           max-width: 100%;
           height: auto;
       }
</style>
   </head>
   <body>
      <div id="wrapper">
         <!-- Navigation -->
         <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
         </nav>
         <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp"%>
            <div class="row">
               <div class="col-lg-12" style="width: inherit; min-width: 100%">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        Payment Gateway Maintenance &gt; FPX Bank List
                     </div>
                     <div class="panel-body">
                        <form id="fpxBankListForm" name="fpxBankListForm" method="post" action="${pageContext.request.contextPath}/admin/spg/pymtGatewayMaintenance/fpxBankList.do"
                           class="form-horizontal">
                           ${successMsg}${errorMsg}								
                           <div class="form-group">
                              <p class="col-lg-5 control-label">
                                 FPX Bank ID :
                              </p>
                              <div class="col-lg-3">
                                 <select class="form-control" id="selectFPXBankId" name="selectFPXBankId">
                                    <option value="">Please Select</option>
                                    <c:forEach items="${fpxBankIdList}" var="item">
                                       <option value="${item.bank_fpx_id}">${item.bank_fpx_id}</option>
                                    </c:forEach>
                                 </select>
                              </div>
                           </div>
                           
                           <div class="form-group">
                              <p class="col-lg-5 control-label">
                                 Bank Name :
                              </p>
                              <div class="col-lg-3">
                                 <select class="form-control" id="selectBankName" name="selectBankName">
                                    <option value="">Please Select</option>
                                    <c:forEach items="${bankNameList}" var="item">
                                       <option value="${item.bank_name}">${item.bank_name}</option>
                                    </c:forEach>
                                 </select>
                              </div>
                           </div>
                           
                           <div class="form-group">
                              <p class="col-lg-5 control-label">
                                 Bank Status :
                              </p>
                              <div class="col-lg-3">
                                 <select class="form-control" id="selectBankStatus" name="selectBankStatus">
                                    <option value="">Please Select</option>                                   
                                       <option value="true">ACTIVE</option>
                                       <option value="false">INACTIVE</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-lg-5 col-lg-offset-3 text-right">
                                 <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> <input type="hidden" id="displayType" name="displayType"
                                    value="" />
                                    <input type="hidden" name="bankStatusText" id="bankStatusText">
                                 <button class="btn btn-primary" type="submit">Search</button>
                                 <button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
                              </div>
                           </div>
                        </form>
                        <div id="resultDiv" style="display: none; margin-top: 10%">
                           <form action="${pageContext.request.contextPath}//admin/spg/pymtGatewayMaintenance/fpx?${_csrf.parameterName}=${_csrf.token}" class="form-horizontal"
                              id="viewResultForm" method="post" name="viewResultForm">
                              <h4>
                                 Search Results :FPX Bank Id :<b>${selectFPXBankId}</b>, Bank Name :<b>${selectBankName}</b>, Bank Status :<b>${bankStatusText}</b>
                              </h4>
                              <div class="form-group">
                                 <div class="alert alert-danger" id="messages" role="alert" style="display: none;"></div>
                              </div>
                              <table cellspacing="0" class="table table-striped table-bordered" id="tblSearch" style="max-width: 100%;">
                                 <thead>
                                    <tr>
                                       <th>ID</th>
                                       <th>FPX Bank ID</th>
                                       <th>Bank Name</th>
                                       <th>Bank Image</th>
                                       <th>Bank Type</th>
                                       <th>Bank Status</th>
                                    </tr>
                                 <tbody>
                                    <c:if test="${not empty totalList}">
                                       <c:forEach items="${totalList}" var="item" varStatus="status">
                                          <tr>
                                             <td><a data-toggle="modal" onclick="bankQuery(${item.bank_id})">${item.bank_id}</a></td>
                                              <td>${item.bank_fpx_id}</td>
                                              <td><span id="xxbankName${item.bank_id}">${item.bank_name}</span></td>
                                              <td><span id="xxbankImage${item.bank_id}"> <c:if test="${not empty item.logo}"><img src="data:image/png;base64,${item.logo}" width="120px" height="39px"/></c:if></span></td>                                            
                                              <td><span id="xxbankType${item.bank_id}">${item.bank_type}</span></td>  
                                             <td>                                            
                                                <c:if test="${item.enabled == false}"><span id="xx${item.bank_id}">INACTIVE</span></c:if>
                                                <c:if test="${item.enabled == true}"><span>ACTIVE</span></c:if>
                                             </td>
                                          </tr>
                                       </c:forEach>
                                    </c:if>
                                 </tbody>
                                 <!-- #viewResultForm -->
                                 <!-- #resultDiv -->
                                 </thead>
                                 <tbody></tbody>
                              </table>
                              <!-- edit modal -->
                              <div class="modal" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                 <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                       <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                          <h4 class="modal-title" id="myModalLabel">FPX Bank Details</h4>
                                       </div>
                                       <div id="paymentClientDetails" class="modal-body">
                                          <div class="form-group">
                                             <p class="col-lg-6 control-label">ID :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                                <label class="lblNoBoldCls" id="eBankId" name="eBankId" />
                                             </p>
                                          </div> 
                                          
                                          <div class="form-group">
                                             <p class="col-lg-6 control-label">FPX Bank ID :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                                <label class="lblNoBoldCls" id="eFPXBankId" name="eFPXBankId" />
                                             </p>
                                          </div> 
                                          
                                           <div class="form-group">
                                             <p class="col-lg-6 control-label">Bank Name :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                    			<input class="form-control" id="eBankName" name="eBankName" placeholder="Bank Name" type="text" maxlength="200"/>
                                             </p>
                                          </div>
                                                                                   
                                          <div class="form-group">
                                             <p class="col-lg-6 control-label">Bank Icon :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                              <div class="col-lg-4">
                                              			<div id="spreview" class="spreview"></div>
                                                        <input class="file" id="bankLogo" name="bankLogo" type="file" onchange="renderPicture(this);">
                                                        
                                                        <p class="smallnotes">
                                                            <small><em>Image size:120px X 39px (JPEG/JPG/PNG), Max 300KB</em></small>
                                                        </p>
                                                        <div id="clearDiv" style="display: none">
                                                            <button id="clear" type="button">Clear</button>
                                                        </div>
                                                        
                                             </p>
                                             </div>
                                          </div>
                                          
                                          <div class="form-group">
                                             <p class="col-lg-6 control-label">Bank Type :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                    			<label class="lblNoBoldCls" id="eBankType" name="eBankType" />
                                             </p>
                                          </div>
                                          
                                          <div class="form-group">
                                             <p class="col-lg-6 control-label">Status :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                        		<label class="lblNoBoldCls" id="eBankStatus" name="eBankStatus" />
                                             </p>
                                          </div>
                                          
                                           <div class="form-group">
                                             <p class="col-lg-6 control-label">Last Updated DateTime :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                                <label class="lblNoBoldCls" id="updateDt" name="updateDt" />
                                             </p>
                                          </div> 
                                          
                                           <div class="form-group">
                                             <p class="col-lg-6 control-label">Updated By :</p>
                                             <p class="col-lg-4 control-label" style="text-align: left;">
                                                <label class="lblNoBoldCls" id="updateBy" name="updateBy" />
                                             </p>
                                          </div> 
                                          
                                          <div class="modal-footer">
                                             <button class="btn btn-primary" id="updateBtn" type="submit" data-toggle="modal" data-target="#confirmModal1">Update</button>
                                             <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- #edit modal -->
                              <!-- Add Update Confirmation Modal -->
									<div class="modal" id="confirmModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button class="close" data-dismiss="modal" type="button">
														<span>&times;</span><span class="sr-only">Close</span>
													</button>
													<h2 class="modal-title">Confirmation</h2>
												</div>
												<div class="modal-body">
												Are you sure you want to update bank details?
												</div>
												<div class="modal-footer">
													<input id="hasPic" name="hasPic" type="hidden"/>
													<button class="btn btn-primary" id="submitBtn" type="button">Update</button>
													<button class="btn btn-warning" data-dismiss="modal" type="button">Cancel</button>
												</div>
											</div>
										</div>
									</div>
									<!-- End Add Update(Preferred Method) Confirmation Modal -->
                              <!-- modal show status after success/fail 1  -->
                                <div class="modal" id="statusModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel">Information</h4>
                                            </div>
                                            <div id="statusContent1" class="modal-body"></div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- #modal show status after success/fail 1 -->
                           </form>
                        </div>
                        <!-- /.resultList -->
                        <!-- /.resultList -->
                     </div>
                     <!-- /.panel-body -->
                  </div>
                  <!-- /.panel -->
               </div>
               <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         </div>
         <!-- /#page-wrapper -->
    </div>
    <script type="text/javascript">
    function resetForm() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#addForm').find('.has-success').removeClass("has-success");
    	$('#addForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#addForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();
    	$('#addForm').formValidation('resetForm', true);
    	$('select').val('');
    	
    	return false;
    }
    
    function renderPicture(input) {
		 $('#clearDiv').show();
		 $('#hasPic').val('1');
		var image = new Image();
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) { 
				image.src = e.target.result;
				 $('#spreview')
                .html('')
                .append($('<img/>').attr('src', e.target.result))
                .show();

			}
			reader.readAsDataURL(input.files[0]);
		}
	}
    
    var existLogo = null;
    function bankQuery(bankId) { 
    	console.log("bankId>"+bankId);
    	
	    var token = $("meta[name='_csrf']").attr("content");
	    var header = $("meta[name='_csrf_header']").attr("content");
	    $.ajax({
	        url: "${pageContext.request.contextPath}/admin/spg/pymtGatewayMaintenance/bankQuery.do",
	        type: "POST",
			dataType: "json",
	        data: JSON.stringify({
	        	bank_id: bankId
	         }),
	         beforeSend: function(xhr) {
                 xhr.setRequestHeader(header, token);
                 xhr.setRequestHeader("Accept", "application/json");
                 xhr.setRequestHeader("Content-Type", "application/json");
                 sebApp.showIndicator();
             },
	        success: function(data) {
	        	console.log(data.success);
              	if (data.success == true) {
              		var html = "";
    	        	$('#detailsModal').modal('show');  
                	$('#eBankId').html(data.objectDetail.bank_id);
                	$('#eFPXBankId').html(data.objectDetail.bank_fpx_id);                	
                	$('#eBankName').val(data.objectDetail.bank_name);
                	$('#eBankType').html(data.objectDetail.bank_type);
                	if(data.objectDetail.enabled == true){
						$('#eBankStatus').html('ACTIVE');
						
					}else{
						$('#eBankStatus').html('INACTIVE');
					}
                	$('#updateDt').html(data.objectDetail.last_updated_datetime);
                	$('#updateBy').html(data.objectDetail.last_updated_by);
                	
                	if(data.objectDetail.logo!=null){
               		 $('#spreview')
                        .html('')
                        .append($('<img/>').attr('src', 'data:image/png;base64,'+data.objectDetail.logo))
                        .show();
               		 $('#clearDiv').show();
               		 $('#hasPic').val('1');
               		existLogo = data.objectDetail.logo;
               	}else{
               		 $('#spreview').html('').hide();
                    	 $('#clearDiv').hide();
                    	 $('#hasPic').val('0');
                    	 existLogo = null;
               	}
                	
              	}else{
              		$('#statusModal1').modal('show'); 
              		$('#statusContent1').html('<span style="color:red;">'+data.message+'</span> ');
              	}
	        	
	        },
	        failure: function(data) {
	            alert("fail");
	        },
	        complete: function(e) {
	            sebApp.hideIndicator();
	        }
	    });
	}
    
    $(document).ready(function() {
   	  $('#selectBankStatus').change(function() {
   	        var selectedValue = $(this).find("option:selected").val();
   	        console.log($(this).find("option:selected").text());
   	        $('#bankStatusText').val($(this).find("option:selected").text());
   	  })
    	    
   	  
   	  $('#clear').on("click", function(){
     	   document.getElementById('bankLogo').value = "";
     	  $('#viewResultForm').formValidation('revalidateField', 'bankLogo');
     	   if(existLogo!=null){
     		  $('#spreview')
              .html('')
              .append($('<img/>').attr('src', 'data:image/png;base64,'+existLogo))
              .show();
     		 $('#clearDiv').show();
     		 $('#hasPic').val('1'); 
     	   }else{
     			$('#spreview').html('').hide();
         	 	$('#clearDiv').hide();
         	 	$('#hasPic').val('0');
     	   }
        });
    	
   	  
   	if (${totalList != null && totalList.size() >= 0}) {
		$('#resultDiv').show();
		var table = $('#tblSearch').DataTable({
			//  responsive: true,   
			bFilter: false,
			bSort: false,
			"autoWidth": false,
			lengthMenu: [
				[50, 100],
				[50, 100]
			]
		});
	}
    	
    	
  //[start]update profile
    $('#viewResultForm').formValidation({
       	framework: 'bootstrap',
     	excluded: ':disabled',
     	icon : {
			invalid : 'glyphicon glyphicon-remove',
		},
        fields: {        	
        	eBankName: {
                validators: {
                    notEmpty: {
                        message: 'Bank Name is required'
                    } 
                }
            },
            bankLogo : {
				validators : {					
					file : {
						extension : 'jpg,jpeg,png',
						type : 'image/jpg,image/jpeg,image/png',
						maxSize : 307200, // 2048 * 1024
						message : "Image must less than 300KB"
					},
					promise : {
						promise : function(value, validator, $field) {
							var dfd = new $.Deferred(), files = $field.get(0).files;

							if (!files.length || typeof FileReader === 'undefined') {
								dfd.resolve({
									valid : true
								});
								return dfd.promise();
							}

							var img = new Image();
							img.onload = function() {
								var w = this.width, h = this.height;
								dfd.resolve({
									valid : (w <= 120 && h <= 39),
									message : 'Image size must not more than 120px X 39px',
									source : img.src,
									width : w,
									height : h
								});
							};
							img.onerror = function() {
								dfd.reject({
									message : 'Please upload a valid image'
								});
							};

							var reader = new FileReader();
							reader.readAsDataURL(files[0]);
							reader.onloadend = function(e) {
								img.src = e.target.result;
							};

							return dfd.promise();
						}
					}
				}
			}
        }
    }).on('err.validator.fv', function(e, data) {
          
        }).on('success.form.fv', function(e,data) {
        	
 /*        	 if (data.field === 'bankLogo' && data.validator === 'promise' && data.result.source) {
	                $('#spreview')
	                    .html('')
	                    .append($('<img/>').attr('src', data.result.source))
	                    .show();
	              $('#clearDiv').show();
	            }
        	  */
        	 
        // Prevent form submission
		e.preventDefault();
      	var isValidForm = $('#viewResultForm').data('formValidation').isValid();
      	console.log("isValidForm>"+isValidForm);
      	if(isValidForm){
     		 $("button#submitBtn").unbind('click');			
     		 //ajax submit update
            $("button#submitBtn").click(function() {
            	$('#detailsModal').modal('show');  
           
            	console.log($('#eBankId').html());
            	console.log($('#eBankName').val());            	
            	
        			var token = $("meta[name='_csrf']").attr("content");
        			var header = $("meta[name='_csrf_header']").attr("content");
        			
        		var formData = new FormData();
             	var data = JSON.stringify({
             		bank_id :$('#eBankId').html(),
             		bank_name :$('#eBankName').val(),
               	  	hasPic : $("#hasPic").val()                    
                 });
             	
             	var imageFile = $('#bankLogo').val(); 
                   if(imageFile!='') 
                   { 
                   	formData.append('bankLogo', $('input[type=file]')[0].files[0]);
                   } 
              		
                   formData.append('data', new Blob([data], {
                   	type : "application/json"  // ** specify that this is JSON**
                    })); 
                   
        			$.ajax({
        				url: "${pageContext.request.contextPath}/admin/spg/pymtGatewayMaintenance/doUpdateFPXBank.do",
        				 type: "POST",
                         data: formData,
                         processData: false,
                         contentType: false,
                         cache: false,
        				beforeSend: function(xhr) {
        					xhr.setRequestHeader(header, token);
        					sebApp.showIndicator();
        				},
        				success: function(data) {
        					$('#statusModal1').modal('show'); 
        					if (data.success == true) {        					
            					console.log("success update.");
        						$('#statusContent1').html(data.message);
        						$('#xxbankName'+data.objectDetail.id).html(data.objectDetail.bank_name);
        						
        						if(data.objectDetail.logo!=null){
        							$('#xxbankImage'+data.objectDetail.bank_id).html('<img src="data:image/png;base64,'+data.objectDetail.logo+'" width="120px" height="39px"/>');
        						}
        						
        						
        					}else{
        	              		$('#statusContent1').html('<span style="color:red;">'+data.message+'</span> ');
        					}
        									
        				},
        				failure: function(data) {
        					alert("Fail To Connect");
        				},
        				complete: function() {
        					sebApp.hideIndicator();
        					$('#detailsModal').modal('hide');
        		  			$('#confirmModal1').modal('hide');
        				}
        			});
        		})
            //end ajax submit update profile
      	}
  	});
    //[end] update profile
    	
    	
    })
    
    
  
    
   
  
    </script>
</body>
</html>