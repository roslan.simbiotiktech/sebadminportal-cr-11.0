package com.seb.admin.portal.spg.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FPXBankQueryRequest extends SPGAPIGeneral {
	
	@JsonProperty("bank_id")
	private int bank_id;

	public int getBank_id() {
		return bank_id;
	}

	public void setBank_id(int bank_id) {
		this.bank_id = bank_id;
	}

	@Override
	public String toString() {
		return "bank_id:[" + bank_id + "]";
	}

	
}
