<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<div class="row" style="margin-left: 0.2%">
        <p class="page-header" style="font-weight: bold">Welcome <em><font color="#317CB6">${userSession.loginId}</font></em>, your last login was at <em><font color="#317CB6"><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${userSession.lastLoggedIn}" pattern="dd/MM/YYYY hh:mm:ss a"/></font></em></p>
    </div>