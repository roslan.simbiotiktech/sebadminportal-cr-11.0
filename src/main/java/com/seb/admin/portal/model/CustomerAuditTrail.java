package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;


/**
 * The persistent class for the CUSTOMER_AUDIT_TRAIL database table.
 * 
 */
@Entity
@Table(name="CUSTOMER_AUDIT_TRAIL", schema = ApplicationConstant.SCHEMA)@NamedQueries({
	@NamedQuery(name="CustomerAuditTrail.findAll", query="SELECT c FROM CustomerAuditTrail c"),
	@NamedQuery(name="CustomerAuditTrail.findAllCustAuditReport", query="SELECT c FROM CustomerAuditTrail c WHERE c.auditDatetime >=:dateFrom AND c.auditDatetime <=:dateTo AND c.userId LIKE :email ORDER BY c.auditId desc")
})
public class CustomerAuditTrail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="AUDIT_ID", unique=true, nullable=false)
	private int auditId;

	@Column(name="ACTIVITY", length=500)
	private String activity;

	@Column(name="AUDIT_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp auditDatetime;

	@Column(name="SUCCESS", nullable=false)
	private int success;

	@Column(name="USER_ID", nullable=false, length=50)
	private String userId;

	public CustomerAuditTrail() {
	}

	public int getAuditId() {
		return this.auditId;
	}

	public void setAuditId(int auditId) {
		this.auditId = auditId;
	}

	public String getActivity() {
		return this.activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Timestamp getAuditDatetime() {
		return this.auditDatetime;
	}

	public void setAuditDatetime(Timestamp auditDatetime) {
		this.auditDatetime = auditDatetime;
	}

	public int getSuccess() {
		return this.success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}