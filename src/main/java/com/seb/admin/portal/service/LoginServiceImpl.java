package com.seb.admin.portal.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.seb.admin.portal.dao.LoginDAO;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuthenticAdmin;

@Service
public class LoginServiceImpl implements LoginService{

	@Autowired
	private LoginDAO loginDAO;

	@Override
	public List<String> findAllLoginID() throws SQLException {
		return loginDAO.findAllLoginID();
	}
	
	@Override
	@Transactional
	public AdminUser getUserDetails(String loginId) {

		return loginDAO.getUserDetails(loginId);
	}
	
	@Override
	public AdminUser findExactAdminUser(int id) throws SQLException{
		return loginDAO.findExactAdminUser(id);
	}

	
	@Override
	@Transactional
	public AdminUser getSelectedUser(String loginId) throws SQLException {

		return loginDAO.getSelectedUser(loginId);
	}

	@Override
	@Transactional
	public boolean updateLastLoggedIn(AdminUser user) {
		return loginDAO.updateLastLoggedIn(user);
	}

	@Override
	public List<AdminUser> findAllUser() throws SQLException {
		return loginDAO.findAllUser();
	}
	
	@Override
	public List<AdminUser> findActiveUser() throws SQLException {
		return loginDAO.findActiveUser();
	}
	
	

	@Override
	public AdminUser checkExistUser(String loginId) {
		return loginDAO.checkExistUser(loginId);
	}

	
}