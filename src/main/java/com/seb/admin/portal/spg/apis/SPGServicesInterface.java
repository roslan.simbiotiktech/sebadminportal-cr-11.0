package com.seb.admin.portal.spg.apis;

import com.seb.admin.portal.spg.entity.SPGAPIGeneral;
import com.seb.admin.portal.spg.request.FPXBankListRequest;
import com.seb.admin.portal.spg.request.FPXBankQueryRequest;
import com.seb.admin.portal.spg.request.GatewayQueryFPXRequest;
import com.seb.admin.portal.spg.request.GatewayQueryMBBRequest;
import com.seb.admin.portal.spg.request.MerchantCreateRequest;
import com.seb.admin.portal.spg.request.MerchantQueryRequest;
import com.seb.admin.portal.spg.request.OFpxBankUpdateRequest;
import com.seb.admin.portal.spg.request.PaymentDetailReportRequest;
import com.seb.admin.portal.spg.request.SPGPaymentReportRequest;
import com.seb.admin.portal.spg.response.MerchantResponse;
import com.seb.admin.portal.spg.response.PaymentDetailReportResponse;
import com.seb.admin.portal.spg.response.PaymentReportExportResponse;
import com.seb.admin.portal.spg.response.QueryFPXBankResponse;
import com.seb.admin.portal.spg.response.SPGPaymentReportResponse;
import com.seb.admin.portal.spg.response.EditMerchantResponse;
import com.seb.admin.portal.spg.response.FPXBankListResponse;
import com.seb.admin.portal.spg.response.GatewayFPXGenerateCSRResponse;
import com.seb.admin.portal.spg.response.GatewayFPXResponse;
import com.seb.admin.portal.spg.response.GatewayMBBResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SPGServicesInterface 
{
	@GET("config/deno/list")
	Call<?> getDenomination();

	@POST("merchant_create/{en}")
	Call<MerchantResponse> createMerchant(@Body MerchantCreateRequest payload,  @Path("en") String language);
	
	@POST("merchant_list/{en}")
	Call<EditMerchantResponse> listMerchant(@Body SPGAPIGeneral payload,  @Path("en") String language);
	
	@POST("merchant_query/{en}")
	Call<MerchantResponse> queryMerchant(@Body MerchantQueryRequest payload,  @Path("en") String language);
	
	@POST("merchant_update/{en}")
	Call<MerchantResponse> updateMerchant(@Body MerchantCreateRequest payload,  @Path("en") String language);

	//payment gateway maintenance
	@POST("gateway_mbb_query/{en}")
	Call<GatewayMBBResponse> queryGatewayMBB (@Body SPGAPIGeneral payload,  @Path("en") String language);
	
	@POST("gateway_mbb_update/{en}")
	Call<GatewayMBBResponse> updateGatewayMBB (@Body GatewayQueryMBBRequest payload,  @Path("en") String language);
	
	@POST("gateway_fpx_query/{en}")
	Call<GatewayFPXResponse> queryGatewayFPX (@Body SPGAPIGeneral payload,  @Path("en") String language);
	
	@POST("gateway_fpx_update/{en}")
	Call<GatewayFPXResponse> updateGatewayFPX (@Body GatewayQueryFPXRequest payload,  @Path("en") String language);
	
	@POST("fpx_bank_list/{en}")
	Call<FPXBankListResponse> fpxBankList (@Body FPXBankListRequest payload,  @Path("en") String language);
	
	@POST("fpx_bank_query/{en}")
	Call<QueryFPXBankResponse> queryBank(@Body FPXBankQueryRequest payload,  @Path("en") String language);
	
	@POST("fpx_bank_update/{en}")
	Call<QueryFPXBankResponse> updateFPXBank (@Body OFpxBankUpdateRequest payload,  @Path("en") String language);
	
	@POST("payment_report/{en}")
	Call<SPGPaymentReportResponse> getPaymentReport (@Body SPGPaymentReportRequest payload,  @Path("en") String language);
	
	@POST("gateway_fpx_generate_csr/{en}")
	Call<GatewayFPXGenerateCSRResponse> generateCSR (@Body SPGAPIGeneral payload,  @Path("en") String language);
	
	@POST("payment_detail_report/{en}")
	Call<PaymentDetailReportResponse> getPaymentDetailReport (@Body PaymentDetailReportRequest payload,  @Path("en") String language);
	
	@POST("payment_report_export/{en}")
	Call<PaymentReportExportResponse> exportPaymentReport (@Body SPGPaymentReportRequest payload,  @Path("en") String language);

}
