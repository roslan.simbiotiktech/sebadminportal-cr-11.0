<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<meta content="" name="description">
<meta content="${_csrf.token}" name="_csrf">
<meta content="${_csrf.headerName}" name="_csrf_header">
      <!DOCTYPE html>

      <html lang="en">

      <head>
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <%@ include file="../menuTitle.jsp" %>
          <%@ include file="../global.jsp" %>
      </head>

      <body>
        <div id="wrapper">
          <!-- Navigation -->

          <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <%@ include file="../menuTop.jsp" %>
              <%@ include file="../menuLeft.jsp" %>
          </nav>

          <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp" %>

              <div class="row">
                <div class="col-lg-12" style="width: inherit; min-width: 100%">
                  <div class="panel panel-default">
                    <div class="panel-heading" style="font-weight: bold">Report &gt; Bill Payment Report</div>

                    <div class="panel-body">
                      <form action="${pageContext.request.contextPath}/admin/billPaymentReport.do" class="form-horizontal" id="reportForm" method="post" name="reportForm">
                        ${successMsg}${errorMsg}
                        <div class="form-group">
                          <p class="col-lg-5 control-label">
                            Date From <span style="color: red;">*</span> :
                          </p>
                          <div class="col-lg-3">
                            <div class='input-group date' id='dateFrom'>
                              <input class="form-control datepicker" id="indateFrom" name="indateFrom" readonly type='text'> <span class="input-group-addon"><span
class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <p class="col-lg-5 control-label">
                            Date To <span style="color: red;">*</span> :
                          </p>
                          <div class="col-lg-3">
                            <div class='input-group date' id='dateTo'>
                              <input class="form-control datepicker" id="indateTo" name="indateTo" readonly type='text'> <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <p class="col-lg-5 control-label">
                            Payment Method <span style="color: red;">*</span> :
                          </p>
                          <div class="col-lg-3">
                            <select class="form-control capital" id="selectPymtMethod" name="selectPymtMethod">
								<option value="">Please Select</option>
								<option value="ALL">All</option>
								<option value="MPG">Debit/Credit Card</option>
								<option value="FPX">FPX</option>
							</select>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <p class="col-lg-5 control-label">
                            SEB Reference Status <span style="color: red;">*</span> :
                          </p>
                          <div class="col-lg-3">
                            <select class="form-control capital" id="selectSebRefStatus" name="selectSebRefStatus">
								<option value="">Please Select</option>
								<option value="ALL">All</option>
								<option value="BILLED">BILLED</option>
								<option value="PENAUTH">B2B-SUBMITTED FOR PROCESSING</option>
								<option value="FAILED">FAILED</option>
								<option value="PENDING">PENDING</option>
								<option value="RECEIVED">RECEIVED</option>
							</select>
                          </div>
                        </div>

                        <div class="form-group">
                          <p class="col-lg-5 control-label">Contract Account Number :</p>

                          <div class="col-lg-3">
                            <input class="form-control" id="contractAccNo" name="contractAccNo" placeholder="Contract Account Number" type="text" maxlength="12">
                          </div>
                        </div>


                        <div class="form-group">
                          <p class="col-lg-5 control-label">Login Id/Email :</p>

                          <div class="col-lg-3">
                            <input class="form-control" id="email" maxlength="50" name="email" placeholder="Login Id/Email" type="text">
                          </div>
                        </div>

                        <div class="form-group">
                          <p class="col-lg-5 control-label">Payment Reference :</p>

                          <div class="col-lg-3">
                            <input class="form-control" id="pymtRef" maxlength="10" name="pymtRef" placeholder="Payment Reference" type="text">
                          </div>
                        </div>

                        <div class="form-group">
                          <p class="col-lg-5 control-label">FPX Transaction ID :</p>
                         
                          <div class="col-lg-3">
                            <input class="form-control" id="fpxTxnId" maxlength="40" name="fpxTxnId" placeholder="FPX Transaction ID" type="text">
                          </div>
                        </div>

                        <div class="form-group">
                          <p class="col-lg-5 control-label">Maybank Reference Number :</p>

                          <div class="col-lg-3">
                            <input class="form-control" id="mbbRefNo" maxlength="100" name="mbbRefNo" placeholder="Maybank Reference Number" type="text">
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-lg-5 col-lg-offset-3 text-right">
                            <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
                            <input type="hidden" id="displayPymtMethod" name="displayPymtMethod" />
                             <input type="hidden" id="displaySebRefStatus" name="displaySebRefStatus" />
                            <button class="btn btn-primary" id="submitBtn" type="submit">Search</button>
                            <button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
                          </div>
                        </div>
                      </form>
                      <div id="resultDiv" style="display: none; margin-top: 10%">
                        <form action="${pageContext.request.contextPath}/admin/billPaymentReport.do?${_csrf.parameterName}=${_csrf.token}" class="form-horizontal" id="viewReportForm" method="post" name="viewReportForm">
                          <h4>
							Search Results : Date From :<b>${dateFrom}</b>, Date To :<b>${dateTo}</b>, Payment Method :<b>${displayPymtMethod}</b>, Contract Account Number :<b>${contractAccNo}</b>, Login Id/Email :<b>${email}</b>, Payment Reference :<b>${pymtRef}</b>, FPX Transaction ID :<b>${fpxTxnId}</b>
							</h4>
                          <div class="form-group">
                            <div class="alert alert-danger" id="messages" role="alert" style="display: none;"></div>
                          </div>
                          <table cellspacing="0" class="table table-striped table-bordered" id="tblSearch">
                            <thead>
                              <tr>
                                <th>No.</th>
                                <th>Created Date</th>
                                <th>Contract Account No</th>
                                <th>Login ID/Email</th>
                                <th>Payment Method</th>
                                <th>Payment Amount(RM)</th>
                                <th>Payment Reference</th>
                                <th>Payment Date</th>
                                <th>Notification Email</th>                                
                                <th>SPG Gateway Status</th>
                                <th>SEB Reference Status</th>
                                <th>FPX Transaction ID</th>
                                <th>FPX Transaction Status</th>
                                <th>Maybank Reference No</th>
                                <th>Maybank Status</th>
                              </tr>
                              <!-- #resultDiv -->
                            </thead>
                            <tbody>
                              <c:if test="${not empty reportList}">
                                <c:forEach items="${reportList}" var="item" varStatus="status">
                                  <tr>
                                    <td>${status.index+1}</td>
                                    <td>
                                      <fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.createdDatetime}" pattern="dd/MM/YYYY hh:mm:ss a" />
                                    </td>
                                    <td>${item.contractAccountNumber}</td>
                                    <td>${item.userEmail}</td>
                                    <c:choose>
	                                    <c:when test="${item.spgBankName!=null && item.spgBankName!=''}">
	                                    	<td>${item.spgBankName}</td>
	                                    </c:when>
	                                    <c:otherwise>
	                                    	<td>${item.spgBankType}</td>
	                                    </c:otherwise>
                                    </c:choose>
                                    <td>${item.amount}</td>
                                    <td>${item.userPaymentId}</td>
                                    <td>
                                      <fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.paymentDatetime}" pattern="dd/MM/YYYY hh:mm:ss a" />
                                    </td>
                                     <td>${item.notificationEmail}</td>                                    
                                    <td>
                                    	<c:if test="${item.gatewayId =='SPG'}">${item.gatewayStatus}</c:if>
                                    	<c:if test="${item.gatewayId !='SPG'}"> </c:if>
                                  </td>
                                  <td>${item.status}</td>
                                    <c:choose>
                                      <c:when test="${fn:containsIgnoreCase(item.referencePrefix, 'F') && item.gatewayId =='SPG'}">
                                        <td> ${item.gatewayRef}</td>
                                        <td><a data-toggle="modal" onclick="viewBankResponse('${item.referencePrefix}','${item.externalGatewayStatus}')">${item.externalGatewayStatus}</a></td>
                                        <td></td>
                                        <td></td>
                                      </c:when>
                                       <c:when test="${fn:containsIgnoreCase(item.referencePrefix, 'M') && item.gatewayId =='SPG'}">
                                        <td></td>
                                        <td></td>
                                        <td> ${item.gatewayRef}</td>
                                        <td><a data-toggle="modal" onclick="viewBankResponse('${item.referencePrefix}','${item.externalGatewayStatus}')">${item.externalGatewayStatus}</a></td>
                                      </c:when>
                                      
                                      <c:when test="${fn:containsIgnoreCase(item.referencePrefix, 'F') && item.gatewayId !='SPG'}">
                                        <td> ${item.gatewayRef}</td>
                                        <td><a data-toggle="modal" onclick="viewBankResponse('${item.referencePrefix}','${item.gatewayStatus}')">${item.gatewayStatus}</a></td>
                                        <td></td>
                                        <td></td>
                                      </c:when>
                                       <c:when test="${fn:containsIgnoreCase(item.referencePrefix, 'M') && item.gatewayId !='SPG'}">
                                        <td></td>
                                        <td></td>
                                        <td> ${item.gatewayRef}</td>
                                        <td><a data-toggle="modal" onclick="viewBankResponse('${item.referencePrefix}','${item.gatewayStatus}')">${item.gatewayStatus}</a></td>
                                      </c:when>
                                    </c:choose>
                                  </tr>
                                </c:forEach>
                              </c:if>
                            </tbody>
                          </table>
                          <!-- Add Confirmation Modal -->
							<div class="modal" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button class="close" data-dismiss="modal" type="button">
												<span>&times;</span><span class="sr-only">Close</span>
											</button>
											<h2 class="modal-title">Response</h2>
										</div>
										<div class="modal-body" id="content"><div class="form-group"><p class="col-lg-6 control-label">Response Code :</p><div class="col-lg-4 control-label" style="text-align: left;"><label class ="lblNoBoldCls" id="mResponseCode"  name="mResponseCode"/></div></div><div class="form-group"><p class="col-lg-6 control-label">Response Description :</p><div class="col-lg-4 control-label" style="text-align: left;"><label class ="lblNoBoldCls" id="mResponseDescription"  name="mResponseDescription"/></div></div></div>
										<div class="modal-footer">
											<button class="btn btn-warning" data-dismiss="modal" type="button">Cancel</button>
										</div>
									</div>
								</div>
							</div>
						<!-- End Confirmation Modal -->
                        </form>
                        <!-- #viewReportForm -->
                      </div>
                      <!-- #resultDiv -->
                    </div>
                    <!-- /.panel-body -->
                  </div>
                  <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
              </div>
              <!-- /.row -->
          </div>
          <!-- /#page-wrapper -->
        </div>
        <script type="text/javascript">
        function resetForm() {
        	  $('#').find('.has-error').removeClass("has-error");
        	  $('#reportForm').find('.has-success').removeClass("has-success");
        	  $('#reportForm').find('.form-control-feedback').removeClass('glyphicon-remove');
        	  $('#reportForm').find('.form-control-feedback').removeClass('glyphicon-ok');
        	  $('#reportForm').formValidation('resetForm', true);
        	  $('.form-group').find('small.help-block').hide();
        	  $('.form-group').removeClass('has-error has-feedback');
        	  $('.form-group').find('i.form-control-feedback').hide();
        	  setTimeout(function() {
        	    $('#indateTo').val(moment(new Date()).format('DD/MM/YYYY'));
        	    $('#indateFrom').val(moment(new Date()).format('DD/MM/YYYY'));
        	  }, 50);
        	  return false;
        	}

        	function formatDate(dateInput) {
        	  if (dateInput != null && dateInput != '' && dateInput != '-') {
        	    var jsDate = new Date(dateInput);
        	    var dateTimeString = moment(jsDate).format('DD/MM/YYYY hh:mm:ss A');
        	    return dateTimeString;
        	  }
        	  return '';

        	}
        	
        	function viewBankResponse(referencePrefix, status) {
        		var gatewayId = 'FPX';
        		
        		if(referencePrefix == 'F')
        			 gatewayId = 'FPX'
        			 
        		else
        			gatewayId = 'MPG';
        		
        		
        	    var token = $("meta[name='_csrf']").attr("content");
        	    var header = $("meta[name='_csrf_header']").attr("content");
        	    $.ajax({
        	        url: "${pageContext.request.contextPath}/report/getBankResponse.do",
        	        type: "POST",
        	        dataType: "json",
        	        contentType: 'application/json; charset=utf-8',
        	        data: JSON.stringify({
        	            merchantId: gatewayId,
        	            responseCode: status
        	        }),
        	        beforeSend: function(xhr) {
        	            xhr.setRequestHeader(header, token);
        	            xhr.setRequestHeader("Accept", "application/json");
        	            xhr.setRequestHeader("Content-Type", "application/json");
        	        },
        	        success: function(data) {
        	        	var html = "";
        	        	if(data!=null){
        	        		 $.each(data, function(index, curr) {
      		                	//console.log(currEmp.id+","+currEmp.station);
             	        		 $("#mResponseCode").html(curr.responseCode);
             	        		 $("#mResponseDescription").html(curr.responseDescription);
      		                });
        	        	}else{
        	        		 $("#mResponseCode").html(status);
         	        		 $("#mResponseDescription").html('');
        	        	}
        	        	
						$('#confirmModal').modal('show');
        	        },
        	        failure: function(data) {
        	            alert("fail");
        	        },
        	        complete: function(e) {
        	            sebApp.hideIndicator();
        	        }
        	    });
        	}
        	

        	$(document).ready(function() {
        	  if(${reportList != null && reportList.size() >= 0}) {
        	    $('#resultDiv').show();
        	    var table = $('#tblSearch').DataTable({
        	      //  responsive: true,   
        	      bFilter: false,
        	      bSort: false,
        	      lengthMenu: [
        	        [50],
        	        [50]
        	      ],
        	      dom: 'Bfrtip',
        	      buttons: [{
        	        extend: 'csvHtml5',
        	        text: 'Export To CSV',
        	        filename: 'BillPaymentReport_' + new Date().getTime(),
        	        exportOptions: {
        	          columns: ':not(:first-child)',
        	        }
        	      }],

        	    });
        	  }

        	  $('#dateFrom').datetimepicker({
        	    format: 'DD/MM/YYYY',
        	    defaultDate: new Date(),
        	    ignoreReadonly: true
        	  }).on("dp.change", function(e) {
        	    $('#reportForm').formValidation('revalidateField', 'indateFrom');
        	  });

        	  $('#dateTo').datetimepicker({
        	    format: 'DD/MM/YYYY',
        	    defaultDate: new Date(),
        	    ignoreReadonly: true
        	  }).on("dp.change", function(e) {
        	    $('#reportForm').formValidation('revalidateField', 'indateTo');
        	  });
        	  $('#selectPymtMethod').change(function() {
        	    var displayPymtMethod = $("#selectPymtMethod option:selected").text();
        	    $('#displayPymtMethod').val(displayPymtMethod);
        	  });
        	  $("#contractAccNo").keypress(function(e) {
        	    //if the letter is not digit then display error and don't type anything
        	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        	      return false;
        	    }
        	  });

        	  $("#fpxTxnId").keypress(function(e) {
        	    //if the letter is not digit then display error and don't type anything
        	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        	      return false;
        	    }
        	  });
        	  $("#mbbRefNo").keypress(function(e) {
        	    //if the letter is not digit then display error and don't type anything
        	    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        	      return false;
        	    }
        	  });
        	  $('#reportForm').formValidation({
        	    framework: 'bootstrap',
        	    icon: {
        	      invalid: 'glyphicon glyphicon-remove',
        	    },
        	    fields: {
        	      indateFrom: {
        	        validators: {
        	          callback: {
        	            callback: function(value, validator, $field) {
        	              if (value == '') {
        	                $('#indateFrom').val(moment(new Date()).format('DD/MM/YYYY'));
        	                return true;
        	              }
        	              return true;
        	            }
        	          },
        	          date: {
        	            format: 'DD/MM/YYYY',
        	            max: 'indateTo',
        	            message: 'The start date is not a valid'
        	          }
        	        }
        	      },
        	      indateTo: {
        	        validators: {
        	          date: {
        	            format: 'DD/MM/YYYY',
        	            min: 'indateFrom',
        	            message: 'The end date is not a valid'
        	          },
        	          callback: {
        	            callback: function(value, validator, $field) {
        	              if (value == '') {
        	                $('#indateTo').val(moment(new Date()).format('DD/MM/YYYY'));
        	                //console.log("value>" + $('#indateTo').val());

        	                return true;
        	              }
        	              return true;
        	            }
        	          },
        	          format: 'DD/MM/YYYY'
        	        }
        	      },
        	      selectSebRefStatus: {
          	        validators: {
          	          callback: {
          	            message: 'Please Select SEB Reference Status',
          	            callback: function(value, validator, $field) {
          	              var options = validator.getFieldElements('selectSebRefStatus').val();
          	              return (options != null && options.length > 0);
          	            }
          	          }
          	        }
          	      },
          	      selectPymtMethod: {
        	        validators: {
        	          callback: {
        	            message: 'Please Select Pyment Method',
        	            callback: function(value, validator, $field) {
        	              var options = validator.getFieldElements('selectPymtMethod').val();
        	              return (options != null && options.length > 0);
        	            }
        	          }
        	        }
        	      },
        	    }
        	  }).on('success.field.fv', function(e, data) {
        	    if (data.field === 'indateFrom' && !data.fv.isValidField('indateTo')) {
        	      // We need to revalidate the end date
        	      data.fv.revalidateField('indateTo');
        	    }

        	    if (data.field === 'indateTo' && !data.fv.isValidField('indateFrom')) {
        	      // We need to revalidate the start date
        	      data.fv.revalidateField('indateFrom');
        	    }
        	  });
        	});
        </script>
	</body>
</html>