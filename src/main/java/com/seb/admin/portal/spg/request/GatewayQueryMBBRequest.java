package com.seb.admin.portal.spg.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OMbb;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;

public class GatewayQueryMBBRequest extends SPGAPIGeneral {
	
	@JsonProperty("mbb")
	private OMbb mbb;

	public OMbb getMbb() {
		return mbb;
	}

	public void setMbb(OMbb mbb) {
		this.mbb = mbb;
	}

	@Override
	public String toString() {
		return "mbb:[" + mbb + "]";
	}

	
	

}
