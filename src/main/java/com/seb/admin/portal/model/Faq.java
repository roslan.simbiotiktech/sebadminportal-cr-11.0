package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;


/**
 * The persistent class for the FAQ database table.
 * 
 */
@Entity
@Table(name="FAQ" , schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="Faq.findAll", query="SELECT f FROM Faq f WHERE f.deletedFlag=0"),
	@NamedQuery(name="Faq.findWithOrderByQuestEn", query="SELECT f FROM Faq f WHERE f.faqCategoryId = :catId AND UPPER(f.status)=:status AND f.deletedFlag=0 ORDER BY f.questionEn ASC"),
	@NamedQuery(name="Faq.findActiveQuesEnOrderBySequence", query="SELECT f FROM Faq f WHERE f.faqCategoryId = :catId AND UPPER(f.status)='ACTIVE' AND f.deletedFlag=0 ORDER BY f.faqSequence ASC,f.id ASC"),
	@NamedQuery(name="Faq.findNextSeq", query="select count(f.id)+1 from Faq f where f.faqCategoryId = :catId and UPPER(f.status)='ACTIVE' and f.deletedFlag=0"),

})
public class Faq implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Lob
	@Column(name="ANSWER_BM")
	private String answerBm;

	@Lob
	@Column(name="ANSWER_CN")
	private String answerCn;

	@Lob
	@Column(name="ANSWER_EN")
	private String answerEn;

	@Column(name="CREATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(name="FAQ_CATEGORY_ID", nullable=false)
	private int faqCategoryId;

	@Column(name="FAQ_SEQUENCE", nullable=false)
	private int faqSequence;

	@Column(name="QUESTION_BM", length=310)
	private String questionBm;

	@Lob
	@Column(name="QUESTION_CN")
	private String questionCn;

	@Column(name="QUESTION_EN", length=310)
	private String questionEn;

	@Column(name="STATUS", nullable=false, length=10)
	private String status;

	@Column(name="UPDATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp updatedDatetime;
	
	@JoinColumn(name = "FAQ_CATEGORY_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.EAGER )
	private FaqCategory  faqCategory;

	

	public Faq() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAnswerBm() {
		return this.answerBm;
	}

	public void setAnswerBm(String answerBm) {
		this.answerBm = answerBm;
	}

	public String getAnswerCn() {
		return this.answerCn;
	}

	public void setAnswerCn(String answerCn) {
		this.answerCn = answerCn;
	}

	public String getAnswerEn() {
		return this.answerEn;
	}

	public void setAnswerEn(String answerEn) {
		this.answerEn = answerEn;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public int getFaqCategoryId() {
		return this.faqCategoryId;
	}

	public void setFaqCategoryId(int faqCategoryId) {
		this.faqCategoryId = faqCategoryId;
	}

	public int getFaqSequence() {
		return this.faqSequence;
	}

	public void setFaqSequence(int faqSequence) {
		this.faqSequence = faqSequence;
	}

	public String getQuestionBm() {
		return this.questionBm;
	}

	public void setQuestionBm(String questionBm) {
		this.questionBm = questionBm;
	}

	public String getQuestionCn() {
		return this.questionCn;
	}

	public void setQuestionCn(String questionCn) {
		this.questionCn = questionCn;
	}

	public String getQuestionEn() {
		return this.questionEn;
	}

	public void setQuestionEn(String questionEn) {
		this.questionEn = questionEn;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	@JsonBackReference //for parent table
	public FaqCategory getFaqCategory() {
		return faqCategory;
	}


	public void setFaqCategory(FaqCategory faqCategory) {
		this.faqCategory = faqCategory;
	}
}