package com.seb.admin.portal.adapter;

public class Contract {
	private String accountNumber;
	private String accountName;
	private String subscriptionStatus;
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getSubscriptionStatus() {
		return subscriptionStatus;
	}
	public void setSubscriptionStatus(String subscriptionStatus) {
		this.subscriptionStatus = subscriptionStatus;
	}
	@Override
	public String toString() {
		return "Contract [accountNumber=" + accountNumber + ", accountName=" + accountName + ", subscriptionStatus="
				+ subscriptionStatus + "]";
	}
	
	
	
}
