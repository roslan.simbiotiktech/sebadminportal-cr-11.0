package com.seb.admin.portal.spg.response;

import java.util.List;

public class JSONResponse<T> {

	private boolean success;
	private int statusCode;
	private String message;
	private T additonalInfo;
	private Object objectDetail;
	private List list;
	
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public T getAdditonalInfo() {
		return additonalInfo;
	}
	public void setAdditonalInfo(T additonalInfo) {
		this.additonalInfo = additonalInfo;
	}
	public Object getObjectDetail() {
		return objectDetail;
	}
	public void setObjectDetail(Object objectDetail) {
		this.objectDetail = objectDetail;
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}
	
	
	
}
