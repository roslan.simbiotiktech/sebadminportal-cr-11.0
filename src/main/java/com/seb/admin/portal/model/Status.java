package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;


/**
 * The persistent class for the "STATUS" database table.
 * 
 */
@Entity
@Table(name="STATUS", schema = ApplicationConstant.SCHEMA_SPG)
@NamedQuery(name="Status.findAll", query="SELECT s FROM Status s")
public class Status implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="STATUS_ID")
	private long statusId;

	@Column(name="CREATED_DATETIME")
	private Timestamp createdDatetime;

	private String direction;

	@Column(name="PAYMENT_ID")
	private long paymentId;

	private String remark;

	@Column(name="STATUS")
	private String status;

	public Status() {
	}

	public long getStatusId() {
		return this.statusId;
	}

	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getDirection() {
		return this.direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public long getPaymentId() {
		return this.paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}