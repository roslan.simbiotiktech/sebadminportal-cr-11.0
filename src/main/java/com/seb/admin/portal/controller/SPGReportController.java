package com.seb.admin.portal.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.model.BankResponse;
import com.seb.admin.portal.model.DataTables;
import com.seb.admin.portal.service.ReportService;
import com.seb.admin.portal.spg.apis.SPGAPIServices;
import com.seb.admin.portal.spg.entity.OFile;
import com.seb.admin.portal.spg.entity.OFileEncode;
import com.seb.admin.portal.spg.entity.OMerchantBase;
import com.seb.admin.portal.spg.entity.OPaymentDetailReport;
import com.seb.admin.portal.spg.entity.OPaymentReport;
import com.seb.admin.portal.spg.entity.PAGING;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;
import com.seb.admin.portal.spg.request.MerchantCreateRequest;
import com.seb.admin.portal.spg.request.MerchantQueryRequest;
import com.seb.admin.portal.spg.request.PaymentDetailReportRequest;
import com.seb.admin.portal.spg.request.SPGPaymentReportRequest;
import com.seb.admin.portal.spg.response.EditMerchantResponse;
import com.seb.admin.portal.spg.response.GatewayFPXGenerateCSRResponse;
import com.seb.admin.portal.spg.response.JSONResponse;
import com.seb.admin.portal.spg.response.MerchantResponse;
import com.seb.admin.portal.spg.response.PaymentDetailReportResponse;
import com.seb.admin.portal.spg.response.PaymentReportExportResponse;
import com.seb.admin.portal.spg.response.SPGPaymentReportResponse;
import com.seb.admin.portal.util.StringUtil;

import retrofit2.Response;

@Controller
@SessionAttributes( {"clientList"})
public class SPGReportController {

	private static final Logger log = Logger.getLogger(ReportController.class);

	@Autowired
	private ReportService reportService;

	@Autowired
	private SPGAPIServices SPGAPIServices;


	/**============================================
	 * SPG Transaction Report
	 *============================================*/
	@RequestMapping(value = "/admin/spg/transactionReport")
	public ModelAndView doSPGTransactionReport(){
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();
		Response<EditMerchantResponse> response;
		try {
			response = SPGAPIServices.executeListMerchant(new MerchantCreateRequest(), StringUtil.getLanguage(""));
			log.info("[doEditPaymentClientMaintenance]"+response.raw().request().url().toString());

			if(response.isSuccessful() && response.body().getResponseStatus().isSuccess()) {
				List<OMerchantBase> merchantList = response.body().getMerchant();
				model.addObject("clientList", merchantList.stream().sorted(Comparator.comparing(OMerchantBase::getName)).collect(Collectors.toList()));
			}
			else {
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", StringUtil.trimToEmpty(response.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(response.body().getResponseStatus().getMessages())));

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		model.setViewName("/spg-maintenance/spgTransactionReport");
		return model;
	}

	@RequestMapping(value = "/admin/spg/transactionReport", method = RequestMethod.POST)
	public ModelAndView doSearchSPGTransactionReport(
			@RequestParam(value="indateFrom", defaultValue = "") String dateFrom,
			@RequestParam(value="indateTo", defaultValue = "") String dateTo,
			@RequestParam(value="selectPymtMethod", defaultValue = "") String selectPymtMethod,
			@RequestParam(value="displayPymtMethod", defaultValue = "") String displayPymtMethod,
			@RequestParam(value="selectSebRefStatus", defaultValue = "") String selectSebRefStatus,
			@RequestParam(value="displaySebRefStatus", defaultValue = "") String displaySebRefStatus,
			@RequestParam(value="selectClientName", defaultValue = "") String selectClientName,		
			@RequestParam(value="displayClientName", defaultValue = "") String displayClientName,			
			@RequestParam(value="pymtRef", defaultValue = "") String pymtRef,
			@RequestParam(value="fpxTxnId", defaultValue = "") String fpxTxnId,
			@RequestParam(value="mbbRefNo", defaultValue = "") String mbbRefNo,
			HttpServletRequest request, HttpServletResponse resp){

		ModelAndView model = new ModelAndView();

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		int totalRecords=0;
		int maxPage=0;


		try
		{				
			for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
				String fieldName = (String) e.nextElement();
				String fieldValue = request.getParameter(fieldName);
				if ((fieldValue != null) && (fieldValue.length() > 0)) {
					System.out.println(fieldName + ":" + fieldValue);

				}
			}	

			SPGPaymentReportRequest rr = new SPGPaymentReportRequest();			

			if(!selectPymtMethod.equalsIgnoreCase("ALL")) {
				rr.setPayment_gateway(selectPymtMethod);
			}
			if(!(fpxTxnId.equalsIgnoreCase("") && mbbRefNo.equalsIgnoreCase(""))) {
				rr.setPayment_gateway_reference_id((!fpxTxnId.equalsIgnoreCase("")? fpxTxnId : mbbRefNo));
			}			

			if(!selectSebRefStatus.equalsIgnoreCase("ALL")) {
				rr.setSpg_reference_status(selectSebRefStatus);
			}

			if(!selectClientName.equalsIgnoreCase("")) {
				rr.setMerchant_id(Integer.parseInt(selectClientName));
			}

			if(!pymtRef.equalsIgnoreCase("")) {
				rr.setMerchant_reference_id(pymtRef);
			}

			rr.setPaging(new PAGING(1));

			Response<SPGPaymentReportResponse> pymtReportResponse = SPGAPIServices.executeGetPaymentReport(rr, StringUtil.getLanguage(""));

			if(pymtReportResponse.isSuccessful() && pymtReportResponse.body().getResponseStatus().isSuccess()) {
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>"+pymtReportResponse.body().getOPaymentReport().size());
				totalRecords = pymtReportResponse.body().getPagination().getTotal_records();
				maxPage = pymtReportResponse.body().getPagination().getMax_page();

				model.addObject("paymentReportList", pymtReportResponse.body().getOPaymentReport());

			}else {
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", StringUtil.trimToEmpty(pymtReportResponse.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(pymtReportResponse.body().getResponseStatus().getMessages())));
				reportService.auditLog("SPG Transaction Report", new AuditTrail(user.getId(),0,"","SPG Transaction Report: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+pymtReportResponse.raw().request().url().toString()+"\n"+StringUtil.trimToEmpty(pymtReportResponse.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(pymtReportResponse.body().getResponseStatus().getMessages())));		
			}
		}catch(Exception ex){
			ex.printStackTrace();
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search SPG Transaction Report."));
			reportService.auditLog("SPG Transaction Report", new AuditTrail(user.getId(),0,"","SPG Transaction Report: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
			log.error(ex.getMessage());
			ex.printStackTrace();

		}finally {

		}


		model.addObject("dateFrom", dateFrom);
		model.addObject("dateTo", dateTo);
		model.addObject("selectPymtMethod", selectPymtMethod);
		model.addObject("displayPymtMethod", displayPymtMethod);
		model.addObject("selectSebRefStatus", selectSebRefStatus);
		model.addObject("displaySebRefStatus", displaySebRefStatus);
		model.addObject("selectClientName", selectClientName);
		model.addObject("displayClientName", displayClientName);
		model.addObject("pymtRef", pymtRef);
		model.addObject("fpxTxnId", fpxTxnId);
		model.addObject("mbbRefNo", mbbRefNo);


		model.setViewName("/spg-maintenance/spgTransactionReport");

		return model;
	}

	@RequestMapping(value = "/admin/spg/transactionReport/searchTableSPGTransactionReportList", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody
	String searchTableSPGTransactionReportList(HttpServletRequest req,
			@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength,
			@RequestParam String sEcho,			
			@RequestParam(value="dateFrom", defaultValue = "") String dateFrom,
			@RequestParam(value="dateTo", defaultValue = "") String dateTo,
			@RequestParam(value="selectPymtMethod", defaultValue = "") String selectPymtMethod,
			@RequestParam(value="displayPymtMethod", defaultValue = "") String displayPymtMethod,
			@RequestParam(value="selectSebRefStatus", defaultValue = "") String selectSebRefStatus,
			@RequestParam(value="displaySebRefStatus", defaultValue = "") String displaySebRefStatus,
			@RequestParam(value="selectClientName", defaultValue = "") String selectClientName,		
			@RequestParam(value="displayClientName", defaultValue = "") String displayClientName,			
			@RequestParam(value="pymtRef", defaultValue = "") String pymtRef,
			@RequestParam(value="fpxTxnId", defaultValue = "") String fpxTxnId,
			@RequestParam(value="mbbRefNo", defaultValue = "") String mbbRefNo

			) {

		log.info("=====searchTableSPGTransactionReportList===========");
		Enumeration params = req.getParameterNames(); 
		while(params.hasMoreElements()){
			String paramName = (String)params.nextElement();
			log.info(paramName+" : "+req.getParameter(paramName));
		}
		log.info("=====searchTableSPGTransactionReportList===========");


		DataTables<OPaymentReport> dt = new DataTables <OPaymentReport> ();

		SPGPaymentReportRequest rr = new SPGPaymentReportRequest();			

		if(!selectPymtMethod.equalsIgnoreCase("ALL")) {
			rr.setPayment_gateway(selectPymtMethod);
		}
		if(!(fpxTxnId.equalsIgnoreCase("") && mbbRefNo.equalsIgnoreCase(""))) {
			rr.setPayment_gateway_reference_id((!fpxTxnId.equalsIgnoreCase("")? fpxTxnId : mbbRefNo));
		}			

		if(!selectSebRefStatus.equalsIgnoreCase("ALL")) {
			rr.setSpg_reference_status(selectSebRefStatus);
		}

		if(!selectClientName.equalsIgnoreCase("")) {
			rr.setMerchant_id(Integer.parseInt(selectClientName));
		}

		if(!pymtRef.equalsIgnoreCase("")) {
			rr.setMerchant_reference_id(pymtRef);
		}

		try {
			int page = (iDisplayStart + iDisplayLength)/iDisplayLength;
			log.info("searchTableSPGTransactionReportList page>"+page);
			rr.setPaging(new PAGING(page));

			//datatables
			Response<SPGPaymentReportResponse> pymtReportResponse = SPGAPIServices.executeGetPaymentReport(rr, StringUtil.getLanguage(""));

			if(pymtReportResponse.isSuccessful() && pymtReportResponse.body().getResponseStatus().isSuccess()) {
				log.info("total record::"+ pymtReportResponse.body().getPagination().getTotal_records());
				dt.setAaData(pymtReportResponse.body().getOPaymentReport());  // this is the dataset reponse to client
				dt.setiTotalDisplayRecords(pymtReportResponse.body().getPagination().getTotal_records());  // // the total data in db for datatables to calculate page no. and position
				dt.setiTotalRecords(pymtReportResponse.body().getPagination().getTotal_records());   // the total data in db for datatables to calculate page no.
				dt.setsEcho(sEcho);
				dt.setiDisplayLength(1);
			}
		} catch (Exception e) {
			log.error(this.getClass().getSimpleName(), e);

		}

		return toJson(dt);
	} 

	@RequestMapping(value = "/admin/spg/transactionReport/getPaymentDetail",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody <T> JSONResponse<?> getPaymentDetail(@RequestBody PaymentDetailReportRequest form, HttpServletRequest req, HttpServletResponse resp) {	
		log.info("***********************************************************************************************");
		log.info("[getPaymentDetail]");
		log.info("form >>"+form.toString());

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		JSONResponse<?> jsonResponse = new JSONResponse<>();
		List<OPaymentDetailReport> detailList = new ArrayList<OPaymentDetailReport>();

		try {
			PaymentDetailReportRequest pdr = new PaymentDetailReportRequest();
			pdr.setPayment_id(form.getPayment_id());

			Response<PaymentDetailReportResponse> pymtDetailResp = SPGAPIServices.executeGetPaymentDetailReport(pdr, StringUtil.getLanguage(""));
			jsonResponse.setSuccess(true);

			if(pymtDetailResp.isSuccessful() && pymtDetailResp.body().getResponseStatus().isSuccess()) {
				detailList = pymtDetailResp.body().getPaymentDetailReport();
				jsonResponse.setList(detailList);
				
				for(OPaymentDetailReport a : detailList) {
					System.out.println(a.getStatus());
				}
				
				
				//ObjectMapper mapper = new ObjectMapper();
				//OPaymentDetailReport a = mapper.convertValue(pymtDetailResp.body().getPaymentDetailReport(), OPaymentDetailReport.class);
				//jsonResponse.setObjectDetail(a);

			}else {
				jsonResponse.setSuccess(false);
				jsonResponse.setStatusCode(-1);
				jsonResponse.setMessage(Arrays.toString(pymtDetailResp.body().getResponseStatus().getMessages()));
				reportService.auditLog("SPG Transaction Report", new AuditTrail(user.getId(),0,"","SPG Transaction Report: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+pymtDetailResp.raw().request().url().toString()+"\n"+StringUtil.trimToEmpty(pymtDetailResp.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(pymtDetailResp.body().getResponseStatus().getMessages())));		

			}

		} catch (Exception e) {
			reportService.auditLog("SPG Transaction Report", new AuditTrail(user.getId(),0,"","SPG Transaction Report: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+e.getMessage()));		
			log.error(this.getClass().getSimpleName(), e);
		}

		return jsonResponse;
	}
	
	@RequestMapping(value = "/admin/spg/transactionReport/exportPaymentReport", method = RequestMethod.GET)
	public @ResponseBody void exportPaymentReport(HttpServletResponse resp,
			HttpServletRequest req,
			@RequestParam(value="indateFrom", defaultValue = "") String dateFrom,
			@RequestParam(value="indateTo", defaultValue = "") String dateTo,
			@RequestParam(value="selectPymtMethod", defaultValue = "") String selectPymtMethod,
			@RequestParam(value="displayPymtMethod", defaultValue = "") String displayPymtMethod,
			@RequestParam(value="selectSebRefStatus", defaultValue = "") String selectSebRefStatus,
			@RequestParam(value="displaySebRefStatus", defaultValue = "") String displaySebRefStatus,
			@RequestParam(value="selectClientName", defaultValue = "") String selectClientName,		
			@RequestParam(value="displayClientName", defaultValue = "") String displayClientName,			
			@RequestParam(value="pymtRef", defaultValue = "") String pymtRef,
			@RequestParam(value="fpxTxnId", defaultValue = "") String fpxTxnId,
			@RequestParam(value="mbbRefNo", defaultValue = "") String mbbRefNo) {
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		Enumeration params = req.getParameterNames(); 
		while(params.hasMoreElements()){
			String paramName = (String)params.nextElement();
			log.info(paramName+" : "+req.getParameter(paramName));
		}
		
		
		String downloadFileName= "csv";
		String downloadStringContent= "";

		SPGPaymentReportRequest rr = new SPGPaymentReportRequest();			

		if(!selectPymtMethod.equalsIgnoreCase("ALL")) {
			rr.setPayment_gateway(selectPymtMethod);
		}
		if(!(fpxTxnId.equalsIgnoreCase("") && mbbRefNo.equalsIgnoreCase(""))) {
			rr.setPayment_gateway_reference_id((!fpxTxnId.equalsIgnoreCase("")? fpxTxnId : mbbRefNo));
		}			

		if(!selectSebRefStatus.equalsIgnoreCase("ALL")) {
			rr.setSpg_reference_status(selectSebRefStatus);
		}

		if(!selectClientName.equalsIgnoreCase("")) {
			rr.setMerchant_id(Integer.parseInt(selectClientName));
		}

		if(!pymtRef.equalsIgnoreCase("")) {
			rr.setMerchant_reference_id(pymtRef);
		}

		Response<PaymentReportExportResponse> exportPaymentReportResponse = null;

		try {

			exportPaymentReportResponse = SPGAPIServices.executeExportPaymentReport(rr, StringUtil.getLanguage(""));

			log.info("[exportPaymentReport]"+exportPaymentReportResponse.raw().request().url().toString());

			if(exportPaymentReportResponse.isSuccessful() && exportPaymentReportResponse.body().getResponseStatus().isSuccess()) {
				OFileEncode file = exportPaymentReportResponse.body().getOfileEncode();
				downloadFileName = file.getName();
				 byte[] decodedBytes = Base64.getDecoder().decode(file.getContent());
				downloadStringContent = new String(decodedBytes, "utf-8");
				System.out.println(downloadStringContent);
			}
			else {
				log.info(StringUtil.trimToEmpty(exportPaymentReportResponse.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(exportPaymentReportResponse.body().getResponseStatus().getMessages()));
			}

			OutputStream out = resp.getOutputStream();
			resp.setContentType("text/csv; charset=utf-8");
			resp.addHeader("Content-Disposition","attachment; filename=\"" + downloadFileName + "\"");
			out.write(downloadStringContent.getBytes(Charset.forName("UTF-8")));
			out.flush();
			out.close();

		} catch (IOException e) {
			log.error("exportPaymentReport",e);
		}

	}
	
	@RequestMapping(value = "/admin/spg/getBankResponse",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody List<BankResponse> getBankResponse(@RequestBody BankResponse bankResp)
			throws IOException {
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		System.out.println("[getBankResponse]");
		List<BankResponse> resp = new ArrayList<BankResponse>() ;	
		try {
			System.out.println("request::"+bankResp.toString());
			resp = reportService.findByMerchantidAndResponsecode(bankResp.getMerchantId(), bankResp.getResponseCode());
			for (BankResponse b : resp){
				//System.out.println("merchant id :: "+b.getMerchantId()+", status code ::"+b.getResponseCode()+", response desc :: "+b.getResponseDescription());		
			}

		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return resp;
	}

	private String toJson(DataTables dt){
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(dt);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}


}
