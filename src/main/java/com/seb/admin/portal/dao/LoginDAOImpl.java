package com.seb.admin.portal.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuthenticAdmin;
import com.seb.admin.portal.model.CustServiceCounter;

@Repository
public class LoginDAOImpl implements LoginDAO {

	@PersistenceContext(unitName = ApplicationConstant.UNIT_NAME, type = PersistenceContextType.EXTENDED)
	private EntityManager em;

	@Override
	public List<String> findAllLoginID() throws SQLException {
		List<String> loginIDs = em.createNamedQuery("AdminUser.findAllLoginID",String.class).getResultList();
		return loginIDs;
	}
	
	@Override
	public AdminUser findExactAdminUser(int id) throws SQLException{
		return em.find(AdminUser.class, id);		
	}

	public AdminUser getUserDetails(String loginId) {	

		AdminUser results = new AdminUser();
		try{
			Query query =  (Query) em.createNamedQuery("AdminUser.getLoginUser").setParameter("loginId", loginId);

			//query.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
			//query.setHint("javax.persistence.cache.retrieveMode", CacheRetrieveMode.BYPASS);
			results = (AdminUser) query.getSingleResult();		
			em.refresh(results);
		}catch(NoResultException ex){
			results = null;}


		return results;
	}

	public AdminUser getSelectedUser(String loginId) throws SQLException  {	

		AdminUser results = new AdminUser();
		try{
			Query query =  (Query) em.createNamedQuery("AdminUser.getSelectedUser").setParameter("loginId", loginId);

			results = (AdminUser) query.getSingleResult();		
			em.refresh(results);
		}catch(NoResultException ex){
			results = null;}


		return results;
	}



	public boolean updateLastLoggedIn(AdminUser user){
		try{	
			AdminUser update = em.find(AdminUser.class, user.getId());
			update.setLastLoggedIn(new Date());
			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();
			return false;
		}	
		return true;
	}

	@Override
	public List<AdminUser> findAllUser() throws SQLException {
		List<AdminUser> userList = new ArrayList<AdminUser>(); 
		userList = em.createNamedQuery("AdminUser.findAllUser",AdminUser.class).getResultList();
		return userList;
	}

	@Override
	public List<AdminUser> findActiveUser() throws SQLException {
		List<AdminUser> userList = new ArrayList<AdminUser>(); 
		userList = em.createNamedQuery("AdminUser.findActiveUser",AdminUser.class).getResultList();
		return userList;
	}

	public AdminUser checkExistUser(String loginId) {	
		AdminUser results = null;
		try{
			Query query =  (Query) em.createNamedQuery("AdminUser.checkExistUser").setParameter("loginId", loginId);
			results = (AdminUser) query.getSingleResult();		
			em.refresh(results);
		}catch(NoResultException ex){
			results = null;}
		return results;
	}


}
