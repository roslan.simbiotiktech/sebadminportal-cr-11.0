package com.seb.admin.portal.model;

import java.io.Serializable;
import java.util.List;

public class LeftMenu  implements Serializable {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String accessUrl;
	private String category;
	private String imageContent;
	private int level;	
	private String moduleCode;
	private String name;
	private String parentModuleCode;	
	private String role;
	
	private boolean hasChildLevel2;
	private boolean hasChildLevel3;
	private List<SubMenu> level2Menu;
	private List<SubMenu> level3Menu;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccessUrl() {
		return accessUrl;
	}
	public void setAccessUrl(String accessUrl) {
		this.accessUrl = accessUrl;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getImageContent() {
		return imageContent;
	}
	public void setImageContent(String imageContent) {
		this.imageContent = imageContent;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getModuleCode() {
		return moduleCode;
	}
	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParentModuleCode() {
		return parentModuleCode;
	}
	public void setParentModuleCode(String parentModuleCode) {
		this.parentModuleCode = parentModuleCode;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public boolean isHasChildLevel2() {
		return hasChildLevel2;
	}
	public void setHasChildLevel2(boolean hasChildLevel2) {
		this.hasChildLevel2 = hasChildLevel2;
	}
	public boolean isHasChildLevel3() {
		return hasChildLevel3;
	}
	public void setHasChildLevel3(boolean hasChildLevel3) {
		this.hasChildLevel3 = hasChildLevel3;
	}
	public List<SubMenu> getLevel2Menu() {
		return level2Menu;
	}
	public void setLevel2Menu(List<SubMenu> level2Menu) {
		this.level2Menu = level2Menu;
	}
	public List<SubMenu> getLevel3Menu() {
		return level3Menu;
	}
	public void setLevel3Menu(List<SubMenu> level3Menu) {
		this.level3Menu = level3Menu;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accessUrl == null) ? 0 : accessUrl.hashCode());
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + (hasChildLevel2 ? 1231 : 1237);
		result = prime * result + (hasChildLevel3 ? 1231 : 1237);
		result = prime * result + id;
		result = prime * result + ((imageContent == null) ? 0 : imageContent.hashCode());
		result = prime * result + level;
		result = prime * result + ((level2Menu == null) ? 0 : level2Menu.hashCode());
		result = prime * result + ((level3Menu == null) ? 0 : level3Menu.hashCode());
		result = prime * result + ((moduleCode == null) ? 0 : moduleCode.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((parentModuleCode == null) ? 0 : parentModuleCode.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LeftMenu other = (LeftMenu) obj;
		if (accessUrl == null) {
			if (other.accessUrl != null)
				return false;
		} else if (!accessUrl.equals(other.accessUrl))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (hasChildLevel2 != other.hasChildLevel2)
			return false;
		if (hasChildLevel3 != other.hasChildLevel3)
			return false;
		if (id != other.id)
			return false;
		if (imageContent == null) {
			if (other.imageContent != null)
				return false;
		} else if (!imageContent.equals(other.imageContent))
			return false;
		if (level != other.level)
			return false;
		if (level2Menu == null) {
			if (other.level2Menu != null)
				return false;
		} else if (!level2Menu.equals(other.level2Menu))
			return false;
		if (level3Menu == null) {
			if (other.level3Menu != null)
				return false;
		} else if (!level3Menu.equals(other.level3Menu))
			return false;
		if (moduleCode == null) {
			if (other.moduleCode != null)
				return false;
		} else if (!moduleCode.equals(other.moduleCode))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parentModuleCode == null) {
			if (other.parentModuleCode != null)
				return false;
		} else if (!parentModuleCode.equals(other.parentModuleCode))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		return true;
	}
	
	
	
	
}

	