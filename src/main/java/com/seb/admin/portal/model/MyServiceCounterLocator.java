package com.seb.admin.portal.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class MyServiceCounterLocator implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int id;

	private String addressLine1;

	private String addressLine2;

	private String addressLine3;

	private int adminUserId;

	private String category;

	private int approverId;

	private String city;

	private Timestamp createdDatetime;

	private int custServiceLocationId;

	private int deletedFlag;

	private double latitude;

	private double longitude;

	private String openingHour;

	private String postcode;

	private String rejectReason;

	private String remark;

	private String status ="P";

	private Timestamp updatedDatetime;
	
	private int custLocationId;

	private String region;

	private String station;
	
	private boolean isMainTable;
	
	private CustServiceLocation custServLoc;
	
	private int editedId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public int getAdminUserId() {
		return adminUserId;
	}

	public void setAdminUserId(int adminUserId) {
		this.adminUserId = adminUserId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getApproverId() {
		return approverId;
	}

	public void setApproverId(int approverId) {
		this.approverId = approverId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Timestamp getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getCustServiceLocationId() {
		return custServiceLocationId;
	}

	public void setCustServiceLocationId(int custServiceLocationId) {
		this.custServiceLocationId = custServiceLocationId;
	}

	public int getDeletedFlag() {
		return deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getOpeningHour() {
		return openingHour;
	}

	public void setOpeningHour(String openingHour) {
		this.openingHour = openingHour;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public int getCustLocationId() {
		return custLocationId;
	}

	public void setCustLocationId(int custLocationId) {
		this.custLocationId = custLocationId;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public boolean isMainTable() {
		return isMainTable;
	}

	public void setMainTable(boolean isMainTable) {
		this.isMainTable = isMainTable;
	}

	public CustServiceLocation getCustServLoc() {
		return custServLoc;
	}

	public void setCustServLoc(CustServiceLocation custServLoc) {
		this.custServLoc = custServLoc;
	}

	public int getEditedId() {
		return editedId;
	}

	public void setEditedId(int editedId) {
		this.editedId = editedId;
	}

	
	
	

}
