package com.seb.admin.portal.service;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.seb.admin.portal.dao.MaintenanceDAO;
import com.seb.admin.portal.model.ContactUs;
import com.seb.admin.portal.model.CustServiceCounter;
import com.seb.admin.portal.model.CustServiceCounterEdited;
import com.seb.admin.portal.model.CustServiceLocation;
import com.seb.admin.portal.model.Faq;
import com.seb.admin.portal.model.FaqCategory;
import com.seb.admin.portal.model.News;
import com.seb.admin.portal.model.PowerAlert;
import com.seb.admin.portal.model.PowerAlertType;
import com.seb.admin.portal.model.TagNotification;
import com.seb.admin.portal.model.TariffCategory;
import com.seb.admin.portal.model.TariffSetting;

@Service
public class MaintenanceServiceImpl implements MaintenanceService {

	@Autowired
	private MaintenanceDAO maintenanceDAO;

	@Override
	public List<String> findAllRegions() throws SQLException{	
		return maintenanceDAO.findAllRegions();
	}

	@Override
	public List <CustServiceLocation> findAllStation(String region) throws SQLException{
		return maintenanceDAO.findAllStation(region);
	}

	@Override
	public List <CustServiceLocation> findAllLocation() throws SQLException{
		return maintenanceDAO.findAllLocation();
	}

	@Override
	public List<CustServiceCounter> findCounterByStatus(String status, String category)throws SQLException{
		return maintenanceDAO.findCounterByStatus(status, category);
	}
	
	@Override
	public List<CustServiceCounter> findMainLocatorByStatus(String status1, String status2)throws SQLException{
		return maintenanceDAO.findMainLocatorByStatus(status1, status2);
	}
	
	@Override
	public List<CustServiceCounterEdited> findSubLocatorByStatus (String status,int counterId)throws SQLException{
		return maintenanceDAO.findSubLocatorByStatus(status, counterId);
	}

	@Override
	public List<CustServiceLocation> findAll() throws SQLException {
		// TODO Auto-generated method stub
		return maintenanceDAO.findAll();
	}

	@Override
	public CustServiceLocation findCustServiceLocationById(int id) {
		// TODO Auto-generated method stub
		return maintenanceDAO.findCustServiceLocationById(id);
	}

	@Override
	public List<CustServiceLocation> findAllStation() throws SQLException {
		return maintenanceDAO.findAllStation();
	}

	@Override
	public List<CustServiceCounter> findAllCounterByLocId(int id) throws SQLException {
		// TODO Auto-generated method stub
		return maintenanceDAO.findAllCounterByLocId(id);
	}

	@Override
	public CustServiceCounter findCustServiceCounterById(int id) throws SQLException {
		return maintenanceDAO.findCustServiceCounterById(id);
	}
	
	@Override
	public CustServiceCounterEdited findCustServiceCounterEditedById(int id) throws SQLException {
		return maintenanceDAO.findCustServiceCounterEditedById(id);
	}

	@Override
	@Transactional
	public void saveCSC(CustServiceCounter c0) throws SQLException {
		maintenanceDAO.saveCSC(c0);	
	}
	
	@Override
	@Transactional
	public void saveEditedCSC(CustServiceCounterEdited c0) throws SQLException {
		maintenanceDAO.saveEditedCSC(c0);	
	}
	
	@Override
	@Transactional
	public boolean updateEditedMakerCSC(CustServiceCounterEdited e0) throws SQLException{
		return maintenanceDAO.updateEditedMakerCSC(e0);
	}
	
	@Override
	@Transactional
	public boolean deleteEditedCSC(int id) throws SQLException {
		return maintenanceDAO.deleteEditedCSC(id);	
	}

	@Override
	@Transactional
	public boolean updateCSCByMaker(CustServiceCounter c0) {
		// TODO Auto-generated method stub
		return maintenanceDAO.updateCSCByMaker(c0);
	}
	
	@Override
	@Transactional
	public boolean updateAprovedEditedCSCByChecker(CustServiceCounter c0) throws SQLException {
		return maintenanceDAO.updateAprovedEditedCSCByChecker(c0);
	}
	
	@Override
	@Transactional
	public boolean updateCSCCustomByMaker(CustServiceCounter c0)throws SQLException {
		// TODO Auto-generated method stub
		return maintenanceDAO.updateCSCCustomByMaker(c0);
	}
	

	@Override
	@Transactional
	public boolean updateCSCByChecker(CustServiceCounter c0) throws SQLException {
		return maintenanceDAO.updateCSCByChecker(c0);
	}

	/** Part: News**/
	@Override
	@Transactional
	public News saveNews(News news) throws SQLException {
		return maintenanceDAO.saveNews(news);		
	}
	
	@Override
	@Transactional
	public News updateNews(News n0) throws SQLException {
		return maintenanceDAO.updateNews(n0);
	}


	@Override
	public List<News> findNews(Date dateFrom, Date dateTo, String title) throws SQLException {
		return maintenanceDAO.findNews(dateFrom, dateTo, title);
	}

	@Override
	public News findNewsById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return maintenanceDAO.findNewsById(id);
	}

	@Override
	public List<News> findNewsTitleByEndDate() throws SQLException {
		return maintenanceDAO.findNewsTitleByEndDate();
	}

	@Override
	public List<News> findNewsIds(List<Integer> ids) throws SQLException {
		return maintenanceDAO.findNewsIds(ids);
	}

	/**Part-Outage Alert**/
	@Override
	public List<PowerAlertType> findPowerAlertType() throws SQLException {
		// TODO Auto-generated method stub
		return maintenanceDAO.findPowerAlertType();
	}

	@Override
	public PowerAlertType findPowerAlertTypeById(int id) throws SQLException {
		return maintenanceDAO.findPowerAlertTypeById(id);
	}

	@Override
	public List<PowerAlert> findAllOutagePowerAlertByIds(List<Integer> ids) throws SQLException {
		return maintenanceDAO.findAllOutagePowerAlertByIds(ids);
	}
	
	@Override
	public List<PowerAlert> findAllPreventivePowerAlertByIds(List<Integer> ids) throws SQLException {
		return maintenanceDAO.findAllPreventivePowerAlertByIds(ids);
	}

	@Override
	@Transactional
	public PowerAlert savePowerAlert(PowerAlert p0) throws SQLException {
		return maintenanceDAO.savePowerAlert(p0);		
	}


	@Override
	@Transactional
	public PowerAlert updatePowerAlert(PowerAlert p0) throws SQLException {
		return maintenanceDAO.updatePowerAlert(p0);
	}

	/*@Override
	public List<PowerAlert> findAllPowerAlertByCondition(Date startDate, Date endDate, int powerTypeId) throws SQLException {
		return maintenanceDAO.findAllPowerAlertByCondition(startDate, endDate, powerTypeId);
	}*/
	@Override
	public List<PowerAlert> findAllOutageByTypeId(Date startDate, Date endDate, int powerTypeId) throws SQLException {
		return maintenanceDAO.findAllOutageByTypeId(startDate, endDate, powerTypeId);
	}
	
	@Override
	public List<PowerAlert> findAllPreventiveByTypeId(Date startDate, Date endDate, int powerTypeId) throws SQLException {
		return maintenanceDAO.findAllPreventiveByTypeId(startDate, endDate, powerTypeId);
	}
	
	@Override
	public PowerAlert findPowerAlertById(int id) throws SQLException {
		return maintenanceDAO.findPowerAlertById(id);
	}
	
	@Override
	public PowerAlert findAllPowerAlertByCondition2ById(int id) throws SQLException {
		return  maintenanceDAO.findAllPowerAlertByCondition2ById(id);
	}

	


	@Override
	public List<PowerAlert> findAllOutageContent() throws SQLException {
		return maintenanceDAO.findAllOutageContent();
	}

	@Override
	public List<PowerAlert> findAllPreventiveContent() throws SQLException {
		return maintenanceDAO.findAllPreventiveContent();
	}

	/**Part-Push Notification**/
	@Override
	public List<TagNotification> findAllPNByTitleId(int titleId) throws SQLException {
		return maintenanceDAO.findAllPNByTitleId(titleId);
	}

	@Override
	public List<Integer> findPNDistinctTitleIdBtwDate(String notifType, String tagName) throws SQLException {
		return maintenanceDAO.findPNDistinctTitleIdBtwDate(notifType,  tagName);
	}
	
	@Override
	public List<Integer> findDistinctTitleIdBtwDatePower(String notifType, String tagName, String powerType)
			throws SQLException {
		return maintenanceDAO.findDistinctTitleIdBtwDatePower(notifType, tagName, powerType);
	}

	@Override
	public List<TagNotification> findAllFreeTextByIds() throws SQLException {
		return maintenanceDAO.findAllFreeTextByIds();
	}
	
	@Override
	public List<TagNotification> findContentByAssIdAndType(Integer associatedId, String notificationType) throws SQLException {
		return maintenanceDAO.findContentByAssIdAndType(associatedId,notificationType );
	}
	
	@Override
	public List<TagNotification> findAllByCondition1(Date startDate, Date endDate, String notifType, 
			Integer associatedId, String message) throws SQLException{
		return maintenanceDAO.findAllByCondition1(startDate, endDate, notifType, associatedId, message);
	}

	@Override
	public TagNotification findPushNotificationById(int id) throws SQLException {
		return maintenanceDAO.findPushNotificationById(id);	
	}
	
	@Override
	public List<Integer> findSubTitleIdBtwDate(Date startDate, Date endDate, String notifType, String tagName) throws SQLException {
		return maintenanceDAO.findSubTitleIdBtwDate(startDate, endDate, notifType, tagName);
	}
	
	@Override
	public List<Integer> findSubTitleIdBtwDatePower(Date startDate, Date endDate, String notifType, String tagName,
			String powerType) throws SQLException{
		return maintenanceDAO.findSubTitleIdBtwDatePower(startDate, endDate, notifType, tagName, powerType);
	}
	
	@Override
	@Transactional
	public TagNotification updatePushNotification(TagNotification p0) {
		return maintenanceDAO.updatePushNotification(p0);
	}
	
	
	@Override
	@Transactional
	public TagNotification savePushNotication(TagNotification p0) throws SQLException {
		return maintenanceDAO.savePushNotication(p0);
	}

	/**============================================
	 * FAQ
	 *============================================*/
	@Override
	public List<FaqCategory> findAllFaqCategory() throws SQLException {
		return maintenanceDAO.findAllFaqCategory();
	}
	
	@Override
	public FaqCategory findFaqCategoryById(int id) throws Exception{
		return maintenanceDAO.findFaqCategoryById(id);
	}
	
	@Override
	public Long findNextSeq(int catId) throws SQLException{
		return maintenanceDAO.findNextSeq(catId);
	}

	
	@Override
	@Transactional
	public Faq saveFAQ(Faq f0) throws SQLException {
		// TODO Auto-generated method stub
		return maintenanceDAO.saveFAQ(f0);
	}

	@Override
	public List<Faq> findWithOrderByQuestEn(int catId, String status) throws SQLException {
		return maintenanceDAO.findWithOrderByQuestEn(catId, status);
	}
	
	@Override
	public List<Faq> findActiveQuesEnOrderBySequence(int catId) throws SQLException {
		return maintenanceDAO.findActiveQuesEnOrderBySequence(catId);
	}

	@Override
	public List<Faq> findAllFaqById(int id) throws SQLException {
		return maintenanceDAO.findAllFaqById(id);
	}

	@Override
	@Transactional
	public boolean updateFAQ(Faq f0, boolean updateSeq) throws SQLException {
		return maintenanceDAO.updateFAQ(f0, updateSeq);
	}

	@Override
	public ContactUs findAllContact() throws SQLException {
		return maintenanceDAO.findAllContact();
	}

	@Override
	@Transactional
	public boolean updateContact(ContactUs contact) throws SQLException {
		return maintenanceDAO.updateContact(contact);
	}
	
	@Override
	@Transactional
	public List<TariffCategory> findAllTariffCategory() throws SQLException {
		return maintenanceDAO.findAllTariffCategory();
	}

	
	@Override
	@Transactional
	public TariffSetting saveTariffSettings(TariffSetting ts) throws SQLException {
		return maintenanceDAO.saveTariffSettings(ts);
	}

	@Override
	public TariffCategory findTariffCategoryById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return maintenanceDAO.findTariffCategoryById(id);
	}

	@Override
	public List<TariffSetting> findAllTariffSettings(int catId) throws SQLException {
		// TODO Auto-generated method stub
		return maintenanceDAO.findAllTariffSettings(catId);
	}

	@Override
	@Transactional
	public int deleteTariffSettingByCatId(int catId) throws SQLException {
		// TODO Auto-generated method stub
		return maintenanceDAO.deleteTariffSettingByCatId(catId);
	}


	
}
