package com.seb.admin.portal.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.seb.admin.portal.constant.SPGConstant;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.service.ReportService;
import com.seb.admin.portal.spg.apis.SPGAPIServices;
import com.seb.admin.portal.spg.entity.OFile;
import com.seb.admin.portal.spg.entity.OFpx;
import com.seb.admin.portal.spg.entity.OFpxBank;
import com.seb.admin.portal.spg.entity.OMbb;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;
import com.seb.admin.portal.spg.entity.UpdateFPXBank;
import com.seb.admin.portal.spg.request.FPXBankListRequest;
import com.seb.admin.portal.spg.request.FPXBankQueryRequest;
import com.seb.admin.portal.spg.request.GatewayQueryFPXRequest;
import com.seb.admin.portal.spg.request.GatewayQueryMBBRequest;
import com.seb.admin.portal.spg.request.OFpxBankUpdateRequest;
import com.seb.admin.portal.spg.response.FPXBankListResponse;
import com.seb.admin.portal.spg.response.GatewayFPXGenerateCSRResponse;
import com.seb.admin.portal.spg.response.GatewayFPXResponse;
import com.seb.admin.portal.spg.response.GatewayMBBResponse;
import com.seb.admin.portal.spg.response.JSONResponse;
import com.seb.admin.portal.spg.response.QueryFPXBankResponse;
import com.seb.admin.portal.util.StringUtil;

import retrofit2.Response;

@Controller
@SessionAttributes( {"fpxBankIdList","bankNameList"})
@RequestMapping("/admin/spg/pymtGatewayMaintenance")
public class SPGGatewayMaintenanceController {	

	private static final Logger log = Logger.getLogger(SPGGatewayMaintenanceController.class);

	@Autowired
	private ReportService reportService;
	@Autowired
	private SPGAPIServices SPGAPIServices;


	@RequestMapping(value = "/mpg")
	public ModelAndView doMPGPaymentGatewayMaintenance(HttpServletRequest request) {

		System.out.println("[doMPGPaymentGatewayMaintenance]");

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();

		SPGAPIGeneral apiGeneral = new SPGAPIGeneral();		
		apiGeneral.setAdmin_id(user.getLoginId());

		Response<GatewayMBBResponse> response;

		try {
			response = SPGAPIServices.executeQueryGatewayMBB(apiGeneral, StringUtil.getLanguage(""));

			log.info("[doMPGPaymentGatewayMaintenance]"+response.raw().request().url().toString());

			if(response.isSuccessful() && response.body().getResponseStatus().isSuccess()) {
				OMbb mbb = response.body().getMbbGateway();
				request.getSession().setAttribute("mbb", mbb);
				model.addObject("mbb", mbb);
			}
			else {
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", StringUtil.trimToEmpty(response.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(response.body().getResponseStatus().getMessages())));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("/spg-maintenance/gatewayMaintenanceMPG");
		return model;
	}

	@RequestMapping(value = "/mpg", method = RequestMethod.POST)
	public ModelAndView doUpdatePaymentGatewayMaintenanceMPG(
			//@RequestParam(value="gatewayId", defaultValue = "") String gatewayId,
			@RequestParam(value="hashKey", defaultValue = "") String hashKey,
			@RequestParam(value="amexHashKey", defaultValue = "") String amexHashKey,
			@RequestParam(value="merchantId", defaultValue = "") String merchantId,
			@RequestParam(value="amexMerchantId", defaultValue = "") String amexMerchantId,
			@RequestParam(value="minPayAmt", defaultValue = "") Double minPayAmt,
			@RequestParam(value="maxPayAmt", defaultValue = "") Double maxPayAmt,
			@RequestParam(value="trxSubmitURL", defaultValue = "") String trxSubmitURL,
			@RequestParam(value="trxQueryURL", defaultValue = "") String trxQueryURL,
			@RequestParam(value="statusOpt", defaultValue = "") String statusOpt,
			HttpServletRequest request, HttpServletResponse resp){

		ModelAndView model = new ModelAndView();

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		try
		{				
			for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
				String fieldName = (String) e.nextElement();
				String fieldValue = request.getParameter(fieldName);
				if ((fieldValue != null) && (fieldValue.length() > 0)) {
					System.out.println(fieldName + ":" + fieldValue);

				}
			}	

			GatewayQueryMBBRequest mbbRequest = new GatewayQueryMBBRequest();			
			mbbRequest.setAdmin_id(user.getLoginId());

			OMbb mbb = new OMbb();
		//	mbb.setGatewayId(StringUtil.trimToEmpty(gatewayId));
			mbb.setHash_key(StringUtil.trimToEmpty(hashKey));
			mbb.setAmex_hash_key(StringUtil.trimToEmpty(amexHashKey));
			mbb.setMerchant_id(StringUtil.trimToEmpty(merchantId));
			mbb.setAmex_merchant_id(StringUtil.trimToEmpty(amexMerchantId));
			mbb.setSubmit_url(StringUtil.trimToEmpty(trxSubmitURL));
			mbb.setQuery_url(StringUtil.trimToEmpty(trxQueryURL));
			mbb.setMinimun_payment_amount(minPayAmt);
			mbb.setMaximum_payment_amount(maxPayAmt);
			mbb.setEnabled((statusOpt.equalsIgnoreCase("true") ? true : false));
			mbbRequest.setMbb(mbb);

			Response<GatewayMBBResponse> mbbResponse = SPGAPIServices.executeUpdateGatewayMBB(mbbRequest, StringUtil.getLanguage(""));

			if(mbbResponse.isSuccessful() && mbbResponse.body().getResponseStatus().isSuccess()) {
				model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully edit Maybank gateway."));
				model.addObject("mbb", mbb);

				OMbb exist = (OMbb) request.getSession().getAttribute("mbb");
				request.getSession().setAttribute("mbb",  mbbResponse.body().getMbbGateway());


				reportService.auditLog("Payment Gateway Maintenance", new AuditTrail(user.getId(),0,"","Maybank Gateway: "+user.getLoginId(),exist.toString(),mbb.toString(), "API:"+mbbResponse.raw().request().url().toString()));		

			}else {
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", StringUtil.trimToEmpty(mbbResponse.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(mbbResponse.body().getResponseStatus().getMessages())));
				reportService.auditLog("Payment Gateway Maintenance", new AuditTrail(user.getId(),0,"","Maybank Gateway: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+mbbResponse.raw().request().url().toString()+"\n"+StringUtil.trimToEmpty(mbbResponse.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(mbbResponse.body().getResponseStatus().getMessages())));		
			}


		}catch(Exception ex){
			ex.printStackTrace();
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to edit Maybank Gateway."));
			reportService.auditLog("Payment Gateway Maintenance", new AuditTrail(user.getId(),0,"","Add Payment Client: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
			log.error(ex.getMessage());
			ex.printStackTrace();
		}finally {

		}
		model.setViewName("/spg-maintenance/gatewayMaintenanceMPG");
		return model;
	}

	//=========================================== [FPX] ===============================================
	@RequestMapping(value = "/fpx")
	public ModelAndView doFPXPaymentGatewayMaintenance(HttpServletRequest request) {

		System.out.println("[doFPXPaymentGatewayMaintenance]");

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();

		SPGAPIGeneral apiGeneral = new SPGAPIGeneral();		
		apiGeneral.setAdmin_id(user.getLoginId());

		Response<GatewayFPXResponse> response;

		try {
			response = SPGAPIServices.executeQueryGatewayFPX(apiGeneral, StringUtil.getLanguage(""));

			log.info("[doFPXPaymentGatewayMaintenance]"+response.raw().request().url().toString());

			if(response.isSuccessful() && response.body().getResponseStatus().isSuccess()) {
				OFpx fpx = response.body().getFpxGateway();
				request.getSession().setAttribute("currentFpx", fpx);
				model.addObject("fpx", fpx);
			}
			else {
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", StringUtil.trimToEmpty(response.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(response.body().getResponseStatus().getMessages())));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("/spg-maintenance/gatewayMaintenanceFPX");
		return model;
	}

	@RequestMapping(value = "/fpx", method = RequestMethod.POST)
	public ModelAndView doUpdatePaymentGatewayMaintenanceFPX(
			@RequestParam(value="merchantId", defaultValue = "") String merchantId,
			@RequestParam(value="exchangeId", defaultValue = "") String exchangeId,
			@RequestParam(value="version", defaultValue = "") String version,
			@RequestParam(value="minPayAmt", defaultValue = "") Double minPayAmt,
			@RequestParam(value="maxPayAmt", defaultValue = "") Double maxPayAmt,
			@RequestParam(value="trxSubmitURL", defaultValue = "") String trxSubmitURL,
			@RequestParam(value="trxQueryURL", defaultValue = "") String trxQueryURL,
			@RequestParam(value="bankListURL", defaultValue = "") String bankListURL,
			@RequestParam(value="sellerBankCode", defaultValue = "") String sellerBankCode,
			@RequestParam(value="csrCountryName", defaultValue = "") String csrCountryName,
			@RequestParam(value="csrStateProvinceName", defaultValue = "") String csrStateProvinceName,
			@RequestParam(value="csrLocalityName", defaultValue = "") String csrLocalityName,
			@RequestParam(value="csrOrgName", defaultValue = "") String csrOrgName,
			@RequestParam(value="csrOrgUnit", defaultValue = "") String csrOrgUnit,
			@RequestParam(value="csrCommonName", defaultValue = "") String csrCommonName,
			@RequestParam(value="csrEmail", defaultValue = "") String csrEmail,			
			@RequestParam(value="fpxEnvCurrentCrt", defaultValue = "") MultipartFile fpxEnvCurrentCrt,
			@RequestParam(value="privateKeyCrt", defaultValue = "") MultipartFile privateKeyCrt,
			@RequestParam(value="privateKeyIdentityCert", defaultValue = "") MultipartFile privateKeyIdentityCert,
			@RequestParam(value="fpxEnvCrtExpiry", defaultValue = "") String fpxEnvCrtExpiry,
			@RequestParam(value="fpxExCrtExpiry", defaultValue = "") String fpxExCrtExpiry,
			@RequestParam(value="warningDay", defaultValue = "0") int warningDay,
			@RequestParam(value="warningEmail", defaultValue = "") String warningEmail,
			@RequestParam(value="statusOpt", defaultValue = "") String statusOpt,

			HttpServletRequest request, HttpServletResponse resp){

		ModelAndView model = new ModelAndView();

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		try
		{				
			for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
				String fieldName = (String) e.nextElement();
				String fieldValue = request.getParameter(fieldName);
				if ((fieldValue != null) && (fieldValue.length() > 0)) {
					System.out.println(fieldName + ":" + fieldValue);
				}
			}	

			GatewayQueryFPXRequest fpxRequest = new GatewayQueryFPXRequest();	
			OFpx exist = (OFpx) request.getSession().getAttribute("currentFpx");
			
			fpxRequest.setAdmin_id(user.getLoginId());

			OFpx fpx = new OFpx();
			fpx.setMerchant_id(merchantId);
			fpx.setExchange_id(exchangeId);
			fpx.setVersion(version);
			fpx.setMinimun_payment_amount(minPayAmt);
			fpx.setMaximum_payment_amount(maxPayAmt);
			fpx.setSubmit_url(trxSubmitURL);
			fpx.setQuery_url(trxQueryURL);
			fpx.setBank_listing_url(bankListURL);
			fpx.setSeller_bank_code(sellerBankCode);
			fpx.setCsr_country_name(csrCountryName);
			fpx.setCsr_state_or_province_name(csrStateProvinceName);
			fpx.setCsr_locality_name(csrLocalityName);
			fpx.setCsr_organization_name(csrOrgName);
			fpx.setCsr_organization_unit(csrOrgUnit);

			fpx.setCertificate_expiry_warning_days_before(warningDay);
			fpx.setCertificate_expiry_warning_email(warningEmail);
			fpx.setEnabled((statusOpt.equalsIgnoreCase("true") ? true : false));
			String publicKeyCert = null;
			String privateKeyCert = null;
			String signingReplyCert = null;
			
			// Create the file on server
			if(fpxEnvCurrentCrt != null && !fpxEnvCurrentCrt.isEmpty()){
				byte[] bytes = fpxEnvCurrentCrt.getBytes();
				System.out.println("new public key file name >>"+fpxEnvCurrentCrt.getOriginalFilename());
				publicKeyCert = Base64.getEncoder().encodeToString(bytes);
				fpx.setPublic_key(publicKeyCert);
				fpx.setPublic_key_file_name(fpxEnvCurrentCrt.getOriginalFilename());
			}else {
				System.out.println("existing  public key file name >>"+exist.getPublic_key_file_name());
				fpx.setPublic_key(exist.getPublic_key());
				fpx.setPublic_key_file_name(exist.getPublic_key_file_name());
			}
			

			if(privateKeyCrt != null && !privateKeyCrt.isEmpty()){
				byte[] bytes = privateKeyCrt.getBytes();
				System.out.println("new private key file name >>"+privateKeyCrt.getOriginalFilename());
				privateKeyCert = Base64.getEncoder().encodeToString(bytes);
				fpx.setPrivate_key(privateKeyCert);
				fpx.setPrivate_key_file_name(privateKeyCrt.getOriginalFilename());
			}else {
				System.out.println("existing private key file name >>"+exist.getPrivate_key_identity_cert_file_name());
				fpx.setPrivate_key(exist.getPrivate_key());
				fpx.setPrivate_key_file_name(exist.getPrivate_key_identity_cert_file_name());
			}
			

			//Certificate Exchange - Signing Reply
			if(privateKeyIdentityCert != null && !privateKeyIdentityCert.isEmpty()){
				byte[] bytes = privateKeyIdentityCert.getBytes();
				System.out.println("new Certificate Exchange - Signing Reply file name >>"+privateKeyIdentityCert.getOriginalFilename());
				signingReplyCert = Base64.getEncoder().encodeToString(bytes);
				fpx.setPrivate_key_identity_cert(signingReplyCert);
				fpx.setPrivate_key_identity_cert_file_name(privateKeyIdentityCert.getOriginalFilename());
			}else {
				System.out.println("existing Certificate Exchange - Signing Reply file name >>"+exist.getPrivate_key_identity_cert_file_name());
				fpx.setPrivate_key_identity_cert(exist.getPrivate_key_identity_cert());
				fpx.setPrivate_key_identity_cert_file_name(exist.getPrivate_key_identity_cert_file_name());
			}


			fpxRequest.setFpx(fpx);

			Response<GatewayFPXResponse> fpxResponse = SPGAPIServices.executeUpdateGatewayFPX(fpxRequest, StringUtil.getLanguage(""));

			if(fpxResponse.isSuccessful() && fpxResponse.body().getResponseStatus().isSuccess()) {
				model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully edit FPX gateway."));
				model.addObject("fpx", fpxResponse.body().getFpxGateway());
				request.getSession().setAttribute("currentFpx",  fpxResponse.body().getFpxGateway());


				reportService.auditLog("Payment Gateway Maintenance", new AuditTrail(user.getId(),0,"","FPX Gateway: "+user.getLoginId(),exist.toString(),fpx.toString(), "API:"+fpxResponse.raw().request().url().toString()));		

			}else {
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", StringUtil.trimToEmpty(fpxResponse.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(fpxResponse.body().getResponseStatus().getMessages())));
				reportService.auditLog("Payment Gateway Maintenance", new AuditTrail(user.getId(),0,"","FPX Gateway: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+fpxResponse.raw().request().url().toString()+"\n"+StringUtil.trimToEmpty(fpxResponse.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(fpxResponse.body().getResponseStatus().getMessages())));		
			}


		}catch(Exception ex){
			ex.printStackTrace();
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to edit FPX Gateway."));
			reportService.auditLog("Payment Gateway Maintenance", new AuditTrail(user.getId(),0,"","Add Payment Client: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
			log.error(ex.getMessage());
			ex.printStackTrace();
		}finally {

		}
		model.setViewName("/spg-maintenance/gatewayMaintenanceFPX");
		return model;
	}

	/************************ FPX CSR Generation *************************************************/
	@RequestMapping(value = "/generateCSR", method = RequestMethod.GET)
	public @ResponseBody void downloadFile(HttpServletResponse resp) {
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		String downloadFileName= "csr.csr";
		String downloadStringContent= "";

		SPGAPIGeneral apiGeneral = new SPGAPIGeneral();		
		apiGeneral.setAdmin_id(user.getLoginId());

		Response<GatewayFPXGenerateCSRResponse> generateCSRResponse = null;

		try {

			generateCSRResponse = SPGAPIServices.executeGenerateCSR(apiGeneral, StringUtil.getLanguage(""));

			log.info("[generateCSR]"+generateCSRResponse.raw().request().url().toString());

			if(generateCSRResponse.isSuccessful() && generateCSRResponse.body().getResponseStatus().isSuccess()) {
				OFile file = generateCSRResponse.body().getoFile();
				downloadFileName = file.getName();
				downloadStringContent = file.getContent();
			}
			else {
				log.info(StringUtil.trimToEmpty(generateCSRResponse.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(generateCSRResponse.body().getResponseStatus().getMessages()));
			}

			OutputStream out = resp.getOutputStream();
			resp.setContentType("text/plain; charset=utf-8");
			resp.addHeader("Content-Disposition","attachment; filename=\"" + downloadFileName + "\"");
			out.write(downloadStringContent.getBytes(Charset.forName("UTF-8")));
			out.flush();
			out.close();

		} catch (IOException e) {
			log.error("generateCSR",e);
		}

	}

	/************************  download downloadCert *************************************************/
	@RequestMapping(value = "/downloadCert/{certType}/{certName}", method = RequestMethod.GET)
	public @ResponseBody void downloadCert(HttpServletRequest request, HttpServletResponse resp , @PathVariable("certType") String certType, @PathVariable("certName") String certName) {

		String downloadStringContent= "";
		String downloadFileName = "";

		try {
			log.info("downloadCert fileName: "+certName);

			OFpx fpx = (OFpx) request.getSession().getAttribute("currentFpx");
			downloadFileName = "";

			switch(certType)
			{
			case "public_key_file_name":
				certName = fpx.getPublic_key_file_name();
				downloadStringContent = fpx.getPublic_key();
				break;
			case "private_key_file_name":
				certName = fpx.getPrivate_key_file_name();
				downloadStringContent = fpx.getPrivate_key();
				break;
			case "private_key_identity_cert_file_name":
				certName = fpx.getPrivate_key_identity_cert_file_name();
				downloadStringContent = fpx.getPrivate_key_identity_cert();
				break;
			default:
			}



			OutputStream out = resp.getOutputStream();
			resp.setContentType("text/plain; charset=utf-8");
			resp.addHeader("Content-Disposition","attachment; filename=\"" + downloadFileName + "\"");
			out.write(downloadStringContent.getBytes(Charset.forName("UTF-8")));
			out.flush();
			out.close();

		} catch (IOException e) {
			log.error("downloadCert",e);
		}

	}


	//FPX Bank List
	@RequestMapping(value = "/fpxBankList")
	public ModelAndView doFPXBankList(HttpServletRequest request) {

		System.out.println("[doFPXBankList]");

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();

		FPXBankListRequest fpxBLReq = new FPXBankListRequest();	

		Response<FPXBankListResponse> fpxBLResp;

		try {
			fpxBLResp = SPGAPIServices.executeFPXBankList(fpxBLReq, StringUtil.getLanguage(""));

			log.info("[doFPX BankList]"+fpxBLResp.raw().request().url().toString());

			if(fpxBLResp.isSuccessful() && fpxBLResp.body().getResponseStatus().isSuccess()) {
				List<OFpxBank> list = fpxBLResp.body().getOfpxBank();


				//List<OFpxBank> fpxBankIdList = new ArrayList<>();
				//fpxBankIdList = list.stream().filter(e -> e.getBank_fpx_id()!=null).sorted(Comparator.comparing(OFpxBank::getBank_fpx_id)).distinct().collect(Collectors.toList());
				Set<String> fpxBankIdSet = new HashSet<>();
				Set<String> fpxBankName = new HashSet<>();
				List<OFpxBank> fpxBankIdList = list.stream()
						.filter(e -> fpxBankIdSet.add(e.getBank_fpx_id()))
						.sorted(Comparator.comparing(OFpxBank::getBank_fpx_id)).collect(Collectors.toList());
				System.out.println(fpxBankIdList.size());

				model.addObject("fpxBankIdList",fpxBankIdList);
				model.addObject("bankNameList", list.stream()
						.filter(e -> e.getBank_name()!=null && fpxBankName.add(e.getBank_name()))
						.sorted(Comparator.comparing(OFpxBank::getBank_name)).collect(Collectors.toList()));
			}
			else {
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", StringUtil.trimToEmpty(fpxBLResp.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(fpxBLResp.body().getResponseStatus().getMessages())));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("/spg-maintenance/fpxBankList");
		return model;
	}


	@RequestMapping(value = "/fpxBankList", method = RequestMethod.POST)
	public ModelAndView doFPXBankList(
			@RequestParam(value="selectFPXBankId", defaultValue = "") String selectFPXBankId,
			@RequestParam(value="selectBankName", defaultValue = "") String selectBankName,
			@RequestParam(value="selectBankStatus", defaultValue = "") String selectBankStatus,
			@RequestParam(value="bankStatusText", defaultValue = "") String bankStatusText,
			HttpServletRequest request, HttpServletResponse resp){
		log.info("====================================================");
		System.out.println("doFPXBankList");
		for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
			String fieldName = (String) e.nextElement();
			String fieldValue = request.getParameter(fieldName);
			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				System.out.println(fieldName + ":" + fieldValue);
			}
		}


		ModelAndView model = new ModelAndView();

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		FPXBankListRequest  fpxBLReq = new FPXBankListRequest();
		fpxBLReq.setAdmin_id(user.getLoginId());
		if(!selectFPXBankId.equalsIgnoreCase(""))
			fpxBLReq.setFpx_bank_id(selectFPXBankId) ;

		if(!selectBankName.equalsIgnoreCase(""))
			fpxBLReq.setBank_name(selectBankName);

		if(!selectBankStatus.equalsIgnoreCase("")) {
			System.out.println("ddddddddd");
			fpxBLReq.setEnabled((selectBankStatus.equalsIgnoreCase("true"))? true : false);
		}

		Response<FPXBankListResponse> fpxBLResp;

		try {
			fpxBLResp = SPGAPIServices.executeFPXBankList(fpxBLReq, StringUtil.getLanguage(""));

			log.info("[doFPXBankList]"+fpxBLResp.raw().request().url().toString());

			if(fpxBLResp.isSuccessful() && fpxBLResp.body().getResponseStatus().isSuccess()) {
				List<OFpxBank> list = fpxBLResp.body().getOfpxBank();
				model.addObject("totalList", list);

			}
			else {
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", StringUtil.trimToEmpty(fpxBLResp.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(fpxBLResp.body().getResponseStatus().getMessages())));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {

		}
		System.out.println("bankStatusText>"+bankStatusText);
		model.addObject("selectFPXBankId",selectFPXBankId );
		model.addObject("selectBankName",selectBankName );
		model.addObject("bankStatusText",bankStatusText );

		model.setViewName("/spg-maintenance/fpxBankList");
		return model;
	}

	@RequestMapping(value = "/bankQuery",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody <T> JSONResponse<?> bankQuery(@RequestBody OFpxBank form, HttpServletRequest req, HttpServletResponse resp) {	
		log.info("***********************************************************************************************");
		log.info("[bankQuery]");
		log.info("form >>"+form.toString());

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		JSONResponse<?> jsonResponse = new JSONResponse<>();

		try {
			FPXBankQueryRequest bankReq = new FPXBankQueryRequest();
			bankReq.setBank_id(form.getBank_id());

			Response<QueryFPXBankResponse> response = SPGAPIServices.executeQueryFPXBank(bankReq, StringUtil.getLanguage(""));
			jsonResponse.setSuccess(true);

			if(response.isSuccessful() && response.body().getResponseStatus().isSuccess()) {
				ObjectMapper mapper = new ObjectMapper();
				OFpxBank a = mapper.convertValue(response.body().getOfpxBank(), OFpxBank.class);
				jsonResponse.setObjectDetail(a);
				req.getSession().setAttribute("selectedBank", a);

			}else {
				jsonResponse.setSuccess(false);
				jsonResponse.setStatusCode(-1);
				jsonResponse.setMessage(Arrays.toString(response.body().getResponseStatus().getMessages()));
				reportService.auditLog("Payment Client Maintenance", new AuditTrail(user.getId(),0,"","Update FPX Bank List: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+response.raw().request().url().toString()+"\n"+StringUtil.trimToEmpty(response.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(response.body().getResponseStatus().getMessages())));		

			}

		} catch (Exception e) {
			reportService.auditLog("Payment Gateway Maintenance", new AuditTrail(user.getId(),0,"","Update FPX Bank List: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+e.getMessage()));		
			log.error(this.getClass().getSimpleName(), e);
		}

		return jsonResponse;
	}

	@RequestMapping(value = "/doUpdateFPXBank",  method = RequestMethod.POST)
	public @ResponseBody JSONResponse<?> doUpdateFPXBank( @RequestPart("data") UpdateFPXBank form,  @RequestPart(value ="bankLogo", required=false) MultipartFile myFile) {	

		log.info("[doUpdateFPXBank]");
		log.info(form.toString());
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		JSONResponse<?> jsonResponse = new JSONResponse<>();
		OFpxBank existFpxBank = new OFpxBank();

		ModelAndView model = new ModelAndView();

		try {
			String logo = null;

			if (myFile != null) {
				logo = Base64.getEncoder().encodeToString(myFile.getBytes());

			}else {
				if(form.getHasPic().equalsIgnoreCase("0")) {
					logo = null;
				}else {

					FPXBankQueryRequest bankReq = new FPXBankQueryRequest();
					bankReq.setBank_id(form.getBank_id());
					Response<QueryFPXBankResponse> response = SPGAPIServices.executeQueryFPXBank(bankReq, StringUtil.getLanguage(""));
					jsonResponse.setSuccess(true);

					if(response.isSuccessful() && response.body().getResponseStatus().isSuccess()) {
						ObjectMapper mapper = new ObjectMapper();
						existFpxBank = mapper.convertValue(response.body().getOfpxBank(), OFpxBank.class);
						if(existFpxBank.getLogo()!=null) {
							logo = existFpxBank.getLogo();
						}

					}

				}

			}

			OFpxBankUpdateRequest queryReq = new OFpxBankUpdateRequest();
			OFpxBank oFpxBank = new OFpxBank();

			oFpxBank.setBank_id(form.getBank_id());
			oFpxBank.setBank_name(form.getBank_name());
			oFpxBank.setEnabled(true);//default;
			if(logo!=null) {
				oFpxBank.setLogo(logo);
			}
			queryReq.setAdmin_id(user.getLoginId());
			queryReq.setFpx_bank(oFpxBank);


			Response<QueryFPXBankResponse> response = SPGAPIServices.executeUpdateFPXBank(queryReq, StringUtil.getLanguage(""));
			if(response.isSuccessful() && response.body().getResponseStatus().isSuccess()) {
				jsonResponse.setSuccess(true);				
				jsonResponse.setMessage("FPX bank list update successfully.");
				ObjectMapper mapper = new ObjectMapper();
				OFpxBank newList = mapper.convertValue(response.body().getOfpxBank(), OFpxBank.class);
				jsonResponse.setObjectDetail(newList);
				reportService.auditLog("Payment Gateway Maintenance", new AuditTrail(user.getId(),0,"","Update FPX Bank List: "+user.getLoginId(),existFpxBank.toString(), form.toString(), "API:"+response.raw().request().url().toString()));		

			}else {
				jsonResponse.setSuccess(false);
				jsonResponse.setStatusCode(-1);
				jsonResponse.setMessage(Arrays.toString(response.body().getResponseStatus().getMessages()));

				reportService.auditLog("Payment Gateway Maintenance", new AuditTrail(user.getId(),0,"","Update FPX Bank List: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+response.raw().request().url().toString()+"\n"+StringUtil.trimToEmpty(response.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(response.body().getResponseStatus().getMessages())));		

			}
		} catch (Exception e) {
			jsonResponse.setSuccess(false);
			jsonResponse.setStatusCode(-1);
			jsonResponse.setMessage(SPGConstant.API_ERROR);

			reportService.auditLog("Payment Gateway Maintenance", new AuditTrail(user.getId(),0,"","Update FPX Bank List: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+e.getMessage()));		
			model.addObject("errorMsg","Failed to update FPX bank");
			log.error(this.getClass().getSimpleName(), e);
		}

		return jsonResponse;
	}

}
