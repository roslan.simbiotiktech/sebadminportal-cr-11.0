package com.seb.admin.portal.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.seb.admin.portal.constant.ApplicationConstant;


/**
 * The persistent class for the ACCESS_RIGHTS database table.
 * 
 */
@Entity
@Table(name="ACCESS_RIGHTS", schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="AccessRight.findAllByAdminId", query="SELECT a FROM AccessRight a WHERE a.adminUserId =:adminId AND a.deletedFlag=0 ORDER BY a.portalModule.moduleCode asc"),
	@NamedQuery(name="AccessRight.findMainAccessById", query="SELECT a FROM AccessRight a WHERE a.adminUserId =:adminId AND a.portalModule.parentModuleCode= '0' AND a.portalModule.role LIKE :role  AND a.deletedFlag=0 ORDER BY a.portalModule.moduleCode asc"),
	@NamedQuery(name="AccessRight.findUserManagement", query="SELECT a FROM AccessRight a WHERE a.adminUserId =:adminId AND a.portalModule.accessUrl<>'' AND a.deletedFlag=0 ORDER BY a.portalModule.moduleCode asc"),
	@NamedQuery(name="AccessRight.findExistingAccessByAdminId", query="SELECT a FROM AccessRight a WHERE a.adminUserId =:adminId ORDER BY a.portalModule.moduleCode asc"),
	@NamedQuery(name="AccessRight.findExistModule", query="SELECT a FROM AccessRight a WHERE a.adminUserId =:adminId AND a.portalModuleId =:moduleId"),

})
public class AccessRight implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(name="ADMIN_USER_ID")
	private int adminUserId;

	@Column(name="CREATED_DATETIME" , nullable=false, insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(name="PORTAL_MODULE_ID")
	private int portalModuleId;

	@Column(name="UPDATED_DATETIME" , nullable=false, insertable = false, updatable = false)
	private Timestamp updatedDatetime;

	@JoinColumn(name = "PORTAL_MODULE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private PortalModule portalModule;

	@JoinColumn(name = "ADMIN_USER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private AdminUser adminUser;

	public AccessRight() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAdminUserId() {
		return this.adminUserId;
	}

	public void setAdminUserId(int adminUserId) {
		this.adminUserId = adminUserId;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public int getPortalModuleId() {
		return this.portalModuleId;
	}

	public void setPortalModuleId(int portalModuleId) {
		this.portalModuleId = portalModuleId;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	 @JsonBackReference
	public PortalModule getPortalModule() {
		return portalModule;
	}

	public void setPortalModule(PortalModule portalModule) {
		this.portalModule = portalModule;
	}

	@JsonBackReference //for parent table
	public AdminUser getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(AdminUser adminUser) {
		this.adminUser = adminUser;
	}



}