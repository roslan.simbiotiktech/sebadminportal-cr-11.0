package com.seb.admin.portal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class MyTariffSettingsResult implements Serializable {
	private static final long serialVersionUID = 1L;

	private static int one_hund = 100;
	private int id;

	private Timestamp createdDatetime;

	private int deletedFlag;

	private Integer exemptedGstFirstKwh;

	private BigDecimal kwMaxDemandAmount;

	private int lowPowerFactor;

	private BigDecimal minMonthlyChargeAmount;

	private BigDecimal nonPeakAmount;

	private int peak;

	private BigDecimal peakAmount;

	private int tariffCategoryId;

	private BigDecimal trAmountPerUnit;

	private Integer trFromUnit;

	private BigDecimal trMoreAmount;

	private Integer trMoreUnit;

	private Integer trToUnit;

	private Timestamp updatedDatetime;

	private TariffCategory tariffCategory;

	public MyTariffSettingsResult() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public Integer getExemptedGstFirstKwh() {
		if (exemptedGstFirstKwh == 0)
			return null;
		return this.exemptedGstFirstKwh;
	}

	public void setExemptedGstFirstKwh(Integer exemptedGstFirstKwh) {
		this.exemptedGstFirstKwh = exemptedGstFirstKwh;
	}

	public BigDecimal getKwMaxDemandAmount() {
		if (kwMaxDemandAmount.compareTo(BigDecimal.ZERO) == 0)
			return null;
		return this.kwMaxDemandAmount;
	}

	public void setKwMaxDemandAmount(BigDecimal kwMaxDemandAmount) {
		this.kwMaxDemandAmount = kwMaxDemandAmount;
	}

	public int getLowPowerFactor() {
		return this.lowPowerFactor;
	}

	public void setLowPowerFactor(int lowPowerFactor) {
		this.lowPowerFactor = lowPowerFactor;
	}

	public BigDecimal getMinMonthlyChargeAmount() {
		if (minMonthlyChargeAmount.compareTo(BigDecimal.ZERO) == 0)
			return null;
		return this.minMonthlyChargeAmount;
	}

	public void setMinMonthlyChargeAmount(BigDecimal minMonthlyChargeAmount) {
		this.minMonthlyChargeAmount = minMonthlyChargeAmount;
	}

	public int getPeak() {
		return this.peak;
	}

	public void setPeak(int peak) {
		this.peak = peak;
	}

	public int getTariffCategoryId() {
		return this.tariffCategoryId;
	}

	public void setTariffCategoryId(int tariffCategoryId) {
		this.tariffCategoryId = tariffCategoryId;
	}

	public BigDecimal getTrAmountPerUnit() {
		trAmountPerUnit = trAmountPerUnit.multiply(new BigDecimal(one_hund));
		if (trAmountPerUnit.compareTo(BigDecimal.ZERO) == 0)
			return null;
		return trAmountPerUnit;
	}

	public void setTrAmountPerUnit(BigDecimal trAmountPerUnit) {
		this.trAmountPerUnit = trAmountPerUnit;
	}

	public Integer getTrFromUnit() {
		if (trFromUnit == 0)
			return null;
		return this.trFromUnit;
	}

	public void setTrFromUnit(Integer trFromUnit) {
		this.trFromUnit = trFromUnit;
	}

	public BigDecimal getTrMoreAmount() {
		trMoreAmount = trMoreAmount.multiply(new BigDecimal(one_hund));
		if (trMoreAmount.compareTo(BigDecimal.ZERO) == 0)
			return null;
		return trMoreAmount;
	}

	public void setTrMoreAmount(BigDecimal trMoreAmount) {
		this.trMoreAmount = trMoreAmount;
	}

	public Integer getTrMoreUnit() {
		if (trMoreUnit  == 0)
			return null;
		return this.trMoreUnit;
	}

	public void setTrMoreUnit(Integer trMoreUnit) {
		this.trMoreUnit = trMoreUnit;
	}

	public Integer getTrToUnit() {
		if (trToUnit == 0)
			return null;
		return this.trToUnit;
	}

	public void setTrToUnit(Integer trToUnit) {
		this.trToUnit = trToUnit;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public TariffCategory getTariffCategory() {
		return tariffCategory;
	}

	public void setTariffCategory(TariffCategory tariffCategory) {
		this.tariffCategory = tariffCategory;
	}

	public BigDecimal getNonPeakAmount() {
		if (nonPeakAmount.compareTo(BigDecimal.ZERO) == 0)
			return null;
		else{
			nonPeakAmount = nonPeakAmount.multiply(new BigDecimal(one_hund));
			return nonPeakAmount;
		}
	}

	public void setNonPeakAmount(BigDecimal nonPeakAmount) {
		this.nonPeakAmount = nonPeakAmount;
	}

	public BigDecimal getPeakAmount() {
		if (peakAmount.compareTo(BigDecimal.ZERO) == 0)
			return null;
		else{
			peakAmount = peakAmount.multiply(new BigDecimal(one_hund));
			return peakAmount;
		}
	}

	public void setPeakAmount(BigDecimal peakAmount) {
		this.peakAmount = peakAmount;
	}



}