package com.seb.admin.portal.adapter;

import java.util.List;

public class SearchProfileResponse {
	private String maxPage;
	private String totalRecords;
	private String page;
		
	private Long respStat;
	private String errCode;
	private String errDesc;	
	private List<USER> respUser;
	
	public String getMaxPage() {
		return maxPage;
	}
	public void setMaxPage(String maxPage) {
		this.maxPage = maxPage;
	}
	public String getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public Long getRespStat() {
		return respStat;
	}
	public void setRespStat(Long respStat) {
		this.respStat = respStat;
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public String getErrDesc() {
		return errDesc;
	}
	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}
	public List<USER> getRespUser() {
		return respUser;
	}
	public void setRespUser(List<USER> respUser) {
		this.respUser = respUser;
	}
	@Override
	public String toString() {
		return "SearchProfileResponse [maxPage=" + maxPage + ", totalRecords=" + totalRecords + ", page=" + page
				+ ", respStat=" + respStat + ", errCode=" + errCode + ", errDesc=" + errDesc + ", respUser=" + respUser
				+ "]";
	}
	

	
	
	
	
}
