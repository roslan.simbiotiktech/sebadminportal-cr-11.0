<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
<style type="text/css">
</style>
</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->

		<nav class="navbar navbar-default navbar-static-top">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12" style="width: inherit; min-width: 100%">
					<div class="panel panel-default">
						<div class="panel-heading">Maintenance &gt; Service Counter Locator &gt; Edit</div>

						<div class="panel-body">
							<form action="${pageContext.request.contextPath}/admin/editCSC.do" class="form-horizontal" id="editCSCForm" method="post" name="editCSCForm">
								<div id="successMsg" style="display: none"></div>
								<div id="errorMsg" style="display: none" class="alert alert-danger" role="alert">
									<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> <span class="sr-only"></span>
								</div>
								<div class="form-group required">
									<p class="col-lg-5 control-label">
										Status <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<select class="form-control capital" id="selectStatus" name="selectStatus">
											<option value="">Please Select</option>
											<option value="A">Approved</option>
											<option value="D">Deleted</option>
											<option value="P">Pending</option>
											<option value="R">Rejected</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" type="reset">Cancel</button>
									</div>
								</div>
							</form>

							<div id=resultDiv style="display: none; margin-top: 10%">
								<form action="${pageContext.request.contextPath}/admin/updateMakerCSC.do" class="form-horizontal" id="updateEditCSCForm" method="post"
									name="updateEditCSCForm">
									<h4>
										Search Results : <b>${status}</b>
									</h4>
									<div class="form-group">
										<div id="messages" style="display: none;" class="alert alert-danger" role="alert"></div>

									</div>


									<table cellspacing="0" class="table table-striped table-bordered" id="tblSearch">
										<thead>
											<tr>
												<th>No.</th>

												<th>Region</th>

												<th>Area</th>

												<th>Address</th>

												<th>Longitude</th>

												<th>Latitude</th>

												<th>Opening Hour</th>

												<th>Status</th>

												<c:choose>
													<c:when test="${status =='Approved'}">
														<th>Date</th>
														<th>Action</th>
														<th>Select</th>
													</c:when>
													<c:when test="${status =='Deleted'}">														
														<th>Reason</th>
														<th>Date</th>
													</c:when>													
													<c:when test="${status =='Pending'}">
														<th>Category</th>
														<th>Reason</th>
														<th>Date</th>
													</c:when>
													<c:when test="${status =='Rejected'}">
														<th>Category</th>
														<th>Reason</th>
														<th>Date</th>
														<th>Select</th>
													</c:when>
												</c:choose>


											</tr>
										</thead>
										<tbody>
											<c:if test="${not empty c0List}">

												<c:choose>
													<%-- Approved --%>
													<c:when test="${status =='Approved'}">
														<c:forEach items="${c0List}" var="item" varStatus="loop">
															<tr>
																<td>${loop.index+1}</td>
																<td><input class="form-control" id="total${item.id}" name="total${item.id}" type="hidden" value="${loop.index+1}"> <select
																	class="form-control capital" id="selectRegion${item.id}" onchange="changeSelectRegion(${item.id},this.value);"
																	name="selectRegion${item.id}">
																		<c:forEach items="${regionList}" var="itemRegion">
																			<option value="${itemRegion}" ${not empty item.custServLoc.region && item.custServLoc.region == itemRegion ? 'selected' : ''}>${fn:replace(itemRegion, "_", " ")}</option>
																		</c:forEach>
																</select></td>

																<td>
																	<div id="stationDiv${item.id}" class="col-lg-3">
																		<select class="form-control capital" id="selectStation${item.id}" name="selectStation${item.id}">
																			<option value="${item.custServLoc.id}">${item.custServLoc.station}</option>
																		</select>
																	</div>
																</td>

																<td>
																	<p>
																		<input class="form-control" id="add1${item.id}" maxlength="200" name="add1${item.id}" placeholder="Address Line1" type="text"
																			value="${item.addressLine1}">
																	</p>
																	<p>
																		<input class="form-control" id="add2${item.id}" maxlength="200" name="add2${item.id}" placeholder="Address Line2" type="text"
																			value="${item.addressLine2}">
																	</p>
																	<p>
																		<input class="form-control" id="add3${item.id}" maxlength="200" name="add3${item.id}" placeholder="Address Line3" type="text"
																			value="${item.addressLine3}">
																	</p>
																	<p>
																		<input class="form-control" id="postcode${item.id}" maxlength="5" name="postcode${item.id}" placeholder="Postcode" type="text"
																			value="${item.postcode}">
																	</p>
																	<p>
																		<input class="form-control" id="city${item.id}" maxlength="200" name="city${item.id}" placeholder="City" type="text"
																			value="${item.city}">
																	</p>
																</td>

																<td class="col-md-1"><input class="form-control" id="longitude${item.id}" maxlength="12" name="longitude${item.id}"
																	placeholder="Longitude" type="text" value="${item.longitude}"></td>

																<td><input class="form-control" id="latitude${item.id}" maxlength="12" name="latitude${item.id}" placeholder="Latitude"
																	type="text" value="${item.latitude}"></td>

																<td><textarea class="form-control" cols="23" id="openingHr${item.id}" name="openingHr${item.id}" maxlength="200" rows="5">${item.openingHour}</textarea></td>

																<td>${status}<input type="hidden" id="status${item.id}" name="status${item.id}" value="${item.status}"></td>

																<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.updatedDatetime}"
																		pattern="dd/MM/YYYY hh:mm:ss a" /></td>

																<td>
																	<div class="radio" style="width: 80px">
																		<label><input type="radio" name="actionStatus${item.id}" value="edited" onclick="onClickRadio('${item.id}')"> EDITED</label>
																	</div>
																	<div class="radio">
																		<label><input type="radio" name="actionStatus${item.id}" value="deleted" onclick="onClickRadio('${item.id}')">
																			DELETED</label>
																	</div>

																	<div>Reason:</div>
																	<div id="reasonDiv${item.id}">
																		<textarea class="form-control" cols="23" id="reason${item.id}" name="reason${item.id}" maxlength="200" rows="10"></textarea>
																	</div>
																</td>

																<td><input type="checkbox" class="call-checkbox" value="${item.id}" onclick="onChangeCheckbox (this,${loop.index+1})" /></td>
															</tr>
														</c:forEach>
													</c:when>
													<%-- #Approved --%>
													<%-- Deleted --%>
													<c:when test="${status =='Deleted'}">
														<c:forEach items="${c0List}" var="item" varStatus="loop">
															<tr>
																<td>${loop.index+1}</td>

																<td>${fn:replace(item.custServLoc.region, "_", " ")}</td>

																<td>${item.custServLoc.station}</td>

																<td>
																	<p>${item.addressLine1}</p>
																	<p>${item.addressLine2}</p>
																	<p>${item.addressLine3}</p>
																	<p>${item.postcode}</p>
																	<p>${item.city}</p>
																</td>

																<td class="col-md-1">${item.longitude}</td>

																<td>${item.latitude}</td>

																<td><textarea readonly="readonly" class="form-control" cols="23" rows="5">${item.openingHour}</textarea></td>

																<td>${status}</td>
																
																<td>${item.rejectReason}</td>

																<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.updatedDatetime}"
																		pattern="dd/MM/YYYY hh:mm:ss a" /></td>
															</tr>
														</c:forEach>
													</c:when>
													<%-- #deleted --%>													
													<%-- pending --%>
													<c:when test="${fn:containsIgnoreCase(status, 'Pending')}">
														<c:forEach items="${c0List}" var="item" varStatus="loop">
															<tr>
																<td>${loop.index+1}</td>

																<td>${fn:replace(item.custServLoc.region, "_", " ")}</td>

																<td>${item.custServLoc.station}</td>

																<td>
																	<p>${item.addressLine1}</p>
																	<p>${item.addressLine2}</p>
																	<p>${item.addressLine3}</p>
																	<p>${item.postcode}</p>
																	<p>${item.city}</p>
																</td>

																<td class="col-md-1">${item.longitude}</td>

																<td>${item.latitude}</td>

																<td><textarea readonly="readonly" class="form-control" cols="23" rows="5">${item.openingHour}</textarea></td>

																<td>${status}</td>
																
																<td>${item.category}</td>
																
																<td>${item.rejectReason}</td>

																<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.updatedDatetime}"
																		pattern="dd/MM/YYYY hh:mm:ss a" /></td>
															</tr>
														</c:forEach>
													</c:when>
													<%-- #pending --%>
													<%-- rejected --%>
													<c:when test="${fn:containsIgnoreCase(status, 'Rejected')}">
														<c:forEach items="${c0List}" var="item" varStatus="loop">
															<tr>
																<td>${loop.index+1}</td>
																<td><input class="form-control" id="total${item.id}" name="total${item.id}" type="hidden" value="${loop.index+1}"> <select
																	class="form-control capital" id="selectRegion${item.id}" onchange="changeSelectRegion(${item.id},this.value);"
																	name="selectRegion${item.id}">
																		<c:forEach items="${regionList}" var="itemRegion">
																			<option value="${itemRegion}" ${not empty item.custServLoc.region && item.custServLoc.region == itemRegion ? 'selected' : ''}>${fn:replace(itemRegion, "_", " ")}</option>
																		</c:forEach>
																</select></td>

																<td>
																	<div id="stationDiv${item.id}" class="col-lg-3">
																		<select class="form-control capital" id="selectStation${item.id}" name="selectStation${item.id}">
																			<option value="${item.custServLoc.id}">${item.custServLoc.station}</option>
																		</select>
																	</div>
																</td>

																<td>
																	<p>
																		<input class="form-control" id="add1${item.id}" maxlength="200" name="add1${item.id}" placeholder="Address Line1" type="text"
																			value="${item.addressLine1}">
																	</p>
																	<p>
																		<input class="form-control" id="add2${item.id}" maxlength="200" name="add2${item.id}" placeholder="Address Line2" type="text"
																			value="${item.addressLine2}">
																	</p>
																	<p>
																		<input class="form-control" id="add3${item.id}" maxlength="200" name="add3${item.id}" placeholder="Address Line3" type="text"
																			value="${item.addressLine3}">
																	</p>
																	<p>
																		<input class="form-control" id="postcode${item.id}" maxlength="5" name="postcode${item.id}" placeholder="Postcode" type="text"
																			value="${item.postcode}">
																	</p>
																	<p>
																		<input class="form-control" id="city${item.id}" maxlength="200" name="city${item.id}" placeholder="City" type="text"
																			value="${item.city}">
																	</p>
																</td>

																<td class="col-md-1"><input class="form-control" id="longitude${item.id}" maxlength="12" name="longitude${item.id}"
																	placeholder="Longitude" type="text" value="${item.longitude}"></td>

																<td><input class="form-control" id="latitude${item.id}" maxlength="12" name="latitude${item.id}" placeholder="Latitude"
																	type="text" value="${item.latitude}"></td>

																<td><textarea class="form-control" cols="23" id="openingHr${item.id}" name="openingHr${item.id}" maxlength="200" rows="5">${item.openingHour}</textarea></td>

																<td>${status}<input type="hidden" id="status${item.id}" name="status${item.id}" value="${item.status}"></td>
																
																<td>${item.category}</td>
																
																<td>${item.rejectReason}</td>
																
																<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.updatedDatetime}"
																		pattern="dd/MM/YYYY hh:mm:ss a" /></td>
																		
																<td><input type="checkbox" class="call-checkbox" value="${item.id}" onclick="onChangeCheckbox (this,${loop.index+1})" /></td>
															</tr>
														</c:forEach>
													</c:when>
													<%-- #rejected --%>

												</c:choose>
											</c:if>
											<!-- #not empty c0List -->
										</tbody>
									</table>
									<br>
									<c:choose>
										<c:when test="${fn:containsIgnoreCase(status, 'Deleted') || fn:containsIgnoreCase(status, 'Pending')}">											
										</c:when>
										<c:otherwise>
											<div class="form-group">
												<div class="col-lg-5 col-lg-offset-3 text-right">
													<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
													<button id="submitBtn" class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal">Update</button>
													<button class="btn btn-warning" id="cancelBtn2" type="reset">Cancel</button>
												</div>
											</div>
										</c:otherwise>
									</c:choose>



									<!-- Add Confirmation Modal -->
									<div class="modal" id="modal" tabindex="-1">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button class="close" data-dismiss="modal" type="button">
														<span>&times;</span><span class="sr-only">Close</span>
													</button>
													<h2 class="modal-title">Confirmation</h2>
												</div>
												<div class="modal-body" id="add">Are you sure you want to update service counter locator?</div>
												<div class="modal-footer">
													<button class="btn btn-primary" id="add" type="button">Update</button>
													<button class="btn btn-warning" data-dismiss="modal" type="button">Close</button>
												</div>
											</div>
										</div>
									</div>
									<!-- End Confirmation Modal -->
								</form>
								<!-- #form -->
							</div>
							<!-- /.resultList -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>

	<script type="text/javascript">
    
    
    function changeSelectRegion(id, selectedText) {
        if (selectedText != "") {
            //console.log("selectedText>" + selectedText);
            var json = {
                "region": selectedText
            };
            var token = $("meta[name='_csrf']").attr("content");
            var header = $("meta[name='_csrf_header']").attr("content");
            $.ajax({
                url: "${pageContext.request.contextPath}/admin/getStation.do?",
                type: "POST",
                data: JSON.stringify({
                    region: selectedText
                }),
                cache: false,
                beforeSend: function(xhr) {
                    xhr.setRequestHeader(header, token);
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                    var html = '<select class="form-control capital" id="selectStation' + id + '" name="selectStation' + id + '">';
                    $.each(data, function(index, currEmp) {
                        //console.log(currEmp.id+","+currEmp.station);
                        html += '<option value="' + currEmp.id + '">' + currEmp.station + '</option>';
                    });
                    html += '</select>';
                    $('#stationDiv' + id).html(html);
                },
                failure: function(data) {}
            });
        }
    }
    
  
 // Handle click on checkbox
    function onChangeCheckbox(checkbox, no) {
        if (checkbox.checked) {
            document.getElementById("submitBtn").disabled = false;
        } else {
            error = "";
            $('#messages').hide();
        }
    }
   
 	var errorNo = "";
    function validateForm(num, item) {
        if ($.trim($('#add1' + item).val()) == '') {
            errorNo += "<p>Error : Item " + num+" : Address is required</p>";
        }
        if ($.trim($('#postcode' + item).val()) == '') {
        	
            errorNo += "<p>Error : Item " + num+" : Postcode is required</p>";
        }
        if ($.trim($('#city' + item).val()) == '') {
            errorNo += "<p>Error : Item " + num+" : City is required</p>";
        }
        if ($('#longitude' + item).val() == '') {
        	errorNo += "<p>Error : Item " + num+" : Longitude is required</p>";
        }
        if($('#longitude' + item).val()!='' && ($('#longitude' + item).val() < -180) || ($('#longitude' + item).val() > 180)){
        	errorNo += "<p>Error : Item " + num+" : The longitude must be between -180.0 and 180.0</p>";
        }
        if ($('#latitude' + item).val() == '') {
            errorNo += "<p>Error : Item " + num+" : Latitude is required</p>";
        }
        if($('#latitude' + item).val()!='' && ($('#latitude' + item).val() < -90) || ($('#latitude' + item).val() > 90)){
        	errorNo += "<p>Error : Item " + num+" : The latitude must be between -90.0 and 90.0</p>";
        }
        if($('#openingHr' + item).val()==''){
        	errorNo += "<p>Error : Item " + num+" : Opening Hour is required</p>";
        }
        
        if($('#status' + item).val()!=null && $('#status' + item).val()=='A'){
        	 var radio = $("input[type='radio'][name='actionStatus"+ item+"']:checked").val();
           if(radio== null | radio ==''){
           	errorNo += "<p>Error : Item " + num+" : Please click Edit or Delete</p>";
           }if ($.trim($('#reason' + item).val()) == '') {
                 errorNo += "<p>Error : Item " + num+" : Reason is required</p>";
            } 
        }
        
    }
    
    function onClickRadio(item) {
   	 	var radio = $("input[type='radio'][name='actionStatus"+ item+"']:checked").val();    	
   	 	$('#reason'+item).val('');
  	 }
    
    
    $(document).ready(function() {    	    	
    	 
        if(${c0List!=null && c0List.size()>=0}){	
            $('#resultDiv').show();
           var table= $('#tblSearch').DataTable({
      		//  responsive: true,   
      		bFilter: false,
          	bSort: false,
          	lengthMenu: [[50, 100], [50, 100]]
        	});
           
           <c:forEach var="item" items="${c0List}" varStatus="loop">
           $('#postcode'+${item.id}).keypress(function (e) {
             //if the letter is not digit then display error and don't type anything
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                       return false;
            }
           });
           
           $('#latitude'+${item.id}).keypress(function (e) {
             //if the letter is not digit then display error and don't type anything
             
             if (e.which != 45 & e.which != 46 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                       return false;
            }
           });
           
           $('#longitude'+${item.id}).keypress(function (e) {
             //if the letter is not digit then display error and don't type anything
             
             if (e.which != 45 & e.which != 46 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                       return false;
            }
           });
           </c:forEach>
        }
    	
      
        $("button#add").click(function() {
		     $('#modal').modal('hide');
        	 var rows_selected = new Array();
        	errorNo = "";
            //console.log("submitBtn was clicked");
            var oTable = $('#tblSearch').dataTable();
            var rowcollection = oTable.$(".call-checkbox:checked", {
                "page": "all"
            });  
            if (rowcollection.length == 0) {
            	window.scrollTo(0,0);
            	$('#errorMsg').html("Please select the checkbox to update.").show();
                return false;
            } 
            	$('#errorMsg').hide();
                rowcollection.each(function(index, elem) {
                
                    var item = $(elem).val();
                    var rowIndex = $('#tblSearch').find('tbody tr:eq(' + item + ')').get(0);
                    //var aData = oTable.fnGetData(rowIndex);
                    //console.log(aData[0]);
                    	var num = $('#total' + item).val();
                	//console.log("no>>"+num);
                    var selectStationIndex = $("#selectStation" + item + " option:selected").val();
                    var selectRegionIndex = $('#selectRegion' + item).val();
                    var stationName = $("#selectStation" + item + " option:selected").text();
                    var selectRegionIndex = $('#selectRegion' + item).val();
                    var add1Index = $('#add1' + item).val();
                    var add2Index = $('#add2' + item).val();
                    var add3Index = $('#add3' + item).val();
                    var postcodeIndex = $('#postcode' + item).val();
                    var cityIndex = $('#city' + item).val();
                    var longitudeIndex = $('#longitude' + item).val();
                    var latitudeIndex = $('#latitude' + item).val();
                    var openingHrIndex = $('#openingHr' + item).val();   
                    var statusIndex = $('#status' + item).val(); 
                    var actionStatus = $("input[type='radio'][name='actionStatus"+ item+"']:checked").val();
                    var reasonIndex = $('#reason' + item).val(); 
                    //end
                    console.log('actionStatus>'+actionStatus);
                    console.log('reasonIndex>'+reasonIndex);
                    rows_selected.push({
                            id: item,
                            regionName: selectRegionIndex,
                            stationId: selectStationIndex,
                            stationName : stationName,
                            addressLine1: add1Index,
                            addressLine2: add2Index,
                            addressLine3: add3Index,
                            postcode: postcodeIndex,
                            city: cityIndex,
                            longitude: longitudeIndex,
                            latitude: latitudeIndex,
                            openingHour: openingHrIndex,
                            status :statusIndex,
                            actionStatus:actionStatus,
                            reason: reasonIndex
                        })	
                        validateForm(num, item);
                   
                });
                //console.log("errorNo="+errorNo);
                //console.log("errorNo.length="+errorNo.length);
                if(errorNo.length>0){                	
                    $('#messages').html(errorNo).show();
                    window.scrollTo(0,250);
                	return false;
                }
                	//console.log("validate success:");
                	 var token = $("meta[name='_csrf']").attr("content");
                     var header = $("meta[name='_csrf_header']").attr("content");
                     $.ajax({
     		            url: "${pageContext.request.contextPath}/admin/updateMakerCSC.do",
                         type: "POST",
                         dataType: "json",
                         data: JSON.stringify(rows_selected),                   
                         beforeSend: function(xhr) {
                             xhr.setRequestHeader(header, token);
                             xhr.setRequestHeader("Accept", "application/json");
                             xhr.setRequestHeader("Content-Type", "application/json");
                             sebApp.showIndicator();
                         },
                         success: function(data) {
                             $('#successMsg').html(data.success).show(); 
                             window.scrollTo(0,5);
                             rows_selected = new Array();
                             $('#resultDiv').hide();
                             
                         },
                         failure: function(data) {alert("Fail To Connect");},
                         complete: function(){sebApp.hideIndicator();}
                     });
                
                return true;
        });
        
       
        
        $("#cancelBtn1").click(function() {
            $('#meditCSCForm').find('.has-error').removeClass("has-error");
            $('#meditCSCForm').find('.has-success').removeClass("has-success");
            $('#meditCSCForm').find('.form-control-feedback').removeClass('glyphicon-remove');
            $('#meditCSCForm').find('.form-control-feedback').removeClass('glyphicon-ok');
            $('.form-group').find('small.help-block').hide();
        });
     
        $('#editCSCForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
                selectStatus: {
                    validators: {
                        callback: {
                            message: 'Please Select Status',
                            callback: function(value, validator, $field) {
                                var options = validator.getFieldElements('selectStatus').val();
                                return (options != null && options.length > 0);
                            }
                        }
                    }
                }
            }
        });
    });
    </script>
</body>
</html>