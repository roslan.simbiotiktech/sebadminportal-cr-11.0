package com.seb.admin.portal.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.seb.admin.portal.dao.LoginDAO;
import com.seb.admin.portal.model.AdminUser;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@PersistenceContext(unitName = "seb", type = PersistenceContextType.EXTENDED)
	private EntityManager em;

	@Autowired
	private LoginDAO loginDAO;
	@Autowired
	private Assembler assembler;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username)throws UsernameNotFoundException, DataAccessException {
		AdminUser userEntity = loginDAO.getUserDetails (username);

		/*System.out.println("UserDetailsDAOImpl.loadUserByUsername()");
		System.out.println("username::"+username);		
		System.out.println("getId::"+userEntity.getId());
		System.out.println("getLoginID::"+userEntity.getLoginId());
		System.out.println("getPassword::"+userEntity.getPassword());
		System.out.println("getRole::"+userEntity.getRole());
		System.out.println("getLastLoggedIn::"+userEntity.getLastLoggedIn());*/

		if (userEntity == null){
			throw new UsernameNotFoundException("user not found");
		}

		return assembler.buildUserFromUserEntity(userEntity);

	}
}
