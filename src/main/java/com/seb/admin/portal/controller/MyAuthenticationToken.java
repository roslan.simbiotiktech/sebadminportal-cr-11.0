package com.seb.admin.portal.controller;


import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.seb.admin.portal.model.AdminUser;

/**
 * This component extends the standard Spring class "UsernamePasswordAuthenticationToken" to allow the storing of additional data/details 
 * attached to authenticated user (for example, fullName, email..).
 * 
 * @author Huseyin OZVEREN
 */
public class MyAuthenticationToken extends UsernamePasswordAuthenticationToken{

	// ----------------------------------- PRIVATE ATTRIBUTES
	private AdminUser myUser = null;
	
	// ----------------------------------- CONSTRUCTOR
	public MyAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, AdminUser myUser){
		super(principal, credentials, authorities);
		this.myUser = myUser;
	}

	// ----------------------------------- GET/SET TERS
	public AdminUser getMyUser() {
		return myUser;
	}

	public void setMyUser(AdminUser myUser) {
		this.myUser = myUser;
	}
			
}