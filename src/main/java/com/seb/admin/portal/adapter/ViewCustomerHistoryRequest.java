package com.seb.admin.portal.adapter;

public class ViewCustomerHistoryRequest {
	
	private String dateFrom;
	private String dateTo;
	private String loginId;
	private String mobileNumber;
	private String contractAccNo;	
	
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getContractAccNo() {
		return contractAccNo;
	}
	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}
	@Override
	public String toString() {
		return "ViewCustomerHistoryRequest [dateFrom=" + dateFrom + ", dateTo=" + dateTo + ", loginId=" + loginId
				+ ", mobileNumber=" + mobileNumber + ", contractAccNo=" + contractAccNo + "]";
	}
	
	
	
	

}
