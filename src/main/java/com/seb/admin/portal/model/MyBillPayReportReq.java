package com.seb.admin.portal.model;

import java.util.Date;

public class MyBillPayReportReq implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	
	private Date dateFrom;
	private Date dateTo; 
	private String paymentMethod;
	private String sebRefStatus;
	private String contractAccNo;
	private String email;
	private String pymtRef;
	private String fpxTxnId;
	private String mbbRefNo;
	
	private int pageNumber;
	private int pageSize = 50;
	
	public MyBillPayReportReq(){
		
	}
	
	public MyBillPayReportReq(Date dateFrom, Date dateTo, String paymentMethod, String sebRefStatus, String contractAccNo, String email,
			String pymtRef, String fpxTxnId, String mbbRefNo, int pageNumber ) {
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.paymentMethod = paymentMethod;
		this.sebRefStatus = sebRefStatus;
		this.contractAccNo = contractAccNo;
		this.email = email;
		this.pymtRef = pymtRef;
		this.fpxTxnId = fpxTxnId;
		this.mbbRefNo = mbbRefNo;
		this.pageNumber = pageNumber;
	}
	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Date getDateTo() {
		return dateTo;
	}
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getContractAccNo() {
		return contractAccNo;
	}
	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPymtRef() {
		return pymtRef;
	}
	public void setPymtRef(String pymtRef) {
		this.pymtRef = pymtRef;
	}
	public String getFpxTxnId() {
		return fpxTxnId;
	}
	public void setFpxTxnId(String fpxTxnId) {
		this.fpxTxnId = fpxTxnId;
	}
	public String getMbbRefNo() {
		return mbbRefNo;
	}
	public void setMbbRefNo(String mbbRefNo) {
		this.mbbRefNo = mbbRefNo;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getSebRefStatus() {
		return sebRefStatus;
	}

	public void setSebRefStatus(String sebRefStatus) {
		this.sebRefStatus = sebRefStatus;
	}

	@Override
	public String toString() {
		return "MyBillPayReportReq [dateFrom=" + dateFrom + ", dateTo=" + dateTo + ", paymentMethod=" + paymentMethod
				+ ", sebRefStatus=" + sebRefStatus + ", contractAccNo=" + contractAccNo + ", email=" + email
				+ ", pymtRef=" + pymtRef + ", fpxTxnId=" + fpxTxnId + ", mbbRefNo=" + mbbRefNo + ", pageNumber="
				+ pageNumber + ", pageSize=" + pageSize + "]";
	}
	
	

	
	
	


}
