package com.seb.admin.portal.spg.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Pagination {
	
	@JsonProperty("paging")
	private PAGING merchant_id;
	
	@JsonProperty("max_page")
	private int max_page;
	
	@JsonProperty("total_records")
	private int total_records;

	public PAGING getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(PAGING merchant_id) {
		this.merchant_id = merchant_id;
	}

	public int getMax_page() {
		return max_page;
	}

	public void setMax_page(int max_page) {
		this.max_page = max_page;
	}

	public int getTotal_records() {
		return total_records;
	}

	public void setTotal_records(int total_records) {
		this.total_records = total_records;
	}

	@Override
	public String toString() {
		return "merchant_id:[" + merchant_id + "], max_page:[" + max_page + "], total_records:[" + total_records + "]";
	}
	
	
}
