package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the POWER_ALERT_TYPE database table.
 * 
 */
@Entity
@Table(name="POWER_ALERT_TYPE", schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="PowerAlertType.findAll", query="SELECT p FROM PowerAlertType p WHERE p.deletedFlag=0 ORDER BY p.name")
})
@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
public class PowerAlertType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(name="CREATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(name="NAME", length=255)
	private String name;

	//bi-directional many-to-one association to PowerAlert
	@OneToMany(mappedBy="powerAlertType")
	private List<PowerAlert> powerAlerts;

	public PowerAlertType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonManagedReference //for children table
	public List<PowerAlert> getPowerAlerts() {
		return this.powerAlerts;
	}

	public void setPowerAlerts(List<PowerAlert> powerAlerts) {
		this.powerAlerts = powerAlerts;
	}

	public PowerAlert addPowerAlert(PowerAlert powerAlert) {
		getPowerAlerts().add(powerAlert);
		powerAlert.setPowerAlertType(this);

		return powerAlert;
	}

	public PowerAlert removePowerAlert(PowerAlert powerAlert) {
		getPowerAlerts().remove(powerAlert);
		powerAlert.setPowerAlertType(null);

		return powerAlert;
	}

}