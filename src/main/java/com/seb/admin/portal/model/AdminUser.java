package com.seb.admin.portal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.seb.admin.portal.constant.ApplicationConstant;

/**
 * The persistent class for the ADMIN_USER database table.
 * 
 */
@Entity
@Table(name = "ADMIN_USER", schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="AdminUser.findAllLoginID", query="SELECT a.loginId FROM AdminUser a WHERE UPPER(a.status)='ACTIVE' AND a.deletedFlag=0 ORDER BY a.loginId"),
	@NamedQuery(name="AdminUser.findAllUser", query="SELECT a FROM AdminUser a WHERE a.deletedFlag=0 ORDER BY a.loginId"),
	@NamedQuery(name="AdminUser.getLoginUser", query="SELECT ss FROM AdminUser ss WHERE ss.loginId = :loginId AND UPPER(ss.status)='ACTIVE' AND ss.deletedFlag=0"),
	@NamedQuery(name="AdminUser.getSelectedUser", query="SELECT ss FROM AdminUser ss WHERE ss.loginId = :loginId  AND ss.deletedFlag=0"),
	@NamedQuery(name="AdminUser.checkOldPassword", query="SELECT ss FROM AdminUser ss WHERE ss.id = :id AND ss.password = :psw AND UPPER(ss.status)='ACTIVE' AND ss.deletedFlag=0"),
	@NamedQuery(name="AdminUser.checkExistUser", query="SELECT ss FROM AdminUser ss WHERE ss.loginId = :loginId  AND ss.deletedFlag=0"),
	@NamedQuery(name="AdminUser.findActiveUser", query="SELECT a FROM AdminUser a WHERE UPPER(a.status)='ACTIVE' AND a.deletedFlag=0 ORDER BY a.loginId"),

})
@JsonIgnoreProperties(value = { "handler", "hibernateLazyInitializer" })
public class AdminUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(name="CREATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(length=160)
	private String department;

	@Column(name="LAST_LOGGED_IN")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLoggedIn;

	@Column(name="LOGIN_ID", unique=true, nullable=false, length=160)
	private String loginId;

	@Column(name="NAME", length=160)
	private String name;

	@Column(name="PASSWORD", nullable=true, length=160)
	private String password;

	@Column(name="ROLE", length=160)
	private String role;

	@Column(name="STATUS", nullable=false, length=25)
	private String status= "ACTIVE";

	@Column(name="UPDATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp updatedDatetime;

	//bi-directional many-to-one association to AccessRight
	@OneToMany(mappedBy="adminUser")
	private List<AccessRight> accessRights;

	//bi-directional many-to-one association to AuditTrail
	@OneToMany(mappedBy="adminUser")
	private List<AuditTrail> auditTrail;

	public AdminUser() {
	}
	
	public AdminUser(Integer id, String loginId, String password, String role, Date lastLoggedIn, int deletedFlag, String status) {
		this.id = id;
		this.loginId = loginId;
		this.password = password;
		this.role = role;
		this.status = status;
		this.lastLoggedIn = lastLoggedIn;
		this.deletedFlag = deletedFlag;

	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Date getLastLoggedIn() {
		return lastLoggedIn;
	}

	public void setLastLoggedIn(Date lastLoggedIn) {
		this.lastLoggedIn = lastLoggedIn;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	@JsonManagedReference //for children table
	public List<AccessRight> getAccessRights() {
		return this.accessRights;
	}

	public void setAccessRights(List<AccessRight> accessRights) {
		this.accessRights = accessRights;
	}

	public AccessRight addAccessRight(AccessRight accessRight) {
		getAccessRights().add(accessRight);
		accessRight.setAdminUser(this);

		return accessRight;
	}

	public AccessRight removeAccessRight(AccessRight accessRight) {
		getAccessRights().remove(accessRight);
		accessRight.setAdminUser(null);

		return accessRight;
	}

	@JsonManagedReference //for children table
	public List<AuditTrail> getAuditTrail() {
		return this.auditTrail;
	}

	public void setAuditTrail(List<AuditTrail> auditTrail) {
		this.auditTrail = auditTrail;
	}

	public AuditTrail addAuditTrail(AuditTrail auditTrail) {
		getAuditTrail().add(auditTrail);
		auditTrail.setAdminUser(this);

		return auditTrail;
	}

	public AuditTrail removeAuditTrail(AuditTrail auditTrail) {
		getAuditTrail().remove(auditTrail);
		auditTrail.setAdminUser(null);

		return auditTrail;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accessRights == null) ? 0 : accessRights.hashCode());
		result = prime * result + ((auditTrail == null) ? 0 : auditTrail.hashCode());
		result = prime * result + ((createdDatetime == null) ? 0 : createdDatetime.hashCode());
		result = prime * result + deletedFlag;
		result = prime * result + ((department == null) ? 0 : department.hashCode());
		result = prime * result + id;
		result = prime * result + ((lastLoggedIn == null) ? 0 : lastLoggedIn.hashCode());
		result = prime * result + ((loginId == null) ? 0 : loginId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((updatedDatetime == null) ? 0 : updatedDatetime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdminUser other = (AdminUser) obj;
		if (accessRights == null) {
			if (other.accessRights != null)
				return false;
		} else if (!accessRights.equals(other.accessRights))
			return false;
		if (auditTrail == null) {
			if (other.auditTrail != null)
				return false;
		} else if (!auditTrail.equals(other.auditTrail))
			return false;
		if (createdDatetime == null) {
			if (other.createdDatetime != null)
				return false;
		} else if (!createdDatetime.equals(other.createdDatetime))
			return false;
		if (deletedFlag != other.deletedFlag)
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (id != other.id)
			return false;
		if (lastLoggedIn == null) {
			if (other.lastLoggedIn != null)
				return false;
		} else if (!lastLoggedIn.equals(other.lastLoggedIn))
			return false;
		if (loginId == null) {
			if (other.loginId != null)
				return false;
		} else if (!loginId.equals(other.loginId))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (updatedDatetime == null) {
			if (other.updatedDatetime != null)
				return false;
		} else if (!updatedDatetime.equals(other.updatedDatetime))
			return false;
		return true;
	}

	
	/*@Override
	public String toString() {
		return "AdminUser [id=" + id + ", createdDatetime=" + createdDatetime + ", deletedFlag=" + deletedFlag
				+ ", department=" + department + ", lastLoggedIn=" + lastLoggedIn + ", loginId=" + loginId + ", name="
				+ name + ", role=" + role + ", status=" + status + ", updatedDatetime="
				+ updatedDatetime + ", accessRights=" + accessRights + ", auditTrail=" + auditTrail + "]";
	}*/

	
	
	

}