<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="" name="description">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>

</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->

		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12" style="width: inherit; min-width: 100%">
					<div class="panel panel-default">
						<div class="panel-heading" style="font-weight: bold">Maintenance &gt; Push Notification &gt; Edit</div>

						<div class="panel-body">
							<form action="${pageContext.request.contextPath}/admin/editPushNotification.do" class="form-horizontal" id="searchForm" method="post"
								name="searchForm">
								${successMsg}${errorMsg}
								<div id="successMsg" style="display: none"></div>
								<div id="errorMsg" style="display: none"></div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Start Date <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<div class='input-group date' id='startDate'>
											<input class="form-control datepicker" readonly="readonly" id="instartDate" name="instartDate" type='text'> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										End Date <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<div class='input-group date' id='endDate'>
											<input class="form-control datepicker" readonly="readonly" name="inendDate" onkeydown="return false;" type='text'> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Type <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<select class="form-control capital" id="selectType" name="selectType">
											<option value="">Please Select</option>
											<option value="PLAIN">Free Text</option>
											<option value="PROMO">Promo</option>
											<option value="POWER_ALERT">Outage Alert</option>
										</select>
									</div>
								</div>

								<div id="contentDiv" class="form-group">
									<p class="col-lg-5 control-label">
										Title <span style="color: red;">*</span> :
									</p>
									<div id="selectTitleDiv" class="col-lg-3">
										<select class="form-control capital" id="selectTitle" onchange="onChangeTitle()" name="selectTitle">
											<option value="">Please Select</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">Message :</p>

									<div class="col-lg-3">
										<textarea class="form-control" cols="23" id="message" maxlength="120" name="message" rows="10"></textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input id="displayType" name="displayType" type="hidden" value=""> <input id="displayTitle" name="displayTitle" type="hidden"
											value=""> <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm(this);" type="button">Cancel</button>
									</div>
								</div>
							</form>

							<div id="resultDiv" style="display: none; margin-top: 10%">
								<form action="${pageContext.request.contextPath}/admin/editPushNotification.do" class="form-horizontal" id="updatePushNotication"
									method="post" name="updatePushNotication">
									<h4>
										Search Results : Start Date :<b>${startDate}</b>, End Date :<b>${endDate}</b>, Type :<b>${displayType}</b> <br>Title :<b>${displayTitle}</b>,<br/>
										Message :<b>${message}</b>
									</h4>
									<div class="form-group">
										<div class="alert alert-danger" id="messages" style="display: none;"></div>
									</div>
									<table cellspacing="0" class="table table-striped table-bordered" id="tblSearch">
										<thead>
											<tr>
												<th>No.</th>
												<th>Push Date Time</th>
												<th>Type</th>
												<th>Title</th>
												<th>Status</th>
												<th>Message</th>
												<th>Select</th>
											</tr>
										</thead>
										<tbody>
											<c:if test="${not empty resultList}">
												<c:forEach items="${resultList}" var="item" varStatus="status">
													<fmt:formatDate var="dateNow" type="both" dateStyle="long" value="<%=new java.util.Date()%>" />
													<input type="text" id="currentDate${item.notificationId}" name="currentDate${item.notificationId}"
														value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="<%=new java.util.Date()%>" pattern="dd/MM/YYYY hh:mm a"/>"
														style="display: none;" />
													<c:choose>
														<c:when test="${item.status eq 'UNSENT'}">
															<tr>
																<td>${status.index+1}<input class="form-control" id="total${item.notificationId}" name="total${item.notificationId}"
																	type="hidden" value="${status.index+1}"></td>
																<td>
																	<div class='input-group date' id='pushDateTime${item.notificationId}'>
																		<input id="inpushDateTime${item.notificationId}" name="inpushDateTime${item.notificationId}" style="width: 150px"
																			class="form-control datepicker" readonly type='text' onkeydown="return false;"
																			value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.pushDatetime}" pattern="dd/MM/YYYY hh:mm a"/>">
																		<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																	</div>
																</td>
																<td><select class="form-control capital" id="listType${item.notificationId}" name="listType${item.notificationId}">
																		<option value="">Please Select</option>
																		<c:choose>
																			<c:when test="${fn:containsIgnoreCase(item.notificationType, 'PLAIN')}">
																				<option value="PLAIN" selected="selected">Free Text</option>
																				<option value="PROMO">Promo</option>
																				<option value="POWER_ALERT">Outage Alert</option>
																			</c:when>
																			<c:when test="${fn:containsIgnoreCase(item.notificationType, 'PROMO')}">
																				<option value="PLAIN">Free Text</option>
																				<option value="PROMO" selected="selected">Promo</option>
																				<option value="POWER_ALERT">Outage Alert</option>
																			</c:when>
																			<c:when test="${fn:containsIgnoreCase(item.notificationType, 'POWER_ALERT')}">
																				<option value="PLAIN">Free Text</option>
																				<option value="PROMO">Promo</option>
																				<option value="POWER_ALERT" selected="selected">Outage Alert</option>
																			</c:when>
																		</c:choose>
																</select></td>
																<td>
																	<div id="listTitleDiv${item.notificationId}" class="col-lg-3">
																		<select class="form-control" id="listTitle${item.notificationId}" name="listTitle${item.notificationId}" style="width: 150px" onchange="onChangeListTitle(${item.notificationId})">
																			<option value="">Please Select</option>
																			<c:forEach items="${dropdown}" var="itemTitle">
																				<option value="${itemTitle.value}" ${not empty itemTitle.value && itemTitle.value == item.associatedId ? 'selected' : ''}>${itemTitle.text}</option>
																			</c:forEach>

																		</select>
																	</div>
																</td>
																<td>${item.status}</td>
																<td><textarea class="form-control" maxlength="120" cols="40" rows="5" id="message${item.notificationId}"
																		name="message${item.notificationId}">${item.text}</textarea></td>
																<td><input type="checkbox" class="call-checkbox" value="${item.notificationId}"
																	onclick="onChangeCheckbox (this,${status.index+1})" /></td>
															</tr>
														</c:when>
														<c:when test="${item.status eq 'SENT' || item.status eq 'SENDING'}">
															<tr>
																<td>${status.index+1}</td>
																<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.pushDatetime}" pattern="dd/MM/YYYY hh:mm a" />
																</td>
																<td>${displayType}</td>
																<td>${displayTitle}</td>
																<td>${item.status}</td>
																<td><c:out value="${item.text}"  escapeXml="true"/></td>
																<td><input type="checkbox" disabled="disabled" class="call-checkbox" value="${item.notificationId}"
																	onclick="onChangeCheckbox (this,${status.index+1})" /></td>
															</tr>
														</c:when>
													</c:choose>
												</c:forEach>
												
												
											</c:if>
										</tbody>
									</table>
									<div class="form-group">
										<div class="col-lg-5 col-lg-offset-3 text-right">
												<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> 
												<input type="hidden" id="listStartDate" value="${startDate }"  />
												<input type="hidden" id="listEndDate" value="${endDate}"  />
											<button id="submitBtn" class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal">Update</button>
											<button class="btn btn-warning" id="cancelBtn2" type="reset">Cancel</button>
										</div>
									</div>
									<!-- Add Confirmation Modal -->
									<div class="modal" id="modal" tabindex="-1">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button class="close" data-dismiss="modal" type="button">
														<span>&times;</span><span class="sr-only">Close</span>
													</button>
													<h2 class="modal-title">Confirmation</h2>
												</div>
												<div class="modal-body" id="add">Are you sure you want to update push notification?</div>
												<div class="modal-footer">
													<button class="btn btn-primary" id="add" type="button">Update</button>
													<button class="btn btn-warning" data-dismiss="modal" type="button">Close</button>
												</div>
											</div>
										</div>
									</div>
									<!-- End Confirmation Modal -->
								</form>
								<!-- #viewATForm -->
							</div>
							<!-- #resultDiv -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
    function resetSS() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#searchForm').find('.has-success').removeClass("has-success");
    	$('#searchForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#searchForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();    	
    }
    
    function resetForm() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#searchForm').find('.has-success').removeClass("has-success");
    	$('#searchForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#searchForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();
    	$('#searchForm').formValidation('resetForm', true);
    	setTimeout(function() {
    		$('#inpushDateTime').val(moment(new Date()).format('DD/MM/YYYY hh:mm A'));
    	}, 50);
    	return false;
    }
 // Handle click on checkbox
	function onChangeCheckbox(checkbox, no) {
		if (checkbox.checked) {
			document.getElementById("submitBtn").disabled = false;
		} else {
			error = "";
			$('#messages').hide();
		}
	}
     
     var errorNo = "";

     function validateForm(num, item) {
     	//console.log('validateForm');
     	
     	//console.log('validateForm');
     	if($("#listType" + item + " option:selected").val()== ""){
     		errorNo += "<p>Error : Item " + num + " : Select Type is required</p>";
     	}
     	if($("#listType" + item + " option:selected").val()!= "PLAIN" && $("#listTitle" + item + " option:selected").val()==""){
     		errorNo += "<p>Error : Item " + num + " : Select Title is required</p>";
     	}
     	if ($.trim($('#message' + item).val()) == '') {
     		errorNo += "<p>Error : Item " + num + " : Message is required</p>";
     	}
     	if(moment($('#inpushDateTime' + item).val(), "DD/MM/YYYY hh:mm A").isBefore(moment($('#currentDate' + item).val(), "DD/MM/YYYY hh:mm A"))){
     		errorNo += "<p>Error : Item " + num + " : Push Notification Date Time must later than current date.</p>";     			
		}  
     }
     
     function onChangeTitle() {
    	    $('#displayTitle').val($('#selectTitle option:selected').text());
    	    //console.log($('#selectTitle option:selected').val());
   			}
     
     function onChangeListTitle(id) {
 	    $('#selectTitle option:selected').text();
 	    //console.log('onChangeListTitle:text>'+$('#listTitle'+id+' option:selected').text());
 	   	//console.log('onChangeListTitle:value>'+$('#listTitle'+id+' option:selected').val());
		}
     
     function convertToEntities(tstr) {
         var bstr = "";
         for (i = 0; i < tstr.length; ++i) {
             if (tstr.charCodeAt(i) > 127) {
                 bstr += "&#" + tstr.charCodeAt(i) + ";";
             } else {
                 bstr += tstr.charAt(i);
             }
         }
         return bstr;
     }
     
     
    
     	$(document).ready(function() {
     		if (${resultList != null && resultList.size() >= 0}) {
     	     	$('#resultDiv').show();
     	     	var table = $('#tblSearch').DataTable({
     	     		//  responsive: true,   
     	     		bFilter: false,
     	     		bSort: false,
     	     		lengthMenu: [
     	     			[50, 100],
     	     			[50, 100]
     	     		]
     	     	});
     	     } 
     		
     		<c:if test="${not empty resultList}">
       		<c:forEach items="${resultList}" var="item" varStatus="loop">
	        	 $('#pushDateTime' + ${item.notificationId}).datetimepicker({
	        	 	format: 'DD/MM/YYYY hh:mm A',
	        	 	defaultDate: new Date(),
	        	 	ignoreReadonly: true
	        	 });
	        	 
	        	 $('#listType'+ ${item.notificationId}).change(function() {
	       			var selectedValue = $(this).find("option:selected").val();
	       			var selectedText = $(this).find("option:selected").text();
	       			if (selectedValue != "" && selectedText.toLowerCase().indexOf('text') <= -1) {
	       				var token = $("meta[name='_csrf']").attr("content");
	       				var header = $("meta[name='_csrf_header']").attr("content");
	       				//console.log('start date :'+$('#listStartDate').val());
	       				//console.log('end date :'+$('#listEndDate').val());
	       				$.ajax({
	       					url: "${pageContext.request.contextPath}/admin/getSubPushNotifEditTitle.do",
	       					type: "POST",
	       					data: JSON.stringify({
	       						dateFrom:$('#listStartDate').val(),
	       						dateTo:$('#listEndDate').val(),
	       						text: selectedText,
	       						value: selectedValue
	       					}),
	       					beforeSend: function(xhr) {
	       						xhr.setRequestHeader(header, token);
	       						xhr.setRequestHeader("Accept", "application/json");
	       						xhr.setRequestHeader("Content-Type", "application/json");
	       						 sebApp.showIndicator();
	       					},
	       					success: function(data) {
	       						var html = '<select class="form-control capital" id="listTitle${item.notificationId}" name="listTitle${item.notificationId}" onchange="onChangeListTitle(${item.notificationId})" style="width: 150px"><option value="">Please Select</option>';
	       						$.each(data, function(index, currEmp) {
	       							//console.log( currEmp.text);
	       							html += '<option value="' + currEmp.value + '">' + currEmp.text + '</option>';
	       						});
	       						html += '</select>';
	       						$('#listTitleDiv${item.notificationId}').html(html);
	       					},
	       					failure: function(data) {},
	       					complete: function(){sebApp.hideIndicator();}
	       				});
	       			} else {
	       				var html = '<select class="form-control capital" id="listTitle${item.notificationId}" name="listTitle${item.notificationId}" onchange="onChangeListTitle(${item.notificationId})" style="width: 150px"><option value="">Please Select</option>';
	       				$('#listTitleDiv${item.notificationId}').html(html);
	       			}
	       		});
	        	 
	       </c:forEach>
           </c:if> 
    		var today = new Date();
    		var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
    		$('#startDate').datetimepicker({
    			format: 'DD/MM/YYYY',
    			defaultDate: new Date(),
    			ignoreReadonly: true
    		});
    		$('#endDate').datetimepicker({
    			format: 'DD/MM/YYYY',
    			defaultDate: tomorrow,
    			ignoreReadonly: true
    		});
    		
    		$('#selectType').change(function() {
    			var selectedValue = $(this).find("option:selected").val();
    			var selectedText = $(this).find("option:selected").text();
    			$('#displayType').val(selectedText);
    			if (selectedValue != "" && selectedText.toLowerCase().indexOf('text') <= -1) {
    				var token = $("meta[name='_csrf']").attr("content");
    				var header = $("meta[name='_csrf_header']").attr("content");
    				$.ajax({
    					url: "${pageContext.request.contextPath}/admin/getPushNotifEditTitle.do",
    					type: "POST",
    					data: JSON.stringify({
    						text: selectedText,
    						value: selectedValue
    					}),
    					beforeSend: function(xhr) {
    						xhr.setRequestHeader(header, token);
    						xhr.setRequestHeader("Accept", "application/json");
    						xhr.setRequestHeader("Content-Type", "application/json");
    						 sebApp.showIndicator();
    					},
    					success: function(data) {
    						var html = '<select class="form-control capital" id="selectTitle" name="selectTitle" onchange="onChangeTitle()"><option value="">Please Select</option>';
    						$.each(data, function(index, currEmp) {
    							//console.log( currEmp.text);
    							html += '<option value="' + currEmp.value + '">' + currEmp.text + '</option>';
    						});
    						html += '</select>';
    						$('#selectTitleDiv').html(html);
    					},
    					failure: function(data) {},
    					complete: function(){sebApp.hideIndicator();}
    				});
    			} else {
    				var html = '<select class="form-control capital" id="selectTitle" name="selectTitle"><option value="">Please Select</option>';
    				$('#selectTitleDiv').html(html);
    			}
    		});
    		
    		
    		
    		 $("button#add").click(function() {
    		     	$('#modal').modal('hide');
    		    	 var rows_selected = new Array();
    		      		errorNo = "";
    		          //console.log("submitBtn was clicked");
    		          var oTable = $('#tblSearch').dataTable();
    		          var rowcollection = oTable.$(".call-checkbox:checked", {
    		              "page": "all"
    		          });  
    		          if (rowcollection.length == 0) {
    		          	window.scrollTo(0,0);
    		          	$('#errorMsg').html("<div id=\"errorMsg\" class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> <span class=\"sr-only\"></span> Please select the checkbox to update. </div>").show();
    		              return false;
    		          }
    		          $('#errorMsg').hide();
    		          rowcollection.each(function(index, elem) {
    		              var item = $(elem).val();
    		              var rowIndex = $('#tblSearch').find('tbody tr:eq(' + item + ')').get(0);
    		              var num = $('#total' + item).val();
    		              //console.log("no>>" + num);console.log("item>>" + item);
    		              var listType = $("#listType" + item + " option:selected").val();
    		              var listTitle = $("#listTitle" + item + " option:selected").val();
    		              var displayListTitle = $("#listTitle" + item + " option:selected").text();
    		              var displayTitle = $('#displayTitle').val();    	
    		              var displayType = $('#displayType').val();
    		              var message = $('#message' + item).val();    		              
    		              var inpushDateTime = $('#inpushDateTime' + item).val();
    		                     
    			         rows_selected.push({
    			                 id: item,
    			                 listType : listType,
    			                 listTitle :listTitle,
    			                 displayListTitle : displayListTitle,
    			                 message : message,    			                
    			                 inpushDateTime: inpushDateTime
    			        		})
    			        validateForm(num, item);

    		         })
    		         
    		         //console.log("errorNo.length="+errorNo.length);
    		          if(errorNo.length>0){                	
    		               $('#messages').html(errorNo).show();
    		               window.scrollTo(0,650);
    		           	return false;
    		           }
    		         
    		          //console.log("validate success:");
    		          var token = $("meta[name='_csrf']").attr("content");
    		          var header = $("meta[name='_csrf_header']").attr("content");
    		          
    		          $.ajax({
    		              url: "${pageContext.request.contextPath}/admin/updatePushNotification.do",
    		              type: "POST",
    		              dataType: "json",
    		              data: JSON.stringify(rows_selected),
    		              beforeSend: function(xhr) {
    		                  xhr.setRequestHeader(header, token);
    		                  xhr.setRequestHeader("Accept", "application/json");
    		                  xhr.setRequestHeader("Content-Type", "application/json");
    		                  sebApp.showIndicator();
    		              },
    		              success: function(data) {
    		            	  if(data.success!=null && data.success.length>0){
    		                      $('#successMsg').html(data.success).show();
    		                  }
    		                  else if(data.error!=null && data.error.length>0){   
    		                      $('#errorMsg').html(data.error).show();
    		                  } 
    		                  
    		                  window.scrollTo(0, 5);
    		                  rows_selected = new Array();
    		                  $('#resultDiv').hide();
    		              },
    		              failure: function(data) {
    		                  alert("Fail To Connect");
    		              },
    		              complete: function(){sebApp.hideIndicator();}
    		          });
    		          return true;
    		      });

    		 
           
    		$('#searchForm').formValidation({
    			 excluded: [':not(:visible)'],
    			framework: 'bootstrap',
    			icon: {
    				invalid: 'glyphicon glyphicon-remove',
    			},
    			fields: {
    				inpushDateTime: {
    					validators: {
    						date: {
    							format: 'DD/MM/YYYY hh:mm A',
    						},
    						callback: {
    							callback: function(value, validator, $field) {
    								if (value == '') {
    						    		$('#inpushDateTime').val(moment(new Date()).format('DD/MM/YYYY hh:mm A'));
    									return true;
    								}
    								return true;
    							}
    						}
    					}
    				},    				
    				selectType: {
    					validators: {
    						callback: {
    							message: 'Please select Type',
    							callback: function(value, validator, $field) {
    								var options = validator.getFieldElements('selectType').val();
    								return (options != null && options.length > 0);
    							}
    						}
    					}
    				},
    				selectTitle: {
    					validators: {    						
    						callback: {
    							message: 'Please select Title',
    							callback: function(value, validator, $field) {
    								// Get the selected options                           
    								var options = validator.getFieldElements('selectTitle').val();
    								return (options != null && options.length > 0);
    							}
    						}
    					}
    				}/* , 
    				message: {
    					validators: {
    						notEmpty: {
    							message: 'Message is required'
    						},
    						stringLength: {
    							min: 1,
    							max: 120,
    							message: 'Message must less than 120 characters'
    						}
    					}
    				} */
    			}
    		})
    		.off('success.form.fv')
            .on('status.field.fv', function(e, data) {
    			  //$('#searchForm').formValidation('revalidateField', 'selectTitle');
    	        if (data.field === 'selectTitle') {
    	        	//$('#searchForm').formValidation('revalidateField', 'selectTitle');
    	        }
    	    }); 
    	});
     

 </script>
</body>
</html>