<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">			
			 <li>
				<a class="sidebarlink" href="${pageContext.request.contextPath}/mainLogin.do"><img src="${pageContext.request.contextPath}/resources/images/sidebar/home.png" alt="" width="32px"  height="32px" />Home</a>
			</li> 
			<c:set var="menuTitle" scope="session" value=""/>
			<c:set var="LV2Menu" scope="session" value=""/>
			
			<c:forEach items="${sessionMenuList}" var="item" varStatus="status">	
				<c:if test="${item.mainMenuName ne menuTitle}">			
					<c:choose>
						<c:when test="${item.mainLevel eq '1' && item.hasChildLevel2 eq 'false'}">
						<li>
							<a class="sidebarlink" href="${pageContext.request.contextPath}${item.mainAccessUrl}"><img src="${pageContext.request.contextPath}/resources/images/sidebar/home.png" alt="" width="32px"  height="32px" />${item.mainMenuName}</a>
						</c:when>
						<c:when test="${item.mainLevel eq '1' && item.hasChildLevel2 eq 'true'}">
						<li>
							<a class="sidebarlink" href="${status.index+1 }"><img src="${pageContext.request.contextPath}/resources/images/sidebar/home.png" alt="" width="32px"  height="32px" />${item.mainMenuName}<span class="fa arrow"></span></a>
						</c:when>
					</c:choose>	
						
						<c:if  test="${item.hasChildLevel2 eq 'true'}">
							<ul class="nav nav-second-level">
							
									<c:forEach items="${item.level2Menu}" var="item2" varStatus="status">	
										<c:choose>
										
											<c:when test="${item.hasChildLevel3 eq 'false'}">
												<li><a class="sidebarlink"  href="${pageContext.request.contextPath}${item2.subAccessUrl}">${item2.subMenuName}</a></li>
											</c:when>											
											<c:when test="${item.hasChildLevel3 eq 'true'}">									
											<li><a class="sidebarlink" href="${status.index+1 }">${item2.subMenuName}<span class="fa arrow"></span></a>
												<ul class="nav nav-third-level">
													 <c:forEach items="${item.level3Menu}" var="item3" varStatus="status">	
													 	 <c:if  test="${item3.subParentModuleCode eq item2.subModuleCode}">							                      
				                        					<li><a class="sidebarlink" href="${pageContext.request.contextPath}${item3.subAccessUrl}">${item3.subMenuName}</a></li>
													 	</c:if>
													 </c:forEach> 
												</ul>						
											</li>											
											</c:when>
										</c:choose>	
										
									</c:forEach>
							
							</ul>							
						</c:if>
					
					<c:set var="menuTitle" scope="session" value="${item.mainMenuName}"/>
				</c:if>
				
				
			
			</c:forEach>
			
		</ul>
	</div>
</div>