<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<meta content="" name="description">
	<meta name="_csrf" content="${_csrf.token}" />
	<meta name="_csrf_header" content="${_csrf.headerName}" />
	<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top">
            <%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
        </nav>
        <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp"%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="font-weight: bold">
                            User Management &gt; Add User
                        </div>
                        <div class="panel-body">
                            <form id="addUserForm"  name="addUserForm" action="${pageContext.request.contextPath}/admin/addUser.do" class="form-horizontal" method="post" >
                                ${successMsg}${errorMsg}
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Name <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <input class="form-control" id="name" maxlength="160" name="name" placeholder="Name" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Login ID <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <input class="form-control" id="loginId" maxlength="160" name="loginId" placeholder="Login ID" type="text">
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <p class="col-lg-5 control-label">Department <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <input class="form-control" id="department" maxlength="160" name="department" placeholder="Department" type="text">
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Role <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <select class="form-control" id="selectRole" name="selectRole">
                                            <option value="">Please Select</option>
                                            <option value="ROLE_C">Checker</option>
                                            <option value="ROLE_M">Maker</option>
                                            <option value="ROLE_N">Normal</option>
                                        </select><input id="roleName" name="roleName" type="hidden" value="">
                                    </div>
                                </div>
                                <div id="moduleDiv" style="display: none;" class="form-group">
                                    <p class="col-lg-5 control-label">Module<span style="color: red;">*</span> :</p>
                                    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="checkbox">
                                                    <label><input name="moduleList" type="checkbox"> Training 1</label>
                                                </div>
                                                <!-- <div class="checkbox">
                                                    <label><input  name="moduleList[]" type="checkbox"> Training 2</label>
                                                </div>
                                                <div class="checkbox">
                                                    <label><input  name="moduleList[]" type="checkbox"> Training 3</label>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-lg-4 col-lg-offset-4 text-right">
                                        <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> <button class="btn btn-primary" data-target="#modal" data-toggle="modal" id="submitBtn" type="button">Add</button> <button class="btn btn-warning" id="cancelBtn1" type="reset">Cancel</button>
                                    </div>
                                </div><!-- Add Confirmation Modal -->
                                <div class="modal fade" id="modal" tabindex="-1">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button class="close" data-dismiss="modal" type="button"><span>&times;</span><span class="sr-only">Close</span></button>
                                                <h2 class="modal-title">Confirmation</h2>
                                            </div>
                                            <div class="modal-body" id="add">
                                                Are you sure you want to add new user?
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-primary" id="add" type="submit">Add</button> <button class="btn btn-warning" data-dismiss="modal" type="button">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- End Confirmation Modal -->
                            </form>
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /#page-wrapper -->
    </div>
    <script type="text/javascript">

    $(document).ready(function() {  
     $("#cancelBtn1").click(function(){
         $('#addUserForm').find('.has-error').removeClass("has-error");
            $('#addUserForm').find('.has-success').removeClass("has-success");
            $('#addUserForm').find('.form-control-feedback').removeClass('glyphicon-remove');
            $('#addUserForm').find('.form-control-feedback').removeClass('glyphicon-ok');
            $('.form-group').find('small.help-block').hide();     
            $('#addUserForm').formValidation('resetForm', true);
        });
     
     $('#selectRole').change(function() {
		    var selectedText = $(this).find("option:selected").val();
		    var roleName = $("#selectRole option:selected").text();
       	 	$('#roleName').val(roleName);
		    if (selectedText != "") {
		    	//console.log("selectedText>"+selectedText);	
		    	 var json = {
				            "role": selectedText
				        };
		    	 var token = $("meta[name='_csrf']").attr("content");
			        var header = $("meta[name='_csrf_header']").attr("content");
			        $.ajax({
			            url: "${pageContext.request.contextPath}/admin/getModulesList.do?",
			            type: "POST",
			            data: JSON.stringify({ role: selectedText }),			           
			            beforeSend: function(xhr) {
			                xhr.setRequestHeader(header, token);
			                xhr.setRequestHeader("Accept", "application/json");
			                xhr.setRequestHeader("Content-Type", "application/json");
			                sebApp.showIndicator();
			                
			            },
			            success: function(data) {
			            	var html = '<div id="moduleDiv" class="form-group"> <p class="col-lg-5 control-label">Module<span style="color: red;">*</span> :</p><div class="col-lg-7"> <div class="row"> <div class="col-md-8">';
			                  $.each(data, function(index, currEmp) {
			                	//console.log(currEmp.id+","+currEmp.name);
			                    //html += '<div class="checkbox"> <label><input name="moduleList" type="checkbox" value="' + currEmp.id +'#'+currEmp.moduleCode+'"> ' + currEmp.description + '</label> </div>';
			                    html += '<div class="checkbox"> <label><input name="moduleList" type="checkbox" value="' + currEmp.moduleCode +'">' + currEmp.description + '</label> </div>';

			                }); 
			                html += '</div></div></div></div>';
			                $('#moduleDiv').html(html);
			                $('#moduleDiv').show();
			                //console.log(html);
			            },
			            failure: function(data) {},
			            complete: function(e){ sebApp.hideIndicator(); }     
			        });
			        
		        /* $.ajax({
		            url: "${pageContext.request.contextPath}/admin/getModulesList.do?",
		            type: "POST",
		            data: JSON.stringify({ role: selectedText }),
		            contentType: "application/json", 
			        // cache: false,
		            beforeSend: function(xhr) {
		                xhr.setRequestHeader(header, token);
		                xhr.setRequestHeader("Accept", "application/json");
		                xhr.setRequestHeader("Content-Type", "application/json");		                
		            },
		            success: function(data) {
		            	var html = '<div id="moduleDiv" class="form-group"> <p class="col-lg-5 control-label">Module<span style="color: red;">*</span> :</p><div class="col-lg-7"> <div class="row"> <div class="col-md-8">';
		                  $.each(data, function(index, currEmp) {
		                	//console.log(currEmp.id+","+currEmp.name);
		                    //html += '<div class="checkbox"> <label><input name="moduleList" type="checkbox" value="' + currEmp.id +'#'+currEmp.moduleCode+'"> ' + currEmp.description + '</label> </div>';
		                    html += '<div class="checkbox"> <label><input name="moduleList" type="checkbox" value="' + currEmp.moduleCode +'">' + currEmp.description + '</label> </div>';

		                }); 
		                html += '</div></div></div></div>';
		                $('#moduleDiv').html(html);
		                $('#moduleDiv').show();
		                //console.log(html);
		            },
		            failure: function(data) {}		           
		        }); */
		    }else{
		    	   $('#moduleDiv').html('');
		    }
	});
     
     $('#addUserForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
                 name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is required'
                        },
                        stringLength: {                           
                            max: 160,
                            message: 'Name must less than 160 characters'
                        }
                    }
                }, 
                loginId: {
                    validators: {
                        notEmpty: {
                            message: 'Login ID is required'
                        },
                        stringLength: {                           
                            max: 160,
                            message: 'Login ID must less than 160 characters'
                        }
                    }
                }, 
                selectRole: {
                    validators: {
                        callback: {
                            message: 'Please Select Role',
                            callback: function(value, validator, $field) {
                            	// alert($('#selectOpenHrFrom').data('date') );
                                // Get the selected options
                                var options = validator.getFieldElements('selectRole').val();
                                return (options != null && options.length >1);
                            }
                        }
                    }
                },               
                'moduleList[]': {
                    validators: {
                        choice: {
                            min: 1,
                            message: 'Please choose at least 1 module'
                        }
                    }
                },
                loginId: {
                    validators: {
                        notEmpty: {
                            message: 'Login ID is required'
                        },
                        stringLength: {
                            max: 160,
                            message: 'Login ID must less than 160 characters'
                        }
                    }
                },
                /* department: {
                    validators: {
                        notEmpty: {
                            message: 'Department is required'
                        },
                        stringLength: {                           
                            max: 160,
                            message: 'Department must less than 160 characters'
                        }
                    }
                }, */
            }
        });
    });
    </script>
</body>
</html>