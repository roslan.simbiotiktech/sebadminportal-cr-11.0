package com.seb.admin.portal.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.ContactUs;
import com.seb.admin.portal.model.CustServiceCounter;
import com.seb.admin.portal.model.CustServiceCounterEdited;
import com.seb.admin.portal.model.CustServiceLocation;
import com.seb.admin.portal.model.Faq;
import com.seb.admin.portal.model.FaqCategory;
import com.seb.admin.portal.model.News;
import com.seb.admin.portal.model.PowerAlert;
import com.seb.admin.portal.model.PowerAlertType;
import com.seb.admin.portal.model.TagNotification;
import com.seb.admin.portal.model.TariffCategory;
import com.seb.admin.portal.model.TariffSetting;
import com.seb.admin.portal.util.StringUtil;

@Repository
public class MaintenanceDAOImpl implements MaintenanceDAO{
	@PersistenceContext(unitName = ApplicationConstant.UNIT_NAME , type = PersistenceContextType.EXTENDED)
	private EntityManager em;

	private static final Logger log = Logger.getLogger(MaintenanceDAOImpl.class);


	public List <String> findAllRegions() throws SQLException{		
		List<String> regions = null;
		regions = em.createNamedQuery("CustServiceLocation.findRegion",String.class).getResultList();
		return regions;
	}

	public List <CustServiceLocation> findAllStation(String region) throws SQLException{		
		List<CustServiceLocation> station = new  ArrayList<CustServiceLocation>();
		station = em.createNamedQuery("CustServiceLocation.findStation",CustServiceLocation.class).setParameter("region", region).getResultList();
		return station;
	}

	public List <CustServiceLocation> findAllLocation() throws SQLException{		
		List<CustServiceLocation> allLocation = null;
		allLocation = em.createNamedQuery("CustServiceLocation.findAll",CustServiceLocation.class).getResultList();
		return allLocation;
	}

	@Override
	public boolean deleteEditedCSC(int id) throws SQLException {				
		try{	
			CustServiceCounterEdited data = em.find(CustServiceCounterEdited.class, id);
			em.remove(data);
			em.getEntityManagerFactory().getCache().evictAll();
		}catch(NoResultException e){
			e.printStackTrace();
			return false;
		}	
		return true;
	}

	@Override
	public void saveEditedCSC(CustServiceCounterEdited counter) throws SQLException {
		try{		
			em.persist(counter);
			em.refresh(counter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public boolean updateEditedMakerCSC(CustServiceCounterEdited e0) throws SQLException {
		try{	
			CustServiceCounterEdited update = em.find(CustServiceCounterEdited.class, e0.getId());


			update.setEditedCustServiceLocation(e0.getEditedCustServiceLocation());
			update.setCustServiceLocationId(e0.getCustServiceLocationId());
			update.setAddressLine1(e0.getAddressLine1());
			update.setAddressLine2(e0.getAddressLine2());
			update.setAddressLine3(e0.getAddressLine3());
			update.setCity(e0.getCity());
			update.setPostcode(e0.getPostcode());
			update.setLatitude(e0.getLatitude());
			update.setLongitude(e0.getLongitude());
			update.setOpeningHour(e0.getOpeningHour());
			update.setAdminUserId(e0.getAdminUserId());
			update.setStatus(e0.getStatus());
			update.setRejectReason(e0.getRejectReason());

			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();
			return false;
		}	
		return true;
	}


	@Override
	public void saveCSC(CustServiceCounter counter) throws SQLException {
		try{		
			em.persist(counter);
			em.refresh(counter);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public List<CustServiceCounter> findCounterByStatus (String status, String category)throws SQLException {
		List<CustServiceCounter> c0 = new ArrayList<CustServiceCounter>();
		try{		
			c0 = em.createNamedQuery("CustServiceCounter.findCounterByStatus",CustServiceCounter.class)
					.setParameter("status", status.toUpperCase())
					.setParameter("category", category.toUpperCase())
					.getResultList();
		}catch (NoResultException ex) {
			// TODO: handle exception
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return c0;
	}
	
	@Override
	public List<CustServiceCounter> findMainLocatorByStatus (String status1, String status2)throws SQLException {
		List<CustServiceCounter> c0 = new ArrayList<CustServiceCounter>();
		try{		
			c0 = em.createNamedQuery("CustServiceCounter.findMainLocatorByStatus",CustServiceCounter.class)
					.setParameter("status1", status1.toUpperCase())
					.setParameter("status2", status2.toUpperCase())
					.getResultList();
		}catch (NoResultException ex) {
			// TODO: handle exception
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return c0;
	}

	@Override
	public List<CustServiceCounterEdited> findSubLocatorByStatus (String status,int counterId)throws SQLException {
		List<CustServiceCounterEdited> c0 = new ArrayList<CustServiceCounterEdited>();
		try{		
			c0 = em.createNamedQuery("CustServiceCounter.findSubLocatorByStatus",CustServiceCounterEdited.class)
					.setParameter("status", status.toUpperCase())
					.setParameter("counterId", counterId)					
					.getResultList();
		}catch (NoResultException ex) {
			// TODO: handle exception
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return c0;
	}

	@Override
	public List<CustServiceLocation> findAll()throws SQLException {
		List<CustServiceLocation> c0 = em.createNamedQuery("CustServiceLocation.findAll",CustServiceLocation.class).getResultList();
		return c0;
	}	

	@Override
	public CustServiceLocation findCustServiceLocationById(int id){		
		CustServiceLocation finder = em.find(CustServiceLocation.class, id);		
		return finder;
	}

	@Override
	public List<CustServiceLocation> findAllStation() throws SQLException {
		List<CustServiceLocation> stationList = null;
		stationList = em.createNamedQuery("CustServiceLocation.findAllStation",CustServiceLocation.class).getResultList();
		return stationList;
	}

	@Override
	public List<CustServiceCounter> findAllCounterByLocId(int id) throws SQLException {
		return em.createNamedQuery("CustServiceCounter.findAllCounterByLocId",CustServiceCounter.class).setParameter("locId", id).getResultList();
	}

	@Override
	public CustServiceCounter findCustServiceCounterById(int id) throws SQLException{
		return em.find(CustServiceCounter.class, id);		
	}

	/*ws@Override
	public List<CustServiceCounterEdited> findEditedByCounterId(int id) throws SQLException {
		return em.createNamedQuery("CustServiceCounter.findAllCounterByLocId",CustServiceCounter.class).setParameter("locId", id).getResultList();
	}*/

	public boolean updateCSCByMaker(CustServiceCounter c0){
		try{	
			CustServiceCounter update = em.find(CustServiceCounter.class, c0.getId());

			/*System.out.println("id>>"+c0.getId());
			System.out.println("loc id>>"+c0.getCustServiceLocationId());
			System.out.println("addressLine1>>"+c0.getAddressLine1());
			System.out.println("addressLine2>>"+c0.getAddressLine2());
			System.out.println("addressLine3>>"+c0.getAddressLine3());
			System.out.println("postcode>>"+c0.getPostcode());
			System.out.println("city>>"+c0.getCity());
			System.out.println("longitude>>"+c0.getLongitude());
			System.out.println("latitude>>"+c0.getLatitude());
			System.out.println("openingHour>>"+c0.getOpeningHour());
			System.out.println("status>>"+c0.getStatus());
			System.out.println("");*/

			update.setCustServLoc(c0.getCustServLoc());
			update.setCustServiceLocationId(c0.getCustServiceLocationId());
			update.setAddressLine1(c0.getAddressLine1());
			update.setAddressLine2(c0.getAddressLine2());
			update.setAddressLine3(c0.getAddressLine3());
			update.setCity(c0.getCity());
			update.setPostcode(c0.getPostcode());
			update.setLatitude(c0.getLatitude());
			update.setLongitude(c0.getLongitude());
			update.setOpeningHour(c0.getOpeningHour());
			update.setAdminUserId(c0.getAdminUserId());

			update.setStatus(c0.getStatus());
			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();
			return false;
		}	
		return true;
	}
	
	@Override
	public boolean updateCSCCustomByMaker(CustServiceCounter c0)throws SQLException{
		try{	
			CustServiceCounter update = em.find(CustServiceCounter.class, c0.getId());

			update.setCategory(c0.getCategory());		

			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();
			return false;
		}	
		return true;
	}

	@Override
	public boolean updateCSCByChecker(CustServiceCounter c0) throws SQLException {
		try{	
			CustServiceCounter update = em.find(CustServiceCounter.class, c0.getId());

			/*System.out.println("MaintenanceDAOImpl.updateCSCByChecker()");
			System.out.println("getId>>"+c0.getId());
			System.out.println("getAdminUserId>>"+c0.getAdminUserId());
			System.out.println("getApproverId>>"+c0.getApproverId());
			 */
			update.setCustServLoc(c0.getCustServLoc());
			update.setStatus(c0.getStatus());
			update.setCategory(c0.getCategory());
			update.setRejectReason(c0.getRejectReason());
			update.setApproverId(c0.getApproverId());
			update.setAdminUserId(c0.getAdminUserId());

			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();
			return false;
		}	
		return true;
	}
	
	@Override
	public CustServiceCounterEdited findCustServiceCounterEditedById(int id) throws SQLException{		
		CustServiceCounterEdited finder = em.find(CustServiceCounterEdited.class, id);		
		return finder;
	}
	
	
	@Override
	public boolean updateAprovedEditedCSCByChecker(CustServiceCounter c0) throws SQLException {
		try{	
			
			CustServiceCounter update = em.find(CustServiceCounter.class, c0.getId());
			
			update.setCustServLoc(c0.getCustServLoc());
			update.setCustServiceLocationId(c0.getCustServiceLocationId());
			update.setAddressLine1(c0.getAddressLine1());
			update.setAddressLine2(c0.getAddressLine2());
			update.setAddressLine3(c0.getAddressLine3());
			update.setCity(c0.getCity());
			update.setPostcode(c0.getPostcode());
			update.setLatitude(c0.getLatitude());
			update.setLongitude(c0.getLongitude());
			update.setOpeningHour(c0.getOpeningHour());
			update.setAdminUserId(c0.getAdminUserId());
			update.setStatus(c0.getStatus());					
						
			update.setCategory(c0.getCategory());
			update.setRejectReason(c0.getRejectReason());
			update.setApproverId(c0.getApproverId());

			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();
			return false;
		}	
		return true;
	}
	

	/** Part: News **/
	@Override
	public News saveNews(News news) throws SQLException {
		try{		
			em.persist(news);
			em.refresh(news);
			return news;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public News updateNews(News n0) {
		News update = new News();
		try{	
			update = em.find(News.class, n0.getId());

			if(StringUtil.trimToEmpty(n0.getTitle())!="")
				update.setTitle(n0.getTitle());
			if(StringUtil.trimToEmpty(n0.getDescription())!="")
				update.setDescription(n0.getDescription());
			if(n0.getStartDatetime()!=null)			
				update.setStartDatetime(n0.getStartDatetime());
			if(n0.getEndDatetime() !=null)
				update.setEndDatetime(n0.getEndDatetime());
			if(n0.getPublishDatetime() !=null)
				update.setPublishDatetime(n0.getPublishDatetime());

			if(n0.getImageSFileType()!=null && n0.getImageSFileType().length()>0){
				update.setImageS(n0.getImageS());
				update.setImageSFileType(n0.getImageSFileType());
			}
			if(n0.getImageLFileType()!=null && n0.getImageLFileType().length()>0){
				update.setImageL(n0.getImageL());
				update.setImageLFileType(n0.getImageLFileType());
			}

			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();

		}	
		return n0;
	}



	public List<News> findNews(Date dateFrom, Date dateTo, String title) throws SQLException{	

		List<News> newslist = new  ArrayList<News>();
		/*System.out.println("findNews[dateFrom]>>"+dateFrom);
		System.out.println("findNews[dateTo]>>"+dateTo);
		System.out.println("findNews[title]>>"+title);*/

		String paramTitle = (!title.equalsIgnoreCase("") ? "%"+title+"%" : "%" );

		try{	
			em.clear();
			Query readQuery = em.createNamedQuery("News.findNewsByTitle",News.class);
			readQuery.setParameter("startDate", dateFrom)
			.setParameter("endDate", dateTo)
			.setParameter("title", paramTitle.toUpperCase()).getResultList();
			readQuery.setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH);
			newslist = readQuery.getResultList();

		}catch(NoResultException e){
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}	
		return newslist; 
	}

	public News findNewsById(int id)  throws SQLException{		
		News finder = em.find(News.class, id);		
		return finder;
	}

	@Override
	public List<News> findNewsTitleByEndDate() throws SQLException {
		List<News> n0 = new ArrayList<News>();
		try{	
			n0 = em.createNamedQuery("News.findNewsTitleByEndDate",News.class).getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
		}	
		return  n0;
	}

	@Override
	public List<News> findNewsIds(List<Integer> ids) throws SQLException {
		List<News> n0 = new ArrayList<News>();
		try{	
			n0 = em.createNamedQuery("News.findNewsIds",News.class).setParameter("ids", ids).getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
		}	
		return  n0;
	}


	/**Part : Outage Alert****/
	@Override
	public List<PowerAlertType> findPowerAlertType() throws SQLException {
		List<PowerAlertType> p0 = em.createNamedQuery("PowerAlertType.findAll",PowerAlertType.class).getResultList();		
		return p0;
	}

	@Override
	public PowerAlertType findPowerAlertTypeById(int id) throws SQLException {
		PowerAlertType finder = em.find(PowerAlertType.class, id);		
		return finder;
	}

	@Override
	public PowerAlert savePowerAlert(PowerAlert p0) throws SQLException {
		try{		
			em.persist(p0);
			em.refresh(p0);
			return p0;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;

	}

	@Override
	public PowerAlert findPowerAlertById(int id) throws SQLException {
		PowerAlert finder = em.find(PowerAlert.class, id);		
		return finder;
	}

	@Override
	public PowerAlert findAllPowerAlertByCondition2ById(int id) throws SQLException {
		PowerAlert p = new PowerAlert();
		try{
			p = em.createNamedQuery("PowerAlert.findAllPowerAlertByCondition2ById",PowerAlert.class)
					.setParameter("id", id).getSingleResult();	

			/*System.out.println("=====================");

			System.out.println("alert id : "+p.getId());
			System.out.println("alert id : "+p.getTitle());
			System.out.println("tag size : "+p.getTagNotifications ().size());
			List<TagNotification> tag = p.getTagNotifications ();				
			for(TagNotification t : tag){
				System.out.println("notification id:"+t.getNotificationId());
				System.out.println("notification status:"+t.getStatus());
				System.out.println("notification text:"+t.getText());
			}

			System.out.println("=====================");*/


		}catch(Exception ex){
			ex.printStackTrace();
		}

		return p;
	}
	@Override
	public List<PowerAlert> findAllOutageByTypeId(Date startDate, Date endDate, int powerTypeId) throws SQLException {
		List<PowerAlert> push0 = new  ArrayList<PowerAlert>();
		try{
			em.clear();
			push0 = em.createNamedQuery("PowerAlert.findAllOutageByTypeId", PowerAlert.class)
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate)
					.setParameter("powerTypeId",powerTypeId).getResultList();		

		}catch(NoResultException eNo){
			eNo.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		return push0;
	}

	@Override
	public List<PowerAlert> findAllPreventiveByTypeId(Date startDate, Date endDate, int powerTypeId) throws SQLException {
		List<PowerAlert> push0 = new  ArrayList<PowerAlert>();
		try{
			em.clear();
			push0 = em.createNamedQuery("PowerAlert.findAllPreventiveByTypeId", PowerAlert.class)
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate)
					.setParameter("powerTypeId",powerTypeId).getResultList();		

		}catch(NoResultException eNo){
			eNo.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		return push0;
	}




	/*	@Override
	public List<PowerAlert> findAllPowerAlertByCondition(Date startDate, Date endDate, int powerTypeId) throws SQLException {
		List<PowerAlert> push0 = new  ArrayList<PowerAlert>();
		try{
			push0 = em.createNativeQuery("WITH SUB_A AS(select * from "+ApplicationConstant.SCHEMA+".TAG_NOTIFICATION t order by t.NOTIFICATION_ID desc limit 1) select * from "+ApplicationConstant.SCHEMA+".POWER_ALERT p left join SUB_A a on p.ID = a.ASSOCIATED_ID where p.CREATED_DATETIME>=:startDate and p.CREATED_DATETIME<=:endDate and POWER_ALERT_TYPE_ID =:powerTypeId  order by p.ID ", PowerAlert.class)
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate)
					.setParameter("powerTypeId",powerTypeId).getResultList();		
			System.out.println(">>>>>>>>>push0="+push0.size());
System.out.println(push0.toString());

			for(PowerAlert p :push0){
				//System.out.println(p.getId());

			}
		}catch(Exception ex){
			ex.printStackTrace();
		}

		return push0;
	}*/


	/*@Override
	public List<PowerAlert> findAllPowerAlertByCondition(Date startDate, Date endDate, int powerTypeId) throws SQLException {
		List<PowerAlert> push0 = new  ArrayList<PowerAlert>();
		try{
			push0 = em.createNamedQuery("PowerAlert.findAllPowerAlertByCondition",PowerAlert.class)
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate)
					.setParameter("powerTypeId",powerTypeId).getResultList();	

			System.out.println(">>>>>>>>>push0="+push0.size());
			System.out.println(push0.toString());

			for(PowerAlert p:push0 ){
				System.out.println("=====================");

				System.out.println("alert id : "+p.getId());
				System.out.println("alert id : "+p.getTitle());
				System.out.println("tag size : "+p.getTagNotifications ().size());
				List<TagNotification> tag = p.getTagNotifications ();				
				for(TagNotification t : tag){
					System.out.println("notification id:"+t.getNotificationId());
					System.out.println("notification status:"+t.getStatus());
					System.out.println("notification text:"+t.getText());
				}

				System.out.println("=====================");
			}

		}catch(Exception ex){
			ex.printStackTrace();
		}

		return push0;
	}
	 */
	@Override
	public List<PowerAlert> findAllOutagePowerAlertByIds(List<Integer> ids) throws SQLException {
		List<PowerAlert> p0 = new ArrayList<PowerAlert>();
		try{	
			p0 = em.createNamedQuery("PowerAlert.findAllOutagePowerAlertByIds",PowerAlert.class).setParameter("ids", ids).getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
		}	
		return  p0;
	}

	@Override
	public List<PowerAlert> findAllPreventivePowerAlertByIds(List<Integer> ids) throws SQLException {
		List<PowerAlert> p0 = new ArrayList<PowerAlert>();
		try{	
			p0 = em.createNamedQuery("PowerAlert.findAllPreventivePowerAlertByIds",PowerAlert.class).setParameter("ids", ids).getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
		}	
		return  p0;
	}

	@Override
	public PowerAlert updatePowerAlert(PowerAlert p0) {
		PowerAlert update = new PowerAlert();
		try{	
			update = em.find(PowerAlert.class, p0.getId());

			update.setCauses(p0.getCauses());
			update.setRestorationDatetime(p0.getRestorationDatetime());



			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();

		}	
		return p0;
	}

	@Override
	public List<PowerAlert> findAllOutageContent() throws SQLException {
		List<PowerAlert> p0 = new ArrayList<PowerAlert>();
		try{	
			p0 = em.createNamedQuery("PowerAlert.findAllOutageContent",PowerAlert.class).getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
		}	
		return  p0;
	}

	@Override
	public List<PowerAlert> findAllPreventiveContent() throws SQLException {
		List<PowerAlert> p0 = new ArrayList<PowerAlert>();
		try{	
			p0 = em.createNamedQuery("PowerAlert.findAllPreventiveContent",PowerAlert.class).getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
		}	
		return  p0;
	}

	/**Part-Push Notification**/

	@Override
	public TagNotification findPushNotificationById(int id) throws SQLException {
		return em.find(TagNotification.class, id);		
	}

	@Override
	public TagNotification updatePushNotification(TagNotification p0) {
		TagNotification update = new TagNotification();
		try{	
			System.out.println("notifId :"+p0.getNotificationId());
			System.out.println("text :"+p0.getText());
			System.out.println("notificationType :"+p0.getNotificationType());
			System.out.println("associated id :"+p0.getAssociatedId());
			System.out.println("power alert type :"+p0.getPowerAlertType());
			System.out.println("tag name :"+p0.getTagName());
			System.out.println("status :"+p0.getStatus());
			System.out.println("push datetime :"+p0.getPushDatetime());


			update = em.find(TagNotification.class, p0.getNotificationId());

			update.setText(p0.getText());
			update.setNotificationType(p0.getNotificationType());
			update.setAssociatedId(p0.getAssociatedId());
			update.setPowerAlertType(p0.getPowerAlertType());
			update.setTagName(p0.getTagName());
			update.setPushDatetime(p0.getPushDatetime());

			//woonsan
			update.setStatus(p0.getStatus());


			em.merge(update);
		}catch(NoResultException e){
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			e.printStackTrace();

		}catch (Exception e) {
			System.out.println("######################################################");
			e.printStackTrace();
		}	
		return update;
	}

	@Override
	public List<TagNotification> findAllPNByTitleId(int associatedId) throws SQLException {

		List<TagNotification> p0 = new ArrayList<TagNotification>();
		try{	
			p0 = em.createNamedQuery("TagNotification.findAllByTitleId", TagNotification.class).setParameter("associatedId", associatedId).getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
			return null;
		}
		return p0;		
	}

	@Override
	public List<TagNotification> findContentByAssIdAndType(Integer associatedId,String notificationType) throws SQLException {

		List<TagNotification> p0 = new ArrayList<TagNotification>();
		try{	
			p0 = em.createNamedQuery("TagNotification.findContentByAssIdAndType", TagNotification.class)
					.setParameter("associatedId", associatedId)
					.setParameter("notificationType", notificationType)
					.getResultList();		
					//.setMaxResults(1).getResultList();
			/*System.out.println("==============findContentByAssIdAndType===========");


			for(TagNotification  p : p0){
				System.out.println("associatedId>>"+associatedId);
				System.out.println("getNotificationId>>"+p.getNotificationId());
				System.out.println("");
			}
			System.out.println("==============findContentByAssIdAndType===========");*/



		}catch(NoResultException e){

		}
		return p0;		
	}


	@Override
	public List<Integer> findPNDistinctTitleIdBtwDate(String notifType, String tagName) throws SQLException {

		List<Integer> p0 = new ArrayList<Integer>();
		try{	
			p0 = em.createNamedQuery("TagNotification.findDistinctTitleIdBtwDate", Integer.class)
					.setParameter("notifType", notifType)
					.setParameter("tagName",( "%"+tagName+"%").toUpperCase())
					.getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
			return null;
		}
		return p0;		
	}

	@Override
	public List<Integer> findSubTitleIdBtwDate(Date startDate, Date endDate, String notifType, String tagName) throws SQLException {

		List<Integer> p0 = new ArrayList<Integer>();
		try{	
			p0 = em.createNamedQuery("TagNotification.findSubTitleIdBtwDate", Integer.class)
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate)
					.setParameter("notifType", notifType)
					.setParameter("tagName",( "%"+tagName+"%").toUpperCase())
					.getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
			return null;
		}
		return p0;		
	}



	@Override
	public List<Integer> findSubTitleIdBtwDatePower(Date startDate, Date endDate, String notifType, String tagName, String powerType) throws SQLException {
		System.out.println("notifType>"+notifType);
		System.out.println("tagName>"+tagName);
		System.out.println("powerType>"+powerType);
		List<Integer> p0 = new ArrayList<Integer>();
		try{	
			p0 = em.createNamedQuery("TagNotification.findSubTitleIdBtwDatePower", Integer.class)
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate)
					.setParameter("notifType", notifType)
					.setParameter("tagName",( "%"+tagName+"%").toUpperCase())
					.setParameter("powerName",( "%"+powerType+"%").toUpperCase())					
					.getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
			return null;
		}
		return p0;		
	}

	@Override
	public List<Integer> findDistinctTitleIdBtwDatePower(String notifType, String tagName, String powerType) throws SQLException {
		System.out.println("notifType>"+notifType);
		System.out.println("tagName>"+tagName);
		System.out.println("powerType>"+powerType);
		List<Integer> p0 = new ArrayList<Integer>();
		try{	
			p0 = em.createNamedQuery("TagNotification.findDistinctTitleIdBtwDatePower", Integer.class)
					.setParameter("notifType", notifType)
					.setParameter("tagName",( "%"+tagName+"%").toUpperCase())
					.setParameter("powerName",( "%"+powerType+"%").toUpperCase())					
					.getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
			return null;
		}
		return p0;		
	}

	@Override
	public List<TagNotification> findAllByCondition1(Date startDate, Date endDate, String notifType, Integer associatedId, String message) throws SQLException {
		List<TagNotification> p0 = new ArrayList<TagNotification>();
		try{	
			System.out.println("===========DAO=============");
			System.out.println("startDate>"+startDate);
			System.out.println("endDate>"+endDate);
			System.out.println("type>"+notifType);
			System.out.println("titleId>"+associatedId);
			System.out.println("message>"+message);
			System.out.println("===========DAO=============");

			System.out.println("MaintenanceDAOImpl.findAllByCondition1()");
			//em.getEntityManagerFactory().getCache().evictAll();
			em.clear();
			Query readQuery = em.createNamedQuery("TagNotification.findAllByCondition1",TagNotification.class);
			readQuery.setParameter("startDate", startDate)
			.setParameter("endDate", endDate)
			.setParameter("notifType", notifType.trim().toUpperCase())
			.setParameter("associatedId", associatedId)
			.setParameter("msg", "%"+message.toUpperCase().trim()+"%");			
			p0 = readQuery.getResultList();
			/*p0 = em.createNamedQuery("TagNotification.findAllByCondition1",TagNotification.class)
					.setParameter("startDate", startDate)
					.setParameter("endDate", endDate)
					.setParameter("notifType", notifType.trim().toUpperCase())
					.setParameter("associatedId", associatedId)
					.setParameter("msg", "%"+message.toUpperCase().trim()+"%")					
					.getResultList();*/
			for(TagNotification a :p0){
				System.out.println("status >>"+a.getStatus());
			}
			return readQuery.getResultList();
		}catch(NoResultException e){
			e.printStackTrace();
		}	
		return  p0;
	}



	@Override
	public List<TagNotification> findAllFreeTextByIds() throws SQLException {
		List<TagNotification> p0 = new ArrayList<TagNotification>();
		try{	
			p0 = em.createNamedQuery("TagNotification.findAllFreeTextByIds",TagNotification.class)
					.getResultList();		
		}catch(NoResultException e){
			e.printStackTrace();
		}	
		return  p0;
	}


	@Override
	public TagNotification savePushNotication(TagNotification p0) throws SQLException {
		try{		
			em.persist(p0);
			em.refresh(p0);
			return p0;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	/**============================================
	 * FAQ
	 *============================================*/
	@Override
	public List<FaqCategory> findAllFaqCategory() throws SQLException {
		List<FaqCategory> categoryList = new  ArrayList<FaqCategory>();
		try{
			categoryList = em.createNamedQuery("FaqCategory.findAllFaqCategory", FaqCategory.class).getResultList();		

		}catch(NoResultException eNo){
			eNo.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		return categoryList;
	}

	@Override
	public FaqCategory findFaqCategoryById(int id) throws Exception{		
		FaqCategory finder = em.find(FaqCategory.class, id);		
		return finder;
	}

	@Override
	public List<Faq> findActiveQuesEnOrderBySequence(int catId) throws SQLException {
		List<Faq> faqList = new  ArrayList<Faq>();
		try{
			faqList = em.createNamedQuery("Faq.findActiveQuesEnOrderBySequence", Faq.class).setParameter("catId", catId).getResultList();			

		}catch(NoResultException eNo){
		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		return faqList;
	}

	@Override
	public Long findNextSeq(int catId) throws SQLException {
		Long result = 1L;

		try{
			result = em.createNamedQuery("Faq.findNextSeq", Long.class).setParameter("catId", catId).getSingleResult();			

		}catch(NoResultException eNo){
		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		return result;
	}


	@Override
	public Faq saveFAQ(Faq f0) throws SQLException {
		try{		
			em.persist(f0);
			em.refresh(f0);
			return f0;
		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
		}
		return null;

	}

	@Override
	public List<Faq> findWithOrderByQuestEn(int catId, String status) throws SQLException {
		List<Faq> faqlist = new ArrayList<Faq>();
		try{
			faqlist = em.createNamedQuery("Faq.findWithOrderByQuestEn",Faq.class)
					.setParameter("catId", catId)
					.setParameter("status", status.toUpperCase()).getResultList();	

		}catch(NoResultException noRe){
		}catch (Exception ex) {
			log.error(ex.getMessage());
		}
		return faqlist;	
	}

	@Override
	public List<Faq> findAllFaqById(int id) throws SQLException {
		Faq myFaq = new Faq();
		List<Faq> faqlist = new ArrayList<Faq>();

		myFaq = em.find(Faq.class, id);	
		faqlist.add(myFaq);
		return faqlist;
	}

	@Override
	public boolean updateFAQ(Faq f0, boolean updateSeq) throws SQLException {
		try{	
			Faq update = em.find(Faq.class, f0.getId());

			if(updateSeq == false){
				update.setQuestionEn(f0.getQuestionEn());
				update.setAnswerEn(f0.getAnswerEn());
				update.setQuestionBm(f0.getQuestionBm());
				update.setAnswerBm(f0.getAnswerBm());
				update.setQuestionCn(f0.getQuestionCn());
				update.setAnswerCn(f0.getAnswerCn());
				update.setFaqCategory(f0.getFaqCategory());
				update.setStatus(f0.getStatus());
			}else
			{
				update.setFaqSequence(f0.getFaqSequence());
			}

			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();
			return false;
		}	
		return true;
	}

	@Override
	public ContactUs findAllContact() throws SQLException {
		ContactUs results = new ContactUs();
		try{
			Query query =  (Query) em.createNamedQuery("ContactUs.findAll");
			results = (ContactUs) query.getSingleResult();		
			em.refresh(results);			
		}catch(NoResultException noRe){
		}catch (Exception ex) {
			log.error(ex.getMessage());
		}
		return results;	
	}

	@Override
	public boolean updateContact(ContactUs contact) throws SQLException {
		try{	
			ContactUs update = em.find(ContactUs.class, contact.getId());
			update.setTelNo(contact.getTelNo());
			update.setFaxNo(contact.getFaxNo());
			update.setAddress(contact.getAddress());
			update.setEmail(contact.getEmail());
			update.setWebUrl(contact.getWebUrl());
			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();
			return false;
		}	
		return true;
	}
	
	
	@Override
	public List<TariffCategory> findAllTariffCategory() {
		List <TariffCategory> typeList = new ArrayList <TariffCategory>();
		try{
			typeList = em.createNamedQuery("TariffCategory.findAll",TariffCategory.class).getResultList();

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return typeList;
	}
	
	@Override
	public List<TariffSetting> findAllTariffSettings(int catId) {
		List <TariffSetting> resultList = new ArrayList <TariffSetting>();
		try{
			resultList = em.createNamedQuery("TariffSetting.findAllByCategoryId",TariffSetting.class)
					.setParameter("catId", catId).getResultList();
		}catch(NoResultException noR){

		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		return resultList;
	}
	
	
	@Override
	public TariffCategory findTariffCategoryById(int id) throws SQLException {
		return em.find(TariffCategory.class, id);		
	}
	
	@Override
	public TariffSetting saveTariffSettings(TariffSetting ts) {
		try{		
			em.persist(ts);
			return ts;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	@Override
	public int deleteTariffSettingByCatId(int catId) throws SQLException {
		int result = -1;
		try{	
			result = em.createNamedQuery("TariffSetting.deleteTariffSettingByCatId")
			.setParameter("catId", catId).executeUpdate();

			em.getEntityManagerFactory().getCache().evictAll();
		}catch(NoResultException e){
			e.printStackTrace();
			return 0;
		}	
		return result;
	}
}
