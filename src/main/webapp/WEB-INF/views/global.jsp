<!-- Bootstrap Core CSS -->
<link href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Bootstrap Dialog -->
<link href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-dialog/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css">
	
<!-- MetisMenu CSS -->
<link href="${pageContext.request.contextPath}/resources/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

<!-- Timeline CSS -->
<link href="${pageContext.request.contextPath}/resources/dist/css/timeline.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="${pageContext.request.contextPath}/resources/dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="${pageContext.request.contextPath}/resources/bower_components/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Form Validation -->
<link href="${pageContext.request.contextPath}/resources/bower_components/formvalidation/css/formValidation.css" rel="stylesheet" />

<!-- Bootstrap Toggle Switch -->
<link href="${pageContext.request.contextPath}/resources/dist/css/bootstrap-switch.css" rel="stylesheet">

<!--  Bootstrap Data Tables CSS -->
<link href="${pageContext.request.contextPath}/resources/bower_components/datatables1.10.9/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bower_components/datatables1.10.9/media/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bower_components/datatables1.10.9/media/css/select.dataTables.min.css" rel="stylesheet" />
<link href="${pageContext.request.contextPath}/resources/bower_components/datatables1.10.9/media/css/buttons.dataTables.min.css" rel="stylesheet" />

<!-- bootstrap-datetimepicker-->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

	
<!-- jQuery -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>

<!-- jQuery UI (drag and drop FAQ category)-->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-ui/jquery-ui.min.js"></script>

<!-- jQuery cookies (drag and drop FAQ category)-->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-cookies/jquery.cookie.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Bootstrap Dialog -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-dialog/js/bootstrap-dialog.min.js"></script>
		
<!-- Metis Menu Plugin JavaScript -->
<script src="${pageContext.request.contextPath}/resources/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="${pageContext.request.contextPath}/resources/bower_components/raphael/raphael-min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="${pageContext.request.contextPath}/resources/dist/js/sb-admin-2.js"></script>
<script src="${pageContext.request.contextPath}/resources/dist/js/jquery.form.js"></script>

<!-- Date Picker -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/datepicker/js/bootstrap-datepicker.js" charset="UTF-8"></script>

<!-- bootstrap-datetimepicker-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment-with-locales.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

 <!-- Form Validation -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/formvalidation/js/formValidation.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/formvalidation/js/framework/bootstrap.js"></script>

<!--  Bootstrap Toggle Switch -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/dist/js/bootstrap-switch.js"></script>

<!--  Bootstrap Data Tables -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/datatables1.10.9/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/datatables1.10.9/media/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/datatables1.10.9/media/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/datatables1.10.9/media/js/dataTables.select.min.js"></script>

<!-- Export to csv -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/datatables1.10.9/media/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/datatables1.10.9/media/js/dataTables.buttons.min.js"></script>


<script type="text/javascript" src="${pageContext.request.contextPath}/resources/dist/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/dist/js/tinymce/plugins/charactercount/charactercount.js"></script>

<!-- tab icon -->
<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico" />




<script type="text/javascript">
var sebApp;
sebApp = sebApp || (function () {
	var pleaseWaitDiv = $('<div class="modal fade" style=top:40% id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-dialog modal-sm" style="left:15%;"><div class="modal-content" style="width:25px;"><img src="${pageContext.request.contextPath}/resources/images/ajax-loader.gif"/></div></div></div>');
	return {
		showIndicator: function() {
			pleaseWaitDiv.modal();
		},
		hideIndicator: function () {
			pleaseWaitDiv.modal('hide');
		},

	};
})();

function date_round(date, duration) { return moment(Math.round((+date)/(+duration)) * (+duration)); } 

String.prototype.capitalize = function(){
  return this.replace( /(^|\s)([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase();
  } );
};
</script>
