package com.seb.admin.portal.spg.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OFileEncode;
import com.seb.admin.portal.spg.entity.ResponseStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentReportExportResponse {
	@JsonProperty("status")
	private ResponseStatus responseStatus;
	
	@JsonProperty("payment_report_excel_file")
	private OFileEncode ofileEncode;

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	public OFileEncode getOfileEncode() {
		return ofileEncode;
	}

	public void setOfileEncode(OFileEncode ofileEncode) {
		this.ofileEncode = ofileEncode;
	}
	
	
}
