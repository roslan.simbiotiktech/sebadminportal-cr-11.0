<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="_csrf" content="${_csrf.token}" />
	<meta name="_csrf_header" content="${_csrf.headerName}" />
    <meta content="" name="description">
    <meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
        </nav>
        <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp"%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Payment Client Maintenance &gt; Add
                        </div>
                        <div class="panel-body">
                            <form id="addForm" name="addForm" action="${pageContext.request.contextPath}/admin/spg/pymtClientMaintenance/add.do" class="form-horizontal" method="post" >
                                ${successMsg}${errorMsg}
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Client Name <span style="color: red;">*</span> :</p>

                                    <div class="col-lg-3">
                                    	<input class="form-control" id="clientName" name="clientName" placeholder="Client Name" type="text" maxlength="50">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Description <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <textarea class="form-control" cols="23" id="description" name="description" rows="3" maxlength="500"></textarea>
									</div>
								</div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Hash Key <span style="color: red;">*</span> :</p>

                                    <div class="col-lg-3">
                                    	<input class="form-control" id="hashKey" name="hashKey" placeholder="Hash Key" type="text" maxlength="64">
                                    </div>
                                </div>
								
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Server to Server Payment Status URL <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                        <textarea class="form-control" cols="23" id="statusUrl" name="statusUrl" rows="3" maxlength="1000"></textarea>
									</div>
								</div>
								
								<div class="form-group">
                                    <p class="col-lg-5 control-label">Client Payment Complete Redirect URL :</p>
                                    <div class="col-lg-3">
                                        <textarea class="form-control" cols="23" id="redirectUrl" name="redirectUrl" rows="3" maxlength="1000"></textarea>
									</div>
								</div>
								
								<div class="form-group">
                                    <p class="col-lg-5 control-label">Status <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-6">
                                        <label class="radio-inline">
									      <input type="radio" name="statusOpt" checked value="true">ENABLED
									    </label>
									    <label class="radio-inline">
									      <input type="radio" name="statusOpt" value="DISABLED">DISABLED
									    </label>
									</div>
								</div>
								
                                <div class="form-group">
                                    <div class="col-lg-5 col-lg-offset-3 text-right">
                                        <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
                                        <button class="btn btn-primary"  id="submitBtn" type="button" data-toggle="modal" data-target="#modal">Add</button> <button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
                                    </div>
                                </div>
                                
                                <!-- Add Confirmation Modal -->
								<div class="modal fade" id="modal" tabindex="-1">
						        <div class="modal-dialog">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button class="close" data-dismiss="modal" type="button"><span>&times;</span><span class="sr-only">Close</span></button>
											<h2 class="modal-title">Confirmation</h2>
						                </div>						
						                <div class="modal-body" id="add">
						                    Are you sure you want to add payment client maintenance?
						                </div>						
						                <div class="modal-footer">
						                    <button class="btn btn-primary" id="add" name="add" type="submit">Add</button> <button class="btn btn-warning" data-dismiss="modal" type="button">Close</button> 
						                </div>
						            </div>
						        </div>
   								</div>
   								<!-- End Confirmation Modal -->
                            </form>
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /#page-wrapper -->
    </div>
    <script type="text/javascript">
    function resetSS() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#addForm').find('.has-success').removeClass("has-success");
    	$('#addForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#addForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();    	
    }
        
    
    function resetForm() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#addForm').find('.has-success').removeClass("has-success");
    	$('#addForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#addForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();
    	$('#addForm').formValidation('resetForm', true);
    	$('select').val('');
    	
    	return false;
    }
    $(document).ready(function() {    	
    	
        $('#addForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
            	clientName: {
                    validators: {
                        notEmpty: {
                            message: 'Client Name is required'
                        } 
                    }
                },
                description: {
                    validators: {
                        notEmpty: {
                            message: 'Description is required'
                        } 
                    }
                },
                hashKey: {
                    validators: {
                        notEmpty: {
                            message: 'Hash Key is required'
                        } 
                    }
                },
                statusUrl: {
                    validators: {
                        notEmpty: {
                            message: 'Server to Server Payment Status URL is required'
                        } 
                    }
                }//end
            }
        });
    });
    </script>
</body>
</html>