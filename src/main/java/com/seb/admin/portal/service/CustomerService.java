package com.seb.admin.portal.service;

import java.sql.SQLException;

import com.seb.admin.portal.adapter.CustomerHistoryResponse;
import com.seb.admin.portal.adapter.ResendOTPRequest;
import com.seb.admin.portal.adapter.ResendOTPResponse;
import com.seb.admin.portal.adapter.ResetPasswordResponse;
import com.seb.admin.portal.adapter.SearchProfileRequest;
import com.seb.admin.portal.adapter.SearchProfileResponse;
import com.seb.admin.portal.adapter.ViewProfileResponse;
import com.seb.admin.portal.model.Subscription;

public interface CustomerService {

	public SearchProfileResponse cms_search_profile(int page, SearchProfileRequest req) throws Exception;

	public ViewProfileResponse cms_view_profile_details(ViewProfileResponse req) throws Exception;

	public CustomerHistoryResponse cms_view_customer_history (int page, String dateFrom, String dateTo, String msisdn, String contractAccNo, String loginId ) throws Exception;
	
	public ResetPasswordResponse cms_reset_customer_password (String loginId) throws Exception;
	
	public Subscription findSelectedSubscription(String email, String accountNumber, String status) throws SQLException;

	public boolean updateSubscriptionStatus(int subsId, String subscriptionStatus, String remark) throws SQLException;

	boolean updateUserProfile(String loginId, String userStatus, String preferredMethod)
			throws SQLException;
	
	public ResendOTPResponse cms_resend_otp (ResendOTPRequest req) throws SQLException;
	
	public ResendOTPResponse cms_resend_mobile_verification_code (ResendOTPRequest req) throws SQLException;

	public boolean updateUserSubscription(String email, String oldStatus, String newStatus) throws SQLException;

	
	

	
}
