package com.seb.admin.portal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seb.admin.portal.dao.ReportRemarksHistoryDAO;
import com.seb.admin.portal.model.ReportRemarksHistory;

@Service
public class ReportRemarksHistoryServiceImpl implements ReportRemarksHistoryService {

	@Autowired
	private ReportRemarksHistoryDAO ReportRemarksHistoryDAO;
	
	@Override
	public List<ReportRemarksHistory> getReportRemarksHistory(String transId) {
		// TODO Auto-generated method stub
		List<ReportRemarksHistory> listReportRemarksHistory=	ReportRemarksHistoryDAO.findAllMakeReportByCondition(transId);
		return listReportRemarksHistory;
	}

}
