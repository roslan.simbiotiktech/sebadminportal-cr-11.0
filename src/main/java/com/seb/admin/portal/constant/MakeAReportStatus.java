package com.seb.admin.portal.constant;

public enum MakeAReportStatus {

	OPEN		("Open"),
	ESCALATED	("Escalated"),
	ASSIGNED	("Assigned"), 
	IN_PROGRESS	("In Progress"),
	PLEASE_CONTACT_SEB	("Please contact SEB"),
	RESOLVED	("Resolved");


	private String label;

	private MakeAReportStatus(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}


}
