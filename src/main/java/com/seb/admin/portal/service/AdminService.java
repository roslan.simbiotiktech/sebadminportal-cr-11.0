package com.seb.admin.portal.service;

import java.sql.SQLException;

public interface AdminService {

	public boolean changePassword(int loginID, String newPassword) throws SQLException;	
	
	
}
