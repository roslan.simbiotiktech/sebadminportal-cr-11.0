<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->

		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="font-weight: bold">Report &gt; Customer Activities Report</div>

						<div class="panel-body">
							<form action="${pageContext.request.contextPath}/admin/customerActivityReport.do" class="form-horizontal" id="reportForm" method="post"
								name="reportForm">
								${successMsg}${errorMsg}
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Date From <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='dateFrom'>
											<input class="form-control datepicker" id="indateFrom" name="indateFrom" readonly type='text'> <span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Date To <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='dateTo'>
											<input class="form-control datepicker" id="indateTo" name="indateTo" readonly type='text'> <span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>


								<div class="form-group">
									<p class="col-lg-5 control-label">Login Id/Email :</p>

									<div class="col-lg-3">
										<input class="form-control" id="email" maxlength="50" name="email" placeholder="Email" type="text">
									</div>
								</div>


								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" id="submitBtn" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
							</form>
							<div id="resultDiv" style="display: none; margin-top: 10%">
								<form action="${pageContext.request.contextPath}/admin/auditTrails.do?${_csrf.parameterName}=${_csrf.token}" class="form-horizontal"
									id="viewReportForm" method="post" name="viewReportForm">
									<h4>
										Search Results : Date From :<b>${dateFrom}</b>, Date To :<b>${dateTo}</b>, Login Id/Email :<b>${email}</b>
									</h4>
									<div class="form-group">
										<div class="alert alert-danger" id="messages" role="alert" style="display: none;"></div>
									</div>
									<table cellspacing="0" class="table table-striped table-bordered" id="tblSearch">
										<thead>
											<tr>
												<th>No.</th>
												<th>Date</th>
												<th>Login ID</th>
												<th>Activity</th>
											</tr>
											<!-- #resultDiv -->
										</thead>
										<tbody>
											<c:if test="${not empty reportList}">
												<c:forEach items="${reportList}" var="item" varStatus="status">
													<tr>
														<td>${status.index+1}</td>
														<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.auditDatetime}" pattern="dd/MM/YYYY hh:mm:ss a" /></td>
														<td>${item.userId}</td>
														<td>${item.activity}</td>
													</tr>
												</c:forEach>
											</c:if>

										</tbody>
									</table>
								</form>
								<!-- #viewReportForm -->
							</div>
							<!-- #resultDiv -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
		function resetForm() {
			$('#').find('.has-error').removeClass("has-error");
			$('#reportForm').find('.has-success').removeClass("has-success");
			$('#reportForm').find('.form-control-feedback').removeClass('glyphicon-remove');
			$('#reportForm').find('.form-control-feedback').removeClass('glyphicon-ok');
			$('#reportForm').formValidation('resetForm', true);
			$('.form-group').find('small.help-block').hide();
			$('.form-group').removeClass('has-error has-feedback');
			$('.form-group').find('i.form-control-feedback').hide();			
			setTimeout(function() {
				$('#indateTo').val(moment(new Date()).format('DD/MM/YYYY'));
				$('#indateFrom').val(moment(new Date()).format('DD/MM/YYYY'));
			}, 50);
			return false;
		}
		$(document).ready(function() {
			if(${reportList!=null && reportList.size()>=0}){	
        		$('#resultDiv').show();
       			var table= $('#tblSearch').DataTable({
  					bFilter: false,
      				bSort: false,
      				lengthMenu: [[50, 100], [50, 100]],
      				dom: 'Bfrtip',
          			buttons: [
          		     {
          		    	extend: 'csvHtml5',
                     	text: 'Export To CSV',
                     	filename : 'CustomerActivityReport_'+new Date().getTime()
          		     } 
          		]
    			});
    		}
			
			$('#dateFrom').datetimepicker({
				format : 'DD/MM/YYYY',
				defaultDate : new Date(),
				ignoreReadonly : true
			}).on("dp.change", function(e) {
        		$('#reportForm').formValidation('revalidateField', 'indateFrom');
      		});
			
			$('#dateTo').datetimepicker({
				format : 'DD/MM/YYYY',
				defaultDate : new Date(),
				ignoreReadonly : true
			}).on("dp.change", function(e) {
    			$('#reportForm').formValidation('revalidateField', 'indateTo');
  			});

			$('#reportForm').formValidation({
				framework : 'bootstrap',
				icon : {
					invalid : 'glyphicon glyphicon-remove',
				},
				fields : {
					indateFrom : {
						validators : {
							callback : {
								callback : function(value, validator, $field) {
									if (value == '') {
										$('#indateFrom').val(moment(new Date()).format('DD/MM/YYYY'));
										return true;
									}
									return true;
								}
							},
							date: {
             	 				format: 'DD/MM/YYYY',
                			 	max: 'indateTo',
                 				message: 'The start date is not a valid'
             				}
						}
					},
					indateTo : {
						validators : {
							date: {
		       	 				format: 'DD/MM/YYYY',
		          			 	min: 'indateFrom',
		           				message: 'The end date is not a valid'
       						},
							callback : {
								callback : function(value, validator, $field) {
									if (value == '') {
										$('#indateTo').val(moment(new Date()).format('DD/MM/YYYY'));
										//console.log("value>" + $('#indateTo').val());

										return true;
									}
									return true;
								}
							},
							format : 'DD/MM/YYYY'
						}
					},
				}
			}).on('success.field.fv', function(e, data) {
			      if (data.field === 'indateFrom' && !data.fv.isValidField('indateTo')) {
			        // We need to revalidate the end date
			        data.fv.revalidateField('indateTo');
			    }
			
			    if (data.field === 'indateTo' && !data.fv.isValidField('indateFrom')) {
			        // We need to revalidate the start date
			        data.fv.revalidateField('indateFrom');
			    }
			});
		});
	</script>
</body>
</html>