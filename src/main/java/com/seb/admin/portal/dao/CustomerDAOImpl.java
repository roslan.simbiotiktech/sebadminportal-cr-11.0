package com.seb.admin.portal.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.Subscription;
import com.seb.admin.portal.model.User;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

	@PersistenceContext(unitName = ApplicationConstant.UNIT_NAME, type = PersistenceContextType.EXTENDED)
	private EntityManager em;
	private static final Logger log = Logger.getLogger(CustomerDAOImpl.class);

	@Override
	public Subscription findSelectedSubscription(String email, String accountNumber, String status) throws SQLException{
		Subscription results = new Subscription();
		try{
			Query query =  (Query) em.createNamedQuery("Subscription.findSelectedSubscription")
					.setParameter("email", email)
					.setParameter("accountNumber", accountNumber)
					.setParameter("status", status);
			results = (Subscription) query.getSingleResult();		
			em.refresh(results);
		}catch(NoResultException ex){
			results = null;}
		return results;		
	}

	@Override
	public boolean updateSubscriptionStatus(int subsId, String subscriptionStatus, String remark) throws SQLException{
		try{	
			Subscription update = em.find(Subscription.class, subsId);
			update.setStatus(subscriptionStatus);
			update.setRemark(remark);
			em.merge(update);
			return true;

		}catch(Exception e){
			return false;
		}
	}

	@Override
	public boolean updateUserProfile(String loginId, String userStatus, String preferredMethod) throws SQLException{

		try{

			String commMethod = (preferredMethod==null || preferredMethod == "" )? "MOBILE" : preferredMethod;

			CriteriaBuilder cb = this.em.getCriteriaBuilder();
			// create update
			CriteriaUpdate<User> update = cb.createCriteriaUpdate(User.class);
			// set the root class
			Root<User> root = update.from(User.class);

			List<Predicate> predicates = new ArrayList<Predicate>();

			predicates.add(cb.equal(root.get("userId"), loginId));

			// set update and where clause
			update.set("preferredCommunicationMethod", commMethod);
			update.set("status", userStatus);
			update.where(predicates.toArray(new Predicate[]{}));

			// perform update
			em.createQuery(update).executeUpdate();
			em.getEntityManagerFactory().getCache().evictAll();
			return true;
		}catch(NoResultException ex){
		
			return false;
		}catch (Exception e) {
			log.error("updateUserProfile", e);
			return false;
		}

		/*try{	
			System.out.println("loginId>"+loginId);
			System.out.println("userStatus>"+userStatus);
			System.out.println("preferredMethod>"+preferredMethod);
			User update = em.find(User.class, loginId);
			update.setPreferredCommunicationMethod(preferredMethod);
			update.setStatus(userStatus);
			//update.setRemark(remark);
			em.merge(update);
			return true;

		}catch(Exception e){
			e.printStackTrace();
			return false;
		}*/
	}

	@Override
	public boolean updateUserSubscription(String email, String oldStatus, String newStatus) throws SQLException{

		try{

			CriteriaBuilder cb = this.em.getCriteriaBuilder();
			// create update
			CriteriaUpdate<Subscription> update = cb.createCriteriaUpdate(Subscription.class);
			// set the root class
			Root<Subscription> root = update.from(Subscription.class);

			List<Predicate> predicates = new ArrayList<Predicate>();

			predicates.add(cb.equal(root.get("status"), oldStatus));
			predicates.add(cb.equal(root.get("userEmail"), email));

			// set update and where clause
			update.set("status", newStatus);
			update.where(predicates.toArray(new Predicate[]{}));

			// perform update
			//Query q = this.em.createQuery(update);
			//q.executeUpdate();
			em.createQuery(update).executeUpdate();
			em.getEntityManagerFactory().getCache().evictAll();
			return true;
		}catch(NoResultException ex){
			ex.printStackTrace();
		}
		/*Subscription results = new Subscription();
		try{
			Query query =  (Query) em.createNamedQuery("Subscription.findSubscriptionByEmail")
					.setParameter("email", email);
			results = (Subscription) query.getSingleResult();		
			em.refresh(results);

			if(results!=null){
				Subscription update = em.find(Subscription.class, results.getSubsId());
				update.setStatus(subStatus);
				em.merge(update);
				return true;
			}
		}catch(NoResultException ex){
			results = null;
		}*/
		return false;
	}


}
