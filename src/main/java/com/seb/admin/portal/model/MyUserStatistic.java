package com.seb.admin.portal.model;

import java.util.Date;

public class MyUserStatistic {
	
	private Date dateFrom;
	private Date dateTo;
	private int totalActive;
	private int totalSuspended;
	private int totalPending;
		
	
	public MyUserStatistic(Date dateFrom, Date dateTo, int totalActive, int totalSuspended, int totalPending) {
		super();
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.totalActive = totalActive;
		this.totalSuspended = totalSuspended;
		this.totalPending = totalPending;
	}
	
	public MyUserStatistic() {
		// TODO Auto-generated constructor stub
	}

	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Date getDateTo() {
		return dateTo;
	}
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	public int getTotalActive() {
		return totalActive;
	}
	public void setTotalActive(int totalActive) {
		this.totalActive = totalActive;
	}
	public int getTotalSuspended() {
		return totalSuspended;
	}
	public void setTotalSuspended(int totalSuspended) {
		this.totalSuspended = totalSuspended;
	}
	public int getTotalPending() {
		return totalPending;
	}
	public void setTotalPending(int totalPending) {
		this.totalPending = totalPending;
	}

	@Override
	public String toString() {
		return "MyUserStatistic [dateFrom=" + dateFrom + ", dateTo=" + dateTo + ", totalActive=" + totalActive
				+ ", totalSuspended=" + totalSuspended + ", totalPending=" + totalPending + "]";
	}
	
	

}
