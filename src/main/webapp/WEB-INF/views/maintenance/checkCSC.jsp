<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include  file="../global.jsp"%>
</head>

<body>
    <div id="wrapper">
        <!-- Navigation -->

        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
        </nav>

        <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp"%>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="font-weight: bold">
                            Maintenance &gt; Service Counter Locator &gt; Edit
                        </div>

                        <div class="panel-body">
                            <form id="checkerCSCForm" name="checkerCSCForm" method="post" action="${pageContext.request.contextPath}/admin/checkCSC.do" class="form-horizontal">
                                <div class="form-group required">
                                    <p class="col-lg-5 control-label">Status <span style="color: red;">*</span> :</p>

                                    <div class="col-lg-3">
                                        <select class="form-control capital" id="selectStatus" name="selectStatus">
                                            <option value="A">
                                                Approved
                                            </option>
                                            <option value="P">
                                                Pending
                                            </option>
                                            <option value="R">
                                                Rejected
                                            </option>
                                        </select>
                                    </div>
                                </div>
                              
                                <div class="form-group">
                                    <div class="col-lg-5 col-lg-offset-3 text-right">
                                        <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> <button class="btn btn-primary" type="submit">Search</button> <button class="btn btn-primary" id="cancelBtn1" type="reset">Cancel</button>
                                    </div>
                                </div>
                            </form>

                            <div hidden="true" id="resultListDiv" style="margin-top: 10%">
                                <h4>Search Type : ${searchType}, Label : ${label }</h4>

                                <table cellspacing="0" class="table table-striped table-bordered" id="tblProfile" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>

                                            <th>Position</th>

                                            <th>Office</th>

                                            <th>Age</th>

                                            <th>Start date</th>

                                            <th>Salary</th>
                                        </tr>
                                    </thead>

                                    <tfoot>
                                        <tr>
                                            <th>Name</th>

                                            <th>Position</th>

                                            <th>Office</th>

                                            <th>Age</th>

                                            <th>Start date</th>

                                            <th>Salary</th>
                                        </tr>
                                    </tfoot>

                                    <tbody>
                                        <tr>
                                            <td>${i}.TigerNixon</td>

                                            <td>
                                                <a>System Architect</a>
                                            </td>

                                            <td>Edinburgh</td>

                                            <td>61</td>

                                            <td>2011/04/25</td>

                                            <td>$320,800</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><!-- /.resultList -->                           
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /#page-wrapper -->
    </div><script type="text/javascript">

    $(document).ready(function() {     
    
     $("#cancelBtn1").click(function(){
         $('#checkerCSCForm').find('.has-error').removeClass("has-error");
            $('#checkerCSCForm').find('.has-success').removeClass("has-success");
            $('#checkerCSCForm').find('.form-control-feedback').removeClass('glyphicon-remove');
            $('#checkerCSCForm').find('.form-control-feedback').removeClass('glyphicon-ok');
            $('.form-group').find('small.help-block').hide();     
        });
     
     $('#checkerCSCForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
                msisdn: {
                    validators: {
                        notEmpty: {
                            message: 'Mobile Number is required'
                        },
                        stringLength: {
                            min: 10,
                            max: 20,
                            message: 'Mobile Number must at least 10 digits'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_]+$/,
                            message: 'The Name can only consist of alphabetical, number and underscore'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Name is required'
                        }                       
                    }
                },
                loginID: {
                    validators: {
                        notEmpty: {
                            message: 'LoginID is required'
                        }                       
                    }
                },
                contractAccNo: {
                    validators: {
                        notEmpty: {
                            message: 'Contract Account Number is required'
                        }                       
                    }
                },
                email: {
                    validators: {
                        emailAddress: {
                            message: 'The value is not a valid email address'
                        }
                    }
                }              
            }
        });
    });
    </script>
</body>
</html>