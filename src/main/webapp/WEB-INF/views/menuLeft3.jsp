<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<!-- <li class="sidebar-search">
				
				<div class="input-group custom-search-form">
					<input type="text" class="form-control" placeholder="Search...">
					<span class="input-group-btn">
					<button class="btn btn-default" type="button">
						<i class="fa fa-search"></i>
					</button>
					</span>
				</div>

			</li> -->
			 <li>
				<a class="sidebarlink" href="${pageContext.request.contextPath}/mainLogin.do"><img src="${pageContext.request.contextPath}/resources/images/sidebar/home.png" alt="" width="32px"  height="32px" />Home</a>
			</li> 
			<li>
				<a class="sidebarlink" href="1"><img src="${pageContext.request.contextPath}/resources/images/sidebar/customer.png" alt="" width="33px"  height="33px" /><spring:message code="menu.customer" /><span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a class="sidebarlink"  href="${pageContext.request.contextPath}/admin/customerProfile.do"><spring:message code="submenu.customer.profile" /></a></li>
					<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/customerHistory.do"><spring:message code="submenu.customer.history" /></a></li>
					<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/customerResetPassword.do"><spring:message code="submenu.customer.resetPassword" /></a></li>
				</ul>
			</li>							
			<li>
				<a class="sidebarlink" href="2"><img src="${pageContext.request.contextPath}/resources/images/sidebar/maintenance.png" alt="" width="32px"  height="32px" />Maintenance<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a class="sidebarlink" href="#">Customer Service Counter<span class="fa arrow"></span></a>                      
                        <ul class="nav nav-third-level">
                        	<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/addCSC.do">Add</a></li>
                            <li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/editCSC.do">Edit</a></li>
						</ul>						
					</li>
				 <li><a class="sidebarlink" href="#">News<span class="fa arrow"></span></a>                      
                        <ul class="nav nav-third-level">
                        	<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/addNews.do">Add</a></li>
                            <li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/editNews.do">Edit</a></li>
						</ul>						
				</li>
				<li><a class="sidebarlink" href="#">Power Alert<span class="fa arrow"></span></a>                      
                        <ul class="nav nav-third-level">
                        	<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/addPowerAlert.do">Add</a></li>
                            <li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/editPowerAlert.do">Edit</a></li>
						</ul>						
				</li>
				<li><a class="sidebarlink" href="#">Push Notification<span class="fa arrow"></span></a>                      
                        <ul class="nav nav-third-level">
                        	<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/addPushNotification.do">Add</a></li>
                            <li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/editPushNotification.do">Edit</a></li>
						</ul>						
				</li>
				<li><a class="sidebarlink" href="#">FAQ<span class="fa arrow"></span></a>                      
                        <ul class="nav nav-third-level">
                        	<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/addFAQ.do">Add</a></li>
                            <li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/editFAQ.do">Edit</a></li>
						</ul>						
				</li>
				<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/contactUs.do">Contact Us</a></li>
				</ul>
			</li>
			<li>
				<a class="sidebarlink" href="1">&nbsp; &nbsp;<span class="glyphicon glyphicon-user" aria-hidden="true"></span> &nbsp;User Management<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/addUser.do">Add User</a></li>
					<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/editUser.do">Update User</a></li>
					<li><a class="sidebarlink" href="${pageContext.request.contextPath}/#">Module Access Management</a></li>
				</ul>
			</li>
			<li>
				<a class="sidebarlink" href="${pageContext.request.contextPath}/admin/policy.do">&nbsp; &nbsp;<span class="glyphicon glyphicon glyphicon-file" aria-hidden="true"></span> &nbsp;Policy</a>
			</li>
			<li>
				<a class="sidebarlink" href="1"><img src="${pageContext.request.contextPath}/resources/images/sidebar/report.png" alt="" width="32px"  height="32px" />Report<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/makeAReport.do">Make a Report</a></li>
					<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/customerActivityReport.do">Customer Activities Report</a></li>
					<li><a class="sidebarlink" href="${pageContext.request.contextPath}/admin/auditTrails.do">Audit Trails</a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>