package com.seb.admin.portal.spg.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OFpx;
import com.seb.admin.portal.spg.entity.ResponseStatus;

public class GatewayFPXResponse {
	
	@JsonProperty("status")
	private ResponseStatus responseStatus;
	@JsonProperty("fpx")
	private OFpx fpxGateway;
	
	
	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}
	public OFpx getFpxGateway() {
		return fpxGateway;
	}
	public void setFpxGateway(OFpx fpxGateway) {
		this.fpxGateway = fpxGateway;
	}
	@Override
	public String toString() {
		return "responseStatus:[" + responseStatus + "], fpxGateway:[" + fpxGateway + "]";
	}
	
	
	
	

}
