package com.seb.admin.portal.spg.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OPaymentDetailReport {
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("direction")
	private String direction;
	
	@JsonProperty("remark")
	private String remark;
	
	@JsonProperty("created_datetime")
	private String created_datetime;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreated_datetime() {
		return created_datetime;
	}

	public void setCreated_datetime(String created_datetime) {
		this.created_datetime = created_datetime;
	}

	@Override
	public String toString() {
		return "status:[" + status + "], direction:[" + direction + "], remark:[" + remark + "], created_datetime:["
				+ created_datetime + "]";
	}
	
	
}
