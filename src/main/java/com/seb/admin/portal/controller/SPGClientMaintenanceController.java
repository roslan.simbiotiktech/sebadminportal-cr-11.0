package com.seb.admin.portal.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.seb.admin.portal.constant.SPGConstant;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.service.ReportService;
import com.seb.admin.portal.spg.apis.SPGAPIServices;
import com.seb.admin.portal.spg.entity.OMerchantBase;
import com.seb.admin.portal.spg.request.MerchantCreateRequest;
import com.seb.admin.portal.spg.request.MerchantQueryRequest;
import com.seb.admin.portal.spg.response.EditMerchantResponse;
import com.seb.admin.portal.spg.response.JSONResponse;
import com.seb.admin.portal.spg.response.MerchantResponse;
import com.seb.admin.portal.util.StringUtil;

import retrofit2.Response;

@Controller
@SessionAttributes( {"clientList"})
@RequestMapping("/admin/spg/pymtClientMaintenance")
public class SPGClientMaintenanceController {	

	private static final Logger log = Logger.getLogger(SPGClientMaintenanceController.class);

	@Autowired
	private ReportService reportService;
	@Autowired
	private SPGAPIServices SPGAPIServices;


	@RequestMapping(value = "/add")
	public ModelAndView doAddPaymentClientMaintenance() {

		return new ModelAndView("/spg-maintenance/addSPGClientMaintenance");
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView doAddPaymentClientMaintenance(
			@RequestParam(value="clientName", defaultValue = "") String clientName,
			@RequestParam(value="description", defaultValue = "") String description,
			@RequestParam(value="hashKey", defaultValue = "") String hashKey,
			@RequestParam(value="statusUrl", defaultValue = "") String statusUrl,
			@RequestParam(value="redirectUrl", defaultValue = "") String redirectUrl,
			@RequestParam(value="statusOpt", defaultValue = "") String statusOpt,
			HttpServletRequest request, HttpServletResponse resp){

		ModelAndView model = new ModelAndView();

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		try
		{				
			for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
				String fieldName = (String) e.nextElement();
				String fieldValue = request.getParameter(fieldName);
				if ((fieldValue != null) && (fieldValue.length() > 0)) {
					System.out.println(fieldName + ":" + fieldValue);

				}
			}	

			MerchantCreateRequest mrr = new MerchantCreateRequest();
			mrr.setUid(SPGConstant.apiUID);
			mrr.setPw(SPGConstant.apiPassword);
			mrr.setAdmin_id(user.getLoginId());

			OMerchantBase merchant = new OMerchantBase();
			merchant.setName(StringUtil.trimToEmpty(clientName));
			merchant.setDescription(StringUtil.trimToEmpty(description));
			merchant.setSignature_secret(StringUtil.trimToEmpty(hashKey));
			merchant.setServer_to_server_payment_update_url(StringUtil.trimToEmpty(statusUrl));
			merchant.setClient_payment_update_url(StringUtil.trimToEmpty(redirectUrl));
			merchant.setEnabled(true);
			merchant.setCreated_by(user.getLoginId());
			merchant.setLast_updated_by(user.getLoginId());
			mrr.setMerchant(merchant);

			Response<MerchantResponse> response = SPGAPIServices.executeCreateMerchant(mrr, StringUtil.getLanguage(""));

			if(response.isSuccessful() && response.body().getResponseStatus().isSuccess()) {
				model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully added new client."));
				reportService.auditLog("Payment Client Maintenance", new AuditTrail(user.getId(),0,"","Add Payment Client: "+user.getLoginId(),"", response.raw().request().url().toString(), mrr.toString()));		

			}else {
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", StringUtil.trimToEmpty(response.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(response.body().getResponseStatus().getMessages())));

			}


		}catch(Exception ex){
			ex.printStackTrace();
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to add new client"));
			reportService.auditLog("Payment Client Maintenance", new AuditTrail(user.getId(),0,"","Add Payment Client: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
			log.error(ex.getMessage());
			ex.printStackTrace();
		}		
		model.setViewName("/spg-maintenance/addSPGClientMaintenance");
		return model;
	}

	@RequestMapping(value = "/edit")
	public ModelAndView doEditPaymentClientMaintenance(HttpServletRequest req, HttpServletResponse resp) {
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();

		req.getSession().removeAttribute("tableMerchantList");

		MerchantCreateRequest mrr = new MerchantCreateRequest();
		mrr.setUid(SPGConstant.apiUID);
		mrr.setPw(SPGConstant.apiPassword);
		mrr.setAdmin_id(user.getLoginId());

		Response<EditMerchantResponse> response;

		try {
			response = SPGAPIServices.executeListMerchant(mrr, StringUtil.getLanguage(""));
			log.info("[doEditPaymentClientMaintenance]"+response.raw().request().url().toString());

			if(response.isSuccessful() && response.body().getResponseStatus().isSuccess()) {
				List<OMerchantBase> merchantList = response.body().getMerchant();

				req.getSession().setAttribute("merchantList", merchantList);
				model.addObject("clientList", merchantList.stream().sorted(Comparator.comparing(OMerchantBase::getName)).collect(Collectors.toList()));
			}
			else {
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", StringUtil.trimToEmpty(response.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(response.body().getResponseStatus().getMessages())));

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.setViewName("/spg-maintenance/editSPGClientMaintenance");
		return model;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public ModelAndView doEditPaymentClientMaintenance(	
			@RequestParam(value="clientName", defaultValue = "") String clientName,
			HttpServletRequest req, HttpServletResponse resp, Model modelMap){

		ModelAndView model = new ModelAndView();

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		List<OMerchantBase> merchantList = new ArrayList<OMerchantBase>();
		Response<EditMerchantResponse> response;
		try
		{				
			for (Enumeration e = req.getParameterNames(); e.hasMoreElements();) {
				String fieldName = (String) e.nextElement();
				String fieldValue = req.getParameter(fieldName);
				if ((fieldValue != null) && (fieldValue.length() > 0)) {
					System.out.println(fieldName + ":" + fieldValue);

				}
			}	

			MerchantCreateRequest mrr = new MerchantCreateRequest();
			mrr.setUid(SPGConstant.apiUID);
			mrr.setPw(SPGConstant.apiPassword);
			mrr.setAdmin_id(user.getLoginId());

			response = SPGAPIServices.executeListMerchant(mrr, StringUtil.getLanguage(""));

			if(response.isSuccessful() && response.body().getResponseStatus().isSuccess()) {
				merchantList = response.body().getMerchant();
				req.getSession().setAttribute("merchantList", merchantList);
				model.addObject("clientList", merchantList.stream().sorted(Comparator.comparing(OMerchantBase::getName)).collect(Collectors.toList()));
			}

			System.out.println("merchantList>"+merchantList.size());

			if(clientName.equalsIgnoreCase("")) {
				req.getSession().setAttribute("tableMerchantList", merchantList);	
			}else {
				req.getSession().setAttribute("tableMerchantList", merchantList.stream()
						.filter(p -> p.getName().equalsIgnoreCase(clientName)).collect(Collectors.toList()));
			}
		}catch(Exception ex){
			ex.printStackTrace();
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search payment client maintenance"));
			reportService.auditLog("Payment Client Maintenance", new AuditTrail(user.getId(),0,"","Edit Payment Client: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
			log.error(ex.getMessage());
			ex.printStackTrace();
		}		

		model.addObject("clientName", clientName);
		model.setViewName("/spg-maintenance/editSPGClientMaintenance");
		return model;
	}

	@RequestMapping(value = "/merchantQuery",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody <T> JSONResponse<?> merchantQuery(@RequestBody OMerchantBase form, HttpServletRequest req, HttpServletResponse resp) {	
		log.info("***********************************************************************************************");
		log.info("[merchantQuery]");
		log.info("form >>"+form.toString());

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		JSONResponse<?> jsonResponse = new JSONResponse<>();

		try {
			MerchantQueryRequest mqr = new MerchantQueryRequest();
			mqr.setMerchantId(form.getId());

			Response<MerchantResponse> response = SPGAPIServices.executeQueryMerchant(mqr, StringUtil.getLanguage(""));
			jsonResponse.setSuccess(true);

			if(response.isSuccessful() && response.body().getResponseStatus().isSuccess()) {
				ObjectMapper mapper = new ObjectMapper();
				OMerchantBase a = mapper.convertValue(response.body().getMerchant(), OMerchantBase.class);
				jsonResponse.setObjectDetail(a);
				req.getSession().setAttribute("selectedClient", a);

			}else {
				jsonResponse.setSuccess(false);
				jsonResponse.setStatusCode(-1);
				jsonResponse.setMessage(Arrays.toString(response.body().getResponseStatus().getMessages()));
				reportService.auditLog("Payment Client Maintenance", new AuditTrail(user.getId(),0,"","Edit Payment Client: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+response.raw().request().url().toString()+"\n"+StringUtil.trimToEmpty(response.body().getResponseStatus().getProcess_code())+"\n"+Arrays.toString(response.body().getResponseStatus().getMessages())));		

			}

		} catch (Exception e) {
			reportService.auditLog("Payment Client Maintenance", new AuditTrail(user.getId(),0,"","Edit Payment Client: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+e.getMessage()));		
			log.error(this.getClass().getSimpleName(), e);
		}

		return jsonResponse;
	}
	
	@RequestMapping(value = "/doUpdatePaymentClient",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody JSONResponse<?> doUpdatePaymentClient(@RequestBody OMerchantBase re, HttpServletRequest req, HttpServletResponse resp){

		log.info("[doUpdatePaymentClient]");
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		JSONResponse<?> jsonResponse = new JSONResponse<>();

		try {
			
			MerchantCreateRequest updateMerchant = new MerchantCreateRequest();
			updateMerchant.setAdmin_id(user.getLoginId());
			
			OMerchantBase base = new OMerchantBase();
			base.setId(re.getId());
			base.setName(StringUtil.trimToEmpty(re.getName()));
			base.setDescription(StringUtil.trimToEmpty(re.getDescription()));
			base.setSignature_secret(StringUtil.trimToEmpty(re.getSignature_secret()));
			base.setServer_to_server_payment_update_url(StringUtil.trimToEmpty(re.getServer_to_server_payment_update_url()));
			base.setClient_payment_update_url(StringUtil.trimToEmpty(re.getClient_payment_update_url()));
			base.setLast_updated_by(user.getLoginId());
			
			System.out.println("enabled ::"+re.isEnabled());
			base.setEnabled(re.isEnabled());
			updateMerchant.setMerchant(base);

			OMerchantBase existClient = (OMerchantBase) req.getSession().getAttribute("selectedClient");
			
			
			Response<MerchantResponse> response = SPGAPIServices.executeUpdateMerchant(updateMerchant, StringUtil.getLanguage(""));

			if(response.isSuccessful() && response.body().getResponseStatus().isSuccess()) {
				jsonResponse.setSuccess(true);
				jsonResponse.setMessage("["+ re.getName()+"] update successfully.");
				
				ObjectMapper mapper = new ObjectMapper();
				OMerchantBase a = mapper.convertValue(response.body().getMerchant(), OMerchantBase.class);
				jsonResponse.setObjectDetail(a);
				
				reportService.auditLog("Payment Client Maintenance", new AuditTrail(user.getId(),0,"","Edit Payment Client: "+user.getLoginId(),existClient.toString(), base.toString(), "API:"+response.raw().request().url().toString()));		

			}else {
				jsonResponse.setSuccess(false);
				jsonResponse.setStatusCode(-1);
				jsonResponse.setMessage(Arrays.toString(response.body().getResponseStatus().getMessages()));
			}

		} catch (Exception e) {
			jsonResponse.setSuccess(false);
			jsonResponse.setStatusCode(-1);
			jsonResponse.setMessage(SPGConstant.API_ERROR);
			reportService.auditLog("Payment Client Maintenance", new AuditTrail(user.getId(),0,"","Edit Payment Client: "+user.getLoginId(),"", "Fail to process", "Fail-Message:"+e.getCause()+"::"+e.getMessage()));	
			log.error(this.getClass().getSimpleName(), e);
		}finally {
			req.getSession().removeAttribute("selectedClient");
		}
		return jsonResponse;
	} 	



}
