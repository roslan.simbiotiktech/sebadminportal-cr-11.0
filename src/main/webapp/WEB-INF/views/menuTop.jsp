<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="navbar-header">
	<a href="${pageContext.request.contextPath}/mainLogin.do"><img src="${pageContext.request.contextPath}/resources/images/logo.png" alt="" width="157" height="50" /></a>

</div>
<ul class="nav navbar-top-links navbar-right">
	<li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#">
			 ${userSession.loginId}  <i class="fa fa-user fa-fw"></i><i class="fa fa-caret-down"></i>
		</a>
		<ul class="dropdown-menu dropdown-user">
		<sec:authorize access="hasRole('ROLE_A')">
			<li><a href="${pageContext.request.contextPath}/admin/changePassword.do"><span class="glyphicon glyphicon-lock"></span> Change Password</a></li>		
			<li class="divider"></li>
			</sec:authorize>
			<li><form name="logoutForm" action="<c:url value='/logout' />" method="post"><a href="javascript:document.logoutForm.submit();"><i class="fa fa-sign-out fa-fw"></i> Logout</a><input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />	</form>
			
			
			</li>
		</ul>
	</li>			
</ul>
