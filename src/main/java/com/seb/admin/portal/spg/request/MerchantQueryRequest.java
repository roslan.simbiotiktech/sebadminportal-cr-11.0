package com.seb.admin.portal.spg.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;

public class MerchantQueryRequest extends SPGAPIGeneral {
	
	@JsonProperty("merchant_id")
	private int merchantId;

	public int getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	@Override
	public String toString() {
		return "merchantId:[" + merchantId + "]";
	}

	
	
}
