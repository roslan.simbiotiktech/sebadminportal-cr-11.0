package com.seb.admin.portal.model;

import java.math.BigDecimal;

public class MyTariffSettings {
	private String from;
	private String to;
	private String sen;
	
	
	private BigDecimal bigDecimalSen;
	
	
	public MyTariffSettings() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public MyTariffSettings(String from, String to, String sen) {
		super();
		this.from = from;
		this.to = to;
		this.sen = sen;
	}


	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getSen() {
		return sen;
	}
	public void setSen(String sen) {
		this.sen = sen;
	}
	
	
	public BigDecimal getBigDecimalSen() {
		BigDecimal RM = new BigDecimal(getSen());
		return RM;
	}

	public void setBigDecimalSen(BigDecimal bigDecimalSen) {
		this.bigDecimalSen = bigDecimalSen;
	}

	@Override
	public String toString() {
		return "MyTariffSettings [from=" + from + ", to=" + to + ", sen=" + sen + "]";
	}
	
	
	

}
