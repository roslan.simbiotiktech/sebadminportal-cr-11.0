package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;
import java.util.Arrays;


/**
 * The persistent class for the NEWS database table.
 * 
 */
@Entity
@Table(name="NEWS" , schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="News.findNews", query="SELECT n FROM News n WHERE n.deletedFlag=0"),
	@NamedQuery(name="News.findNewsByTitle", query="SELECT n FROM News n WHERE UPPER(n.title) LIKE :title AND n.startDatetime >=:startDate AND  n.endDatetime<=:endDate AND n.deletedFlag=0"),
	@NamedQuery(name="News.findNewsTitleByEndDate", query="SELECT n FROM News n WHERE n.endDatetime > CURRENT_TIMESTAMP  AND n.deletedFlag=0"),
	@NamedQuery(name="News.findNewsIds", query="SELECT n FROM News n WHERE n.id  IN (:ids) AND n.endDatetime > CURRENT_TIMESTAMP AND n.deletedFlag=0")


})
public class News implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(name="CREATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Lob
	private String description;

	@Column(name="END_DATETIME", nullable=false, insertable = true, updatable = true)
	private Timestamp endDatetime;

	@Lob
	@Column(name="IMAGE_L")
	private byte[] imageL;

	@Column(name="IMAGE_L_FILE_TYPE", length=20)
	private String imageLFileType;

	@Lob
	@Column(name="IMAGE_S")
	private byte[] imageS;

	@Column(name="IMAGE_S_FILE_TYPE", length=20)
	private String imageSFileType;

	@Column(name="PUBLISH_DATETIME", nullable=false, insertable = true, updatable = true)
	private Timestamp publishDatetime;

	@Column(name="START_DATETIME", nullable=false, insertable = true, updatable = true)
	private Timestamp startDatetime;

	@Column(length=100)
	private String title;

	@Column(name="UPDATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp updatedDatetime;

	public News() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Timestamp endDatetime) {
		this.endDatetime = endDatetime;
	}

	public byte[] getImageL() {
		return this.imageL;
	}

	public void setImageL(byte[] imageL) {
		this.imageL = imageL;
	}

	public String getImageLFileType() {
		return this.imageLFileType;
	}

	public void setImageLFileType(String imageLFileType) {
		this.imageLFileType = imageLFileType;
	}

	public byte[] getImageS() {
		return this.imageS;
	}

	public void setImageS(byte[] imageS) {
		this.imageS = imageS;
	}

	public String getImageSFileType() {
		return this.imageSFileType;
	}

	public void setImageSFileType(String imageSFileType) {
		this.imageSFileType = imageSFileType;
	}

	public Timestamp getPublishDatetime() {
		return this.publishDatetime;
	}

	public void setPublishDatetime(Timestamp publishDatetime) {
		this.publishDatetime = publishDatetime;
	}

	public Timestamp getStartDatetime() {
		return this.startDatetime;
	}

	public void setStartDatetime(Timestamp startDatetime) {
		this.startDatetime = startDatetime;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", createdDatetime=" + createdDatetime + ", deletedFlag=" + deletedFlag
				+ ", description=" + description + ", endDatetime=" + endDatetime + ", imageLFileType=" + imageLFileType
				+ ", imageSFileType=" + imageSFileType + ", publishDatetime=" + publishDatetime + ", startDatetime="
				+ startDatetime + ", title=" + title + ", updatedDatetime=" + updatedDatetime + ", getId()=" + getId()
				+ ", getCreatedDatetime()=" + getCreatedDatetime() + ", getDeletedFlag()=" + getDeletedFlag()
				+ ", getDescription()=" + getDescription() + ", getEndDatetime()=" + getEndDatetime() + ", getImageL()="
				+ Arrays.toString(getImageL()) + ", getImageLFileType()=" + getImageLFileType() + ", getImageS()="
				+ Arrays.toString(getImageS()) + ", getImageSFileType()=" + getImageSFileType()
				+ ", getPublishDatetime()=" + getPublishDatetime() + ", getStartDatetime()=" + getStartDatetime()
				+ ", getTitle()=" + getTitle() + ", getUpdatedDatetime()=" + getUpdatedDatetime() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
	

}