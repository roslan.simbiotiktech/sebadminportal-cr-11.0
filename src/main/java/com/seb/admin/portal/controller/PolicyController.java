package com.seb.admin.portal.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.model.Setting;
import com.seb.admin.portal.service.ReportService;
import com.seb.admin.portal.service.SettingService;
import com.seb.admin.portal.util.StringUtil;

@Controller
public class PolicyController {

	private static final Logger log = Logger.getLogger(PolicyController.class);

	@Autowired
	private ReportService reportService;
	@Autowired
	private SettingService settingService;

	@RequestMapping(value = "/admin/policy",  method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView updatePolicy(
			@RequestParam(value="expiry", defaultValue = "") String expiry)	{
		
		
		ModelAndView model = new ModelAndView();

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		Setting oldSetting = new Setting();
		try{
			
			oldSetting = settingService.findByKey(ApplicationConstant.ACTIVATION_CODE_EXPIRE_MINUTES);
			String oldValue = oldSetting.getSettingValue();

			if(!expiry.isEmpty() && oldSetting!=null){
				Setting newSetting = new Setting();
				
				newSetting.setSettingKey("ACTIVATION_CODE_EXPIRE_MINUTES");
				newSetting.setSettingValue(expiry);
				
				settingService.updateSetting(newSetting);
				reportService.auditLog("Policy", new AuditTrail(user.getId(),0,"","Update Policy: "+user.getLoginId()+",Task ID:"+oldSetting.getSettingKey(),"expriy:["+oldValue+"]", "expriy:["+newSetting.getSettingValue()+"]", ""));		
				model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully update policy."));
				log.info("Successfully update policy.");
			}

			
		}catch (Exception ex) {
			reportService.auditLog("Policy", new AuditTrail(user.getId(),0,"","Update Policy: "+user.getLoginId(),"","Fail to process", "expriy:["+expiry+"]\nFail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to update policy."));
			ex.printStackTrace();
		}	

		model.addObject("oldSetting", oldSetting);
		model.setViewName("/policy/policy");
		return model;
		
		
		
	}
}