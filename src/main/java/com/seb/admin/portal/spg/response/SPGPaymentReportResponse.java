package com.seb.admin.portal.spg.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OPaymentReport;
import com.seb.admin.portal.spg.entity.Pagination;
import com.seb.admin.portal.spg.entity.ResponseStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SPGPaymentReportResponse {
	@JsonProperty("status")
	private ResponseStatus responseStatus;
	
	@JsonProperty("pagination")
	private Pagination pagination;
	
	@JsonProperty("payment_reports")
	private List<OPaymentReport> OPaymentReport;

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	public Pagination getPagination() {
		return pagination;
	}

	public void setPagination(Pagination pagination) {
		this.pagination = pagination;
	}

	public List<OPaymentReport> getOPaymentReport() {
		return OPaymentReport;
	}

	public void setOPaymentReport(List<OPaymentReport> oPaymentReport) {
		OPaymentReport = oPaymentReport;
	}
	
	
	

}
