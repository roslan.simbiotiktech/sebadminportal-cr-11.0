package com.seb.admin.portal.model;

import java.util.List;

public class MyBillPayReportResp implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	
	private int maxPage;
	private int totalRecords=0;
	private int page=1;
	
	private List <MyPayment> paymentList;
	

	public int getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(int maxPage) {
		this.maxPage = maxPage;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public List<MyPayment> getPaymentList() {
		return paymentList;
	}

	public void setPaymentList(List<MyPayment> paymentList) {
		this.paymentList = paymentList;
	}

	@Override
	public String toString() {
		return "MyBillPayReportResp [maxPage=" + maxPage + ", totalRecords=" + totalRecords + ", page=" + page
				+ ", paymentList=" + paymentList + "]";
	}
	
	

	
}
