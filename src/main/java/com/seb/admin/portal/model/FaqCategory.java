package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.seb.admin.portal.constant.ApplicationConstant;

import java.util.List;


/**
 * The persistent class for the FAQ_CATEGORY database table.
 * 
 */
@Entity
@Table(name="FAQ_CATEGORY" , schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="FaqCategory.findAllFaqCategory", query="SELECT f FROM FaqCategory f WHERE f.deletedFlag=0 ORDER BY f.description ASC")

})
public class FaqCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(name="CATEGORY_SEQUENCE", nullable=false)
	private int categorySequence;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(length=255)
	private String description;

	@Column(name="NAME", length=255)
	private String name;

	//bi-directional many-to-one association to Faq
	@OneToMany(mappedBy="faqCategory")
	private List<Faq> faqs;

	public FaqCategory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCategorySequence() {
		return this.categorySequence;
	}

	public void setCategorySequence(int categorySequence) {
		this.categorySequence = categorySequence;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonManagedReference //for children table
	public List<Faq> getFaqs() {
		return this.faqs;
	}

	public void setFaqs(List<Faq> faqs) {
		this.faqs = faqs;
	}

	public Faq addFaq(Faq faq) {
		getFaqs().add(faq);
		faq.setFaqCategory(this);

		return faq;
	}

	public Faq removeFaq(Faq faq) {
		getFaqs().remove(faq);
		faq.setFaqCategory(null);

		return faq;
	}

	

}