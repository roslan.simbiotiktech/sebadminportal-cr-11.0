package com.seb.admin.portal.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.seb.admin.portal.constant.ApplicationConstant;

public class StringUtil {
	public static boolean isNumeric(String str)  
	{  
		try  
		{  
			double d = Double.parseDouble(str);  
		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}  
		return true;  
	}


	public static String showCommonMsg (String type, String message)
	{
		String reply = "";
		if(type.equalsIgnoreCase("success")){
			reply = "<div id=\"successMsg\"  class=\"alert alert-success\" role=\"alert\"><span class=\"glyphicon glyphicon glyphicon-ok\" aria-hidden=\"true\"></span> <span class=\"sr-only\"></span> "+message+" </div>";
		}else if(type.equalsIgnoreCase("error")){
			reply = "<div id=\"errorMsg\" class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> <span class=\"sr-only\"></span> "+message+" </div>";

		}
		return reply;
	}

	public static Timestamp convertStringToTimestamp(String str_date, String format) {
		try {
			DateFormat formatter;
			formatter = new SimpleDateFormat(format);
			Date date = (Date) formatter.parse(str_date);
			java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

			return timeStampDate;
		} catch (ParseException e) {
			System.out.println("Exception :" + e);
			return null;
		}
	}

	public static String trimToEmpty(String str)
	{
		return str == null ? "" : str.trim();
	}
	
	public static String replaceHTML(String string) {
		string = string.replace("&eacute;", "�");
		string = string.replace("&nbsp;", " ");
		string = string.replace("&amp;", "&");
		string = string.replace("&lt;", "<");
		string = string.replace("&gt;", ">");
		string = string.replace("&apos;", "'");
		string = string.replace("&lsquo;", "'");
		string = string.replace("&rsquo;", "'");
		string = string.replace("&quot;", "\"");
		string = string.replace("&ldquo;", "\"");
		string = string.replace("&rdquo;", "\"");
		string = string.replace("&ndash;", "-");
		string = string.replace("&hellip;", "...");

		string = string.replaceAll("<.*?>","");

		return string;
	}

	public static String removeOfferings(String string) {
		string = string.replaceAll("\\[Offered.*\\]", "");
		string = string.replaceAll("\\[Also offered.*\\]", "");

		return string;
	}

	public static String cleanContent(String string) {
		string = replaceHTML(string);

		string = string.replaceAll("^, ","");
		string = string.replace("\"\"","\"");
		string = string.replace(" Details.","");
		string = string.replace("&#x27;s","");

		return string;
	}

	public static String getLink(String string) {
		string = string.replace("{\"result\":[\"","");
		string = string.replace("\"]}","");
		string = string.replace("\\/","/");

		return string;
	}
	
	public static String getLanguage(String language) {
		return ApplicationConstant.EN;
	}
}
