package com.seb.admin.portal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


/**
 * The persistent class for the PAYMENT_REFERENCE database table.
 * 
 */
@Entity
@Table(name="PAYMENT_REFERENCE")
@NamedQuery(name="PaymentReference.findAll", query="SELECT p FROM PaymentReference p")
@SqlResultSetMappings({
	@SqlResultSetMapping(name = "sebBillPaymentResult", classes = {
			@ConstructorResult(targetClass=com.seb.admin.portal.model.SEBBillPayment.class, columns = {
					@ColumnResult(name = "spgPaymentId", type = int.class), 
					@ColumnResult(name = "spgMerchantTransactionId", type = String.class), 
					@ColumnResult(name = "spgAmount", type = BigDecimal.class), 
					@ColumnResult(name = "spgGatewayTransactionId", type = String.class),
					@ColumnResult(name = "paymentId", type = int.class), 
					@ColumnResult(name = "amount", type = BigDecimal.class), 
					@ColumnResult(name = "bankId", type = String.class), 
					@ColumnResult(name = "contractAccountNumber", type = String.class), 
					@ColumnResult(name = "createdDatetime", type = String.class),
					@ColumnResult(name = "gatewayId", type = String.class), 
					@ColumnResult(name = "gatewayRef", type = String.class), 
					@ColumnResult(name = "gatewayStatus", type = String.class), 
					@ColumnResult(name = "isMobile", type = int.class), 
					@ColumnResult(name = "paymentDatetime", type = String.class),
					@ColumnResult(name = "status", type = String.class), 
					@ColumnResult(name = "updatedDatetime", type = String.class), 
					@ColumnResult(name = "userEmail", type = String.class), 
					@ColumnResult(name = "spgBankID", type = String.class),
					@ColumnResult(name = "spgBankName", type = String.class), 
					@ColumnResult(name = "spgBankType", type = String.class), 
					@ColumnResult(name = "spgCardType", type = String.class), 
					@ColumnResult(name = "referencePrefix", type = String.class),
					@ColumnResult(name = "notificationEmail", type = String.class)
			})
	})
})

public class PaymentReference implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PAYMENT_ID")
	private int paymentId;

	//@Column(name="BANK_ID")
	//private String bankId;

	@Column(name="CREATED_DATETIME", insertable=false, updatable=false)
	private Timestamp createdDatetime;

	@Column(name="GATEWAY_ID")
	private String gatewayId;

	@Column(name="GATEWAY_REF")
	private String gatewayRef;

	@Column(name="GATEWAY_STATUS")
	private String gatewayStatus;

	@Column(name="IS_MOBILE")
	private int isMobile;

	@Column(name="NOTIFICATION_EMAIL")
	private String notificationEmail;

	@Column(name="PAYMENT_DATETIME")
	private Timestamp paymentDatetime;

	@Column(name="REFERENCE_PREFIX")
	private String referencePrefix;

	@Column(name="SPG_BANK_ID")
	private Integer spgBankId;

	@Column(name="SPG_BANK_NAME")
	private String spgBankName;

	@Column(name="SPG_BANK_TYPE")
	private String spgBankType;

	@Column(name="SPG_CARD_TYPE")
	private String spgCardType;

	@Column(name="STATUS")
	private String status;

	@Column(name="UPDATED_DATETIME", insertable=false, updatable=false)
	private Timestamp updatedDatetime;

	@Column(name="USER_EMAIL")
	private String userEmail;
	
	@Column(name="EXTERNAL_GATEWAY_STATUS")
	private String externalGatewayStatus;

	//bi-directional many-to-one association to PaymentReferenceEntry
	@OneToMany(mappedBy="paymentReference")
	private List<PaymentReferenceEntry> paymentReferenceEntries;

	public PaymentReference() {
	}

	public int getPaymentId() {
		return this.paymentId;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getGatewayId() {
		return this.gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getGatewayRef() {
		return this.gatewayRef;
	}

	public void setGatewayRef(String gatewayRef) {
		this.gatewayRef = gatewayRef;
	}

	public String getGatewayStatus() {
		return this.gatewayStatus;
	}

	public void setGatewayStatus(String gatewayStatus) {
		this.gatewayStatus = gatewayStatus;
	}

	public int getIsMobile() {
		return this.isMobile;
	}

	public void setIsMobile(int isMobile) {
		this.isMobile = isMobile;
	}

	public String getNotificationEmail() {
		return this.notificationEmail;
	}

	public void setNotificationEmail(String notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	public Timestamp getPaymentDatetime() {
		return this.paymentDatetime;
	}

	public void setPaymentDatetime(Timestamp paymentDatetime) {
		this.paymentDatetime = paymentDatetime;
	}

	public String getReferencePrefix() {
		return this.referencePrefix;
	}

	public void setReferencePrefix(String referencePrefix) {
		this.referencePrefix = referencePrefix;
	}

	public Integer getSpgBankId() {
		return this.spgBankId;
	}

	public void setSpgBankId(Integer spgBankId) {
		this.spgBankId = spgBankId;
	}

	public String getSpgBankName() {
		return this.spgBankName;
	}

	public void setSpgBankName(String spgBankName) {
		this.spgBankName = spgBankName;
	}

	public String getSpgBankType() {
		return this.spgBankType;
	}

	public void setSpgBankType(String spgBankType) {
		this.spgBankType = spgBankType;
	}

	public String getSpgCardType() {
		return this.spgCardType;
	}

	public void setSpgCardType(String spgCardType) {
		this.spgCardType = spgCardType;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
	@JsonManagedReference //for children table
	public List<PaymentReferenceEntry> getPaymentReferenceEntries() {
		return this.paymentReferenceEntries;
	}

	public void setPaymentReferenceEntries(List<PaymentReferenceEntry> paymentReferenceEntries) {
		this.paymentReferenceEntries = paymentReferenceEntries;
	}

	public PaymentReferenceEntry addPaymentReferenceEntry(PaymentReferenceEntry paymentReferenceEntry) {
		getPaymentReferenceEntries().add(paymentReferenceEntry);
		paymentReferenceEntry.setPaymentReference(this);

		return paymentReferenceEntry;
	}

	public PaymentReferenceEntry removePaymentReferenceEntry(PaymentReferenceEntry paymentReferenceEntry) {
		getPaymentReferenceEntries().remove(paymentReferenceEntry);
		paymentReferenceEntry.setPaymentReference(null);

		return paymentReferenceEntry;
	}
	
	

	public String getExternalGatewayStatus() {
		return externalGatewayStatus;
	}

	public void setExternalGatewayStatus(String externalGatewayStatus) {
		this.externalGatewayStatus = externalGatewayStatus;
	}

	@Override
	public String toString() {
		return "paymentId:[" + paymentId + "], createdDatetime:[" + createdDatetime + "], gatewayId:[" + gatewayId
				+ "], gatewayRef:[" + gatewayRef + "], gatewayStatus:[" + gatewayStatus + "], isMobile:[" + isMobile
				+ "], notificationEmail:[" + notificationEmail + "], paymentDatetime:[" + paymentDatetime
				+ "], referencePrefix:[" + referencePrefix + "], spgBankId:[" + spgBankId + "], spgBankName:["
				+ spgBankName + "], spgBankType:[" + spgBankType + "], spgCardType:[" + spgCardType + "], status:["
				+ status + "], updatedDatetime:[" + updatedDatetime + "], userEmail:[" + userEmail
				+ "], externalGatewayStatus:[" + externalGatewayStatus + "]";
	}


	

}