package com.seb.admin.portal.controller;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.seb.admin.portal.model.AccessRight;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.model.LeftMenu;
import com.seb.admin.portal.model.PortalModule;
import com.seb.admin.portal.model.SubMenu;
import com.seb.admin.portal.service.LoginService;
import com.seb.admin.portal.service.ReportService;
import com.seb.admin.portal.service.UserMgmtService;

@Controller
@SessionAttributes( {"userSession","sessionMenuList"})

public class LoginController {	

	private static final Logger log = Logger.getLogger(LoginController.class);
	private static final String HOME_PAGE = "/admin/welcome.do";

	@Autowired
	private LoginService loginService;
	@Autowired
	private ReportService reportService;
	@Autowired
	private UserMgmtService userMgmtService;

	@RequestMapping(value="/" , method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView doDefault(HttpServletRequest request, HttpServletResponse response) throws IOException{
		checkAccess(request, response);
		return new ModelAndView("/main/mainLogin");
	}

	@RequestMapping(value="/mainLogin" , method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView doMain(HttpServletRequest request, HttpServletResponse response) throws IOException{
		checkAccess(request, response);
		return new ModelAndView("/main/mainLogin");
	}

	public void checkAccess(HttpServletRequest request, HttpServletResponse response) throws IOException{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null && !(auth instanceof AnonymousAuthenticationToken)) {
			response.sendRedirect(request.getContextPath() + HOME_PAGE);
		}
	}

	@RequestMapping(value="/index" , method = {RequestMethod.GET, RequestMethod.POST})
	public String index(Principal principal, HttpServletRequest request, HttpServletResponse response) throws IOException{
		
		//Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		//AdminUser user = (AdminUser) authentication.getPrincipal();			
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		
		try {
			if(user.getLoginId()!=null){
				log.info("["+user.getLoginId()+"] with role ["+user.getRole()+"] success logged into into portal.");		
				loginService.updateLastLoggedIn(user);
				reportService.auditLog("Login" , new AuditTrail(user.getId(),0,"","Login: "+user.getLoginId(),"", "", ""));
			}

		}catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
		}


		return "redirect:/admin/welcome.do";
	}

	/** welcome page after login**/
	@RequestMapping(value = "/admin/welcome", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView  doLogin(HttpServletRequest request) {
		ModelAndView model = new ModelAndView();
		//Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		//AdminUser user = (AdminUser) authentication.getPrincipal();			
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();		

		List<AccessRight> mainModule = new ArrayList<AccessRight>();
		List<PortalModule> level2ModuleList = new ArrayList<PortalModule>();

		List<LeftMenu> menuList = new ArrayList<LeftMenu>();


		try {

			mainModule = userMgmtService.findMainAccessById(user.getId(),user.getRole());
			for (AccessRight ac : mainModule){
				LeftMenu menu = new LeftMenu();	
				List<SubMenu> level2list = new ArrayList<SubMenu>();
				List<SubMenu> level3list = new ArrayList<SubMenu>();

				menu.setName(ac.getPortalModule().getName());				
				menu.setModuleCode(ac.getPortalModule().getModuleCode());
				menu.setCategory(ac.getPortalModule().getCategory());
				menu.setAccessUrl(ac.getPortalModule().getAccessUrl());
				menu.setImageContent(ac.getPortalModule().getImageContent());
				menu.setParentModuleCode(ac.getPortalModule().getParentModuleCode());
				menu.setLevel(ac.getPortalModule().getLevel());
				menu.setRole(ac.getPortalModule().getRole());

				level2ModuleList = userMgmtService.findSubModuleByModuleCode(user.getId(),ac.getPortalModule().getModuleCode(),2, user.getRole());

				if(level2ModuleList.size()>0){
					List<PortalModule> level3ModuleList = new ArrayList<PortalModule>();
					for(PortalModule level2 : level2ModuleList){
						SubMenu submenu2 = new SubMenu();
						submenu2.setName(level2.getName());
						submenu2.setModuleCode(level2.getModuleCode());
						submenu2.setCategory(level2.getCategory());
						submenu2.setAccessUrl(level2.getAccessUrl());
						submenu2.setImageContent(level2.getImageContent());
						submenu2.setParentModuleCode(level2.getParentModuleCode());
						submenu2.setLevel(level2.getLevel());
						submenu2.setRole(level2.getRole());

						level2list.add(submenu2);
						level3ModuleList = userMgmtService.findSubModuleByModuleCode(user.getId(), level2.getModuleCode(),3, user.getRole());
						//System.out.println("level3ModuleList: select * from SEB.PORTAL_MODULE WHERE PARENT_MODULE_CODE='"+level2.getModuleCode()+"' and level=3;"+">>>>>>>>>>>"+level3ModuleList.size());


						if(level3ModuleList.size()>0){
							for(PortalModule level3 : level3ModuleList){
								SubMenu submenu3 = new SubMenu();
								submenu3.setName(level3.getName());
								submenu3.setModuleCode(level3.getModuleCode());
								submenu3.setCategory(level3.getCategory());
								submenu3.setAccessUrl(level3.getAccessUrl());
								submenu3.setImageContent(level3.getImageContent());
								submenu3.setParentModuleCode(level3.getParentModuleCode());
								submenu3.setLevel(level3.getLevel());
								submenu3.setRole(level3.getRole());
								level3list.add(submenu3);
							}
							menu.setHasChildLevel3(true);
							menu.setLevel3Menu(level3list);
							menuList.add(menu);

						}
					}
					menu.setHasChildLevel2(true);
					menu.setLevel2Menu(level2list);					
				}else
				{
					menu.setHasChildLevel2(false);
					menu.setHasChildLevel3(false);
				}
				menuList.add(menu);
			}

		} catch (SQLException ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
		}


		model.addObject("sessionMenuList",menuList);
		model.addObject("lastLoggedIn",user.getLastLoggedIn());
		model.addObject("loginID", user.getLoginId());		
		model.addObject("userSession", user);
		model.setViewName("/main/welcome");

		return model;				
	}

	/** redirect when login failed
	 * @throws IOException **/
	@RequestMapping(value = "/login", method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView redirectLogin(HttpServletRequest request, HttpServletResponse response, @RequestParam(value="error", required=false) boolean error,
			@RequestParam(value="param") String param) throws IOException {
		ModelAndView model = new ModelAndView();
		System.out.println("param::"+param);
		String msg ="Invalid Login ID or Password";
		if(error){
			if(!param.isEmpty()){
				if(param.indexOf("Maximum sessions")>=0){
					msg = "User is already logged in.";
				}else if(param.indexOf("No AuthenticationProvider found")>=0){
					msg ="Invalid Login ID or Password";
				}else
					msg = param;
			}
			model.addObject("loginErrorMsg", "<div id=\"loginError\"  class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> <span class=\"sr-only\">Error:</span>"+msg+"</div>");
			model.setViewName("/main/mainLogin");
		}else{
			System.out.println("login success");
			checkAccess(request, response);
		}

		return model;
	}

	/*@RequestMapping(value = "/login/{error}", method = RequestMethod.GET)	 
	public final String displayLoginform(Model model,  @PathVariable final String error) {
	    model.addAttribute("error", error);
	    System.out.println("dddddddddddddd");
	    return "/main/mainLogin";
	}
	 */

	/*restrict access login*/
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied(Principal user,@RequestParam(value="error", required=false) boolean error) {
		ModelAndView model = new ModelAndView();
		model.addObject("loginErrorMsg", "<div id=\"loginError\"  class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> <span class=\"sr-only\">Error:</span> Access is Denied. Please contact administrator.</div>");
		model.setViewName("/main/mainLogin");
		return model;

	}

	@RequestMapping(value="/logout" , method = {RequestMethod.GET , RequestMethod.POST})
	public ModelAndView doLogout (HttpServletRequest request, HttpServletResponse response) throws IOException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){  
			System.out.println("auth>>>"+auth.getPrincipal());
			request.getSession().invalidate();
			new SecurityContextLogoutHandler().logout(request, response, auth);
			SecurityContextHolder.clearContext();
		}
		return new ModelAndView("/main/mainLogin");
	}
}
