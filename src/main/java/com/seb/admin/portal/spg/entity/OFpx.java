package com.seb.admin.portal.spg.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OFpx {
	@JsonProperty("merchant_id")
	private String merchant_id;
	
	@JsonProperty("exchange_id")
	private String exchange_id;
	
	@JsonProperty("seller_bank_code")
	private String seller_bank_code;
	
	@JsonProperty("version")
	private String version;
	
	@JsonProperty("submit_url")
	private String submit_url;
	
	@JsonProperty("query_url")
	private String query_url;
	
	@JsonProperty("bank_listing_url")
	private String bank_listing_url;
	
	@JsonProperty("csr_country_name")
	private String csr_country_name;

	@JsonProperty("csr_state_or_province_name")
	private String csr_state_or_province_name;
	
	@JsonProperty("csr_locality_name")
	private String csr_locality_name;
	
	@JsonProperty("csr_organization_name")
	private String csr_organization_name;
	
	@JsonProperty("csr_organization_unit")
	private String csr_organization_unit;
	
	@JsonProperty("csr_common_name")
	private String csr_common_name;
	
	@JsonProperty("csr_email")
	private String csr_email;
	
	@JsonProperty("certificate_expiry_warning_days_before")
	private int certificate_expiry_warning_days_before;

	@JsonProperty("certificate_expiry_warning_email")
	private String certificate_expiry_warning_email;

	@JsonProperty("public_key_file_name")
	private String public_key_file_name;

	@JsonProperty("public_key")
	private String public_key;
	
	@JsonProperty("public_key_expiry_date")
	private String public_key_expiry_date;
	
	@JsonProperty("private_key_file_name")
	private String private_key_file_name;
	
	@JsonProperty("private_key")
	private String private_key;
	
	@JsonProperty("private_key_expiry_date")
	private String private_key_expiry_date;
	
	@JsonProperty("private_key_identity_cert_file_name")
	private String private_key_identity_cert_file_name;
	
	@JsonProperty("private_key_identity_cert")
	private String private_key_identity_cert;
	
	@JsonProperty("minimun_payment_amount")
	private Double minimun_payment_amount;
	
	@JsonProperty("maximum_payment_amount")
	private Double maximum_payment_amount;
	
	@JsonProperty("enabled")
	private boolean enabled;

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getExchange_id() {
		return exchange_id;
	}

	public void setExchange_id(String exchange_id) {
		this.exchange_id = exchange_id;
	}

	public String getSeller_bank_code() {
		return seller_bank_code;
	}

	public void setSeller_bank_code(String seller_bank_code) {
		this.seller_bank_code = seller_bank_code;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSubmit_url() {
		return submit_url;
	}

	public void setSubmit_url(String submit_url) {
		this.submit_url = submit_url;
	}

	public String getQuery_url() {
		return query_url;
	}

	public void setQuery_url(String query_url) {
		this.query_url = query_url;
	}

	public String getBank_listing_url() {
		return bank_listing_url;
	}

	public void setBank_listing_url(String bank_listing_url) {
		this.bank_listing_url = bank_listing_url;
	}

	public String getCsr_country_name() {
		return csr_country_name;
	}

	public void setCsr_country_name(String csr_country_name) {
		this.csr_country_name = csr_country_name;
	}

	public String getCsr_state_or_province_name() {
		return csr_state_or_province_name;
	}

	public void setCsr_state_or_province_name(String csr_state_or_province_name) {
		this.csr_state_or_province_name = csr_state_or_province_name;
	}

	public String getCsr_locality_name() {
		return csr_locality_name;
	}

	public void setCsr_locality_name(String csr_locality_name) {
		this.csr_locality_name = csr_locality_name;
	}

	public String getCsr_organization_name() {
		return csr_organization_name;
	}

	public void setCsr_organization_name(String csr_organization_name) {
		this.csr_organization_name = csr_organization_name;
	}

	public String getCsr_organization_unit() {
		return csr_organization_unit;
	}

	public void setCsr_organization_unit(String csr_organization_unit) {
		this.csr_organization_unit = csr_organization_unit;
	}

	public String getCsr_common_name() {
		return csr_common_name;
	}

	public void setCsr_common_name(String csr_common_name) {
		this.csr_common_name = csr_common_name;
	}

	public String getCsr_email() {
		return csr_email;
	}

	public void setCsr_email(String csr_email) {
		this.csr_email = csr_email;
	}

	public int getCertificate_expiry_warning_days_before() {
		return certificate_expiry_warning_days_before;
	}

	public void setCertificate_expiry_warning_days_before(int certificate_expiry_warning_days_before) {
		this.certificate_expiry_warning_days_before = certificate_expiry_warning_days_before;
	}

	public String getCertificate_expiry_warning_email() {
		return certificate_expiry_warning_email;
	}

	public void setCertificate_expiry_warning_email(String certificate_expiry_warning_email) {
		this.certificate_expiry_warning_email = certificate_expiry_warning_email;
	}

	public String getPublic_key_file_name() {
		return public_key_file_name;
	}

	public void setPublic_key_file_name(String public_key_file_name) {
		this.public_key_file_name = public_key_file_name;
	}

	public String getPublic_key() {
		return public_key;
	}

	public void setPublic_key(String public_key) {
		this.public_key = public_key;
	}

	public String getPublic_key_expiry_date() {
		return public_key_expiry_date;
	}

	public void setPublic_key_expiry_date(String public_key_expiry_date) {
		this.public_key_expiry_date = public_key_expiry_date;
	}

	public String getPrivate_key_file_name() {
		return private_key_file_name;
	}

	public void setPrivate_key_file_name(String private_key_file_name) {
		this.private_key_file_name = private_key_file_name;
	}

	public String getPrivate_key() {
		return private_key;
	}

	public void setPrivate_key(String private_key) {
		this.private_key = private_key;
	}

	public String getPrivate_key_expiry_date() {
		return private_key_expiry_date;
	}

	public void setPrivate_key_expiry_date(String private_key_expiry_date) {
		this.private_key_expiry_date = private_key_expiry_date;
	}

	public String getPrivate_key_identity_cert_file_name() {
		return private_key_identity_cert_file_name;
	}

	public void setPrivate_key_identity_cert_file_name(String private_key_identity_cert_file_name) {
		this.private_key_identity_cert_file_name = private_key_identity_cert_file_name;
	}

	public String getPrivate_key_identity_cert() {
		return private_key_identity_cert;
	}

	public void setPrivate_key_identity_cert(String private_key_identity_cert) {
		this.private_key_identity_cert = private_key_identity_cert;
	}

	public Double getMinimun_payment_amount() {
		return minimun_payment_amount;
	}

	public void setMinimun_payment_amount(Double minimun_payment_amount) {
		this.minimun_payment_amount = minimun_payment_amount;
	}

	public Double getMaximum_payment_amount() {
		return maximum_payment_amount;
	}

	public void setMaximum_payment_amount(Double maximum_payment_amount) {
		this.maximum_payment_amount = maximum_payment_amount;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "merchant_id:[" + merchant_id + "], exchange_id:[" + exchange_id + "], seller_bank_code:["
				+ seller_bank_code + "], version:[" + version + "], submit_url:[" + submit_url + "], query_url:["
				+ query_url + "], bank_listing_url:[" + bank_listing_url + "], csr_country_name:[" + csr_country_name
				+ "], csr_state_or_province_name:[" + csr_state_or_province_name + "], csr_locality_name:["
				+ csr_locality_name + "], csr_organization_name:[" + csr_organization_name
				+ "], csr_organization_unit:[" + csr_organization_unit + "], csr_common_name:[" + csr_common_name
				+ "], csr_email:[" + csr_email + "], certificate_expiry_warning_days_before:["
				+ certificate_expiry_warning_days_before + "], certificate_expiry_warning_email:["
				+ certificate_expiry_warning_email + "], public_key_file_name:[" + public_key_file_name
				+ "], public_key_expiry_date:[" + public_key_expiry_date + "], private_key_file_name:["
				+ private_key_file_name + "], private_key_expiry_date:[" + private_key_expiry_date
				+ "], private_key_identity_cert_file_name:[" + private_key_identity_cert_file_name
				+ "], minimun_payment_amount:[" + minimun_payment_amount + "], maximum_payment_amount:["
				+ maximum_payment_amount + "], enabled:[" + enabled + "]";
	}

		

}
