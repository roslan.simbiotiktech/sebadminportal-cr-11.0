package com.seb.admin.portal.model;

public class MyAccessRight {


	private int id;
	private int adminUserId;
	private int deletedFlag;	
	private int portalModuleId;
	private String status="1";
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAdminUserId() {
		return adminUserId;
	}
	public void setAdminUserId(int adminUserId) {
		this.adminUserId = adminUserId;
	}
	public int getDeletedFlag() {
		return deletedFlag;
	}
	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}
	public int getPortalModuleId() {
		return portalModuleId;
	}
	public void setPortalModuleId(int portalModuleId) {
		this.portalModuleId = portalModuleId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}


}
