package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;


/**
 * The persistent class for the SUBSCRIPTION database table.
 * 
 */
@Entity
@Table(name="SUBSCRIPTION", schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="Subscription.findAll", query="SELECT s FROM Subscription s"),
	@NamedQuery(name="Subscription.findSelectedSubscription", query="SELECT s FROM Subscription s WHERE s.userEmail =:email AND s.contractAccountNumber =:accountNumber AND UPPER(s.status)=:status"),
	@NamedQuery(name="Subscription.findSubscriptionByEmail", query="SELECT s FROM Subscription s WHERE s.userEmail =:email"),
	//@NamedQuery(name="Subscription.updateSubscriptionByStatus", query="UPDATE Subscription s SET s.status= :status WHERE s.status=:oldStatus")

})
public class Subscription implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SUBS_ID", unique=true, nullable=false)
	private int subsId;

	@Column(name="CONTRACT_ACCOUNT_NICK")
	private String contractAccountNick;

	@Column(name="CONTRACT_ACCOUNT_NUMBER")
	private String contractAccountNumber;

	@Column(name="CREATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="NOTIFY_BY_EMAIL")
	private int notifyByEmail;

	@Column(name="NOTIFY_BY_SMS")
	private int notifyBySms;

	private String remark;

	@Column(name="STATUS")
	private String status;

	@Column(name="SUBS_TYPE")
	private String subsType;

	@Column(name="UPDATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp updatedDatetime;

	@Column(name="USER_EMAIL")
	private String userEmail;

	public Subscription() {
	}

	public int getSubsId() {
		return this.subsId;
	}

	public void setSubsId(int subsId) {
		this.subsId = subsId;
	}

	public String getContractAccountNick() {
		return this.contractAccountNick;
	}

	public void setContractAccountNick(String contractAccountNick) {
		this.contractAccountNick = contractAccountNick;
	}

	public String getContractAccountNumber() {
		return this.contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getNotifyByEmail() {
		return this.notifyByEmail;
	}

	public void setNotifyByEmail(int notifyByEmail) {
		this.notifyByEmail = notifyByEmail;
	}

	public int getNotifyBySms() {
		return this.notifyBySms;
	}

	public void setNotifyBySms(int notifyBySms) {
		this.notifyBySms = notifyBySms;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSubsType() {
		return this.subsType;
	}

	public void setSubsType(String subsType) {
		this.subsType = subsType;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

}