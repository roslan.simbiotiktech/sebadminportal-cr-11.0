package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
/**
 * The persistent class for the POWER_ALERT database table.
 * 
 */
@Entity
@Table(name="POWER_ALERT" , schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="PowerAlert.findAll", query="SELECT p FROM PowerAlert p WHERE p.deletedFlag=0"),
	@NamedQuery(name="PowerAlert.findAllOutageByTypeId", query="SELECT p FROM PowerAlert p WHERE  p.restorationDatetime >=:startDate AND p.restorationDatetime <=:endDate AND p.powerAlertTypeId= :powerTypeId AND p.deletedFlag=0"),
	@NamedQuery(name="PowerAlert.findAllPreventiveByTypeId", query="SELECT p FROM PowerAlert p WHERE  p.maintenanceStartDatetime >=:startDate AND p.maintenanceEndDatetime <=:endDate AND p.powerAlertTypeId= :powerTypeId AND p.deletedFlag=0"),

	//@NamedQuery(name="PowerAlert.findAllPowerAlertByCondition", query="SELECT distinct p FROM PowerAlert p LEFT JOIN p.tagNotifications t WHERE p.createdDatetime >=:startDate AND p.createdDatetime <=:endDate AND p.powerAlertTypeId= :powerTypeId AND p.deletedFlag=0"),
	@NamedQuery(name="PowerAlert.findAllPowerAlertByCondition2ById", query="SELECT distinct p FROM PowerAlert p  WHERE p.id =:id  AND p.deletedFlag=0"),
	@NamedQuery(name="PowerAlert.findAllOutageContent", query="SELECT p FROM  PowerAlert p WHERE   p.restorationDatetime > CURRENT_TIMESTAMP AND UPPER(p.powerAlertType.name) LIKE '%OUTAGE%'"),
	@NamedQuery(name="PowerAlert.findAllPreventiveContent", query="SELECT p FROM  PowerAlert p WHERE  p.maintenanceEndDatetime> CURRENT_TIMESTAMP AND UPPER(p.powerAlertType.name) LIKE '%PREVENTIVE%'"),
	@NamedQuery(name="PowerAlert.findAllOutagePowerAlertByIds", query="SELECT p FROM PowerAlert p WHERE p.id  IN (:ids)  AND p.restorationDatetime> CURRENT_TIMESTAMP AND p.deletedFlag=0"),
	@NamedQuery(name="PowerAlert.findAllPreventivePowerAlertByIds", query="SELECT p FROM PowerAlert p WHERE p.id  IN (:ids)  AND p.maintenanceEndDatetime> CURRENT_TIMESTAMP AND p.deletedFlag=0")

})
public class PowerAlert implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(length=500)
	private String area;

	@Column(length=500)
	private String causes;

	@Column(name="CREATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="CUST_SERVICE_LOCATION_ID")
	private int custServiceLocationId;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(name="MAINTENANCE_END_DATETIME" , nullable=false, insertable = true, updatable = true)
	private Timestamp maintenanceEndDatetime;

	@Column(name="MAINTENANCE_START_DATETIME" , nullable=false, insertable = true, updatable = true)
	private Timestamp maintenanceStartDatetime;

	@Column(name="POWER_ALERT_TYPE_ID")
	private int powerAlertTypeId;

	@Column(name="RESTORATION_DATETIME" , nullable=false, insertable = true, updatable = true)
	private Timestamp restorationDatetime;

	@Column(length=200)
	private String title;

	@Column(name="UPDATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	private Timestamp updatedDatetime;
	
	@JoinColumn(name = "POWER_ALERT_TYPE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private PowerAlertType powerAlertType;

	@JoinColumn(name = "CUST_SERVICE_LOCATION_ID", referencedColumnName = "ID", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private CustServiceLocation custServLoc;


	//bi-directional many-to-one association to TagNotification
	@OneToMany(mappedBy="powerAlert")
	private List<TagNotification> tagNotifications;

	public PowerAlert() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCauses() {
		return this.causes;
	}

	public void setCauses(String causes) {
		this.causes = causes;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getCustServiceLocationId() {
		return this.custServiceLocationId;
	}

	public void setCustServiceLocationId(int custServiceLocationId) {
		this.custServiceLocationId = custServiceLocationId;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public Timestamp getMaintenanceEndDatetime() {
		return this.maintenanceEndDatetime;
	}

	public void setMaintenanceEndDatetime(Timestamp maintenanceEndDatetime) {
		this.maintenanceEndDatetime = maintenanceEndDatetime;
	}

	public Timestamp getMaintenanceStartDatetime() {
		return this.maintenanceStartDatetime;
	}

	public void setMaintenanceStartDatetime(Timestamp maintenanceStartDatetime) {
		this.maintenanceStartDatetime = maintenanceStartDatetime;
	}

	public int getPowerAlertTypeId() {
		return this.powerAlertTypeId;
	}

	public void setPowerAlertTypeId(int powerAlertTypeId) {
		this.powerAlertTypeId = powerAlertTypeId;
	}

	public Timestamp getRestorationDatetime() {
		return this.restorationDatetime;
	}

	public void setRestorationDatetime(Timestamp restorationDatetime) {
		this.restorationDatetime = restorationDatetime;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}
	
	@JsonBackReference //for parent table
	public PowerAlertType getPowerAlertType() {
		return powerAlertType;
	}

	public void setPowerAlertType(PowerAlertType powerAlertType) {
		this.powerAlertType = powerAlertType;
	}

	public CustServiceLocation getCustServLoc() {
		return custServLoc;
	}

	public void setCustServLoc(CustServiceLocation custServLoc) {
		this.custServLoc = custServLoc;
	}

	public List<TagNotification> getTagNotifications() {
		return this.tagNotifications;
	}

	public void setTagNotifications(List<TagNotification> tagNotifications) {
		this.tagNotifications = tagNotifications;
	}

	public TagNotification addTagNotification(TagNotification tagNotification) {
		getTagNotifications().add(tagNotification);
		tagNotification.setPowerAlert(this);

		return tagNotification;
	}

	public TagNotification removeTagNotification(TagNotification tagNotification) {
		getTagNotifications().remove(tagNotification);
		tagNotification.setPowerAlert(null);

		return tagNotification;
	}

	@Override
	public String toString() {
		return "PowerAlert [id=" + id + ", area=" + area + ", causes=" + causes + ", createdDatetime=" + createdDatetime
				+ ", custServiceLocationId=" + custServiceLocationId + ", deletedFlag=" + deletedFlag
				+ ", maintenanceEndDatetime=" + maintenanceEndDatetime + ", maintenanceStartDatetime="
				+ maintenanceStartDatetime + ", powerAlertTypeId=" + powerAlertTypeId + ", restorationDatetime="
				+ restorationDatetime + ", title=" + title + ", updatedDatetime=" + updatedDatetime
				+ ", powerAlertType=" + powerAlertType + ", custServLoc=" + custServLoc + ", tagNotifications="
				+ tagNotifications + "]";
	}
	
	

}