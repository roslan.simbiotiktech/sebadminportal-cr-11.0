<%@page session="true"%>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author"><%@ include file="../menuTitle.jsp" %><%@ include file="../global.jsp" %>
</head>

<body>
    <div id="wrapper">
        <!-- Navigation -->

        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <%@ include file="../menuTop.jsp" %><%@ include file="../menuLeft.jsp" %>
        </nav>

        <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp" %>
        </div><!-- #page-wrapper -->
    </div><!-- #wrapper -->
</body>
</html>