package com.seb.admin.portal.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.News;
import com.seb.admin.portal.model.ReportRemarksHistory;
@Repository
public class ReportRemarksHistoryDAOImpl implements ReportRemarksHistoryDAO {

private static final Logger log = Logger.getLogger(ReportDAOImpl.class);
	
	SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd", Locale.ENGLISH);

	@PersistenceContext(unitName = ApplicationConstant.UNIT_NAME, type = PersistenceContextType.EXTENDED)
	private EntityManager em;
	
	

	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReportRemarksHistory> findAllMakeReportByCondition(String transID) {
		// TODO Auto-generated method stub
		
		
		List <ReportRemarksHistory> reportRemarksHistory = new ArrayList <ReportRemarksHistory>();
		
		try{
			int id = Integer.parseInt(transID);
			System.out.println("transIDInfo>>>  "+id);
			
			Query readQuery =  em.createQuery("SELECT r FROM ReportRemarksHistory r WHERE r.idInfo=:idInfo",ReportRemarksHistory.class);
			readQuery.setParameter("idInfo", Integer.valueOf(id));
			reportRemarksHistory = readQuery.getResultList();
		
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return reportRemarksHistory;
	}

}
