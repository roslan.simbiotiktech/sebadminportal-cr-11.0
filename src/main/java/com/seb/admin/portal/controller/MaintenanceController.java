package com.seb.admin.portal.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.model.ContactUs;
import com.seb.admin.portal.model.CustServiceCounter;
import com.seb.admin.portal.model.CustServiceCounterEdited;
import com.seb.admin.portal.model.CustServiceLocation;
import com.seb.admin.portal.model.Faq;
import com.seb.admin.portal.model.FaqCategory;
import com.seb.admin.portal.model.MyDropDown;
import com.seb.admin.portal.model.MyNews;
import com.seb.admin.portal.model.MyPowerAlert;
import com.seb.admin.portal.model.MyServiceCounterLocator;
import com.seb.admin.portal.model.MyTagNotification;
import com.seb.admin.portal.model.MyTariffSettings;
import com.seb.admin.portal.model.MyTariffSettingsResult;
import com.seb.admin.portal.model.News;
import com.seb.admin.portal.model.PowerAlert;
import com.seb.admin.portal.model.PowerAlertType;
import com.seb.admin.portal.model.Setting;
import com.seb.admin.portal.model.TagNotification;
import com.seb.admin.portal.model.TariffCategory;
import com.seb.admin.portal.model.TariffSetting;
import com.seb.admin.portal.model.UploadedFile;
import com.seb.admin.portal.service.LoginService;
import com.seb.admin.portal.service.MaintenanceService;
import com.seb.admin.portal.service.ReportService;
import com.seb.admin.portal.service.SettingService;
import com.seb.admin.portal.util.DateUtil;
import com.seb.admin.portal.util.Mail;
import com.seb.admin.portal.util.StringUtil;

@Controller
public class MaintenanceController {

	private static final Logger log = Logger.getLogger(MaintenanceController.class);

	@Autowired
	private ReportService reportService;
	@Autowired
	private MaintenanceService maintenanceService;
	@Autowired
	private LoginService loginService;
	@Autowired
	private SettingService settingService;

	/** Maker-Maintenance>Service Counter Locator>Add **/
	@RequestMapping(value = "/admin/addCSC",  method = RequestMethod.GET)
	public ModelAndView doAddCSC()	{
		ModelAndView model = new ModelAndView();

		try {
			List<String> regionList = maintenanceService.findAllRegions();			
			//List<String> regionList = allRegionList.stream().distinct().collect(Collectors.toList());
			model.addObject("regionList", regionList);
		} catch (SQLException ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		model.setViewName("/maintenance/addCSC");
		return model;
	}

	@RequestMapping(value = "/admin/getStation",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody List<CustServiceLocation> getStation(@RequestBody CustServiceLocation location)
			throws IOException {
		List<CustServiceLocation> stationList = new ArrayList<CustServiceLocation>() ;	
		try {
			//ObjectMapper mapper = new ObjectMapper();
			//CustServiceLocation reqValue = mapper.readValue(json,CustServiceLocation.class);
			//String paramRegion  = reqValue.getRegion();
			stationList = maintenanceService.findAllStation(location.getRegion());
			System.out.println("paramRegion>>"+location.getRegion());			
		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return stationList;
	}

	@RequestMapping(value = "/admin/getArea",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody List<CustServiceCounter> getArea(@RequestBody CustServiceCounter counter)
			throws IOException {
		List<CustServiceCounter> areaList = new ArrayList<CustServiceCounter>() ;	
		try {
			areaList = maintenanceService.findAllCounterByLocId(counter.getCustServiceLocationId());
			System.out.println("paramRegion>>"+counter.getCustServiceLocationId());			
		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return areaList;
	}

	@RequestMapping(value = "/admin/getTitle",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody List<MyDropDown> getTitle(@RequestBody MyDropDown data)
			throws IOException {
		List<MyDropDown> dropDown = new ArrayList<MyDropDown>() ;	
		try {
			System.out.println("text:"+data.getText());
			System.out.println("value:"+data.getValue());
			if(data!=null){

				if(data.getValue().equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_PROMO_KEY)){//news
					List<News> myNews = maintenanceService.findNewsTitleByEndDate();
					for(News n : myNews){
						MyDropDown dd = new MyDropDown();
						dd.setText(n.getTitle());
						dd.setValue(String.valueOf(n.getId()));
						dropDown.add(dd);						
					}

				}else if(data.getValue().equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY)){//Outage Alert
					//get outage
					List<PowerAlert> outage = maintenanceService.findAllOutageContent();
					for(PowerAlert p : outage){
						MyDropDown dd = new MyDropDown();
						dd.setText(p.getTitle());
						dd.setValue(String.valueOf(p.getId()));
						dropDown.add(dd);
					}

					//get preventive
					List<PowerAlert> preventive = maintenanceService.findAllPreventiveContent();
					for(PowerAlert p : preventive){
						MyDropDown dd = new MyDropDown();
						dd.setText(p.getTitle());
						dd.setValue(String.valueOf(p.getId()));
						dropDown.add(dd);

					}
				}

				dropDown.stream().sorted((object1, object2) -> object1.getText().compareTo(object2.getText()));
			}

		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return dropDown;
	}


	@RequestMapping(value = "/admin/getPushNotifEditTitle",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody List<MyDropDown> getPushNotifEditTitle(@RequestBody MyDropDown data)
			throws IOException {
		List<MyDropDown> dropDown = new ArrayList<MyDropDown>() ;	
		try {
			MyDropDown ddObj = new MyDropDown();
			//System.out.println(ddObj.toString());

			String dateFrom = data.getDateFrom();
			String dateTo = data.getDateTo();

			System.out.println("data.getText()>>"+data.getText());
			System.out.println("data.getValue()>>"+data.getValue());

			if(data!=null){

				DateUtil dateUtil = new DateUtil();
				Date myStartDate = new Date();
				Date myEndDate = new Date();

				myStartDate  = (StringUtil.trimToEmpty(dateFrom).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateFrom+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");		
				myEndDate  = (StringUtil.trimToEmpty(dateTo).equals("")) ? dateUtil.convertToDate(dateUtil.getTomorrowDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateTo+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						

				System.out.println("myStartDate>"+myStartDate);
				System.out.println("myEndDate>"+myEndDate);

				List<Integer> titleIdList = new ArrayList<Integer>();

				if(data.getValue().equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_PROMO_KEY)){//news or promotion
					titleIdList = maintenanceService.findPNDistinctTitleIdBtwDate(data.getValue(), ApplicationConstant.TAG_NAME_GENERAL);
					System.out.println(">>>>p-promo:"+titleIdList.size());

					if(titleIdList!=null && titleIdList.size()>0){
						List<News> newslist = maintenanceService.findNewsIds(titleIdList);
						System.out.println(">>>>newslist:"+newslist.size());

						for(News n0 : newslist){
							MyDropDown dd = new MyDropDown();
							dd.setText(n0.getTitle());
							dd.setValue(String.valueOf(n0.getId()));
							dropDown.add(dd);	
						}
					}

				}else if(data.getValue().equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY)){//Outage Alert
					System.out.println("tag like :"+ ApplicationConstant.TAG_NAME_POWER_ALERT);					
					titleIdList = maintenanceService.findDistinctTitleIdBtwDatePower(data.getValue(), ApplicationConstant.TAG_NAME_POWER_ALERT, ApplicationConstant.POWER_ALERT_OUTAGE);//outage

					System.out.println(">>>>p-outage:"+titleIdList.size());
					if(titleIdList!=null && titleIdList.size()>0){
						List<PowerAlert> outage = maintenanceService.findAllOutagePowerAlertByIds(titleIdList);
						System.out.println(">>>>outagelist:"+outage.size());
						for(PowerAlert p : outage){
							MyDropDown dd = new MyDropDown();
							dd.setText(p.getTitle());
							dd.setValue(String.valueOf(p.getId()));
							dropDown.add(dd);
						}
					}

					titleIdList = maintenanceService.findDistinctTitleIdBtwDatePower(data.getValue(), ApplicationConstant.TAG_NAME_MAINTENANCE, ApplicationConstant.POWER_ALERT_PREVENTIVE);//preventive
					if(titleIdList!=null && titleIdList.size()>0){
						List<PowerAlert> preventive = maintenanceService.findAllPreventivePowerAlertByIds(titleIdList);
						System.out.println(">>>>p-preventive:"+preventive.size());

						for(PowerAlert p : preventive){
							MyDropDown dd = new MyDropDown();
							dd.setText(p.getTitle());
							dd.setValue(String.valueOf(p.getId()));
							dropDown.add(dd);
						}
					}


				}	
				dropDown.stream().sorted((object1, object2) -> object1.getText().compareTo(object2.getText()));

			}
		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return dropDown;
	}

	@RequestMapping(value = "/admin/getSubPushNotifEditTitle",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody List<MyDropDown> getSubPushNotifEditTitle(@RequestBody MyDropDown data)
			throws IOException {
		List<MyDropDown> dropDown = new ArrayList<MyDropDown>() ;	
		try {
			String dateFrom = data.getDateFrom();
			String dateTo = data.getDateTo();

			System.out.println("getDateFrom>>"+data.getDateFrom());
			System.out.println("data.getDateTo()>>"+data.getDateTo());
			System.out.println("data.getText()>>"+data.getText());
			System.out.println("data.getValue()>>"+data.getValue());

			if(data!=null){

				DateUtil dateUtil = new DateUtil();
				Date myStartDate = new Date();
				Date myEndDate = new Date();

				myStartDate  = (StringUtil.trimToEmpty(dateFrom).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateFrom+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");		
				myEndDate  = (StringUtil.trimToEmpty(dateTo).equals("")) ? dateUtil.convertToDate(dateUtil.getTomorrowDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateTo+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						

				System.out.println("myStartDate>"+myStartDate);
				System.out.println("myEndDate>"+myEndDate);

				List<Integer> titleIdList = new ArrayList<Integer>();

				if(data.getValue().equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_PROMO_KEY)){//news or promotion
					titleIdList = maintenanceService.findSubTitleIdBtwDate(myStartDate, myEndDate, data.getValue(), ApplicationConstant.TAG_NAME_GENERAL);
					System.out.println(">>>>p-promo/news:"+titleIdList.size());

					if(titleIdList!=null && titleIdList.size()>0){
						List<News> newslist = maintenanceService.findNewsIds(titleIdList);
						System.out.println(">>>>newslist:"+newslist.size());

						for(News n0 : newslist){
							MyDropDown dd = new MyDropDown();
							dd.setText(n0.getTitle());
							dd.setValue(String.valueOf(n0.getId()));
							dropDown.add(dd);	
						}
					}

				}else if(data.getValue().equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY)){//Outage Alert
					System.out.println("tag like :"+ ApplicationConstant.TAG_NAME_POWER_ALERT);				
					titleIdList = maintenanceService.findSubTitleIdBtwDatePower(myStartDate, myEndDate, data.getValue(), ApplicationConstant.TAG_NAME_POWER_ALERT, ApplicationConstant.POWER_ALERT_OUTAGE);//outage

					System.out.println(">>>>p-outage:"+titleIdList.size());
					if(titleIdList!=null && titleIdList.size()>0){
						List<PowerAlert> outage = maintenanceService.findAllOutagePowerAlertByIds(titleIdList);
						System.out.println(">>>>outagelist:"+outage.size());
						for(PowerAlert p : outage){
							MyDropDown dd = new MyDropDown();
							dd.setText(p.getTitle());
							dd.setValue(String.valueOf(p.getId()));
							dropDown.add(dd);
						}
					}

					titleIdList = maintenanceService.findSubTitleIdBtwDatePower(myStartDate, myEndDate, data.getValue(), ApplicationConstant.TAG_NAME_MAINTENANCE, ApplicationConstant.POWER_ALERT_PREVENTIVE);//preventive
					if(titleIdList!=null && titleIdList.size()>0){
						List<PowerAlert> preventive = maintenanceService.findAllPreventivePowerAlertByIds(titleIdList);
						System.out.println(">>>>p-preventive:"+preventive.size());

						for(PowerAlert p : preventive){
							MyDropDown dd = new MyDropDown();
							dd.setText(p.getTitle());
							dd.setValue(String.valueOf(p.getId()));
							dropDown.add(dd);
						}
					}


				}
				if(dropDown.size()>0){
					dropDown.stream().sorted((object1, object2) -> object1.getText().toUpperCase().compareTo(object2.getText().toUpperCase()));
				}

			}
		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return dropDown;
	}


	@RequestMapping(value = "/admin/addCSC",  method = RequestMethod.POST)
	public ModelAndView saveCSC(
			@RequestParam(value="selectRegion", required=true) String selectRegion,
			@RequestParam(value="selectStation", required=true) String selectStation,
			@RequestParam(value="add1", required=true, defaultValue = "") String add1,
			@RequestParam(value="add2", required = false, defaultValue = "") String add2,
			@RequestParam(value="add3", required = false, defaultValue = "") String add3,
			@RequestParam(value="postcode", required=true, defaultValue = "") String postcode,
			@RequestParam(value="city", required=true, defaultValue = "") String city,
			@RequestParam(value="longitude", required=true, defaultValue = "") String longitude,
			@RequestParam(value="latitude", required=true, defaultValue = "") String latitude,
			@RequestParam(value="openingHr", required=true, defaultValue = "") String openingHour){

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();

		List<String> regionList = new ArrayList<String>();

		try{
			regionList = maintenanceService.findAllRegions();

			if(StringUtil.isNumeric(latitude) && StringUtil.isNumeric(longitude)){
				CustServiceCounter c0 = new CustServiceCounter();
				c0.setCustServiceLocationId(Integer.parseInt(selectStation));
				c0.setAddressLine1(add1);
				c0.setAddressLine2(add2);
				c0.setAddressLine3(add3);
				c0.setPostcode(postcode);
				c0.setCity(city);
				c0.setOpeningHour(openingHour);
				c0.setLongitude(Double.parseDouble(longitude));
				c0.setLatitude(Double.parseDouble(latitude));
				c0.setAdminUserId(user.getId());
				c0.setCategory(ApplicationConstant.CATEGORY_NEW);

				maintenanceService.saveCSC(c0);
				reportService.auditLog("Service Counter Locator", new AuditTrail(user.getId(),0,"","Add Service Counter Locator: "+user.getLoginId(),"", "Add 1:["+add1+"], Add 2:["+add2+"], Add 3:["+add3+"], Postcode:["+postcode+"], City:["+city+"], Opening Hour:["+openingHour+"],longitude:["+longitude+"], latitude:["+latitude+"]","" ));		
				model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully added new service counter locator."));

				//START send email
				BufferedReader br = null;
				try {
					String sCurrentLine;
					int numberOfLine = 1;
					String filePath = "";

					filePath = System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE)==null ?System.getProperty("catalina.home") + File.separator + "csc_email" + File.separator + "NEW_checker_add_email.txt" : System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE) + File.separator + "csc_email" + File.separator + "NEW_checker_add_email.txt";

					br = new BufferedReader(new FileReader(filePath));					

					String subject = "";
					StringBuffer strBuffer = new StringBuffer();
					while ((sCurrentLine = br.readLine()) != null) {
						if(numberOfLine==1)
							subject = sCurrentLine;
						else							
							strBuffer.append(sCurrentLine);

						numberOfLine++;
					}

					String body =  strBuffer.toString().replace("{loginID}", user.getLoginId());
					System.out.println("subject :"+subject);
					System.out.println("body :"+body);

					Mail mail = new Mail();
					Setting setting = settingService.findByKey(ApplicationConstant.CHECKER_EMAIL);
					List<String> emailList = new ArrayList<String>();
					emailList.add(setting.getSettingValue());

					mail.sendMail("Service Counter Locator", user, user.getLoginId(), emailList, new ArrayList(), new ArrayList(), subject, body, "text/html");

				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if (br != null)br.close();
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
				//END send email

			}
		}catch (Exception ex) {
			reportService.auditLog("Service Counter Locator", new AuditTrail(user.getId(),0,"","Add Service Counter Locator: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to add new service counter locator. Unable to send email."));
			ex.printStackTrace();

		}	
		model.addObject("regionList", regionList);
		model.setViewName("/maintenance/addCSC");	
		return model;
	}

	/**Maintenance>Service Counter Locator>Edit
	 * @throws IOException **/
	@RequestMapping(value = "/admin/editCSC",  method = RequestMethod.GET)
	public ModelAndView doEditCSC(HttpServletRequest req, HttpServletResponse resp) throws IOException	{		
		ModelAndView model = new ModelAndView();
		if(req.isUserInRole("ROLE_A") || req.isUserInRole("ROLE_M")){
			model.setViewName("/maintenance/meditCSC");
		}else if(req.isUserInRole("ROLE_A") || req.isUserInRole("ROLE_C")){
			model.setViewName("/maintenance/ceditCSC");
		}else{
			return new ModelAndView(new RedirectView(req.getContextPath() +"/mainLogin.do"));
		}
		return model;
	}

	@RequestMapping(value = "/admin/editCSC",  method = RequestMethod.POST)
	public ModelAndView searchCSC( 
			HttpServletRequest req, HttpServletResponse resp, 
			@RequestParam(value="selectStatus", required=true, defaultValue = "") String inStatus){
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		List<CustServiceCounter> c0List  = new ArrayList<CustServiceCounter>();
		List<MyServiceCounterLocator> myCounterList = new ArrayList<MyServiceCounterLocator>();
		List<String> regionList = new ArrayList<String>();
		String status = "";

		try{	
			regionList = maintenanceService.findAllRegions();

			switch (inStatus) {
			case "A":
				status = "Approved";
				break;
			case "D":
				status = "Deleted";
				break;
			case "P":
				status = "Pending";
				break;
			case "R":
				status = "Rejected";
				break;	       
			}

			if(inStatus!=null){
				if(inStatus.equalsIgnoreCase(ApplicationConstant.APPROVED)){
					c0List = maintenanceService.findCounterByStatus(inStatus,ApplicationConstant.CATEGORY_NEW);
					model.addObject("c0List",c0List);
				}
				else if(inStatus.equalsIgnoreCase(ApplicationConstant.DELETED)){
					c0List = maintenanceService.findCounterByStatus(inStatus,ApplicationConstant.CATEGROY_DELETED);
					model.addObject("c0List",c0List);
				}
				else if(inStatus.equalsIgnoreCase(ApplicationConstant.PENDING)){
					System.out.println("======"+ApplicationConstant.PENDING+"==============");
					c0List = maintenanceService.findMainLocatorByStatus(ApplicationConstant.APPROVED, ApplicationConstant.PENDING);
					System.out.println("main table P size ::"+c0List.size());

					for(CustServiceCounter main : c0List){
						System.out.println("=====================");
						System.out.println("main table id >>"+main.getId());
						System.out.println("main table status >>"+main.getStatus());
						System.out.println("main table category >>"+main.getCategory());
						List<CustServiceCounterEdited> table2  = maintenanceService.findSubLocatorByStatus(ApplicationConstant.PENDING, main.getId());

						if(main.getStatus().equalsIgnoreCase(ApplicationConstant.PENDING) && main.getCategory().equalsIgnoreCase(ApplicationConstant.CATEGORY_NEW)){

							System.out.println("main table>id>"+main.getId());
							System.out.println("main table>status>"+main.getStatus());

							System.out.println("main table>adminUserId>"+main.getAdminUserId());
							MyServiceCounterLocator mainObject1 = new MyServiceCounterLocator();
							mainObject1.setAdminUserId(main.getAdminUserId());
							mainObject1.setCustServiceLocationId(main.getCustServiceLocationId());
							mainObject1.setAddressLine1(main.getAddressLine1());
							mainObject1.setAddressLine2(main.getAddressLine2());
							mainObject1.setAddressLine3(main.getAddressLine3());
							mainObject1.setCity(main.getCity());
							mainObject1.setPostcode(main.getPostcode());
							mainObject1.setLatitude(main.getLatitude());
							mainObject1.setLongitude(main.getLongitude());
							mainObject1.setOpeningHour(main.getOpeningHour());
							mainObject1.setStatus(main.getStatus());
							mainObject1.setCategory(main.getCategory());
							mainObject1.setUpdatedDatetime(main.getUpdatedDatetime());							
							mainObject1.setCustServLoc(maintenanceService.findCustServiceLocationById(main.getCustServiceLocationId()));

							mainObject1.setId(main.getId());
							mainObject1.setRejectReason(main.getRejectReason());
							mainObject1.setMainTable(true);

							myCounterList.add(mainObject1);

						}else if(main.getStatus().equalsIgnoreCase(ApplicationConstant.APPROVED) && table2!=null && table2.size()>0){
							/*if table 2 has record, then use table 2, 
							else use table 1*/

							for(CustServiceCounterEdited e0 : table2){
								System.out.println("sub table>id>"+e0.getId());
								System.out.println("sub table>counter id>"+e0.getCounterId());
								System.out.println("sub table>status>"+e0.getStatus());
								System.out.println("sub table>add1>"+e0.getAddressLine1());

								MyServiceCounterLocator myObject2 = new MyServiceCounterLocator();
								myObject2.setAdminUserId(e0.getAdminUserId());
								myObject2.setCustServiceLocationId(e0.getCustServiceLocationId());
								myObject2.setAddressLine1(e0.getAddressLine1());
								myObject2.setAddressLine2(e0.getAddressLine2());
								myObject2.setAddressLine3(e0.getAddressLine3());
								myObject2.setCity(e0.getCity());
								myObject2.setPostcode(e0.getPostcode());
								myObject2.setLatitude(e0.getLatitude());
								myObject2.setLongitude(e0.getLongitude());
								myObject2.setOpeningHour(e0.getOpeningHour());
								myObject2.setStatus(e0.getStatus());
								myObject2.setCategory(e0.getCategory());
								myObject2.setUpdatedDatetime(e0.getUpdatedDatetime());							
								myObject2.setCustServLoc(maintenanceService.findCustServiceLocationById(e0.getCustServiceLocationId()));
								myObject2.setId(e0.getCounterId());
								myObject2.setRejectReason(e0.getRejectReason());
								myObject2.setMainTable(false);
								myObject2.setEditedId(e0.getId());
								myCounterList.add(myObject2);								
							}							
						}
					}

					System.out.println("myCounterList size>>"+myCounterList.size());
					model.addObject("c0List",myCounterList);
				}
				else if(inStatus.equalsIgnoreCase(ApplicationConstant.REJECTED)){
					/*display rejected data (for category NEW)*/					
					c0List = maintenanceService.findCounterByStatus(inStatus,ApplicationConstant.CATEGORY_NEW);
					for(CustServiceCounter c : c0List)
					{
						System.out.println(">>>>>>>>>>>"+c.getAddressLine1());
					}
					model.addObject("c0List",c0List);
				}

			}

		}catch(Exception ex){
			ex.printStackTrace();
			log.error(ex);

		}
		System.out.println("search: "+inStatus);
		System.out.println(c0List.size());



		model.addObject("regionList", regionList);
		model.addObject("status",status);

		reportService.auditLog("Service Counter Locator", new AuditTrail(user.getId(),0,"","Search Service Counter Locator: "+user.getLoginId(),"", "status: ["+status+"]","" ));		


		if(req.isUserInRole("ROLE_A") || req.isUserInRole("ROLE_M")){
			model.setViewName("/maintenance/meditCSC");
		}else if(req.isUserInRole("ROLE_A") || req.isUserInRole("ROLE_C")){
			model.setViewName("/maintenance/ceditCSC");
		}else{
			return new ModelAndView(new RedirectView(req.getContextPath() +"/mainLogin.do"));
		}	

		return model;
	}


	@RequestMapping(value = "/admin/updateCSC",  method = RequestMethod.POST)
	public ModelAndView updateCSC(/*@RequestParam(value = "myParam", defaultValue = "") String myParam*/){
		ModelAndView model = new ModelAndView();
		model.setViewName("/maintenance/meditCSC");
		return model;

	}

	@RequestMapping(value = "/admin/updateMakerCSC",  method = RequestMethod.POST )
	public @ResponseBody Map<String,String> updateMakerCSC( @RequestBody List<Map<String, String>> client)
			throws IOException {
		int count = 0;
		List<CustServiceCounter> c0List  = new ArrayList<CustServiceCounter>();

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		try {

			CustServiceCounter c0 = new CustServiceCounter();
			for (Map<String, String> formInput : client) {
				//formInputs.put(formInput.get("name"), formInput.get("value"));
				log.info("no "+count);
				log.info("id>>"+formInput.get("id"));
				log.info("stationId>>"+formInput.get("stationId"));
				log.info("regionName>>"+formInput.get("regionName"));
				log.info("addressLine1>>"+formInput.get("addressLine1"));
				log.info("addressLine2>>"+formInput.get("addressLine2"));
				log.info("addressLine3>>"+formInput.get("addressLine3"));
				log.info("postcode>>"+formInput.get("postcode"));
				log.info("city>>"+formInput.get("city"));
				log.info("longitude>>"+formInput.get("longitude"));
				log.info("latitude>>"+formInput.get("latitude"));
				log.info("openingHour>>"+formInput.get("openingHour"));
				log.info("maker status>>"+formInput.get("status"));
				log.info("maker actionStatus>>"+formInput.get("actionStatus"));
				log.info("maker reason>>"+formInput.get("reason"));
				log.info("");
				count++;


				if(formInput!=null && !formInput.isEmpty()){

					int rowId			= formInput.get("id") != null ? Integer.parseInt(formInput.get("id")) : 0;
					int stationId		= formInput.get("stationId") != null ? Integer.parseInt(formInput.get("stationId")) : 0;
					String regionName	= formInput.get("regionName") != null ? formInput.get("regionName") : "".trim();
					String stationName	= formInput.get("stationName") != null ? formInput.get("stationName") : "".trim();
					String addressLine1 = formInput.get("addressLine1") != null ? formInput.get("addressLine1") : "".trim();
					String addressLine2 = formInput.get("addressLine2") != null ? formInput.get("addressLine2") : "".trim();
					String addressLine3 = formInput.get("addressLine3") != null ? formInput.get("addressLine3") : "".trim();
					String postcode 	= formInput.get("postcode") != null ? formInput.get("postcode") : "".trim();
					String city			= formInput.get("city") != null ? formInput.get("city") : "".trim();
					double longitude	= formInput.get("longitude") != null ? Double.parseDouble(formInput.get("longitude")) : 0.00;
					double latitude		= formInput.get("latitude") != null ? Double.parseDouble(formInput.get("latitude")) : 0.00;;
					String openingHour 	= formInput.get("openingHour") != null ? formInput.get("openingHour") : "".trim();
					String selectedStatus	= formInput.get("status") != null ? formInput.get("status") : "".trim();
					String actionStatus 	= formInput.get("actionStatus") != null ? formInput.get("actionStatus") : "".trim();					
					String reason  			= formInput.get("reason") != null ? formInput.get("reason") : "".trim();

					/*maker edit approved locator ,
					insert new record into edited table
					Approved*/
					if(selectedStatus.equalsIgnoreCase(ApplicationConstant.APPROVED)){
						if(	actionStatus.equalsIgnoreCase(ApplicationConstant.CATEGORY_EDITED)){//edited
							c0.setId(rowId);							
							c0.setCategory(ApplicationConstant.CATEGORY_EDITED);
							boolean update = maintenanceService.updateCSCCustomByMaker(c0);
							log.info("update >>"+update);

							if(update){
								CustServiceCounterEdited e0 = new CustServiceCounterEdited();
								e0.setAdminUserId(user.getId());
								e0.setCustServiceLocationId(stationId);
								e0.setAddressLine1(addressLine1);
								e0.setAddressLine2(addressLine2);
								e0.setAddressLine3(addressLine3);
								e0.setCity(city);
								e0.setPostcode(postcode);
								e0.setLatitude(latitude);
								e0.setLongitude(longitude);
								e0.setOpeningHour(openingHour);
								e0.setStatus(ApplicationConstant.PENDING);
								e0.setCategory(ApplicationConstant.CATEGORY_EDITED);
								e0.setEditedCustServiceLocation(maintenanceService.findCustServiceLocationById(stationId));
								e0.setCounterId(rowId);
								e0.setRejectReason(reason);

								maintenanceService.saveEditedCSC(e0);
								//updateMakerCSC
								log.info("Update (Approved)Service Counter Locator: "+user.getLoginId()+",region name:"+regionName+",station name:"+stationName+",reason:"+reason);
								reportService.auditLog("Service Counter Locator", new AuditTrail(user.getId(),0,"","Update (Approved)Service Counter Locator: "+user.getLoginId(),"","region name:"+regionName+",station name:"+stationName+",reason:"+reason,""));		

								//START send email
								BufferedReader br = null;
								try {
									String sCurrentLine;
									int numberOfLine = 1;
									String filePath = "";

									filePath = System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE)==null ?System.getProperty("catalina.home") + File.separator + "csc_email" + File.separator + "maker_updated_approved_email.txt" : System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE) + File.separator + "csc_email" + File.separator + "maker_updated_approved_email.txt";

									br = new BufferedReader(new FileReader(filePath));					

									String subject = "";
									StringBuffer strBuffer = new StringBuffer();
									while ((sCurrentLine = br.readLine()) != null) {
										if(numberOfLine==1)
											subject = sCurrentLine;
										else							
											strBuffer.append(sCurrentLine);

										numberOfLine++;
									}

									log.info("login id ="+user.getLoginId());
									String body =  strBuffer.toString().replace("{loginID}", user.getLoginId());						

									Mail mail = new Mail();
									Setting setting = settingService.findByKey(ApplicationConstant.CHECKER_EMAIL);
									List<String> emailList = new ArrayList<String>();
									emailList.add(setting.getSettingValue());

									mail.sendMail("Service Counter Locator", user, user.getLoginId(), emailList, new ArrayList(), new ArrayList(), subject, body, "text/html");
								} catch (IOException e) {
									e.printStackTrace();
									log.error(e.getMessage());
								} finally {
									try {
										if (br != null)br.close();
									} catch (IOException ex) {
										ex.printStackTrace();
										log.error(ex.getMessage());
									}
								}
								//END send email
							}

						}
						else if(actionStatus.equalsIgnoreCase(ApplicationConstant.CATEGROY_DELETED)){//DELETED
							c0.setId(rowId);							
							c0.setCategory(ApplicationConstant.CATEGROY_DELETED);
							boolean update = maintenanceService.updateCSCCustomByMaker(c0);
							log.info("update >>"+update);

							if(update){
								CustServiceCounterEdited e0 = new CustServiceCounterEdited();
								e0.setAdminUserId(user.getId());
								e0.setCustServiceLocationId(stationId);
								e0.setAddressLine1(addressLine1);
								e0.setAddressLine2(addressLine2);
								e0.setAddressLine3(addressLine3);
								e0.setCity(city);
								e0.setPostcode(postcode);
								e0.setLatitude(latitude);
								e0.setLongitude(longitude);
								e0.setOpeningHour(openingHour);
								e0.setStatus(ApplicationConstant.PENDING);
								e0.setCategory(ApplicationConstant.CATEGROY_DELETED);
								e0.setEditedCustServiceLocation(maintenanceService.findCustServiceLocationById(stationId));
								e0.setCounterId(rowId);
								e0.setRejectReason(reason);

								maintenanceService.saveEditedCSC(e0);
								log.info("Delete (Approved)Service Counter Locator: "+user.getLoginId()+",region name:"+regionName+",station name:"+stationName+",reason:"+reason);
								reportService.auditLog("Service Counter Locator", new AuditTrail(user.getId(),0,"","Delete (Approved)Service Counter Locator: "+user.getLoginId(),"","region name:"+regionName+",station name:"+stationName+",reason:"+reason,""));		

								//START send email
								BufferedReader br = null;
								try {
									String sCurrentLine;
									int numberOfLine = 1;
									String filePath = "";

									filePath = System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE)==null ?System.getProperty("catalina.home") + File.separator + "csc_email" + File.separator + "maker_updated_approved_email.txt" : System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE) + File.separator + "csc_email" + File.separator + "maker_updated_approved_email.txt";

									br = new BufferedReader(new FileReader(filePath));					

									String subject = "";
									StringBuffer strBuffer = new StringBuffer();
									while ((sCurrentLine = br.readLine()) != null) {
										if(numberOfLine==1)
											subject = sCurrentLine;
										else							
											strBuffer.append(sCurrentLine);

										numberOfLine++;
									}

									log.info("login id ="+user.getLoginId());
									String body =  strBuffer.toString().replace("{loginID}", user.getLoginId());						

									Mail mail = new Mail();
									Setting setting = settingService.findByKey(ApplicationConstant.CHECKER_EMAIL);
									List<String> emailList = new ArrayList<String>();
									emailList.add(setting.getSettingValue());

									mail.sendMail("Service Counter Locator", user, user.getLoginId(), emailList, new ArrayList(), new ArrayList(), subject, body, "text/html");
								} catch (IOException e) {
									e.printStackTrace();
									log.error(e.getMessage());
								} finally {
									try {
										if (br != null)br.close();
									} catch (IOException ex) {
										ex.printStackTrace();
										log.error(ex.getMessage());
									}
								}
								//END send email
							}

						}//end DELETED

					}else{
						//update rejected NEW csc
						c0.setId(rowId);
						c0.setAdminUserId(user.getId());
						c0.setCustServiceLocationId(stationId);
						c0.setAddressLine1(addressLine1);
						c0.setAddressLine2(addressLine2);
						c0.setAddressLine3(addressLine3);
						c0.setCity(city);
						c0.setPostcode(postcode);
						c0.setLatitude(latitude);
						c0.setLongitude(longitude);
						c0.setOpeningHour(openingHour);
						c0.setStatus(ApplicationConstant.PENDING);	
						c0.setCategory(ApplicationConstant.CATEGORY_NEW);
						c0.setCustServLoc(maintenanceService.findCustServiceLocationById(stationId));

						maintenanceService.updateCSCByMaker(c0);
						log.info("Update Service Counter Locator: "+user.getLoginId()+",region name:"+regionName+",station name:"+stationName+",reason:"+reason);
						reportService.auditLog("Service Counter Locator", new AuditTrail(user.getId(),0,"","Update Service Counter Locator: "+user.getLoginId(),"","region name:"+regionName+",station name:"+stationName+",reason:"+reason,""));		

						//START send email
						BufferedReader br = null;
						try {
							String sCurrentLine;
							int numberOfLine = 1;
							String filePath = "";

							filePath = System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE)==null ?System.getProperty("catalina.home") + File.separator + "csc_email" + File.separator + "updated_rejected_csc_email.txt" : System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE) + File.separator + "csc_email" + File.separator + "updated_rejected_csc_email.txt";

							br = new BufferedReader(new FileReader(filePath));					

							String subject = "";
							StringBuffer strBuffer = new StringBuffer();
							while ((sCurrentLine = br.readLine()) != null) {
								if(numberOfLine==1)
									subject = sCurrentLine;
								else							
									strBuffer.append(sCurrentLine);

								numberOfLine++;
							}

							log.info("login id ="+user.getLoginId());
							String body =  strBuffer.toString().replace("{loginID}", user.getLoginId());						

							Mail mail = new Mail();
							Setting setting = settingService.findByKey(ApplicationConstant.CHECKER_EMAIL);
							List<String> emailList = new ArrayList<String>();
							emailList.add(setting.getSettingValue());

							mail.sendMail("Service Counter Locator", user, user.getLoginId(), emailList, new ArrayList(), new ArrayList(), subject, body, "text/html");
						} catch (IOException e) {
							e.printStackTrace();
							log.error(e.getMessage());
						} finally {
							try {
								if (br != null)br.close();
							} catch (IOException ex) {
								ex.printStackTrace();
								log.error(ex.getMessage());
							}
						}
						//END send email

					}

				}
			}
		} catch (Exception ex) {
			reportService.auditLog("Service Counter Locator", new AuditTrail(user.getId(),0,"","Service Counter Locator(Maker): "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));			
			log.error(ex.getMessage());
			ex.printStackTrace();		

		}

		Map<String,String> map = new HashMap<String,String>();
		map.put("success", StringUtil.showCommonMsg("success", "Successfully update ("+count+") service counter locator."));

		return map;
	}

	@RequestMapping(value = "/admin/updateCheckerCSC",  method = RequestMethod.POST )
	public @ResponseBody Map<String,String> updateCheckerCSC( @RequestBody List<Map<String, String>> client)
			throws IOException {
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		int count = 0;
		String myStatus = "";
		String error  = "";
		Map<String,String> map = new HashMap<String,String>();
		String emailFileName = "";

		try {

			CustServiceCounter c0 = new CustServiceCounter();
			for (Map<String, String> formInput : client) {
				//formInputs.put(formInput.get("name"), formInput.get("value"));
				/*System.out.println(formInput.get("id"));
				System.out.println(formInput.get("status"));
				System.out.println(formInput.get("reason"));
				System.out.println("no "+count);
				System.out.println("id>>"+formInput.get("id"));
				System.out.println("stationId>>"+formInput.get("stationId"));
				System.out.println("regionName>>"+formInput.get("regionName"));
				System.out.println("addressLine1>>"+formInput.get("addressLine1"));
				System.out.println("addressLine2>>"+formInput.get("addressLine2"));
				System.out.println("addressLine3>>"+formInput.get("addressLine3"));
				System.out.println("postcode>>"+formInput.get("postcode"));
				System.out.println("city>>"+formInput.get("city"));
				System.out.println("longitude>>"+formInput.get("longitude"));
				System.out.println("latitude>>"+formInput.get("latitude"));
				System.out.println("openingHour>>"+formInput.get("openingHour"));
				System.out.println("");*/



				if(formInput!=null && !formInput.isEmpty()){
					count++;
					log.info("=====updateCheckerCSC============");
					log.info("reason >>"+ formInput.get("reason")+"<<<<");
					log.info("checker status>>"+formInput.get("status"));
					log.info("checker reason>>"+formInput.get("reason"));
					log.info("checker category>>"+formInput.get("category"));
					log.info("checker maintable id>>"+formInput.get("id"));
					log.info("checker editedId>>"+formInput.get("editedId"));
					log.info("checker maintable>>"+formInput.get("mainTable"));

					int rowId		= formInput.get("id") != null ? Integer.parseInt(formInput.get("id")) : 0;
					int stationId	= formInput.get("stationId") != null ? Integer.parseInt(formInput.get("stationId")) : 0;
					String reason	= formInput.get("reason") != null ? formInput.get("reason") : "".trim();
					String status 	= formInput.get("status") != null ? formInput.get("status") : "P".trim();
					int adminUserId = formInput.get("adminUserId") != null ?  Integer.parseInt(formInput.get("adminUserId")) : 0;
					String category	= formInput.get("category") != null ? formInput.get("category") : "".trim();
					int editedId		= formInput.get("editedId") != null ? Integer.parseInt(formInput.get("editedId")) : 0;

					System.out.println("admin user id>"+adminUserId);
					if(status.equalsIgnoreCase("R") && reason.length()<=0){
						error = "Reject reason is required";
					}

					c0.setId(rowId);	
					c0.setAdminUserId(adminUserId);
					c0.setStatus(status);		
					c0.setCategory(ApplicationConstant.CATEGORY_NEW);
					c0.setRejectReason(reason);
					c0.setApproverId(user.getId());
					System.out.println("admin id >>"+adminUserId);
					System.out.println("approved id >>"+user.getId());
					c0.setCustServLoc(maintenanceService.findCustServiceLocationById(stationId));
					CustServiceLocation loc = maintenanceService.findCustServiceLocationById(stationId);


					if(status.equalsIgnoreCase(ApplicationConstant.APPROVED)){
						if(category.equalsIgnoreCase(ApplicationConstant.CATEGORY_NEW)){							
							maintenanceService.updateCSCByChecker(c0);
							emailFileName = "NEW_maker_approved_email.txt";								
						}else if(category.equalsIgnoreCase(ApplicationConstant.CATEGORY_EDITED)){
							CustServiceCounterEdited editedObject = maintenanceService.findCustServiceCounterEditedById(editedId);
							System.out.println("editedObject >>"+editedObject);
							if(editedObject!=null){
								System.out.println("here>>"+rowId);
								System.out.println("here 2>>"+editedObject.toString());

								c0.setId(rowId);//main table id
								c0.setAdminUserId(editedObject.getAdminUserId());
								c0.setCustServiceLocationId(editedObject.getCustServiceLocationId());
								c0.setAddressLine1(editedObject.getAddressLine1());
								c0.setAddressLine2(editedObject.getAddressLine2());
								c0.setAddressLine3(editedObject.getAddressLine3());
								c0.setCity(editedObject.getCity());
								c0.setPostcode(editedObject.getPostcode());
								c0.setLatitude(editedObject.getLatitude());
								c0.setLongitude(editedObject.getLongitude());
								c0.setOpeningHour(editedObject.getOpeningHour());
								c0.setStatus(ApplicationConstant.APPROVED);	
								c0.setCategory(ApplicationConstant.CATEGORY_NEW);
								c0.setCustServLoc(maintenanceService.findCustServiceLocationById(stationId));

								boolean update = false;
								update = maintenanceService.updateAprovedEditedCSCByChecker(c0);
								if(update){
									maintenanceService.deleteEditedCSC(editedId);
									emailFileName = "EDITED_maker_approved_email.txt";									
									System.out.println("end edited");
								}
							}
						}else if(category.equalsIgnoreCase(ApplicationConstant.CATEGROY_DELETED)){
							CustServiceCounterEdited editedObject = maintenanceService.findCustServiceCounterEditedById(editedId);
							System.out.println("editedObject >>"+editedObject);
							if(editedObject!=null){
								c0.setAdminUserId(editedObject.getAdminUserId());
								c0.setStatus(ApplicationConstant.DELETED);	
								c0.setCategory(ApplicationConstant.CATEGROY_DELETED);
								c0.setRejectReason(editedObject.getRejectReason());								

								boolean update = false;
								update = maintenanceService.updateCSCByChecker(c0);
								if(update){
									maintenanceService.deleteEditedCSC(editedId);
									emailFileName = "DELETED_maker_approved_email.txt";
									System.out.println("end edited");
								}	
							}
						}

					}else if(status.equalsIgnoreCase(ApplicationConstant.REJECTED)){
						if(category.equalsIgnoreCase(ApplicationConstant.CATEGORY_NEW)){
							maintenanceService.updateCSCByChecker(c0);
							emailFileName = "NEW_maker_rejected_email.txt";

						}else if(category.equalsIgnoreCase(ApplicationConstant.CATEGORY_EDITED)){//edited
							c0.setCategory(ApplicationConstant.CATEGORY_NEW);
							c0.setStatus(ApplicationConstant.APPROVED);
							boolean update = false;
							update = maintenanceService.updateCSCByChecker(c0);
							if(update){
								maintenanceService.deleteEditedCSC(editedId);
								emailFileName = "EDITED_maker_rejected_email.txt";
								System.out.println("end edited");
							}
						}
						else if(category.equalsIgnoreCase(ApplicationConstant.CATEGROY_DELETED)){//deleted
							c0.setCategory(ApplicationConstant.CATEGORY_NEW);
							c0.setStatus(ApplicationConstant.APPROVED);
							boolean update = false;
							update = maintenanceService.updateCSCByChecker(c0);
							if(update){
								maintenanceService.deleteEditedCSC(editedId);
								emailFileName = "DELETED_maker_rejected_email.txt";
								System.out.println("end edited");
							}
						}

					}


					switch (status) {
					case "A":
						status = "Approved";
						break;
					case "D":
						status = "Deleted";
						break;
					case "P":
						status = "Pending";
						break;
					case "R":
						status = "Rejected";
						break;	       
					}

					map.put("success", StringUtil.showCommonMsg("success", "Successfully update ("+count+") service counter locator."));
					reportService.auditLog("Service Counter Locator", new AuditTrail(user.getId(),0,"","Service Counter Locator: "+user.getLoginId(),"Pending",myStatus,"Reject Reason :"+reason));

					//START send email
					BufferedReader br = null;
					try {
						String sCurrentLine;
						int numberOfLine = 1;
						String filePath = "";

						filePath = System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE)==null ?System.getProperty("catalina.home") + File.separator + "csc_email" + File.separator + emailFileName : System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE) + File.separator + "csc_email" + File.separator + emailFileName;

						log.info(filePath);
						br = new BufferedReader(new FileReader(filePath));					

						String subject = "";
						StringBuffer strBuffer = new StringBuffer();
						while ((sCurrentLine = br.readLine()) != null) {
							if(numberOfLine==1)
								subject = sCurrentLine;
							else							
								strBuffer.append(sCurrentLine);

							numberOfLine++;
						}

						String body = "";

						if(status.equalsIgnoreCase("Rejected")){
							System.out.println("status>>"+status);
							body =  strBuffer.toString().replace("{reason}", reason);
						}else{
							body =  strBuffer.toString().replace("{reason}", "");
						}

						body = body.toString().replace("{area}", loc.getStation());

						System.out.println("subject :"+subject);
						System.out.println("body :"+body);

						Mail mail = new Mail();
						List<String> emailList = new ArrayList<String>();
						emailList.add(loginService.findExactAdminUser(adminUserId).getName());//name in admin portal is email
						mail.sendMail("Service Counter Locator", user, user.getLoginId(), emailList, new ArrayList(), new ArrayList(), subject, body, "text/html");

					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						try {
							if (br != null)br.close();
						} catch (IOException ex) {
							ex.printStackTrace();
						}
					}
					//END send email

				}
			}
		} catch (Exception ex) {
			error +=" Fail to update service counter locator.";
			reportService.auditLog("Service Counter Locator", new AuditTrail(user.getId(),0,"","Service Counter Locator(Checker): "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
			log.error(ex.getMessage());
			ex.printStackTrace();		

		}

		map.put("error", StringUtil.showCommonMsg("error", error));

		return map;
	}

	@RequestMapping(value = "/admin/addNews",  method = RequestMethod.GET)
	public ModelAndView doAddNews()	{
		ModelAndView model = new ModelAndView();
		model.setViewName("/maintenance/addNews");
		return model;
	}

	/**
	 * Upload single file using Spring Controller
	 */
	@RequestMapping(value = "/admin/addNews", method = RequestMethod.POST)
	public @ResponseBody ModelAndView saveNews(			 
			@RequestParam(value = "title", defaultValue = "") String title,
			@RequestParam(value ="simg", required=false) MultipartFile sFile, 
			@RequestParam("limg") MultipartFile lFile,
			@RequestParam(value = "desc", defaultValue = "") String desc,
			@RequestParam(value = "instartDate", defaultValue = "") String startDate,
			@RequestParam(value = "inendDate", defaultValue = "") String endDate,
			@RequestParam(value = "inpublishDate", defaultValue = "") String publishDate,
			@RequestParam(value = "pushNotif", defaultValue = "") String pushNotif,
			@RequestParam(value = "inpushDateTime", defaultValue = "") String pushDateTime,
			@RequestParam("text") String text
			){
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();

		if (!(title.isEmpty() && desc.isEmpty() 
				&& startDate.isEmpty() && endDate.isEmpty() && publishDate.isEmpty())) {
			try {

				pushDateTime = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(pushDateTime);

				DateUtil dateUtil = new DateUtil();			

				String convertStartDate = "";
				String convertEndDate = "";
				String simgFileName = "";

				convertStartDate	= (StringUtil.trimToEmpty(startDate).equals("")) ? dateUtil.getCurrentDate()+ " 00:00:00" : startDate+ " 00:00:00";						
				convertEndDate		= (StringUtil.trimToEmpty(endDate).equals("")) ? dateUtil.getCurrentDate()+ " 23:59:59" : endDate+ " 23:59:59";						


				System.out.println("title ::"+title);

				System.out.println("desc ::"+desc);
				System.out.println("startDate ::"+startDate);
				System.out.println("endDate ::"+endDate);
				System.out.println("publishDate ::"+publishDate);
				System.out.println("pushNotif ::"+pushNotif);
				System.out.println("pushDateTime ::"+pushDateTime);
				System.out.println("convertStartDate ::"+convertStartDate);
				System.out.println("convertEndDate ::"+convertEndDate);
				System.out.println("text ::"+text);

				// Creating the directory to store file
				String rootPath = System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE);
				File dir = new File(rootPath + File.separator + "news_images");
				if (!dir.exists())
					dir.mkdirs();


				//System.out.println("Server File Location="+serverFile.getAbsolutePath());
				//System.out.println("Server File2 Location="+ serverFile2.getAbsolutePath());
				News news = new News();
				news.setTitle(title.trim());
				news.setDescription(desc.trim());
				news.setStartDatetime(StringUtil.convertStringToTimestamp(convertStartDate, "dd/MM/yyyy hh:mm:ss"));
				news.setEndDatetime(StringUtil.convertStringToTimestamp(convertEndDate, "dd/MM/yyyy hh:mm:ss"));
				news.setPublishDatetime(StringUtil.convertStringToTimestamp(publishDate, "dd/MM/yyyy"));


				// Create the file on server
				if(sFile != null && !sFile.isEmpty()){
					byte[] bytes = sFile.getBytes();
					//File serverFile = new File(dir.getAbsolutePath()
					//		+ File.separator + sFile.getOriginalFilename());
					//simg
					System.out.println("file name >>"+dir.getAbsolutePath()
					+ File.separator + sFile.getOriginalFilename());
					//BufferedOutputStream stream = new BufferedOutputStream(
					//		new FileOutputStream(serverFile));
					//stream.write(bytes);
					//stream.close();

					news.setImageS(bytes);
					//news.setImageSFileType(Files.probeContentType(serverFile.toPath()));
					System.out.println(user.getLoginId() +" upload news image["+sFile.getOriginalFilename()+"],["+sFile.getContentType()+"]");
					log.info(user.getLoginId() +" upload news image["+sFile.getOriginalFilename()+"],["+sFile.getContentType()+"]");
					news.setImageSFileType(sFile.getContentType());
					simgFileName = sFile.getOriginalFilename();
				}

				if(lFile != null && !lFile.isEmpty()){
					byte[] bytes2 = lFile.getBytes();
					File serverFile2 = new File(dir.getAbsolutePath()
							+ File.separator + lFile.getOriginalFilename());
					//limg
					System.out.println(">>"+serverFile2);
					/*BufferedOutputStream stream2 = new BufferedOutputStream(
							new FileOutputStream(serverFile2));
					stream2.write(bytes2);
					stream2.close();*/

					news.setImageL(bytes2);
					news.setImageLFileType(Files.probeContentType(serverFile2.toPath()));
				}
				News newNews = maintenanceService.saveNews(news);

				TagNotification tag0 = new TagNotification();
				if(newNews!=null && Integer.parseInt(pushNotif)==1){					
					tag0.setNotificationType(ApplicationConstant.NOTIF_TYPE_PROMO_KEY);
					tag0.setAssociatedId(newNews.getId());
					tag0.setText(StringUtil.trimToEmpty(text.trim()));				
					tag0.setTagName(ApplicationConstant.TAG_NAME_GENERAL);
					tag0.setStatus(ApplicationConstant.NOTIF_STATUS_UNSENT);
					tag0.setPushDatetime(StringUtil.convertStringToTimestamp(pushDateTime, "dd/MM/yyyy hh:mm:ss a"));				
					tag0 = maintenanceService.savePushNotication(tag0);
				}				

				model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully added news."));
				reportService.auditLog("News", new AuditTrail(user.getId(),0,"","Add News: "+user.getLoginId(),"", "title:["+title+"], simg:["+simgFileName+"], desc:["+desc.trim()+"], startDate:["+startDate+"], endDate:["+endDate+"], publishDate:["+publishDate+"], pushNotif:["+tag0.getStatus()+"], pushDateTime:["+pushDateTime+"]", ""));		


			} catch (Exception ex) {
				ex.printStackTrace();
				log.error(ex.getMessage());
				System.out.println(ex.getCause());
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to add news"));
				reportService.auditLog("News", new AuditTrail(user.getId(),0,"","Add News: "+user.getLoginId(),"", "Fail to process", "Fail-Message:"+ex.getMessage()));		
			}
		} else {
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Mandatory inputs are required."));
		}
		model.setViewName("/maintenance/addNews");
		return model;
	}

	/**
	 * Upload multiple file using Spring Controller
	 */
	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST)
	public @ResponseBody
	String uploadMultipleFileHandler(@RequestParam("title") String[] names,
			@RequestParam("file") MultipartFile[] files) {

		if (files.length != names.length)
			return "Mandatory information missing";

		String message = "";
		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			String name = names[i];
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				/*BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();*/

				System.out.println("Server File Location="
						+ serverFile.getAbsolutePath());

				message = message + "You successfully uploaded file=" + name
						+ "<br />";
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		}
		return message;
	}


	@RequestMapping(value = "/admin/editNews",  method = RequestMethod.GET)
	public ModelAndView doEditNews()	{
		ModelAndView model = new ModelAndView();
		model.setViewName("/maintenance/editNews");
		return model;
	}

	@RequestMapping(value = "/admin/editNews",  method = RequestMethod.POST)
	public ModelAndView searchNews(@RequestParam (value = "instartDate") String dateFrom,
			@RequestParam (value = "inendDate") String dateTo,
			@RequestParam (value = "title", defaultValue = "") String title) throws SQLException{

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		List<News> newslist = new ArrayList<News>();
		List<MyNews> myNewsList = new ArrayList<MyNews>();

		DateUtil dateUtil = new DateUtil();
		Date myStartDate = new Date();
		Date myEndDate = new Date();
		//Date today = dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");
		Date today = new Timestamp(System.currentTimeMillis());

		myStartDate  = (StringUtil.trimToEmpty(dateFrom).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateFrom+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");		
		myEndDate  = (StringUtil.trimToEmpty(dateTo).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(dateTo+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						


		newslist = maintenanceService.findNews(myStartDate, myEndDate, title.trim());


		for(News n : newslist){
			System.out.println("================= [START NEWS] ============================");
			System.out.println("getId >>"+n.getId());
			System.out.println("getTitle >>"+n.getTitle());

			System.out.println("================= [END NEWS] ============================");
			MyNews my = new MyNews();
			my.setId(n.getId());
			my.setTitle(n.getTitle());
			my.setImageS(n.getImageS());
			my.setImageSFileType(n.getImageSFileType());
			my.setImageL(n.getImageL());
			my.setImageLFileType(n.getImageLFileType());
			my.setDescription(n.getDescription());
			my.setStartDatetime(n.getStartDatetime());
			my.setEndDatetime(n.getEndDatetime());
			my.setPublishDatetime(n.getPublishDatetime());
			my.setCreatedDatetime(n.getCreatedDatetime());
			/*System.out.println("enddate>>>>>>>>"+n.getEndDatetime());
			System.out.println("my news enddate>>>>>>>>"+my.getEndDatetime());
			System.out.println("today>>>>>>>>"+today);*/

			List <TagNotification> notifList = maintenanceService.findContentByAssIdAndType(my.getId(), ApplicationConstant.NOTIF_TYPE_PROMO_KEY);
			System.out.println("notifList size::"+notifList.size());
			List myTagList = new ArrayList<MyTagNotification>();
			if(notifList!=null && notifList.size()>0){				
				for(int i=0;i<notifList.size();i++){
					MyTagNotification innerTag = new MyTagNotification();
					innerTag.setNotificationId(notifList.get(i).getNotificationId());
					innerTag.setAssociatedId(notifList.get(i).getAssociatedId());				
					innerTag.setText(StringUtil.trimToEmpty(notifList.get(i).getText()));
					innerTag.setNotificationType(notifList.get(i).getNotificationType());
					innerTag.setTagName(notifList.get(i).getTagName());
					innerTag.setStatus(notifList.get(i).getStatus());
					innerTag.setPushDatetime(notifList.get(i).getPushDatetime());

					myTagList.add(innerTag);
				}

				my.setMyTagList(myTagList);
				/*my.setNotificationId(notifList.get(0).getNotificationId());
				my.setAssociatedId(notifList.get(0).getAssociatedId());				
				my.setText(StringUtil.trimToEmpty(notifList.get(0).getText()));
				my.setNotificationType(notifList.get(0).getNotificationType());
				my.setTagName(notifList.get(0).getTagName());
				my.setStatus(notifList.get(0).getStatus());
				my.setPushDatetime(notifList.get(0).getPushDatetime());*/

			}else{

				MyTagNotification innerTag = new MyTagNotification();				
				innerTag.setStatus(ApplicationConstant.NOTIF_STATUS_OFF);
				innerTag.setNotificationId(0);
				innerTag.setAssociatedId(null);				
				innerTag.setText(null);
				innerTag.setNotificationType(null);
				innerTag.setTagName(null);
				innerTag.setStatus(ApplicationConstant.NOTIF_STATUS_OFF);
				innerTag.setPushDatetime(null);
				myTagList.add(innerTag);

				my.setMyTagList(myTagList);
				/*my.setStatus(ApplicationConstant.NOTIF_STATUS_OFF);
				my.setNotificationId(0);
				my.setAssociatedId(null);				
				my.setText(null);
				my.setNotificationType(null);
				my.setTagName(null);
				my.setStatus(ApplicationConstant.NOTIF_STATUS_OFF);*/
				my.setPushDatetime(null);
			}
			myNewsList.add(my);				
		}

		reportService.auditLog("News", new AuditTrail(user.getId(),0,"","Search News: "+user.getLoginId(),"", "dateFrom:["+dateFrom+"], dateTo:["+dateTo+"], title:["+title+"]", ""));		

		model.addObject("dateFrom",dateFrom);
		model.addObject("dateTo",dateTo);
		model.addObject("title",title);
		model.addObject("newslist",myNewsList);	

		List <MyNews> test1ist =  myNewsList;
		for(MyNews a : myNewsList){
			for(MyTagNotification b : a.getMyTagList()){
				System.out.println("b>>>"+b.getText());
			}
		}

		model.setViewName("/maintenance/editNews");
		return model;
	}


	@RequestMapping(value="/admin/getNewsImage/{type}/{id}")
	public void getUserImage(HttpServletResponse response ,  @PathVariable("type") String type, @PathVariable("id") int tweetID) throws IOException{

		//System.out.println("type="+type);
		//System.out.println("id="+tweetID);
		try {
			News myNews = maintenanceService.findNewsById(tweetID);
			response.setContentType(myNews.getImageSFileType());
			//System.out.println("title="+myNews.getTitle());

			if(type.equalsIgnoreCase("simg") && !StringUtil.trimToEmpty(myNews.getImageSFileType()).isEmpty()){
				byte[] buffer = myNews.getImageS();
				InputStream in1 = new ByteArrayInputStream(buffer);
				IOUtils.copy(in1, response.getOutputStream());
				response.getOutputStream().close();

			}else if(type.equalsIgnoreCase("limg") && !StringUtil.trimToEmpty(myNews.getImageLFileType()).isEmpty()){
				byte[] buffer = myNews.getImageL();
				InputStream in1 = new ByteArrayInputStream(buffer);
				IOUtils.copy(in1, response.getOutputStream()); 
				response.getOutputStream().close();
			}

			/*response.getOutputStream().write(myNews.getImageS());*/

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	UploadedFile ufile  = new UploadedFile();
	@RequestMapping(value = "/admin/get/{value}", method = RequestMethod.GET)
	public void get(HttpServletResponse response,@PathVariable String value){
		try {

			response.setContentType(ufile.type); 
			response.setContentLength(ufile.length);
			FileCopyUtils.copy(ufile.bytes, response.getOutputStream());
			/*
	            URL url = new URL("http://www.mkyong.com/image/mypic.jpg");
	            image = ImageIO.read(url);

	            ImageIO.write(image, "jpg",new File("C:\\out.jpg"));
	            ImageIO.write(image, "gif",new File("C:\\out.gif"));
	            ImageIO.write(image, "png",new File("C:\\out.png"));*/

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Upload multiple file using Spring Controller
	 *//*
	@RequestMapping(value = "/admin/upload", method = RequestMethod.POST)
	public @ResponseBody  String upload(@RequestParam("title") String[] names,@RequestParam("checkbox") String[] checkboxs,
			@RequestParam("file") MultipartFile[] files) {

		if (files.length != names.length)
			return "Mandatory information missing";

		String message = "";
		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			String name = names[i];
			String checkbox = checkboxs[i];
			System.out.println("name="+name);
			System.out.println("checkbox="+checkbox);

			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath()
						+ File.separator + name);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				System.out.println("Server File Location="
						+ serverFile.getAbsolutePath());

				message = message + "You successfully uploaded file=" + name
						+ "<br />";
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		}
		return message;
	}
	  */

	/*@RequestMapping(value = "/admin/upload", method = RequestMethod.POST)
	public @ResponseBody  String upload(MultipartHttpServletRequest request, HttpServletResponse response) {                 
		AuthenticAdmin user=null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		user=(AuthenticAdmin)principal;	 

		//0. notice, we have used MultipartHttpServletRequest
		String id = request.getParameter("id");
		System.out.println("upload title="+request.getParameter("title"));
		System.out.println("upload title="+request.getParameter("title"));
		System.out.println("upload id:"+id);

		System.out.println("upload checkbox:"+request.getParameter("checkbox"));
		//1. get the files from the request object
		Iterator<String> itr =  request.getFileNames();

		MultipartFile mpf = request.getFile(itr.next());
		System.out.println(mpf.getOriginalFilename() +" uploaded!");

		try {
			//just temporary save file info into ufile
			ufile.length = mpf.getBytes().length;
			ufile.bytes= mpf.getBytes();
			ufile.type = mpf.getContentType();
			ufile.name = mpf.getOriginalFilename();


			byte[] bytes = mpf.getBytes();
			BufferedOutputStream stream =
					new BufferedOutputStream(new FileOutputStream(new File("E:/SEB/templates/news_images/"+Calendar.getInstance().getTimeInMillis()+"_simg_"+  mpf.getOriginalFilename())));
			stream.write(bytes);
			stream.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//2. send it back to the client as <img> that calls get method
		//we are using getTimeInMillis to avoid server cached image 
		//  String a ="<img id=\"spreview"+id+"\" src="${pageContext.request.contextPath}/admin/getNewsImage/simg/<c:out value="${item.id}"/>.do" width="140" height="107"/>";
		   HashMap<String, Object> map = new HashMap<String, Object>();
	        map.put("image", "<img src='"+request.getContextPath()+"/admin/get/"+Calendar.getInstance().getTimeInMillis()+"' />");
	        map.put("success", "yeah");


		return "<img src='"+request.getContextPath()+"/admin/get/"+Calendar.getInstance().getTimeInMillis()+"' />";

	}*/


	/*@RequestMapping(value = "/admin/upload", method = RequestMethod.POST)
	public @ResponseBody  String upload(MultipartHttpServletRequest request, HttpServletResponse response) {                 
		AuthenticAdmin user=null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		user=(AuthenticAdmin)principal;	 

		//0. notice, we have used MultipartHttpServletRequest
		String id = request.getParameter("id");
		System.out.println("upload title="+request.getParameter("title"));
		System.out.println("upload title="+request.getParameter("title"));
		System.out.println("upload id:"+id);

		System.out.println("upload checkbox:"+request.getParameter("checkbox"));
		//1. get the files from the request object
		Iterator<String> itr =  request.getFileNames();

		MultipartFile mpf = request.getFile(itr.next());
		System.out.println(mpf.getOriginalFilename() +" uploaded!");

		try {
			//just temporary save file info into ufile
			ufile.length = mpf.getBytes().length;
			ufile.bytes= mpf.getBytes();
			ufile.type = mpf.getContentType();
			ufile.name = mpf.getOriginalFilename();


			byte[] bytes = mpf.getBytes();
			BufferedOutputStream stream =
					new BufferedOutputStream(new FileOutputStream(new File("E:/SEB/templates/news_images/"+Calendar.getInstance().getTimeInMillis()+"_simg_"+  mpf.getOriginalFilename())));
			stream.write(bytes);
			stream.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//2. send it back to the client as <img> that calls get method
		//we are using getTimeInMillis to avoid server cached image 
		//  String a ="<img id=\"spreview"+id+"\" src="${pageContext.request.contextPath}/admin/getNewsImage/simg/<c:out value="${item.id}"/>.do" width="140" height="107"/>";
		   HashMap<String, Object> map = new HashMap<String, Object>();
	        map.put("image", "<img src='"+request.getContextPath()+"/admin/get/"+Calendar.getInstance().getTimeInMillis()+"' />");
	        map.put("success", "yeah");


		return "<img src='"+request.getContextPath()+"/admin/get/"+Calendar.getInstance().getTimeInMillis()+"' />";

	}*/

	@RequestMapping(value = "/admin/updateNews", method = RequestMethod.POST)
	public @ResponseBody HashMap<String, Object>  updateNews(HttpServletRequest  request, HttpServletResponse response)throws Exception{                
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();	 

		HashMap<String, Object> map = new HashMap<String, Object>();
		int count =0;

		Map paramCheckBox = new HashMap();
		Map paramTitle = new HashMap();
		Map paramSimg = new HashMap();
		Map paramLimg = new HashMap();
		Map paramDesc = new HashMap();
		Map paramStartDate = new HashMap();
		Map paramEndDate = new HashMap();
		Map paramPublishDate = new HashMap();
		Map paramPushNotif = new HashMap();
		Map paramPushDateTime = new HashMap();
		Map paramText = new HashMap();
		Map paramNotificatoinId = new HashMap();

		//System.out.println("=============loop===========================================");
		for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
			String fieldName = (String) e.nextElement();
			String fieldValue = request.getParameter(fieldName);
			if ((fieldValue != null) && (fieldValue.length() > 0)) {
				System.out.println(fieldName + ":" + fieldValue);
				if(fieldName.toLowerCase().startsWith("checkbox"))
					paramCheckBox.put(fieldName, fieldValue);
				if(fieldName.toLowerCase().startsWith("title"))
					paramTitle.put(fieldName, fieldValue);
				if(fieldName.toLowerCase().startsWith("newsimg"))
					paramSimg.put(fieldName, fieldValue);
				if(fieldName.toLowerCase().startsWith("newlimg"))
					paramLimg.put(fieldName, fieldValue);
				if(fieldName.toLowerCase().startsWith("description"))
					paramDesc.put(fieldName, fieldValue);
				if(fieldName.toLowerCase().startsWith("instartdate"))
					paramStartDate.put(fieldName, fieldValue);
				if(fieldName.toLowerCase().startsWith("inenddate"))
					paramEndDate.put(fieldName, fieldValue);
				if(fieldName.toLowerCase().startsWith("inpublishdate"))
					paramPublishDate.put(fieldName, fieldValue);
				if(fieldName.toLowerCase().startsWith("pushnotif"))
					paramPushNotif.put(fieldName, fieldValue);
				if(fieldName.toLowerCase().startsWith("inpushdatetime"))
					paramPushDateTime.put(fieldName, fieldValue);
				if(fieldName.toLowerCase().startsWith("text"))
					paramText.put(fieldName, fieldValue);
				if(fieldName.toLowerCase().startsWith("notificationid"))
					paramNotificatoinId.put(fieldName, fieldValue);			

			}
		}
		//System.out.println("=============[end]loop===========================================");

		try{
			//check which row is checked, then will iterate it out.
			Iterator iteratorCheckBox = paramCheckBox.entrySet().iterator();

			//paramSelectedSimg : store small image, paramSelectedLimg : store large image
			Map paramSelectedSimg = new HashMap();
			Map paramSelectedLimg = new HashMap();
			log.info("#################[UPDATE NEWS]############################");
			while (iteratorCheckBox.hasNext()) {

				Map.Entry mapEntry1 = (Map.Entry) iteratorCheckBox.next();
				//String key1 = (String) mapEntry1.getKey();
				String val1 = (String) mapEntry1.getValue();
				String mixId = val1.trim();
				String ids [] = val1.trim().split("-");
				String id = ids[0].toString();
				String notifId = ids[1].toString();

				log.info(new Date());
				log.info("[news] checkbox ::"+id);
				log.info("[news] notif id ::"+notifId);
				log.info("[news] mixid ::"+mixId);
				String title 	= String.valueOf(paramTitle.get("title"+id)).trim();
				String imageS	= String.valueOf(paramSimg.get("newsimg"+id)).trim();
				String imageL	= String.valueOf(paramLimg.get("newlimg"+id)).trim();
				String description	= String.valueOf(paramDesc.get("description"+id)).trim();
				String startDate	= String.valueOf(paramStartDate.get("instartDate"+id)).trim();
				String endDate 		= String.valueOf(paramEndDate.get("inendDate"+id)).trim();
				String publishDate	= String.valueOf(paramPublishDate.get("inpublishDate"+id)).trim();
				String pushNotif	= String.valueOf(paramPushNotif.get("pushNotif"+mixId)).trim();
				String pushDateTime	= String.valueOf(paramPushDateTime.get("inpushDateTime"+mixId)).trim();
				String text				= String.valueOf(paramText.get("text"+mixId)).trim();
				String notificationId	= String.valueOf(paramNotificatoinId.get("notificationId"+mixId)).trim();


				//below is for upload image use
				paramSelectedSimg.put("rsimg"+id, imageS);
				paramSelectedLimg.put("rlimg"+id, imageL);

				log.info("id:"+id);
				log.info("title:"+title);
				log.info("imageS:"+imageS);
				log.info("imageL:"+imageL);
				log.info("description:"+description);
				log.info("startDate:"+startDate);
				log.info("endDate:"+endDate);
				log.info("publishDate:"+publishDate);
				log.info("pushNotif:"+pushNotif);
				log.info("inpushDateTime:"+pushDateTime);
				log.info("text:"+text);
				log.info("notificationId:"+notificationId);
				log.info("==========================================================");



				DateUtil dateUtil = new DateUtil();
				pushDateTime = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(pushDateTime);				

				String convertStartDate = "";
				String convertEndDate = "";
				String convertPublishDate = "";

				convertStartDate	= (StringUtil.trimToEmpty(startDate).equals("")) ? dateUtil.getCurrentDate()+ " 00:00:00" : startDate+ " 00:00:00";						
				convertEndDate		= (StringUtil.trimToEmpty(endDate).equals("")) ? dateUtil.getCurrentDate()+ " 23:59:59" : endDate+ " 23:59:59";		
				convertPublishDate		= (StringUtil.trimToEmpty(publishDate).equals("")) ? dateUtil.getCurrentDate()+ " 00:00:00" : publishDate+ " 00:00:00";				

				News oldNews = new News();
				oldNews = maintenanceService.findNewsById(Integer.parseInt(id));
				String oldPNStatus="OFF";
				String oldPNText = "";
				Timestamp oldPNDateTime = new Timestamp(System.currentTimeMillis());
				String oldTitle = oldNews.getTitle();
				String oldDesc	= oldNews.getDescription();
				Timestamp oldStartDate	= oldNews.getStartDatetime();
				Timestamp oldEndDate 	= oldNews.getEndDatetime();
				Timestamp oldPublishDate= oldNews.getPublishDatetime();

				boolean hasTagNotifyRecord = false;

				if(oldNews!=null){
					List <TagNotification> notifList = maintenanceService.findContentByAssIdAndType(oldNews.getId(), ApplicationConstant.NOTIF_TYPE_PROMO_KEY);
					if(notifList!=null && notifList.size()>0){
						TagNotification oldTag = maintenanceService.findPushNotificationById(Integer.parseInt(notificationId));
						hasTagNotifyRecord = true;
						oldPNStatus = oldTag.getStatus();
						oldPNDateTime =oldTag.getPushDatetime();
						oldPNText = oldTag.getText();
					}else{//dont have tag notification
						hasTagNotifyRecord = false;
						oldPNStatus = "OFF";
					}
				}

				News data = new News();
				data.setId(Integer.parseInt(id));
				data.setTitle(title);
				data.setDescription(description);
				data.setStartDatetime(StringUtil.convertStringToTimestamp(convertStartDate, "dd/MM/yyyy hh:mm:ss"));
				data.setEndDatetime(StringUtil.convertStringToTimestamp(convertEndDate, "dd/MM/yyyy hh:mm:ss"));
				data.setPublishDatetime(StringUtil.convertStringToTimestamp(convertPublishDate, "dd/MM/yyyy hh:mm:ss"));			

				News news0 = maintenanceService.updateNews(data);
				log.info(">>>>>>..news0 :"+news0.toString());
				log.info("=============================================");
				TagNotification tag0 = new TagNotification();
				if(news0!=null){				
					tag0.setNotificationType(ApplicationConstant.NOTIF_TYPE_PROMO_KEY);
					tag0.setAssociatedId(Integer.parseInt(id));
					tag0.setText(StringUtil.trimToEmpty(text));	
					tag0.setTagName(ApplicationConstant.TAG_NAME_GENERAL);

					pushNotif = pushNotif.equalsIgnoreCase("1") ? ApplicationConstant.NOTIF_STATUS_UNSENT :  ApplicationConstant.NOTIF_STATUS_OFF;
					tag0.setStatus(pushNotif);
					tag0.setPushDatetime(StringUtil.convertStringToTimestamp(pushDateTime, "dd/MM/yyyy hh:mm:ss a"));				

					if(hasTagNotifyRecord){
						log.info("[news]update exisiting notification");	
						log.info("[news]notificationId>>"+Integer.parseInt(notificationId));
						tag0.setNotificationId(Integer.parseInt(notificationId));	
						//System.out.println(tag0.toString());
						maintenanceService.updatePushNotification(tag0);
					}else if(hasTagNotifyRecord==false && pushNotif.equalsIgnoreCase(ApplicationConstant.NOTIF_STATUS_UNSENT)){
						log.info("insert new notification");
						/*System.out.println(tag0.toString());*/
						tag0 = maintenanceService.savePushNotication(tag0);						
					}	
				}
				log.info("Update"+user.getLoginId()+", Task ID: "+id+",old ::title:["+oldTitle+"], desc:["+oldDesc+"], startDate:["+oldStartDate+"], endDate:["+oldEndDate+"], publishDate:["+oldPublishDate+"], pushNotif:["+oldPNStatus+"], pushDateTime:["+oldPNDateTime+"],text:["+oldPNText+"]"+", new :: title:["+title+"], desc:["+description+"], startDate:["+startDate+"], endDate:["+endDate+"], publishDate:["+publishDate+"], pushNotif:["+tag0.getStatus()+"], pushDateTime:["+tag0.getPushDatetime()+"],text:["+tag0.getText()+"]");
				reportService.auditLog("News", new AuditTrail(user.getId(),0,"","Update"+user.getLoginId()+", Task ID: "+id,"title:["+oldTitle+"], desc:["+oldDesc+"], startDate:["+oldStartDate+"], endDate:["+oldEndDate+"], publishDate:["+oldPublishDate+"], pushNotif:["+oldPNStatus+"], pushDateTime:["+oldPNDateTime+"],text:["+oldPNText+"]","title:["+title+"], desc:["+description+"], startDate:["+startDate+"], endDate:["+endDate+"], publishDate:["+publishDate+"], pushNotif:["+tag0.getStatus()+"], pushDateTime:["+tag0.getPushDatetime()+"],text:["+tag0.getText()+"]", ""));		
				count++;
			}

			log.info("============[MultipartHttpServletRequest]============");
			if(ServletFileUpload.isMultipartContent(request)){
				log.info("isMultipartContent");

				MultipartHttpServletRequest mpr = (MultipartHttpServletRequest)request;
				if(request instanceof MultipartHttpServletRequest)
				{

					Iterator<String> itr =   mpr.getFileNames();
					while(itr.hasNext()) {

						MultipartFile mpf = mpr.getFile(itr.next());

						System.out.println(mpf.getOriginalFilename() +" uploaded!");
						System.out.println("content type ::"+mpf.getContentType());
						System.out.println("name ::"+mpf.getName());


						try {
							//just temporary save file info into ufile
							ufile.length = mpf.getBytes().length;
							ufile.bytes= mpf.getBytes();
							ufile.type = mpf.getContentType();
							ufile.name = mpf.getOriginalFilename();


							String rootPath = System.getProperty(ApplicationConstant.SEB_TEMPLATES_BASE);
							File dir = new File(rootPath + File.separator + "news_images"+ File.separator);
							if (!dir.exists())
								dir.mkdirs();

							/*byte[] bytes = mpf.getBytes();
							BufferedOutputStream stream =
									new BufferedOutputStream(new FileOutputStream(new File(rootPath + File.separator + "news_images"+ File.separator+Calendar.getInstance().getTimeInMillis()+"_simg_"+  mpf.getOriginalFilename())));
							stream.write(bytes);
							stream.close();*/

							News data1 = new News();
							String id = mpf.getName().replaceAll("rsimg", "".trim());
							id = id.replaceAll("rlimg", "".trim());
							data1.setId(Integer.parseInt(id));						
							//check got new image upload or not.
							Iterator iS = paramSelectedSimg.entrySet().iterator();
							Iterator iL = paramSelectedLimg.entrySet().iterator();

							while (iL.hasNext()) {
								Map.Entry mapEntry1 = (Map.Entry) iL.next();						
								String key1 = (String) mapEntry1.getKey();
								String val1 = (String) mapEntry1.getValue();
								System.out.println("iL - key1>"+key1+","+"val1>"+val1);
							}

							while (iS.hasNext()) {
								Map.Entry mapEntry1 = (Map.Entry) iS.next();						
								String key1 = (String) mapEntry1.getKey();
								String val1 = (String) mapEntry1.getValue();
								System.out.println("iS - key1>"+key1+","+"val1>"+val1);
							}

							//if paramSelectedSimg contain the field name rsimg/rlimg, means user is uploaded new images.
							if(paramSelectedSimg.containsKey(mpf.getName())==true){	
								data1.setImageS(mpf.getBytes());
								data1.setImageSFileType( mpf.getContentType());
							}if(paramSelectedLimg.containsKey(mpf.getName())==true){	
								System.out.println("rlimg>>id >>"+id);
								data1.setId(Integer.parseInt(id));
								data1.setImageL(mpf.getBytes());
								data1.setImageLFileType( mpf.getContentType());
							}
							maintenanceService.updateNews(data1);

						} catch (IOException e) {
							e.printStackTrace();
						}
					}//end While
				}//request instanceof MultipartHttpServletRequest


				System.out.println("============END [MultipartHttpServletRequest]============");

				//2. send it back to the client as <img> that calls get method
				//we are using getTimeInMillis to avoid server cached image 
				//  String a ="<img id=\"spreview"+id+"\" src="${pageContext.request.contextPath}/admin/getNewsImage/simg/<c:out value="${item.id}"/>.do" width="140" height="107"/>";
				//map.put("image", "<img src='"+request.getContextPath()+"/admin/get/"+Calendar.getInstance().getTimeInMillis()+"' />");
				//return request.getContextPath()+"/admin/get/"+Calendar.getInstance().getTimeInMillis();

			}//end ServletFileUpload.isMultipartContent()
			map.put("success", StringUtil.showCommonMsg("success", "Successfully update ("+count+") news."));
		}catch(Exception ex){
			reportService.auditLog("News", new AuditTrail(user.getId(),0,"","Update News: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			map.put("error", StringUtil.showCommonMsg("error", "Failed to news."));
			ex.printStackTrace();
		}


		return map;
	}


	@RequestMapping(value = "/admin/addPowerAlert",  method = RequestMethod.GET)
	public ModelAndView doAddPowerAlert()	{
		ModelAndView model = new ModelAndView();

		List<CustServiceLocation> stationList = new ArrayList<CustServiceLocation>();
		List<PowerAlertType> typeList = new  ArrayList<PowerAlertType>();

		try {
			stationList	= maintenanceService.findAllStation();	
			typeList	= maintenanceService.findPowerAlertType();


		} catch (SQLException ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		model.addObject("stationList", stationList);
		model.addObject("typeList", typeList);
		model.setViewName("/maintenance/addPowerAlert");
		return model;
	}

	@RequestMapping(value = "/admin/addPowerAlert",  method = RequestMethod.POST)
	public ModelAndView savePowerAlert(
			@RequestParam(value="selectType", defaultValue = "") String selectType,
			@RequestParam(value="selectStation", defaultValue = "") String selectStation,
			@RequestParam(value="area", defaultValue = "") String area,
			@RequestParam(value="powerTypeName", defaultValue = "") String powerTypeName,
			@RequestParam(value="displayStation", defaultValue = "") String displayStation,
			@RequestParam(value="causes", defaultValue = "") String causes,
			@RequestParam(value="inrestoreDate") String restoreDate,
			@RequestParam(value="pushNotif") String pushNotif,
			@RequestParam(value="inpushDateTime") String pushDateTime,
			@RequestParam(value="instartDateTime") String startDateTime,
			@RequestParam(value="inendDateTime") String endDateTime,
			@RequestParam(value="title", defaultValue = "") String title,
			@RequestParam(value="message", defaultValue = "") String message){

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		List<CustServiceLocation> stationList = new ArrayList<CustServiceLocation>();
		List<PowerAlertType> typeList = new  ArrayList<PowerAlertType>();

		try{
			stationList	= maintenanceService.findAllStation();	
			typeList	= maintenanceService.findPowerAlertType();

			//restoration date : dd/mm/yyyy hh a
			//maintenance start or end date : dd/mm/yyyy hh a
			//push notification date : dd/mm/yyyy hh:mm a

			System.out.println("restoreDate>>"+restoreDate);
			System.out.println("startDateTime>>"+startDateTime);
			System.out.println("endDateTime>>"+endDateTime);
			System.out.println("pushDateTime>>"+pushDateTime);
			System.out.println("powerTypeName>>"+powerTypeName);
			System.out.println("displayStation>>"+displayStation);
			System.out.println("title>>"+title);
			System.out.println("causes>>"+causes);
			System.out.println("message>>"+message);

			/*System.out.println("restoreDate>>"+restoreDate);
			System.out.println("startDateTime>>"+startDateTime);
			System.out.println("endDateTime>>"+endDateTime);
			System.out.println("pushDateTime>>"+pushDateTime);*/

			PowerAlert p0 = new PowerAlert();

			if(powerTypeName.toLowerCase().indexOf(ApplicationConstant.POWER_ALERT_OUTAGE)>-1){//outage
				restoreDate =  DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(restoreDate);
				pushDateTime = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(pushDateTime);

				p0.setRestorationDatetime(StringUtil.convertStringToTimestamp(restoreDate, "dd/MM/yyyy hh:mm:ss a"));
				p0.setMaintenanceStartDatetime(new Timestamp(System.currentTimeMillis()));
				p0.setMaintenanceEndDatetime(new Timestamp(System.currentTimeMillis()));

			}else{//preventive
				//startDateTime = startDateTime.substring(0,13).concat(":00:00")+(startDateTime.substring(startDateTime.length()-3));
				//endDateTime = endDateTime.substring(0,13).concat(":00:00")+(endDateTime.substring(endDateTime.length()-3));
				//pushDateTime = pushDateTime.substring(0,16).concat(":00")+(pushDateTime.substring(pushDateTime.length()-3));

				startDateTime =  DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(startDateTime);
				endDateTime = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(endDateTime);
				pushDateTime = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(pushDateTime);


				//ori restore : 12/10/2015 06pm
				//changed to  : 12/10/2015 06:00:00pm
				p0.setRestorationDatetime(new Timestamp(System.currentTimeMillis()));
				p0.setMaintenanceStartDatetime(StringUtil.convertStringToTimestamp(startDateTime, "dd/MM/yyyy hh:mm:ss a"));
				p0.setMaintenanceEndDatetime(StringUtil.convertStringToTimestamp(endDateTime, "dd/MM/yyyy hh:mm:ss a"));
			}

			p0.setArea(StringUtil.trimToEmpty(area));
			p0.setTitle(StringUtil.trimToEmpty(title));
			p0.setCauses(StringUtil.trimToEmpty(causes));	
			//p0.setPushNotificationStatus(Integer.parseInt(pushNotif));
			//p0.setPushNotificationDatetime(StringUtil.convertStringToTimestamp(pushDateTime, "dd/MM/yyyy hh:mm:ss a"));		
			p0.setPowerAlertTypeId(Integer.parseInt(selectType));
			p0.setCustServiceLocationId(Integer.parseInt(selectStation));

			p0.setCustServLoc(maintenanceService.findCustServiceLocationById(Integer.parseInt(selectStation)));
			p0.setPowerAlertType(maintenanceService.findPowerAlertTypeById(Integer.parseInt(selectType)));
			PowerAlert p1 = maintenanceService.savePowerAlert(p0);

			if(p1!=null && Integer.parseInt(pushNotif)==1){				
				TagNotification tag0 = new TagNotification();

				tag0.setNotificationType(ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY);
				tag0.setPowerAlertType(p0.getPowerAlertType().getName().toUpperCase());
				tag0.setAssociatedId(p1.getId());
				tag0.setText(StringUtil.trimToEmpty(message));	
				if(powerTypeName.toLowerCase().indexOf(ApplicationConstant.POWER_ALERT_OUTAGE)>-1){//outage
					tag0.setTagName(ApplicationConstant.TAG_NAME_POWER_ALERT+"_"+(p0.getCustServLoc().getStation().toUpperCase().replace(" ", "_")));
				}else{
					tag0.setTagName(ApplicationConstant.TAG_NAME_MAINTENANCE+"_"+(p0.getCustServLoc().getStation().toUpperCase().replace(" ", "_")));
				}
				tag0.setStatus(ApplicationConstant.NOTIF_STATUS_UNSENT);
				tag0.setPushDatetime(StringUtil.convertStringToTimestamp(pushDateTime, "dd/MM/yyyy hh:mm:ss a"));				
				tag0 = maintenanceService.savePushNotication(tag0);
			}


			reportService.auditLog("Outage Alert", new AuditTrail(user.getId(),0,"","Add Outage Alert: "+user.getLoginId()+",Task ID:"+p1.getId(),"", "power type:["+powerTypeName+"], selectStation:["+displayStation+"], area:["+StringUtil.trimToEmpty(area)+"], title:["+StringUtil.trimToEmpty(title)+"], causes:["+StringUtil.trimToEmpty(causes)+"], restoreDate:["+restoreDate+"], pushNotif:["+pushNotif+"], pushDateTime:["+pushDateTime+"], message:["+StringUtil.trimToEmpty(message)+"], startDateTime:["+startDateTime+"], endDateTime:["+endDateTime+"]", ""));		
			model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully added new outage alert."));
			log.info("success save outage alert.");


		}catch (Exception ex) {
			reportService.auditLog("Outage Alert", new AuditTrail(user.getId(),0,"","Add Outage Alert: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to add new outage alert."));
			ex.printStackTrace();

		}	
		model.addObject("stationList", stationList);
		model.addObject("typeList", typeList);
		model.setViewName("/maintenance/addPowerAlert");
		return model;
	}

	@RequestMapping(value = "/admin/editPowerAlert",  method = RequestMethod.GET)
	public ModelAndView doEditPowerAlert()	{

		ModelAndView model = new ModelAndView();
		List<PowerAlertType> typeList = new  ArrayList<PowerAlertType>();
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		try {
			typeList	= maintenanceService.findPowerAlertType();

			List<String> regionList = maintenanceService.findAllRegions();	
			model.addObject("regionList", regionList);
		} catch (SQLException ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		model.setViewName("/maintenance/editPowerAlert");
		model.addObject("typeList", typeList);
		return model;
	}

	@RequestMapping(value = "/admin/editPowerAlert",  method = RequestMethod.POST)
	public ModelAndView searchPowerAlert(
			@RequestParam(value="instartDate", defaultValue = "") String startDate,
			@RequestParam(value="inendDate", defaultValue = "") String endDate,
			@RequestParam(value="selectType", defaultValue = "") String powerTypeId,
			@RequestParam(value="powerTypeName", defaultValue = "") String powerTypeName){

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		List<PowerAlertType> typeList = new  ArrayList<PowerAlertType>();
		List<PowerAlert> outageList =  new  ArrayList<PowerAlert>();
		List<MyPowerAlert> myPowerList = new ArrayList<MyPowerAlert>();
		try {

			System.out.println("startDate>>"+startDate);
			System.out.println("endDate>>"+endDate);
			System.out.println("powerTypeId>>"+powerTypeId);
			System.out.println("powerTypeName>>"+powerTypeName);

			DateUtil dateUtil = new DateUtil();			
			Date convertStartDate = new Date();
			Date convertEndDate = new Date();

			convertStartDate  = (StringUtil.trimToEmpty(startDate).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(startDate+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");						
			convertEndDate  = (StringUtil.trimToEmpty(endDate).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(endDate+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						


			typeList	= maintenanceService.findPowerAlertType();			
			//outageList = maintenanceService.findAllPowerAlertByCondition(convertStartDate, convertEndDate, Integer.parseInt(powerTypeId));
			if(powerTypeName.toLowerCase().indexOf(ApplicationConstant.POWER_ALERT_OUTAGE)>-1){
				outageList = maintenanceService.findAllOutageByTypeId(convertStartDate, convertEndDate, Integer.parseInt(powerTypeId));

			}else if(powerTypeName.toLowerCase().indexOf(ApplicationConstant.POWER_ALERT_PREVENTIVE)>-1){
				outageList = maintenanceService.findAllPreventiveByTypeId(convertStartDate, convertEndDate, Integer.parseInt(powerTypeId));

			}

			for(PowerAlert o : outageList){
				MyPowerAlert my = new MyPowerAlert();
				my.setId(o.getId());
				my.setPowerAlertTypeId(o.getPowerAlertTypeId());
				my.setPowerAlertTypeName(o.getPowerAlertType().getName().replaceAll("_", " "));
				my.setStation(o.getCustServLoc().getStation());
				my.setCustServiceLocationId(o.getCustServiceLocationId());
				my.setArea(o.getArea());
				my.setTitle(o.getTitle());
				my.setCauses(o.getCauses());
				my.setRestorationDatetime(o.getRestorationDatetime());
				my.setMaintenanceStartDatetime(o.getMaintenanceStartDatetime());
				my.setMaintenanceEndDatetime(o.getMaintenanceEndDatetime());
				my.setCreatedDatetime(o.getCreatedDatetime());

				List <TagNotification> notifList = maintenanceService.findContentByAssIdAndType(o.getId(), ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY);
				System.out.println("notifList size::"+notifList.size());
				List myTagList = new ArrayList<MyTagNotification>();
				if(notifList!=null && notifList.size()>0){
					for(int i=0;i<notifList.size();i++){
						MyTagNotification innerTag = new MyTagNotification();
						innerTag.setNotificationId(notifList.get(i).getNotificationId());
						innerTag.setAssociatedId(notifList.get(i).getAssociatedId());				
						innerTag.setText(StringUtil.trimToEmpty(notifList.get(i).getText()));
						innerTag.setNotificationType(notifList.get(i).getNotificationType());
						innerTag.setTagName(notifList.get(i).getTagName());
						innerTag.setStatus(notifList.get(i).getStatus());
						innerTag.setPushDatetime(notifList.get(i).getPushDatetime());
						myTagList.add(innerTag);
					}
					my.setMyTagList(myTagList);
					/*my.setNotificationId(notifList.get(0).getNotificationId());
					my.setAssociatedId(notifList.get(0).getAssociatedId());				
					my.setText(StringUtil.trimToEmpty(notifList.get(0).getText()));
					my.setNotificationType(notifList.get(0).getNotificationType());
					my.setTagName(notifList.get(0).getTagName());
					my.setStatus(notifList.get(0).getStatus());
					my.setPushDatetime(notifList.get(0).getPushDatetime());*/

				}else{
					MyTagNotification innerTag = new MyTagNotification();				
					innerTag.setNotificationId(0);
					innerTag.setAssociatedId(null);				
					innerTag.setText(null);
					innerTag.setNotificationType(null);
					innerTag.setTagName(null);
					innerTag.setStatus(ApplicationConstant.NOTIF_STATUS_OFF);
					innerTag.setPushDatetime(null);
					myTagList.add(innerTag);					

					my.setMyTagList(myTagList);

					/*my.setNotificationId(0);
					my.setAssociatedId(null);				
					my.setText(null);
					my.setNotificationType(null);
					my.setTagName(null);
					my.setStatus(ApplicationConstant.NOTIF_STATUS_OFF);
					my.setPushDatetime(null);*/
				}
				myPowerList.add(my);				
			}
			/*for(PowerAlert o : outageList){
				MyPowerAlert my = new MyPowerAlert();
				my.setId(o.getId());
				my.setPowerAlertTypeId(o.getPowerAlertTypeId());
				my.setPowerAlertTypeName(o.getPowerAlertType().getName().replaceAll("_", " "));
				my.setStation(o.getCustServLoc().getStation());
				my.setCustServiceLocationId(o.getCustServiceLocationId());
				my.setArea(o.getArea());
				my.setTitle(o.getTitle());
				my.setCauses(o.getCauses());
				my.setRestorationDatetime(o.getRestorationDatetime());
				my.setMaintenanceStartDatetime(o.getMaintenanceStartDatetime());
				my.setMaintenanceEndDatetime(o.getMaintenanceEndDatetime());
				my.setCreatedDatetime(o.getCreatedDatetime());

				if(o.getTagNotifications()!=null && o.getTagNotifications().size()>0){
					int index = o.getTagNotifications().size()-1;
					my.setNotificationId(o.getTagNotifications().get(index).getNotificationId());
					my.setAssociatedId(o.getTagNotifications().get(index).getAssociatedId());				
					my.setText(o.getTagNotifications().get(index).getText());
					my.setNotificationType(o.getTagNotifications().get(index).getNotificationType());
					my.setTagName(o.getTagNotifications().get(index).getTagName());
					my.setStatus(o.getTagNotifications().get(index).getStatus());
					my.setPushDatetime(o.getTagNotifications().get(index).getPushDatetime());
				}else{
					my.setNotificationId(0);
					my.setAssociatedId(null);				
					my.setText(null);
					my.setNotificationType(null);
					my.setTagName(null);
					my.setStatus(ApplicationConstant.NOTIF_STATUS_OFF);
					my.setPushDatetime(new Timestamp(System.currentTimeMillis()));
				}
				myPowerList.add(my);
			}	*/
			reportService.auditLog("Outage Alert", new AuditTrail(user.getId(),0,"","Search Outage Alert: "+user.getLoginId(),"", "startDate:["+startDate+"], endDate:["+endDate+"], type:["+powerTypeName+"]", ""));		

			System.out.println("outageList>>"+outageList.size());


		} catch (SQLException ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		model.addObject("typeList", typeList);
		model.addObject("outageList", myPowerList);
		model.addObject("startDate", startDate);
		model.addObject("endDate", endDate);
		model.addObject("powerTypeName", powerTypeName);
		model.setViewName("/maintenance/editPowerAlert");

		for(MyPowerAlert a : myPowerList){
			System.out.println("id::"+a.getId()+", outage alert type name>>"+a.getPowerAlertTypeName());
			for(MyTagNotification b : a.getMyTagList()){
				System.out.println("message status >>"+a.getStatus()+",outage alert push messages >>>"+b.getText());
			}
		}
		return model;
	}

	@RequestMapping(value = "/admin/updatePowerAlert",  method = RequestMethod.POST )
	public @ResponseBody Map<String,String> updatePowerAlert(@RequestBody List<Map<String, String>> client)
			throws IOException {

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		int count = 0;
		String myStatus = "";
		String error  = "";
		Map<String,String> map = new HashMap<String,String>();
		ModelAndView model = new ModelAndView();

		try {
			PowerAlert p0 = new PowerAlert();
			for (Map<String, String> formInput : client) {
				if(formInput!=null && !formInput.isEmpty()){
					String id	= formInput.get("id") != null ? formInput.get("id") : "".trim();
					String powerTypeName	= formInput.get("powerTypeName") != null ? formInput.get("powerTypeName") : "".trim();
					String station	= formInput.get("station") != null ? formInput.get("station") : "".trim();
					String area	= formInput.get("area") != null ? formInput.get("area") : "".trim();
					String causes	= formInput.get("causes") != null ? formInput.get("causes") : "".trim();
					String restoreDate 	= formInput.get("inrestoreDate") != null ? formInput.get("inrestoreDate") : "".trim();
					String startDateTime 	= formInput.get("instartDateTime") != null ? formInput.get("instartDateTime") : "".trim();
					String endDateTime 	= formInput.get("inendDateTime") != null ? formInput.get("inendDateTime") : "".trim();
					String pushNotif	= formInput.get("pushNotif") != null ? formInput.get("pushNotif") : "".trim();
					String pushDateTime	= formInput.get("inpushDateTime") != null ? formInput.get("inpushDateTime") : "".trim();
					String text	= formInput.get("text") != null ? formInput.get("text") : "".trim();
					String notificationId	= formInput.get("notificationId") != null ? formInput.get("notificationId") : "".trim();

					//restoration date : dd/mm/aye hh a
					//maintenance start or end date : dd/mm/yyyy hh a
					//push notification date : dd/mm/yyyy hh:mm a
					log.info("power alertid>>"+id);
					log.info("station>>"+station);
					log.info("area>>"+area);
					log.info("causes>>"+causes);
					log.info("push notification id>>"+notificationId);
					log.info("text>>"+text);
					log.info("pushNotif>>"+pushNotif);
					log.info("restoreDate>>"+restoreDate);
					log.info("startDateTime>>"+startDateTime);
					log.info("endDateTime>>"+endDateTime);
					log.info("pushDateTime>>"+pushDateTime);

					PowerAlert oldPA = maintenanceService.findAllPowerAlertByCondition2ById(Integer.parseInt(id));
					log.info("oldPA>>"+oldPA.getTitle());
					String oldCauses= oldPA.getCauses();
					Date oldRestoreTime= oldPA.getRestorationDatetime();
					String oldPNStatus = "";
					Date oldPNDateTime =  new Date();
					String oldPNText = "";
					Date oldMaintenanceStartDatetime = oldPA.getMaintenanceStartDatetime();
					Date oldMaintenanceEndDatetime = oldPA.getMaintenanceEndDatetime();
					powerTypeName = oldPA.getPowerAlertType().getName();
					log.info("powerTypeName>>"+powerTypeName);


					boolean hasTagNotifyRecord = false;
					if(oldPA!=null){
						log.info("old id >>"+oldPA.getId());
						log.info("notification type ::"+ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY);
						List <TagNotification> notifList = maintenanceService.findContentByAssIdAndType(oldPA.getId(), ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY);
						log.info("notifList size::"+notifList.size());	
						if(notifList!=null && notifList.size()>0){
							TagNotification oldTag = maintenanceService.findPushNotificationById(Integer.parseInt(notificationId));
							//log.info("old push notification data::"+oldTag.toString());
							hasTagNotifyRecord = true;
							oldPNStatus = oldTag.getStatus();
							oldPNDateTime = oldTag.getPushDatetime();
							oldPNText = oldTag.getText();
						}else{//dont have tag notification
							log.info("no push notification");
							hasTagNotifyRecord = false;
							oldPNStatus = "OFF";
						}
					}

					//log.info("powerTypeName.toLowerCase().indexOf(ApplicationConstant.POWER_ALERT_OUTAGE)>-1>>"+(powerTypeName.toLowerCase().indexOf(ApplicationConstant.POWER_ALERT_OUTAGE)>-1));
					if(powerTypeName.toLowerCase().indexOf(ApplicationConstant.POWER_ALERT_OUTAGE)>-1){
						log.info("outage");
						restoreDate = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(restoreDate);
						pushDateTime = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(pushDateTime);

						p0.setId(Integer.parseInt(id));
						p0.setCauses(StringUtil.trimToEmpty(causes));					
						p0.setRestorationDatetime(StringUtil.convertStringToTimestamp(restoreDate, "dd/MM/yyyy hh:mm:ss a"));
						p0.setMaintenanceStartDatetime(oldPA.getMaintenanceStartDatetime());
						p0.setMaintenanceEndDatetime(oldPA.getMaintenanceEndDatetime());

					}else{
						log.info("preventive");
						startDateTime = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(startDateTime);
						endDateTime = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(endDateTime);
						pushDateTime = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(pushDateTime);

						p0.setId(Integer.parseInt(id));
						p0.setCauses(StringUtil.trimToEmpty(causes));					
						p0.setRestorationDatetime(oldPA.getRestorationDatetime());
						p0.setMaintenanceStartDatetime(StringUtil.convertStringToTimestamp(startDateTime, "dd/MM/yyyy hh:mm:ss a"));
						p0.setMaintenanceEndDatetime(StringUtil.convertStringToTimestamp(endDateTime, "dd/MM/yyyy hh:mm:ss a"));
					}

					//update outage alert
					log.info("cause:"+p0.getCauses());
					p0 = maintenanceService.updatePowerAlert(p0);

					TagNotification tag0 = new TagNotification();
					tag0.setNotificationType(ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY);
					tag0.setAssociatedId(Integer.parseInt(id));
					tag0.setText(StringUtil.trimToEmpty(text));	
					tag0.setPowerAlertType(powerTypeName.toUpperCase());

					if(powerTypeName.toLowerCase().indexOf(ApplicationConstant.POWER_ALERT_OUTAGE)>-1){
						tag0.setTagName(ApplicationConstant.TAG_NAME_POWER_ALERT+"_"+(station.toUpperCase().replace(" ", "_")));
					}else{
						tag0.setTagName(ApplicationConstant.TAG_NAME_MAINTENANCE+"_"+(station.toUpperCase().replace(" ", "_")));
					}

					pushNotif = pushNotif.equalsIgnoreCase("1") ? ApplicationConstant.NOTIF_STATUS_UNSENT :  ApplicationConstant.NOTIF_STATUS_OFF;
					tag0.setStatus(pushNotif);
					tag0.setPushDatetime(StringUtil.convertStringToTimestamp(pushDateTime, "dd/MM/yyyy hh:mm:ss a"));				

					if(hasTagNotifyRecord){
						log.info("update exisiting notification");	
						log.info("notificationId>>"+notificationId);
						tag0.setNotificationId(Integer.parseInt(notificationId));						
						maintenanceService.updatePushNotification(tag0);
					}else if(hasTagNotifyRecord==false && pushNotif.equalsIgnoreCase(ApplicationConstant.NOTIF_STATUS_UNSENT)){
						log.info("insert new notification");
						tag0 = maintenanceService.savePushNotication(tag0);						
					}


					if(powerTypeName.toLowerCase().indexOf(ApplicationConstant.POWER_ALERT_OUTAGE)>-1){
						log.info("Update Outage Alert: "+user.getLoginId()+", task id: "+p0.getId()+"causes:["+oldCauses+"], estimated restoration time:["+oldRestoreTime+"], push notification status:["+oldPNStatus+"], push notification datetime:["+oldPNDateTime+"], push notification message:["+oldPNText.trim()+"]"+"new ::causes:["+causes.trim()+"], estimated restoration time:["+p0.getRestorationDatetime()+"], push notification status:["+pushNotif+"], push notification datetime:["+pushDateTime+"], push notification message:["+text.trim()+"]");
						reportService.auditLog("Outage Alert", new AuditTrail(user.getId(),0,"","Update Outage Alert: "+user.getLoginId()+", task id: "+p0.getId(),"causes:["+oldCauses+"], estimated restoration time:["+oldRestoreTime+"], push notification status:["+oldPNStatus+"], push notification datetime:["+oldPNDateTime+"], push notification message:["+oldPNText.trim()+"]","causes:["+causes.trim()+"], estimated restoration time:["+p0.getRestorationDatetime()+"], push notification status:["+pushNotif+"], push notification datetime:["+pushDateTime+"], push notification message:["+text.trim()+"]",""));
					}else		{
						reportService.auditLog("Outage Alert", new AuditTrail(user.getId(),0,"","Update Outage Alert: "+user.getLoginId()+", task id: "+p0.getId(),"causes:["+oldCauses+"], start date time of maintenance :["+oldMaintenanceStartDatetime+"], end date time of maintenance :["+oldMaintenanceEndDatetime+"], push notification status:["+oldPNStatus+"], push notification datetime:["+oldPNDateTime+"], push notification message:["+oldPNText.trim()+"]","causes:["+causes.trim()+"], start date time of maintenance :["+startDateTime+"], end date time of maintenance :["+endDateTime+"], push notification status:["+pushNotif+"], push notification datetime:["+pushDateTime+"], push notification message:["+text.trim()+"]",""));
						log.info("Update Outage Alert: "+user.getLoginId()+", task id: "+p0.getId()+",old::causes:["+oldCauses+"], start date time of maintenance :["+oldMaintenanceStartDatetime+"], end date time of maintenance :["+oldMaintenanceEndDatetime+"], push notification status:["+oldPNStatus+"], push notification datetime:["+oldPNDateTime+"], push notification message:["+oldPNText.trim()+"]"+", new ::causes:["+causes.trim()+"], start date time of maintenance :["+startDateTime+"], end date time of maintenance :["+endDateTime+"], push notification status:["+pushNotif+"], push notification datetime:["+pushDateTime+"], push notification message:["+text.trim()+"]");
					}
					count++;

				}


			}
			map.put("success", StringUtil.showCommonMsg("success", "Successfully update ("+count+") outage alert."));
		}catch(Exception ex){
			reportService.auditLog("Outage Alert", new AuditTrail(user.getId(),0,"","Update Outage Alert: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			map.put("error", StringUtil.showCommonMsg("error", "Failed to update outage alert."));
			ex.printStackTrace();

		}
		return map;
	}

	@RequestMapping(value = "/admin/addPushNotification",  method = RequestMethod.GET)
	public ModelAndView doAddPushNotification()	{
		ModelAndView model = new ModelAndView();

		model.setViewName("/maintenance/addPushNotification");
		return model;
	}

	@RequestMapping(value = "/admin/addPushNotification",  method = RequestMethod.POST)
	public ModelAndView savePushNotification(
			@RequestParam(value="inpushDateTime", defaultValue = "") String pushDateTime,
			@RequestParam(value="selectType", defaultValue = "") String selectType,
			@RequestParam(value="selectTitle", defaultValue = "0") String selectTitle,
			@RequestParam(value="message", defaultValue = "") String message,
			@RequestParam(value="displayType", defaultValue = "") String displayType,
			@RequestParam(value="displayTitle", defaultValue = "") String displayTitle
			){
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		int count = 0;

		ModelAndView model = new ModelAndView();

		log.info("inpushDateTime>"+pushDateTime);
		log.info("selectType>"+selectType);
		log.info("selectTitle>"+selectTitle);
		log.info("message>"+StringEscapeUtils.unescapeHtml(message));
		log.info("displayType>"+displayType);
		log.info("displayTitle>"+displayTitle);


		try {

			String auditPushDateTime = pushDateTime;
			pushDateTime = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(pushDateTime);

			if(!(pushDateTime.isEmpty() && selectType.isEmpty() && message.isEmpty())){

				TagNotification data = new TagNotification();
				data.setNotificationType(StringUtil.trimToEmpty(selectType));
				data.setPushDatetime(StringUtil.convertStringToTimestamp(pushDateTime, "dd/MM/yyyy hh:mm:ss a"));
				data.setText(StringUtil.trimToEmpty(message));
				data.setStatus(ApplicationConstant.NOTIF_STATUS_UNSENT);

				if(selectTitle.equalsIgnoreCase("0") && !(selectType.toUpperCase().equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_PLAIN_KEY))){//other than FREE TEXT
					model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Select Title is required."));
					model.setViewName("/maintenance/addPushNotification");
					return model;
				}else if(selectType.toUpperCase().equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_PLAIN_KEY)){//Free Text
					data.setTagName(ApplicationConstant.TAG_NAME_GENERAL);					

				}else if(selectType.toUpperCase().equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_PROMO_KEY)){//news
					News n0 = maintenanceService.findNewsById(Integer.parseInt(selectTitle));	
					data.setAssociatedId(n0.getId());
					data.setTagName(ApplicationConstant.TAG_NAME_GENERAL);

				}else{
					PowerAlert p0 = maintenanceService.findPowerAlertById(Integer.parseInt(selectTitle));
					data.setAssociatedId(p0.getId());
					data.setPowerAlertType(p0.getPowerAlertType().getName().toUpperCase());
					if(p0.getPowerAlertType().getName().toLowerCase().indexOf(ApplicationConstant.POWER_ALERT_OUTAGE)>-1){//outage
						data.setTagName(ApplicationConstant.TAG_NAME_POWER_ALERT+"_"+(p0.getCustServLoc().getStation()).replace(" ", "_").toUpperCase());
					}else{						
						data.setTagName(ApplicationConstant.TAG_NAME_MAINTENANCE+"_"+(p0.getCustServLoc().getStation()).replace(" ", "_").toUpperCase());
					}
				}					
				System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");
				System.out.println("text>"+data.getText());
				System.out.println("NOTIFICATION_TYPE>"+data.getNotificationType());
				System.out.println("ASSOCIATED_ID>"+data.getAssociatedId());
				System.out.println("POWER_ALERT_TYPE>"+data.getPowerAlertType());
				System.out.println("TAG_NAME>"+data.getTagName());
				System.out.println("STATUS>"+data.getStatus());
				System.out.println("ASSOCIATED_ID>"+data.getAssociatedId());

				System.out.println(data.toString());

				System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++");

				maintenanceService.savePushNotication(data);
				model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully added push notification."));
				reportService.auditLog("Push Notification", new AuditTrail(user.getId(),0,"","Add Push Notification: "+user.getLoginId()+", Task ID: "+data.getNotificationId(),"", "pushDateTime:["+auditPushDateTime+"], type:["+displayType+", title:["+displayTitle+"], message:["+message+"]", ""));		

			}else{
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Mandatory inputs are required."));
			}
		} catch (SQLException ex) {			
			reportService.auditLog("Push Notification", new AuditTrail(user.getId(),0,"","Add Push Notification: "+user.getLoginId(),"","Fail to process","Fail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to add new push notification."));
			ex.printStackTrace();
		}
		model.setViewName("/maintenance/addPushNotification");
		return model;
	}

	@RequestMapping(value = "/admin/editPushNotification",  method = RequestMethod.GET)
	public ModelAndView doEditPushNotification()	{
		ModelAndView model = new ModelAndView();
		model.setViewName("/maintenance/editPushNotification");
		return model;
	}

	@RequestMapping(value = "/admin/editPushNotification",  method = RequestMethod.POST)
	public ModelAndView searchPushNotification(
			@RequestParam(value="instartDate", defaultValue = "") String startDate,
			@RequestParam(value="inendDate", defaultValue = "") String endDate,
			@RequestParam(value="selectType", defaultValue = "") String selectType,
			@RequestParam(value="selectTitle", defaultValue = "0") String selectTitle,
			@RequestParam(value="displayType", defaultValue = "") String displayType,
			@RequestParam(value="displayTitle", defaultValue = "") String displayTitle,
			@RequestParam(value="message", defaultValue = "") String message)	{

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();	

		ModelAndView model = new ModelAndView();
		List<TagNotification> resultList = new ArrayList<TagNotification>();
		List<MyTagNotification> myTagList = new ArrayList <MyTagNotification>();
		List<MyDropDown> dropDown = new ArrayList<MyDropDown>() ;
		try {

			displayTitle = (!displayTitle.equalsIgnoreCase("Please Select")) ? displayTitle : "";

			System.out.println("startDate>>"+startDate);
			System.out.println("endDate>>"+endDate);
			System.out.println("selectType>>"+selectType);
			System.out.println("selectTitle>>"+selectTitle);
			System.out.println("displayType>>"+displayType);
			System.out.println("displayTitle>>"+displayTitle);
			System.out.println("message>>"+message);

			DateUtil dateUtil = new DateUtil();			
			Date convertStartDate = new Date();
			Date convertEndDate = new Date();

			convertStartDate  = (StringUtil.trimToEmpty(startDate).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 00:00:00", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(startDate+ " 00:00:00", "dd/MM/yyyy hh:mm:ss");						
			convertEndDate  = (StringUtil.trimToEmpty(endDate).equals("")) ? dateUtil.convertToDate(dateUtil.getCurrentDate()+ " 23:59:59", "dd/MM/yyyy hh:mm:ss") : dateUtil.convertToDate(endDate+ " 23:59:59", "dd/MM/yyyy hh:mm:ss");						

			resultList = maintenanceService.findAllByCondition1(convertStartDate, convertEndDate, selectType, Integer.parseInt(selectTitle), message);
			System.out.println("resultList>>"+resultList.size());
			List<Integer> titleIdList = new ArrayList<Integer>();
			for(TagNotification tag : resultList)
			{
				MyTagNotification myTag = new MyTagNotification();
				myTag.setNotificationId(tag.getNotificationId());
				myTag.setText(StringUtil.trimToEmpty(tag.getText()));
				myTag.setNotificationType(tag.getNotificationType());
				myTag.setAssociatedId(tag.getAssociatedId());
				myTag.setPowerAlertType(tag.getPowerAlertType());
				myTag.setTagName(tag.getTagName());
				myTag.setStatus(tag.getStatus());
				myTag.setPushDatetime(tag.getPushDatetime());				
				myTagList.add(myTag);
			}

			if(selectType.equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_PLAIN_KEY)){
				System.out.println("free text");
			}else if (selectType.equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_PROMO_KEY)){
				System.out.println("promo/news");
				titleIdList = maintenanceService.findSubTitleIdBtwDate(convertStartDate, convertEndDate, ApplicationConstant.NOTIF_TYPE_PROMO_KEY, ApplicationConstant.TAG_NAME_GENERAL);

				List<News> newslist = maintenanceService.findNewsIds(titleIdList);
				//System.out.println(">>>> my newslist:"+newslist.size());
				for(News n0 : newslist){
					MyDropDown dd = new MyDropDown();
					dd.setText(n0.getTitle());
					dd.setValue(String.valueOf(n0.getId()));

					dropDown.add(dd);	

				}				
				/*for(MyDropDown d :dropDown)
				{
					System.out.println("value="+d.getValue()+",text="+d.getText());
				}*/				
			}
			else if (selectType.equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY)){
				System.out.println("power alert");				
				System.out.println("tag like :"+ ApplicationConstant.TAG_NAME_POWER_ALERT);					
				titleIdList = maintenanceService.findSubTitleIdBtwDatePower(convertStartDate, convertEndDate, ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY, ApplicationConstant.TAG_NAME_POWER_ALERT, ApplicationConstant.POWER_ALERT_OUTAGE);

				System.out.println(">>>>p-outage:"+titleIdList.size());
				if(titleIdList!=null && titleIdList.size()>0){
					List<PowerAlert> outage = maintenanceService.findAllOutagePowerAlertByIds(titleIdList);
					System.out.println(">>>>outagelist:"+outage.size());
					for(PowerAlert p : outage){
						MyDropDown dd = new MyDropDown();
						dd.setText(p.getTitle());
						dd.setValue(String.valueOf(p.getId()));
						dropDown.add(dd);
					}
				}
				System.out.println("tag like :"+ ApplicationConstant.TAG_NAME_MAINTENANCE);		
				titleIdList = maintenanceService.findSubTitleIdBtwDatePower(convertStartDate, convertEndDate, ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY, ApplicationConstant.TAG_NAME_MAINTENANCE, ApplicationConstant.POWER_ALERT_PREVENTIVE);
				if(titleIdList!=null && titleIdList.size()>0){
					List<PowerAlert> preventive = maintenanceService.findAllPreventivePowerAlertByIds(titleIdList);
					System.out.println(">>>>p-preventive:"+preventive.size());

					for(PowerAlert p : preventive){
						MyDropDown dd = new MyDropDown();
						dd.setText(p.getTitle());
						dd.setValue(String.valueOf(p.getId()));
						dropDown.add(dd);
					}
				}

			}			
			dropDown.stream().sorted((object1, object2) -> object1.getText().compareTo(object2.getText()));

			reportService.auditLog("Push Notification", new AuditTrail(user.getId(),0,"","Search Push Notification: "+user.getLoginId(),"", "startDate:["+startDate+"], endDate:["+endDate+"], selectType:["+displayType+"], title:["+displayTitle+"], message:["+message+"]", ""));		
			dropDown.stream() .sorted((object1, object2) -> object1.getText().compareTo(object2.getText()));

		} catch (SQLException ex) {	
			reportService.auditLog("Push Notification", new AuditTrail(user.getId(),0,"","Update Push Notification: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));					
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		model.addObject("dropdown", dropDown);
		model.addObject("resultList", myTagList);
		model.addObject("startDate", startDate);
		model.addObject("endDate", endDate);
		model.addObject("displayType", displayType);
		model.addObject("displayTitle", displayTitle);
		model.addObject("message", message);
		model.setViewName("/maintenance/editPushNotification");
		return model;
	}

	@RequestMapping(value = "/admin/updatePushNotification",  method = RequestMethod.POST )
	public @ResponseBody Map<String,String> updatePushNotification(@RequestBody List<Map<String, String>> client)
			throws IOException {

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		int count = 0;
		String myStatus = "";
		String error  = "";
		Map<String,String> map = new HashMap<String,String>();
		ModelAndView model = new ModelAndView();

		try {

			for (Map<String, String> formInput : client) {
				if(formInput!=null && !formInput.isEmpty()){

					String id	= formInput.get("id") != null ? formInput.get("id") : "".trim();

					String listType	= formInput.get("listType") != null ? formInput.get("listType") : "".trim();
					String listTitle	= formInput.get("listTitle") != null ? formInput.get("listTitle") : null;
					String displayListTitle	= formInput.get("displayListTitle") != null ? formInput.get("displayListTitle") : "".trim();

					String message	= formInput.get("message") != null ? formInput.get("message") : "".trim();
					String pushDateTime	= formInput.get("inpushDateTime") != null ? formInput.get("inpushDateTime") : "".trim();

					//restoration date : dd/mm/yyyy hh a
					//maintenance start or end date : dd/mm/yyyy hh a
					//push notification date : dd/mm/yyyy hh:mm a
					System.out.println("id>>"+id);					
					System.out.println("listType>>"+listType);
					System.out.println("listTitle>>"+listTitle);
					System.out.println("displayListTitle>>"+displayListTitle);

					System.out.println("message>>"+message);
					System.out.println("pushDateTime>>"+pushDateTime);

					TagNotification oldTag = maintenanceService.findPushNotificationById(Integer.parseInt(id));
					String oldMsg= oldTag.getText();
					String oldNotifType = oldTag.getNotificationType();
					Integer oldAssociatedId = oldTag.getAssociatedId();
					String oldPowerAlertType = oldTag.getPowerAlertType();
					String oldTagName = oldTag.getTagName();
					Date oldPushNotificationDateTime = oldTag.getPushDatetime();

					pushDateTime = DateUtil.convertDdMmYyyyHhMmAToDdMmYyyyHhMmSs(pushDateTime);
					System.out.println(">>>>>>>>"+StringUtil.convertStringToTimestamp(pushDateTime, "dd/MM/yyyy hh:mm:ss a"));

					TagNotification p0 = new TagNotification();
					p0.setNotificationId(Integer.parseInt(id));
					p0.setText(StringUtil.trimToEmpty(message));
					p0.setNotificationType(StringUtil.trimToEmpty(listType.toUpperCase()));					
					p0.setPushDatetime(StringUtil.convertStringToTimestamp(pushDateTime, "dd/MM/yyyy hh:mm:ss a"));	
					p0.setStatus(ApplicationConstant.NOTIF_STATUS_UNSENT);

					if(listType.equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_PLAIN_KEY)){
						System.out.println("plain");
						p0.setAssociatedId(null);
						p0.setTagName(ApplicationConstant.TAG_NAME_GENERAL);

					}else if(listType.equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_PROMO_KEY)){
						System.out.println("promo/news");
						p0.setAssociatedId(Integer.parseInt(listTitle));
						p0.setTagName(ApplicationConstant.TAG_NAME_GENERAL);


					}else if(listType.equalsIgnoreCase(ApplicationConstant.NOTIF_TYPE_POWER_ALERT_KEY)){
						System.out.println("power alert");
						PowerAlert power = maintenanceService.findPowerAlertById(Integer.parseInt(listTitle));						
						p0.setAssociatedId(Integer.parseInt(listTitle));
						p0.setPowerAlertType(power.getPowerAlertType().getName().toUpperCase());
						if(power.getPowerAlertType().getName().toLowerCase().indexOf(ApplicationConstant.POWER_ALERT_OUTAGE)>-1){//outage
							p0.setTagName(ApplicationConstant.TAG_NAME_POWER_ALERT+"_"+(power.getCustServLoc().getStation()).replace(" ", "_").toUpperCase());
						}else{						
							p0.setTagName(ApplicationConstant.TAG_NAME_MAINTENANCE+"_"+(power.getCustServLoc().getStation()).replace(" ", "_").toUpperCase());
						}
					}


					//update outage alert
					maintenanceService.updatePushNotification(p0);

					reportService.auditLog("Push Notification", new AuditTrail(user.getId(),0,"","Update Push Notification: "+user.getLoginId()+", task id: "+p0.getNotificationId(),"message:["+oldMsg+"], type:["+oldNotifType+"], associated id:["+oldAssociatedId+"], tag name:["+oldTagName+"], push notification datetime :["+oldPushNotificationDateTime+"]","message:["+message+"], type:["+p0.getNotificationType()+"], associated id:["+p0.getAssociatedId()+"], tag name:["+p0.getTagName()+"], push notification datetime :["+pushDateTime+"]",""));
					count++;
				}

			}
			map.put("success", StringUtil.showCommonMsg("success", "Successfully update ("+count+") push notification."));
		}catch(Exception ex){
			reportService.auditLog("Push Notification", new AuditTrail(user.getId(),0,"","Update Push Notification: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			map.put("error", StringUtil.showCommonMsg("error", "Failed to update push notification."));
			ex.printStackTrace();
		}
		return map;
	}

	@RequestMapping(value = "/admin/addFAQ",  method = RequestMethod.GET)
	public ModelAndView doAddFAQ()	{
		ModelAndView model = new ModelAndView();
		List<FaqCategory> typeList = new ArrayList<FaqCategory>();
		try {
			typeList = maintenanceService.findAllFaqCategory();
		} catch (SQLException e) {			
			e.printStackTrace();
		}

		model.addObject("typeList",typeList);		
		model.setViewName("/maintenance/addFAQ");
		return model;
	}

	@RequestMapping(value = "/admin/addFAQ",  method = RequestMethod.POST)
	public ModelAndView saveFAQ(
			@RequestParam(value="selectType", defaultValue = "0") String selectType,
			@RequestParam(value="displayType", defaultValue = "") String displayType,
			@RequestParam(value="question", defaultValue = "") String question,
			@RequestParam(value="answer", defaultValue = "") String answer,
			@RequestParam(value="soalan", defaultValue = "") String soalan,
			@RequestParam(value="jawapan", defaultValue = "") String jawapan,
			@RequestParam(value="wenTi", defaultValue = "") String wenTi,
			@RequestParam(value="daAn", defaultValue = "") String daAn)	{
		ModelAndView model = new ModelAndView();
		List<FaqCategory> typeList = new ArrayList<FaqCategory>();

		System.out.println("selectType>>"+selectType);
		System.out.println("displayType>>"+displayType);
		/*System.out.println("question>>"+question);System.out.println("question length>>"+question.length());
		System.out.println("answer>>"+answer);System.out.println("answer length>>"+answer.length());
		System.out.println("soalan>>"+soalan);System.out.println("soalan length>>"+soalan.length());
		System.out.println("jawapan>>"+jawapan);System.out.println("jawapan length>>"+jawapan.length());
		System.out.println("wenTi>>"+wenTi);System.out.println("wenTi length>>"+wenTi.length());
		System.out.println("daAn>>"+daAn);System.out.println("daAn length>>"+daAn.length());
		 */
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		try{
			typeList = maintenanceService.findAllFaqCategory();

			if(!(question.isEmpty() && answer.isEmpty() && soalan.isEmpty() 
					&& jawapan.isEmpty() && wenTi.isEmpty() && daAn.isEmpty())){
				Faq f0 = new Faq();
				f0.setFaqCategoryId(Integer.parseInt(selectType));
				f0.setQuestionEn(question.trim());
				f0.setAnswerEn(answer.trim());
				f0.setQuestionBm(soalan.trim());
				f0.setAnswerBm(jawapan.trim());
				f0.setQuestionCn(wenTi.trim());
				f0.setAnswerCn(daAn.trim());
				f0.setFaqCategory(maintenanceService.findFaqCategoryById(Integer.parseInt(selectType)));
				f0.setStatus("ACTIVE");

				long result = maintenanceService.findNextSeq(Integer.parseInt(selectType));
				System.out.println("result ::"+result);
				int nextCount = (int) (long) result;
				System.out.println("sequence to be added ::"+nextCount);
				f0.setFaqSequence(nextCount);

				maintenanceService.saveFAQ(f0);
				System.out.println("success save");
				reportService.auditLog("FAQ", new AuditTrail(user.getId(),0,"","Add FAQ: "+user.getLoginId()+",Task ID: "+f0.getId(),"", "category:["+displayType+"], question:["+question+"], answer:["+answer+"]", ""));		
				model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully added new FAQ."));
				log.info("saveFAQ");

			}else{
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Mandatory inputs are required."));

			}
		}catch (Exception ex) {
			reportService.auditLog("FAQ", new AuditTrail(user.getId(),0,"","Add FAQ: "+user.getLoginId(),"","Fail to process", "question:["+question+"], answer:["+answer+"]\nFail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to add new FAQ."));
			ex.printStackTrace();
		}	

		model.addObject("typeList",typeList);		
		model.setViewName("/maintenance/addFAQ");
		return model;
	}

	@RequestMapping(value = "/admin/editFAQ",  method = RequestMethod.GET)
	public ModelAndView doEditFAQ()	{

		ModelAndView model = new ModelAndView();
		List<FaqCategory> typeList = new ArrayList<FaqCategory>();

		try {
			typeList = maintenanceService.findAllFaqCategory();

		} catch (SQLException ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		model.addObject("typeList",typeList);		
		model.setViewName("/maintenance/editFAQ");
		return model;
	}

	@RequestMapping(value = "/admin/getFaqQuestion",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody List<Faq> getFaqQuestion(@RequestBody Faq f0)
			throws IOException {
		List<Faq> questionList = new ArrayList<Faq>() ;	
		try {
			System.out.println("category id::"+f0.getFaqCategoryId());
			System.out.println("status ::"+f0.getStatus());
			questionList = maintenanceService.findWithOrderByQuestEn(f0.getFaqCategoryId(),f0.getStatus());
			System.out.println("questionList>"+questionList.size());
		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return questionList;
	}

	@RequestMapping(value = "/admin/editFAQ",  method = RequestMethod.POST)
	public ModelAndView searchFAQ(
			@RequestParam(value="displayType" , defaultValue="") String displayType,
			@RequestParam(value="selectQuestion" , defaultValue="") String id,
			@RequestParam(value="displayStatus" , defaultValue="") String displayStatus)	{

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		List<Faq> faqList = new ArrayList<Faq>();
		List<FaqCategory> typeList = new ArrayList<FaqCategory>();

		try {
			typeList = maintenanceService.findAllFaqCategory();

			if(!StringUtil.trimToEmpty(id).isEmpty()){
				faqList = maintenanceService.findAllFaqById(Integer.parseInt(id));
				reportService.auditLog("FAQ", new AuditTrail(user.getId(),0,"","Search FAQ: "+user.getLoginId(), "","category:["+displayType+"], question:["+faqList.get(0).getQuestionEn()+"]", ""));		
				model.addObject("question", faqList.get(0).getQuestionEn());
			}
			else{
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Select Question is required."));
			}


		} catch (SQLException ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		System.out.println("displayStatus>>>>>>"+displayStatus);
		model.addObject("displayType", WordUtils.capitalize(displayType));
		model.addObject("displayStatus", WordUtils.capitalize(displayStatus));
		model.addObject("typeList",typeList);		
		model.addObject("faqList", faqList);
		model.setViewName("/maintenance/editFAQ");
		return model;
	}

	@RequestMapping(value = "/admin/updateFAQ",  method = RequestMethod.POST )
	public @ResponseBody Map<String,String> updateFAQ(@RequestBody List<Map<String, String>> client)
			throws IOException {

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		String myStatus = "";
		String error  = "";
		Map<String,String> map = new HashMap<String,String>();
		try {


			Faq f0 = new Faq();
			for (Map<String, String> formInput : client) {
				/*	System.out.println("id>>"+formInput.get("id"));
				System.out.println("question>>"+formInput.get("question"));
				System.out.println("answer>>"+formInput.get("answer"));
				System.out.println("soalan>>"+formInput.get("soalan"));
				System.out.println("jawapan>>"+formInput.get("jawapan"));
				System.out.println("wenTi>>"+formInput.get("wenTi"));
				System.out.println("daAn>>"+formInput.get("daAn"));
				 */


				if(formInput!=null && !formInput.isEmpty()){


					String id	= formInput.get("id") != null ? formInput.get("id") : "".trim();
					String question	= formInput.get("question") != null ? formInput.get("question") : "".trim();
					String answer 	= formInput.get("answer") != null ? formInput.get("answer") : "".trim();
					String soalan	= formInput.get("soalan") != null ? formInput.get("soalan") : "".trim();
					String jawapan 	= formInput.get("jawapan") != null ? formInput.get("jawapan") : "".trim();
					String wenTi	= formInput.get("wenTi") != null ? formInput.get("wenTi") : "".trim();
					String daAn 	= formInput.get("daAn") != null ? formInput.get("daAn") : "".trim();
					String catId	= formInput.get("catId") != null ? formInput.get("catId") : "0".trim();
					String status	= formInput.get("status") != null ? formInput.get("status") : "0".trim();

					//retrieve old data
					List<Faq> oldFaqList = maintenanceService.findAllFaqById(Integer.parseInt(id));
					String oldQuesEn	= oldFaqList.get(0).getQuestionEn();
					String oldAnsEn		=  oldFaqList.get(0).getAnswerEn();
					String oldQuesBm	= oldFaqList.get(0).getQuestionBm();
					String oldAnsBm		=  oldFaqList.get(0).getAnswerBm();
					String oldQuesCn	= oldFaqList.get(0).getQuestionCn();
					String oldAnsCn 	=  oldFaqList.get(0).getAnswerCn();
					FaqCategory oldFaqCat = maintenanceService.findFaqCategoryById(Integer.parseInt(catId));
					String oldFaqCategory = oldFaqCat.getDescription(); 

					f0.setId(Integer.parseInt(id));
					f0.setQuestionEn(question);
					f0.setAnswerEn(answer);
					f0.setQuestionBm(soalan);
					f0.setAnswerBm(jawapan);
					f0.setQuestionCn(wenTi);
					f0.setAnswerCn(daAn);
					f0.setFaqCategory(maintenanceService.findFaqCategoryById(Integer.parseInt(catId)));
					f0.setStatus(status);

					maintenanceService.updateFAQ(f0, false);

					System.out.println("2.catId>>"+catId);
					System.out.println("2.status>>"+status);

					/*System.out.println("2.id>>"+oldFaqList.get(0).getId());
					System.out.println("2.question>>"+oldFaqList.get(0).getQuestionEn());
					System.out.println("2.answer>>"+oldFaqList.get(0).getAnswerEn());
					System.out.println("2.soalan>>"+oldFaqList.get(0).getQuestionBm());
					System.out.println("2.jawapan>>"+oldFaqList.get(0).getAnswerBm());
					System.out.println("2.wenTi>>"+oldFaqList.get(0).getQuestionCn());
					System.out.println("2.daAn>>"+oldFaqList.get(0).getAnswerCn());*/

					//reportService.auditLog("FAQ", new AuditTrail(user.getId(),0,"","Update FAQ: "+user.getLoginId(),"category:["+oldFaqCategory+"], question:["+oldQuesEn+"], answer:["+oldAnsEn+"], soalan:["+oldQuesBm+"], jawapan:["+oldAnsBm+"], wenTi:["+oldQuesCn+"], daAn:["+oldAnsCn+"]","category:["+f0.getFaqCategory().getDescription()+"], question:["+question+"], answer:["+answer+"], soalan:["+soalan+"], jawapan:["+jawapan+"], wenTi:["+wenTi+"], daAn:["+daAn+"]",""));
					reportService.auditLog("FAQ", new AuditTrail(user.getId(),0,"","Update FAQ: "+user.getLoginId(),"category:["+oldFaqCategory+"], question:["+oldQuesEn+"], answer:["+oldAnsEn+"]","category:["+f0.getFaqCategory().getDescription()+"], question:["+question+"], answer:["+answer+"]",""));

					map.put("success", StringUtil.showCommonMsg("success", "Successfully update FAQ."));


				}
			}
		} catch (Exception ex) {
			error +=" Fail to update FAQ.";
			reportService.auditLog("FAQ", new AuditTrail(user.getId(),0,"","Update FAQ: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
			log.error(ex.getMessage());
			ex.printStackTrace();		

		}

		map.put("error", StringUtil.showCommonMsg("error", error));

		return map;
	}

	// new module - to enable sorting
	@RequestMapping(value = "/admin/editFAQSequence",  method = RequestMethod.GET)
	public ModelAndView doEditFAQSequence()	{
		ModelAndView model = new ModelAndView();
		List<Faq> faqList = new ArrayList<Faq>();
		List<FaqCategory> typeList = new ArrayList<FaqCategory>();

		try {
			typeList = maintenanceService.findAllFaqCategory();

		} catch (SQLException ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		model.addObject("typeList",typeList);		
		model.setViewName("/maintenance/editFAQSeq");
		return model;
	}

	// new module - to enable sorting
	@RequestMapping(value = "/admin/editFAQSequence",  method = RequestMethod.POST)
	public ModelAndView searchFAQSequence(@RequestParam(value="selectType" , defaultValue="0") String selectType,
			@RequestParam(value="displayType" , defaultValue="") String displayType){

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		ModelAndView model = new ModelAndView();
		List<Faq> faqList = new ArrayList<Faq>();
		List<FaqCategory> typeList = new ArrayList<FaqCategory>();

		try {
			typeList = maintenanceService.findAllFaqCategory();
			faqList  = maintenanceService.findActiveQuesEnOrderBySequence(Integer.parseInt(selectType));
			System.out.println("faq active list >"+faqList.size());
			reportService.auditLog("FAQ", new AuditTrail(user.getId(),0,"","Search FAQ Sequence: "+user.getLoginId(),"category:["+selectType+"]","",""));


		} catch (SQLException ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		if(faqList.size()==0)
		{
			displayType = displayType+"-No Result Found";
		}
		model.addObject("displayType", displayType);	
		model.addObject("typeList",typeList);		
		model.addObject("faqList", faqList);
		model.setViewName("/maintenance/editFAQSeq");
		return model;
	}
	@RequestMapping(value = "/admin/updateFAQSequence",  method = RequestMethod.POST )
	public @ResponseBody Map<String,String> updateFAQSequence(@RequestBody List<Map<String, String>> client)
			throws IOException {

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		String error  = "";
		Map<String,String> map = new HashMap<String,String>();
		try {

			for (Map<String, String> formInput : client) {
				if(formInput!=null && !formInput.isEmpty()){


					String sort		= formInput.get("sort") != null ? formInput.get("sort") : "".trim();
					String displayType	= formInput.get("displayType") != null ? formInput.get("displayType") : "".trim();

					System.out.println("sort>>"+sort);
					System.out.println("displayType>>"+displayType);

					if(!StringUtil.trimToEmpty(sort).isEmpty()){
						String [] arr = sort.split("\\,");
						for(int i=0;i<arr.length;i++){
							System.out.println(i+">"+arr[i].toString());
							Faq f0 = new Faq();
							f0.setId(Integer.parseInt(arr[i].toString()));
							f0.setFaqSequence(i+1);
							maintenanceService.updateFAQ(f0, true);							
						}

					}
					//maintenanceService.updateFAQ(f0, true);

					/*System.out.println("2.id>>"+oldFaqList.get(0).getId());
					System.out.println("2.question>>"+oldFaqList.get(0).getQuestionEn());
					System.out.println("2.answer>>"+oldFaqList.get(0).getAnswerEn());
					System.out.println("2.soalan>>"+oldFaqList.get(0).getQuestionBm());
					System.out.println("2.jawapan>>"+oldFaqList.get(0).getAnswerBm());
					System.out.println("2.wenTi>>"+oldFaqList.get(0).getQuestionCn());
					System.out.println("2.daAn>>"+oldFaqList.get(0).getAnswerCn());*/

					reportService.auditLog("FAQ", new AuditTrail(user.getId(),0,"","Update FAQ Sequence: "+user.getLoginId(),"","category:["+displayType+"]",""));
					map.put("success", StringUtil.showCommonMsg("success", "Successfully update FAQ sequence."));
				}
			}
		} catch (Exception ex) {
			error +=" Fail to update FAQ Sequence.";
			reportService.auditLog("FAQ", new AuditTrail(user.getId(),0,"","Update FAQ Sequence: "+user.getLoginId(),"","Fail to process", "Fail-Message:"+ex.getMessage()));		
			log.error(ex.getMessage());
			ex.printStackTrace();		
			map.put("error", StringUtil.showCommonMsg("error", error));
		}

		return map;
	}

	@RequestMapping(value = "/admin/contactUs",  method = {RequestMethod.GET, RequestMethod.POST})
	public ModelAndView updateContactUs(
			@RequestParam(value="tel", defaultValue = "") String tel,
			@RequestParam(value="fax", defaultValue = "") String fax,
			@RequestParam(value="email", defaultValue = "") String email,
			@RequestParam(value="address", defaultValue = "") String address,
			@RequestParam(value="url", defaultValue = "") String url)	{


		ModelAndView model = new ModelAndView();

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ContactUs contact = new ContactUs();

		try{		
			contact = maintenanceService.findAllContact();

			if(contact!=null){
				String oldTelNo	= contact.getTelNo();
				String oldFaxNo	= contact.getFaxNo();
				String oldEmail	= contact.getEmail();
				String oldURL	= contact.getWebUrl();
				String oldAddress=contact.getAddress();

				if(!(tel.isEmpty() && fax.isEmpty() && email.isEmpty() 
						&& address.isEmpty() && url.isEmpty())){
					ContactUs us = new ContactUs();

					us.setId(contact.getId());
					us.setTelNo(StringUtil.trimToEmpty(tel));
					us.setFaxNo(StringUtil.trimToEmpty(fax));
					us.setEmail(StringUtil.trimToEmpty(email));
					us.setWebUrl(StringUtil.trimToEmpty(url));
					us.setAddress(StringUtil.trimToEmpty(address));

					maintenanceService.updateContact(us);
					reportService.auditLog("Contact Us", new AuditTrail(user.getId(),0,"","Update Contact Us: "+user.getLoginId()+",Task ID:"+contact.getId(),"tel:["+oldTelNo+"], fax:["+oldFaxNo+"], email:["+oldEmail+"], address:["+oldAddress+"], url:["+oldURL+"]", "tel:["+tel+"], fax:["+fax+"], email:["+tel+"], address:["+tel+"], url:["+url+"]", ""));		
					model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully update contact us."));
					log.info("update Contact Us");
				}	
			}
		}catch (Exception ex) {
			reportService.auditLog("Contact Us", new AuditTrail(user.getId(),0,"","Update Contact Us: "+user.getLoginId(),"","Fail to process", "tel:["+tel+"], fax:["+fax+"], email:["+tel+"], address:["+tel+"], url:["+url+"]\nFail-Message:"+ex.getMessage()));					
			log.error(ex.getMessage());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to update contact us."));
			ex.printStackTrace();
		}	

		model.addObject("contact", contact);
		model.setViewName("/maintenance/contactUs");
		return model;
	}

	@RequestMapping(value = "/admin/tariffSettings",  method = RequestMethod.GET)
	public ModelAndView doTariffSetting()	{
		ModelAndView model = new ModelAndView();
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		List<TariffCategory> categoryLs = new ArrayList<TariffCategory>();
		try {
			categoryLs = maintenanceService.findAllTariffCategory();
		} catch (SQLException ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
		}

		model.addObject("categoryLs",categoryLs);
		model.setViewName("/maintenance/tariffSettings");
		return model;
	}

	@RequestMapping(value = "/admin/tariffSettings",  method = RequestMethod.POST)
	public ModelAndView doUpdateTariffSetting(
			@RequestParam(value="action", defaultValue = "") String action,
			@RequestParam(value="selectCategory", defaultValue = "") String selectCategory,
			@RequestParam(value="displayCategory", defaultValue = "") String displayCategory,
			@RequestParam(value="rateMoreUnit", defaultValue = "0") String rateMoreUnit,
			@RequestParam(value="rateMorePrice", defaultValue = "0") String rateMorePrice,
			@RequestParam(value="exemptGst", defaultValue = "0") String exemptGst,
			@RequestParam(value="maxDemand", defaultValue = "0") String maxDemand,
			@RequestParam(value="minMonthlyCharge", defaultValue = "0") String minMonthlyCharge,
			@RequestParam(value="lowPowerFactor", defaultValue = "0") String lowPowerFactor,
			@RequestParam(value="peak", defaultValue = "0") String peak,
			@RequestParam(value="peakAmount", defaultValue = "0") String peakAmount,
			@RequestParam(value="nonPeakAmount", defaultValue = "0") String nonPeakAmount,
			@RequestParam(value="totalMyResult", defaultValue = "0") String totalMyResult,

			HttpServletRequest request, HttpServletResponse response){
		ModelAndView model = new ModelAndView();
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();

		List <TariffCategory> categoryLs = new ArrayList<TariffCategory>();

		try {
			log.info("action>"+action);
			int isDelete = 0;
			categoryLs = maintenanceService.findAllTariffCategory();
			TariffCategory tc = maintenanceService.findTariffCategoryById(Integer.parseInt(selectCategory));

			if(!action.isEmpty() && action.equalsIgnoreCase("search") && !selectCategory.isEmpty()){
				//System.out.println("search :"+displayCategory+">selectCategory:"+selectCategory);

				List <TariffSetting> dbResult = new ArrayList<TariffSetting>();
				List <MyTariffSettingsResult> myResult = new ArrayList<MyTariffSettingsResult>();
				dbResult = maintenanceService.findAllTariffSettings(Integer.parseInt(selectCategory));

				for(TariffSetting s : dbResult){
					MyTariffSettingsResult m = new MyTariffSettingsResult();
					m.setId(s.getId());
					m.setTariffCategoryId(s.getTariffCategoryId());
					m.setDeletedFlag(s.getDeletedFlag());
					m.setExemptedGstFirstKwh(s.getExemptedGstFirstKwh());
					m.setKwMaxDemandAmount(s.getKwMaxDemandAmount());
					m.setMinMonthlyChargeAmount(s.getMinMonthlyChargeAmount());
					m.setLowPowerFactor(s.getLowPowerFactor());
					m.setPeak(s.getPeak());
					m.setPeakAmount(s.getPeakAmount());
					m.setNonPeakAmount(s.getNonPeakAmount());
					m.setTrFromUnit(s.getTrFromUnit());
					m.setTrToUnit(s.getTrToUnit());
					m.setTrAmountPerUnit(s.getTrAmountPerUnit());
					m.setTrMoreUnit(s.getTrMoreUnit());
					m.setTrMoreAmount(s.getTrMoreAmount());
					m.setTariffCategory(s.getTariffCategory());
					myResult.add(m);

				}
				//System.out.println("myResult size ::"+myResult.size());
				model.addObject("myResult",myResult);
				model.addObject("selectCategory",selectCategory);
				model.addObject("categoryName",tc.getName());
				model.addObject("displayCategory",tc.getDescription());
				model.addObject("show",true);;
				model.setViewName("/maintenance/tariffSettings");
			}else if(!action.isEmpty() && action.equalsIgnoreCase("update") && !selectCategory.isEmpty()){
				List<String> fromLs = new ArrayList<String>();
				List<String> toLs = new ArrayList<String>();
				List<String> senLs = new ArrayList<String>();

				for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
					String fieldName = (String) e.nextElement();
					String fieldValue = request.getParameter(fieldName);
					if ((fieldValue != null) && (fieldValue.length() > 0)) {
						//System.out.println(fieldName + ":" + fieldValue);
						if(fieldName.toLowerCase().startsWith("rate") && fieldName.toLowerCase().endsWith("from")){
							fromLs.add(fieldValue);
						}
						if(fieldName.toLowerCase().startsWith("rate") && fieldName.toLowerCase().endsWith("to")){
							toLs.add(fieldValue);
						}
						if(fieldName.toLowerCase().startsWith("rate") && fieldName.toLowerCase().endsWith("price")){
							senLs.add(fieldValue);
						}
					}
				}

				try {
					/*System.out.println("selectCategory :"+selectCategory);
					System.out.println("displayCategory :"+displayCategory);
					System.out.println("rateMoreUnit :"+rateMoreUnit);
					System.out.println("rateMorePrice :"+rateMorePrice);
					System.out.println("exemptGst :"+exemptGst);
					System.out.println("maxDemand :"+maxDemand);
					System.out.println("minMonthlyCharge :"+minMonthlyCharge);
					System.out.println("peak :"+peak);
					System.out.println("lowPowerFactor :"+lowPowerFactor);*/

					List <MyTariffSettings> tariffList = new ArrayList<MyTariffSettings>();
					if(fromLs!=null && fromLs.size()>0){
						for(int i=0;i<fromLs.size();i++){
							MyTariffSettings tr = new MyTariffSettings();

							tr.setFrom(fromLs.get(i).toString());
							tr.setTo(toLs.get(i).toString());
							tr.setSen(senLs.get(i).toString());
							tariffList.add(tr);
						}
					}else{//no tariff rate
						MyTariffSettings tr = new MyTariffSettings();

						tr.setFrom("0");
						tr.setTo("0");
						tr.setSen("0");
						tariffList.add(tr);
					}

					//System.out.println("tariffList size ::"+tariffList.size());
					//System.out.println("totalMyResult :"+totalMyResult);

					if(Integer.parseInt(totalMyResult)>0){
						//System.out.println("at least one record(s)");
						isDelete = maintenanceService.deleteTariffSettingByCatId(Integer.parseInt(selectCategory));
						log.info("delete >>>"+isDelete);

					}else {
						isDelete = 1;
					}

					if(isDelete > 0){
						log.info("save data ::"+isDelete);
						//save
						StringBuilder sb = new StringBuilder();
						for(MyTariffSettings my : tariffList){
							sb.append("from(unit per month):["+my.getFrom()+"]\n");
							sb.append("to(unit per month):["+my.getTo()+"]\n");
							sb.append("price(sen):["+my.getSen()+"]\n");

							TariffSetting ts = new TariffSetting();
							BigDecimal inUnit = new BigDecimal("100");
							int decimal = 3;

							ts.setTrFromUnit(Integer.parseInt(my.getFrom()));
							ts.setTrToUnit(Integer.parseInt(my.getTo()));

							BigDecimal inSen = new BigDecimal(my.getSen());
							BigDecimal rmSen = inSen.divide(inUnit,decimal, BigDecimal.ROUND_HALF_UP);
							ts.setTrAmountPerUnit(rmSen);

							ts.setTariffCategoryId(Integer.parseInt(selectCategory));
							ts.setTrMoreUnit(Integer.parseInt(rateMoreUnit));

							BigDecimal inMore = new BigDecimal(rateMorePrice);
							BigDecimal rmMore = inMore.divide(inUnit,decimal, BigDecimal.ROUND_HALF_UP);
							ts.setTrMoreAmount(rmMore);

							ts.setExemptedGstFirstKwh(Integer.parseInt(exemptGst));

							BigDecimal rmUnit = new BigDecimal(maxDemand);
							ts.setKwMaxDemandAmount(rmUnit);

							ts.setLowPowerFactor(Integer.parseInt(lowPowerFactor));

							BigDecimal rmMin = new BigDecimal(minMonthlyCharge);
							ts.setMinMonthlyChargeAmount(rmMin);

							ts.setPeak(Integer.parseInt(peak));

							if(Integer.parseInt(peak) == 1){
								BigDecimal inPeakSen = new BigDecimal(peakAmount);
								BigDecimal rmPeakSen = inPeakSen.divide(inUnit,decimal, BigDecimal.ROUND_HALF_UP);
								ts.setPeakAmount(rmPeakSen);

								BigDecimal inNonPeakSen = new BigDecimal(nonPeakAmount);
								BigDecimal rmNonPeakSen = inNonPeakSen.divide(inUnit,decimal, BigDecimal.ROUND_HALF_UP);
								ts.setNonPeakAmount(rmNonPeakSen);
							}else{
								ts.setPeakAmount(new BigDecimal("0.00"));
								ts.setNonPeakAmount(new BigDecimal("0.00"));
							}

							ts.setTariffCategory(maintenanceService.findTariffCategoryById(Integer.parseInt(selectCategory)));

							maintenanceService.saveTariffSettings(ts);

							/*System.out.println("getTariffCategoryId>"+ts.getTariffCategoryId());
							System.out.println("getTrFromUnit>"+ts.getTrFromUnit());
							System.out.println("getTrToUnit>"+ts.getTrToUnit());
							System.out.println("getTrAmountPerUnit>"+ts.getTrAmountPerUnit());
							System.out.println("getTrMoreAmount>"+ts.getTrMoreUnit());
							System.out.println("getTrMoreAmount>"+ts.getTrMoreAmount());
							System.out.println("getExemptedGstFirstKwh>"+ts.getExemptedGstFirstKwh());
							System.out.println("getKwMaxDemandAmount>"+ts.getKwMaxDemandAmount());
							System.out.println("getLowPowerFactor>"+ts.getLowPowerFactor());
							System.out.println("getMinMonthlyChargeAmount>"+ts.getMinMonthlyChargeAmount());
							System.out.println("getPeak>"+ts.getPeak());
							System.out.println("getTariffCategory>"+ts.getTariffCategory().toString());*/

							reportService.auditLog("Tariff Settings", new AuditTrail(user.getId(),0,"","Tariff Settings: "+user.getLoginId(),"", "Tariff Type:["+displayCategory+"], Tariff Rate:["+sb.toString()+"], Tariff Rate(unit)>:["+rateMoreUnit+"], Tariff Rate(sen)>:["+rateMorePrice+"], Exempted GST(RM):["+exemptGst+"]kwh, kw of Max Demand(RM):["+maxDemand+"]kwh, Min Monthly Charge(RM):["+minMonthlyCharge+"],Low Power Factor:["+(Integer.parseInt(lowPowerFactor) == 1 ? "yes" : "no" )+"], Peak/Non Peak:["+(Integer.parseInt(peak) == 1 ? "yes" : "no" )+"],peak amount:["+peakAmount+"],non-peak amount:["+nonPeakAmount+"]","" ));		
							model.addObject("show",false);
							model.addObject("successMsg",StringUtil.showCommonMsg("success", "Successfully add/update tariff setting for "+displayCategory+"."));
						}
					}
				} catch (SQLException ex) {
					reportService.auditLog("Tariff Settings", new AuditTrail(user.getId(),0,"","Tariff Settings: "+user.getLoginId(),"","Fail to process","Fail-Message:"+ex.getMessage()));					
					log.error(ex.getMessage());
					model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to add/update tariff settings."));
					ex.printStackTrace();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		model.addObject("categoryLs",categoryLs);
		model.setViewName("/maintenance/tariffSettings");
		return model;
	}

	/*private static String getFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }

    @RequestMapping(value = "/admin/getStation",  method = RequestMethod.POST, produces="application/json")
    public @ResponseBody List<CustServiceLocation> getStation(@RequestBody  String json)
            throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        CustServiceLocation reqValue = mapper.readValue(json, 
        		CustServiceLocation.class);
        CustServiceLocation person = new CustServiceLocation();
        String paramRegion  = reqValue.getRegion();
        person.setRegion("vvdfd");
        person.setStation("setStation");        
        //return toJson(person);
    }	
	private String toJson(CustServiceLocation person) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String value = mapper.writeValueAsString(person);
            // return "["+value+"]";
            return value;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
	 */
}
