package com.seb.admin.portal.adapter;

import java.util.List;

public class USER {
	private String loginId;
	private String email;
	private String name;
	private String mobileNumber;
	private String lastLoginAt;
	private String status;
	private List <Contract> contractSubscribed ;
	
	
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getLastLoginAt() {
		return lastLoginAt;
	}
	public void setLastLoginAt(String lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Contract> getContractSubscribed() {
		return contractSubscribed;
	}
	public void setContractSubscribed(List<Contract> contractSubscribed) {
		this.contractSubscribed = contractSubscribed;
	}
	
	@Override
	public String toString() {
		return "USER [loginId=" + loginId + ", email=" + email + ", name=" + name + ", mobileNumber=" + mobileNumber
				+ ", lastLoginAt=" + lastLoginAt + ", status=" + status + ", contractSubscribed=" + contractSubscribed + "]";
	}


	
	

}

