package com.seb.admin.portal.spg.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.util.StringUtil;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OPaymentReport {
	
	@JsonProperty("payment_id")
	private long payment_id;
	
	@JsonProperty("merchant_id")
	private int merchant_id;
	
	@JsonProperty("merchant_transaction_id")
	private String merchant_transaction_id;
	
	@JsonProperty("amount")
	private double amount;
	
	@JsonProperty("customer_email")
	private String customer_email;
	
	@JsonProperty("customer_username")
	private String customer_username;
	
	@JsonProperty("status")
	private String status;
	
	@JsonProperty("product_description")
	private String product_description;
	
	@JsonProperty("client_return_url")
	private String client_return_url;
	
	@JsonProperty("gateway_id")
	private String gateway_id;
	
	@JsonProperty("gateway_transaction_id")
	private String gateway_transaction_id;
	
	@JsonProperty("gateway_authorization_datetime")
	private String gateway_authorization_datetime;
	
	@JsonProperty("gateway_status_id")
	private long gateway_status_id;
	
	@JsonProperty("merchant_s2s_status_id")
	private long merchant_s2s_status_id;
	
	@JsonProperty("merchant_client_status_id")
	private long merchant_client_status_id;
	
	@JsonProperty("last_updated_by")
	private String last_updated_by;
	
	@JsonProperty("last_updated_datetime")
	private String last_updated_datetime;
	
	@JsonProperty("created_by")
	private String created_by;
	
	@JsonProperty("created_datetime")
	private String created_datetime;

	public long getPayment_id() {
		return payment_id;
	}

	public void setPayment_id(long payment_id) {
		this.payment_id = payment_id;
	}

	public int getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(int merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getMerchant_transaction_id() {
		return merchant_transaction_id;
	}

	public void setMerchant_transaction_id(String merchant_transaction_id) {
		this.merchant_transaction_id = merchant_transaction_id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCustomer_email() {
		return customer_email;
	}

	public void setCustomer_email(String customer_email) {
		this.customer_email = customer_email;
	}

	public String getCustomer_username() {
		return customer_username;
	}

	public void setCustomer_username(String customer_username) {
		this.customer_username = customer_username;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProduct_description() {
		return product_description;
	}

	public void setProduct_description(String product_description) {
		this.product_description = product_description;
	}

	public String getClient_return_url() {
		return client_return_url;
	}

	public void setClient_return_url(String client_return_url) {
		this.client_return_url = client_return_url;
	}

	public String getGateway_id() {
		return gateway_id;
	}

	public void setGateway_id(String gateway_id) {
		this.gateway_id = gateway_id;
	}

	public String getGateway_transaction_id() {
		return StringUtil.trimToEmpty(gateway_transaction_id);
	}

	public void setGateway_transaction_id(String gateway_transaction_id) {
		this.gateway_transaction_id = gateway_transaction_id;
	}

	public String getGateway_authorization_datetime() {
		return StringUtil.trimToEmpty(gateway_authorization_datetime);
	}

	public void setGateway_authorization_datetime(String gateway_authorization_datetime) {
		this.gateway_authorization_datetime = gateway_authorization_datetime;
	}

	public long getGateway_status_id() {
		return gateway_status_id;
	}

	public void setGateway_status_id(long gateway_status_id) {
		this.gateway_status_id = gateway_status_id;
	}

	public long getMerchant_s2s_status_id() {
		return merchant_s2s_status_id;
	}

	public void setMerchant_s2s_status_id(long merchant_s2s_status_id) {
		this.merchant_s2s_status_id = merchant_s2s_status_id;
	}

	public long getMerchant_client_status_id() {
		return merchant_client_status_id;
	}

	public void setMerchant_client_status_id(long merchant_client_status_id) {
		this.merchant_client_status_id = merchant_client_status_id;
	}

	public String getLast_updated_by() {
		return last_updated_by;
	}

	public void setLast_updated_by(String last_updated_by) {
		this.last_updated_by = last_updated_by;
	}

	public String getLast_updated_datetime() {
		return last_updated_datetime;
	}

	public void setLast_updated_datetime(String last_updated_datetime) {
		this.last_updated_datetime = last_updated_datetime;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getCreated_datetime() {
		return created_datetime;
	}

	public void setCreated_datetime(String created_datetime) {
		this.created_datetime = created_datetime;
	}

	@Override
	public String toString() {
		return "payment_id:[" + payment_id + "], merchant_id:[" + merchant_id + "], merchant_transaction_id:["
				+ merchant_transaction_id + "], amount:[" + amount + "], customer_email:[" + customer_email
				+ "], customer_username:[" + customer_username + "], status:[" + status + "], product_description:["
				+ product_description + "], client_return_url:[" + client_return_url + "], gateway_id:[" + gateway_id
				+ "], gateway_transaction_id:[" + gateway_transaction_id + "], gateway_authorization_datetime:["
				+ gateway_authorization_datetime + "], gateway_status_id:[" + gateway_status_id
				+ "], merchant_s2s_status_id:[" + merchant_s2s_status_id + "], merchant_client_status_id:["
				+ merchant_client_status_id + "], last_updated_by:[" + last_updated_by + "], last_updated_datetime:["
				+ last_updated_datetime + "], created_by:[" + created_by + "], created_datetime:[" + created_datetime
				+ "]";
	}
	
	

}
