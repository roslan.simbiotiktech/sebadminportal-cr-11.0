package com.seb.admin.portal.spg.apis;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.constant.SPGConstant;
import com.seb.admin.portal.model.Setting;
import com.seb.admin.portal.service.SettingService;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;
import com.seb.admin.portal.spg.request.FPXBankListRequest;
import com.seb.admin.portal.spg.request.FPXBankQueryRequest;
import com.seb.admin.portal.spg.request.GatewayQueryFPXRequest;
import com.seb.admin.portal.spg.request.GatewayQueryMBBRequest;
import com.seb.admin.portal.spg.request.MerchantCreateRequest;
import com.seb.admin.portal.spg.request.MerchantQueryRequest;
import com.seb.admin.portal.spg.request.OFpxBankUpdateRequest;
import com.seb.admin.portal.spg.request.PaymentDetailReportRequest;
import com.seb.admin.portal.spg.request.SPGPaymentReportRequest;
import com.seb.admin.portal.spg.response.EditMerchantResponse;
import com.seb.admin.portal.spg.response.FPXBankListResponse;
import com.seb.admin.portal.spg.response.GatewayFPXGenerateCSRResponse;
import com.seb.admin.portal.spg.response.GatewayFPXResponse;
import com.seb.admin.portal.spg.response.GatewayMBBResponse;
import com.seb.admin.portal.spg.response.MerchantResponse;
import com.seb.admin.portal.spg.response.PaymentDetailReportResponse;
import com.seb.admin.portal.spg.response.PaymentReportExportResponse;
import com.seb.admin.portal.spg.response.QueryFPXBankResponse;
import com.seb.admin.portal.spg.response.SPGPaymentReportResponse;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Component
public class SPGAPIServices 
{
	@Autowired
	private SettingService settingService;

	
	@Value("http://219.92.233.8:8080/SPG/api/")
	private String API_URL;
	
	private SPGServicesInterface service;
	
	private static final Logger log = Logger.getLogger(SPGAPIServices.class);

	
	@PostConstruct
	private void init() throws SQLException 
	{
		Setting setting = new Setting();
		setting = settingService.findByKey(SPGConstant.SPG_API_URL);
		String apiURL = setting.getSettingValue();
		
		log.info("SPG API service init() - "+ apiURL);

		HttpLoggingInterceptor headerLog = new HttpLoggingInterceptor();
		headerLog.setLevel(Level.HEADERS);
		HttpLoggingInterceptor bodyLog = new HttpLoggingInterceptor();
		bodyLog.setLevel(Level.BODY);
		OkHttpClient client = new OkHttpClient.Builder()
		  .addInterceptor(headerLog)
		  .addInterceptor(bodyLog)
		  .build();

		log.info("[SPG API]"+bodyLog.toString());
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

				
		Retrofit retrofit = new Retrofit.Builder()
			    .baseUrl(apiURL)
			    .client(client)
			    .addConverterFactory(JacksonConverterFactory.create(mapper))
			    .build();
				

		service = retrofit.create(SPGServicesInterface.class);
	}
	
	public Response<MerchantResponse> executeCreateMerchant(MerchantCreateRequest payload, String language) throws IOException 
	{		
		return this.service.createMerchant(payload, language).execute();
	}
	
	public Response<EditMerchantResponse> executeListMerchant(SPGAPIGeneral payload, String language) throws IOException 
	{		
		return this.service.listMerchant(payload, language).execute();
	}
	
	public Response<MerchantResponse> executeQueryMerchant(MerchantQueryRequest payload, String language) throws IOException 
	{		
		return this.service.queryMerchant(payload, language).execute();
	}
	
	public Response<MerchantResponse> executeUpdateMerchant(MerchantCreateRequest payload, String language) throws IOException 
	{		
		return this.service.updateMerchant(payload, language).execute();
	}
	
	public Response<GatewayMBBResponse> executeQueryGatewayMBB (SPGAPIGeneral payload, String language) throws IOException 
	{		
		return this.service.queryGatewayMBB(payload, language).execute();
	}
	
	public Response<GatewayMBBResponse> executeUpdateGatewayMBB (GatewayQueryMBBRequest payload, String language) throws IOException 
	{		
		return this.service.updateGatewayMBB(payload, language).execute();
	}
	
	public Response<GatewayFPXResponse> executeQueryGatewayFPX (SPGAPIGeneral payload, String language) throws IOException 
	{		
		return this.service.queryGatewayFPX(payload, language).execute();
	}
	
	public Response<GatewayFPXResponse> executeUpdateGatewayFPX (GatewayQueryFPXRequest payload, String language) throws IOException 
	{		
		return this.service.updateGatewayFPX(payload, language).execute();
	}
	
	public Response<FPXBankListResponse> executeFPXBankList (FPXBankListRequest payload, String language) throws IOException 
	{		
		return this.service.fpxBankList(payload, language).execute();
	}
	
	public Response<QueryFPXBankResponse> executeQueryFPXBank (FPXBankQueryRequest payload, String language) throws IOException 
	{		
		return this.service.queryBank(payload, language).execute();
	}
	
	public Response<QueryFPXBankResponse> executeUpdateFPXBank (OFpxBankUpdateRequest payload, String language) throws IOException 
	{		
		return this.service.updateFPXBank(payload, language).execute();
	}
	
	public Response<SPGPaymentReportResponse> executeGetPaymentReport (SPGPaymentReportRequest payload, String language) throws IOException 
	{		
		return this.service.getPaymentReport(payload, language).execute();
	}
	
	public Response<GatewayFPXGenerateCSRResponse> executeGenerateCSR (SPGAPIGeneral payload, String language) throws IOException 
	{		
		return this.service.generateCSR(payload, language).execute();
	}
	
	public Response<PaymentDetailReportResponse> executeGetPaymentDetailReport (PaymentDetailReportRequest payload, String language) throws IOException 
	{		
		return this.service.getPaymentDetailReport(payload, language).execute();
	}
	
	public Response<PaymentReportExportResponse> executeExportPaymentReport (SPGPaymentReportRequest payload, String language) throws IOException 
	{		
		return this.service.exportPaymentReport(payload, language).execute();
	}
	
	
}
