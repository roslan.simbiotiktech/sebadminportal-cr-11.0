package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;


/**
 * The persistent class for the CONTACT_US database table.
 * 
 */
@Entity
@Table(name="CONTACT_US" , schema = ApplicationConstant.SCHEMA)
@NamedQuery(name="ContactUs.findAll", query="SELECT c FROM ContactUs c WHERE c.deletedFlag=0")
public class ContactUs implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", unique=true, nullable=false)
	private int id;

	@Column(name="ADDRESS", length=300)
	private String address;

	@Column(name="CREATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp createdDatetime;

	@Column(name="DELETED_FLAG", nullable=false)
	private int deletedFlag;

	@Column(length=50)
	private String email;

	@Column(name="FAX_NO", length=20)
	private String faxNo;

	@Column(name="TEL_NO", length=20)
	private String telNo;

	@Column(name="UPDATED_DATETIME", nullable=false, insertable = false, updatable = false)
	private Timestamp updatedDatetime;

	@Column(name="WEB_URL", length=200)
	private String webUrl;

	public ContactUs() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public int getDeletedFlag() {
		return this.deletedFlag;
	}

	public void setDeletedFlag(int deletedFlag) {
		this.deletedFlag = deletedFlag;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFaxNo() {
		return this.faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getTelNo() {
		return this.telNo;
	}

	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}

	public Timestamp getUpdatedDatetime() {
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Timestamp updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public String getWebUrl() {
		return this.webUrl;
	}

	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}

}