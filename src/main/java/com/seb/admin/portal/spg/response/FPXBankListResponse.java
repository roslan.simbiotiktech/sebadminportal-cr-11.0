package com.seb.admin.portal.spg.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OFpxBank;
import com.seb.admin.portal.spg.entity.ResponseStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FPXBankListResponse {
	@JsonProperty("status")
	private ResponseStatus responseStatus;
	@JsonProperty("fpx_banks")
	private List<OFpxBank> ofpxBank;
	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}
	public List<OFpxBank> getOfpxBank() {
		return ofpxBank;
	}
	public void setOfpxBank(List<OFpxBank> ofpxBank) {
		this.ofpxBank = ofpxBank;
	}
	@Override
	public String toString() {
		return "responseStatus:[" + responseStatus + "], ofpxBank:[" + ofpxBank + "]";
	}
	
	
}
