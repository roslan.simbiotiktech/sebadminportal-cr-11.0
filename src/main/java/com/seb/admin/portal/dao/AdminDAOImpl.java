package com.seb.admin.portal.dao;

import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.springframework.stereotype.Repository;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.AdminUser;

@Repository
public class AdminDAOImpl implements AdminDAO {

	@PersistenceContext(unitName = ApplicationConstant.UNIT_NAME, type = PersistenceContextType.EXTENDED)
	private EntityManager em;

	public boolean changePassword(int loginID, String newPassword) throws SQLException{
		try{	
			AdminUser update = em.find(AdminUser.class, loginID);
			update.setPassword(newPassword);
			em.merge(update);
			return true;

		}catch(Exception e){
			return false;
		}
	}

}
