package com.seb.admin.portal.adapter;

import com.seb.admin.portal.util.StringUtil;

public class ResendOTPResponse {
	private long respStat = -1;
	private String errCode;
	private String errDesc;	

	private String status;
	private String preferredCommMethod;
	private String otp;
	private String loginId;
	private String typeDesc;

	public ResendOTPResponse() {
		// TODO Auto-generated constructor stub
	}

	public long getRespStat() {
		return respStat;
	}

	public void setRespStat(long respStat) {
		this.respStat = respStat;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrDesc() {
		return errDesc;
	}

	public void setErrDesc(String errDesc) {
		this.errDesc = errDesc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPreferredCommMethod() {
		return StringUtil.trimToEmpty(preferredCommMethod);
	}

	public void setPreferredCommMethod(String preferredCommMethod) {
		this.preferredCommMethod = preferredCommMethod;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getTypeDesc() {
		return typeDesc;
	}

	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	
	



}
