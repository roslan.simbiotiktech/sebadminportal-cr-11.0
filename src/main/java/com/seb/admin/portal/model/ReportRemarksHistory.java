package com.seb.admin.portal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.seb.admin.portal.constant.ApplicationConstant;


@Entity
@Table(name="REPORT_REMARKS_HISTORY", schema = ApplicationConstant.SCHEMA)
public class ReportRemarksHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID",updatable = false, nullable = false)
	private int id;

	
	@Column(name="TRANS_ID")
	private int idInfo;
	
	
	@Column(name="REPORT_TYPE")
	private String reportType;
	
	@Column(name="UPDATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedDatetime;
	
	
	@Column(name="CREATED_DATETIME", nullable=false,  insertable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDatetime;
	
	@Column(name="CUSER_EMAIL")
	private String cuserEmail;
	
	@Column(name="ADMIN_EMAIL")
	private String adminEmail;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="REMARK",length=500)
	private String remark;


	public ReportRemarksHistory(int idInfo, int id, String reportType, Date updatedDatetime, Date createdDatetime,
			String cuserEmail, String adminEmail, String status, String remark) {
		super();
		this.idInfo = idInfo;
		this.id = id;
		this.reportType = reportType;
		this.updatedDatetime = updatedDatetime;
		this.createdDatetime = createdDatetime;
		this.cuserEmail = cuserEmail;
		this.adminEmail = adminEmail;
		this.status = status;
		this.remark = remark;
	}

	public ReportRemarksHistory() {
		// TODO Auto-generated constructor stub
	}




	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}



	public Date getUpdatedDatetime() {
		return updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public Date getCreatedDatetime() {
		return createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getCuserEmail() {
		return cuserEmail;
	}

	public void setCuserEmail(String cuserEmail) {
		this.cuserEmail = cuserEmail;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getIdInfo() {
		return idInfo;
	}

	public void setIdInfo(int idInfo) {
		this.idInfo = idInfo;
	}

	@Override
	public String toString() {
		return "idInfo:[" + idInfo + "], id:[" + id + "], reportType:[" + reportType + "], updatedDatetime:["
				+ updatedDatetime + "], createdDatetime:[" + createdDatetime + "], cuserEmail:[" + cuserEmail
				+ "], adminEmail:[" + adminEmail + "], status:[" + status + "], remark:[" + remark + "]";
	}


	
	
	
	
}

