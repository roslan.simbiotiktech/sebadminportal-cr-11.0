<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta name="_csrf" content="${_csrf.token}" />
	<meta name="_csrf_header" content="${_csrf.headerName}" />
    <meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
        </nav>
        <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp"%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            User Management &gt; Module Access Management
                        </div>
                        <div class="panel-body">
                            <form id="searchModuleForm"name="searchModuleForm" action="${pageContext.request.contextPath}/admin/moduleAccessMgmt.do" class="form-horizontal"  method="post" >
                                ${successMsg}${errorMsg}<div id="successMsg" style="display: none"></div><div id="errorMsg" style="display: none" class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> <span class="sr-only"></span></div>
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Login ID <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                     <input type="hidden" id="displayLoginId" name="displayLoginId" value = ""/>                                    
                                        <select class="form-control" id="selectLoginId" name="selectLoginId">
                                            <option value="">
                                                Please Select
                                            </option>                                            
                                           <c:forEach items="${loginIdList}" var="item">
											<option value="${item.id}">${item.loginId}</option>
											</c:forEach>
                                        </select>
                                    </div>
                                </div>                                
                                <div class="form-group">
                                    <div class="col-lg-5 col-lg-offset-3 text-right">
                                        <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> <button class="btn btn-primary" type="submit">Search</button> <button class="btn btn-warning" id="cancelBtn1" type="reset">Cancel</button>
                                    </div>
                                </div>
                            </form>
                            <div id="resultDiv" style="display:none; margin-top: 10%">
                             <form  id="editUserForm" name="editUserForm" action="${pageContext.request.contextPath}/admin/updateUser.do" class="form-horizontal" method="post">
                                <h4>Search Results : Login ID: <b>${displayLoginId}</b></h4>
                                <div class="form-group">
                                        <div class="alert alert-danger" id="messages" style="display: none;"></div>
                                    </div>
                                <table cellspacing="0" class="table table-striped table-bordered" id="tblSearch" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Use</th>
                                            <th>Module Name</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <c:if test="${not empty module}">
                                     <c:forEach items="${module}" var="item" varStatus="loop">
	                                 	<tr>
											<td>
											<c:choose>
											<c:when test="${item.access == 'Y' }"> 
											 
												<input type="checkbox" class="call-checkbox" name="access${item.id}"  onclick="onChangeCheckbox (this)"  checked value="${item.id}" >
											</c:when>
											<c:otherwise>
												<input type="checkbox" class="call-checkbox" name="access${item.id}" onclick="onChangeCheckbox (this)" value="${item.id}" >
											</c:otherwise>
											</c:choose>
	                                        <td> ${item.description}
	                                        <input id="adminId${item.id}" name="adminId${item.id}" type="hidden" value="${item.adminId}">
	                                        <input id="adminRole${item.id}" name="adminRole${item.id}" type="hidden" value="${item.adminRole}">
	                                        <input id="moduleCode${item.id}" name="moduleCode${item.id}" type="hidden" value="${item.moduleCode}">
                                        	<input id="adminLoginId${item.id}" name="adminLoginId${item.id}" type="hidden" value="${item.adminLoginId}">
                                        
	                                        </td>                                            
	                                  	</tr>
                                     </c:forEach>
                                     </c:if>
                                    </tbody>
                                </table><br>
                                    <div class="form-group">
                                        <div class="col-lg-5 col-lg-offset-3 text-right">
                                            <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}"> <button class="btn btn-primary" id="submitBtn" type="button" data-toggle="modal" data-target="#modal">Update</button> <button class="btn btn-warning" id="cancelBtn2" type="reset">Cancel</button>
                                        </div>
                                    </div>
                                    
                                     <!-- Add Confirmation Modal -->
									<div class="modal" id="modal" tabindex="-1">
							        <div class="modal-dialog">
							            <div class="modal-content">
							                <div class="modal-header">
							                    <button class="close" data-dismiss="modal" type="button"><span>&times;</span><span class="sr-only">Close</span></button>
												<h2 class="modal-title">Confirmation</h2>
							                </div>						
							                <div class="modal-body" id="add">
							                    Are you sure you want to update module access management?
							                </div>						
							                <div class="modal-footer">
							                    <button class="btn btn-primary" id="add" type="button">Update</button> <button class="btn btn-warning" data-dismiss="modal" type="button">Close</button> 
							                </div>
							            </div>
							        </div>
	   								</div>
   								<!-- End Confirmation Modal -->
                                </form>
                             </div><!-- /.resultList -->                           
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /#page-wrapper -->
    </div>
    <script type="text/javascript">
 // Handle click on checkbox
    function onChangeCheckbox(checkbox, no) {
        if (checkbox.checked) {
            document.getElementById("submitBtn").disabled = false;
        } else {
            error = "";
            $('#messages').hide();
        }
    }
   
 	var errorNo = ""; 	
    $(document).ready(function() {
        $('#selectLoginId').change(function() {
            var loginId = $("#selectLoginId option:selected").text();
            $('#displayLoginId').val(loginId);
        });
        if(${module != null && module.size() >=0}) {
            $('#resultDiv').show();
            var table = $('#tblSearch').DataTable({
                //  responsive: true,   
                bFilter: false,
                bSort: false,
                lengthMenu: [[50, 100], [50, 100]]
            });
        }
        $("button#add").click(function() {
            $('#searchModuleForm').find('.has-error').removeClass("has-error");
            $('#searchModuleForm').find('.has-success').removeClass("has-success");
            $('#searchModuleForm').find('.form-control-feedback').removeClass('glyphicon-remove');
            $('#searchModuleForm').find('.form-control-feedback').removeClass('glyphicon-ok');
            $('.form-group').find('small.help-block').hide();
            $('#searchModuleForm').formValidation('resetForm', true);
        });
        
        $("button#add").click(function() {
        	$('#modal').modal('hide');
        	
        	 var rows_selected = new Array();
         	errorNo = "";
             //console.log("submitBtn was clicked");
             var oTable = $('#tblSearch').dataTable();
             var rowcollection = oTable.$(".call-checkbox:checked", {
                 "page": "all"
             }); 
             if (rowcollection.length == 0) {
             	window.scrollTo(0,0);
             	//console.log('Please select the checkbox to update.');
             	$('#errorMsg').html("Please select the checkbox to update.").show();
                 return false;
             }
             rowcollection.each(function(index, elem) {
            	 var item = $(elem).val();
            	 var moduleCode = $("#moduleCode" + item).val();
            	 var adminId = $("#adminId" + item).val();
            	 var adminRole = $("#adminRole" + item).val();
            	 var adminLoginId = $("#adminLoginId" + item).val();
            	 //console.log('moduleCode>'+moduleCode);
            	 //console.log('adminId>'+adminId);
            	 
            	 rows_selected.push({
                     id: item,
                     moduleCode: moduleCode,
                     adminId:adminId,
                     adminRole:adminRole,
                     adminLoginId:adminLoginId
            	 })
             });
             
             //console.log("errorNo="+errorNo);
             //console.log("errorNo.length="+errorNo.length);
             if(errorNo.length>0){                	
                 $('#messages').html(errorNo).show();
                 window.scrollTo(0,250);
             	return false;
             }             
             //console.log("validate success:");
        	 var token = $("meta[name='_csrf']").attr("content");
             var header = $("meta[name='_csrf_header']").attr("content");
             $.ajax({
		         url: "${pageContext.request.contextPath}/admin/updateModuleAccessMgmt.do",
                 type: "POST",
                 dataType: "json",
                 data: JSON.stringify(rows_selected),                   
                 beforeSend: function(xhr) {
                     xhr.setRequestHeader(header, token);
                     xhr.setRequestHeader("Accept", "application/json");
                     xhr.setRequestHeader("Content-Type", "application/json");
                     sebApp.showIndicator();
                 },
                 success: function(data) {
                     $('#successMsg').html(data.success).show(); 
                     window.scrollTo(0,5);
                     rows_selected = new Array();
                     $('#resultDiv').hide();
                     
                 },
                 failure: function(data) {alert("Fail To Connect");},
                 complete: function(){sebApp.hideIndicator();}
             });
        
        return true;
        
        });
        $('#searchModuleForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
            	selectLoginId: {
                    validators: {
                        callback: {
                            message: 'Please Select Login ID',
                            callback: function(value, validator, $field) {
                                var options = validator.getFieldElements('selectLoginId').val();                                
                                return (options != null && options.length >0);
                            }
                        }
                    }
                }
            }
        });
    });
    </script>
</body>
</html>