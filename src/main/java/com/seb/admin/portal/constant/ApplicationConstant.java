package com.seb.admin.portal.constant;

import com.novell.ldap.LDAPConnection;

public class ApplicationConstant {

	public final static String SCHEMA = "seb";
	public final static String SCHEMA_SPG = "spg";
	public final static String UNIT_NAME = "seb";
	public final static String PROP_PATH = "/opt/application/conf/admin_portal.properties";
	public final static String SEB_TEMPLATES_BASE = "seb.templates.base";
	
	//setting
	public final static String ACTIVATION_CODE_EXPIRE_MINUTES = "ACTIVATION_CODE_EXPIRE_MINUTES";
	public final static String CHECKER_EMAIL = "CHECKER_EMAIL";

	public final static String POWER_ALERT_OUTAGE = "outage";
	public final static String POWER_ALERT_PREVENTIVE = "preventive";

	public final static String TAG_NAME_GENERAL = "NOTIFICATION_GENERAL";
	public final static String TAG_NAME_POWER_ALERT = "NOTIFICATION_POWER_ALERT";
	public final static String TAG_NAME_MAINTENANCE = "NOTIFICATION_MAINTENANCE";


	public final static String NOTIF_TYPE_PLAIN_KEY = "PLAIN";
	public final static String NOTIF_TYPE_PROMO_KEY = "PROMO";
	public final static String NOTIF_TYPE_POWER_ALERT_KEY = "POWER_ALERT";

	public final static String NOTIF_STATUS_UNSENT = "UNSENT";
	public final static String NOTIF_STATUS_SENT = "SENT";
	public final static String NOTIF_STATUS_SENDING = "SENDING";
	public final static String NOTIF_STATUS_OFF = "OFF";

	//CMS Adapter
	public final static String SEARCH_PROFILE_URL = "SEARCH.PROFILE.URL";
	public final static String VIEW_PROFILE_DETAILS_URL = "VIEW.PROFILE.DETAILS.URL";
	public final static String VIEW_CUSTOMER_HISTORY_URL = "VIEW.CUSTOMER.HISTORY.URL";
	public final static String RESET_CUSTOMER_PASSWORD_URL = "RESET.CUSTOMER.PASSWORD.URL";
	public final static String RESEND_OTP_URL = "RESEND.OTP.URL";
	public final static String RESEND_MOBILE_VERIFICATION_URL = "RESEND.MOBILE.VERIFICATION.URL";
	
	// LDAP constant
	public static final String ENCODING_UTF8 = "UTF8";
	public static final String LDAP_HOST = "LDAP.HOST";
	public static final String LDAP_PORT = "LDAP.PORT";
	public static final int LDAP_SEARCH_SCOPE = LDAPConnection.SCOPE_SUB;
	public static final int LDAP_VERSION = LDAPConnection.LDAP_V3;
	public static final String LDAP_DN = "LDAP.DN";
	public static final String LDAP_PASSWORD = "LDAP.PASSWORD";

	public static final String LDAP_SEARCH_BASE = "LDAP.SEARCH.BASE";
	public static final String LDAP_SEARCH_CRITERIA_1 = "LDAP.SEARCH.CRITERIA.1";
	public static final String LDAP_TYPE = "LDAP.TYPE";
	
	//ROLE
	public static final String ROLE_A = "ROLE_A";
	public static final String ROLE_M = "ROLE_M";
	public static final String ROLE_C = "ROLE_C";
	public static final String ROLE_N = "ROLE_N";
	
	//Portal Module
	public static final String MODULE_C_SCL = "Maintenance-Service Counter Locator(Checker)";
	public static final String MODULE_M_SCL = "Maintenance-Service Counter Locator(Maker)";
	public static final String MODULE_CODE_SCL = "02.01";
	public static final String MODULE_CODE_M_SCL_ADD = "02.01.01";
	public static final String MODULE_CODE_M_SCL_EDIT = "02.01.02";
	
	//Customer Service Locator Status
	public static final String APPROVED = "A";
	public static final String DELETED 	= "D";
	public static final String PENDING	= "P";
	public static final String REJECTED	= "R";
	
	public static final String CATEGORY_NEW		= "NEW";
	public static final String CATEGORY_EDITED	= "EDITED";
	public static final String CATEGROY_DELETED	= "DELETED";
	
	public static final String PCM1 = "FORGOT_PASS";
	public static final String PCM2 = "CHANGE_MOBILE";
	public static final String PCM3 = "CHANGE_PASS";
	
	public static final String PCM1_DESC = "FORGOT PASSWORD";
	public static final String PCM2_DESC = "CHANGE MOBILE NUMBER";
	public static final String PCM3_DESC = "CHANGE PASSWORD";
	public static final String EN = "en";

     
	
	
}
