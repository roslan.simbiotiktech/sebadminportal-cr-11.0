package com.seb.admin.portal.spg.request;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seb.admin.portal.spg.entity.OFpx;
import com.seb.admin.portal.spg.entity.SPGAPIGeneral;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GatewayQueryFPXRequest extends SPGAPIGeneral {
	
	@JsonProperty("fpx")
	private OFpx fpx;

	public OFpx getFpx() {
		return fpx;
	}

	public void setFpx(OFpx fpx) {
		this.fpx = fpx;
	}

	

	
	

}
