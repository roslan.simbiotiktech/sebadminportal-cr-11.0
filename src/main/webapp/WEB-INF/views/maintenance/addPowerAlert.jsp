<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="${_csrf.token}" name="_csrf">
<meta content="${_csrf.headerName}" name="_csrf_header">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
<title></title>
</head>
<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>
		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="font-weight: bold">Maintenance &gt; Outage Alert &gt; Add</div>
						<div class="panel-body">
							<form action="${pageContext.request.contextPath}/admin/addPowerAlert.do" class="form-horizontal" id="addPowerAlertForm" method="post"
								name="addPowerAlertForm">
								${successMsg}${errorMsg}
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Type <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<select class="form-control capital" id="selectType" name="selectType">
											<option value="">Please Select</option>
											<c:forEach items="${typeList}" var="item">
												<option value="${item.id}">${item.name}</option>
											</c:forEach>
										</select>

									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Station<span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<select class="form-control capital" id="selectStation" name="selectStation">
											<option value="">Please Select</option>
											<c:forEach items="${stationList}" var="item">
												<option value="${item.id}">${item.station}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Area <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<input class="form-control" id="area" maxlength="500" name="area" placeholder="Area" type="text">
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Title <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="title" maxlength="100" name="title" placeholder="Title" type="text">
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Causes <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<textarea class="form-control" cols="23" id="causes" maxlength="500" name="causes" rows="10">
</textarea>
									</div>
								</div>
								<div id="restoreDateDiv" class="form-group" style="display: none;">
									<p class="col-lg-5 control-label">
										Estimated Restoration Time <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='restoreDate'>
											<input id="inrestoreDate" name="inrestoreDate" class="form-control datepicker" type='text' readonly="readonly"> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div id="startDateTimeDiv" style="display: none;" class="form-group">
									<p class="col-lg-5 control-label">
										Start Date Time of Maintenance <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='startDateTime'>
											<input id="instartDateTime" name="instartDateTime" class="form-control datepicker" type='text' readonly="readonly"> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div id="endDateTimeDiv" style="display: none;" class="form-group">
									<p class="col-lg-5 control-label">
										End Date Time of Maintenance <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='endDateTime'>
											<input id="inendDateTime" name="inendDateTime" class="form-control datepicker" type='text' readonly="readonly"> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Push Notification <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<label class="radio-inline"><input id="pushNotif1" name="pushNotif" type="radio" value="1">On</label> <label class="radio-inline"><input
											checked id="pushNotif2" name="pushNotif" type="radio" value="0">Off</label>
									</div>
								</div>

								<div id="pushDateTimeDiv" class="form-group" style="display: none;">
									<p class="col-lg-5 control-label">
										Push DateTime <span style="color: red;">*</span> :
									</p>
									<div class="form-group">
										<div class="col-lg-3">
											<div class='input-group date' id='pushDateTime'>
												<input id="inpushDateTime" name="inpushDateTime" class="form-control datepicker" type='text' readonly="readonly"> <span
													class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
											</div>
										</div>
									</div>

									<div class="form-group">
										<p class="col-lg-5 control-label">
											Message <span style="color: red;">*</span> :
										</p>
										<div class="col-lg-3">
											<textarea class="form-control" cols="23" id="message" maxlength="120" name="message" rows="10"></textarea>
										</div>
									</div>
								</div>
								<br>
								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" id="submitBtn" type="button" data-toggle="modal" data-target="#modal">Add</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
										<input type="hidden" id="powerTypeName" name="powerTypeName" value="" /> <input type="hidden" id="displayStation" name="displayStation"
											value="" />
									</div>
								</div>
								<!-- Add Confirmation Modal -->
								<div class="modal fade" id="modal" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button class="close" data-dismiss="modal" type="button">
													<span>&times;</span><span class="sr-only">Close</span>
												</button>
												<h2 class="modal-title">Confirmation</h2>
											</div>
											<div class="modal-body" id="add">Are you sure you want to add outage alert?</div>
											<div class="modal-footer">
												<button class="btn btn-primary" id="add" name="add" type="submit">Add</button>
												<button class="btn btn-warning" data-dismiss="modal" type="button">Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- End Confirmation Modal -->

							</form>
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
	function resetForm() {
    //document.getElementById("addPowerAlertForm").reset();
    $('#').find('.has-error').removeClass("has-error");
    $('#addPowerAlertForm').find('.has-success').removeClass("has-success");
    $('#addPowerAlertForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    $('#addPowerAlertForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    $('.form-group').find('small.help-block').hide();
    //Removing the error elements from the from-group
    $('.form-group').removeClass('has-error has-feedback');
    $('.form-group').find('i.form-control-feedback').hide();
    $('#addPowerAlertForm').formValidation('resetForm', true);
    setTimeout(function() {
        $('#inrestoreDate').val(moment(date_round(new Date(), moment.duration(30, 'minutes'))).format('DD/MM/YYYY hh:mm A'));
        $('#instartDateTime').val(moment(date_round(new Date(), moment.duration(30, 'minutes'))).format('DD/MM/YYYY hh:mm A'));
        $('#inendDateTime').val(moment(date_round(new Date(), moment.duration(30, 'minutes'))).format('DD/MM/YYYY hh:mm A'));
        $('#inpushDateTime').val(moment(new Date()).format('DD/MM/YYYY hh:mm A'));
    }, 50);
    return false;
}
$(document).ready(function() {
    $('#restoreDate').datetimepicker({
        format: 'DD/MM/YYYY hh:mm A',
        defaultDate: moment(date_round(new Date(), moment.duration(30, 'minutes'))),
        stepping: 30,
        minDate: moment(date_round(new Date(), moment.duration(30, 'minutes'))),
        ignoreReadonly: true
    }).on("dp.change", function(e) {
        $('#restoreDate').data("DateTimePicker").minDate(date_round(new Date(), moment.duration(30, 'minutes')));
        $('#addPowerAlertForm').formValidation('revalidateField', 'inpushDateTime');
    });
    $('#startDateTime').datetimepicker({
        format: 'DD/MM/YYYY hh:mm A',
        defaultDate: moment(date_round(new Date(), moment.duration(30, 'minutes'))),
        stepping: 30,
        ignoreReadonly: true
    }).on("dp.change", function(e) {
        $('#addPowerAlertForm').formValidation('revalidateField', 'instartDateTime');
        $('#addPowerAlertForm').formValidation('revalidateField', 'inendDateTime');
        $('#addPowerAlertForm').formValidation('revalidateField', 'inpushDateTime');
        //console.log('2.instartDateTime>' + $('#instartDateTime').val());
        //console.log('2.inendDateTime>' + $('#inendDateTime').val());
    });
    $('#endDateTime').datetimepicker({
        format: 'DD/MM/YYYY hh:mm A',
        defaultDate: moment(date_round(new Date(), moment.duration(30, 'minutes'))),
        stepping: 30,
        ignoreReadonly: true
    }).on("dp.change", function(e) {
        $('#addPowerAlertForm').formValidation('revalidateField', 'instartDateTime');
        $('#addPowerAlertForm').formValidation('revalidateField', 'inendDateTime');
        $('#addPowerAlertForm').formValidation('revalidateField', 'inpushDateTime');
        //console.log('2.instartDateTime>' + $('#instartDateTime').val());
        //console.log('2.inendDateTime>' + $('#inendDateTime').val());
    });
    $('#pushDateTime').datetimepicker({
        format: 'DD/MM/YYYY hh:mm A',
        defaultDate: new Date(),
        ignoreReadonly: true
    }).on("dp.change", function(e) {
        $('#addPowerAlertForm').formValidation('revalidateField', 'inpushDateTime');
    });
    $('#pushNotif1').on("click", function() {
        $('#pushDateTimeDiv').show();
    });
    $('#pushNotif2').on("click", function() {
        $('#pushDateTimeDiv').hide();
    });
    $('#pushNotif2').on("change", function() {
        $('#pushDateTimeDiv').hide();
    });
    $('#selectType').change(function() {
        var powerTypeName = $("#selectType option:selected").text();
        if (powerTypeName != "") {
            $('#addPowerAlertForm').formValidation('revalidateField', 'inpushDateTime');
            $('#powerTypeName').val(powerTypeName);
            if (powerTypeName.toLowerCase().indexOf('outage') > -1) {
                $('#restoreDateDiv').show();
                $('#startDateTimeDiv').hide();
                $('#endDateTimeDiv').hide();
            } else {
                $('#restoreDateDiv').hide();
                $('#startDateTimeDiv').show();
                $('#endDateTimeDiv').show();
            }
        }
    });
    $('#selectStation').change(function() {
        var displayStation = $("#selectStation option:selected").text();
        $('#displayStation').val(displayStation);
    });
    $('#addPowerAlertForm').formValidation({
        framework: 'bootstrap',
        icon: {
            invalid: 'glyphicon glyphicon-remove',
        },
        fields: {
            selectType: {
                validators: {
                    callback: {
                        message: 'Please Select Type',
                        callback: function(value, validator, $field) {
                            var options = validator.getFieldElements('selectType').val();
                            return (options != null && options.length > 0);
                        }
                    }
                }
            },
            selectStation: {
                validators: {
                    callback: {
                        message: 'Please Select Station',
                        callback: function(value, validator, $field) {
                            var options = validator.getFieldElements('selectStation').val();
                            return (options != null && options.length > 0);
                        }
                    }
                }
            },
            area: {
                validators: {
                    notEmpty: {
                        message: 'Area is required'
                    },
                    stringLength: {
                        max: 500,
                        message: 'Area should not more than 200 characters'
                    }
                }
            },
            message: {
                validators: {
                    callback: {
                        callback: function(value, validator, $field) {
                            //var radio = $("input[type='radio'][name='pushNotif']:checked").val();
                            var radio = $('#addPowerAlertForm').find('[name="pushNotif"]:checked').val();
                            //console.log("text:radio>" + radio);
                            //console.log("text:value>" + $.trim(value));
                            if (radio == 1 && $.trim(value) == '') {
                                return {
                                    valid: false,
                                    message: 'Message is required'
                                }
                            } else {
                                //console.log("else");
                                return true;
                            }
                        }
                    }
                }
            },
            causes: {
                validators: {
                    notEmpty: {
                        message: 'Causes is required'
                    },
                    stringLength: {
                        min: 1,
                        max: 500,
                        message: 'Causes not more than 500 characters'
                    }
                }
            },
            inrestoreDate: {
                validators: {
                    callback: {
                        callback: function(value, validator, $field) {
                            if (value == '') {
                                $('#inrestoreDate').val(moment(new Date()).format('DD/MM/YYYY hh:mm A'));
                                //console.log("inrestoreDate>" + $('#inrestoreDate').val());
                                return true;
                            }
                            return true;
                        }
                    },
                    format: 'DD/MM/YYYY hh:mm A'
                }
            },
            instartDateTime: {
                validators: {
                    date: {
                        format: 'DD/MM/YYYY hh:mm A',
                    },
                    callback: {
                        callback: function(value, validator, $field) {
                            if (value == '') {
                                $('#instartDateTime').val(moment(new Date()).format('DD/MM/YYYY hh:mm A'));
                                //console.log("value>" + $('#instartDateTime').val());
                                return true;
                            } else if (moment($('#instartDateTime').val(), "DD/MM/YYYY hh:mm A").isAfter(moment($('#inendDateTime').val(), "DD/MM/YYYY hh:mm A"))) {
                                return {
                                    valid: false,
                                    message: 'Start Date time of Maintenance is not a valid.'
                                }
                            }
                            return true;
                        }
                    }
                }
            },
            inendDateTime: {
                validators: {
                    date: {
                        format: 'DD/MM/YYYY hh:mm A',
                    },
                    callback: {
                        callback: function(value, validator, $field) {
                            if (value == '') {
                                $('#inendDateTime').val(moment(new Date()).format('DD/MM/YYYY hh:mm A'));
                                //console.log("value>" + $('#inendDateTime').val());
                                return true;
                            } else if (moment($('#instartDateTime').val(), "DD/MM/YYYY hh:mm A").isAfter(moment($('#inendDateTime').val(), "DD/MM/YYYY hh:mm A"))) {
                                return {
                                    valid: false,
                                    message: 'End Date Time of Maintenance is not a valid.'
                                }
                            }
                            return true;
                        }
                    }
                }
            },
            inpushDateTime: {
                validators: {
                    callback: {
                        callback: function(value, validator, $field) {
                            if (value == '') {
                                $('#inpushDateTime').val(moment(new Date()).format('DD/MM/YYYY hh:mm A'));
                            }
                         	if ($("#selectType option:selected").text().toLowerCase().indexOf('outage') > -1 && moment(value, "DD/MM/YYYY hh:mm A").isAfter(moment($('#inrestoreDate').val(), "DD/MM/YYYY hh:mm A"))) {
                           // if ($("#selectType option:selected").text().toLowerCase().indexOf('outage') > -1 && value > $('#inrestoreDate').val()) {
                                return {
                                    valid: false,
                                    message: 'Push notification date time must  ealier than estimated restoration time'
                                }
                            } else if ($("#selectType option:selected").text().toLowerCase().indexOf('preventive') > -1 && moment(value, "DD/MM/YYYY hh:mm A").isAfter(moment($('#inendDateTime').val(), "DD/MM/YYYY hh:mm A"))) {
                                return {
                                    valid: false,
                                    message: 'Push notification date time must ealier than end date time of maintenance'
                                }
                            } else {
                                return true;
                            }
                        }
                    },
                    format: 'DD/MM/YYYY hh:mm A'
                }
            },
            title: {
                validators: {
                    notEmpty: {
                        message: 'Title is required'
                    },
                    stringLength: {
                        max: 200,
                        message: 'Title should less than 100 characters'
                    }
                }
            },
        }
    }).on('change', '[name="pushNotif"]', function(e) {
        $('#addPowerAlertForm').formValidation('revalidateField', 'message');
    }).on('success.field.fv', function(e, data) {
        if (data.field === 'instartDateTime' && !data.fv.isValidField('inendDateTime')) {
            // We need to revalidate the end date
            data.fv.revalidateField('inendDateTime');
        }
        if (data.field === 'inendDate' && !data.fv.isValidField('instartDateTime')) {
            // We need to revalidate the start date
            data.fv.revalidateField('instartDateTime');
        }
    }).on('success.validator.fv', function(e, data) {
        data.fv.disableSubmitButtons(false);
    });
});
    </script>
</body>
</html>