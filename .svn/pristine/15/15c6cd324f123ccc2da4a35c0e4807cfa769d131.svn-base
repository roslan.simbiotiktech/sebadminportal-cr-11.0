package com.seb.admin.portal.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.model.AuditTrailType;
import com.seb.admin.portal.model.BankResponse;
import com.seb.admin.portal.model.CustomerAuditTrail;
import com.seb.admin.portal.model.MyBillPayReportReq;
import com.seb.admin.portal.model.MyBillPayReportResp;
import com.seb.admin.portal.model.MyPayment;
import com.seb.admin.portal.model.PaymentGateway;
import com.seb.admin.portal.model.PaymentReference;
import com.seb.admin.portal.model.Report;
import com.seb.admin.portal.util.StringUtil;

@Repository
public class ReportDAOImpl implements ReportDAO{

	private static final Logger log = Logger.getLogger(ReportDAOImpl.class);

	@PersistenceContext(unitName = ApplicationConstant.UNIT_NAME)
	private EntityManager em;

	/**============================================
	 * Audit Trails Report
	 *============================================*/
	public AuditTrailType findAuditByID(String name) {
		AuditTrailType au = new AuditTrailType();
		try{
			au = (AuditTrailType) em.createNamedQuery("AuditTrailType.findById").setParameter("name", name).getSingleResult();

		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
		}		
		return au;
	}

	public int insertAuditTrail(AuditTrail at) {
		int ret = -3;
		try{
			AuditTrail newAt = new AuditTrail(at.getAdminUserId(), at.getAuditTrailTypeId(), at.getIpAddress(), at.getActivity(), 
					at.getOldValue(), at.getNewValue(),at.getRemark());		
			em.persist(newAt);
			ret = 1;

		}catch(Exception ex){
			log.error(ex.getMessage());
			ex.printStackTrace();
		}


		return ret;
	}

	@Override
	public List<AuditTrailType> findAllAuditType() throws SQLException {
		List <AuditTrailType> typeList = new ArrayList <AuditTrailType>();
		try{
			typeList = em.createNamedQuery("AuditTrailType.findAllAuditType",AuditTrailType.class).getResultList();

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return typeList;
	}

	@Override
	public List<AuditTrail> findAllByAdmin(Date dateFrom, Date dateTo, int adminId) {
		List <AuditTrail> trailList = new ArrayList <AuditTrail>();
		try{
			trailList = em.createNamedQuery("AuditTrail.findAllByAdmin",AuditTrail.class).setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.setParameter("adminId", adminId).getResultList();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return trailList;


	}

	@Override
	public List<AuditTrail> findAllByDate(Date dateFrom, Date dateTo) {
		List <AuditTrail> trailList = new ArrayList <AuditTrail>();
		try{
			trailList = em.createNamedQuery("AuditTrail.findAllByDate",AuditTrail.class)
					.setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo).getResultList();

		}catch(NoResultException noR){

		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return trailList;
	}



	@Override
	public List<AuditTrail> findAllByType (Date dateFrom, Date dateTo, int typeId){
		List <AuditTrail> trailList = new ArrayList <AuditTrail>();
		try{
			trailList = em.createNamedQuery("AuditTrail.findAllByType",AuditTrail.class).setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.setParameter("typeId", typeId).getResultList();
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return trailList;
	}

	@Override
	public List<AuditTrail> findAllByTypeAdmin(Date dateFrom, Date dateTo, int adminId, int typeId) {
		List <AuditTrail> trailList = new ArrayList <AuditTrail>();
		try{
			trailList = em.createNamedQuery("AuditTrail.findAllByTypeAdmin",AuditTrail.class).setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.setParameter("adminId", adminId)
					.setParameter("typeId", typeId).getResultList();
		}catch(NoResultException noR){}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return trailList;
	}

	/**============================================
	 * Make A Report
	 *============================================*/

	@Override
	public List<Report> findAllMakeReportByCondition(Date dateFrom, Date dateTo, String source, String type, String status) {
		List <Report> list = new ArrayList <Report>();
		try{
			list = em.createNamedQuery("Report.findAllByCondition",Report.class).setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.setParameter("channel", source)
					.setParameter("type", type)
					.setParameter("status", status).getResultList();
		}catch(NoResultException noR){}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return list;
	}	

	@Override
	public Report findMakeAReportById (int id) throws SQLException{
		return em.find(Report.class, id);		
	}


	@Override
	public Report updateMakeAReport(Report re) throws SQLException{
		Report update = new Report();
		try{	
			System.out.println("dao>>"+re.toString());
			System.out.println(">>>>>>>>>>>>>>>>>>>>"+re.toString());
			update = em.find(Report.class, re.getTransId());
			update.setCaseNumber(re.getCaseNumber());
			if(!StringUtil.trimToEmpty(re.getStatus()).equalsIgnoreCase(""))
				update.setStatus(re.getStatus());
			if(!StringUtil.trimToEmpty(re.getRemark()).equalsIgnoreCase(""))
				update.setRemark(re.getRemark());

			em.merge(update);
		}catch(NoResultException e){
			e.printStackTrace();
			return new Report();
		}	
		return update;
	}

	/**============================================
	 * Customer Activities Report
	 *============================================*/
	@Override
	public List<CustomerAuditTrail> findAllCustAuditReport(Date dateFrom, Date dateTo, String email) throws Exception {
		List <CustomerAuditTrail> list = new ArrayList <CustomerAuditTrail>();
		try{
			list = em.createNamedQuery("CustomerAuditTrail.findAllCustAuditReport",CustomerAuditTrail.class)
					.setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo)
					.setParameter("email", "%"+email+"%").getResultList();
		}catch(NoResultException noR){}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return list;
	}	


	/**============================================
	 * User Statistic Report
	 *============================================*/
	@Override
	public long findTotalByStatus(String status, Date dateFrom, Date dateTo) throws Exception {
		long result  = 0;
		try{
			result = em.createNamedQuery("User.findTotalByStatus",Long.class)
					.setParameter("status", status.toUpperCase())
					.setParameter("dateFrom", dateFrom)
					.setParameter("dateTo", dateTo).getSingleResult();

		}catch(NoResultException noR){}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public MyBillPayReportResp findAllBillPayReport(MyBillPayReportReq b0) throws Exception {
		int pageNumber = b0.getPageNumber();
		int pageSize = b0.getPageSize();
		int totalRecords = 0;

		List <PaymentReference> paymentList = new ArrayList <PaymentReference>();
		List <MyPayment> myPaymentList = new ArrayList <MyPayment>(); 
		MyBillPayReportResp resp = new MyBillPayReportResp();



		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<PaymentReference> cq = cb.createQuery(PaymentReference.class);
		Root<PaymentReference> root = cq.from(PaymentReference.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		cq.select(root);


		if(b0.getDateFrom()!=null && b0.getDateTo()!=null){
			System.out.println("dateFrom>>"+b0.getDateFrom());
			System.out.println("dateTo>>"+b0.getDateTo());
			Path<Date> createdDate = root.get("createdDatetime");
			predicates.add(cb.between(createdDate, b0.getDateFrom(), b0.getDateTo()));

		}
		if(b0.getContractAccNo() !=null && !b0.getContractAccNo().equalsIgnoreCase("")){
			System.out.println("contractAccNo>>"+b0.getContractAccNo());
			Path<String> contractAccountNumber = root.get("contractAccountNumber");
			predicates.add(cb.equal(contractAccountNumber, b0.getContractAccNo()));
		}
		if(b0.getPaymentMethod()!=null && !b0.getPaymentMethod().equalsIgnoreCase("ALL")){
			System.out.println("paymentMethod>"+b0.getPaymentMethod());
			Path<String> gatewayId = root.get("gatewayId");
			predicates.add(cb.equal(gatewayId, b0.getPaymentMethod()));
		}
		if(b0.getSebRefStatus()!=null && !b0.getSebRefStatus().equalsIgnoreCase("ALL")){
			System.out.println("SebRefStatus>"+b0.getSebRefStatus());
			Path<String> status = root.get("status");
			predicates.add(cb.equal(status, b0.getSebRefStatus()));
		}
		if(b0.getEmail()!=null && !b0.getEmail().equalsIgnoreCase("")){
			System.out.println("email>>"+b0.getEmail());
			Path<String> email = root.get("userEmail");
			predicates.add(cb.equal(email, b0.getEmail()));
		}
		if(b0.getPymtRef() !=null && !b0.getPymtRef().equalsIgnoreCase("")){
			System.out.println("payment reference>>"+b0.getPymtRef());
			Path<Integer> paymentId = root.get("paymentId");
			predicates.add(cb.equal(paymentId, Integer.parseInt(b0.getPymtRef())));
		}
		if(b0.getPaymentMethod().equalsIgnoreCase("FPX")){//FPX
			if(!b0.getFpxTxnId().equalsIgnoreCase("")){
				System.out.println("fpx transaction id>>"+b0.getFpxTxnId());
				Path<String> fpxRef = root.get("gatewayRef");
				predicates.add(cb.equal(fpxRef, b0.getFpxTxnId()));
			}
		}else if(b0.getPaymentMethod().equalsIgnoreCase("MPG")){//MPG
			if(!b0.getMbbRefNo().equalsIgnoreCase("")){
				System.out.println("maybank reference>>"+b0.getMbbRefNo());
				Path<String> gatewayRef = root.get("gatewayRef");
				predicates.add(cb.equal(gatewayRef, b0.getMbbRefNo()));
			}
		}


		try{
			cq.where(predicates.toArray(new Predicate[]{}));

			TypedQuery<PaymentReference> query = em.createQuery(cq);
			paymentList = query.getResultList();

			for(PaymentReference l : paymentList){
				MyPayment my = new MyPayment();

				my.setPaymentId(l.getPaymentId());
				my.setAmount(l.getAmount());
				my.setBankId(l.getBankId());
				my.setContractAccountNumber(l.getContractAccountNumber());
				my.setCreatedDatetime(l.getCreatedDatetime());
				my.setGatewayId(l.getGatewayId());
				my.setGatewayRef(l.getGatewayRef());
				my.setGatewayStatus(l.getGatewayStatus());
				my.setIsMobile(l.getIsMobile());
				my.setPaymentDatetime(l.getPaymentDatetime());
				my.setStatus(l.getStatus());
				my.setUpdatedDatetime(l.getUpdatedDatetime());
				my.setUserEmail(l.getUserEmail());

				PaymentGateway pg = findPaymentGatewayByID(l.getGatewayId());
				my.setUserPaymentId(getReferenceNo(pg.getReferencePrefix(),l.getPaymentId()));
				myPaymentList.add(my);

			}
			resp.setPaymentList(myPaymentList);


		}catch(NoResultException noR){
			log.error(noR.getMessage());
			noR.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
			log.error(ex.getMessage());
		}


		return resp;
	}	

	public static String getReferenceNo(String prefix, long referenceNo){
		return String.format("%s%08d", prefix, referenceNo);
	}

	/*@Override
	public MyBillPayReportResp findAllBillPayReport(MyBillPayReportReq b0) throws Exception {
		int pageNumber = b0.getPageNumber();
		int pageSize = b0.getPageSize();
		int totalRecords = 0;

		List <PaymentReference> totalPaymentList = new ArrayList <PaymentReference>();
		List <PaymentReference> paymentList = new ArrayList <PaymentReference>();
		List <MyPayment> myPaymentList = new ArrayList <MyPayment>(); 
		MyBillPayReportResp resp = new MyBillPayReportResp();



		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<PaymentReference> cq = cb.createQuery(PaymentReference.class);
		Root<PaymentReference> root = cq.from(PaymentReference.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		cq.select(root);


		if(b0.getDateFrom()!=null && b0.getDateTo()!=null){
			System.out.println("dateFrom>>"+b0.getDateFrom());
			System.out.println("dateTo>>"+b0.getDateTo());
			Path<Date> createdDate = root.get("createdDatetime");
			predicates.add(cb.between(createdDate, b0.getDateFrom(), b0.getDateTo()));

		}
		if(b0.getContractAccNo() !=null && !b0.getContractAccNo().equalsIgnoreCase("")){
			System.out.println("contractAccNo>>"+b0.getContractAccNo());
			Path<String> contractAccountNumber = root.get("contractAccountNumber");
			predicates.add(cb.equal(contractAccountNumber, b0.getContractAccNo()));
		}
		if(b0.getPaymentMethod()!=null && !b0.getPaymentMethod().equalsIgnoreCase("ALL")){
			System.out.println("paymentMethod>"+b0.getPaymentMethod());
			Path<String> gatewayId = root.get("gatewayId");
			predicates.add(cb.equal(gatewayId, b0.getPaymentMethod()));
		}
		if(b0.getEmail()!=null && !b0.getEmail().equalsIgnoreCase("")){
			System.out.println("email>>"+b0.getEmail());
			Path<String> email = root.get("userEmail");
			predicates.add(cb.equal(email, b0.getEmail()));
		}
		if(b0.getPymtRef() !=null && !b0.getPymtRef().equalsIgnoreCase("")){
			System.out.println("payment reference>>"+b0.getPymtRef());
			Path<Integer> paymentId = root.get("paymentId");
			predicates.add(cb.equal(paymentId, Integer.parseInt(b0.getPymtRef())));
		}
		if(b0.getPaymentMethod().equalsIgnoreCase("FPX")){//FPX
			if(!b0.getFpxTxnId().equalsIgnoreCase("")){
				System.out.println("fpx transaction id>>"+b0.getFpxTxnId());
				Path<String> fpxRef = root.get("gatewayRef");
				predicates.add(cb.equal(fpxRef, b0.getFpxTxnId()));
			}
		}else if(b0.getPaymentMethod().equalsIgnoreCase("MPG")){//MPG
			if(!b0.getMbbRefNo().equalsIgnoreCase("")){
				System.out.println("maybank reference>>"+b0.getMbbRefNo());
				Path<String> gatewayRef = root.get("gatewayRef");
				predicates.add(cb.equal(gatewayRef, b0.getMbbRefNo()));
			}
		}


		try{
			cq.where(predicates.toArray(new Predicate[]{}));

			TypedQuery<PaymentReference> query = em.createQuery(cq);
			query.setFirstResult((pageNumber-1) * pageSize); 
			query.setMaxResults(pageSize);
			paymentList = query.getResultList();

			//get Total Records
			TypedQuery<PaymentReference> queryAll = em.createQuery(cq);
			totalPaymentList  =  queryAll.getResultList();
			totalRecords = totalPaymentList.size();

			float nrOfPages = (float)totalRecords/pageSize;
			int maxPages = (int)( ((nrOfPages>(int)nrOfPages) || nrOfPages==0.0)?nrOfPages+1:nrOfPages);

			System.out.println("findAllBillPayReport totalRecords ::"+totalRecords);
			System.out.println("nrOfPages ::"+nrOfPages+",round = "+Math.round(nrOfPages));
			System.out.println("maxPages ::"+maxPages);

			resp.setMaxPage(maxPages);
			resp.setTotalRecords(totalRecords);
			resp.setPage(pageNumber);

			for(PaymentReference l : paymentList){
				MyPayment my = new MyPayment();

				my.setPaymentId(l.getPaymentId());
				my.setAmount(l.getAmount());
				my.setBankId(l.getBankId());
				my.setContractAccountNumber(l.getContractAccountNumber());
				my.setCreatedDatetime(l.getCreatedDatetime());
				my.setGatewayId(l.getGatewayId());
				my.setGatewayRef(l.getGatewayRef());
				my.setGatewayStatus(l.getGatewayStatus());
				my.setIsMobile(l.getIsMobile());
				my.setPaymentDatetime(l.getPaymentDatetime());
				my.setStatus(l.getStatus());
				my.setUpdatedDatetime(l.getUpdatedDatetime());
				my.setUserEmail(l.getUserEmail());

				PaymentGateway pg = findPaymentGatewayByID(l.getGatewayId());
				my.setUserPaymentId(pg.getReferencePrefix() + l.getPaymentId());
				myPaymentList.add(my);

			}
			resp.setPaymentList(myPaymentList);


		}catch(NoResultException noR){
			log.error(noR.getMessage());
			noR.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
			log.error(ex.getMessage());
		}


		return resp;
	}	*/

	@Override
	public PaymentGateway findPaymentGatewayByID(String id){		
		PaymentGateway finder = em.find(PaymentGateway.class, id);		
		return finder;
	}

	@Override
	public List<PaymentGateway> findAllPaymentGateway() throws SQLException {
		List <PaymentGateway> pgList = new ArrayList <PaymentGateway>();
		try{
			pgList = em.createNamedQuery("PaymentGateway.findAll",PaymentGateway.class).getResultList();
			System.out.println("pgList>>>"+pgList.size());
		}catch(NoResultException noR){
			log.error(noR.getMessage());
			noR.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return pgList;
	}


	@Override
	public List<BankResponse> findByMerchantidAndResponsecode(String merchantId, String status) throws SQLException {
		List <BankResponse> bankRespList = new ArrayList <BankResponse>();
		try{
			bankRespList = em.createNamedQuery("BankResponse.findByMerchantidAndResponsecode",BankResponse.class)
					.setParameter("merchantId", merchantId)
					.setParameter("responseCode", status)
					.getResultList();
			System.out.println("bankRespList>>>"+bankRespList.size());
		}catch(NoResultException noR){
			log.error(noR.getMessage());
			noR.printStackTrace();
		}
		catch(Exception ex){
			ex.printStackTrace();
			log.error(ex.getMessage());
		}

		return bankRespList;
	}

}
