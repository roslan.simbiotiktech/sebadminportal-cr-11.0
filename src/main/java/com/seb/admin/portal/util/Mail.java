package com.seb.admin.portal.util;

import java.util.List;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.service.ReportService;

public class Mail {

	@Autowired
	private ReportService reportService;
	private static final Logger log = Logger.getLogger(Mail.class);


	public void sendMail(String method , AdminUser user, String from, List <String> to, List <String> cc, List <String> bcc, String subject, String text, String type ){

		try {

			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			Session session = (Session) envCtx.lookup("mail/Session");

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));

			InternetAddress toAddr [] = convert(to);
			InternetAddress ccAddr [] = convert(cc);
			InternetAddress bccAddr [] = convert(bcc);

			if(to != null){
				message.setRecipients(Message.RecipientType.TO, toAddr); 
			}
			if(cc != null && cc.size() >0){
				message.setRecipients(Message.RecipientType.CC, ccAddr); 
			}
			if(bcc != null && bcc.size() >0){
				message.setRecipients(Message.RecipientType.BCC, bccAddr); 
			}

			message.setSubject(subject);
			message.setContent(text, type);
			Transport.send(message);
			log.info("Done");
			log.info("method ::"+method);
			log.info("getId ::"+user.getId());
			log.info("getLoginId() ::"+user.getLoginId());
			log.info("from ::"+from);
			log.info("to ::"+to);
			log.info("subject ::"+subject);
			log.info("body ::"+text);
			
			//reportService.auditLog("Service Counter Locator", new AuditTrail(user.getId(),0,"","Add Service Counter Locator: "+user.getLoginId(),"", "Email sent to "+to,"Email Subject: "+subject ));		


		} catch (MessagingException e) {
			log.error("e = "+e.getMessage());
			log.error("e2 = "+user.getLoginId());
			log.error("e3 = "+user.getId());
			reportService.auditLog(method, new AuditTrail(user.getId(),0,"","Add Service Counter Locator: "+user.getLoginId(),"","Fail to send email", "Fail-Message:"+to.toString()));					

			throw new RuntimeException(e);
		} catch (NamingException e) {
			log.error(e.getMessage());
			reportService.auditLog(method, new AuditTrail(user.getId(),0,"","Add Service Counter Locator: "+user.getLoginId(),"","Fail to send email", "Fail-Message:"+to.toString()));					
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/*private String convert(List <String> list)
	{
		if(list!= null && list.size()>0){
			String line ="\"";
			int count =0;
			for(String ls : list){
				line += count > 0 ? "," : "";
				line +=  ls.toString() ;

				count++;
			}
			line += "\"";
			return line;
		}
		return null;
	}*/

	private InternetAddress[] convert (List <String> to){
		InternetAddress[] toAddr = new InternetAddress [to.size()] ;
		System.out.println();
		for(int i=0;i<to.size();i++){
			try {
				System.out.println("to"+i+":"+to.get(i));
				toAddr[i] = new InternetAddress(to.get(i));
			} catch (AddressException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return toAddr;
	}

}
