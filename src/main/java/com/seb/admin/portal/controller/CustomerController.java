package com.seb.admin.portal.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.seb.admin.portal.adapter.CustAuditTrail;
import com.seb.admin.portal.adapter.CustomerHistoryResponse;
import com.seb.admin.portal.adapter.DataTablesSearchProfile;
import com.seb.admin.portal.adapter.DataTablesTO;
import com.seb.admin.portal.adapter.MySubscription;
import com.seb.admin.portal.adapter.ResendOTPRequest;
import com.seb.admin.portal.adapter.ResendOTPResponse;
import com.seb.admin.portal.adapter.ResetPasswordResponse;
import com.seb.admin.portal.adapter.SearchProfileRequest;
import com.seb.admin.portal.adapter.SearchProfileResponse;
import com.seb.admin.portal.adapter.USER;
import com.seb.admin.portal.adapter.ViewProfileResponse;
import com.seb.admin.portal.constant.ApplicationConstant;
import com.seb.admin.portal.model.AdminUser;
import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.model.MyProfileUser;
import com.seb.admin.portal.model.Subscription;
import com.seb.admin.portal.service.CustomerService;
import com.seb.admin.portal.service.ReportService;
import com.seb.admin.portal.util.StringUtil;

@Controller
public class CustomerController {
	private static final Logger log = Logger.getLogger(CustomerController.class);

	@Autowired
	private CustomerService custService;
	@Autowired
	private ReportService reportService;

	@RequestMapping(value = "/admin/customerProfile",  method = RequestMethod.GET)
	public ModelAndView doCustomerProfile() {
		ModelAndView model = new ModelAndView();

		model.setViewName("/customer/customerProfile");
		return model;
	}	

	@RequestMapping(value = "/admin/customerProfile",  method = RequestMethod.POST)
	public ModelAndView searchCustomerProfile(
			@RequestParam(value="selectProfile" , defaultValue= "") String selectProfile,
			@RequestParam(value="displayType" , defaultValue= "") String displayType,
			@RequestParam(value="msisdn", defaultValue= "") String msisdn,
			@RequestParam(value="name", defaultValue= "" )String name,
			@RequestParam(value="loginID" , defaultValue= "") String loginID,
			@RequestParam(value="contractAccNo" , defaultValue= "") String contractAccNo,
			@RequestParam(value="email" , defaultValue= "") String email)
	{	
		System.out.println("searchCustomerProfile>>"+selectProfile);
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();
		SearchProfileResponse r0 = new SearchProfileResponse();
		List<String> userStatusList = new ArrayList<String>();
		List<String> comMethodList = new ArrayList<String>();
		try{
			userStatusList.add("ACTIVE");
			userStatusList.add("DEACTIVATED");
			//userStatusList.add("TERMINATE");

			comMethodList.add("EMAIL");
			comMethodList.add("MOBILE");

			SearchProfileRequest myReq = new  SearchProfileRequest(loginID.trim(), email.trim(), name.trim(), msisdn.trim(), "", contractAccNo.trim());
			r0 = custService.cms_search_profile(1,myReq);
			System.out.println(r0.toString());

			if(r0.getRespStat()!=0){
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", r0.getErrDesc()));
			}
			
			String label ="";
			String myValue = "";

			model.addObject("searchType",selectProfile);
			if(msisdn!=null && !StringUtil.trimToEmpty(msisdn).equalsIgnoreCase("")){
				label = "Mobile Number";
				myValue = msisdn;
			}else if(name!=null && !StringUtil.trimToEmpty(name).equalsIgnoreCase("")){
				label = "Name";
				myValue = name;
			}else if(loginID!=null && !StringUtil.trimToEmpty(loginID).equalsIgnoreCase("")){
				label = "Login ID";
				myValue = loginID;
			}else if(contractAccNo!=null && !StringUtil.trimToEmpty(contractAccNo).equalsIgnoreCase("")){
				label = "Contract Account Number";
				myValue = contractAccNo;
			}else if(email!=null && !StringUtil.trimToEmpty(email).equalsIgnoreCase("")){
				label = "Email";
				myValue = email;
			}else{
				myValue = "All";
			}

			System.out.println(label+">"+myValue);
			model.addObject("label",label);
			model.addObject("myvalue",myValue);

			model.addObject("msisdn",msisdn);
			model.addObject("name",name);
			model.addObject("loginId",loginID);
			model.addObject("contractAccNo",contractAccNo);
			model.addObject("email",email);

			model.addObject("userStatusList",userStatusList);
			model.addObject("comMethodList",comMethodList);

			reportService.auditLog("Customer Profile", new AuditTrail(user.getId(),0,"","Search Customer Profile: "+user.getLoginId(),"", label+":["+myValue+"]", ""));		

		} catch (Exception ex) {
			log.error(ex.getMessage());
			System.out.println(ex.getCause());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search customer profile."));
			reportService.auditLog("Customer Profile", new AuditTrail(user.getId(),0,"","Customer Profile: "+user.getLoginId(),"", "Fail to process", "Fail-Message:"+ex.getMessage()));		
		}
		model.addObject("respStatus",r0.getRespStat());
		model.addObject("errorDesc",r0.getErrDesc());
		model.addObject("userList",r0.getRespUser());
		model.setViewName("/customer/customerProfile");
		return model;
	}

	@RequestMapping(value = "/admin/searchProfileTable", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody
	String searchProfileTable( @RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength,
			@RequestParam String sEcho,
			@RequestParam(value="msisdn", defaultValue= "") String msisdn,
			@RequestParam(value="name", defaultValue= "" )String name,
			@RequestParam(value="loginId" , defaultValue= "") String loginId,
			@RequestParam(value="contractAccNo" , defaultValue= "") String contractAccNo,
			@RequestParam(value="email" , defaultValue= "") String email		
			) {
		System.out.println("[searchProfileTable]");
		/*System.out.println("iDisplayStart---"+iDisplayStart);
		System.out.println("iDisplayLength---"+iDisplayLength);
		System.out.println("sEcho---"+sEcho);
		System.out.println("msisdn>>"+msisdn);
		System.out.println("name>>"+name);
		System.out.println("msisdn>>"+msisdn);
		System.out.println("loginId>>"+loginId);
		System.out.println("contractAccNo>>"+contractAccNo);
		System.out.println("email>>"+email);
*/
		DataTablesSearchProfile<USER> dt = new DataTablesSearchProfile<USER>();
		int totalRecords = 0;
		SearchProfileResponse r0 = new SearchProfileResponse();
		List<USER> userList = new ArrayList<USER>();
		try {

			int page = (iDisplayStart + iDisplayLength)/iDisplayLength;
			System.out.println("page >>"+page);

			SearchProfileRequest myReq = new  SearchProfileRequest(loginId.trim(), email.trim(), name.trim(), msisdn.trim(), "", contractAccNo.trim());
			r0 = custService.cms_search_profile(page, myReq);
			totalRecords = r0.getTotalRecords()!=null ?Integer.parseInt(r0.getTotalRecords()):0;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dt.setAaData(r0.getRespUser());  // this is the dataset reponse to client
		dt.setiTotalDisplayRecords(totalRecords);  // // the total data in db for datatables to calculate page no. and position
		dt.setiTotalRecords(totalRecords);   // the total data in db for datatables to calculate page no.
		dt.setsEcho(sEcho);
		dt.setiDisplayLength(1);
		System.out.println("total records>"+totalRecords);
		System.out.println("display records>"+dt.getiTotalDisplayRecords());
		return toJson(dt);
	}


	private String toJson(DataTablesTO<CustAuditTrail> dt){
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(dt);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	private String toJson(DataTablesSearchProfile<USER> dt){
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(dt);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/admin/getProfileDetails",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody ViewProfileResponse getProfileDetails(@RequestBody ViewProfileResponse req)
			throws IOException {
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		System.out.println("[getProfileDetails]");
		ViewProfileResponse viewResp = new ViewProfileResponse() ;	

		try {
			viewResp = custService.cms_view_profile_details(req);
			System.out.println("getLOGIN_ID>>"+req.getLoginId());		
			//System.out.println("getCONTRACT_ACC_NO>>"+req.getContractAccNo());	
			//System.out.println("getAccountName>"+viewResp.getContractDetail().getAccountName());
			//System.out.println("home tel "+viewResp.getLastLoginAt());
			System.out.println("size::"+viewResp.toString());
		} catch (Exception ex) {			
			ex.printStackTrace();
			log.error(ex.getMessage());
		}
		return viewResp;
	}


	@RequestMapping(value = "/admin/deleteSubscription",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody Map<String,String> deleteSubscription(@RequestBody MySubscription re)
			throws IOException {
		log.info("[deleteSubscription]");
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		Map<String,String> map = new HashMap<String,String>();

		try {
			String email = re.getEmail();
			String accountNumber = re.getAccountNumber();
			String status = re.getSubscriptionStatus();
			String remark = StringUtil.trimToEmpty(re.getRemark());

			System.out.println("email>>"+re.getEmail());	
			System.out.println("account Number>>"+re.getAccountNumber());	
			System.out.println("subscription status>>"+re.getSubscriptionStatus());	
			System.out.println("remark>"+re.getRemark());	
			System.out.println("finish~~");
			map.put("success","");
			Subscription subs =  new Subscription();
			subs = custService.findSelectedSubscription(email, accountNumber, status);
			System.out.println("findSelectedSubscription>>>"+subs);
			boolean result = false;

			if(subs!=null){
				result = custService.updateSubscriptionStatus(subs.getSubsId(), "DELETED", remark);
				System.out.println("update subscription status >>"+result);
				if(result)
					map.put("success","DELETED");
				else
				{
					System.out.println(">>>>>>>>>>"+result);
				}
			}

			reportService.auditLog("Customer Profile", new AuditTrail(user.getId(),0,"","Delete Subscription: "+user.getLoginId(),"", "email:["+re.getEmail()+"], account number:["+re.getAccountNumber()+"],remark:["+remark+"]", ""));		
		}  catch (Exception ex) {
			ex.printStackTrace();
			map.put("success","");
			log.error(ex.getMessage());
			System.out.println(ex.getCause());
			reportService.auditLog("Customer Profile", new AuditTrail(user.getId(),0,"","Delete Subscription: "+user.getLoginId(),"", "Fail to process", "Fail-Message:"+ex.getMessage()));		
		}
		return map;
	}


	@RequestMapping(value = "/admin/doEditProfile",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody Map<String,String> doEditProfile(@RequestBody MyProfileUser re)
			throws IOException {
		System.out.println("[doEditProfile]");
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		Map<String,String> map = new HashMap<String,String>();

		try {
			String loginId = re.getLoginId();
			String preferredMethod = re.getPreferredCommunicationMethod();
			String status = re.getUserStatus();
			String remark = StringUtil.trimToEmpty(re.getRemark());

			System.out.println("loginId>>"+re.getLoginId());	
			System.out.println("preferredMethod>>"+re.getPreferredCommunicationMethod());	
			System.out.println("status>>"+re.getUserStatus());	
			System.out.println("remark>"+re.getRemark());	
			System.out.println("finish~~");
			map.put("success","");
			boolean result = false;
			result = custService.updateUserProfile(loginId, status, preferredMethod);
			System.out.println(loginId+":: updateUserProfile>>>"+result);
			
			String oldSubStatus;
			String newSubStatus;
		     switch (status) {
		         case "ACTIVE":
		        	 oldSubStatus = "INACTIVE";
		        	 newSubStatus = "ACTIVE";
		             break;
		         case "DEACTIVATED":
		        	 oldSubStatus = "ACTIVE";
		        	 newSubStatus = "INACTIVE";
		             break;
		         default:
		        	 oldSubStatus = "ACTIVE";
		        	 newSubStatus = "ACTIVE";
		     }
			boolean resultSubs = custService.updateUserSubscription(loginId,oldSubStatus, newSubStatus );
			System.out.println(loginId+":: updateUserSubscription>>>"+resultSubs);
			
			if(result &resultSubs ){
				map.put("RESPONSE","OK");
				map.put("METHOD",preferredMethod);
				map.put("STATUS",status);
				map.put("REMARK",remark);
				map.put("LOGIN",loginId);
				map.put("SUB_STATUS",newSubStatus);
			}
			else
			{
				map.put("RESPONSE","ERROR");
				map.put("METHOD","");
				map.put("STATUS","");
				map.put("REMARK","");
				map.put("SUB_STATUS","");
				map.put("LOGIN",loginId);
			}
			//reportService.auditLog("Customer Profile", new AuditTrail(user.getId(),0,"","Update Profile: "+user.getLoginId(),"", "preferred method:["+re.getPreferredCommunicationMethod()+"],remark:["+remark+"]", ""));		
			reportService.auditLog("Customer Profile", new AuditTrail(user.getId(),0,"","Update Profile: "+user.getLoginId(),"", "update profile:"+result+", update subscription:"+resultSubs+", Status:["+re.getUserStatus()+"],preferred method:["+re.getPreferredCommunicationMethod()+"],remark:["+remark+"]", ""));		
		}  catch (Exception ex) {
			ex.printStackTrace();
			map.put("success","");
			log.error(ex.getMessage());
			System.out.println(ex.getCause());
			reportService.auditLog("Customer Profile", new AuditTrail(user.getId(),0,"","Update Profile: "+user.getLoginId(),"", "Fail to process", "Fail-Message:"+ex.getMessage()));		
		}
		return map;
	}



	@RequestMapping(value = "/admin/customerHistory",  method = RequestMethod.GET)
	public ModelAndView doCustomerHistory()	{
		ModelAndView model = new ModelAndView();
		model.setViewName("/customer/customerHistory");
		return model;
	}

	@RequestMapping(value = "/admin/customerHistory",  method = RequestMethod.POST)
	public ModelAndView searchCustomerHistory(
			@RequestParam(value="indateFrom", defaultValue = "") String dateFrom,
			@RequestParam(value="indateTo", defaultValue = "") String dateTo,
			@RequestParam(value="msisdn", defaultValue= "") String msisdn,
			@RequestParam(value="contractAccNo", defaultValue= "" )String contractAccNo,
			@RequestParam(value="loginID" , defaultValue= "") String loginId,
			@RequestParam(value="page" , defaultValue= "1") String page)	{		

		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();

		CustomerHistoryResponse custResp = new CustomerHistoryResponse();
		List<CustAuditTrail> custAuditList = new ArrayList<CustAuditTrail> ();

		try{			
			custResp = custService.cms_view_customer_history(Integer.parseInt(page), (dateFrom+ " 00:00:00"), (dateTo+ " 23:59:59"), msisdn, contractAccNo, loginId);
			reportService.auditLog("Customer History", new AuditTrail(user.getId(),0,"","Search Customer History: "+user.getLoginId(),"", "date From:["+dateFrom+"], date To:["+dateTo+"], Mobile Number:["+msisdn+"], Contract Account Number:["+contractAccNo+"], Login Id:["+loginId+"]", ""));		

			if(custResp.getRespStat()!=0){
				System.out.println("custResp.getErrDesc()>>"+custResp.getErrDesc());
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", custResp.getErrDesc()));
			}

		} catch (Exception ex) {
			log.error(ex.getMessage());
			ex.printStackTrace();
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search customer history."));
			reportService.auditLog("Customer History", new AuditTrail(user.getId(),0,"","Search Customer History: "+user.getLoginId(),"", "Fail to process", "Fail-Message:"+ex.getMessage()));		
		}
		System.out.println("total for audit record ::"+custAuditList.size());
		model.addObject("dateFrom",dateFrom);
		model.addObject("dateTo",dateTo);
		model.addObject("msisdn",msisdn);
		model.addObject("contractAccNo",contractAccNo);
		model.addObject("loginId",loginId);

		model.addObject("respStatus",custResp.getRespStat());
		model.addObject("errorDesc",custResp.getErrDesc());
		model.addObject("custAuditList",custAuditList);
		model.addObject("custResp",custResp);

		model.setViewName("/customer/customerHistory");
		return model;
	}

	@RequestMapping(value = "/admin/searchCustHistoryTable", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody
	String searchCustHistoryTable( @RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength,
			@RequestParam String sEcho,
			@RequestParam String dateFrom,
			@RequestParam String dateTo,
			@RequestParam String msisdn,
			@RequestParam String contractAccNo,
			@RequestParam String loginId			
			) {
		System.out.println("searchCustHistoryTable");
		System.out.println("iDisplayStart---"+iDisplayStart);
		System.out.println("iDisplayLength---"+iDisplayLength);
		System.out.println("sEcho---"+sEcho);
		System.out.println("dateFrom>>"+dateFrom);
		System.out.println("dateTo>>"+dateTo);
		System.out.println("msisdn>>"+msisdn);
		System.out.println("contractAccNo>>"+contractAccNo);
		System.out.println("loginId>>"+loginId);

		DataTablesTO<CustAuditTrail> dt = new DataTablesTO<CustAuditTrail>();
		int totalRecords = 0;
		CustomerHistoryResponse custResp = new CustomerHistoryResponse();
		List<CustAuditTrail> custAuditList = new ArrayList<CustAuditTrail> ();
		try {
			//iDisplayStart = iDisplayStart==0?50:iDisplayStart;

			int page = (iDisplayStart + iDisplayLength)/iDisplayLength;
			System.out.println("page >>"+page);

			custResp = custService.cms_view_customer_history(page, (dateFrom+ " 00:00:00"), (dateTo+ " 23:59:59"), msisdn, contractAccNo, loginId);
			totalRecords = custResp.getTotalRecords()!=null ?Integer.parseInt(custResp.getTotalRecords()):0;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		custAuditList.addAll(custResp.getCustATList());
		dt.setAaData(custAuditList);  // this is the dataset reponse to client
		dt.setiTotalDisplayRecords(totalRecords);  // // the total data in db for datatables to calculate page no. and position
		dt.setiTotalRecords(totalRecords);   // the total data in db for datatables to calculate page no.
		dt.setsEcho(sEcho);
		dt.setiDisplayLength(1);
		System.out.println("total records>"+totalRecords);
		System.out.println("display records>"+dt.getiTotalDisplayRecords());
		return toJson(dt);
	}

	@RequestMapping(value = "/admin/customerResetPassword",  method = RequestMethod.GET)
	public ModelAndView doCustomerResetPassword()	{
		ModelAndView model = new ModelAndView();

		model.setViewName("/customer/customerResetPassword");
		return model;
	}

	@RequestMapping(value = "/admin/customerResetPassword",  method = RequestMethod.POST)
	public ModelAndView searchCustomerResetPassword(
			@RequestParam(value="msisdn", defaultValue= "") String msisdn,
			@RequestParam(value="name", defaultValue= "" )String name,
			@RequestParam(value="loginID" , defaultValue= "") String loginID,
			@RequestParam(value="email" , defaultValue= "") String email
			)	{
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();

		SearchProfileResponse r0 = new SearchProfileResponse();
		try{
			SearchProfileRequest myReq = new  SearchProfileRequest(loginID.trim(), email.trim(), name.trim(), msisdn.trim(), "", "".trim());
			r0 = custService.cms_search_profile(1,myReq);

			if(r0.getRespStat()!=0){
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", r0.getErrDesc()));
			}
			/*System.out.println("[Customer - customerProfile]");
			System.out.println("selectProfile::"+selectProfile);
			System.out.println("msisdn::"+msisdn);
			System.out.println("name::"+name);
			System.out.println("loginID::"+loginID);
			System.out.println("contractAccNo::"+contractAccNo);
			System.out.println("email::"+email);*/

			String searchResult="";

			if(msisdn!=null && !StringUtil.trimToEmpty(msisdn).equalsIgnoreCase("")){
				System.out.println(msisdn);	
				searchResult +="Mobile Number :<b>"+msisdn+"</b><br/>";

			}else if(name!=null && !StringUtil.trimToEmpty(name).equalsIgnoreCase("")){
				System.out.println(name);	
				searchResult +="Name :<b>"+name+"</b><br/>";
			}else if(loginID!=null && !StringUtil.trimToEmpty(loginID).equalsIgnoreCase("")){
				System.out.println(loginID);	
				searchResult +="Login ID :<b>"+loginID+"</b><br/>";
			}else if(email!=null && !StringUtil.trimToEmpty(email).equalsIgnoreCase("")){
				System.out.println(email);	
				searchResult +="Email :<b>"+email+"</b><br/>";
			}

			reportService.auditLog("Customer Reset Password", new AuditTrail(user.getId(),0,"","Search Customer Reset Password: "+user.getLoginId(),"", searchResult.replaceAll("<b>", "").replaceAll("</b>", "").replaceAll("<br/>", ""), ""));		

			model.addObject("msisdn",msisdn);
			model.addObject("name",name);
			model.addObject("loginId",loginID);
			model.addObject("email",email);


		} catch (Exception ex) {
			log.error(ex.getMessage());
			System.out.println(ex.getCause());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search customer reset password."));
			reportService.auditLog("Customer Reset Password", new AuditTrail(user.getId(),0,"","Customer Reset Password: "+user.getLoginId(),"", "Fail to process", "Fail-Message:"+ex.getMessage()));		
		}
		model.addObject("respStatus",r0.getRespStat());
		model.addObject("errorDesc",r0.getErrDesc());
		model.addObject("userList",r0.getRespUser());

		model.setViewName("/customer/customerResetPassword");
		return model;
	}


	@RequestMapping(value = "/admin/resetCustomerPassword",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody ResetPasswordResponse resetCustomerPassword(@RequestBody ResetPasswordResponse re)
			throws IOException {
		System.out.println("[resetCustomerPassword]");
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ResetPasswordResponse viewResp = new ResetPasswordResponse() ;	

		try {
			System.out.println("loginId>>"+re.getLoginId());	
			viewResp = custService.cms_reset_customer_password(re.getLoginId());
			System.out.println("finish~~");
			System.out.println("status>>"+viewResp.getRespStat());
			System.out.println("new password>>"+viewResp.getNewPassword());

			reportService.auditLog("Customer Reset Password", new AuditTrail(user.getId(),0,"","Customer Reset Password: "+user.getLoginId(),"", "Success Reset Customer ["+re.getLoginId()+"] Password ", ""));		

		}  catch (Exception ex) {
			log.error(ex.getMessage());
			System.out.println(ex.getCause());
			reportService.auditLog("Customer Reset Password", new AuditTrail(user.getId(),0,"","Customer Reset Password: "+user.getLoginId(),"", "Fail to process", "Fail-Message:"+ex.getMessage()));		
		}
		return viewResp;
	}

	//resend activation code
	@RequestMapping(value = "/admin/resendActivationCode",  method = RequestMethod.GET)
	public ModelAndView doResendActivationCode()	{
		ModelAndView model = new ModelAndView();

		model.setViewName("/customer/customerResentActivationCode");
		return model;
	}

	@RequestMapping(value = "/admin/resendActivationCode",  method = RequestMethod.POST)
	public ModelAndView searchCustomerResendActivationCode(
			@RequestParam(value="msisdn", defaultValue= "") String msisdn,
			@RequestParam(value="name", defaultValue= "" )String name,
			@RequestParam(value="loginID" , defaultValue= "") String loginID,
			@RequestParam(value="email" , defaultValue= "") String email
			)	{
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ModelAndView model = new ModelAndView();

		SearchProfileResponse r0 = new SearchProfileResponse();
		try{
			SearchProfileRequest myReq = new  SearchProfileRequest(loginID.trim(), email.trim(), name.trim(), msisdn.trim(), "", "".trim());
			r0 = custService.cms_search_profile(1,myReq);

			if(r0.getRespStat()!=0){
				model.addObject("errorMsg",StringUtil.showCommonMsg("error", r0.getErrDesc()));
			}

			String searchResult="";

			if(msisdn!=null && !StringUtil.trimToEmpty(msisdn).equalsIgnoreCase("")){
				System.out.println(msisdn);	
				searchResult +="Mobile Number :<b>"+msisdn+"</b><br/>";

			}else if(name!=null && !StringUtil.trimToEmpty(name).equalsIgnoreCase("")){
				System.out.println(name);	
				searchResult +="Name :<b>"+name+"</b><br/>";
			}else if(loginID!=null && !StringUtil.trimToEmpty(loginID).equalsIgnoreCase("")){
				System.out.println(loginID);	
				searchResult +="Login ID :<b>"+loginID+"</b><br/>";
			}else if(email!=null && !StringUtil.trimToEmpty(email).equalsIgnoreCase("")){
				System.out.println(email);	
				searchResult +="Email :<b>"+email+"</b><br/>";
			}

			reportService.auditLog("Customer Resend Activation Code", new AuditTrail(user.getId(),0,"","Search Resend Activation Code: "+user.getLoginId(),"", searchResult.replaceAll("<b>", "").replaceAll("</b>", "").replaceAll("<br/>", ""), ""));		

			model.addObject("msisdn",msisdn);
			model.addObject("name",name);
			model.addObject("loginId",loginID);
			model.addObject("email",email);


		} catch (Exception ex) {
			log.error(ex.getMessage());
			System.out.println(ex.getCause());
			model.addObject("errorMsg",StringUtil.showCommonMsg("error", "Failed to search customer reset password."));
			reportService.auditLog("Customer Resend Activation Code", new AuditTrail(user.getId(),0,"","Resend Activation Code: "+user.getLoginId(),"", "Fail to process", "Fail-Message:"+ex.getMessage()));		
		}
		model.addObject("respStatus",r0.getRespStat());
		model.addObject("errorDesc",r0.getErrDesc());
		model.addObject("userList",r0.getRespUser());

		model.setViewName("/customer/customerResentActivationCode");
		return model;
	}


	@RequestMapping(value = "/admin/updateActivationCode",  method = RequestMethod.POST, produces="application/json")
	public @ResponseBody ResendOTPResponse resendActivationCode(@RequestBody ResendOTPRequest re)
			throws IOException {
		System.out.println("[updateActivationCode]");
		AdminUser user = CustomAuthenticationProvider.getCurrentUser();
		ResendOTPResponse viewResp = new ResendOTPResponse() ;
		ResendOTPRequest req = new ResendOTPRequest();
		String method = "";
		String typeDesc = "";

		try {
			System.out.println("getLoginId>>"+re.getLoginId());	
			System.out.println("getRemark>>"+re.getRemark());	
			System.out.println("getPreferredCommunicationMethod>>"+re.getType());
			System.out.println("getMobileNumber>>"+re.getMobileNumber());

			if(re.getType()!=null && re.getType().equalsIgnoreCase("PCM1")){
				method = ApplicationConstant.PCM1;
			}
			switch (re.getType()) {
			case "PCM1":
				method = ApplicationConstant.PCM1;
				typeDesc = ApplicationConstant.PCM1_DESC;
				break;
			case "PCM2":
				method = ApplicationConstant.PCM2;
				typeDesc = ApplicationConstant.PCM2_DESC;
				break;
			case "PCM3":
				method = ApplicationConstant.PCM3;
				typeDesc = ApplicationConstant.PCM3_DESC;
				break;
			default:
				throw new IllegalArgumentException("Invalid type: " + re.getType());
			}

			req.setLoginId(re.getLoginId());
			req.setMobileNumber(re.getMobileNumber());
			req.setRemark(re.getRemark());
			req.setType(method);
			req.setTypeDesc(typeDesc);

			if(re.getType().equalsIgnoreCase("PCM2")){
				viewResp = custService.cms_resend_mobile_verification_code(req);

			}else{
				viewResp = custService.cms_resend_otp(req);
			}
			
			if(viewResp.getRespStat()==0){
				reportService.auditLog("Customer Resend Activation Code", new AuditTrail(user.getId(),0,"","Resend Activation Code: "+user.getLoginId(),"", "Success Resend Activation Code for ["+re.getLoginId()+"], Type:["+method+"], Email:["+re.getLoginId()+"], New Mobile Number:["+re.getMobileNumber()+"], Preferred Communication Method:["+viewResp.getPreferredCommMethod()+"]", "remark:["+re.getRemark()+"]"));		
			}else{
				reportService.auditLog("Customer Resend Activation Code", new AuditTrail(user.getId(),0,"","Resend Activation Code: "+user.getLoginId(),"", "Fail Resend Activation Code for ["+re.getLoginId()+"], Type:["+method+"], Email:["+re.getLoginId()+"], New Mobile Number:["+re.getMobileNumber()+"], remark:["+re.getRemark()+"]", "Fail-Error Code:["+viewResp.getErrCode()+"], Error Description :["+viewResp.getErrDesc()+"]"));		
			}

		}  catch (Exception ex) {
			log.error(ex.getMessage());
			System.out.println(ex.getCause());
			reportService.auditLog("Customer Resend Activation Code", new AuditTrail(user.getId(),0,"","Resend Activation Code: "+user.getLoginId(),"", "Fail to process", "Fail-Message:"+ex.getMessage()));		
		}
		return viewResp;
	}

}
