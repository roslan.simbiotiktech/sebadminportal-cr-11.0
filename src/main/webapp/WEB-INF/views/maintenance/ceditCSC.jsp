<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>

</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->

		<nav class="navbar navbar-default navbar-static-top">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12" style="width: inherit; min-width: 100%">
					<div class="panel panel-default">
						<div class="panel-heading">Maintenance &gt; Service Counter Locator &gt; Edit (checker)</div>

						<div class="panel-body">
							<form action="${pageContext.request.contextPath}/admin/editCSC.do" class="form-horizontal" id="editCSCForm" method="post" name="editCSCForm">
								${successMsg}${errorMsg}
								<div id="successMsg" style="display: none"></div>
								<div id="errorMsg" style="display: none"></div>
								<div class="form-group required">
									<p class="col-lg-5 control-label">
										Status <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<select class="form-control capital" id="selectStatus" name="selectStatus">
											<option value="">Please Select</option>
											<option value="A">Approved</option>
											<option value="D">Deleted</option>
											<option value="P">Pending</option>
											<option value="R">Rejected</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" type="reset">Cancel</button>
									</div>
								</div>
							</form>

							<div id=resultDiv style="display: none; margin-top: 10%">
								<form action="${pageContext.request.contextPath}/admin/updateCheckerCSC.do" class="form-horizontal" id="updateEditCSCForm" method="post"
									name="updateEditCSCForm">
									<h4>
										Search Results : <b>${status}</b>
									</h4>
									<div class="form-group">
										<div id="messages" style="display: none;" class="alert alert-danger" role="alert"></div>
									</div>

									<table cellspacing="0" class="table table-striped table-bordered" id="tblSearch">

										<thead>
											<tr>
												<th>No.</th>

												<th>Region</th>

												<th>Area</th>

												<th>Address</th>

												<th>Longitude</th>

												<th>Latitude</th>

												<th>Opening Hour</th>

												<th>Status</th>

												<c:choose>
													<c:when test="${status =='Approved'}">
														<th>Date</th>
													</c:when>
													<c:when test="${status =='Deleted'}">
														<th>Reason</th>
														<th>Date</th>
													</c:when>
													<c:when test="${status =='Pending'}">
														<th>Category</th>
														<th>Reason</th>
														<th>Action</th>
														<th>Select</th>
													</c:when>
													<c:when test="${status =='Rejected'}">
														<th>Category</th>
														<th>Reason</th>
														<th>Date</th>
													</c:when>
												</c:choose>
											</tr>
										</thead>
										<tbody>
											<c:if test="${not empty c0List}">
												<c:choose>
													<%-- Approved --%>
													<c:when test="${status =='Approved'}">
														<c:forEach items="${c0List}" var="item" varStatus="loop">
															<tr>
																<td>${loop.index+1}<input class="form-control" id="total${item.id}" name="total${item.id}" type="hidden" value="${loop.index+1}"></td>
																<td>${fn:replace(item.custServLoc.region, "_", " ")}<input type="hidden" id="adminUserId${item.id}" name="adminUserId${item.id}"
																	value="${item.adminUserId}" /></td>
																<td>${item.custServLoc.station}<input class="form-control" id="station${item.id}" name="station${item.id}" type="hidden"
																	value="${item.custServLoc.id}"></td>

																<td>
																	<p>${item.addressLine1}</p>
																	<p>${item.addressLine2}</p>
																	<p>${item.addressLine3}</p>
																	<p>${item.postcode}</p>
																	<p>${item.city}</p>
																</td>

																<td class="col-md-1">${item.longitude}</td>

																<td>${item.latitude}</td>

																<td><textarea readonly="readonly" class="form-control" cols="23" rows="5">${item.openingHour}</textarea></td>

																<td>${status}</td>

																<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.updatedDatetime}"
																		pattern="dd/MM/YYYY hh:mm:ss a" /></td>
															</tr>
														</c:forEach>
													</c:when>
													<%-- #Approved --%>
													<%-- Deleted --%>
													<c:when test="${status =='Deleted'}">
														<c:forEach items="${c0List}" var="item" varStatus="loop">
															<tr>
																<td>${loop.index+1}</td>

																<td>${fn:replace(item.custServLoc.region, "_", " ")}</td>

																<td>${item.custServLoc.station}</td>

																<td>
																	<p>${item.addressLine1}</p>
																	<p>${item.addressLine2}</p>
																	<p>${item.addressLine3}</p>
																	<p>${item.postcode}</p>
																	<p>${item.city}</p>
																</td>

																<td class="col-md-1">${item.longitude}</td>

																<td>${item.latitude}</td>

																<td><textarea readonly="readonly" class="form-control" cols="23" rows="5">${item.openingHour}</textarea></td>

																<td>${status}</td>

																<td>${item.rejectReason}</td>

																<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.updatedDatetime}"
																		pattern="dd/MM/YYYY hh:mm:ss a" /></td>
															</tr>
														</c:forEach>
													</c:when>
													<%-- #deleted --%>

													<%-- Pending --%>
													<c:when test="${status =='Pending'}">
														<c:forEach items="${c0List}" var="item" varStatus="loop">
															<tr>
																<td>${loop.index+1}<input class="form-control" id="total${item.id}" name="total${item.id}" type="hidden" value="${loop.index+1}"><input type="hidden" id="editedId${item.id}" name="editedId${item.id}" value="${item.editedId}"></td>
																<td>${fn:replace(item.custServLoc.region, "_", " ")}<input type="hidden" id="adminUserId${item.id}" name="adminUserId${item.id}"
																	value="${item.adminUserId}" /></td>
																<td>${item.custServLoc.station}<input class="form-control" id="station${item.id}" name="station${item.id}" type="hidden"
																	value="${item.custServLoc.id}"></td>

																<td>
																	<p>${item.addressLine1}</p>
																	<p>${item.addressLine2}</p>
																	<p>${item.addressLine3}</p>
																	<p>${item.postcode}</p>
																	<p>${item.city}</p>
																</td>

																<td class="col-md-1">${item.longitude}</td>

																<td>${item.latitude}</td>

																<td><textarea readonly="readonly" class="form-control" cols="23" rows="5">${item.openingHour}</textarea></td>

																<td>${status}<input type="hidden" id="mainTable${item.id}" name="mainTable${item.id}" value="${item.mainTable}"></td>
																
																<td>${item.category}<input type="hidden" id="category${item.id}" name="category${item.id}" value="${item.category}"></td>
																
																<td><div style="width: 100px">${item.rejectReason}</div></td>

																<c:choose>
																	<c:when test="${item.status =='P'}">
																		<td>
																			<div class="radio" style="width: 80px">
																				<label><input type="radio" name="actionStatus${item.id}" value="A" onclick="onClickRadio('${item.id}')"> Approved</label>
																			</div>
																			<div class="radio" style="width: 80px">
																				<label><input type="radio" name="actionStatus${item.id}" value="R" onclick="onClickRadio('${item.id}')"> Rejected</label>
																			</div>
																			<div id="reasonDiv${item.id}" style="display: none;">
																				<textarea class="form-control" cols="23" id="reason${item.id}" name="reason${item.id}" maxlength="200" rows="10"></textarea>
																			</div>
																		</td>
																		<td><input type="checkbox" class="call-checkbox" value="${item.id}" onclick="onChangeCheckbox (this,${loop.index+1})" /></td>
																	</c:when>
																</c:choose>
															</tr>
														</c:forEach>
													</c:when>
													<%-- #Pending --%>
													<%-- Rejected --%>
													<c:when test="${fn:containsIgnoreCase(status, 'Rejected')}">
														<c:forEach items="${c0List}" var="item" varStatus="loop">
															<tr>
																<td>${loop.index+1}</td>

																<td>${fn:replace(item.custServLoc.region, "_", " ")}</td>

																<td>${item.custServLoc.station}</td>

																<td>
																	<p>${item.addressLine1}</p>
																	<p>${item.addressLine2}</p>
																	<p>${item.addressLine3}</p>
																	<p>${item.postcode}</p>
																	<p>${item.city}</p>
																</td>

																<td class="col-md-1">${item.longitude}</td>

																<td>${item.latitude}</td>

																<td><textarea readonly="readonly" class="form-control" cols="23" rows="5">${item.openingHour}</textarea></td>

																<td>${status}</td>
																
																<td>${item.category}</td>

																<td>${item.rejectReason}</td>

																<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.updatedDatetime}"
																		pattern="dd/MM/YYYY hh:mm:ss a" /></td>
															</tr>
														</c:forEach>
													</c:when>
													<%-- #Rejected --%>
												</c:choose>
											</c:if>

										</tbody>
									</table>
									<br>
									<c:choose>
										<c:when test="${status =='Pending'}">
											<div class="form-group">
												<div class="col-lg-5 col-lg-offset-3 text-right">
													<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
													<button id="submitBtn" class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal">Update</button>
													<button class="btn btn-warning" id="cancelBtn1" type="reset">Cancel</button>
												</div>
											</div>
										</c:when>
									</c:choose>
									<!-- Add Confirmation Modal -->
									<div class="modal" id="modal" tabindex="-1">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button class="close" data-dismiss="modal" type="button">
														<span>&times;</span><span class="sr-only">Close</span>
													</button>
													<h2 class="modal-title">Confirmation</h2>
												</div>
												<div class="modal-body" id="add">Are you sure you want to update service counter locater?</div>
												<div class="modal-footer">
													<button class="btn btn-primary" id="add" type="button">Update</button>
													<button class="btn btn-warning" data-dismiss="modal" type="button">Close</button>
												</div>
											</div>
										</div>
									</div>
									<!-- End Confirmation Modal -->
								</form>
								<!-- #form -->
							</div>
							<!-- /.resultList -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
    
 // Handle click on checkbox
    function onChangeCheckbox(checkbox, no) {
        if (checkbox.checked) {
            document.getElementById("submitBtn").disabled = false;
        } else {
            error = "";
            $('#messages').hide();
        }
    }
 
    function onClickRadio(item) {
    	 var radio = $("input[type='radio'][name='actionStatus"+ item+"']:checked").val(); 
    	 //console.log(radio);
    	if (radio == "A"){
    		$('#reasonDiv'+item).hide();
    	}else if(radio == "R"){
    		$('#reasonDiv'+item).show();
    	}
    }
    
    var errorNo = "";
    function validateForm(num, item) {
    	var radio = $("input[type='radio'][name='actionStatus"+ item+"']:checked").val();
    	//console.log("radio ::"+radio);
        if (radio == "R" && $.trim($('#reason' + item).val()) == '') {
            errorNo += "<p>Error : Item " + num+" : Reject Reason is required</p>";
        }if(radio== null | radio ==''){
        	errorNo += "<p>Error : Item " + num+" : Please click Approved or Reject</p>";
        }       
    }
 
    $(document).ready(function() {     	 
        if(${c0List!=null && c0List.size()>=0}){	
            $('#resultDiv').show();
           var table= $('#tblSearch').DataTable({
      		//  responsive: true,   
      		bFilter: false,
          	bSort: false,
          	lengthMenu: [[50, 100], [50, 100]]
        	});
        }
    	
      
        $("button#add").click(function() {
         	$('#modal').modal('hide');
        	 var rows_selected = new Array();
        	errorNo = "";
            //console.log("submitBtn was clicked");
            var oTable = $('#tblSearch').dataTable();
            var rowcollection = oTable.$(".call-checkbox:checked", {
                "page": "all"
            });  
            if (rowcollection.length == 0) {
            	window.scrollTo(0,0);
            	$('#errorMsg').html("<div id=\"errorMsg\" class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> <span class=\"sr-only\"></span> Please select the checkbox to update. </div>").show();
                return false;
            }
            	$('#errorMsg').hide();
                rowcollection.each(function(index, elem) {
                
                    var item 		= $(elem).val();
                    var rowIndex 	= $('#tblSearch').find('tbody tr:eq(' + item + ')').get(0);
                    var num 		= $('#total' + item).val();
                	var stationid 	= $('#station' + item).val();
                    var radio 		= $("input[type='radio'][name='actionStatus"+ item+"']:checked").val();
                    var reason 		= $('#reason' + item).val();
                    var adminUserId = $('#adminUserId'+item).val();
                    var category 	= $('#category'+item).val();
                    var mainTable 	= $('#mainTable'+item).val();
                    var editedId 	= $('#editedId'+item).val();
                    //end
                    
                    rows_selected.push({
                            id: item,
                            stationId : stationid,
                            reason: reason,  
                            status:radio,
                            adminUserId :adminUserId,
                            category: category,
                            mainTable: mainTable,
                            editedId: editedId
                            
                   })	
                     validateForm(num, item);
                   
                });
                
                //console.log("errorNo.length="+errorNo.length);
                if(errorNo.length>0){                	
                    $('#messages').html(errorNo).show();
                    window.scrollTo(0,250);
                	return false;
                }
            		
                	//console.log("validate success:");
                	 var token = $("meta[name='_csrf']").attr("content");
                     var header = $("meta[name='_csrf_header']").attr("content");
                     $.ajax({
     		            url: "${pageContext.request.contextPath}/admin/updateCheckerCSC.do",
                         type: "POST",
                         dataType: "json",
                         data: JSON.stringify(rows_selected),                   
                         beforeSend: function(xhr) {
                             xhr.setRequestHeader(header, token);
                             xhr.setRequestHeader("Accept", "application/json");
                             xhr.setRequestHeader("Content-Type", "application/json");
                             sebApp.showIndicator();
                         },
                         success: function(data) {                         	 
                             if(data.success.length>0){
                            	 $('#successMsg').html(data.success).show();
                             }
                             else if(data.error.length>0){	 
                            	 $('#errorMsg').html(data.error).show();
	                         } 
	                         
                             window.scrollTo(0,5);
                             rows_selected = new Array();
                             $('#resultDiv').hide();
                             
                         },                         
                         failure: function(data) {alert("Fail To Connect");},
                         complete: function(){
                        	 sebApp.hideIndicator();
                         }
                     });
                
                return true;
        });
        
       
        
        $("#cancelBtn1").click(function() {
            $('#ceditCSCForm').find('.has-error').removeClass("has-error");
            $('#ceditCSCForm').find('.has-success').removeClass("has-success");
            $('#ceditCSCForm').find('.form-control-feedback').removeClass('glyphicon-remove');
            $('#ceditCSCForm').find('.form-control-feedback').removeClass('glyphicon-ok');
            $('.form-group').find('small.help-block').hide();
        });
     
        $('#editCSCForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
                selectStatus: {
                    validators: {
                        callback: {
                            message: 'Please Select Status',
                            callback: function(value, validator, $field) {
                                var options = validator.getFieldElements('selectStatus').val();
                                return (options != null && options.length > 0);
                            }
                        }
                    }
                }
            }
        });
    });
    </script>
</body>
</html>