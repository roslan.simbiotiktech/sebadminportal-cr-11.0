package com.seb.admin.portal.adapter;

public class SearchProfileRequest {
	
	private String loginId;
	private String email;
	private String name;
	private String mobileNumber;
	private String lastLoginAt;
	private String contractAccNo;
	
	
	public SearchProfileRequest(String loginId, String email, String name, String mobileNumber, String lastLoginAt,
			String contractAccNo) {
		super();
		this.loginId = loginId;
		this.email = email;
		this.name = name;
		this.mobileNumber = mobileNumber;
		this.lastLoginAt = lastLoginAt;
		this.contractAccNo = contractAccNo;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getLastLoginAt() {
		return lastLoginAt;
	}
	public void setLastLoginAt(String lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}
	public String getContractAccNo() {
		return contractAccNo;
	}
	public void setContractAccNo(String contractAccNo) {
		this.contractAccNo = contractAccNo;
	}
	@Override
	public String toString() {
		return "SearchProfileRequest [loginId=" + loginId + ", email=" + email + ", name=" + name + ", mobileNumber="
				+ mobileNumber + ", lastLoginAt=" + lastLoginAt + ", contractAccNo=" + contractAccNo + "]";
	}
	
}
