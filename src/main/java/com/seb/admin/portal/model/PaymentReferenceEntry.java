package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.seb.admin.portal.constant.ApplicationConstant;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the PAYMENT_REFERENCE_ENTRY database table.
 * 
 */
@Entity
@Table(name="PAYMENT_REFERENCE_ENTRY", schema = ApplicationConstant.SCHEMA)
@NamedQuery(name="PaymentReferenceEntry.findAll", query="SELECT p FROM PaymentReferenceEntry p")
public class PaymentReferenceEntry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PAYMENT_ENTRY_ID")
	private int paymentEntryId;

	private BigDecimal amount;

	private int billed;

	@Column(name="BILLING_ATTEMPT_COUNT")
	private int billingAttemptCount;

	@Column(name="BILLING_ATTEMPT_LAST_DATETIME")
	private Timestamp billingAttemptLastDatetime;

	@Column(name="BILLING_ATTEMPT_LAST_ERROR")
	private String billingAttemptLastError;

	@Column(name="CONTRACT_ACCOUNT_NUMBER")
	private String contractAccountNumber;

	//bi-directional many-to-one association to PaymentReference
	@ManyToOne
	@JoinColumn(name="PAYMENT_ID", insertable = false, updatable = false)
	private PaymentReference paymentReference;

	public PaymentReferenceEntry() {
	}

	public int getPaymentEntryId() {
		return this.paymentEntryId;
	}

	public void setPaymentEntryId(int paymentEntryId) {
		this.paymentEntryId = paymentEntryId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public int getBilled() {
		return this.billed;
	}

	public void setBilled(int billed) {
		this.billed = billed;
	}

	public int getBillingAttemptCount() {
		return this.billingAttemptCount;
	}

	public void setBillingAttemptCount(int billingAttemptCount) {
		this.billingAttemptCount = billingAttemptCount;
	}

	public Timestamp getBillingAttemptLastDatetime() {
		return this.billingAttemptLastDatetime;
	}

	public void setBillingAttemptLastDatetime(Timestamp billingAttemptLastDatetime) {
		this.billingAttemptLastDatetime = billingAttemptLastDatetime;
	}

	public String getBillingAttemptLastError() {
		return this.billingAttemptLastError;
	}

	public void setBillingAttemptLastError(String billingAttemptLastError) {
		this.billingAttemptLastError = billingAttemptLastError;
	}

	public String getContractAccountNumber() {
		return this.contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}

	@JsonBackReference //for parent table
	public PaymentReference getPaymentReference() {
		return this.paymentReference;
	}

	public void setPaymentReference(PaymentReference paymentReference) {
		this.paymentReference = paymentReference;
	}

	@Override
	public String toString() {
		return "paymentEntryId:[" + paymentEntryId + "], amount:[" + amount + "], billed:[" + billed
				+ "], billingAttemptCount:[" + billingAttemptCount + "], billingAttemptLastDatetime:["
				+ billingAttemptLastDatetime + "], billingAttemptLastError:[" + billingAttemptLastError
				+ "], contractAccountNumber:[" + contractAccountNumber + "], paymentReference:[" + paymentReference
				+ "]";
	}

	
}