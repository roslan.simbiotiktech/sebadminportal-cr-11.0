<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>

<title></title>
<style type="text/css">
/* table.dataTable thead > tr > th {
padding-left: 8px;
padding-right: 30px;
} */
</style>
</head>

<body>
	<div id="wrapper">
		<!-- Navigation -->

		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12" style="width: inherit; min-width: 100%">
					<div class="panel panel-default">
						<div class="panel-heading">Maintenance &gt; News &gt; Edit</div>

						<div class="panel-body">
							<form action="${pageContext.request.contextPath}/admin/editNews.do" class="form-horizontal" id="searchNewsForm" method="post"
								name="searchNewsForm">
								${successMsg}${errorMsg}
								<div id="successMsg" style="display: none"></div>
								<div id="errorMsg" style="display: none"></div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Start Date <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<div class='input-group date' id='startDate'>
											<input class="form-control datepicker" id="instartDate" name="instartDate" type='text' readonly="readonly"> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										End Date <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<div class='input-group date' id='endDate'>
											<input class="form-control datepicker" id="inendDate" name="inendDate" readonly="readonly" type='text'> <span
												class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Title <span style="color: red;"></span> :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="title" maxlength="100" name="title" placeholder="Title" type="text">
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" id="searchBtn" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" type="button" onclick="return resetForm();">Cancel</button>
									</div>
								</div>
							</form>

							<div id="resultDiv" style="display: none; margin-top: 10%">
								<form action="${pageContext.request.contextPath}/admin/updateNews.do?" class="form-horizontal" id="updateNewsForm" method="post"
									name="updateNewsForm">
									<h4>
										Search Results : Start Date : <b>${dateFrom}</b>, End Date : <b>${dateTo}</b>, Title : <b>${title}</b>
									</h4>
									<div class="form-group">
										<div class="alert alert-danger" id="messages" style="display: none;"></div>
									</div>

									<table cellspacing="0" class="table table-striped table-bordered" id="tblSearch">
										<thead>
											<tr>
												<th>No.</th>

												<th>Title</th>

												<th>Image(Small)</th>

												<!-- <th>Image(Large)</th> -->

												<th>Description</th>

												<th>Start Date</th>

												<th>End Date</th>

												<th>Publish Date</th>

												<th>Push Notification</th>

												<th>Select</th>
											</tr>
										</thead>
										<tbody>
											<c:if test="${not empty newslist}">
												<c:forEach items="${newslist}" var="item" varStatus="status">
													<fmt:formatDate var="dateNow" type="both" dateStyle="long" value="<%=new java.util.Date()%>" pattern="yyyy-MM-dd HH:mm:ss" />
													<fmt:formatDate var="endDate" type="both" dateStyle="long" value="${item.endDatetime}" pattern="yyyy-MM-dd HH:mm:ss" />
													<input type="text" style="display: none;" id="currentDate${item.id}" name="currentDate${item.id}"
														value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="<%=new java.util.Date()%>" pattern="dd/MM/YYYY"/>" />
													<input type="text" style="display: none;" id="newDate${item.id}" name="newDate${item.id}"
														value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="<%=new java.util.Date()%>" pattern="dd/MM/YYYY hh:mm a"/>" />
													<c:choose>
														<c:when test="${endDate lt dateNow }">
															<!-- closed news end date less than current date -->
															<tr>
																<td>${status.index+1}</td>
																<td>${item.title}</td>
																<td><img src="${pageContext.request.contextPath}/admin/getNewsImage/simg/<c:out value="${item.id}"/>.do" width="140" height="107"
																	alt="" onerror='this.style.display = "none"' /></td>
																<td><textarea class="form-control" cols="23" id="description${item.id}" name="description${item.id}" maxlength="2000" rows="5"
																		readonly="readonly">${item.description}</textarea></td>
																<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.startDatetime}" pattern="dd/MM/YYYY" /></td>
																<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.endDatetime}" pattern="dd/MM/YYYY" /></td>
																<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.publishDatetime}" pattern="dd/MM/YYYY" /></td>
																<td><c:if test="${not empty item.myTagList}">
																		<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																			<c:if test="${tagStatus.index!=0}">
																				<br />
																				<br />
																				<br />
																				<br />
																			</c:if>

																			<c:choose>
																				<c:when test="${tagItem.status eq 'OFF'}">
																					<p>${tagItem.status}</p>
																				</c:when>
																				<c:otherwise>
																					<p>${tagItem.status}</p>
																					<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a" />
																					<br>
																					<br>
																					<p>Message :</p>
																					<textarea readonly="readonly" class="form-control" cols="23" rows="5">${tagItem.text}</textarea>
																				</c:otherwise>
																			</c:choose>
																		</c:forEach>
																	</c:if></td>
																<td><c:if test="${not empty item.myTagList}">
																		<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																			<c:if test="${tagStatus.index!=0}">
																				<p>
																					<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																				</p>
																			</c:if>
																			<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																		</c:forEach>
																	</c:if></td>
															</tr>
														</c:when>
														<c:otherwise>
															<tr>
																<td>${status.index+1}<input class="form-control" id="total${item.id}" name="total${item.id}" type="hidden"
																	value="${status.index+1}"> <input type="hidden" name="id" val="${item.id}" />
																</td>

																<td><input class="form-control" id="title${item.id}" maxlength="100" name="title${item.id}" placeholder="Title" type="text"
																	value="${item.title}"></td>

																<td><img id="spreview${item.id}" src="${pageContext.request.contextPath}/admin/getNewsImage/simg/<c:out value="${item.id}"/>.do"
																	alt="" onerror='this.style.display = "none"' width="140" height="107" /><input onchange="sreadFile(${item.id},this);" class="file"
																	id="rsimg${item.id}" name="rsimg${item.id}" type="file" accept="image/jpg, image/jpeg, image/png"> <input type="hidden"
																	id="newsimg${item.id}" name="newsimg${item.id}" value="" />
																	<button id="clear${item.id}" type="button">Clear</button>
																	<div id="serrorMsg${item.id}"></div>
																<td><textarea class="form-control" cols="23" id="description${item.id}" name="description${item.id}" maxlength="2000" rows="5">${item.description}</textarea></td>

																<td><div class='input-group date' id='startDate${item.id}'>
																		<input id="instartDate${item.id}" style="width: 100px" class="form-control" type='text' name="instartDate${item.id}"
																			value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.startDatetime}" pattern="dd/MM/YYYY"/>">
																		<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																	</div></td>

																<td>
																	<div class='input-group date' id='endDate${item.id}'>
																		<input id="inendDate${item.id}" style="width: 100px" class="form-control" type='text' name="inendDate${item.id}"
																			value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.endDatetime}" pattern="dd/MM/YYYY"/>">
																		<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																	</div>
																</td>

																<td><div class='input-group date' id='publishDate${item.id}'>
																		<input id="inpublishDate${item.id}" style="width: 100px" class="form-control" type='text' name="inpublishDate${item.id}"
																			value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.publishDatetime}" pattern="dd/MM/YYYY"/>">
																		<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																	</div></td>

																<td><c:if test="${not empty item.myTagList}">
																		<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																			<c:if test="${tagStatus.index!=0}">
																				<br />
																				<br />
																				<br />
																			</c:if>

																			<c:choose>
																				<c:when test="${tagItem.status eq 'SENT' || tagItem.status eq 'SENDING' }">
																					<p>${tagItem.status}</p>
																					<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a" />
																					<br>
																					<br>
																					<p>Message :</p>
																					<textarea readonly="readonly" class="form-control" cols="23" rows="5">${tagItem.text}</textarea>
																				</c:when>
																				<c:when test="${tagItem.status == 'UNSENT'}">
																					<div class="radio">
																						<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="1"
																							onclick="onClickRadio('${item.id}-${tagItem.notificationId}')" checked="checked"> On</label>
																					</div>
																					<br />
																					<div class="radio">
																						<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="0"
																							onclick="onClickRadio('${item.id}-${tagItem.notificationId}')"> Off</label>
																					</div>
																					<div id="pushNotifDiv${item.id}-${tagItem.notificationId}" style="visibility: hidden">
																						<div class='input-group date' id='pushDateTime${item.id}-${tagItem.notificationId}'>
																							<input id="inpushDateTime${item.id}-${tagItem.notificationId}" name="inpushDateTime${item.id}-${tagItem.notificationId}"
																								style="width: 150px" class="form-control datepicker" readonly type='text' onkeydown="return false;"
																								value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a"/>">
																							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																						</div>
																						<br> <br>
																						<p>
																							Message <span style="color: red;">*</span> :
																						</p>
																						<textarea class="form-control" id="text${item.id}-${tagItem.notificationId}" maxlength="120"
																							name="text${item.id}-${tagItem.notificationId}" rows="5">${tagItem.text}</textarea>
																						<input type="hidden" id="notificationId${item.id}-${tagItem.notificationId}"
																							name="notificationId${item.id}-${tagItem.notificationId}" value="${tagItem.notificationId}" />

																					</div>
																				</c:when>
																				<c:otherwise>
																					<div class="radio">
																						<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="1"
																							onclick="onClickRadio('${item.id}-${tagItem.notificationId}')"> On</label>
																					</div>
																					<br />
																					<div class="radio">
																						<label><input type="radio" name="pushNotif${item.id}-${tagItem.notificationId}" value="0"
																							onclick="onClickRadio('${item.id}-${tagItem.notificationId}')" checked="checked"> Off</label>
																					</div>
																					<div id="pushNotifDiv${item.id}-${tagItem.notificationId}" style="visibility: hidden">
																						<div class='input-group date' id='pushDateTime${item.id}-${tagItem.notificationId}'>
																							<input id="inpushDateTime${item.id}-${tagItem.notificationId}" name="inpushDateTime${item.id}-${tagItem.notificationId}"
																								style="width: 150px" class="form-control datepicker" readonly type='text' onkeydown="return false;"
																								value="<fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${tagItem.pushDatetime}" pattern="dd/MM/YYYY hh:mm a"/>">
																							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																						</div>
																						<br> <br>
																						<p>
																							Message <span style="color: red;">*</span> :
																						</p>
																						<textarea class="form-control" id="text${item.id}-${tagItem.notificationId}" maxlength="120"
																							name="text${item.id}-${tagItem.notificationId}" rows="5">${tagItem.text}</textarea>
																						<input type="hidden" id="notificationId${item.id}-${tagItem.notificationId}"
																							name="notificationId${item.id}-${tagItem.notificationId}" value="${tagItem.notificationId}" />
																					</div>
																				</c:otherwise>
																			</c:choose>
																		</c:forEach>
																	</c:if></td>
																<td><c:if test="${not empty item.myTagList}">
																		<c:set var="lastStatus" scope="session" value="" />
																		<c:forEach items="${item.myTagList}" var="tagItem" varStatus="tagStatus">
																			<c:choose>
																				<c:when test="${tagStatus.index==0}">
																					<!-- first row of result -->
																					<c:if test="${tagItem.status == 'SENT'}">
																						<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																					</c:if>
																					<c:if test="${tagItem.status != 'SENT'}">
																						<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																							name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																							onclick="onChangeCheckbox (this,${status.index+1})" />
																					</c:if>
																				</c:when>
																				<c:when test="${tagStatus.index!=0}">
																					<!-- subsequently result -->
																					<c:choose>
																						<c:when test="${lastStatus == 'SENT' && tagItem.status == 'SENT'}">
																							<p>
																								<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																							</p>
																							<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																						</c:when>
																						<c:when test="${lastStatus == 'SENT' && (tagItem.status == 'UNSENT' || tagItem.status == 'OFF')}">
																							<p>&nbsp;</p>
																							<p>
																								<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																							</p>
																							<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																								name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																								onclick="onChangeCheckbox (this,${status.index+1})" />
																						</c:when>
																						<c:when test="${lastStatus != 'SENT' && (tagItem.status == 'UNSENT' || tagItem.status == 'OFF')}">
																							<p>&nbsp;</p>
																							<p>
																								<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																							</p>
																							<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																								name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																								onclick="onChangeCheckbox (this,${status.index+1})" />
																						</c:when>
																						<c:when test="${lastStatus != 'SENT' && (tagItem.status == 'SENT')}">
																							<p>
																								<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																							</p>
																							<input type="checkbox" class="call-checkbox" value="${item.id}-${tagItem.notificationId}" disabled="disabled" />
																						</c:when>
																						<c:otherwise>
																							<p>
																								<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> <br />
																							</p>
																							<input type="checkbox" id="checkbox${item.id}-${tagItem.notificationId}" class="call-checkbox"
																								name="checkbox${item.id}-${tagItem.notificationId}" value="${item.id}-${tagItem.notificationId}"
																								onclick="onChangeCheckbox (this,${status.index+1})" />
																						</c:otherwise>
																					</c:choose>
																				</c:when>
																			</c:choose>
																			<c:set var="lastStatus" scope="session" value="${tagItem.status}" />
																		</c:forEach>
																	</c:if></td>
															</tr>
														</c:otherwise>
													</c:choose>

												</c:forEach>
											</c:if>

										</tbody>

									</table>
									<br>
									<div class="form-group">
										<div class="col-lg-5 col-lg-offset-3 text-right">
											<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
											<button id="submitBtn" class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal">Update</button>
											<button class="btn btn-warning" id="cancelBtn1" type="reset">Cancel</button>
										</div>
									</div>
									<!-- Add Confirmation Modal -->
									<div class="modal" id="modal" tabindex="-1">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button class="close" data-dismiss="modal" type="button">
														<span>&times;</span><span class="sr-only">Close</span>
													</button>
													<h2 class="modal-title">Confirmation</h2>
												</div>
												<div class="modal-body" id="add">Are you sure you want to update news?</div>
												<div class="modal-footer">
													<button class="btn btn-primary" id="add" type="button">Update</button>
													<button class="btn btn-warning" data-dismiss="modal" type="button">Close</button>
												</div>
											</div>
										</div>
									</div>
									<!-- End Confirmation Modal -->


								</form>
							</div>
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
	 function resetForm() {
	 		$('#').find('.has-error').removeClass("has-error");
	 		$('#searchNewsForm').find('.has-success').removeClass("has-success");
	 		$('#searchNewsForm').find('.form-control-feedback').removeClass('glyphicon-remove');
	 		$('#searchNewsForm').find('.form-control-feedback').removeClass('glyphicon-ok');
	 		$('.form-group').find('small.help-block').hide();
	 		//Removing the error elements from the from-group
	 		$('.form-group').removeClass('has-error has-feedback');
	 		$('.form-group').find('i.form-control-feedback').hide();
	 		$('#searchNewsForm').formValidation('resetForm', true);
	 		setTimeout(function() {
	 			$('#inendDate').val(moment(moment().add(1, 'd').toDate()).format('DD/MM/YYYY'));
	 			$('#instartDate').val(moment(new Date()).format('DD/MM/YYYY'));
	 		}, 50);
	 		return false;
	 	}
	 	//using jquery.form.js

	 function uploadJqueryForm() {
		 tinyMCE.triggerSave();
	 	$("#updateNewsForm").ajaxForm({
	 		success: function(data) {
	 			// $(name).html('<img id="spreview'+id+'" src="'+data+'" width="140" height="107"/>');
	 		},
	 		dataType: "text"
	 	}).submit();
	 }

	 function sreadFile(id, input) {
		// console.log("id>>"+id);
	 	var ext = $('#rsimg' + id).val().split('.').pop().toLowerCase();
	 	//console.log("1.size>>"+input.files[0].size);
	 	//console.log(input.files[0].size> 307200);
	 	//console.log("1.ext>>"+ext);
	 	if ($.inArray(ext, ['jpeg', 'jpg', 'png']) == -1 || input.files[0].size> 307200) {
	 		$('#serrorMsg' + id).html('<span style="font-size: small; color: #B64243">Invalid Format. Image must be 640px X 4900px(JPEG/JPG/PNG), Max 300KB</span>').show();
	 	} else {
	 		var image = new Image();
	 		if (input.files && input.files[0]) {
	 			var reader = new FileReader();
	 			reader.onload = function(e) {
	 				image.src = e.target.result;
	 				image.onload = function() {
	 					if (this.width > 640 || this.height > 490) {
	 						$('#serrorMsg' + id).html('<span style="font-size: small; color: #B64243">Invalid Format. Image must be 640px X 960px(JPEG/JPG/PNG), Max 300KB</span>').show();
	 					} else {
	 						$('#serrorMsg' + id).html('');
	 					}
	 				}
	 				$('#spreview' + id).attr('src', e.target.result);
	 				document.getElementById('spreview' + id).style.display = "block";	
	 			}
	 			reader.readAsDataURL(input.files[0]);
	 			document.getElementById('newsimg' + id).value = document.getElementById('rsimg' + id).value;
	 			 			

	 		}
	 	}
	 }

	 function lreadFile(id, input) {
	 	var ext = $('#rlimg' + id).val().split('.').pop().toLowerCase();
	 	if ($.inArray(ext, ['jpeg', 'jpg', 'png']) == -1 || input.files[0].size> 307200) {
	 		$('#lerrorMsg' + id).html('<span style="font-size: small; color: #B64243">Invalid Format. Image must be 640px X 960px(JPEG/JPG/PNG), Max 300KB</span>').show();
	 	} else {
	 		var image = new Image();
	 		if (input.files && input.files[0]) {
	 			var reader = new FileReader();
	 			reader.onload = function(e) {
	 				image.src = e.target.result;
	 				image.onload = function() {
	 					if (this.width > 640 || this.height > 960) {
	 				 		$('#lerrorMsg' + id).html('<span style="font-size: small; color: #B64243">Invalid Format. Image must be 640px X 960px(JPEG/JPG/PNG), Max 300KB</span>').show();
	 					} else {
	 						$('#lerrorMsg' + id).html('');
	 					}
	 				}
	 				$('#lpreview' + id).attr('src', e.target.result);
	 			}
	 			reader.readAsDataURL(input.files[0]);
	 			document.getElementById('newlimg' + id).value = document.getElementById('rlimg' + id).value;
	 		}
	 	}
	 }

	 function onClickRadio(item) {
	 		var radio = $("input[type='radio'][name='pushNotif" + item + "']:checked").val();
	 		if (radio == "1") {
	 		//	$('#pushNotifDiv' + item).show();
	 		 document.getElementById('pushNotifDiv' + item).style.visibility = "visible";
	 		} else if (radio == "0") {
	 			//$('#pushNotifDiv' + item).hide();
	 		 document.getElementById('pushNotifDiv' + item).style.visibility = "hidden";
	 		}
	 	}
	 	// Handle click on checkbox

	 function onChangeCheckbox(checkbox, no) {
	 	if (checkbox.checked) {
	 		document.getElementById("submitBtn").disabled = false;
	 	} else {
	 		error = "";
	 		$('#messages').hide();
	 	}
	 }
	 var errorNo = "";

	 function validateForm(num, mixId) {
    	var alls = mixId.split("-");
    	var item = alls[0];
    	if(alls.length>1){
    		var notifId = alls[1];
    	}
		
	 	if ($.trim($('#title' + item).val()) == '') {
	 		errorNo += "<p>Error : Item " + num + " : Title is required</p>";
	 	}
	 	if ($.trim(tinyMCE.get('description' + item).getContent({format : 'text'})) == '') {
	 		errorNo += "<p>Error : Item " + num + " : Description is required</p>";
	 	}else if(tinyMCE.get('description' + item).getContent({format : 'text'}).length >2000){
	 		errorNo += "<p>Error : Item " + num + " : Description must be less than 2000 characters.</p>";
	 	}
	 	
	 	if (moment($('#inendDate' + item).val(), "dd/MM/yyyy").isBefore(moment($('#instartDate' + item).val(), "dd/MM/yyyy"))) {
	 		errorNo += "<p>Error : Item " + num + " : Start date must ealier than end date</p>";
	 	} else if (moment($('#inendDate' + item).val(), "dd/MM/yyyy").isBefore(moment($('#currentDate' + item).val(), "dd/MM/yyyy"))) {
	 		errorNo += "<p>Error : Item " + num + " : End date must later than current date</p>";
	 	}
	 	if (moment($('#inendDate' + item).val(), "DD/MM/YYYY").isBefore(moment($('#inpublishDate' + item).val(), "DD/MM/YYYY"))) {
	 		errorNo += "<p>Error : Item " + num + " : Publish date must ealier than end date</p>";
	 	}
	 	//console.log('current >'+moment($('#newDate' + item).val(), "DD/MM/YYYY hh:mm a"));
	 	//console.log('mypushDate >'+moment($('#inpushDateTime' + item).val(), "DD/MM/YYYY hh:mm a"));
	 	//console.log('compare>'+moment($('#inpushDateTime' + item).val(), "DD/MM/YYYY hh:mm a").isBefore(moment($('#newDate' + item).val(), "DD/MM/YYYY hh:mm a")));
	 	if ($("input[type='radio'][name='pushNotif" + mixId + "']:checked").val() == 1 && (moment($('#inendDate' + item).val(), "DD/MM/YYYY").isBefore(moment($('#inpushDateTime' + mixId).val(), "DD/MM/YYYY")))) {
	 		errorNo += "<p>Error : Item " + num + " : Push notification date time must ealier than end date time</p>";
	 	}
	 	if ($("input[type='radio'][name='pushNotif" + mixId + "']:checked").val() == 1 && $.trim($('#text' + mixId).val()).length <= 0) {
	 		errorNo += "<p>Error : Item " + num + " : Push notification message is required</p>";
	 	}
	 	if ($("input[type='radio'][name='pushNotif" + mixId + "']:checked").val() == 1 && moment($('#inpushDateTime' + mixId).val(), "DD/MM/YYYY hh:mm a").isBefore(moment($('#newDate' + item).val(), "DD/MM/YYYY hh:mm a"))) {
 			errorNo += "<p>Error : Item " + num + " : Push notification date time must later than current date.</p>";
	 	} 
	 	if ($('#serrorMsg' + item).html() != "") {
	 		//console.log('#serrorMsg' + item);
	 		errorNo += "<p>Error : Item " + num + " : Invalid Format. Image must be 640px X 960px(JPEG/JPG/PNG), Max 300KB</p>";
	 	}
	 	/* if ($('#lerrorMsg' + item).html() != "") {
	 		errorNo += "<p>Error : Item " + num + " : Invalid Format. Image must be 640px X 960px(JPEG/JPG/PNG), Max 300KB</p>";
	 	} */
	 }
   

    $(document).ready(function() {
        $(function() {
            var today = new Date();
            var tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));
            $('#startDate').datetimepicker({
                format: 'DD/MM/YYYY',
                defaultDate: new Date(),
                ignoreReadonly: true
            });
            $('#endDate').datetimepicker({
                format: 'DD/MM/YYYY',
                defaultDate: moment().add(1, 'd').toDate(),
                ignoreReadonly: true
            });
            
        });
        
        
        
        if(${newslist!=null && newslist.size()>=0}){   
            $('#resultDiv').show();
           var table= $('#tblSearch').DataTable({
            //  responsive: true,   
            bFilter: false,
            bSort: false,
            lengthMenu: [[50, 100], [50, 100]]
            });
						
           <c:forEach var="item" items="${newslist}" varStatus="loop">
           $('#startDate'+${item.id}).datetimepicker({
               format: 'DD/MM/YYYY',
               defaultDate: new Date(),
               ignoreReadonly: true
           });
           $('#endDate'+${item.id}).datetimepicker({
               format: 'DD/MM/YYYY',
               ignoreReadonly: true
           
           });
           $('#publishDate'+${item.id}).datetimepicker({
               format: 'DD/MM/YYYY',
               defaultDate: new Date(),
               ignoreReadonly: true
           });
           
           <c:forEach var="tagItem" items="${item.myTagList}"  varStatus="loop2">
	           $('#pushDateTime'+${item.id}+'-'+${tagItem.notificationId}).datetimepicker({
	             format: 'DD/MM/YYYY hh:mm A',
	             defaultDate: new Date(),
	             ignoreReadonly: true
	         	}); 
	           
	           <c:if test="${tagItem.status == 'UNSENT'}">
		        	//$('#pushNotifDiv' + ${item.id}+'-'+${tagItem.notificationId}).show();
		        	$('#pushNotifDiv' + ${item.id}+'-'+${tagItem.notificationId}).css('visibility', 'visible');
				</c:if>	
           </c:forEach>
           
           
           $('#clear'+${item.id}).on("click", function(){
        	   document.getElementById('rsimg${item.id}').value = "";
        	   document.getElementById('newsimg${item.id}').value = "";
        	   document.getElementById('spreview${item.id}').src = '${pageContext.request.contextPath}/admin/getNewsImage/simg/${item.id}.do';   
        	   $('#serrorMsg'+${item.id}).html('');
           });
        	 
        	
			tinymce.init({
				selector : '#description'+${item.id},				
				theme : "modern",
				mode : "textareas",
				width :500,
				height :250,
				menubar : false,
				statusbar : true,
				plugins : "charactercount",		
				name :'description'+${item.id},
				setup : function(editor) {
				  if ($('#'+editor.id).prop('readonly')) {
				  	editor.settings.readonly = true;
        		}
					var maxlength = parseInt($('#' + (editor.id)).attr("maxlength"));
				
					editor.on('keyup', function(e) {
						// Revalidate the hobbies field
					});
					 editor.on('keydown', function(e) {
					    var charcnt = tinyMCE.editors[this.getParam('name')].plugins["charactercount"].getCount() >= maxlength;
					    if (charcnt && e.keyCode != 8 && e.keyCode != 46) {
					        return tinymce.dom.Event.cancel(e);
					    }
					});
					editor.on('change', function(e) {
					    var charcnt = tinyMCE.editors[this.getParam('name')].plugins["charactercount"].getCount() >= maxlength;
					    editor.save();
					    if (charcnt) {
					        return tinymce.dom.Event.cancel(e);
					    }	
					    editor.save();
					 });
				}, //setup
				init_instance_callback : function(editor) {
					$('.mce-tinymce').show('fast');
					$(editor.getContainer()).find(".mce-path").css("display", "none");
				}
			});
           
    	    </c:forEach>
        }
        
        $("button#add").click(function() {
         	$('#modal').modal('hide');
        	 var rows_selected = new Array();
         	 errorNo = "";
             var oTable = $('#tblSearch').dataTable();
             var rowcollection = oTable.$(".call-checkbox:checked", {
                 "page": "all"
             });  
             if (rowcollection.length == 0) {
             	window.scrollTo(0,0);
             	$('#errorMsg').html("<div id=\"errorMsg\" class=\"alert alert-danger\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span> <span class=\"sr-only\"></span> Please select the checkbox to update. </div>").show();
                 return false;
             }
             $('#errorMsg').hide();
             rowcollection.each(function(index, elem) {
            	// console.log("index>>"+index);
            	// console.log("elem>>"+elem);
                 var item = $(elem).val();
                 var rowIndex = $('#tblSearch').find('tbody tr:eq(' + item + ')').get(0);
                 var num = $('#total' + item.split("-")[0]).val();
	             rows_selected.push({
	                 id: item	                 
	        		})
	        		validateForm(num, item);
            })
        	
        	//console.log("errorNo.length="+errorNo.length);
            if(errorNo.length>0){                	
                 $('#messages').html(errorNo).show();
                 window.scrollTo(0,250);
             	return false;
            }
            //console.log("validate success:");
            var token = $("meta[name='_csrf']").attr("content");
            var header = $("meta[name='_csrf_header']").attr("content");
            
            $("#updateNewsForm").ajaxForm({
              	//dataType:"text",
            	beforeSend: function(xhr) {
            		xhr.setRequestHeader(header, token);
                	sebApp.showIndicator();
            	},            	
            	success: function(data) {
            	  if(data.success!=null && data.success.length>0){
                      $('#successMsg').html(data.success).show();
                  }
                  else if(data.error!=null && data.error.length>0){   
                      $('#errorMsg').html(data.error).show();
                  } 
                  
                  window.scrollTo(0, 5);
                  rows_selected = new Array();
                  $('#resultDiv').hide();
              },
        	    failure: function(data) {
                	alert("Fail To Connect");
             	},
             	complete: function(e){ sebApp.hideIndicator(); }        	     
        	   }).submit(); 
            
            return true;
        });
        
        $('#searchNewsForm').formValidation({
            framework: 'bootstrap',
            excluded : [ ':disabled' ],
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
                instartDate: {
                    validators: {
                        callback: {
                            callback: function(value, validator, $field) {                                
                                if (value=='') {
                                    $('#instartDate').val(moment(new Date()).format('DD/MM/YYYY'));
                                    return true;
                                }
                                return true;
                            }
                        },
                        format: 'DD/MM/YYYY'                        
                    }
                },
                inendDate: {
                    validators: {
                        notEmpty: {
                            message: 'End Date is required'
                        },
                        format: 'DD/MM/YYYY'
                    }
                },
                title: {
                    validators: {
                            stringLength: {
                            min: 1,
                            max: 100,
                            message: 'Title must less than 100 characters'
                        }
                    }
                }
            }
        });
    });
    </script>
</body>
</html>