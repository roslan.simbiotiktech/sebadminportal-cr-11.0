package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.seb.admin.portal.constant.ApplicationConstant;


/**
 * The persistent class for the PAYMENT_GATEWAY database table.
 * 
 */
@Entity
@Table(name="PAYMENT_GATEWAY"  , schema = ApplicationConstant.SCHEMA)
@NamedQueries({
	@NamedQuery(name="PaymentGateway.findAll", query="SELECT p FROM PaymentGateway p")
})

public class PaymentGateway implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GATEWAY_ID")
	private String gatewayId;

	@Column(name="MERCHANT_ID")
	private String merchantId;

	@Column(name="REFERENCE_PREFIX")
	private String referencePrefix;

	public PaymentGateway() {
	}

	public String getGatewayId() {
		return this.gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getMerchantId() {
		return this.merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getReferencePrefix() {
		return this.referencePrefix;
	}

	public void setReferencePrefix(String referencePrefix) {
		this.referencePrefix = referencePrefix;
	}

}