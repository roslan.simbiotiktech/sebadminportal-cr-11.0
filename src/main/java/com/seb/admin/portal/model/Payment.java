package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;

import com.seb.admin.portal.constant.ApplicationConstant;

import java.sql.Timestamp;


/**
 * The persistent class for the PAYMENT database table.
 * 
 */
@Entity
@Table(name="PAYMENT", schema = ApplicationConstant.SCHEMA_SPG)
@NamedQuery(name="Payment.findAll", query="SELECT p FROM Payment p")
public class Payment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PAYMENT_ID")
	private long paymentId;

	private double amount;

	@Column(name="CLIENT_RETURN_URL")
	private String clientReturnUrl;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Column(name="CREATED_DATETIME")
	private Timestamp createdDatetime;

	@Column(name="CUSTOMER_EMAIL")
	private String customerEmail;

	@Column(name="CUSTOMER_USERNAME")
	private String customerUsername;

	@Column(name="GATEWAY_AUTHORIZATION_DATETIME")
	private Timestamp gatewayAuthorizationDatetime;

	@Column(name="GATEWAY_ID")
	private String gatewayId;

	@Column(name="GATEWAY_STATUS_ID")
	private long gatewayStatusId;

	@Column(name="GATEWAY_TRANSACTION_ID")
	private String gatewayTransactionId;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Column(name="LAST_UPDATED_DATETIME")
	private Timestamp lastUpdatedDatetime;

	@Column(name="MERCHANT_CLIENT_STATUS_ID")
	private long merchantClientStatusId;

	@Column(name="MERCHANT_ID")
	private int merchantId;

	@Column(name="MERCHANT_S2S_STATUS_ID")
	private long merchantS2sStatusId;

	@Column(name="MERCHANT_TRANSACTION_ID")
	private String merchantTransactionId;

	@Column(name="PRODUCT_DESCRIPTION")
	private String productDescription;

	@Column(name="STATUS")
	private String status;

	public Payment() {
	}

	public long getPaymentId() {
		return this.paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getClientReturnUrl() {
		return this.clientReturnUrl;
	}

	public void setClientReturnUrl(String clientReturnUrl) {
		this.clientReturnUrl = clientReturnUrl;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Timestamp createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getCustomerEmail() {
		return this.customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerUsername() {
		return this.customerUsername;
	}

	public void setCustomerUsername(String customerUsername) {
		this.customerUsername = customerUsername;
	}

	public Timestamp getGatewayAuthorizationDatetime() {
		return this.gatewayAuthorizationDatetime;
	}

	public void setGatewayAuthorizationDatetime(Timestamp gatewayAuthorizationDatetime) {
		this.gatewayAuthorizationDatetime = gatewayAuthorizationDatetime;
	}

	public String getGatewayId() {
		return this.gatewayId;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public long getGatewayStatusId() {
		return this.gatewayStatusId;
	}

	public void setGatewayStatusId(long gatewayStatusId) {
		this.gatewayStatusId = gatewayStatusId;
	}

	public String getGatewayTransactionId() {
		return this.gatewayTransactionId;
	}

	public void setGatewayTransactionId(String gatewayTransactionId) {
		this.gatewayTransactionId = gatewayTransactionId;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Timestamp getLastUpdatedDatetime() {
		return this.lastUpdatedDatetime;
	}

	public void setLastUpdatedDatetime(Timestamp lastUpdatedDatetime) {
		this.lastUpdatedDatetime = lastUpdatedDatetime;
	}

	public long getMerchantClientStatusId() {
		return this.merchantClientStatusId;
	}

	public void setMerchantClientStatusId(long merchantClientStatusId) {
		this.merchantClientStatusId = merchantClientStatusId;
	}

	public int getMerchantId() {
		return this.merchantId;
	}

	public void setMerchantId(int merchantId) {
		this.merchantId = merchantId;
	}

	public long getMerchantS2sStatusId() {
		return this.merchantS2sStatusId;
	}

	public void setMerchantS2sStatusId(long merchantS2sStatusId) {
		this.merchantS2sStatusId = merchantS2sStatusId;
	}

	public String getMerchantTransactionId() {
		return this.merchantTransactionId;
	}

	public void setMerchantTransactionId(String merchantTransactionId) {
		this.merchantTransactionId = merchantTransactionId;
	}

	public String getProductDescription() {
		return this.productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}