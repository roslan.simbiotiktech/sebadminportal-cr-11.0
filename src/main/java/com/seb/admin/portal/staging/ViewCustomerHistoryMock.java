package com.seb.admin.portal.staging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.seb.admin.portal.adapter.Contract;
import com.seb.admin.portal.adapter.ContractDetail;
import com.seb.admin.portal.adapter.CustAuditTrail;
import com.seb.admin.portal.adapter.CustomerHistoryResponse;
import com.seb.admin.portal.adapter.USER;
import com.seb.admin.portal.adapter.ViewProfileResponse;

public class ViewCustomerHistoryMock {


	public static void main(String[] args) {

		System.out.println("[CMS View Customer History]");
		String serviceURL = "http://1.32.122.125:8080/SEB-Middleware/api/cms_view_customer_history";  
		RestTemplate restTemplate = new RestTemplate();  
		restTemplate.getMessageConverters().add( new MappingJackson2HttpMessageConverter() );      


		HttpHeaders headers = new HttpHeaders();  
		headers.setContentType(MediaType.APPLICATION_JSON);  
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));


		try { 

			System.out.println("Calling");

			Map objMap =new LinkedHashMap();
			Map pagingObj =new LinkedHashMap();

			pagingObj.put("PAGE", 1);
			pagingObj.put("RECORDS_PER_PAGE", 50);
			objMap.put("PAGING", pagingObj);
			objMap.put("DATE_FROM", "05/11/2015 00:00:00");
			objMap.put("DATE_TO", "05/12/2015 23:59:59");
			objMap.put("LOGIN_ID", "");//woon.san.yap@isentric.com
			objMap.put("MOBILE_NUMBER", "");
			objMap.put("CONTRACT_ACC_NO", "");//201057928110
			String jsonText = JSONValue.toJSONString(objMap);
			System.out.println(jsonText);


			HttpEntity<String> entity = new HttpEntity<String>(jsonText,headers);
			ResponseEntity<String> result = restTemplate.exchange(serviceURL, HttpMethod.POST, entity, String.class);
			System.out.println(result.getBody());

			long RESP_STAT = -1;
			long MAX_PAGE = 1;
			long TOTAL_RECORDS = 1;
			long PAGE = 1;
			String ERR_CODE = "";
			StringBuilder ERR_DESC = new StringBuilder(""); 
			CustomerHistoryResponse resp = new CustomerHistoryResponse();
			List<CustAuditTrail> auditList = new ArrayList<CustAuditTrail>();

			if(result.getStatusCode() == HttpStatus.OK){
				JSONParser parser = new JSONParser();
				try{
					Object obj = parser.parse(result.getBody());
					JSONObject jsonObject = (JSONObject) obj;

					// handle a structure into the json object
					JSONObject subObject = (JSONObject) jsonObject.get("STATUS");
					RESP_STAT = (Long) subObject.get("RESP_STAT");

					if(RESP_STAT!=0){
						JSONObject errorObj = (JSONObject) subObject.get("ERROR");	
						ERR_CODE = (String) errorObj.get("ERR_CODE");

						JSONArray errorList = (JSONArray) errorObj.get("ERR_DESC");
						Iterator<String> iterator = errorList.iterator();
						while (iterator.hasNext()) {
							ERR_DESC.append(iterator.next()+System.getProperty("line.separator"));
						}
					}else{// callback:sucess
						JSONObject paginationObj = (JSONObject) jsonObject.get("PAGINATION");
						MAX_PAGE = (Long) paginationObj.get("MAX_PAGE");
						resp.setMaxPage(String.valueOf((Long) paginationObj.get("MAX_PAGE")));
						TOTAL_RECORDS = (Long) paginationObj.get("TOTAL_RECORDS");
						resp.setTotalRecords(String.valueOf((Long) paginationObj.get("TOTAL_RECORDS")));

						JSONObject jsonPagingObj = (JSONObject) paginationObj.get("PAGING");	
						PAGE = (Long) jsonPagingObj.get("PAGE");
						resp.setPage(String.valueOf((Long) paginationObj.get("PAGE")));

						JSONArray auditObj = (JSONArray) jsonObject.get("AUDIT_TRAILS");	

						if(auditObj!=null){
							Iterator au = auditObj.iterator();							
							while (au.hasNext()) {
								CustAuditTrail audit = new CustAuditTrail();
								JSONObject innerAuditObj = (JSONObject) au.next();

								audit.setActivityAt((String) innerAuditObj.get("ACITIVTY_AT"));
								audit.setLoginId((String) innerAuditObj.get("LOGIN_ID"));
								audit.setMobileNumber((String) innerAuditObj.get("MOBILE_NUMBER"));			
								audit.setActivity((String) innerAuditObj.get("ACTIVITY"));


								JSONArray contractObject = (JSONArray) innerAuditObj.get("CONTRACT_SUBSCRIBED");	
								if(contractObject!=null){
									Iterator c = contractObject.iterator();
									List<Contract> contractList = new ArrayList<Contract>();
									while (c.hasNext()) {
										Contract contract = new Contract();
										JSONObject innerContractObj = (JSONObject) c.next();
										contract.setAccountName((String) innerContractObj.get("ACCOUNT_NAME"));
										contract.setAccountNumber((String) innerContractObj.get("ACCOUNT_NUMBER"));
										contract.setSubscriptionStatus((String) innerContractObj.get("SUBSCRIPTION_STATUS"));
										contractList.add(contract);
									}//end loop contractList
									audit.setContractSubscribed(contractList);
								}

								auditList.add(audit);
							}//end loop audit trail list						
						}//(auditObj !=null)

					}
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			resp.setRespStat(RESP_STAT);
			resp.setErrCode(ERR_CODE);
			resp.setErrDesc(ERR_DESC.toString());
			resp.setCustATList(auditList);
			/*System.out.println(result.getBody());  
		System.out.println(result.getStatusCode());*/

			///listing all records
			int count = 0;
			/*for(CustAuditTrail c : resp.getCustATList())
			{
				System.out.println("loginId:"+c.getLoginId());
				System.out.println("mobileNumber:"+c.getMobileNumber());
				System.out.println("activity:"+c.getActivity());
				System.out.println("activityAt:"+c.getActivityAt());
				if(c.getContractSubscribed()!=null){
					System.out.println("contract subscribe:"+c.getContractSubscribed().toString());
				}else{
					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@["+count+"]"+c.getLoginId()+"> no contract account");
				}
				count ++;

			}*/

		} catch (RestClientException re) {  
			System.out.println("Re");
			re.printStackTrace();
			if (re instanceof HttpClientErrorException) {  
				HttpClientErrorException he = (HttpClientErrorException) re;  
				System.out.println(he.getResponseBodyAsString());  
				System.out.println(he.getStatusCode());  
			} else if (re instanceof HttpServerErrorException) {  
				HttpServerErrorException he = (HttpServerErrorException) re;  
				System.out.println(he.getResponseBodyAsString());  
				System.out.println(he.getStatusCode());  
			}   
		} catch (Exception e) {  
			e.printStackTrace();  
		}  



	}
}
