<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<meta content="" name="description">
<meta content="${_csrf.token}" name="_csrf">
<meta content="${_csrf.headerName}" name="_csrf_header">
      <!DOCTYPE html>

      <html lang="en">

      <head>
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <%@ include file="../menuTitle.jsp" %>
          <%@ include file="../global.jsp" %>
      </head>
	<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>
		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Maintenance &gt; Tariff Calculator &gt; Tariff Settings</div>
						<div class="panel-body">
                            <form id="tariffSearchForm" name="tariffSearchForm" action="${pageContext.request.contextPath}/admin/tariffSettings.do" class="form-horizontal" method="post" >
								${successMsg}${errorMsg}
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Tariff Category <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
									 <select class="form-control capital" id="selectCategory" name="selectCategory">
    									<option value="">Please Select</option>
    									<c:forEach items="${categoryLs}" var="item"> 
									       	<option value="${item.id}" ${selectCategory == item.id ? 'selected' : ' '}><c:out value="${item.description}" ></c:out></option>   
									    </c:forEach>                     
									</select>
									</div>
									<div class="form-group paddingTopCls10">
									<div class="col-lg-4 col-lg-offset-2 text-right">
									 	<input id="action" name="action" type="hidden" value="search"> 
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
								</div>
							
								</form>
								<form id="tariffSettingForm" name="tariffSettingsForm" action="${pageContext.request.contextPath}/admin/tariffSettings.do" class="form-horizontal" method="post">
								<div id="tariffBodyDiv" class="paddingTopCls10 ${show ? '' : 'hide'}">
									<h4>
										<div id="resultInfoDiv">Display Settings for <label class="control-label">${displayCategory}</label></div>
									
									</h4>
								<div id="tariffRateDiv" style="display:${(categoryName == 'COMMERCIAL_C3' || categoryName == 'INDUSTRIAL_I3') ? 'none' : ''}">
								<div class="form-group">
									<p class="col-sm-2 control-label leftCls">
										Tariff Rate <span style="color: red;">*</span> :
									</p>
									<p class="col-sm-2 control-label leftCls">
										Unit per Month (From)
									</p>
									<p class="col-sm-2 control-label leftCls">
										Unit per Month (To)
									</p>
									<p class="col-sm-2 control-label leftCls">
										Sen
									</p>
								</div>	
								<div class="form-group">
									<div class="col-xs-2">
							        </div>
									<div class="col-xs-2">
            							<input type="text"   pattern="^[0-9]" class="form-control" id="from0" name="rate[0].from" placeholder="Unit per Month(From)" value="${myResult[0].trFromUnit}"/>
							        </div>
							        <div class="col-xs-2">
							            <input type="text" class="form-control" id="to0" name="rate[0].to" placeholder="Unit per Month(To)" value="${myResult[0].trToUnit}"/>
							        </div>
							        <div class="col-xs-2">
							            <input type="text" class="form-control" id="price0" name="rate[0].price" placeholder="Sen" value="<fmt:formatNumber value="${myResult[0].trAmountPerUnit}" maxFractionDigits="2"/>"/>
							        </div>
							        <div class="col-xs-1">
							            <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
							        </div>
								</div>
								 
								 <!-- The template for adding new field -->
								 <div class="form-group hide" id="rateTemplate">
									<div class="col-xs-2">
							        </div>
									<div class="col-xs-2">
            							<input type="text" class="form-control" id="from" name="from" placeholder="Unit per Month(From)" />
							        </div>
							        <div class="col-xs-2">
							            <input type="text" class="form-control" id="to" name="to" placeholder="Unit per Month(To)" />
							        </div>
							        <div class="col-xs-2">
							            <input type="text" class="form-control" id="price" name="price" placeholder="Sen" />
							        </div>
							        <div class="col-xs-1">
							            <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
							        </div>
								</div>
								 <div class="form-group">
									<div class="col-xs-2">
							        </div>
									<p class="col-sm-2 control-label rightCls">
										(more than) >
									</p>
							        <div class="col-xs-2">
							            <input type="text" class="form-control" name="rateMoreUnit" placeholder="Unit per Month" value="${myResult[0].trMoreUnit}"/>
							        </div>
							        <div class="col-xs-2">
							        	<input type="text" class="form-control" name="rateMorePrice" placeholder="Sen" value ="<fmt:formatNumber value="${myResult[0].trMoreAmount}" maxFractionDigits="2"/>"/>
							        </div>
							        <div class="col-xs-1">
							        </div>
								</div>
								</div><!-- tariffRateDiv -->
								<!-- Exempt GST -->
								<div class="form-group paddingTopCls3">
									<p class="col-sm-2 control-label leftCls">
										Exempt GST <span style="color: red; display:${(categoryName == 'DOMESTIC') ? '' : 'none'}" >*</span> :
									</p>
									<p class="col-sm-1 control-label leftCls">First</p>
									<div class="col-xs-2">
										<input type="text" class="form-control" name="exemptGst" placeholder="" value="${myResult[0].exemptedGstFirstKwh}" />
									</div>
									<p class="col-sm-1 control-label leftCls">kwh</p>
								</div>
								
								<!-- kw of Maximum Demand per month -->
								<div class="form-group">
									<p class="col-sm-2 control-label leftCls">
										kw of Maximum Demand :
									</p>
									<p class="col-sm-1 control-label leftCls">RM</p>
									<div class="col-xs-2">
										<input type="text" class="form-control" name="maxDemand" placeholder="" value="<fmt:formatNumber value="${myResult[0].kwMaxDemandAmount}" minFractionDigits="0" maxFractionDigits="2"/>"/>
									</div>
									<p class="col-sm-6 control-label leftCls">x Billing Demamd (kw)</p>
								</div>
								
								<!-- kw of Maximum Demand per month -->
								<div class="form-group">
									<p class="col-sm-2 control-label leftCls">
										Minimum Monthly Charge (RM) :
									</p>
									<p class="col-sm-1 control-label leftCls">RM</p>
									<div class="col-xs-2">
										<input type="text" class="form-control" name="minMonthlyCharge" placeholder="" value="<fmt:formatNumber value="${myResult[0].minMonthlyChargeAmount}" minFractionDigits="0" maxFractionDigits="2"/>"/>
									</div>
									<p class="col-sm-6 control-label leftCls">x Billing Demamd (kw)</p>
								</div>
								
								<!-- lower power factor	 -->
								<div class="form-group">
									<p class="col-sm-2 control-label leftCls">
										Low Power Factor :
									</p>
									<div class="col-xs-2">
										<div class="checkbox">
  											 <label><input type="checkbox" value="1" name="lowPowerFactor"  ${myResult[0].lowPowerFactor == '1' ? 'checked' : ''}></label>
										</div>
									</div>
								</div>
								
								<!-- peak/Non peak	 -->
								<div class="form-group">
									<p class="col-sm-2 control-label leftCls">
										Peak / Non Peak <span style="color: red; display:${(categoryName == 'COMMERCIAL_C3' ||categoryName == 'INDUSTRIAL_I3' ) ? '' : 'none'}" >*</span> :
									</p>
									<div class="col-xs-2">
										<div class="checkbox">
  											 <label><input type="checkbox" value="1" id="peak" name="peak" ${myResult[0].peak == '1' ? 'checked' : ''}></label>
										</div>
									</div>
								</div>
								<div id="peakDiv" style="display:${myResult[0].peak == '1' ? 'block' : 'none'}">
									<div class="form-group">
										<p class="col-sm-2 control-label leftCls">
										</p>
										<p class="col-sm-1 control-label leftCls">Peak <span style="color: red;">*</span> </p>
										<div class="col-xs-2">
											<input type="text" class="form-control" name="peakAmount" placeholder="" value="<fmt:formatNumber value="${myResult[0].peakAmount}" maxFractionDigits="2"/>" />
										</div>
										<p class="col-sm-1 control-label leftCls">Sen</p>
									</div>
									<div class="form-group">
										<p class="col-sm-2 control-label leftCls">
										</p>
										<p class="col-sm-1 control-label leftCls">Non Peak <span style="color: red;">*</span> </p>
										<div class="col-xs-2">
											<input type="text" class="form-control" name="nonPeakAmount" placeholder="" value="<fmt:formatNumber value="${myResult[0].nonPeakAmount}" maxFractionDigits="2"/>" />
										</div>
										<p class="col-sm-1 control-label leftCls">Sen</p>
									</div>
								</div>
								
								<div class="form-group paddingTopCls10">
									<div class="col-lg-4 col-lg-offset-2 text-right">
										<input name="action" type="hidden" value="update">
										<input id="selectCategory" name="selectCategory" type="hidden" value="${selectCategory}"> 
										<input id="categoryName" name="categoryName" type="hidden" value="${categoryName}"> 
									 	<input id="displayCategory" name="displayCategory" type="hidden" value="${displayCategory}"> 
									 	<input id="totalMyResult" name="totalMyResult" type="hidden" value="${fn:length(myResult)}"> 
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal" id="updateBtn">Update</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
								</div><!-- tariffBodyDiv -->
								
								<!-- Add Confirmation Modal -->
								<div class="modal fade" id="modal" tabindex="-1">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button class="close" data-dismiss="modal" type="button">
													<span>&times;</span><span class="sr-only">Close</span>
												</button>
												<h2 class="modal-title">Confirmation</h2>
											</div>
											<div class="modal-body">Are you sure you want to add/update Tariff Settings?</div>
											<div class="modal-footer">
												<button class="btn btn-primary" id="add" type="submit">Add</button>
												<button class="btn btn-warning" data-dismiss="modal" type="button">Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- End Confirmation Modal -->
							</form>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
	function resetForm() {
	    $('#').find('.has-error').removeClass("has-error");
	    $('#tariffSettingForm').find('.has-success').removeClass("has-success");
	    $('#tariffSettingForm').find('.form-control-feedback').removeClass('glyphicon-remove');
	    $('#tariffSettingForm').find('.form-control-feedback').removeClass('glyphicon-ok');
	    $('.form-group').find('small.help-block').hide();
	    $('.form-group').removeClass('has-error has-feedback');
	    $('.form-group').find('i.form-control-feedback').hide();
	    $('#tariffSettingForm').formValidation('resetForm', true);
	    
	    return false;
	}
	
	
	$(document).ready(function() {
			var MAX = 5;
			var domestic = "DOMESTIC";
			rateIndex = 0;
			var arrLength = $('#totalMyResult').val();
			console.log("arrLength>"+arrLength);
			
			if(arrLength>0){

				<c:forEach items="${myResult}" var="item" begin="1" varStatus="loop">
				 rateIndex++;
					var $template = $('#rateTemplate'),
	                $clone    = $template
	                                .clone()
	                                .removeClass('hide')
	                                .removeAttr('id')
	                                .attr('data-rate-index', rateIndex)
	                                .insertBefore($template);
	
	            	// Update the name attributes
	            	 $clone
		                .find('[name="from"]').attr('name', 'rate[' + rateIndex + '].from').end()
		                .find('[name="to"]').attr('name', 'rate[' + rateIndex + '].to').end()
		                .find('[name="price"]').attr('name', 'rate[' + rateIndex + '].price').end()
		            	
		                .find('[id="from"]').attr('id', 'from'+rateIndex).end()
		                .find('[id="to"]').attr('id', 'to'+rateIndex).end()
		                .find('[id="price"]').attr('id', 'price'+rateIndex).end();
	            	
	            	
	            	$('#from'+rateIndex).val(${item.trFromUnit});
	            	$('#to'+rateIndex).val(${item.trToUnit});
	            	$('#price'+rateIndex).val(${item.trAmountPerUnit});
	            	
				</c:forEach>
	            
			}
			
			
		
		var fromValidators = {
	            row: '.col-xs-2',   // The title is placed inside a <div class="col-xs-4"> element
	            validators: {
	                notEmpty: {
	                    message: 'Unit per Month (From) is required'
	                },
	                integer: {
                        message: 'Unit per Month (From) not an integer'
                    }
	            }
	        },
	        toValidators = {
	            row: '.col-xs-2',
	            validators: {
	                notEmpty: {
	                    message: 'Unit per Month (To) is required'
	                },
	                integer: {
                        message: 'Unit per Month (To) not an integer'
                    }
	            }
	        },
	        priceValidators = {
	            row: '.col-xs-2',
	            validators: {
	                notEmpty: {
	                    message: 'Sen is required'
	                },
	                numeric: {
	                    message: 'Invalid price'
	                }
	            }
	        }
	        
		/* if($('#selectCategory').val()!== ''){
			$("#tariffBodyDiv").removeClass("hide");
		} */
		
		
		 $('#selectCategory').change(function() {
			 $("#tariffBodyDiv").addClass("hide");
		        var name = $("#selectCategory option:selected").text();
		        console.log("tariffBodyDiv>>"+name);
		        if (name != "Please Select") {
		        	 $('#resultInfoDiv').html('Display Settings for <label class="control-label">'+name+'</label>');
		        	 //$("#tariffBodyDiv").removeClass("hide");
		        	 $('#displayCategory').val(name);
		        }else{
		        	 $('#displayCategory').val('');
		        	 $("#tariffBodyDiv").addClass("hide");
		        }
		 })
		 
		$('#peak').on('change', function() { 
    		// From the other examples
    		if (!this.checked) {
    			console.log("uncheck");
    			 $("#peakDiv").hide();
    		}
    		else{
    			console.log("check");
    			$("#peakDiv").show();
    		}
		});
		 
		
		 $('#tariffSearchForm').formValidation({
			framework: 'bootstrap',
			icon: {
				invalid: 'glyphicon glyphicon-remove',
			},
			fields: {
				selectCategory: {
    				validators: {
    					callback: {
    						message: 'Please select Tariff Category',
    						callback: function(value, validator, $field) {
    							var options = validator.getFieldElements('selectCategory').val();
    							return (options != null && options.length > 0);
    						}
    					}
    				}
    			}
			}
		}).on('success.form.fv', function(e) {
			$("#tariffBodyDiv").removeClass("hide");
		});
		 
		 
		 $('#updateBtn').on('click', function() {
	    	  if($("#categoryName").val() == domestic){
					console.log(">>>>>>>>>>>>>>>>>>>>>>"+$("#categoryName").val());
					$('#tariffSettingForm').formValidation('enableFieldValidators', 'exemptGst', true);
			}
	    	  if($("#categoryName").val() == 'COMMERCIAL_C3' ||$("#categoryName").val() == 'INDUSTRIAL_I3'){
	    			$('#tariffSettingForm').formValidation('enableFieldValidators', 'peak', true);
	    	  }
	      });
		 
		
		$('#tariffSettingForm')
        .formValidation({
            framework: 'bootstrap',
            icon : {
				invalid : 'glyphicon glyphicon-remove',
			},
            fields: {
                'rate[0].from': fromValidators,
                'rate[0].to': toValidators,
                'rate[0].price': priceValidators,
                selectCategory: {
    				validators: {
    					callback: {
    						message: 'Please select Tariff Category',
    						callback: function(value, validator, $field) {
    							var options = validator.getFieldElements('selectCategory').val();
    							return (options != null && options.length > 0);
    						}
    					}
    				}
    			},
    			rateMoreUnit: {
                    validators: {
                        notEmpty: {
                            message: '(More Than)>  is required'
                        },
                        integer: {
                            message: '(More Than)> not an integer'
                        }
                    }
                },
                rateMorePrice: {
                    validators: {
                        notEmpty: {
                            message: '(More Than)> Sen is required'
                        },
                        numeric: {
    	                    message: 'Invalid price'
    	                }
                    }
                },
                exemptGst: {
                	enabled: false,
                    validators: {
                        notEmpty: {
                            message: 'Exempt GST is required'
                        },
                        numeric: {
    	                    message: 'Exempt GST not an integer'
    	                }
                    }
                },
                maxDemand: {
                    validators: {
                        numeric: {
    	                    message: 'Invalid Price'
    	                }
                    }
                },
                minMonthlyCharge: {
                    validators: {
                        numeric: {
    	                    message: 'Invalid Price'
    	                }
                    }
                },
                peak: {
                	enabled: false,
                    validators: {
                        notEmpty: {
                            message: 'Peak is required'
                        },
                    }
                },
                peakAmount: {
                    validators: {
                        callback: {
                            callback: function(value, validator, $field) {
                            	 var channel = $('#tariffSettingForm').find('[name="peak"]:checked').val();
                            	 var reg = new RegExp('^[1-9]\d*(\.\d+)?$');
                                if (channel == 1 && $.trim(value) == "" ) {
                                  return{
                                        valid: false,
                                        message: 'Peak Amount is requred'
                                    }
                                }/*  else  if (channel == 1 && $.trim(value) != "" && reg.test(value) ) {
                                    return {
                                        valid: false,
                                        message: 'Invalid Price'
                                    }
                                }  */else {
                                    return true;
                                }
                            }
                        },
                        numeric: {
    	                    message: 'Invalid Price'
    	                }
                    }
                },
                nonPeakAmount: {
                    validators: {
                        callback: {
                            callback: function(value, validator, $field) {
                            	 var channel = $('#tariffSettingForm').find('[name="peak"]:checked').val();
                            	 var reg = new RegExp('^[1-9]\d*(\.\d+)?$');
                                if (channel == 1 && $.trim(value) == "" ) {
                                  return{
                                        valid: false,
                                        message: 'Peak Amount is requred'
                                    }
                                }/*  else  if (channel == 1 && $.trim(value) != "" && reg.test(value) ) {
                                    return {
                                        valid: false,
                                        message: 'Invalid Price'
                                    }
                                }  */else {
                                    return true;
                                }
                            }
                        },
                        numeric: {
    	                    message: 'Invalid Price'
    	                }
                    }
                }
            }
        }).on('click', '.addButton', function() {
			 rateIndex++;
		            var $template = $('#rateTemplate'),
		                $clone    = $template
		                                .clone()
		                                .removeClass('hide')
		                                .removeAttr('id')
		                                .attr('data-rate-index', rateIndex)
		                                .insertBefore($template);

		            // Update the name attributes
		            $clone
		                .find('[name="from"]').attr('name', 'rate[' + rateIndex + '].from').end()
		                .find('[name="to"]').attr('name', 'rate[' + rateIndex + '].to').end()
		                .find('[name="price"]').attr('name', 'rate[' + rateIndex + '].price').end()
		            	
		                .find('[id="from"]').attr('id', 'from'+rateIndex).end()
		                .find('[id="to"]').attr('id', 'to'+rateIndex).end()
		                .find('[id="price"]').attr('id', 'price'+rateIndex).end();
		            
		           
		            $('#tariffSettingForm')
	                .formValidation('addField', 'rate[' + rateIndex + '].from', fromValidators)
	                .formValidation('addField', 'rate[' + rateIndex + '].to', toValidators)
	                .formValidation('addField', 'rate[' + rateIndex + '].price', priceValidators);
		})
		 
		 // Remove button click handler
	     .on('click', '.removeButton', function() {
	            var $row  = $(this).parents('.form-group'),
	                index = $row.attr('data-rate-index');
	           

	            // Remove fields
	            $('#tariffSettingForm')
		            .formValidation('addField', 'rate[' + rateIndex + '].from', fromValidators)
	                .formValidation('addField', 'rate[' + rateIndex + '].to', toValidators)
	                .formValidation('addField', 'rate[' + rateIndex + '].price', priceValidators);
	            // Remove element containing the fields
	            $row.remove();
	            $('#tariffSettingForm').find('.addButton').removeAttr('disabled');
	            console.log("remove>"+rateIndex);
	        })
	        
	        .on('success.form.fv', function(e) {
	        	 $("#tariffBodyDiv").addClass("hide");
		  });// Enable the password/confirm password validators if the password is not empty
		  
		 
	})
	
    </script>
</body>
</html>