package com.seb.admin.portal.spg.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.seb.admin.portal.spg.entity.OMerchantBase;
import com.seb.admin.portal.spg.entity.ResponseStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EditMerchantResponse 
{
	@JsonProperty("status")
	private ResponseStatus responseStatus;
	@JsonProperty("merchants")
	private List<OMerchantBase> merchant;
	
	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}
	public List<OMerchantBase> getMerchant() {
		return merchant;
	}
	public void setMerchant(List<OMerchantBase> merchant) {
		this.merchant = merchant;
	}
	
}