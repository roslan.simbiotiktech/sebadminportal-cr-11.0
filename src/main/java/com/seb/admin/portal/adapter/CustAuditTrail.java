package com.seb.admin.portal.adapter;

import java.util.List;

public class CustAuditTrail {
	
	private String loginId;
	private String mobileNumber;
	private String activity;
	private String activityAt;
	private List <Contract> contractSubscribed ;
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getActivityAt() {
		return activityAt;
	}
	public void setActivityAt(String activityAt) {
		this.activityAt = activityAt;
	}
	public List<Contract> getContractSubscribed() {
		return contractSubscribed;
	}
	public void setContractSubscribed(List<Contract> contractSubscribed) {
		this.contractSubscribed = contractSubscribed;
	}
	@Override
	public String toString() {
		return "CustAuditTrail [loginId=" + loginId + ", mobileNumber=" + mobileNumber + ", activity=" + activity
				+ ", activityAt=" + activityAt + ", contractSubscribed=" + contractSubscribed + "]";
	}
	
	

}
