package com.seb.admin.portal.dao;

import java.util.List;

import com.seb.admin.portal.model.ReportRemarksHistory;

public interface ReportRemarksHistoryDAO {

	public List<ReportRemarksHistory> findAllMakeReportByCondition(String transID);
	
}
