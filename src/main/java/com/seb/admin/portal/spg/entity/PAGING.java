package com.seb.admin.portal.spg.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PAGING {
	@JsonProperty("page")
	private int page;
	
	@JsonProperty("records_per_page")
	private int records_per_page = 50;


	public PAGING() {
	}
	
	public PAGING(int page) {
		super();
		this.page = page;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords_per_page() {
		return records_per_page;
	}

	public void setRecords_per_page(int records_per_page) {
		this.records_per_page = records_per_page;
	}

	@Override
	public String toString() {
		return "page:[" + page + "], records_per_page:[" + records_per_page + "]";
	}
	
	
}
