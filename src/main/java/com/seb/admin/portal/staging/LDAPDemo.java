package com.seb.admin.portal.staging;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import com.novell.ldap.LDAPConnection;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPReferralException;
import com.novell.ldap.LDAPSearchResults;
import com.seb.admin.portal.model.AdminUser;

public class LDAPDemo 
{
	/*public static final String LDAP_HOST = "localhost";
	public static final int LDAP_PORT = 3890;
	public static final int LDAP_SEARCH_SCOPE = LDAPConnection.SCOPE_SUB;
	public static final int LDAP_VERSION = LDAPConnection.LDAP_V3;
	public static final String LDAP_DN = "cmareader";
	public static final String LDAP_PASSWORD = "CM@@2015";

	public static final String ENCODING_UTF8 = "UTF8";
	public static final String LDAP_SEARCH_BASE = "DC=sarawakenergy,DC=com,DC=my";
	public static final String LDAP_CONTAINER_NAME = "DC=sarawakenergy,DC=com,DC=my";
	public static final String LDAP_SEARCH_CRITERIA_1 = "sAMAccountName";
	public static final String LDAP_SEARCH_CRITERIA_2 = "userPassword";*/

	public static final String LDAP_HOST = "localhost";
	public static final int LDAP_PORT = 3890;
	public static final int LDAP_SEARCH_SCOPE = LDAPConnection.SCOPE_SUB;
	public static final int LDAP_VERSION = LDAPConnection.LDAP_V3;
	public static final String LDAP_DN = "cmareader";
	public static final String LDAP_PASSWORD = "CM@@2015";

	public static final String ENCODING_UTF8 = "UTF8";
	public static final String LDAP_SEARCH_BASE = "DC=sarawakenergy,DC=com,DC=my";
	public static final String LDAP_CONTAINER_NAME = "DC=sarawakenergy,DC=com,DC=my";
	public static final String LDAP_SEARCH_CRITERIA_1 = "sAMAccountName";
	public static final String LDAP_SEARCH_CRITERIA_2 = "userPassword";


	public LDAPDemo()
	{

	}

	//searchResults = lc.search( searchBase, searchScope, searchFilter, attributesToReturn, attributesOnlyFlag );

	/**
	 * createConnection = bind to AD
	 * @return
	 * date added: 10th April 2009
	 */
	private LDAPConnection createConnection()
	{
		LDAPConnection lc = null;

		try
		{
			lc = new LDAPConnection();
			lc.connect(LDAP_HOST, LDAP_PORT);
			lc.bind(LDAP_VERSION, LDAP_DN, LDAP_PASSWORD.getBytes(ENCODING_UTF8));

			System.out.println("createConnection() LDAP Connect = " + lc.isConnected());
		}
		catch(Exception e)
		{
			System.out.println( "createConnection() Error: " + e.toString() );
			e.printStackTrace();
		}
		finally
		{
			return lc;
		}

	}

	/**
	 * 
	 * @return get LDAP connection using JNDI
	 */
	public LdapContext getLdapContext(){
		LdapContext ctx = null;
		try{
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY,
					"com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.SECURITY_AUTHENTICATION, "Simple");
			env.put(Context.SECURITY_PRINCIPAL, LDAP_DN);
			env.put(Context.SECURITY_CREDENTIALS, LDAP_PASSWORD);
			env.put(Context.PROVIDER_URL, "ldap://"+LDAP_HOST+":"+LDAP_PORT);
			ctx = new InitialLdapContext(env, null);
			System.out.println("Connection Successful.");
		}catch(NamingException nex){
			System.out.println("LDAP Connection: FAILED");
			nex.printStackTrace();
		}
		return ctx;
	}

	/**
	 * get all attributes
	 * @param loginId
	 * @param ctx
	 * @return
	 */
	private String getUserBasicAttributes(String criteria, String loginId, LdapContext ctx) {
		String search ="";
		try {			
			if(criteria.equalsIgnoreCase("mail")){
				search = "mail";
			}else{
				search = "distinguishedName";
			}

			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String[] attrIDs = {search,"userPrincipalName"};
			constraints.setReturningAttributes(attrIDs);
			//First input parameter is search bas, it can be "CN=Users,DC=YourDomain,DC=com"
			//Second Attribute can be uid=username
			NamingEnumeration answer = ctx.search(LDAP_SEARCH_BASE, "sAMAccountName="
					+ loginId, constraints);
			if (answer.hasMore()) {
				System.out.println("criteria>>"+criteria+",search>>"+search);
				Attributes attrs = ((SearchResult) answer.next()).getAttributes();				
				if(!criteria.equalsIgnoreCase("mail")){
					String distinguishedName = attrs.get("distinguishedName").toString();
					System.out.println(">>"+distinguishedName);
					String splitName [] = distinguishedName.split(",");
					search = splitName[1].replaceAll("OU=", "").trim();
					System.out.println("department>>"+search);
				} else{
					if(attrs.get("mail")!=null){
						search = attrs.get("mail").toString();						
						search = search.replaceAll("mail:", "").trim();
						System.out.println("mail>>"+search);
					}else if(attrs.get("userPrincipalName")!=null){
						search = attrs.get("userPrincipalName").toString();
						search = search.replaceAll("userPrincipalName:", "").trim();
						System.out.println("userPrincipalName>"+search);
					}else{
						search ="";
					}
				}
			}else{
				throw new Exception("Invalid User");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return search;
	}


	/**
	 * validateLogin = validate input against AD using AD sAMAccountName & password
	 * @param dn
	 * @param password
	 * @return
	 * date added: 10th April 2009
	 */
	public boolean validateLogin(String dn,String password)
	{	
		LDAPConnection lc = null;
		boolean result = false;
		try
		{
			lc = new LDAPConnection();
			lc.connect(LDAP_HOST, LDAP_PORT);
			lc.bind(LDAP_VERSION, dn, password.getBytes(ENCODING_UTF8));

			System.out.println("validateLogin() LDAP Connect = " + lc.isConnected());
			result = lc.isConnected();
		}
		catch(Exception ex)
		{
			System.out.println("validateLogin() [" + dn + "] Invalid Login");
		}
		return result;	
	}

	/**
	 * getDN = retrieve DN of AD login
	 * @param userId
	 * @return
	 * date added: 10th April 2009
	 */
	public String getDN (String userId)
	{
		LDAPSearchResults searchResults = new LDAPSearchResults();
		LDAPConnection lc = null;
		String filterCriteria = null;
		String result = null;
		//String[] attrIDs = {"cn", "sn", "name", "givenName", "mailNickname", "name", "sAMAccountName", "mail", "ipPhone", "distinguishedName", "department"};

		try
		{	
			filterCriteria = "(" + LDAP_SEARCH_CRITERIA_1 + "="  + userId + ")";			
			lc = createConnection();		
			System.out.println("filterCriteria = " + filterCriteria);

			searchResults = lc.search( LDAP_SEARCH_BASE, LDAP_SEARCH_SCOPE, filterCriteria, null, true);			
			if(searchResults.hasMore())
			{				
				try
				{
					LDAPEntry nextEntry = searchResults.next();	 

					result = nextEntry.getDN();
					System.out.println("getDN() result [" + userId + "] >> " + result);  //getDN() result  [wckwah] = CN=William Chan Kai Wah,OU=DataCenter,OU=Chulan Tower,OU=ARB Users,DC=alrajhibank,DC=com,DC=my
				}
				catch (LDAPReferralException ldre)
				{
					System.out.println("getDN() [" + userId + "] Record not Found");	
				}
			}
		}
		catch( LDAPException e ) 
		{			
			e.printStackTrace();
		}
		catch( Exception e ) 
		{			
			e.printStackTrace();
		}   
		finally
		{
			try
			{
				if(lc != null)
				{
					lc.disconnect();
				}
			}
			catch( Exception e )
			{
				System.out.println( "Error: " + e.toString() );
				e.printStackTrace();
			}						
		}	        
		return result;
	}

	/**
	 * checkOU = derive OU from DN
	 * @param dn
	 * @return
	 * date added: 10th April 2009
	 */
	public String checkOU(String dn)
	{
		String[] element = dn.split(",");
		String OU = "";

		//added on 25th April 2012
		for (int i = 0; i < element.length; i++)
		{
			if(element[i].startsWith("OU"))
			{
				System.out.println("OU >> " + element[i]);

				OU += i > 1 ? "," : "";
				OU += element[i];
			}
		}

		System.out.println("DN no CN, no DC = " + OU);



		/*   	 	for(int j = 0; j < element.length; j++)
   	 	{
   	 		System.out.println("element " + j + " = " + element[j]);

   	 		if(element[j].split("=")[0].equalsIgnoreCase("OU"))
   	 		{
   	 			OU = element[j].split("=")[1];
   	 			System.out.println("element[j].split[1] = " + OU);

   	 			if(OU.endsWith("Branch"))
   	 			{
   	 				OU = OU + "," + element[j+1];

   	 				System.out.println("OU Branch = " + OU);   //OU=BangsarBranch,OU=Branches

   	 				return OU;
   	 			}
   	 		}
   	 	}*/
		if(OU!=null)
		{	
			return OU;
		}
		else
		{
			return "OU not exist";
		}
	}


	//Search DN = DC=alrajhibank,DC=com,DC=my
	//Filter = (&(objectclass=*)(OU=*))   --> list out all OU in LDAP

	/**
	 * checkOU = derive OU from DN
	 * @param dn
	 * @return
	 * date added: 10th April 2009
	 */
	public ArrayList getOU(String ou)
	{
		LDAPSearchResults searchResults = new LDAPSearchResults();
		LDAPConnection lc = null;
		String filterCriteria = null;
		ArrayList result = new ArrayList();
		String a = "";
		//String[] attr = {"OU"};

		try
		{	
			if(ou.equalsIgnoreCase("all"))
			{
				filterCriteria = "(&(objectclass=*)(OU=*))";
			}
			else
			{
				filterCriteria = "(&(objectclass=*)(OU=" + ou + "))";
			}

			lc = createConnection();		
			System.out.println("filterCriteria OU = " + filterCriteria);

			searchResults = lc.search( LDAP_SEARCH_BASE, LDAP_SEARCH_SCOPE, filterCriteria, null, true);	

			while(searchResults.hasMore())
			{				
				try
				{
					LDAPEntry nextEntry = searchResults.next();	  				
					a = nextEntry.getDN().replace(",DC=alrajhibank,DC=com,DC=my", "");
					//System.out.println("a = " + a);

					result.add(a);
				}
				catch (LDAPReferralException ldre)
				{
					System.out.println("getOU >> " + ou + " = Record not Found");	
				}
			}
		}
		catch( LDAPException e ) 
		{			
			e.printStackTrace();
		}
		catch( Exception e ) 
		{			
			e.printStackTrace();
		}   
		finally
		{
			try
			{
				if(lc != null)
				{
					lc.disconnect();
				}
			}
			catch( Exception e )
			{
				System.out.println( "Error: " + e.toString() );
				e.printStackTrace();
			}						
		}	    

		//System.out.println("result = " + result);


		return result;
	}

	/**
	 * searchBySamacctName = search by sAMAccountName, cn (name) from AD
	 * @param samaAccount
	 * @param cnName
	 * @return
	 * date added: 20th April 2009
	 */
	public ArrayList searchBySamacctName(String samaAccount, String cnName)
	{
		ArrayList result = new ArrayList();

		LDAPSearchResults searchResults = new LDAPSearchResults();
		LDAPConnection lc = null;
		String filterCriteria = null;
		String a = "";

		try
		{	
			if(samaAccount.equalsIgnoreCase("") && !cnName.equalsIgnoreCase(""))
			{
				filterCriteria = "(&(objectclass=*)(cn=*" + cnName + "*))";
			}
			else if(!samaAccount.equalsIgnoreCase("") && cnName.equalsIgnoreCase(""))
			{
				filterCriteria = "(&(objectclass=*)(sAMAccountName=*" + samaAccount + "*))";
			}
			else if(!samaAccount.equalsIgnoreCase("") && !cnName.equalsIgnoreCase(""))
			{
				filterCriteria = "(&(objectclass=*)(|(sAMAccountName=*" + samaAccount + "*)(cn=*" + cnName + "*)))";
			}

			lc = createConnection();		

			System.out.println("filterCriteria searchBySamacctName = " + filterCriteria);

			searchResults = lc.search( LDAP_SEARCH_BASE, LDAP_SEARCH_SCOPE, filterCriteria, null, true);	

			while(searchResults.hasMore())
			{				
				try
				{
					LDAPEntry nextEntry = searchResults.next();	  				
					a = nextEntry.getDN();
					//System.out.println("b = " + a);

					result.add(a);
				}
				catch (LDAPReferralException ldre)
				{
					System.out.println("searchBySamacctName >> " + samaAccount + " = Record not Found");	
				}
			}
		}
		catch( LDAPException e ) 
		{			
			e.printStackTrace();
		}
		catch( Exception e ) 
		{			
			e.printStackTrace();
		}   
		finally
		{
			try
			{
				if(lc != null)
				{
					lc.disconnect();
				}
			}
			catch( Exception e )
			{
				System.out.println( "Error: " + e.toString() );
				e.printStackTrace();
			}						
		}	
		return result;
	}


	/*public static void main(String[] args) {
		String loginId = "cmareader";
		String password = "CM@@2015";

		LDAPDemo ldap = new LDAPDemo();
		String dn = ldap.getDN(loginId);  //get DN

		if(dn != null)
		{
			if(password.equalsIgnoreCase(""))
			{
				System.out.println("Empty password : " + loginId);

				System.out.println("Password require. Please input password.");

			}
			else
			{
				if(!ldap.checkOU(dn).equalsIgnoreCase("OU not exist"))  //OU exist in ldap
				{
					//check OU exist in tbl_secuser_group
					String secuserGroup = ldap.checkOU(dn); //active group
					//String secuserGroup = sessionUser.loginGroupName(con, "OU=" + ldap.getOU(dn)); //active group
					System.out.println("secuserGroup = " + secuserGroup);

					if(!secuserGroup.equalsIgnoreCase(""))  //OU exist in tbl_secuser_group and status = active
					{
						boolean result = ldap.validateLogin(dn, password);   //validate samAcount and password from LDAP
						System.out.println("validateLogin() result = " + result);

						if(result == true)
						{
							//secuserGroup = OU=ServiceAccounts,OU=_HQ
							String [] OU = secuserGroup.split(",");
							System.out.println(OU[0].toString());
							//check of Login exist in local db
							MyUser secuser = new MyUser();
							secuser.setLoginId("cmareader");
							secuser.setStatus("ACTIVE");

							//Login exist in local db
							if(secuser != null)
							{
								if(secuser.getStatus().equalsIgnoreCase("Deleted"))  //deleted login
								{
									System.out.println("user deleted");
								}
								else
								{
									System.out.println("user is ACTIVE");			
								}
							}
						}

						else
						{
							System.out.println("Invalid password : " + loginId);						

						}
					}
					else   //group_status = deleted or not group not exist 
					{
						System.out.println("Group does not authorise access.");
					}
				}
				else
				{
					System.out.println("Group not exist. Please input a valid AD login.");
				}
			}
		}
	}*/


	private AdminUser getUserBasicAttributes(String loginId) {
		AdminUser user=null;
		LDAPConnection lc = null;

		try {
			lc = createConnection();	

			String searchFilter ="(cn="+loginId+")";
			SearchControls constraints = new SearchControls();
			constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String[] attrIDs = { "distinguishedName",
					"description",
					"userPrincipalName",
			"displayName"};
			constraints.setReturningAttributes(attrIDs);


			LDAPSearchResults searchResults = new LDAPSearchResults();

			searchResults = lc.search( LDAP_SEARCH_BASE, LDAP_SEARCH_SCOPE, searchFilter, attrIDs, true);



			//lc.se.search("DC=sarawakenergy,DC=com,DC=my", "sAMAccountName="
			// + loginId, constraints);
			if (searchResults.hasMore()) {
				LDAPEntry nextEntry = searchResults.next();	 
				System.out.println("distinguishedName "+ nextEntry.getAttributeSet().getAttribute("distinguishedName"));
				System.out.println("description "+ nextEntry.getAttribute("description"));
				System.out.println("userPrincipalName "+ nextEntry.getAttribute("userPrincipalName"));
				System.out.println("displayName "+ nextEntry.getAttribute("displayName"));
				System.out.println("telephonenumber "+nextEntry.getAttribute("telephonenumber"));
			}else{
				throw new Exception("Invalid User");
			}

		} catch( LDAPException e ) 
		{			
			e.printStackTrace();
		}
		catch( Exception e ) 
		{			
			e.printStackTrace();
		}   
		finally
		{
			try
			{
				if(lc != null)
				{
					lc.disconnect();
				}
			}
			catch( Exception e )
			{
				System.out.println( "Error: " + e.toString() );
				e.printStackTrace();
			}						
		}	
		return user;
	}


	public static void main(String[] args) {
		LDAPDemo ldap = new LDAPDemo();

		String loginId = "cmareader";//"cfj10000400";
		String password = "CM@@2015";

		System.out.println(ldap.getUserBasicAttributes("mail", loginId, ldap.getLdapContext()));

		//ldap.getUserBasicAttributes("neus1");
		//ArrayList a =  ldap.searchBySamacctName("neus1", "neus1");

	}


	/*public static void main(String[] args) 
	{
		LDAPDemo ldap = new LDAPDemo();

		String samaAccount = "vusr02";
		String cnName = "yuenyyh";

		//1. GET DN BY sAMAccountName
		String dn = ldap.getDN("wckwah");	
		String ou = "";
		String[] element = dn.split(",");


		if(dn!=null)
		{
			//2. CHECK OU			
			System.out.println("dn = " + dn); //dn = CN=William Chan Kai Wah,OU=DataCenter,OU=Chulan Tower,OU=ARB Users,DC=alrajhibank,DC=com,DC=my

			for(int j = 0; j < element.length; j++)
			{
				System.out.println("element " + j + "=" + element[j]);
			}


			for (int i = 0; i < element.length; i++)
			{
				if(element[i].startsWith("OU"))
				{
					System.out.println("OU >> " + element[i]);

					ou += i > 1 ? "," : "";
					ou += element[i];
				}
			}

			System.out.println("new dn no CN, no DC = " + ou);


			if(!ldap.checkOU(dn).equalsIgnoreCase("OU not exist"))
			{
				System.out.println("Valid OU");
				//3. LOGIN VALIDATION
				System.out.println("Login = " + ldap.validateLogin(dn,"testing"));
			}
		}
		else
		{
			System.out.println("Invalid Login Username");
		}


		//2. get OU list from LDAP
		//ldap.getOU("all");


		//3. search bu samaccountname & name(cn)
		//ldap.searchBySamacctName(samaAccount, cnName);


	}	*/
}
