<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="_csrf" content="${_csrf.token}" />
	<meta name="_csrf_header" content="${_csrf.headerName}" />
    <meta content="" name="description">
    <meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
            <%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
        </nav>
        <div id="page-wrapper">
            <%@ include file="../menuLastLogin.jsp"%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Payment Gateway Maintenance &gt; FPX Gateway
                        </div>
                        <div class="panel-body">
                            <form id="fpxForm" name="fpxForm" action="${pageContext.request.contextPath}/admin/spg/pymtGatewayMaintenance/fpx.do?${_csrf.parameterName}=${_csrf.token}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                ${successMsg}${errorMsg}
                                <c:if test="${not empty fpx}">
                                    <c:set var="item" scope="session" value="${fpx}"/>
                                    
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Merchant ID <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                   	<input class="form-control" id="merchantd" name="merchantId" placeholder="Merchant ID" type="text" maxlength="20" value="${item.merchant_id}">
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Seller Exchange ID <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="exchangeId" name="exchangeId" placeholder="Seller Exchange ID" type="text" maxlength="20" value="${item.exchange_id}" required>
                                    </div>
                                </div>
                                
                               <div class="form-group">
                                    <p class="col-lg-5 control-label">API Version <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="version" name="version" placeholder="API version" type="text" maxlength="255" value="${item.version}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Minimum Payment Amount :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="minPayAmt" name="minPayAmt" placeholder="Minimum Payment Amount" type="text" maxlength="10" value="${item.minimun_payment_amount }">
                                    </div>
                                </div>
								
								<div class="form-group">
                                    <p class="col-lg-5 control-label">Maximum Payment Amount :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="maxPayAmt" name="maxPayAmt" placeholder="Maximum Payment Amount" type="text" maxlength="10" value="${item.maximum_payment_amount }">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Transaction Submit URL <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="trxSubmitURL" name="trxSubmitURL" placeholder="Transaction Submit URL" type="text" maxlength="1000" value="${item.submit_url}" required>
                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <p class="col-lg-5 control-label">Transaction Query URL <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="trxQueryURL" name="trxQueryURL" placeholder="Transaction Query URL" type="text" maxlength="1000" value="${item.query_url}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Bank Listing URL <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="bankListURL" name="bankListURL" placeholder="Bank List URL" type="text" maxlength="1000" value="${item.bank_listing_url}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Seller Bank Code <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="sellerBankCode" name="sellerBankCode" placeholder="Seller Bank Code" type="text" maxlength="50" value="${item.seller_bank_code}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">CSR Country Name <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="csrCountryName" name="csrCountryName" placeholder="CSR Country Name" type="text" maxlength="50" value="${item.csr_country_name}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">CSR State Or Province Name <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="csrStateProvinceName" name="csrStateProvinceName" placeholder="CSR State Or Province Name" type="text" maxlength="50" value="${item.csr_state_or_province_name}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">CSR Locality Name <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="csrLocalityName" name="csrLocalityName" placeholder="CSR State Or Province Name" type="text" maxlength="50" value="${item.csr_locality_name}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">CSR Organization Name <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="csrOrgName" name="csrOrgName" placeholder="CSR Organization Name" type="text" maxlength="50" value="${item.csr_organization_name}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">CSR Organization Unit <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="csrOrgUnit" name="csrOrgUnit" placeholder="CSR Organization Unit" type="text" maxlength="50" value="${item.csr_organization_unit}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">CSR Common Name <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="csrCommonName" name="csrCommonName" placeholder="CSR Common Name" type="text" maxlength="50" value="${item.csr_common_name}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">CSR Email <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="csrEmail" name="csrEmail" placeholder="CSR Email" type="text" maxlength="50" value="${item.csr_email}" required>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">FPX Environment Current Certificate  :</p>	                                	
                                    <div class="col-lg-3">
	                                	<a class="" href="${pageContext.request.contextPath}/admin/spg/pymtGatewayMaintenance/downloadCert/public_key_file_name/<c:out value="${item.public_key_file_name}"/>">${item.public_key_file_name }</a>
	                                </div>
                                </div>                                
                                 <div class="form-group">
                                    <p class="col-lg-5 control-label"></p>	                                	
                                    <div class="col-lg-3">
                                    	<input class="file" id="fpxEnvCurrentCrt" name="fpxEnvCurrentCrt" type="file" >  
                                    </div>                                    
                                </div>
                             
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">FPX Environment Certificate Expiry <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="fpxEnvCrtExpiry" name="fpxEnvCrtExpiry" placeholder="FPX Environment Certificate Expiry" type="text" maxlength="1000" value="${item.public_key_expiry_date}" readonly="readonly">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Certificate Exchange - Private Key :</p>
	                                <div class="col-lg-3">
	                                	<a class="" href="${pageContext.request.contextPath}/admin/spg/pymtGatewayMaintenance/downloadCert/private_key_file_name/<c:out value="${item.private_key_file_name}"/>.do">${item.private_key_file_name }</a>
	                                </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label"></p>                                    
                                    <div class="col-lg-3">
                                    	<input class="file" id="privateKeyCrt" name="privateKeyCrt" type="file" >  
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">FPX Exchange Certificate Expiry <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="fpxExCrtExpiry" name="fpxExCrtExpiry" placeholder="FPX Exchange Certificate Expiry" type="text" maxlength="1000" value="${item.public_key_expiry_date}" readonly="readonly">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Certificate Exchange - Signing Reply :</p>                                    
                                    <div class="col-lg-3">
	                                	<a class="" href="${pageContext.request.contextPath}/admin/spg/pymtGatewayMaintenance/downloadCert/private_key_identity_cert_file_name/<c:out value="${item.private_key_identity_cert_file_name}"/>.do">${item.private_key_identity_cert_file_name}</a>
	                                </div>                                   
                                </div>
                                 
                                <div class="form-group">
                                    <p class="col-lg-5 control-label"></p>                                    
                                    <div class="col-lg-3">
                                    	<input class="file" id="privateKeyIdentityCert" name="privateKeyIdentityCert" type="file" > 
                                    	<div id="privateKeyIdentityCertDiv" class="fileDiv"></div>
                                    </div>
                                    <div class="col-lg-4">
                                    	<a class="btn btn-success btn-sm" href="${pageContext.request.contextPath}/admin/spg/pymtGatewayMaintenance/generateCSR.do">Generate CSR</a>
                                    </div>
                                </div>
                                
                               
                                
                                 <div class="form-group">
                                    <p class="col-lg-5 control-label">FPX Certificate Expiry Warning Days Before <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="warningDay" name="warningDay" placeholder="FPX Certificate Expiry Warning Days Before" type="text" maxlength="5" value="${item.certificate_expiry_warning_days_before}">
                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <p class="col-lg-5 control-label">FPX Certificate Expiry Warning Email<span style="color: red;">*</span> :</p>
                                    <div class="col-lg-3">
                                    	<input class="form-control" id="warningEmail" name="warningEmail" placeholder="FPX Certificate Expiry Warning Email" type="text" maxlength="50" value="${item.certificate_expiry_warning_email}">
                                    </div>
                                </div>    
                                
                                <div class="form-group">
                                    <p class="col-lg-5 control-label">Status <span style="color: red;">*</span> :</p>
                                    <div class="col-lg-6">
                                        <label class="radio-inline">
									      <input type="radio" name="statusOpt" ${not empty item.enabled && item.enabled == true ? 'checked' : ''} value="true">ENABLED
									    </label>
									    <label class="radio-inline">
									      <input type="radio" name="statusOpt"  ${not empty item.enabled && item.enabled == false ? 'checked' : ''} value="DISABLED">DISABLED
									    </label>
									</div>
								</div>                  
								
                                <div class="form-group">
                                    <div class="col-lg-5 col-lg-offset-3 text-right">
                                        <input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
                                        <button class="btn btn-primary"  id="submitBtn" type="button" data-toggle="modal" data-target="#modal">Update</button> <button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
                                    </div>
                                </div>
                                </c:if>
                                <!-- Add Confirmation Modal -->
								<div class="modal fade" id="modal" tabindex="-1">
						        <div class="modal-dialog">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button class="close" data-dismiss="modal" type="button"><span>&times;</span><span class="sr-only">Close</span></button>
											<h2 class="modal-title">Confirmation</h2>
						                </div>						
						                <div class="modal-body" id="add">
						                    Are you sure you want to edit FPX Gateway?
						                </div>						
						                <div class="modal-footer">
						                    <button class="btn btn-primary" id="add" name="add" type="submit">Update</button> <button class="btn btn-warning" data-dismiss="modal" type="button">Close</button> 
						                </div>
						            </div>
						        </div>
   								</div>
   								<!-- End Confirmation Modal -->
                            </form>
                        </div><!-- /.panel-body -->
                    </div><!-- /.panel -->
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        </div><!-- /#page-wrapper -->
    </div>
    <script type="text/javascript">
    function resetSS() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#fpxForm').find('.has-success').removeClass("has-success");
    	$('#fpxForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#fpxForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();    	
    }
        
    
    function resetForm() {
    	$('#').find('.has-error').removeClass("has-error");
    	$('#fpxForm').find('.has-success').removeClass("has-success");
    	$('#fpxForm').find('.form-control-feedback').removeClass('glyphicon-remove');
    	$('#fpxForm').find('.form-control-feedback').removeClass('glyphicon-ok');
    	$('.form-group').find('small.help-block').hide();
    	//Removing the error elements from the from-group
    	$('.form-group').removeClass('has-error has-feedback');
    	$('.form-group').find('i.form-control-feedback').hide();
    	$('#fpxForm').formValidation('resetForm', true);
    	$('select').val('');
    	
    	return false;
    }
    $(document).ready(function() {    	
    	
        $('#fpxForm').formValidation({
            framework: 'bootstrap',
            icon: {
                invalid: 'glyphicon glyphicon-remove',
            },
            fields: {
            	merchantId: {
                    validators: {
                        notEmpty: {
                            message: 'Merchant Id is required'
                        } 
                    }
                },
                exchangeId: {
                    validators: {
                        notEmpty: {
                            message: 'Exchange is required'
                        } 
                    }
                },
                version: {
                    validators: {
                        notEmpty: {
                            message: 'Version is required'
                        } 
                    }
                },
                minPayAmt : {
					validators : {
						callback : {
							callback : function(value, validator, $field) {
							   var reg = /^-?\d+\.?\d*$/;
								if(!reg.test(value)){
									   return {
												valid : false,
												message : 'Please enter valid amount'
											}
								   }  else{
									   return true;									   
								   }   										   
							   
								
							}
						}
						 
					}
				},  
				maxPayAmt : {
					validators : {
						callback : {
							callback : function(value, validator, $field) {
							   var reg = /^-?\d+\.?\d*$/;
								if(!reg.test(value)){
									   return {
												valid : false,
												message : 'Please enter valid amount'
											}
								   }  else{
									   return true;									   
								   }   										   
							   
								
							}
						}
						 
					}
				}, 
				trxSubmitURL: {
                    validators: {
                        notEmpty: {
                            message: 'Transaction Submit URL is required'
                        } 
                    }
                },
                trxQueryURL: {
                    validators: {
                        notEmpty: {
                            message: 'Transaction Query URL is required'
                        } 
                    }
                },
                bankListURL: {
                    validators: {
                        notEmpty: {
                            message: 'Bank List URL is required'
                        } 
                    }
                },   
                sellerBankCode: {
                    validators: {
                        notEmpty: {
                            message: 'Seller Bank Code is required'
                        } 
                    }
                },   
                csrCountryName: {
                    validators: {
                        notEmpty: {
                            message: 'CSR Country Name is required'
                        } 
                    }
                },   
                csrStateProvinceName: {
                    validators: {
                        notEmpty: {
                            message: 'CSR State or Province Name is required'
                        } 
                    }
                },   
                csrLocalityName: {
                    validators: {
                        notEmpty: {
                            message: 'CSR Locality Name is required'
                        } 
                    }
                },
                csrOrgName: {
                    validators: {
                        notEmpty: {
                            message: 'CSR Organization Name is required'
                        } 
                    }
                },
                csrOrgUnit: {
                    validators: {
                        notEmpty: {
                            message: 'CSR Organization Unit is required'
                        } 
                    }
                },
                csrCommonName: {
                    validators: {
                        notEmpty: {
                            message: 'CSR Common Name is required'
                        } 
                    }
                },
                csrEmail: {
                    validators: {
                        notEmpty: {
                            message: 'CSR Email is required'
                        } 
                    }
                },
                warningDay: {
                    validators: {
                        notEmpty: {
                            message: 'FPX Certificate Expiry Warning Days Before is required'
                        },                        
                        regexp: {
                            regexp: /^[0-9]+$/,
                            message: 'Please enter valid number'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Email is required'
                        },
                        emailAddress: {
                            message: 'The value is not a valid email address'
                        }
                    }
                }//end
            }
        });
    });
    </script>
</body>
</html>