package com.seb.admin.portal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class MyPayment implements Serializable {
	private static final long serialVersionUID = 1L;

	private int paymentId;

	private BigDecimal amount;

	private String bankId;

	private String contractAccountNumber;

	private Date createdDatetime;

	private String gatewayId;

	private String gatewayRef;

	private String gatewayStatus;

	private int isMobile;

	private Date paymentDatetime;

	private String status;

	private Date updatedDatetime;

	private String userEmail;
	
	private String userPaymentId;
	
	private String spgBankID;
	
	private String spgBankName;
	
	private String spgBankType;
	
	private String spgCardType;
	
	private String referencePrefix;
	
	private String notificationEmail;
	
	private String externalGatewayStatus;
	
	public MyPayment() {
	}

	public int getPaymentId() {
		return this.paymentId;
	}

	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	
	public String getUserPaymentId() {
		return userPaymentId;
	}

	public void setUserPaymentId(String userPaymentId) {
		this.userPaymentId = userPaymentId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBankId() {
		return this.bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getContractAccountNumber() {
		return this.contractAccountNumber;
	}

	public void setContractAccountNumber(String contractAccountNumber) {
		this.contractAccountNumber = contractAccountNumber;
	}

	public Date getCreatedDatetime() {
		return this.createdDatetime;
	}

	public void setCreatedDatetime(Date createdDatetime) {
		this.createdDatetime = createdDatetime;
	}

	public String getGatewayId() {
		
		if(this.gatewayId!=null && this.gatewayId.equalsIgnoreCase("MPG")){
			gatewayId = "Debit/Credit Card";
			//System.out.println("[PaymentReference]>>>>>>>gateway>>"+ gatewayId);
		}
		return this.gatewayId;
	}
	

	public String getNotificationEmail() {
		return notificationEmail;
	}

	public void setNotificationEmail(String notificationEmail) {
		this.notificationEmail = notificationEmail;
	}

	public void setGatewayId(String gatewayId) {
		this.gatewayId = gatewayId;
	}

	public String getGatewayRef() {
		if(gatewayRef !=null && gatewayRef!="")
			return this.gatewayRef;
			else return "";
	}

	public void setGatewayRef(String gatewayRef) {
		this.gatewayRef = gatewayRef;
	}

	public String getGatewayStatus() {
		if(gatewayStatus !=null && gatewayStatus!="")
		return this.gatewayStatus;
		else return "";
	}

	public void setGatewayStatus(String gatewayStatus) {
		this.gatewayStatus = gatewayStatus;
	}

	public int getIsMobile() {
		return this.isMobile;
	}

	public void setIsMobile(int isMobile) {
		this.isMobile = isMobile;
	}

	public Date getPaymentDatetime() {
		return this.paymentDatetime;
	}

	public void setPaymentDatetime(Date paymentDatetime) {
		this.paymentDatetime = paymentDatetime;
	}

	public String getStatus() {
		String myStatus = this.status;
		if(myStatus!=null && myStatus.equalsIgnoreCase("PENAUTH")) {
			myStatus = "B2B-Submitted for Processing";
		}
		return myStatus;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdatedDatetime() {
		
		return this.updatedDatetime;
	}

	public void setUpdatedDatetime(Date updatedDatetime) {
		this.updatedDatetime = updatedDatetime;
	}

	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getSpgBankID() {
		return spgBankID;
	}

	public void setSpgBankID(String spgBankID) {
		this.spgBankID = spgBankID;
	}

	public String getSpgBankName() {
		return spgBankName;
	}

	public void setSpgBankName(String spgBankName) {
		this.spgBankName = spgBankName;
	}

	public String getSpgBankType() {
		return spgBankType;
	}

	public void setSpgBankType(String spgBankType) {
		this.spgBankType = spgBankType;
	}

	public String getSpgCardType() {
		return spgCardType;
	}

	public void setSpgCardType(String spgCardType) {
		this.spgCardType = spgCardType;
	}
	
	

	public String getReferencePrefix() {
		return referencePrefix;
	}

	public void setReferencePrefix(String referencePrefix) {
		this.referencePrefix = referencePrefix;
	}
	
	

	public String getExternalGatewayStatus() {
		return externalGatewayStatus;
	}

	public void setExternalGatewayStatus(String externalGatewayStatus) {
		this.externalGatewayStatus = externalGatewayStatus;
	}

	@Override
	public String toString() {
		return "paymentId:[" + paymentId + "], amount:[" + amount + "], bankId:[" + bankId
				+ "], contractAccountNumber:[" + contractAccountNumber + "], createdDatetime:[" + createdDatetime
				+ "], gatewayId:[" + gatewayId + "], gatewayRef:[" + gatewayRef + "], gatewayStatus:[" + gatewayStatus
				+ "], isMobile:[" + isMobile + "], paymentDatetime:[" + paymentDatetime + "], status:[" + status
				+ "], updatedDatetime:[" + updatedDatetime + "], userEmail:[" + userEmail + "], userPaymentId:["
				+ userPaymentId + "], spgBankID:[" + spgBankID + "], spgBankName:[" + spgBankName + "], spgBankType:["
				+ spgBankType + "], spgCardType:[" + spgCardType + "]";
	}

	

	
}