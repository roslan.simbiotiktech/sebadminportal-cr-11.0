package com.seb.admin.portal.service;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.seb.admin.portal.model.AuditTrail;
import com.seb.admin.portal.model.AuditTrailType;
import com.seb.admin.portal.model.BankResponse;
import com.seb.admin.portal.model.CustomerAuditTrail;
import com.seb.admin.portal.model.MyBillPayReportReq;
import com.seb.admin.portal.model.MyBillPayReportResp;
import com.seb.admin.portal.model.PaymentGateway;
import com.seb.admin.portal.model.Report;

public interface ReportService {
	public AuditTrailType findAuditByID(String methodName);	
	public List<AuditTrailType> findAllAuditType() throws SQLException;
	public int auditLog(String name , AuditTrail at);

	public List<AuditTrail> findAllByAdmin (Date dateFrom, Date dateTo, int adminId);
	public List<AuditTrail> findAllByType (Date dateFrom, Date dateTo, int typeId);
	public List<AuditTrail> findAllByTypeAdmin (Date dateFrom, Date dateTo, int adminId, int typeId);
	public List<AuditTrail> findAllByDate (Date dateFrom, Date dateTo);

	//make a report
	public List<Report> findAllMakeReportByCondition(Date dateFrom, Date dateTo, String source, String type, String status);
	public Report findMakeAReportById(int id) throws SQLException;
	public Report findMakeAReportByIdAndDate(int id,String createdDatetime) throws Exception;
	public Report updateMakeAReport(Report report,String loginId,String createdDatetime) throws SQLException;

	//customer activities report
	public List<CustomerAuditTrail> findAllCustAuditReport(Date dateFrom, Date dateTo, String email) throws Exception;
	
	//user statistic report
	public long findTotalByStatus(String status, Date dateFrom, Date dateTo) throws Exception;
	
	//bill payment report
	public MyBillPayReportResp findAllBillPayReport (MyBillPayReportReq billPayReq)  throws Exception;
	public PaymentGateway findPaymentGatewayByID(String id) throws Exception;
	public List<PaymentGateway> findAllPaymentGateway() throws SQLException;
	public List<BankResponse> findByMerchantidAndResponsecode(String merchantId, String responseCode) throws SQLException;

	



}
