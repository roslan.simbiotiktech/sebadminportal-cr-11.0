<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
<title></title>
</head>
<body>
	<div id="wrapper">
		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>
		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>
			<div class="col-lg-12" style="width: inherit; min-width: 100%">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">Report &gt; Audit Trails</div>
						<div class="panel-body">
							<form action="${pageContext.request.contextPath}/admin/auditTrails.do" class="form-horizontal" id="searchATForm" method="post"
								name="searchATForm">
								${successMsg}${errorMsg}
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Date From <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='dateFrom'>
											<input class="form-control datepicker" id="indateFrom" name="indateFrom" readonly type='text'> <span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Date To <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<div class='input-group date' id='dateTo'>
											<input class="form-control datepicker" id="indateTo" name="indateTo" readonly type='text'> <span class="input-group-addon"><span
												class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Type <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<input type="hidden" id="displayType" name="displayType" value="" /> <select class="form-control capital" id="selectType" name="selectType">
											<option value="">Please Select</option>
											<option value="ALL">ALL</option>
											<c:forEach items="${typeList}" var="item">
												<option value="${item.id}">${item.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-group">
									<p class="col-lg-5 control-label">
										Login ID <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
										<input type="hidden" id="displayName" name="displayName" value="" /> <select class="form-control" id="selectLoginId" name="selectLoginId">
											<option value="">Please Select</option>
											<option value="ALL">ALL</option>
											<c:forEach items="${adminList}" var="item">
												<option value="${item.id}">${item.loginId}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-5 col-lg-offset-3 text-right">
										<input name="${_csrf.parameterName}" type="hidden" value="${_csrf.token}">
										<button class="btn btn-primary" type="submit">Search</button>
										<button class="btn btn-warning" id="cancelBtn1" onclick="return resetForm();" type="button">Cancel</button>
									</div>
								</div>
							</form>
							<div id="resultDiv" style="display: none; margin-top: 10%">
								<form action="${pageContext.request.contextPath}/admin/auditTrails.do?${_csrf.parameterName}=${_csrf.token}" class="form-horizontal"
									id="viewATForm" method="post" name="viewATForm">
									<h4>
										Search Results : Date From :<b>${dateFrom}</b>, Date To :<b>${dateTo}</b>, Login ID :<b>${displayName}</b>, Type :<b>${displayType}</b>
									</h4>
									<div class="form-group">
										<div class="alert alert-danger" id="messages" role="alert" style="display: none;"></div>
									</div>
									<table cellspacing="0" class="table table-striped table-bordered" id="tblSearch">
										<thead>
											<tr>
												<th>No.</th>
												<th>Date</th>
												<th>Login ID</th>
												<th>Activity</th>
												<th>Description</th>
											</tr>
											<!-- #viewATForm -->
											<!-- #resultDiv -->
										</thead>
										<tbody>
											<c:if test="${not empty auditList}">
												<c:forEach items="${auditList}" var="item" varStatus="status">
													<tr>
														<td>${status.index+1}</td>
														<td><fmt:formatDate type="both" dateStyle="short" timeStyle="medium" value="${item.auditDatetime}" pattern="dd/MM/YYYY hh:mm:ss a" /></td>
														<td>${item.adminUser.loginId}</td>
														<td>${item.activity}</td>
														<td>${item.oldValue}${item.newValue}<br>${item.remark}</td>
													</tr>
												</c:forEach>
											</c:if>

										</tbody>
									</table>
								</form>
								<!-- #viewATForm -->
							</div>
							<!-- #resultDiv -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->
	</div>
	<script type="text/javascript">
	function resetForm() {
		$('#').find('.has-error').removeClass("has-error");
		$('#searchATForm').find('.has-success').removeClass("has-success");
		$('#searchATForm').find('.form-control-feedback').removeClass('glyphicon-remove');
		$('#searchATForm').find('.form-control-feedback').removeClass('glyphicon-ok');
		$('#searchATForm').formValidation('resetForm', true);
		
		$('.form-group').find('small.help-block').hide();
		$('.form-group').removeClass('has-error has-feedback');
		$('.form-group').find('i.form-control-feedback').hide();
		
		setTimeout(function() {
			$('#indateTo').val(moment(new Date()).format('DD/MM/YYYY'));
			$('#indateFrom').val(moment(new Date()).format('DD/MM/YYYY'));
		}, 50);
		return false;
	}
	$(document).ready(function() {
		if (${auditList != null && auditList.size() >= 0}) {
			$('#resultDiv').show();
			var table = $('#tblSearch').DataTable({
				//  responsive: true,   
				bFilter: false,
				bSort: false,
				lengthMenu: [
					[50, 100],
					[50, 100]
				]
			});
		}
		$('#dateFrom').datetimepicker({
			format: 'DD/MM/YYYY',
			defaultDate: new Date(),
			ignoreReadonly: true
		}).on("dp.change", function(e) {
			$('#searchATForm').formValidation('revalidateField', 'indateFrom');
		});
		$('#dateTo').datetimepicker({
			format: 'DD/MM/YYYY',
			defaultDate: new Date(),
			ignoreReadonly: true
		}).on("dp.change", function(e) {
			$('#searchATForm').formValidation('revalidateField', 'indateTo');
		});
		$('#selectLoginId').change(function() {
			var displayName = $("#selectLoginId option:selected").text();
			$('#displayName').val(displayName);
		});
		$('#selectType').change(function() {
			var displayType = $("#selectType option:selected").text();
			$('#displayType').val(displayType);
		});
		$('#searchATForm').formValidation({
			framework: 'bootstrap',
			icon: {
				invalid: 'glyphicon glyphicon-remove',
			},
			fields: {
				indateFrom: {
					validators: {
						callback: {
							callback: function(value, validator, $field) {
								if (value == '') {
									$('#indateFrom').val(moment(new Date()).format('DD/MM/YYYY'));
									return true;
								}
								return true;
							}
						},
						date: {
							format: 'DD/MM/YYYY',
							max: 'indateTo',
							message: 'The start date is not a valid'
						}
					}
				},
				indateTo: {
					validators: {
						date: {
							format: 'DD/MM/YYYY',
							min: 'indateFrom',
							message: 'The end date is not a valid'
						},
						callback: {
							callback: function(value, validator, $field) {
								if (value == '') {
									$('#indateTo').val(moment(new Date()).format('DD/MM/YYYY'));
									//console.log("value>" + $('#indateTo').val());
									return true;
								}
								return true;
							}
						},
						format: 'DD/MM/YYYY'
					}
				},
				selectLoginId: {
					validators: {
						callback: {
							message: 'Please Select Login ID',
							callback: function(value, validator, $field) {
								var options = validator.getFieldElements('selectLoginId').val();
								return (options != null && options.length > 0);
							}
						}
					}
				},
				selectType: {
					validators: {
						callback: {
							message: 'Please Select Type',
							callback: function(value, validator, $field) {
								var options = validator.getFieldElements('selectType').val();
								return (options != null && options.length > 0);
							}
						}
					}
				},
			}
		}).on('success.field.fv', function(e, data) {
		      if (data.field === 'indateFrom' && !data.fv.isValidField('indateTo')) {
		        // We need to revalidate the end date
		        data.fv.revalidateField('indateTo');
		    }
		
		    if (data.field === 'indateTo' && !data.fv.isValidField('indateFrom')) {
		        // We need to revalidate the start date
		        data.fv.revalidateField('indateFrom');
		    }
		});
	});
    </script>
</body>
</html>