package com.seb.admin.portal.model;

import java.util.Comparator;

public class MyDropDown implements Comparator<MyDropDown> {
	
	private String text;
	private String value;
	private String description;
	private String dateFrom;
	private String dateTo;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	@Override
	public String toString() {
		return "MyDropDown [text=" + text + ", value=" + value + ", description=" + description + ", dateFrom="
				+ dateFrom + ", dateTo=" + dateTo + "]";
	}
	@Override
	public int compare(MyDropDown o1, MyDropDown o2) {
		 return o1.getText().compareToIgnoreCase(o2.getText());
	}
	
	
	

}
