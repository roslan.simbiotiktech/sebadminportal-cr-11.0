package com.seb.admin.portal.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Arrays;

import com.seb.admin.portal.constant.ApplicationConstant;;


public class MyReportRequest implements Serializable {
	private static final long serialVersionUID = 1L;


	private int transId;

	private String caseNumber;

	private String remark;

	private String status;

	private String userName;
	
	private String preCaseNumber;
	
	private String preStatus;
	
	private String preRemark;
	
	private String createdDate;

	public MyReportRequest() {
	}

	public int getTransId() {
		return this.transId;
	}

	public void setTransId(int transId) {
		this.transId = transId;
	}

	public String getCaseNumber() {
		return this.caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPreCaseNumber() {
		return preCaseNumber;
	}

	public void setPreCaseNumber(String preCaseNumber) {
		this.preCaseNumber = preCaseNumber;
	}

	public String getPreStatus() {
		return preStatus;
	}

	public void setPreStatus(String preStatus) {
		this.preStatus = preStatus;
	}

	public String getPreRemark() {
		return preRemark;
	}

	public void setPreRemark(String preRemark) {
		this.preRemark = preRemark;
	}
	
	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public String toString() {
		return "MyReportRequest [transId=" + transId + ", caseNumber=" + caseNumber + ", remark=" + remark + ", status="
				+ status + ", userName=" + userName + ", preCaseNumber=" + preCaseNumber + ", preStatus=" + preStatus
				+ ", preRemark=" + preRemark + "]";
	}

	

}