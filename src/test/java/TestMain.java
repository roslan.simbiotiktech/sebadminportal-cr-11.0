import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestMain {
	
	public static void main(String[] args) {
		String patternString = "[0-9]+";
		Pattern pattern = Pattern.compile(patternString);

		String caseid = "1286103";
		Matcher matcher = pattern.matcher(caseid);
		boolean matches = matcher.matches();
		System.out.println(matches);
	}
}
