<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="" name="author"><%@ include file="../menuTitle.jsp"%><%@ include file="../global.jsp"%>
</head>

<body>
	<div id="wrapper">
		<nav class="navbar navbar-default navbar-static-top"
			style="margin-bottom: 0">
			<%@ include file="../menuTop.jsp"%><%@ include file="../menuLeft.jsp"%>
		</nav>

		<div id="page-wrapper">
			<%@ include file="../menuLastLogin.jsp"%>

			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading" style="font-weight: bold">
							Maintenance &gt; Service Counter Locator &gt; Add</div>

						<div class="panel-body">
							<form class="form-horizontal" id="addCSCForm" method="post" name="addCSCForm" action="${pageContext.request.contextPath}/admin/addCSC.do";>
								${successMsg}${errorMsg}		
								<div class="form-group">
								
									<p class="col-lg-5 control-label">
										Region <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<select class="form-control capital" id="selectRegion"
											name="selectRegion">
											<option value="">Please Select</option>
											<c:forEach items="${regionList}" var="item">
												<option value="${item}">${fn:replace(item, "_", " ")}</option>
											</c:forEach>
										</select>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Area <span style="color: red;">*</span> :
									</p>
									<div id="stationDiv" class="col-lg-3">
										<select class="form-control capital" id="selectStation"
											name="selectStation">
											<option value="">Please Select</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Address <span style="color: red;">*</span> :
									</p>
									<div class="col-lg-3">
                                        <input class="form-control" id="add1" maxlength="100" name="add1" placeholder="Address Line1" type="text">
                                	</div>
                                </div>
                                
                                <div class="form-group">
									<p class="col-lg-5 control-label">
										 <span style="color: red;"></span> 
									</p>
									<div class="col-lg-3">
                                        <input class="form-control" id="add2" maxlength="100" name="add2" placeholder="Address Line2" type="text">
                                	</div>
                                </div>
                                
                                 <div class="form-group">
									<p class="col-lg-5 control-label">
										 <span style="color: red;"></span> 
									</p>
									<div class="col-lg-3">
                                        <input class="form-control" id="add3" maxlength="100" name="add3" placeholder="Address Line 3" type="text">
                                	</div>
                                </div>
                                
                                <div class="form-group">
									<p class="col-lg-5 control-label">
										Postcode <span style="color: red;">*</span> :
									</p>
	                                <div class="col-lg-3">
	                                     <input class="form-control" id="postcode" maxlength="5" name="postcode" placeholder="Postcode" type="text">
	                                </div>
	                            </div>
	                            
	                             <div class="form-group">
									<p class="col-lg-5 control-label">
										City <span style="color: red;">*</span> :
									</p>
	                                 <div class="col-lg-3">
                                        <input class="form-control" id="city" maxlength="100" name="city" placeholder="City" type="text">
                               		</div>
	                            </div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Longitude <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="longitude" maxlength="12"
											name="longitude" placeholder="Longitude" type="text">
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Latitude <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<input class="form-control" id="latitude" maxlength="12"
											name="latitude" placeholder="Latitude" type="text">
									</div>
								</div>

								<div class="form-group">
									<p class="col-lg-5 control-label">
										Opening Hour  <span style="color: red;">*</span> :
									</p>

									<div class="col-lg-3">
										<textarea class="form-control" cols="23" id="openingHr"  name="openingHr" 
											maxlength="200" rows="10"></textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-4 col-lg-offset-4 text-right">
										<input name="${_csrf.parameterName}" type="hidden"
											value="${_csrf.token}">
										<button class="btn btn-primary" id="submitBtn"
											type="submit" data-toggle="modal" data-target="#modal">Add</button>
										<button class="btn btn-warning" id="cancelBtn1" type="reset">Cancel</button>
									</div>
								</div>
								<!-- Add Confirmation Modal -->
								<div class="modal fade" id="modal" tabindex="-1">
						        <div class="modal-dialog">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button class="close" data-dismiss="modal" type="button"><span>&times;</span><span class="sr-only">Close</span></button>
											<h2 class="modal-title">Confirmation</h2>
						                </div>						
						                <div class="modal-body" id="add">
						                    Are you sure you want to add service counter locator?
						                </div>						
						                <div class="modal-footer">
						                    <button class="btn btn-primary" id="add" type="submit">Add</button> <button class="btn btn-warning" data-dismiss="modal" type="button">Close</button> 
						                </div>
						            </div>
						        </div>
   								</div>
   								<!-- End Confirmation Modal -->
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$(document).ready(function() {
		$('#selectRegion').change(function() {
		    var selectedText = $(this).find("option:selected").val();
		    if (selectedText != "") {
		    	//console.log("selectedText>"+selectedText);
		        var json = {
		            "region": selectedText
		        };
		        var token = $("meta[name='_csrf']").attr("content");
		        var header = $("meta[name='_csrf_header']").attr("content");
		        $.ajax({
		            url: "${pageContext.request.contextPath}/admin/getStation.do?",
		            type: "POST",
		            data: JSON.stringify({ region: selectedText }),
		           // cache: false,
		            beforeSend: function(xhr) {
		                xhr.setRequestHeader(header, token);
		                xhr.setRequestHeader("Accept", "application/json");
		                xhr.setRequestHeader("Content-Type", "application/json");
		                sebApp.showIndicator();			                
		            },
		            success: function(data) {
		            	var html = '<select class="form-control capital" id="selectStation" name="selectStation"><option value="">Please Select</option>';
		                  $.each(data, function(index, currEmp) {
		                	//console.log(currEmp.id+","+currEmp.station);
		                    html += '<option value="' + currEmp.id + '">' + currEmp.station + '</option>';
		                }); 
		                html += '</select>';
		                $('#stationDiv').html(html);
		                //console.log(html);
		                reset();
		            },
		            failure: function(data) {},
		            complete: function(e){ sebApp.hideIndicator();}
		        });
		    }
		});
	    //$('#selectOpenHrFrom').data("DateTimePicker").date("18:56:00");
	    $(function () {
            $('#selectOpenHrFrom').datetimepicker({
            	defaultDate:new Date(),
            	format: 'LT'
            });
            
            $('#selectOpenHrTo').datetimepicker({
            	defaultDate:new Date(),
            	format: 'LT'
            });
            
           
        });
	    
	    $("#postcode").keypress(function (e) {
	         //if the letter is not digit then display error and don't type anything
	         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	                   return false;
	        }
	       });
	    
	    /* $("#latitude").keypress(function (e) {
	         //if the letter is not digit then display error and don't type anything
	         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	                   return false;
	        }
	       });
	    
	    $("#longitude").keypress(function (e) {
	         //if the letter is not digit then display error and don't type anything
	         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	                   return false;
	        }
	       }); */
	    
	    $("#submitBtn").on('click', function(e){    	   
    	    e.preventDefault();
    	});
	   	    
	    $("#cancelBtn1").click(function() {
	    	
	        $('#addCSCForm').find('.has-error').removeClass("has-error");
	        $('#addCSCForm').find('.has-success').removeClass("has-success");
	        $('#addCSCForm').find('.form-control-feedback').removeClass('glyphicon-remove');
	        $('#addCSCForm').find('.form-control-feedback').removeClass('glyphicon-ok');
	        $('.form-group').find('small.help-block').hide();
	    });
	    
	    function reset(){
	    	$('#addCSCForm').find('.has-error').removeClass("has-error");
	        $('#addCSCForm').find('.has-success').removeClass("has-success");
	        $('#addCSCForm').find('.form-control-feedback').removeClass('glyphicon-remove');
	        $('#addCSCForm').find('.form-control-feedback').removeClass('glyphicon-ok');
	        $('.form-group').find('small.help-block').hide();
	    }
	    
	    
	    $('#addCSCForm').formValidation({
	        framework: 'bootstrap',
	        icon: {
	            invalid: 'glyphicon glyphicon-remove',
	        },
	        fields: {
	        	selectRegion: {
                    validators: {
                        callback: {
                            message: 'Please Select Region',
                            callback: function(value, validator, $field) {
                            	// alert($('#selectOpenHrFrom').data('date') );
                                // Get the selected options
                                var options = validator.getFieldElements('selectRegion').val();
                                return (options != null && options.length >1);
                            }
                        }
                    }
                },
                selectStation: {
                    validators: {
                        callback: {
                            message: 'Please Select Area',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('selectRegion').val();
                                return (options != null && options.length >1);
                            }
                        }
                    }
                },
	            longitude: {
	                validators: {	                	
	                    notEmpty: {
	                        message: 'Longitude is required'
	                    },
	                    between: {
	                    	 min: -180,
	                         max: 180,
	                         message: 'The longitude must be between -180.0 and 180.0'
	                    }
	                }
	            },
	            latitude: {
	                validators: {
	                    notEmpty: {
	                        message: 'Latitude is required'
	                    },
	                    between: {
	                        min: -90,
	                        max: 90,
	                        message: 'The latitude must be between -90.0 and 90.0'
	                    }
	                }
	            },
	            add1: {
	                validators: {
	                    notEmpty: {
	                        message: 'Address is required'
	                    },
	                    stringLength: {
                            max: 200,
                            message: 'Address should not more than 200 characters'
                        }
	                }
	            },
	            postcode: {
	                validators: {
	                    notEmpty: {
	                        message: 'Postcode is required'
	                    }	                    
	                }
	            },
	            city: {
	                validators: {
	                    notEmpty: {
	                        message: 'City is required'
	                    }	                    
	                }
	            },
	            openingHr: {
	                validators: {
	                    notEmpty: {
	                        message: 'Opening Hour is required'
	                    },
	                    stringLength: {
	                    	
                            max: 200,
                            message: 'Opening Hour should not more than 200 characters'
                        }
	                }
	            }
	        }
	    });
	});
    </script>
</body>
</html>